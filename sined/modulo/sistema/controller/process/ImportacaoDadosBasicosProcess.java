package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Command;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.OnErrors;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.report.Report;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.StringUtils;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Campanha;
import br.com.linkcom.sined.geral.bean.Campanhalead;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Clienteespecialidade;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contato;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Enderecotipo;
import br.com.linkcom.sined.geral.bean.Especialidade;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Importacaoresultado;
import br.com.linkcom.sined.geral.bean.Lead;
import br.com.linkcom.sined.geral.bean.Leadhistorico;
import br.com.linkcom.sined.geral.bean.Leadqualificacao;
import br.com.linkcom.sined.geral.bean.Leadsegmento;
import br.com.linkcom.sined.geral.bean.Leadsituacao;
import br.com.linkcom.sined.geral.bean.Leadtelefone;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialclasse;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Ncmcapitulo;
import br.com.linkcom.sined.geral.bean.Pessoa;
import br.com.linkcom.sined.geral.bean.PessoaContato;
import br.com.linkcom.sined.geral.bean.Pessoacategoria;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.geral.bean.Sexo;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Telefonetipo;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoDadosBasicosTipo;
import br.com.linkcom.sined.geral.bean.enumeration.Tiporesponsavel;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.geral.service.CampanhaService;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.ConcorrenteService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContatoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.EspecialidadeService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.ImportacaoresultadoService;
import br.com.linkcom.sined.geral.service.LeadService;
import br.com.linkcom.sined.geral.service.LeadhistoricoService;
import br.com.linkcom.sined.geral.service.LeadqualificacaoService;
import br.com.linkcom.sined.geral.service.LeadsegmentoService;
import br.com.linkcom.sined.geral.service.LeadsituacaoService;
import br.com.linkcom.sined.geral.service.LeadtelefoneService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.geral.service.NcmcapituloService;
import br.com.linkcom.sined.geral.service.PessoaContatoService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.PessoacategoriaService;
import br.com.linkcom.sined.geral.service.SegmentoService;
import br.com.linkcom.sined.geral.service.TelefoneService;
import br.com.linkcom.sined.geral.service.UfService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ImportacaoDadosBasicosFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ImportacaoDadosLeadBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ImportacaoDadosMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ImportacaoDadosPessoaBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.SinedExcel;
import br.com.linkcom.sined.util.report.MergeReport;

import com.ibm.icu.util.Calendar;

@Controller(path="/sistema/process/ImportacaoDadosBasicos", authorizationModule=ProcessAuthorizationModule.class)
public class ImportacaoDadosBasicosProcess extends MultiActionController {
	
	private MaterialgrupoService materialgrupoService;
	private UnidademedidaService unidademedidaService;
	private ContagerencialService contagerencialService;
	private MaterialtipoService materialtipoService;
	private CentrocustoService centrocustoService;
	private MaterialService materialService;
	private NcmcapituloService ncmcapituloService;
	private ImportacaoresultadoService importacaoresultadoService;
	private EmpresaService empresaService;
	private ArquivoDAO arquivoDAO;
	private PessoaService pessoaService;
	private ClienteService clienteService;
	private FornecedorService fornecedorService;
	private UfService ufService;
	private MunicipioService municipioService;
	private CategoriaService categoriaService;
	private TelefoneService telefoneService;
	private ContatoService contatoService;
	private EnderecoService enderecoService;
	private PessoacategoriaService pessoacategoriaService;
	private LeadsituacaoService leadsituacaoService;
	private LeadqualificacaoService leadqualificacaoService;
	private SegmentoService segmentoService;
	private LeadService leadService;
	private LeadhistoricoService leadhistoricoService;
	private LeadtelefoneService leadtelefoneService;
	private LeadsegmentoService leadsegmentoService;
	private ColaboradorService colaboradorService;
	private EspecialidadeService especialidadeService;
	private CampanhaService campanhaService;
	private ConcorrenteService concorrenteService;
	private PessoaContatoService pessoaContatoService;
	
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setLeadsegmentoService(LeadsegmentoService leadsegmentoService) {
		this.leadsegmentoService = leadsegmentoService;
	}
	public void setLeadtelefoneService(LeadtelefoneService leadtelefoneService) {
		this.leadtelefoneService = leadtelefoneService;
	}
	public void setLeadhistoricoService(
			LeadhistoricoService leadhistoricoService) {
		this.leadhistoricoService = leadhistoricoService;
	}
	public void setLeadService(LeadService leadService) {
		this.leadService = leadService;
	}
	public void setLeadqualificacaoService(
			LeadqualificacaoService leadqualificacaoService) {
		this.leadqualificacaoService = leadqualificacaoService;
	}
	public void setSegmentoService(SegmentoService segmentoService) {
		this.segmentoService = segmentoService;
	}
	public void setLeadsituacaoService(LeadsituacaoService leadsituacaoService) {
		this.leadsituacaoService = leadsituacaoService;
	}
	public void setPessoacategoriaService(
			PessoacategoriaService pessoacategoriaService) {
		this.pessoacategoriaService = pessoacategoriaService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setContatoService(ContatoService contatoService) {
		this.contatoService = contatoService;
	}
	public void setTelefoneService(TelefoneService telefoneService) {
		this.telefoneService = telefoneService;
	}
	public void setUfService(UfService ufService) {
		this.ufService = ufService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setImportacaoresultadoService(
			ImportacaoresultadoService importacaoresultadoService) {
		this.importacaoresultadoService = importacaoresultadoService;
	}
	public void setNcmcapituloService(NcmcapituloService ncmcapituloService) {
		this.ncmcapituloService = ncmcapituloService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	public void setUnidademedidaService(
			UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setMaterialgrupoService(
			MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setEspecialidadeService(EspecialidadeService especialidadeService) {
		this.especialidadeService = especialidadeService;
	}
	public void setCampanhaService(CampanhaService campanhaService) {
		this.campanhaService = campanhaService;
	}
	public void setConcorrenteService(ConcorrenteService concorrenteService) {
		this.concorrenteService = concorrenteService;
	}
	public void setPessoaContatoService(PessoaContatoService pessoaContatoService) {
		this.pessoaContatoService = pessoaContatoService;
	}
	
	@DefaultAction
	@Command(session=false)
	public ModelAndView index(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro) {
		filtro.setArquivo(null);
		return new ModelAndView("process/importacaoDadosBasicos", "filtro", filtro);
	}
	
	@Input("index")
	@OnErrors("index")
	@Command(session=false)
	public ModelAndView abrirConfirmacaoDados(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro) throws UnsupportedEncodingException {
		if (filtro.getArquivo() == null || 
			filtro.getArquivo().getContent() == null || 
			filtro.getArquivo().getContent().length == 0)
			throw new SinedException("O arquivo deve ser informado.");		
		
		String strFile = new String(filtro.getArquivo().getContent());
		String[] linhas = strFile.split("\\r?\\n");
		List<String> linhasPreenchidas = new ArrayList<String>();
		for (int i = 1; i < linhas.length; i++) {
			String linha = linhas[i];
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				linhasPreenchidas.add(linha);
			}
		}
		if(linhasPreenchidas.size() == 0){
			throw new SinedException("Nenhum registro a ser importado.");		
		}
		
		int hora = SinedDateUtils.getDateProperty(SinedDateUtils.currentDate(), Calendar.HOUR_OF_DAY);
		boolean diaUtil = !SinedDateUtils.isFinalSemana(SinedDateUtils.currentDate());
		if((diaUtil && hora >= 7 && hora <= 17) && linhasPreenchidas.size() > 300){
			throw new SinedException("Entre os hor�rios 07:00 e 18:00, s� � permitido importar 300 registros. (Planilha com " + linhasPreenchidas.size() + " registros)");
		}
		if((hora < 7 || hora > 17) && linhasPreenchidas.size() > 1000){
			throw new SinedException("S� � permitido importar 1000 registros. (Planilha com " + linhasPreenchidas.size() + " registros)");
		}
		
		String chaveArquivo = "CHAVE_ARQUIVO_IMPORTACAO_" + SinedUtil.datePatternForReport();
		request.getSession().setAttribute(chaveArquivo, filtro.getArquivo());
		filtro.setChaveArquivo(chaveArquivo);
		
		if(filtro.getImportacaoDadosBasicosTipo().equals(ImportacaoDadosBasicosTipo.MATERIAL)){
			return this.makeConferenciaDadosMaterial(request, filtro, linhasPreenchidas);
		} else if(filtro.getImportacaoDadosBasicosTipo().equals(ImportacaoDadosBasicosTipo.CLIENTE) || filtro.getImportacaoDadosBasicosTipo().equals(ImportacaoDadosBasicosTipo.FORNECEDOR)){
			return this.makeConferenciaDadosPessoa(request, filtro, linhasPreenchidas, filtro.getImportacaoDadosBasicosTipo());
		} else if(filtro.getImportacaoDadosBasicosTipo().equals(ImportacaoDadosBasicosTipo.LEAD)){
			return this.makeConferenciaDadosLead(request, filtro, linhasPreenchidas);
		} else {
			throw new SinedException("Tipo inv�lido.");
		}
	}
	
	class NoDuplicatesList<E> extends LinkedList<E> {
		
		private static final long serialVersionUID = 9002016921774364875L;

		@Override
	    public boolean add(E e) {
	        if (this.contains(e)) {
	            return false;
	        }
	        else {
	            return super.add(e);
	        }
	    }

	    @Override
	    public boolean addAll(Collection<? extends E> collection) {
	        Collection<E> copy = new LinkedList<E>(collection);
	        copy.removeAll(this);
	        return super.addAll(copy);
	    }

	    @Override
	    public boolean addAll(int index, Collection<? extends E> collection) {
	        Collection<E> copy = new LinkedList<E>(collection);
	        copy.removeAll(this);
	        return super.addAll(index, copy);
	    }

	    @Override
	    public void add(int index, E element) {
	        if (this.contains(element)) {
	            return;
	        }
	        else {
	            super.add(index, element);
	        }
	    }
	}   
	
	private ModelAndView makeConferenciaDadosLead(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro, List<String> linhasPreenchidas) {
		List<ImportacaoDadosLeadBean> listaLeadBean = new ArrayList<ImportacaoDadosLeadBean>();
		
		NoDuplicatesList<ImportacaoDadosLeadBean> listaLeadComErro = new NoDuplicatesList<ImportacaoDadosLeadBean>();
		
		List<ImportacaoDadosLeadBean> listaLeadErrosContato = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosSituacao = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosQualificacao = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosResponsavel = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosMunicipio = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosUf = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosCep = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosSegmento = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosSexo = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosDtretorno = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosCampanha = new ArrayList<ImportacaoDadosLeadBean>();
		List<ImportacaoDadosLeadBean> listaLeadErrosProximoPasso = new ArrayList<ImportacaoDadosLeadBean>();
		
		for (int i = 0; i < linhasPreenchidas.size(); i++) {
			String linha = linhasPreenchidas.get(i);
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				String[] campos = linha.split(";");
				
				ImportacaoDadosLeadBean bean = new ImportacaoDadosLeadBean();
				List<String> listaErro = new ArrayList<String>();
				List<String> listaAvisos = new ArrayList<String>();
				
				try{
					bean.setLinha(i+1);
					
					if(campos.length > 0) bean.setContato(campos[0]);
					if(campos.length > 1) bean.setSexoStr(campos[1]);
					if(campos.length > 2) bean.setEmpresa(campos[2]);
					if(campos.length > 3) bean.setEmail(campos[3]);
					if(campos.length > 4) bean.setWebsite(campos[4]);
					if(campos.length > 5) bean.setResponsavelCpfStr(campos[5]);
					if(campos.length > 6) bean.setSituacaoStr(campos[6]);
					if(campos.length > 7) bean.setQualificacaoStr(campos[7]);
					if(campos.length > 8) bean.setTelefonePrincipal(campos[8]);
					if(campos.length > 9) bean.setTelefoneCelular(campos[9]);
					if(campos.length > 10) bean.setTelefoneComercial(campos[10]);
					if(campos.length > 11) bean.setSegmentoStr(campos[11]);
					if(campos.length > 12) bean.setEnderecologradouro(campos[12]);
					if(campos.length > 13) bean.setEndereconumero(campos[13]);
					if(campos.length > 14) bean.setEnderecocomplemento(campos[14]);
					if(campos.length > 15) bean.setEnderecobairro(campos[15]);
					if(campos.length > 16) bean.setEnderecoufStr(campos[16]);
					if(campos.length > 17) bean.setEnderecomunicipioStr(campos[17]);
					if(campos.length > 18) bean.setEnderecocepStr(campos[18]);
					if(campos.length > 19) bean.setDtretornoStr(campos[19]);
					if(campos.length > 20) bean.setObservacao(campos[20]);
					if(campos.length > 21) bean.setConcorrenteStr(campos[21]);
					if(campos.length > 22) bean.setProximoPasso(campos[22]);
					if(campos.length > 23) bean.setCampanhaStr(campos[23]);
					
					if(bean.getContato() == null || bean.getContato().equals("")){
						listaErro.add("O campo Contato n�o foi preenchido.");
						listaLeadErrosContato.add(bean);
						listaLeadComErro.add(bean);
					}
					
					if(bean.getSituacaoStr() == null || bean.getSituacaoStr().equals("")){
						listaErro.add("O campo Situa��o n�o foi preenchido.");
						listaLeadErrosSituacao.add(bean);
						listaLeadComErro.add(bean);
					}
					
					if(bean.getQualificacaoStr() == null || bean.getQualificacaoStr().equals("")){
						listaErro.add("O campo Qualifica��o n�o foi preenchido.");
						listaLeadErrosQualificacao.add(bean);
						listaLeadComErro.add(bean);
					}
					
					if(bean.getResponsavelCpfStr() == null || bean.getResponsavelCpfStr().equals("")){
						listaErro.add("O campo Respons�vel n�o foi preenchido.");
						listaLeadErrosResponsavel.add(bean);
						listaLeadComErro.add(bean);
					}
					
					if(listaErro.size() == 0){
						if(bean.getResponsavelCpfStr() != null && !bean.getResponsavelCpfStr().equals("")){
							String cpf = StringUtils.soNumero(bean.getResponsavelCpfStr());
							try{
								if(cpf.length() == 11){
									bean.setResponsavelCpf(new Cpf(cpf));
								} else {
									listaErro.add("CPF inv�lido.");
									listaLeadComErro.add(bean);
									listaLeadErrosResponsavel.add(bean);
								}
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o de CPF: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosResponsavel.add(bean);
							}
						}
						
						if(bean.getResponsavelCpf() != null){
							try{
								Colaborador responsavel = colaboradorService.findByCpf(bean.getResponsavelCpf());
								if(responsavel == null){
									listaErro.add("Respons�vel n�o encontrado na base.");
									listaLeadComErro.add(bean);
									listaLeadErrosResponsavel.add(bean);
								}
								bean.setResponsavel(responsavel);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o do respons�vel: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosResponsavel.add(bean);
							}
						}
						
						if(bean.getDtretornoStr() != null && !bean.getDtretornoStr().equals("")){
							try{
								Date dtretorno = SinedDateUtils.stringToSqlDate(bean.getDtretornoStr());
								if(dtretorno == null){
									listaErro.add("Data de retorno inv�lida.");
									listaLeadComErro.add(bean);
									listaLeadErrosDtretorno.add(bean);
								}
								bean.setDtretorno(dtretorno);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o da data de retorno: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosDtretorno.add(bean);
							}
						}
						
						if(bean.getCampanhaStr() != null && !bean.getCampanhaStr().equals("")){
							try{
								Campanha campanha = campanhaService.findByName(bean.getCampanhaStr());
								if (campanha == null) {
									listaErro.add("Erro na importa��o dos dados! A campanha " + bean.getCampanhaStr() + 
											" n�o est� cadastrada no sistema.<br>Favor cadastrar a campanha e importar novamente os dados!");
									listaLeadComErro.add(bean);
									listaLeadErrosCampanha.add(bean);
								}
								bean.setCampanha(campanha);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o da campanha: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosCampanha.add(bean);
							}
						}
						
						if(bean.getConcorrenteStr() != null && !bean.getConcorrenteStr().equals("")){
							try{
								Concorrente concorrente = concorrenteService.findByName(bean.getConcorrenteStr());
								if (concorrente != null) {
									bean.setConcorrente(concorrente);
								}
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o do concorrente: " + e.getMessage());
							}
						}
							
						if(bean.getSexoStr() != null && !bean.getSexoStr().equals("")){
							try{
								if(bean.getSexoStr().equalsIgnoreCase("MASCULINO")){
									bean.setSexo(new Sexo(Sexo.MASCULINO));
								} else if(bean.getSexoStr().equalsIgnoreCase("FEMININO")){
									bean.setSexo(new Sexo(Sexo.FEMININO));
								} else {
									listaErro.add("Sexo inv�lido.");
									listaLeadComErro.add(bean);
									listaLeadErrosSexo.add(bean);
								}
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o do campo sexo: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosSexo.add(bean);
							}
						}
						
						if(bean.getSituacaoStr() != null && !bean.getSituacaoStr().equals("")){
							try{
								Leadsituacao situacao = leadsituacaoService.findByNome(bean.getSituacaoStr());
								if(situacao == null){
									listaErro.add("Situa��o n�o encontrada na base.");
									listaLeadComErro.add(bean);
									listaLeadErrosSituacao.add(bean);
								} 
								bean.setSituacao(situacao);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o da situa��o: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosSituacao.add(bean);
							}
						}
						
						if(bean.getQualificacaoStr() != null && !bean.getQualificacaoStr().equals("")){
							try{
								Leadqualificacao qualificacao = leadqualificacaoService.findByNome(bean.getQualificacaoStr());
								if(qualificacao == null){
									listaErro.add("Qualifica��o n�o encontrada na base.");
									listaLeadComErro.add(bean);
									listaLeadErrosQualificacao.add(bean);
								} 
								bean.setQualificacao(qualificacao);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o da qualifica��o: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosQualificacao.add(bean);
							}
						}
						
						if(bean.getSegmentoStr() != null && !bean.getSegmentoStr().equals("")){
							try{
								Segmento segmento = segmentoService.findByNome(bean.getSegmentoStr());
								if(segmento == null){
									listaErro.add("Segmento n�o encontrado na base.");
									listaLeadComErro.add(bean);
									listaLeadErrosSegmento.add(bean);
								} 
								bean.setSegmento(segmento);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o do segmento: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosSegmento.add(bean);
							}
						}
						
						if(bean.getEnderecoufStr() != null && !bean.getEnderecoufStr().equals("")){
							Uf uf = ufService.findBySigla(bean.getEnderecoufStr().toUpperCase());
							if(uf != null){
								bean.setEnderecouf(uf);
								
								if(bean.getEnderecomunicipioStr() != null && !bean.getEnderecomunicipioStr().equals("")){
									Municipio municipio = municipioService.findByNomeSigla(bean.getEnderecomunicipioStr(), bean.getEnderecoufStr().toUpperCase());
									if(municipio == null){
										listaErro.add("Munic�pio n�o encontrado no sistema.");
										listaLeadComErro.add(bean);
										listaLeadErrosMunicipio.add(bean);
									}
									bean.setEnderecomunicipio(municipio);
								}
							} else {
								listaErro.add("UF n�o encontrada no sistema.");
								listaLeadComErro.add(bean);
								listaLeadErrosUf.add(bean);
							}
						}

						if(bean.getEnderecocepStr() != null && !bean.getEnderecocepStr().equals("")){
							try{
								Cep cep = new Cep(bean.getEnderecocepStr());
								bean.setEnderecocep(cep);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na convers�o de CEP: " + e.getMessage());
								listaLeadComErro.add(bean);
								listaLeadErrosCep.add(bean);
							}
						}
						
						if (bean.getProximoPasso() != null && bean.getProximoPasso().length() > 500) {
							listaErro.add("O campo Pr�ximo Passo pode ter at� 500 caracteres.");
							listaLeadErrosProximoPasso.add(bean);
							listaLeadComErro.add(bean);
						}
					}
				} catch (Exception e) {
					listaErro.add(e.getMessage());
					e.printStackTrace();
				}
				
				if(listaErro.size() > 0){
					bean.setErro("<ul><li>" + CollectionsUtil.concatenate(listaErro, "</li><li>") + "</li></ul>");
					bean.setMarcado(Boolean.FALSE);
				}
				
				if(listaAvisos.size() > 0){
					bean.setAvisos("<ul><li>" + CollectionsUtil.concatenate(listaAvisos, "</li><li>") + "</li></ul>");
				}
				
				listaLeadBean.add(bean);
			}
		}
		
		filtro.setListaLeadBean(listaLeadBean);
		filtro.setListaLeadComErro(listaLeadComErro);
		
		filtro.setListaLeadErrosContato(listaLeadErrosContato);
		filtro.setListaLeadErrosSituacao(listaLeadErrosSituacao);
		filtro.setListaLeadErrosQualificacao(listaLeadErrosQualificacao);
		filtro.setListaLeadErrosResponsavel(listaLeadErrosResponsavel);
		filtro.setListaLeadErrosMunicipio(listaLeadErrosMunicipio);
		filtro.setListaLeadErrosUf(listaLeadErrosUf);
		filtro.setListaLeadErrosCep(listaLeadErrosCep);
		filtro.setListaLeadErrosSegmento(listaLeadErrosSegmento);
		filtro.setListaLeadErrosSexo(listaLeadErrosSexo);
		filtro.setListaLeadErrosDtretorno(listaLeadErrosDtretorno);
		filtro.setListaLeadErrosCampanha(listaLeadErrosCampanha);
		filtro.setListaLeadErrosProximoPasso(listaLeadErrosProximoPasso);
		
		request.setAttribute("qtdErrosContato", listaLeadErrosContato.size());
		request.setAttribute("qtdErrosSituacao", listaLeadErrosSituacao.size());
		request.setAttribute("qtdErrosQualificacao", listaLeadErrosQualificacao.size());
		request.setAttribute("qtdErrosMunicipio", listaLeadErrosMunicipio.size());
		request.setAttribute("qtdErrosCep", listaLeadErrosCep.size());
		request.setAttribute("qtdErrosUf", listaLeadErrosUf.size());
		request.setAttribute("qtdErrosResponsavel", listaLeadErrosResponsavel.size());
		request.setAttribute("qtdErrosSegmento", listaLeadErrosSegmento.size());
		request.setAttribute("qtdErrosSexo", listaLeadErrosSexo.size());
		request.setAttribute("qtdErrosDtretorno", listaLeadErrosDtretorno.size());
		request.setAttribute("qtdErrosCampanha", listaLeadErrosCampanha.size());
		request.setAttribute("qtdErrosProximoPasso", listaLeadErrosProximoPasso.size());
		
		return new ModelAndView("process/importacaoDadosBasicosConfirmacaoLead", "filtro", filtro);
	}
	
	private ModelAndView makeConferenciaDadosPessoa(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro, List<String> linhasPreenchidas, ImportacaoDadosBasicosTipo tipo) {
		List<ImportacaoDadosPessoaBean> listaPessoaBean = new ArrayList<ImportacaoDadosPessoaBean>();
				
		ListSet<ImportacaoDadosPessoaBean> listaPessoasComErro = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		
		ListSet<ImportacaoDadosPessoaBean> listaErrosNome = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosCpfCnpj = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosClienteFornecedor = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosMunicipio = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosUf = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosCep = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosCategoria = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosDataNascimento = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosEspecialidade = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		ListSet<ImportacaoDadosPessoaBean> listaErrosRegistro = new ListSet<ImportacaoDadosPessoaBean>(ImportacaoDadosPessoaBean.class);
		
		for (int i = 0; i < linhasPreenchidas.size(); i++) {
			String linha = linhasPreenchidas.get(i);
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				String[] campos = linha.split(";");
				
				ImportacaoDadosPessoaBean bean = new ImportacaoDadosPessoaBean();
				List<String> listaErro = new ArrayList<String>();
				List<String> listaAvisos = new ArrayList<String>();
				
				try{
					bean.setLinha(i+1);
					
					if(campos.length > 0) bean.setIdentificador(campos[0]);
					if(campos.length > 1) bean.setNome(campos[1]);
					if(campos.length > 2) bean.setRazaosocial(campos[2]);
					if(campos.length > 3) bean.setCpfcnpj(campos[3]);
					if(campos.length > 4) bean.setInscricaoestadual(campos[4]);
					if(campos.length > 5) bean.setInscricaomunicipal(campos[5]);
					if(campos.length > 6) bean.setEmail(campos[6]);
					if(campos.length > 7) bean.setTelefoneprincipal(campos[7]);
					if(campos.length > 8) bean.setTelefonecelular(campos[8]);
					if(campos.length > 9) bean.setTelefonefax(campos[9]);
					if(campos.length > 10) bean.setContatonome(campos[10]);
					if(campos.length > 11) bean.setContatoemail(campos[11]);
					if(campos.length > 12) bean.setEnderecologradouro(campos[12]);
					if(campos.length > 13) bean.setEndereconumero(campos[13]);
					if(campos.length > 14) bean.setEnderecocomplemento(campos[14]);
					if(campos.length > 15) bean.setEnderecobairro(campos[15]);
					if(campos.length > 16) bean.setEnderecoufStr(campos[16]);
					if(campos.length > 17) bean.setEnderecomunicipioStr(campos[17]);
					if(campos.length > 18) bean.setEnderecocepStr(campos[18]);
					if(campos.length > 19) bean.setCategoriaStr(campos[19]);
					if(campos.length > 20) bean.setObservacao(campos[20]);
					if(campos.length > 21) bean.setDataNascimentoStr(campos[21]);
					if(campos.length > 22) bean.setSexoStr(campos[22]);
					if(campos.length > 23) bean.setEspecialidadeStr1(campos[23]);
					if(campos.length > 24) bean.setRegistro1(campos[24]);
					if(campos.length > 25) bean.setEspecialidadeStr2(campos[25]);
					if(campos.length > 26) bean.setRegistro2(campos[26]);
					if(campos.length > 27) bean.setEspecialidadeStr3(campos[27]);
					if(campos.length > 28) bean.setRegistro3(campos[28]);
					if(campos.length > 29) bean.setEspecialidadeStr4(campos[29]);
					if(campos.length > 30) bean.setRegistro4(campos[30]);
					if(campos.length > 31) bean.setEspecialidadeStr5(campos[31]);
					if(campos.length > 32) bean.setRegistro5(campos[32]);
					if(campos.length > 33) bean.setAssociacao(campos[33]);
					
					if(bean.getNome() == null || bean.getNome().equals("")){
						listaErro.add("O campo Nome n�o foi preenchido.");
						listaErrosNome.add(bean);
						listaPessoasComErro.add(bean);
					}
					
					if((bean.getContatonome() == null || bean.getContatonome().equals("")) 
							&& bean.getContatoemail() != null && !bean.getContatoemail().equals("")){
						listaAvisos.add("Para a cria��o do contato � necess�rio o preenchimento do nome do contato.");
					}
					
					if(bean.getCpfcnpj() != null && !bean.getCpfcnpj().equals("")){
						String cpfcnpj = StringUtils.soNumero(bean.getCpfcnpj());
						
						try{
							if(cpfcnpj.length() == 11){
								bean.setCpf(new Cpf(cpfcnpj));
							} else if(cpfcnpj.length() == 14){
								bean.setCnpj(new Cnpj(cpfcnpj));
							} else {
								listaErro.add("CPF/CNPJ inv�lido.");
								listaPessoasComErro.add(bean);
								listaErrosCpfCnpj.add(bean);
							}
						} catch (Exception e) {
							e.printStackTrace();
							listaErro.add("Erro na convers�o de CPF/CNPJ: " + e.getMessage());
							listaPessoasComErro.add(bean);
							listaErrosCpfCnpj.add(bean);
							}
					}
					
					if(bean.getCpf() == null) bean.setSexoStr(null);
					
					if(listaErro.size() == 0){
						if(bean.getCpf() != null){
							Pessoa pessoa = pessoaService.findPessoaByCpf(bean.getCpf());
							if(pessoa != null){
								listaErro.add("CPF j� existe na base de dados.");
								listaPessoasComErro.add(bean);
								listaErrosCpfCnpj.add(bean);
							}
						} else if(bean.getCnpj() != null){
							Pessoa pessoa = pessoaService.findPessoaByCnpj(bean.getCnpj());
							if(pessoa != null){
								listaErro.add("CNPJ j� existe na base de dados.");
								listaPessoasComErro.add(bean);
								listaErrosCpfCnpj.add(bean);
							}
						} else {
							if(tipo.equals(ImportacaoDadosBasicosTipo.CLIENTE) &&
									clienteService.haveClienteIdentificadorNome(bean.getIdentificador(), bean.getNome())){
								listaErro.add("Cliente j� existe na base de dados.");
								listaPessoasComErro.add(bean);
								listaErrosClienteFornecedor.add(bean);
							} else if(tipo.equals(ImportacaoDadosBasicosTipo.FORNECEDOR) &&
									fornecedorService.haveFornecedorIdentificadorNome(bean.getIdentificador(), bean.getNome())){
								listaErro.add("Fornecedor j� existe na base de dados.");
								listaPessoasComErro.add(bean);
								listaErrosClienteFornecedor.add(bean);
							}
						}
					}
					
					if(bean.getEnderecoufStr() != null && !bean.getEnderecoufStr().equals("")){
						Uf uf = ufService.findBySigla(bean.getEnderecoufStr().toUpperCase());
						if(uf != null){
							bean.setEnderecouf(uf);
							
							if(bean.getEnderecomunicipioStr() != null && !bean.getEnderecomunicipioStr().equals("")){
								Municipio municipio = municipioService.findByNomeSigla(bean.getEnderecomunicipioStr(), bean.getEnderecoufStr().toUpperCase());
								if(municipio == null){
									listaErro.add("Munic�pio n�o encontrado no sistema.");
									listaPessoasComErro.add(bean);
									listaErrosMunicipio.add(bean);
								}
								bean.setEnderecomunicipio(municipio);
							}
						} else {
							listaErro.add("UF n�o encontrada no sistema.");
							listaPessoasComErro.add(bean);
							listaErrosUf.add(bean);
						}
					}

					if(bean.getEnderecocepStr() != null && !bean.getEnderecocepStr().equals("")){
						try{
							Cep cep = new Cep(bean.getEnderecocepStr());
							bean.setEnderecocep(cep);
						} catch (Exception e) {
							e.printStackTrace();
							listaErro.add("Erro na convers�o de CEP: " + e.getMessage());
							listaPessoasComErro.add(bean);
							listaErrosCep.add(bean);
						}
					}
					
					if(bean.getCategoriaStr() != null && !bean.getCategoriaStr().equals("")){
						try{
							Categoria categoria = categoriaService.findByNome(bean.getCategoriaStr());
							if(categoria == null){
								listaAvisos.add("Categoria '" + bean.getCategoriaStr() + "' n�o encontrada no sistema, ser� criada uma categoria nova.");
							}
							bean.setCategoria(categoria);
						} catch (Exception e) {
							e.printStackTrace();
							listaErro.add("Erro na convers�o da categoria: " + e.getMessage());
							listaPessoasComErro.add(bean);
							listaErrosCategoria.add(bean);
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getDataNascimentoStr())){
						try{
							bean.setDtnascimento(SinedDateUtils.stringToDate(bean.getDataNascimentoStr(), "dd/MM/yyyy"));
						} catch (Exception e) {
							e.printStackTrace();
							listaErro.add("Erro na convers�o da Data de Nascimento: " + e.getMessage());
							listaPessoasComErro.add(bean);
							listaErrosDataNascimento.add(bean);
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getSexoStr())){
						try{
							Sexo sexo = null;
							if(bean.getSexoStr().equalsIgnoreCase("MASCULINO")) sexo = new Sexo(Sexo.MASCULINO);
							if(bean.getSexoStr().equalsIgnoreCase("FEMININO")) sexo = new Sexo(Sexo.FEMININO);
							bean.setSexo(sexo);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getAssociacao())){
						try {
							bean.setFornecedorassociacao(fornecedorService.loadFornecedorAssociacao(new Fornecedor(Integer.parseInt(bean.getAssociacao()))));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					boolean erroEspecialidade = false;
					boolean erroRegistro = false;
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getEspecialidadeStr1())){
						try{
							Especialidade especialidade = especialidadeService.findByDescricao(bean.getEspecialidadeStr1());
							if(especialidade == null){
								listaErro.add("Especialidade (1) n�o encontrada no sistema.");
								if(!erroEspecialidade){
									listaPessoasComErro.add(bean);
									erroEspecialidade = true;
									listaErrosEspecialidade.add(bean);
								}
							}else {
								bean.setEspecialidade1(especialidade);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getRegistro1())){
						listaErro.add("Registro 1 preenchido e a especialidade (1) n�o informada no arquivo.");
						if(!erroRegistro){
							listaPessoasComErro.add(bean);
							erroRegistro = true;
							listaErrosRegistro.add(bean);
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getEspecialidadeStr2())){
						try{
							Especialidade especialidade = especialidadeService.findByDescricao(bean.getEspecialidadeStr2());
							if(especialidade == null){
								listaErro.add("Especialidade (2) n�o encontrada no sistema.");
								if(!erroEspecialidade){
									listaPessoasComErro.add(bean);
									erroEspecialidade = true;
									listaErrosEspecialidade.add(bean);
								}
							}else {
								bean.setEspecialidade2(especialidade);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getRegistro2())){
						listaErro.add("Registro 2 preenchido e a especialidade (2) n�o informada no arquivo.");
						if(!erroRegistro){
							listaPessoasComErro.add(bean);
							erroRegistro = true;
							listaErrosRegistro.add(bean);
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getEspecialidadeStr3())){
						try{
							Especialidade especialidade = especialidadeService.findByDescricao(bean.getEspecialidadeStr3());
							if(especialidade == null){
								listaErro.add("Especialidade (3) n�o encontrada no sistema.");
								if(!erroEspecialidade){
									listaPessoasComErro.add(bean);
									erroEspecialidade = true;
									listaErrosEspecialidade.add(bean);
								}
							}else {
								bean.setEspecialidade3(especialidade);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getRegistro3())){
						listaErro.add("Registro 3 preenchido e a especialidade (3) n�o informada no arquivo.");
						if(!erroRegistro){
							listaPessoasComErro.add(bean);
							erroRegistro = true;
							listaErrosRegistro.add(bean);
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getEspecialidadeStr4())){
						try{
							Especialidade especialidade = especialidadeService.findByDescricao(bean.getEspecialidadeStr4());
							if(especialidade == null){
								listaErro.add("Especialidade (4) n�o encontrada no sistema.");
								if(!erroEspecialidade){
									listaPessoasComErro.add(bean);
									erroEspecialidade = true;
									listaErrosEspecialidade.add(bean);
								}
							}else {
								bean.setEspecialidade4(especialidade);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getRegistro4())){
						listaErro.add("Registro 4 preenchido e a especialidade (4) n�o informada no arquivo.");
						if(!erroRegistro){
							listaPessoasComErro.add(bean);
							erroRegistro = true;
							listaErrosRegistro.add(bean);
						}
					}
					
					if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getEspecialidadeStr5())){
						try{
							Especialidade especialidade = especialidadeService.findByDescricao(bean.getEspecialidadeStr5());
							if(especialidade == null){
								listaErro.add("Especialidade (5) n�o encontrada no sistema.");
								if(!erroEspecialidade){
									listaPessoasComErro.add(bean);
									erroEspecialidade = true;
									listaErrosEspecialidade.add(bean);
								}
							}else {
								bean.setEspecialidade5(especialidade);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else if(org.apache.commons.lang.StringUtils.isNotBlank(bean.getRegistro5())){
						listaErro.add("Registro 5 preenchido e a especialidade (5) n�o informada no arquivo.");
						if(!erroRegistro){
							listaPessoasComErro.add(bean);
							erroRegistro = true;
							listaErrosRegistro.add(bean);
						}
					}
					
				} catch (Exception e) {
					listaErro.add(e.getMessage());
					e.printStackTrace();
				}
				
				if(listaErro.size() > 0){
					bean.setErro("<ul><li>" + CollectionsUtil.concatenate(listaErro, "</li><li>") + "</li></ul>");
					bean.setMarcado(Boolean.FALSE);
				}
				
				if(listaAvisos.size() > 0){
					bean.setAvisos("<ul><li>" + CollectionsUtil.concatenate(listaAvisos, "</li><li>") + "</li></ul>");
				}
				
				listaPessoaBean.add(bean);
			}
		}
		
		filtro.setListaPessoaBean(listaPessoaBean);
		filtro.setListaPessoaBeanComErro(listaPessoasComErro);
		filtro.setListaErrosNome(listaErrosNome);
		filtro.setListaErrosCpfCnpj(listaErrosCpfCnpj);
		filtro.setListaErrosClienteFornecedor(listaErrosClienteFornecedor);
		filtro.setListaErrosMunicipio(listaErrosMunicipio);
		filtro.setListaErrosCep(listaErrosCep);
		filtro.setListaErrosCategoria(listaErrosCategoria);
		filtro.setListaErrosDataNascimento(listaErrosDataNascimento);
		filtro.setListaErrosEspecialidade(listaErrosEspecialidade);
		filtro.setListaErrosRegistro(listaErrosRegistro);
		
		request.setAttribute("qtdErrosNome", listaErrosNome.size());
		request.setAttribute("qtdErrosCpfCnpj", listaErrosCpfCnpj.size());
		request.setAttribute("qtdErrosClienteFornecedor", listaErrosClienteFornecedor.size());
		request.setAttribute("qtdErrosMunicipio", listaErrosMunicipio.size());
		request.setAttribute("qtdErrosCep", listaErrosCep.size());
		request.setAttribute("qtdErrosUf", listaErrosUf.size());
		request.setAttribute("qtdErrosCategoria", listaErrosCategoria.size());
		request.setAttribute("qtdErrosDataNascimento", listaErrosDataNascimento.size());
		request.setAttribute("qtdErrosEspecialidade", listaErrosEspecialidade.size());
		request.setAttribute("qtdErrosRegistro", listaErrosRegistro.size());
		
		return new ModelAndView("process/importacaoDadosBasicosConfirmacaoPessoa", "filtro", filtro);
	}
	
	private ModelAndView makeConferenciaDadosMaterial(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro, List<String> linhasPreenchidas) {
		List<ImportacaoDadosMaterialBean> listaMaterialBean = new ArrayList<ImportacaoDadosMaterialBean>();
		
		ListSet<ImportacaoDadosMaterialBean> listaMateriaisComErro = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosNomeMaterial = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosGrupoMaterial = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosUnidadeMedida = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosClasse = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosContaGerencial = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosCentroCusto = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosValorCusto = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		ListSet<ImportacaoDadosMaterialBean> listaErrosValorVenda = new ListSet<ImportacaoDadosMaterialBean>(ImportacaoDadosMaterialBean.class);
		
		for (int i = 0; i < linhasPreenchidas.size(); i++) {
			String linha = linhasPreenchidas.get(i);
			if(linha != null && !linha.replaceAll(";", "").trim().equals("")){
				String[] campos = linha.split(";");
				
				ImportacaoDadosMaterialBean bean = new ImportacaoDadosMaterialBean();
				List<String> listaErro = new ArrayList<String>();
				List<String> listaAvisos = new ArrayList<String>();
				
				try{
					bean.setLinha(i+1);
					
					if(campos.length > 0) bean.setIdentificador(campos[0]);
					if(campos.length > 1) bean.setReferencia(campos[1]);
					if(campos.length > 2) bean.setNome(campos[2]);
					if(campos.length > 3) bean.setNomenf(campos[3]);
					if(campos.length > 4) bean.setMaterialclasseStr(campos[4]);
					if(campos.length > 5) bean.setUnidademedidaStr(campos[5]);
					if(campos.length > 6) bean.setMaterialgrupoStr(campos[6]);
					if(campos.length > 7) bean.setMaterialtipoStr(campos[7]);
					if(campos.length > 8) bean.setContagerencialcompraStr(campos[8]);
					if(campos.length > 9) bean.setCentrocustovendaStr(campos[9]);
					if(campos.length > 10) bean.setContagerencialvendaStr(campos[10]);
					if(campos.length > 11) bean.setValorcustoStr(campos[11]);
					if(campos.length > 12) bean.setValorvendaStr(campos[12]);
					if(campos.length > 13) bean.setNcm(campos[13]);
					
					if(bean.getNome() == null || bean.getNome().equals("")){
						listaErro.add("O campo Nome n�o foi preenchido.");
						listaErrosNomeMaterial.add(bean);
						listaMateriaisComErro.add(bean);
					} else {
						if(materialService.haveMaterialNomeIdentificacao(bean.getIdentificador(), bean.getNome())){
							listaErro.add("Material j� existe na base de dados.");
							listaErrosNomeMaterial.add(bean);
							listaMateriaisComErro.add(bean);
						} else if(materialService.haveMaterialNomeIdentificacao(null, bean.getNome())){
							listaErro.add("Material j� existe na base de dados.");
							listaErrosNomeMaterial.add(bean);
							listaMateriaisComErro.add(bean);
						}
					}
					
					if(bean.getNomenf() != null && bean.getNomenf().length() > 100){
						bean.setNomenf(bean.getNomenf().substring(0, 100));
					}
					
					if(bean.getMaterialgrupoStr() == null || bean.getMaterialgrupoStr().equals("")){
						listaErro.add("O campo Grupo do material n�o foi preenchido.");
						listaErrosGrupoMaterial.add(bean);
					} else {
						String materialgrupoStr = Util.strings.tiraAcento(bean.getMaterialgrupoStr().trim().toUpperCase());
						Materialgrupo materialgrupo = null;
						try{
							materialgrupo = materialgrupoService.findByNomeForImportacao(materialgrupoStr);
							bean.setMaterialgrupo(materialgrupo);
							if(materialgrupo == null){
								listaAvisos.add("Grupo de material '" + bean.getMaterialgrupoStr() + "' n�o encontrado, ser� criado um Grupo novo.");
							}
						} catch (Exception e) {
							listaErro.add(e.getMessage());
							e.printStackTrace();
						}
					}
					
					if(bean.getUnidademedidaStr() == null || bean.getUnidademedidaStr().equals("")){
						listaErro.add("O campo Unidade de medida n�o foi preenchido.");
						listaErrosUnidadeMedida.add(bean);
						listaMateriaisComErro.add(bean);
					}  else {
						String unidademedidadeStr = bean.getUnidademedidaStr();
						Unidademedida unidademedida = null;
						try{
							unidademedida = unidademedidaService.findBySimbolo(unidademedidadeStr.trim());
							bean.setUnidademedida(unidademedida);
							if(unidademedida == null){
								listaAvisos.add("Unidade de medida '" + bean.getUnidademedidaStr() + "' n�o encontrada, ser� criado uma Unidade de medida nova.");
							}
						} catch (Exception e) {
							listaErro.add(e.getMessage());
							e.printStackTrace();
						}
					}
					
					if(bean.getMaterialclasseStr() == null || bean.getMaterialclasseStr().equals("")){
						listaErro.add("O campo Classe n�o foi preenchido.");
						listaErrosClasse.add(bean);
						listaMateriaisComErro.add(bean);
					} else {
						String materialclasseStr = Util.strings.tiraAcento(bean.getMaterialclasseStr().trim().toUpperCase());
						
						Materialclasse materialclasse = null;
						if(materialclasseStr.equals("PRODUTO")){
							materialclasse = Materialclasse.PRODUTO;
						} else if(materialclasseStr.equals("PATRIMONIO")){
							materialclasse = Materialclasse.PATRIMONIO;
						} else if(materialclasseStr.equals("EPI")){
							materialclasse = Materialclasse.EPI;
						} else if(materialclasseStr.equals("SERVICO")){
							materialclasse = Materialclasse.SERVICO;
						} else {
							listaErro.add("N�o foi poss�vel encontrar a classe '" + bean.getMaterialclasseStr() + "' do material.");
							listaErrosClasse.add(bean);
							listaMateriaisComErro.add(bean);
						}
						bean.setMaterialclasse(materialclasse);
					}
					
					if(bean.getContagerencialcompraStr() == null || bean.getContagerencialcompraStr().equals("")){
						listaErro.add("O campo Conta gerencial (Compra) n�o foi preenchido.");
						listaErrosContaGerencial.add(bean);
						listaMateriaisComErro.add(bean);
					} else {
						String contagerencialcompraStr = bean.getContagerencialcompraStr().trim();
						
						Contagerencial contagerencial = null;
						try{
							Integer cdcontagerencial = Integer.parseInt(contagerencialcompraStr);
							contagerencial = contagerencialService.load(new Contagerencial(cdcontagerencial));
							bean.setContagerencialcompra(contagerencial);
							if(contagerencial == null){
								listaErro.add("Conta gerencial (Compra) n�o encontrada no sistema.");
								listaErrosContaGerencial.add(bean);
								listaMateriaisComErro.add(bean);
							}
						} catch (Exception e) {
							listaErro.add("Erro na convers�o da Conta gerencial (Compra): " + e.getMessage());
							listaErrosContaGerencial.add(bean);
							listaMateriaisComErro.add(bean);
							e.printStackTrace();
						}
					}
					
					if(bean.getMaterialtipoStr() != null && !bean.getMaterialtipoStr().equals("")){
						String materialtipoStr = Util.strings.tiraAcento(bean.getMaterialtipoStr().trim().toUpperCase());
						Materialtipo materialtipo = null;
						try{
							materialtipo = materialtipoService.findByNomeForImportacao(materialtipoStr);
							bean.setMaterialtipo(materialtipo);
							if(materialtipo == null){
								listaAvisos.add("Tipo de material '" + bean.getMaterialtipoStr() + "' n�o encontrado, ser� criado um Tipo novo.");
							}
						} catch (Exception e) {
							listaErro.add(e.getMessage());
							e.printStackTrace();
						}
					}
					
					if(bean.getCentrocustovendaStr() != null && !bean.getCentrocustovendaStr().equals("")){
						String centrocustocompraStr = bean.getCentrocustovendaStr().trim();
						
						Centrocusto centrocusto = null;
						try{
							Integer cdcentrocusto = Integer.parseInt(centrocustocompraStr);
							centrocusto = centrocustoService.load(new Centrocusto(cdcentrocusto));
							bean.setCentrocustovenda(centrocusto);
							if(centrocusto == null){
								listaErro.add("Centro de custo (Venda) n�o encontrado no sistema.");
								listaErrosCentroCusto.add(bean);
								listaMateriaisComErro.add(bean);
							}
						} catch (Exception e) {
							listaErro.add("Erro na convers�o de Centro de custo (Venda): " + e.getMessage());
							listaErrosCentroCusto.add(bean);
							listaMateriaisComErro.add(bean);
							e.printStackTrace();
						}
						
					}
					
					if(bean.getContagerencialvendaStr() != null && !bean.getContagerencialvendaStr().equals("")){
						String contagerencialvendaStr = bean.getContagerencialvendaStr().trim();
						
						Contagerencial contagerencial = null;
						try{
							Integer cdcontagerencial = Integer.parseInt(contagerencialvendaStr);
							contagerencial = contagerencialService.load(new Contagerencial(cdcontagerencial));
							bean.setContagerencialvenda(contagerencial);
							if(contagerencial == null){
								listaErro.add("Conta gerencial (Venda) n�o encontrada no sistema.");
								listaErrosContaGerencial.add(bean);
								listaMateriaisComErro.add(bean);
							}
						} catch (Exception e) {
							listaErro.add("Erro na convers�o da Conta gerencial (Venda): " + e.getMessage());
							listaErrosContaGerencial.add(bean);
							listaMateriaisComErro.add(bean);
							e.printStackTrace();
						}
					}
					
					if(bean.getValorcustoStr() != null && !bean.getValorcustoStr().equals("")){
						try{
							Double valorcusto = Double.valueOf(bean.getValorcustoStr().replace(",", "."));
							bean.setValorcusto(valorcusto);
						} catch (Exception e) {
							listaErro.add("Erro na convers�o do Valor custo.");
							listaErrosValorCusto.add(bean);
							listaMateriaisComErro.add(bean);
						}
					}
					
					if(bean.getValorvendaStr() != null && !bean.getValorvendaStr().equals("")){
						try{
							Double valorvenda = Double.valueOf(bean.getValorvendaStr().replace(",", "."));
							bean.setValorvenda(valorvenda);
						} catch (Exception e) {
							listaErro.add("Erro na convers�o do Valor venda.");
							listaErrosValorVenda.add(bean);
							listaMateriaisComErro.add(bean);
						}
					}
				} catch (Exception e) {
					listaErro.add(e.getMessage());
					e.printStackTrace();
				}
				
				if(listaErro.size() > 0){
					bean.setErro("<ul><li>" + CollectionsUtil.concatenate(listaErro, "</li><li>") + "</li></ul>");
					bean.setMarcado(Boolean.FALSE);
				}
				
				if(listaAvisos.size() > 0){
					bean.setAvisos("<ul><li>" + CollectionsUtil.concatenate(listaAvisos, "</li><li>") + "</li></ul>");
				}
				
				listaMaterialBean.add(bean);
			}
		}
		
		filtro.setListaMaterialBean(listaMaterialBean);
		
		filtro.setListaErrosNomeMaterial(listaErrosNomeMaterial);
		filtro.setListaErrosUnidadeMedida(listaErrosUnidadeMedida);
		filtro.setListaErrosGrupoMaterial(listaErrosGrupoMaterial);
		filtro.setListaErrosClasse(listaErrosClasse);
		filtro.setListaErrosContaGerencial(listaErrosContaGerencial);
		filtro.setListaErrosCentroCusto(listaErrosCentroCusto);
		filtro.setListaErrosValorCusto(listaErrosValorCusto);
		filtro.setListaErrosValorVenda(listaErrosValorVenda);
		
		filtro.setListaMateriaisComErro(listaMateriaisComErro);
		
		request.setAttribute("qtdErrosNome", listaErrosNomeMaterial.size());
		request.setAttribute("qtdErrosUnidadeMedida", listaErrosUnidadeMedida.size());
		request.setAttribute("qtdErrosGrupoMaterial", listaErrosGrupoMaterial.size());
		request.setAttribute("qtdErrosClasse", listaErrosClasse.size());
		request.setAttribute("qtdErrosContaGerencial", listaErrosContaGerencial.size());
		request.setAttribute("qtdErrosCentroCusto", listaErrosCentroCusto.size());
		request.setAttribute("qtdErrosValorCusto", listaErrosValorCusto.size());
		request.setAttribute("qtdErrosValorVenda", listaErrosValorVenda.size());
		
		return new ModelAndView("process/importacaoDadosBasicosConfirmacaoMaterial", "filtro", filtro);
	}
	
	@Input("index")
	@OnErrors("index")
	@Command(session=false)
	public ModelAndView salvaConfirmacaoDadosLead(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro){
		try{
			List<ImportacaoDadosLeadBean> listaLeadBean = filtro.getListaLeadBean();
			if(listaLeadBean != null){
				
				for (int i=0; i < listaLeadBean.size(); i++) {
					ImportacaoDadosLeadBean bean = listaLeadBean.get(i);
					
					/**
					 * Importante, para evitar que a importa��o n�o trave o servidor de aplica��o
					 */
					if(i%100==0){
						Thread.sleep(10000);
					}
					
					if(bean.getMarcado() != null && bean.getMarcado()){
						List<String> listaErro = new ArrayList<String>();
						
						Lead lead = new Lead();
						
						try{
							lead.setNome(bean.getContato());
							lead.setSexo(bean.getSexo());
							lead.setEmpresa(bean.getEmpresa());
							lead.setEmail(bean.getEmail());
							lead.setWebsite(bean.getWebsite());
							lead.setTiporesponsavel(Tiporesponsavel.COLABORADOR);
							lead.setResponsavel(bean.getResponsavel());
							lead.setLeadsituacao(bean.getSituacao());
							lead.setLeadqualificacao(bean.getQualificacao());
							lead.setLogradouro(bean.getEnderecologradouro());
							lead.setNumero(bean.getEndereconumero());
							lead.setComplemento(bean.getEnderecocomplemento());
							lead.setBairro(bean.getEnderecobairro());
							lead.setUf(bean.getEnderecouf());
							lead.setMunicipio(bean.getEnderecomunicipio());
							lead.setCep(bean.getEnderecocep());
							lead.setDtretorno(bean.getDtretorno());
							lead.setObservacao(bean.getObservacao());
							
							if (bean.getConcorrente() == null && bean.getConcorrenteStr() != null && !bean.getConcorrenteStr().equals("")) {
								Concorrente concorrente = new Concorrente();
								concorrente.setNome(bean.getConcorrenteStr());
								concorrenteService.saveOrUpdate(concorrente);
								bean.setConcorrente(concorrente);
							}
							
							lead.setConcorrente(bean.getConcorrente());
							
							lead.setProximopasso(bean.getProximoPasso());
							
							if (bean.getCampanha() != null) {
								Campanhalead campanhalead = new Campanhalead();
								campanhalead.setCampanha(bean.getCampanha());
								campanhalead.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
								campanhalead.setDtaltera(SinedDateUtils.currentTimestamp());
								
								Set<Campanhalead> camapanhaLeadList = new HashSet<Campanhalead>();
								camapanhaLeadList.add(campanhalead);
								lead.setListLeadcampanha(camapanhaLeadList);
							}
							
							leadService.saveOrUpdate(lead);
							
							bean.setCdlead(lead.getCdlead());
						} catch (Exception e) {
							e.printStackTrace();
							listaErro.add("Erro na cria��o do lead: " + e.getMessage());
						}
						
						if(listaErro.size() == 0){
							try{
								Leadhistorico leadhistorico = new Leadhistorico();
								leadhistorico.setLead(lead);
								leadhistorico.setObservacao("Registro importado via planilha.");
								leadhistoricoService.saveOrUpdate(leadhistorico);
								
								if(bean.getObservacao() != null && !bean.getObservacao().equals("")){
									Leadhistorico leadObservacaoHistorico = new Leadhistorico();
									leadObservacaoHistorico.setLead(lead);
									leadObservacaoHistorico.setObservacao(bean.getObservacao());
									leadhistoricoService.saveOrUpdate(leadObservacaoHistorico);
								}
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na cria��o do hist�rico: " + e.getMessage());
							}
							
							if(Util.strings.isNotEmpty(bean.getTelefonePrincipal())){
								try{
									this.createTelefoneLead(bean.getTelefonePrincipal(), lead, Telefonetipo.PRINCIPAL);
								} catch (Exception e) {
									e.printStackTrace();
									listaErro.add("Erro na cria��o do telefone principal: " + e.getMessage());
								}
							}
							
							if(Util.strings.isNotEmpty(bean.getTelefoneCelular())){
								try{
									this.createTelefoneLead(bean.getTelefoneCelular(), lead, Telefonetipo.CELULAR);
								} catch (Exception e) {
									e.printStackTrace();
									listaErro.add("Erro na cria��o do telefone celular: " + e.getMessage());
								}
							}
							
							if(Util.strings.isNotEmpty(bean.getTelefoneComercial())){
								try{
									this.createTelefoneLead(bean.getTelefoneComercial(), lead, Telefonetipo.COMERCIAL);
								} catch (Exception e) {
									e.printStackTrace();
									listaErro.add("Erro na cria��o do telefone comercial: " + e.getMessage());
								}
							}
							
							if(bean.getSegmento() != null){
								try{
									Leadsegmento leadsegmento = new Leadsegmento();
									leadsegmento.setSegmento(bean.getSegmento());
									leadsegmento.setLead(lead);
									
									leadsegmentoService.saveOrUpdate(leadsegmento);
								} catch (Exception e) {
									e.printStackTrace();
									listaErro.add("Erro na cria��o do segmento: " + e.getMessage());
								}
							}
						}
						bean.setErro(CollectionsUtil.concatenate(listaErro, "\n"));
					}
				}
				
				request.addMessage("Importa��o feita com sucesso.");
				
				for (ImportacaoDadosLeadBean bean : listaLeadBean) {
					String erros = bean.getErro();
					String avisos = bean.getAvisos();
					
					
					if(avisos != null){
						avisos = avisos.replaceAll("<ul>", "");
						avisos = avisos.replaceAll("</ul>", "");
						avisos = avisos.replaceAll("<li>", "");
						avisos = avisos.replaceAll("</li>", "\n");
					}
					
					if(erros != null){
						erros = erros.replaceAll("<ul>", "");
						erros = erros.replaceAll("</ul>", "");
						erros = erros.replaceAll("<li>", ""); 
						erros = erros.replaceAll("</li>", "\n");
					}
					
					bean.setErro(erros);
					bean.setAvisos(avisos);
				}
				
				Importacaoresultado importacaoresultado = this.makeResultadoImportacaoLead(listaLeadBean, filtro.getChaveArquivo(), request);
				
				request.addMessage("Para imprimir o resultado do processamento <a href='" + request.getServletRequest().getContextPath() + "/sistema/crud/Importacaoresultado?ACAO=imprimeRelatorioResultado&cdimportacaoresultado=" + importacaoresultado.getCdimportacaoresultado() + "'>clique aqui</a>.");
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println(
						"<html><body>" +
						"<script>" +
							"window.open('" + request.getServletRequest().getContextPath() + "/sistema/crud/Importacaoresultado?ACAO=imprimeRelatorioResultado&cdimportacaoresultado=" + importacaoresultado.getCdimportacaoresultado() + "');" +
							"window.location = '" + request.getServletRequest().getContextPath() + "/sistema/process/ImportacaoDadosBasicos';" +
						"</script>" +
						"</body></html>"
						);
				
				return null;
				
			} else {
				request.addError("Erro na importa��o: n�o foi enviada a lista para o servidor.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na importa��o: " + e.getMessage());
		}
		
		return sendRedirectToAction("index");
	}
	
	@Input("index")
	@OnErrors("index")
	@Command(session=false)
	public ModelAndView salvaConfirmacaoDadosPessoa(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro){
		try{
			List<ImportacaoDadosPessoaBean> listaPessoaBean = filtro.getListaPessoaBean();
			if(listaPessoaBean != null){

				Map<String, Categoria> mapCategoria = new HashMap<String, Categoria>();
				
				for (int i=0; i < listaPessoaBean.size(); i++) {
					ImportacaoDadosPessoaBean bean = listaPessoaBean.get(i);
					
					/**
					 * Importante, para evitar que a importa��o n�o trave o servidor de aplica��o
					 */
					if(i%100==0){
						Thread.sleep(10000);
					}
					
					if(bean.getMarcado() != null && bean.getMarcado()){
						List<String> listaErro = new ArrayList<String>();
						List<String> listaAviso = new ArrayList<String>();
						
						Integer cdpessoa = null;
						
						if(filtro.getImportacaoDadosBasicosTipo().equals(ImportacaoDadosBasicosTipo.CLIENTE)){
							try{
								Cliente cliente = new Cliente();
								cliente.setIdentificador(bean.getIdentificador());
								cliente.setNome(bean.getNome());
								cliente.setRazaosocial(bean.getRazaosocial());
								cliente.setAtivo(Boolean.TRUE);
								if(bean.getCpf() != null){
									cliente.setTipopessoa(Tipopessoa.PESSOA_FISICA);
									cliente.setCpf(bean.getCpf());
								} else if(bean.getCnpj() != null){
									cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
									cliente.setCnpj(bean.getCnpj());
								} else {
									cliente.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
								}
								cliente.setInscricaoestadual(bean.getInscricaoestadual());
								cliente.setInscricaomunicipal(bean.getInscricaomunicipal());
								cliente.setEmail(bean.getEmail());
								cliente.setObservacao(bean.getObservacao());
								cliente.setDtnascimento(bean.getDtnascimento());
								cliente.setSexo(bean.getSexo());
								cliente.setAssociacao(bean.getFornecedorassociacao());
								
								cliente.setListaClienteespecialidade(new ListSet<Clienteespecialidade>(Clienteespecialidade.class));
								if(bean.getEspecialidade1() != null){
									cliente.getListaClienteespecialidade().add(new Clienteespecialidade(bean.getEspecialidade1(), bean.getRegistro1()));
								}
								if(bean.getEspecialidade2() != null){
									cliente.getListaClienteespecialidade().add(new Clienteespecialidade(bean.getEspecialidade2(), bean.getRegistro2()));
								}
								if(bean.getEspecialidade3() != null){
									cliente.getListaClienteespecialidade().add(new Clienteespecialidade(bean.getEspecialidade3(), bean.getRegistro3()));
								}
								if(bean.getEspecialidade4() != null){
									cliente.getListaClienteespecialidade().add(new Clienteespecialidade(bean.getEspecialidade4(), bean.getRegistro4()));
								}
								if(bean.getEspecialidade5() != null){
									cliente.getListaClienteespecialidade().add(new Clienteespecialidade(bean.getEspecialidade5(), bean.getRegistro5()));
								}
								
								clienteService.saveOrUpdate(cliente);
								cdpessoa = cliente.getCdpessoa();
							} catch (Exception e) {
								e.printStackTrace();
								bean.setErro("Erro na cria��o do cliente: " + e.getMessage());
								continue;
							}
						} else {
							try{
								Fornecedor fornecedor = new Fornecedor();
								fornecedor.setIdentificador(bean.getIdentificador());
								fornecedor.setNome(bean.getNome());
								fornecedor.setRazaosocial(bean.getRazaosocial());
								fornecedor.setAtivo(Boolean.TRUE);
								fornecedor.setAcesso(Boolean.FALSE);
								if(bean.getCpf() != null){
									fornecedor.setTipopessoa(Tipopessoa.PESSOA_FISICA);
									fornecedor.setCpf(bean.getCpf());
								} else if(bean.getCnpj() != null){
									fornecedor.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
									fornecedor.setCnpj(bean.getCnpj());
								} else {
									fornecedor.setTipopessoa(Tipopessoa.PESSOA_JURIDICA);
								}
								fornecedor.setInscricaoestadual(bean.getInscricaoestadual());
								fornecedor.setInscricaomunicipal(bean.getInscricaomunicipal());
								fornecedor.setEmail(bean.getEmail());
								fornecedor.setObservacao(bean.getObservacao());
								
								fornecedorService.saveOrUpdate(fornecedor);
								cdpessoa = fornecedor.getCdpessoa();
							} catch (Exception e) {
								e.printStackTrace();
								bean.setErro("Erro na cria��o do fornecedor: " + e.getMessage());
								continue;
							}
						}
						
						bean.setCdpessoa(cdpessoa);
						Pessoa pessoa = new Pessoa(cdpessoa);
						
						if(Util.strings.isNotEmpty(bean.getTelefoneprincipal())){
							try{
								this.createTelefone(bean.getTelefoneprincipal(), pessoa, Telefonetipo.PRINCIPAL);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na cria��o do telefone principal: " + e.getMessage());
							}
						}
						
						if(Util.strings.isNotEmpty(bean.getTelefonecelular())){
							try{
								this.createTelefone(bean.getTelefonecelular(), pessoa, Telefonetipo.CELULAR);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na cria��o do telefone celular: " + e.getMessage());
							}
						}
						
						if(Util.strings.isNotEmpty(bean.getTelefonefax())){
							try{
								this.createTelefone(bean.getTelefonefax(), pessoa, Telefonetipo.FAX);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na cria��o do telefone fax: " + e.getMessage());
							}
						}
						
						if(Util.strings.isNotEmpty(bean.getContatonome())){
							try{
								PessoaContato pessoaContato = new PessoaContato();
								
								Contato contato = new Contato();
								contato.setNome(bean.getContatonome());
								contato.setEmailcontato(bean.getContatoemail());
								
								pessoaContato.setPessoa(pessoa);
								pessoaContato.setContato(contato);
								
								contatoService.saveOrUpdate(contato);
								pessoaContatoService.saveOrUpdate(pessoaContato);
								
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na cria��o do contato: " + e.getMessage());
							}
						}
						
						if(Util.strings.isNotEmpty(bean.getEnderecologradouro())){
							try{
								Endereco endereco = new Endereco();
								endereco.setPessoa(pessoa);
								endereco.setEnderecotipo(Enderecotipo.UNICO);
								endereco.setLogradouro(bean.getEnderecologradouro());
								endereco.setNumero(bean.getEndereconumero());
								endereco.setComplemento(bean.getEnderecocomplemento());
								endereco.setBairro(bean.getEnderecobairro());
								endereco.setUf(bean.getEnderecouf());
								endereco.setMunicipio(bean.getEnderecomunicipio());
								endereco.setCep(bean.getEnderecocep());
								
								enderecoService.saveOrUpdate(endereco);
							} catch (Exception e) {
								e.printStackTrace();
								listaErro.add("Erro na cria��o do endere�o: " + e.getMessage());
							}
						}
						
						if(bean.getCategoriaStr() != null && !bean.getCategoriaStr().equals("")){
							Categoria categoria = bean.getCategoria();
							if(categoria == null){
								if(mapCategoria.containsKey(bean.getCategoriaStr())){
									categoria = mapCategoria.get(bean.getCategoriaStr());
								} else {
									try{
										categoria = new Categoria();
										categoria.setCliente(filtro.getImportacaoDadosBasicosTipo().equals(ImportacaoDadosBasicosTipo.CLIENTE));
										categoria.setFornecedor(filtro.getImportacaoDadosBasicosTipo().equals(ImportacaoDadosBasicosTipo.FORNECEDOR));
										categoria.setNome(bean.getCategoriaStr());
										
										categoriaService.preparaParaSalvar(categoria);
										categoriaService.saveOrUpdate(categoria);
										
										mapCategoria.put(bean.getCategoriaStr(), categoria);
									} catch (Exception e) {
										e.printStackTrace();
										listaErro.add("Erro na cria��o do tipo de material: " + e.getMessage());
									}
								}
								
								listaAviso.add("Categoria '" + bean.getCategoriaStr() + "' criado com o id " + categoria.getCdcategoria());
							}
							
							if(categoria != null){
								try{
									Pessoacategoria pessoacategoria = new Pessoacategoria();
									pessoacategoria.setCategoria(categoria);
									pessoacategoria.setPessoa(pessoa);
									
									pessoacategoriaService.saveOrUpdate(pessoacategoria);
								} catch (Exception e) {
									e.printStackTrace();
									listaErro.add("Erro na vincula��o da categoria: " + e.getMessage());
								}
							}
						}
						
						bean.setAvisos(CollectionsUtil.concatenate(listaAviso, "\n"));
						bean.setErro(CollectionsUtil.concatenate(listaErro, "\n"));
					}
				}
				
				request.addMessage("Importa��o feita com sucesso.");
				
				for (ImportacaoDadosPessoaBean bean : listaPessoaBean) {
					String erros = bean.getErro();
					String avisos = bean.getAvisos();
					
					
					if(avisos != null){
						avisos = avisos.replaceAll("<ul>", "");
						avisos = avisos.replaceAll("</ul>", "");
						avisos = avisos.replaceAll("<li>", "");
						avisos = avisos.replaceAll("</li>", "\n");
					}
					
					if(erros != null){
						erros = erros.replaceAll("<ul>", "");
						erros = erros.replaceAll("</ul>", "");
						erros = erros.replaceAll("<li>", ""); 
						erros = erros.replaceAll("</li>", "\n");
					}
					
					bean.setErro(erros);
					bean.setAvisos(avisos);
				}
				
				Importacaoresultado importacaoresultado = this.makeResultadoImportacaoPessoa(listaPessoaBean, filtro.getImportacaoDadosBasicosTipo(), filtro.getChaveArquivo(), request);
				
				request.addMessage("Para imprimir o resultado do processamento <a href='" + request.getServletRequest().getContextPath() + "/sistema/crud/Importacaoresultado?ACAO=imprimeRelatorioResultado&cdimportacaoresultado=" + importacaoresultado.getCdimportacaoresultado() + "'>clique aqui</a>.");
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println(
						"<html><body>" +
						"<script>" +
							"window.open('" + request.getServletRequest().getContextPath() + "/sistema/crud/Importacaoresultado?ACAO=imprimeRelatorioResultado&cdimportacaoresultado=" + importacaoresultado.getCdimportacaoresultado() + "');" +
							"window.location = '" + request.getServletRequest().getContextPath() + "/sistema/process/ImportacaoDadosBasicos';" +
						"</script>" +
						"</body></html>"
						);
				
				return null;
			} else {
				request.addError("Erro na importa��o: n�o foi enviada a lista para o servidor.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na importa��o: " + e.getMessage());
		}
		
		return sendRedirectToAction("index");
	}
	
	private Importacaoresultado makeResultadoImportacaoLead(List<ImportacaoDadosLeadBean> listaLeadBean, String chaveArquivo, WebRequestContext request) throws Exception {
		Arquivo arquivoimportado = (Arquivo)request.getSession().getAttribute(chaveArquivo);
		
		Report report = new Report("/sistema/importacaoresultadolead");
		
		Empresa empresa = empresaService.loadPrincipal();
		
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
		report.addParameter("TITULO", "RESULTADO DA IMPORTA��O DE LEAD");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		report.setDataSource(listaLeadBean);
		
		MergeReport mergeReport = new MergeReport("importacao_resultado_" + SinedUtil.datePatternForReport() + ".pdf");
		mergeReport.addReport(report);
		
		Resource resource = mergeReport.generateResource();
		
		Arquivo arquivorelatorioresultado = new Arquivo();
		
		arquivorelatorioresultado.setNome(resource.getFileName());
		arquivorelatorioresultado.setContent(resource.getContents());
		arquivorelatorioresultado.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
		arquivorelatorioresultado.setTamanho(Long.parseLong("" + resource.getContents().length));
		arquivorelatorioresultado.setTipoconteudo(resource.getContentType());
		
		Importacaoresultado importacaoresultado = new Importacaoresultado();
		importacaoresultado.setArquivoimportado(arquivoimportado);
		importacaoresultado.setArquivorelatorioresultado(arquivorelatorioresultado);
		importacaoresultado.setTipo(ImportacaoDadosBasicosTipo.LEAD);
		
		arquivoDAO.saveFile(importacaoresultado, "arquivoimportado");
		arquivoDAO.saveFile(importacaoresultado, "arquivorelatorioresultado");
		
		importacaoresultadoService.saveOrUpdate(importacaoresultado);
		
		return importacaoresultado;
	}
	
	private Importacaoresultado makeResultadoImportacaoPessoa(List<ImportacaoDadosPessoaBean> listaPessoaBean, 
			ImportacaoDadosBasicosTipo importacaoDadosBasicosTipo,
			String chaveArquivo, WebRequestContext request) throws Exception {
		
		Arquivo arquivoimportado = (Arquivo)request.getSession().getAttribute(chaveArquivo);
		
		Report report = new Report("/sistema/importacaoresultadopessoa");
		
		Empresa empresa = empresaService.loadPrincipal();
		
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
		if(importacaoDadosBasicosTipo.equals(ImportacaoDadosBasicosTipo.CLIENTE)){
			report.addParameter("TITULO", "RESULTADO DA IMPORTA��O DE CLIENTE");
		} else {
			report.addParameter("TITULO", "RESULTADO DA IMPORTA��O DE FORNECEDOR");
		}
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		report.setDataSource(listaPessoaBean);
		
		MergeReport mergeReport = new MergeReport("importacao_resultado_" + SinedUtil.datePatternForReport() + ".pdf");
		mergeReport.addReport(report);
		
		Resource resource = mergeReport.generateResource();
		
		Arquivo arquivorelatorioresultado = new Arquivo();
		
		arquivorelatorioresultado.setNome(resource.getFileName());
		arquivorelatorioresultado.setContent(resource.getContents());
		arquivorelatorioresultado.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
		arquivorelatorioresultado.setTamanho(Long.parseLong("" + resource.getContents().length));
		arquivorelatorioresultado.setTipoconteudo(resource.getContentType());
		
		Importacaoresultado importacaoresultado = new Importacaoresultado();
		importacaoresultado.setArquivoimportado(arquivoimportado);
		importacaoresultado.setArquivorelatorioresultado(arquivorelatorioresultado);
		importacaoresultado.setTipo(importacaoDadosBasicosTipo);
		
		arquivoDAO.saveFile(importacaoresultado, "arquivoimportado");
		arquivoDAO.saveFile(importacaoresultado, "arquivorelatorioresultado");
		
		importacaoresultadoService.saveOrUpdate(importacaoresultado);
		
		return importacaoresultado;
	}
	
	private void createTelefone(String telefoneStr, Pessoa pessoa, Integer cdtelefonetipo) {
		Telefone telefone = new Telefone();
		telefone.setPessoa(pessoa);
		telefone.setTelefone(telefoneStr);
		telefone.setTelefonetipo(new Telefonetipo(cdtelefonetipo));
		
		telefoneService.saveOrUpdate(telefone);
	}
	
	private void createTelefoneLead(String telefoneStr, Lead lead, Integer cdtelefonetipo) {
		Leadtelefone telefone = new Leadtelefone();
		telefone.setLead(lead);
		telefone.setTelefone(telefoneStr);
		telefone.setTelefonetipo(new Telefonetipo(cdtelefonetipo));
		
		leadtelefoneService.saveOrUpdate(telefone);
	}
	
	@Input("index")
	@OnErrors("index")
	@Command(session=false)
	public ModelAndView salvaConfirmacaoDadosMaterial(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro){
		
		try{
			List<ImportacaoDadosMaterialBean> listaMaterialBean = filtro.getListaMaterialBean();
			if(listaMaterialBean != null){
				
				Map<String, Unidademedida> mapUnidademedida = new HashMap<String, Unidademedida>();
				Map<String, Materialtipo> mapMaterialtipo = new HashMap<String, Materialtipo>();
				Map<String, Materialgrupo> mapMaterialgrupo = new HashMap<String, Materialgrupo>();
				
				for (int i=0; i< listaMaterialBean.size(); i++) {
					ImportacaoDadosMaterialBean bean = listaMaterialBean.get(i);
					/**
					 * Importante, para evitar que a importa��o n�o trave o servidor de aplica��o
					 */
					if(i%100==0){
						Thread.sleep(10000);
					}
					if(bean.getMarcado() != null && bean.getMarcado()){
						if(materialService.haveMaterialNomeIdentificacao(bean.getIdentificador(), bean.getNome())){
							bean.setErro("Material j� existe na base de dados.");
							continue;
						}
						
						List<String> listaErro = new ArrayList<String>();
						List<String> listaAviso = new ArrayList<String>();
						
						Material material = new Material();
						material.setIdentificacao(bean.getIdentificador());
						material.setReferencia(bean.getReferencia());
						material.setNome(bean.getNome());
						material.setAtivo(Boolean.TRUE);
						material.setNomenf(bean.getNomenf());
						material.setContagerencial(bean.getContagerencialcompra());
						material.setCentrocustovenda(bean.getCentrocustovenda());
						material.setContagerencialvenda(bean.getContagerencialvenda());
						material.setValorcusto(bean.getValorcusto());
						material.setValorvenda(bean.getValorvenda());
						material.setNcmcompleto(bean.getNcm());
						
						if(bean.getNcm() != null && !bean.getNcm().equals("")){
							if(bean.getNcm().length() >= 2){
								try{
									String ncmcapituloCodigo = bean.getNcm().substring(0, 2);
									Ncmcapitulo ncmcapitulo = new Ncmcapitulo(Integer.parseInt(ncmcapituloCodigo));
									ncmcapitulo = ncmcapituloService.load(ncmcapitulo);
									material.setNcmcapitulo(ncmcapitulo);
									
									if(ncmcapitulo == null){
										listaAviso.add("N�o foi poss�vel carregar o cap�tulo de NCM.");
									}
								} catch (Exception e) {
									e.printStackTrace();
									listaAviso.add("Erro no carregamento do cap�tulo do NCM: " + e.getMessage());
								}
							}
						}
						
						if(bean.getMaterialclasse() != null){
							material.setProduto(bean.getMaterialclasse().equals(Materialclasse.PRODUTO));
							material.setPatrimonio(bean.getMaterialclasse().equals(Materialclasse.PATRIMONIO));
							material.setEpi(bean.getMaterialclasse().equals(Materialclasse.EPI));
							material.setServico(bean.getMaterialclasse().equals(Materialclasse.SERVICO));
						} else {
							listaErro.add("N�o foi poss�vel encontrar a classe do material.");
						}
						
						if(bean.getUnidademedida() == null){
							Unidademedida unidademedida = null;
							if(mapUnidademedida.containsKey(bean.getUnidademedidaStr())){
								unidademedida = mapUnidademedida.get(bean.getUnidademedidaStr());
							} else {
								try{
									unidademedida = new Unidademedida();
									unidademedida.setAtivo(Boolean.TRUE);
									unidademedida.setNome(bean.getUnidademedidaStr());
									unidademedida.setSimbolo(bean.getUnidademedidaStr());
									
									unidademedidaService.saveOrUpdate(unidademedida);
									
									mapUnidademedida.put(bean.getUnidademedidaStr(), unidademedida);
								} catch (Exception e) {
									e.printStackTrace();
									listaErro.add("Erro na cria��o da unidade de medida: " + e.getMessage());
								}
							}
							listaAviso.add("Unidade de medida '" + bean.getUnidademedidaStr() + "' criada com o id " + unidademedida.getCdunidademedida());
							bean.setUnidademedida(unidademedida);
						}
						material.setUnidademedida(bean.getUnidademedida());
						
						
						if(bean.getMaterialgrupo() == null){
							Materialgrupo materialgrupo = null;
							if(mapMaterialgrupo.containsKey(bean.getMaterialgrupoStr())){
								materialgrupo = mapMaterialgrupo.get(bean.getMaterialgrupoStr());
							} else {
								try{
									materialgrupo = new Materialgrupo();
									materialgrupo.setAtivo(Boolean.TRUE);
									materialgrupo.setNome(bean.getMaterialgrupoStr());
									materialgrupo.setLocacao(Boolean.FALSE);
									
									materialgrupoService.saveOrUpdate(materialgrupo);
									
									mapMaterialgrupo.put(bean.getMaterialgrupoStr(), materialgrupo);
								} catch (Exception e) {
									e.printStackTrace();
									listaErro.add("Erro na cria��o do grupo de material: " + e.getMessage());
								}
							}
							listaAviso.add("Grupo de material '" + bean.getMaterialgrupoStr() + "' criado com o id " + materialgrupo.getCdmaterialgrupo());
							bean.setMaterialgrupo(materialgrupo);
						}
						material.setMaterialgrupo(bean.getMaterialgrupo());
						
						
						if(bean.getMaterialtipoStr() != null && !bean.getMaterialtipoStr().equals("")){
							if(bean.getMaterialtipo() == null){
								Materialtipo materialtipo = null;
								if(mapMaterialtipo.containsKey(bean.getMaterialtipoStr())){
									materialtipo = mapMaterialtipo.get(bean.getMaterialtipoStr());
								} else {
									try{
										materialtipo = new Materialtipo();
										materialtipo.setAtivo(Boolean.TRUE);
										materialtipo.setNome(bean.getMaterialtipoStr());
										materialtipo.setVeiculo(Boolean.FALSE);
										
										materialtipoService.saveOrUpdate(materialtipo);
										
										mapMaterialtipo.put(bean.getMaterialtipoStr(), materialtipo);
									} catch (Exception e) {
										e.printStackTrace();
										listaErro.add("Erro na cria��o do tipo de material: " + e.getMessage());
									}
								}
								listaAviso.add("Grupo de material '" + bean.getMaterialtipoStr() + "' criado com o id " + materialtipo.getCdmaterialtipo());
								bean.setMaterialtipo(materialtipo);
							}
							material.setMaterialtipo(bean.getMaterialtipo());
						}
						
						if(listaErro.size() == 0){
							materialService.saveOrUpdate(material);
							bean.setCdmaterial(material.getCdmaterial());
						}
						
						bean.setAvisos(CollectionsUtil.concatenate(listaAviso, "\n"));
						bean.setErro(CollectionsUtil.concatenate(listaErro, "\n"));
					}
				}
				
				request.addMessage("Importa��o feita com sucesso.");
				
				for (ImportacaoDadosMaterialBean bean : listaMaterialBean) {
					String erros = bean.getErro();
					String avisos = bean.getAvisos();
					
					
					if(avisos != null){
						avisos = avisos.replaceAll("<ul>", "");
						avisos = avisos.replaceAll("</ul>", "");
						avisos = avisos.replaceAll("<li>", "");
						avisos = avisos.replaceAll("</li>", "\n");
					}
					
					if(erros != null){
						erros = erros.replaceAll("<ul>", "");
						erros = erros.replaceAll("</ul>", "");
						erros = erros.replaceAll("<li>", "");
						erros = erros.replaceAll("</li>", "\n");
					}
					
					bean.setErro(erros);
					bean.setAvisos(avisos);
				}
				
				Importacaoresultado importacaoresultado = this.makeResultadoImportacaoMaterial(listaMaterialBean, filtro.getChaveArquivo(), request);
				
				request.addMessage("Para imprimir o resultado do processamento <a href='" + request.getServletRequest().getContextPath() + "/sistema/crud/Importacaoresultado?ACAO=imprimeRelatorioResultado&cdimportacaoresultado=" + importacaoresultado.getCdimportacaoresultado() + "'>clique aqui</a>.");
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println(
						"<html><body>" +
						"<script>" +
							"window.open('" + request.getServletRequest().getContextPath() + "/sistema/crud/Importacaoresultado?ACAO=imprimeRelatorioResultado&cdimportacaoresultado=" + importacaoresultado.getCdimportacaoresultado() + "');" +
							"window.location = '" + request.getServletRequest().getContextPath() + "/sistema/process/ImportacaoDadosBasicos';" +
						"</script>" +
						"</body></html>"
						);
				
				return null;
				
			} else {
				request.addError("Erro na importa��o: n�o foi enviada a lista para o servidor.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na importa��o: " + e.getMessage());
		}
		
		return sendRedirectToAction("index");
	}
	
	private Importacaoresultado makeResultadoImportacaoMaterial(List<ImportacaoDadosMaterialBean> listaMaterialBean, String chaveArquivo, WebRequestContext request) throws Exception {
		Arquivo arquivoimportado = (Arquivo)request.getSession().getAttribute(chaveArquivo);
		
		Report report = new Report("/sistema/importacaoresultadomaterial");
		
		Empresa empresa = empresaService.loadPrincipal();
		
		report.addParameter("LOGO", SinedUtil.getLogo(empresa));
		report.addParameter("EMPRESA", empresa == null ? null : empresa.getRazaosocialOuNome());
		report.addParameter("TITULO", "RESULTADO DA IMPORTA��O DE MATERIAL");
		report.addParameter("USUARIO", Util.strings.toStringDescription(Neo.getUser()));
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		
		report.setDataSource(listaMaterialBean);
		
		MergeReport mergeReport = new MergeReport("importacao_resultado_" + SinedUtil.datePatternForReport() + ".pdf");
		mergeReport.addReport(report);
		
		Resource resource = mergeReport.generateResource();
		
		Arquivo arquivorelatorioresultado = new Arquivo();
		
		arquivorelatorioresultado.setNome(resource.getFileName());
		arquivorelatorioresultado.setContent(resource.getContents());
		arquivorelatorioresultado.setDtmodificacao(new Timestamp(System.currentTimeMillis()));
		arquivorelatorioresultado.setTamanho(Long.parseLong("" + resource.getContents().length));
		arquivorelatorioresultado.setTipoconteudo(resource.getContentType());
		
		Importacaoresultado importacaoresultado = new Importacaoresultado();
		importacaoresultado.setArquivoimportado(arquivoimportado);
		importacaoresultado.setArquivorelatorioresultado(arquivorelatorioresultado);
		importacaoresultado.setTipo(ImportacaoDadosBasicosTipo.MATERIAL);
		
		arquivoDAO.saveFile(importacaoresultado, "arquivoimportado");
		arquivoDAO.saveFile(importacaoresultado, "arquivorelatorioresultado");
		
		importacaoresultadoService.saveOrUpdate(importacaoresultado);
		
		return importacaoresultado;
	}
	
	public ModelAndView exportarLeadsNaoMarcadas(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro) throws IOException{
		SinedExcel wb = new SinedExcel();
		HSSFSheet planilha = wb.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) 35);
		
		HSSFRow row = null;
		HSSFCell cell = null;
		
		row = planilha.createRow((short) 0);
		cell = row.createCell((short) 0);
		
		planilha.setColumnWidth((short) 0, (short) (150*35));
		planilha.setColumnWidth((short) 1, (short) (150*35));
		planilha.setColumnWidth((short) 2, (short) (150*35));
		planilha.setColumnWidth((short) 3, (short) (150*35));
		planilha.setColumnWidth((short) 4, (short) (150*35));
		planilha.setColumnWidth((short) 5, (short) (150*35));
		planilha.setColumnWidth((short) 6, (short) (150*35));
		planilha.setColumnWidth((short) 7, (short) (150*35));
		planilha.setColumnWidth((short) 8, (short) (150*35));
		planilha.setColumnWidth((short) 9, (short) (150*35));
		planilha.setColumnWidth((short) 10, (short) (150*35));
		planilha.setColumnWidth((short) 11, (short) (150*35));
		planilha.setColumnWidth((short) 12, (short) (150*35));
		planilha.setColumnWidth((short) 13, (short) (150*35));
		planilha.setColumnWidth((short) 14, (short) (150*35));
		planilha.setColumnWidth((short) 15, (short) (150*35));
		planilha.setColumnWidth((short) 16, (short) (150*35));
		planilha.setColumnWidth((short) 17, (short) (150*35));
		planilha.setColumnWidth((short) 18, (short) (150*35));
		planilha.setColumnWidth((short) 19, (short) (150*35));
		planilha.setColumnWidth((short) 20, (short) (150*35));
		planilha.setColumnWidth((short) 21, (short) (150*35));
		planilha.setColumnWidth((short) 22, (short) (150*35));
		planilha.setColumnWidth((short) 23, (short) (150*35));
		planilha.setColumnWidth((short) 24, (short) (150*35));
		
        row = planilha.createRow(0);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Contato");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Sexo");
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Empresa");
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("E-mail");
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Website");
		
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Respons�vel");
		
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Situa��o");
		
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Qualifica��o");
		
		cell = row.createCell((short) 8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Telefone (Principal)");
		
		cell = row.createCell((short) 9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Telefone (Celular)");
		
		cell = row.createCell((short) 10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Telefone (Comercial)");
		
		cell = row.createCell((short) 11);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Segmento");
		
		cell = row.createCell((short) 12);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Logradouro");
		
		cell = row.createCell((short) 13);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("N�mero");
		
		cell = row.createCell((short) 14);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Complemento");
		
		cell = row.createCell((short) 15);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Bairro");
		
		cell = row.createCell((short) 16);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("UF (Sigla)");
		
		cell = row.createCell((short) 17);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Cidade");
		
		cell = row.createCell((short) 18);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("CEP");
		
		cell = row.createCell((short) 19);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Data de Retorno");
		
		cell = row.createCell((short) 20);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Observa��o");
		
		cell = row.createCell((short) 21);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Concorrente");
		
		cell = row.createCell((short) 22);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Pr�ximo Passo");
		
		cell = row.createCell((short) 23);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Campanha");
		
		if(filtro.getListaLeadComErro() != null && !filtro.getListaLeadComErro().isEmpty()){
			for(int i=0; i<filtro.getListaLeadComErro().size(); i++ ){
				ImportacaoDadosLeadBean bean = filtro.getListaLeadComErro().get(i);
				
				row = planilha.createRow(i + 1);
				
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(bean.getContato());
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getSexoStr());
				
				cell = row.createCell((short) 2);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEmpresa());
				
				cell = row.createCell((short) 3);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEmail());
				
				cell = row.createCell((short) 4);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getWebsite());
				
				cell = row.createCell((short) 5);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getResponsavelCpfStr());
				
				cell = row.createCell((short) 6);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getSituacaoStr());
			
				cell = row.createCell((short) 7);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getQualificacaoStr());
				
				cell = row.createCell((short) 8);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getTelefonePrincipal());
				
				cell = row.createCell((short) 9);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getTelefoneCelular());
			
				cell = row.createCell((short) 10);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getTelefoneComercial());
				
				cell = row.createCell((short) 11);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getSegmentoStr());
				
				cell = row.createCell((short) 12);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecologradouro());
				
				cell = row.createCell((short) 13);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEndereconumero());
				
				cell = row.createCell((short) 14);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecocomplemento());
				
				cell = row.createCell((short) 15);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecobairro());
				
				cell = row.createCell((short) 16);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getConcorrenteStr());
				
				cell = row.createCell((short) 17);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecomunicipioStr());
				
				cell = row.createCell((short) 18);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecocepStr());
				
				cell = row.createCell((short) 19);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getDtretornoStr());
				
				cell = row.createCell((short) 20);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getObservacao());
				
				cell = row.createCell((short) 21);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getConcorrenteStr());
				
				cell = row.createCell((short) 22);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getProximoPasso());
				
				cell = row.createCell((short) 23);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getCampanhaStr());
			}
		}
		return new ResourceModelAndView(wb.getWorkBookResource("exportacao_lead.xls"));
	}
	
	/**
	 * M�todo que faz a exporta��o dos objetos (Clientes ou fornecedores)
	 * n�o marcados para um xls
	 * @param request
	 * @param filtro
	 * @return
	 * @throws IOException
	 */
	public ModelAndView exportarPessoasNaoMarcadas(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro) throws IOException{
		
		SinedExcel wb = new SinedExcel();
		HSSFSheet planilha = wb.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) 35);
		
		HSSFRow row = null;
		HSSFCell cell = null;
		
		row = planilha.createRow((short) 0);
		cell = row.createCell((short) 0);
		
		planilha.setColumnWidth((short) 0, (short) (150*35));
		planilha.setColumnWidth((short) 1, (short) (150*35));
		planilha.setColumnWidth((short) 2, (short) (150*35));
		planilha.setColumnWidth((short) 3, (short) (150*35));
		planilha.setColumnWidth((short) 4, (short) (150*35));
		planilha.setColumnWidth((short) 5, (short) (150*35));
		planilha.setColumnWidth((short) 6, (short) (150*35));
		planilha.setColumnWidth((short) 7, (short) (150*35));
		planilha.setColumnWidth((short) 8, (short) (150*35));
		planilha.setColumnWidth((short) 9, (short) (150*35));
		planilha.setColumnWidth((short) 10, (short) (150*35));
		planilha.setColumnWidth((short) 11, (short) (150*35));
		planilha.setColumnWidth((short) 12, (short) (150*35));
		planilha.setColumnWidth((short) 13, (short) (150*35));
		planilha.setColumnWidth((short) 14, (short) (150*35));
		planilha.setColumnWidth((short) 15, (short) (150*35));
		planilha.setColumnWidth((short) 16, (short) (150*35));
		planilha.setColumnWidth((short) 17, (short) (150*35));
		planilha.setColumnWidth((short) 18, (short) (150*35));
		planilha.setColumnWidth((short) 19, (short) (150*35));
		planilha.setColumnWidth((short) 20, (short) (150*35));
		planilha.setColumnWidth((short) 21, (short) (150*35));
		
        row = planilha.createRow(0);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Identificador");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Nome Fantasia");
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Raz�o Social");
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("CNPJ/CPF");
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Insc. Estadual");
		
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Insc. Municipial");
		
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("E-mail");
		
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Telefone (Principal)");
		
		cell = row.createCell((short) 8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Telefone (Celular)");
		
		cell = row.createCell((short) 9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Telefone (Fax)");
		
		cell = row.createCell((short) 10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Contato (Nome)");
		
		cell = row.createCell((short) 11);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("E-mail do contato");
		
		cell = row.createCell((short) 12);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Logradouro");
		
		cell = row.createCell((short) 13);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("N�mero");
		
		cell = row.createCell((short) 14);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Complemento");
		
		cell = row.createCell((short) 15);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Bairro");
		
		cell = row.createCell((short) 16);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("UF (Sigla)");
		
		cell = row.createCell((short) 17);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Cidade");
		
		cell = row.createCell((short) 18);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("CEP");
		
		cell = row.createCell((short) 19);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Categoria (Nome)");
		
		cell = row.createCell((short) 20);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Observa��o");
		
		if(filtro.getListaPessoaBeanComErro() != null && !filtro.getListaPessoaBeanComErro().isEmpty()){
			for(int i=0; i<filtro.getListaPessoaBeanComErro().size(); i++ ){
				ImportacaoDadosPessoaBean bean = filtro.getListaPessoaBeanComErro().get(i);
				
				row = planilha.createRow(i + 1);
				
				cell = row.createCell((short) 0);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_RIGHT_WHITE);
				cell.setCellValue(bean.getIdentificador());
				
				cell = row.createCell((short) 1);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getNome());
				
				cell = row.createCell((short) 2);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getRazaosocial());
				
				cell = row.createCell((short) 3);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getCpfcnpj());
				
				cell = row.createCell((short) 4);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getInscricaoestadual());
				
				cell = row.createCell((short) 5);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getInscricaomunicipal());
				
				cell = row.createCell((short) 6);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEmail());
			
				cell = row.createCell((short) 7);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getTelefoneprincipal());
				
				cell = row.createCell((short) 8);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getTelefonecelular());
				
				cell = row.createCell((short) 9);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getTelefonefax());
			
				cell = row.createCell((short) 10);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getContatonome());
				
				cell = row.createCell((short) 11);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getContatoemail());
				
				cell = row.createCell((short) 12);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecologradouro());
				
				cell = row.createCell((short) 13);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEndereconumero());
				
				cell = row.createCell((short) 14);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecocomplemento());
				
				cell = row.createCell((short) 15);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecobairro());
				
				cell = row.createCell((short) 16);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecoufStr());
				
				cell = row.createCell((short) 17);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecomunicipioStr());
				
				cell = row.createCell((short) 18);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getEnderecocepStr());
				
				cell = row.createCell((short) 19);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getCategoriaStr());
				
				cell = row.createCell((short) 20);
				cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
				cell.setCellValue(bean.getObservacao());
			}
		}
		return new ResourceModelAndView(wb.getWorkBookResource("exportacao_cliente_fornecedor.xls"));
	}
	
	/**
	 * M�todo que faz a exporta��o dos materiais
	 * n�o marcados para um arquivo xls
	 * @param request
	 * @param filtro
	 * @return
	 * @throws IOException
	 */
	public ModelAndView exportarMateriaisNaoMarcados(WebRequestContext request, ImportacaoDadosBasicosFiltro filtro) throws IOException{
		
		SinedExcel wb = new SinedExcel();
		HSSFSheet planilha = wb.createSheet("Planilha");
		planilha.setDefaultColumnWidth((short) 35);
		
		HSSFRow row = null;
		HSSFCell cell = null;
		
		row = planilha.createRow((short) 0);
		cell = row.createCell((short) 0);
		
		planilha.setColumnWidth((short) 0, (short) (150*35));
		planilha.setColumnWidth((short) 1, (short) (150*35));
		planilha.setColumnWidth((short) 2, (short) (150*35));
		planilha.setColumnWidth((short) 3, (short) (150*35));
		planilha.setColumnWidth((short) 4, (short) (150*35));
		planilha.setColumnWidth((short) 5, (short) (150*35));
		planilha.setColumnWidth((short) 6, (short) (150*35));
		planilha.setColumnWidth((short) 7, (short) (150*35));
		planilha.setColumnWidth((short) 8, (short) (150*35));
		planilha.setColumnWidth((short) 9, (short) (150*35));
		planilha.setColumnWidth((short) 10, (short) (150*35));
		planilha.setColumnWidth((short) 11, (short) (150*35));
		planilha.setColumnWidth((short) 12, (short) (150*35));
		planilha.setColumnWidth((short) 13, (short) (150*35));
		
        row = planilha.createRow(0);
		
		cell = row.createCell((short) 0);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Identificador");
		
		cell = row.createCell((short) 1);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Refer�ncia");
		
		cell = row.createCell((short) 2);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Nome");
		
		cell = row.createCell((short) 3);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Nome para NF");
		
		cell = row.createCell((short) 4);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Classe(Produto / Patrim�nio / Servi�o / EPI)");
		
		cell = row.createCell((short) 5);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Unidade de Medida(Somente S�mbolo)");
		
		cell = row.createCell((short) 6);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Grupo de Material(Nome)");
		
		cell = row.createCell((short) 7);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Tipo do Material(Nome)");
		
		cell = row.createCell((short) 8);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Conta Gerencial de Compra (C�digo do W3erp)");
		
		cell = row.createCell((short) 9);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Centro de Custo de Venda (C�digo no W3erp)");
		
		cell = row.createCell((short) 10);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Conta Gerencial de Venda (C�digo no W3erp)");
		
		cell = row.createCell((short) 11);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor Custo");
		
		cell = row.createCell((short) 12);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("Valor Venda");
		
		cell = row.createCell((short) 13);
		cell.setCellStyle(SinedExcel.STYLE_HEADER_DATAGRID_BLUE_CENTER);
		cell.setCellValue("NCM");
		
		if(filtro.getListaMaterialBean() != null && !filtro.getListaMaterialBean().isEmpty()){
			for(int i=0; i<filtro.getListaMaterialBean().size(); i++ ){
				ImportacaoDadosMaterialBean bean = filtro.getListaMaterialBean().get(i);
				
				if(bean.getMarcado() != null && Boolean.FALSE.equals(bean.getMarcado())){
					row = planilha.createRow(i + 1);
					
					cell = row.createCell((short) 0);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getIdentificador());
					
					cell = row.createCell((short) 1);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getReferencia());
					
					cell = row.createCell((short) 2);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getNome());
					
					cell = row.createCell((short) 3);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getNomenf());
					
					cell = row.createCell((short) 4);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getMaterialclasseStr());
					
					cell = row.createCell((short) 5);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getUnidademedidaStr());
					
					cell = row.createCell((short) 6);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getMaterialgrupoStr());
				
					cell = row.createCell((short) 7);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getMaterialtipoStr());
					
					cell = row.createCell((short) 8);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getContagerencialcompraStr());
					
					cell = row.createCell((short) 9);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getCentrocustovendaStr());
				
					cell = row.createCell((short) 10);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getContagerencialvendaStr());
					
					cell = row.createCell((short) 11);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getValorcustoStr());
					
					cell = row.createCell((short) 12);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getValorvendaStr());
					
					cell = row.createCell((short) 13);
					cell.setCellStyle(SinedExcel.STYLE_DETALHE_CENTER_WHITE);
					cell.setCellValue(bean.getNcm());
					
				}
			}
		}
		return new ResourceModelAndView(wb.getWorkBookResource("exportacao_material.xls"));
	}
}
