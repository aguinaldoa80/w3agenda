package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.NotaFiscalServicoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.ImportadorBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.ImportacaoNfeTDriveFiltro;

@Controller(path="/sistema/process/ImportacaoNfeTDrive", authorizationModule=ProcessAuthorizationModule.class)
public class ImportacaoNfeTDriveProcess extends MultiActionController {
	
	private EmpresaService empresaService;
	private NotaFiscalServicoService notaFiscalServicoService;
	private UsuarioService usuarioService;
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNotaFiscalServicoService(NotaFiscalServicoService notaFiscalServicoService) {this.notaFiscalServicoService = notaFiscalServicoService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	
	@DefaultAction
	public void index(WebRequestContext request, ImportacaoNfeTDriveFiltro filtro) throws IOException {
		try{
			if(StringUtils.isBlank(filtro.getCnpj())){
				throw new RuntimeException("CNPJ � obrigat�rio.");
			}
			
			if(!Cnpj.cnpjValido(filtro.getCnpj())){
				throw new RuntimeException("CNPJ inv�lido.");
			}

			if(filtro.getArquivo()==null){
				throw new RuntimeException("Arquivo � obrigat�rio.");
			}
			
			Cnpj cnpj = new Cnpj(filtro.getCnpj());
			Empresa empresa = empresaService.findByCnpj(cnpj);
			if(empresa==null){
				throw new RuntimeException("Empresa n�o encontrada.");
			}
			
			if(!usuarioService.possuiPermissaoNaEmpresa(empresa)){
				throw new RuntimeException("O login informado n�o possui permiss�o para envio de notas fiscais para o CNPJ " + cnpj);
			}


			List<ImportadorBean> msgs = notaFiscalServicoService.processarArquivo(request, new String(filtro.getArquivo().getContent()), empresa);
			retornoSucesso(request, msgs);
		}catch(Exception e){
			e.printStackTrace();
			retornoErro(request, e.getMessage());
		}
		
	}
	
	private void retornoErro(WebRequestContext request, String erro) throws IOException{
		request.getServletResponse().getWriter().print("MSG_ERRO=" + erro);
	}
	
	private void retornoSucesso(WebRequestContext request, List<ImportadorBean> msgs) throws IOException{
		List<Integer> notasGeradas = new ArrayList<Integer>();
		List<Integer> notasErro = new ArrayList<Integer>();
		if(msgs != null && msgs.size() > 0){
			for (ImportadorBean bean : msgs) {
				if(bean.getErro()){
					notasErro.add(bean.getNumeroNota());
				} else {
					notasGeradas.add(bean.getNumeroNota());
				}
			}
		}
		
		StringBuilder retorno = new StringBuilder("NOTAS_GERADAS=");
		Iterator<Integer> iterator = notasGeradas.iterator();
		while(iterator.hasNext()){
			retorno.append(iterator.next());
			if(iterator.hasNext()) retorno.append(",");
		}
		
		retorno.append("NOTAS_ERRO=");
		iterator = notasErro.iterator();
		while(iterator.hasNext()){
			retorno.append(iterator.next());
			if(iterator.hasNext()) retorno.append(",");
		}
		request.getServletResponse().getWriter().print(retorno.toString());
	}
	
	public void validarURL(WebRequestContext request) throws IOException {		
	}
}