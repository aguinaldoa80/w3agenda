package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.GrupotributacaoNcm;
import br.com.linkcom.sined.geral.bean.Grupotributacaoimposto;
import br.com.linkcom.sined.geral.bean.Grupotributacaonaturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.geral.bean.enumeration.Origemproduto;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancacofins;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaicms;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancaipi;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocobrancapis;
import br.com.linkcom.sined.geral.bean.enumeration.Tipotributacaoicms;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.NatOprCfop;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ImportarSPEDFiscalFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.util.LeitorArquivoSPEDUtil;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path = "/sistema/process/ImportacaoSPEDFiscal", authorizationModule=ProcessAuthorizationModule.class)
public class ImportarSPEDFiscalProcess extends MultiActionController {

	private EmpresaService empresaService;
	private GrupotributacaoService grupotributacaoService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private CfopService cfopService;
	private LeitorArquivoSPEDUtil reader;
	
	private static final String INDEX = "process/importarspedfiscal";
	private static final String NATUREZAOPERACAO = "process/conferenciaNaturezaOperacao";
	private static final String GRUPOTRIBUTACAO = "process/conferenciaGrupotributacao";
	private static final String BLOCO_EMPRESA = "0000";
	private static final String BLOCO_MATERIAL = "0200";
	private static final String BLOCO_NATUREZAOPERACAO = "0400";
	private static final String BLOCO_NOTAFISCAL = "C100";
	private static final String BLOCO_ITEM_NOTA_FISCAL = "C170";
	
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService){this.grupotributacaoService = grupotributacaoService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService){this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ImportarSPEDFiscalFiltro filtro) {
		filtro.setArquivo(null);
		
		return goToPage(INDEX, filtro);
	}
	
	public ModelAndView ajaxChangeCnpj(WebRequestContext request, ImportarSPEDFiscalFiltro filtro) {
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if (filtro.getEmpresa() != null) {
			Empresa empresa = empresaService.load(filtro.getEmpresa(), "empresa.cnpj");
			
			if (empresa.getCnpj() != null) {
				jsonModelAndView.addObject("cnpj", empresa.getCnpj().getValue());
			}
		}
		
		return jsonModelAndView;
	}

	@Action("importarGrupoTributacao")
	public ModelAndView importarGrupoTributacao(WebRequestContext request, ImportarSPEDFiscalFiltro filtro) throws IOException {
		try {
			reader = new LeitorArquivoSPEDUtil(filtro.getArquivo(), BLOCO_EMPRESA, BLOCO_MATERIAL, BLOCO_NATUREZAOPERACAO, BLOCO_NOTAFISCAL, BLOCO_ITEM_NOTA_FISCAL);
		} catch (IllegalArgumentException e) {
			request.addError(e.getMessage());
		} catch (Exception e) {
			request.addError("N�o foi poss�vel importar o arquivo SPED Fiscal. Por favor entre em contato com o Suporte.");
		}
		
		filtro.setEmpresa(empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.cnpj"));
		filtro.setListaNatOprCfopSPEDFiscal(extrairListaNatOprCfop());
		
		if (filtro.getListaNatOprCfopSPEDFiscal() != null && filtro.getListaNatOprCfopSPEDFiscal().size() > 0) {
			return goToPage(NATUREZAOPERACAO, filtro);
		} else {
			this.reader.resetPointers();
			filtro.setListaGrupoTributacao(extrairGrupoTributacao(filtro.getListaNatOprCfopSPEDFiscal(), filtro.getEmpresa(), filtro.getListaGrupoTributacao()));
			
			return goToPage(GRUPOTRIBUTACAO, filtro);
		}
	}
	
	@Action("salvarNaturezaOperacao")
	public ModelAndView salvarNaturezaOperacao(WebRequestContext request, ImportarSPEDFiscalFiltro filtro) throws IOException{
		try {
			this.reader.resetPointers();
			filtro.setListaGrupoTributacao(extrairGrupoTributacao(filtro.getListaNatOprCfopSPEDFiscal(), filtro.getEmpresa(), filtro.getListaGrupoTributacao()));
			
			return goToPage(GRUPOTRIBUTACAO, filtro);
		} catch(Exception e) {
			e.printStackTrace();
			request.addError("N�o foi poss�vel importar o arquivo SPED Fiscal. Por favor entre em contato com o Suporte.");
			
			return goToPage(NATUREZAOPERACAO, filtro);
		}
	}
	
	@Action("inserirGrupoTributacao")
	public ModelAndView inserirGrupoTributacao(WebRequestContext request, ImportarSPEDFiscalFiltro filtro){
		Boolean erro = Boolean.FALSE;
		List<Grupotributacao> grupoTributacaoList = new ArrayList<Grupotributacao>();
		
		for(GrupoTributacaoSPEDFiscal gt: filtro.getListaGrupoTributacao()){
			if(!gt.isSelecionado()) {
				continue;
			}
			
			if (gt.getCfop() != null) {
				gt.setCfop(cfopService.load(gt.getCfop(), "cfop.cdcfop, cfop.codigo, cfop.descricaoresumida "));
			}
			
			if (gt.getNaturezaoperacao() != null) {
				gt.setNaturezaoperacao(naturezaoperacaoService.load(gt.getNaturezaoperacao(), "naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome"));
			}
			
			Grupotributacao grupoTributacao = adapterGrupoTributacao(gt);
			
			if (grupotributacaoService.conferirExisteGrupoTributacaoImportacaoSped(grupoTributacao)) {
				erro = Boolean.TRUE;
			}
			
			grupoTributacaoList.add(grupoTributacao);
		}
		
		if (erro) {
			request.addError("J� existe(m) grupo(s) de tributa��o com CFOP(s) selecinado(s)!");
			
			return goToPage(GRUPOTRIBUTACAO, filtro);
		}
		
		for (Grupotributacao grupoTributacao : grupoTributacaoList) {
			grupotributacaoService.saveOrUpdate(grupoTributacao);
		}
		
		request.addMessage("Aquivo SPED Fiscal importado com sucesso!");
		
		SinedUtil.redirecionamento(request, "/sistema/process/ImportacaoSPEDFiscal");
		return null;
	}
	
	private Grupotributacao adapterGrupoTributacao(GrupoTributacaoSPEDFiscal grupoSPED){
		Grupotributacao grupo = new Grupotributacao();
		
		if (grupoSPED.getCfop() != null) {
			grupo.setCfopgrupo(grupoSPED.getCfop());
		}
		
		if (StringUtils.isEmpty(grupo.getNome())) {
			if (grupoSPED.getCfop() != null) {
				grupo.setNome(cfopService.load(grupoSPED.getCfop(), "cfop.descricaoresumida").getDescricaoresumida());
			} else if (grupoSPED.getNaturezaoperacao() != null) {
				grupo.setNome(naturezaoperacaoService.load(grupoSPED.getNaturezaoperacao(), "naturezaoperacao.nome").getNome());
			} else {
				grupo.setNome("Grupo Tributa��o da Importa��o do SPED");
			}
		}
		
		if (grupoSPED.getUtilizacao() != null) {
			grupo.setOperacao("Entrada".equals(grupoSPED.getUtilizacao()) ? Operacao.ENTRADA : Operacao.SAIDA);
		}
		
		if (grupoSPED.getIcms() != null) {
			grupo.setOrigemprodutoicms(Origemproduto.getEnum(Integer.parseInt(grupoSPED.getIcms().substring(0, 1))));
		}
		
		if (grupoSPED.getIcms() != null) {
			grupo.setTipocobrancaicms(Tipocobrancaicms.getTipocobrancaicms(grupoSPED.getIcms().substring(1, 3)));
		}
		
		if (grupo.getTipocobrancaicms() != null) {
			grupo.setTipotributacaoicms(grupo.getTipocobrancaicms().isSimplesnacional() ? Tipotributacaoicms.SIMPLES_NACIONAL : Tipotributacaoicms.NORMAL);
		}
		
		Grupotributacaoimposto grupoimposto;
		
		if (grupoSPED.getAliquotaicms() != null) {
			grupoimposto = new Grupotributacaoimposto();
			grupoimposto.setControle(Faixaimpostocontrole.ICMS);
			grupoimposto.setAliquotafixa(new Money(Double.valueOf(grupoSPED.getAliquotaicms().replaceAll("\\.", "").replaceAll(",", "."))));
			grupo.getListaGrupotributacaoimposto().add(grupoimposto);
		}
		
		if (grupoSPED.getIpi() != null) {
			grupo.setTipocobrancaipi(Tipocobrancaipi.getTipocobrancaipi(grupoSPED.getIpi()));
		}
		
		if (grupoSPED.getAliquotaipi() != null) {
			grupoimposto = new Grupotributacaoimposto();
			grupoimposto.setControle(Faixaimpostocontrole.IPI);
			grupoimposto.setAliquotafixa(new Money(Double.valueOf(grupoSPED.getAliquotaipi().replaceAll("\\.", "").replaceAll(",", "."))));
			grupo.getListaGrupotributacaoimposto().add(grupoimposto);
		}
		
		if (grupoSPED.getPis() != null) {
			grupo.setTipocobrancapis(Tipocobrancapis.getTipocobrancapis(grupoSPED.getPis()));
		}
		
		if (grupoSPED.getAliquotapis() != null) {
			grupoimposto = new Grupotributacaoimposto();
			grupoimposto.setControle(Faixaimpostocontrole.PIS);
			grupoimposto.setAliquotafixa(new Money(Double.valueOf(grupoSPED.getAliquotapis().replaceAll("\\.", "").replaceAll(",", "."))));
			grupo.getListaGrupotributacaoimposto().add(grupoimposto);
		}
		
		if (grupoSPED.getCofins() != null) {
			grupo.setTipocobrancacofins(Tipocobrancacofins.getTipocobrancacofins(grupoSPED.getCofins()));
		}
		
		if (grupoSPED.getAliquotacofins() != null) {
			grupoimposto = new Grupotributacaoimposto();
			grupoimposto.setControle(Faixaimpostocontrole.COFINS);
			grupoimposto.setAliquotafixa(new Money(Double.valueOf(grupoSPED.getAliquotacofins().replaceAll("\\.", "").replaceAll(",", "."))));
			grupo.getListaGrupotributacaoimposto().add(grupoimposto);
		}
		
		GrupotributacaoNcm grupotributacaoNcm = null;
		Set<GrupotributacaoNcm> listaGrupotributacaoncm = new HashSet<GrupotributacaoNcm>();
		
		if (grupoSPED.getNcm() != null && BooleanUtils.isTrue(grupoSPED.isNcmSelecionado())) {
			for (String ncm : grupoSPED.getNcm().split(" ")) {
				grupotributacaoNcm = new GrupotributacaoNcm();
				
				if (!StringUtils.isEmpty(ncm.trim())) {
					grupotributacaoNcm.setNcm(ncm);
					listaGrupotributacaoncm.add(grupotributacaoNcm);
				}
			}
		}
		
		if (listaGrupotributacaoncm != null && listaGrupotributacaoncm.size() > 0) {
			grupo.setListaGrupotributacaoncm(listaGrupotributacaoncm);
		}
		
		if (grupoSPED.getNaturezaoperacao() != null) {
			grupo.getListaGrupotributacaonaturezaoperacao().add(new Grupotributacaonaturezaoperacao(grupo, grupoSPED.getNaturezaoperacao()));
		}
		
		return grupo;
	}
	
	@Action("cancelarGrupoTributacao")
	public ModelAndView cancelarGrupoTributacao(WebRequestContext request, ImportarSPEDFiscalFiltro filtro){
		return goToPage(NATUREZAOPERACAO, filtro);
	}
	
	private ModelAndView goToPage(String page, ImportarSPEDFiscalFiltro filtro){
		return new ModelAndView(page, "filtro", filtro);
	}
	
	private List<GrupoTributacaoSPEDFiscal> extrairGrupoTributacao(List<NatOprCfopSPEDFiscal> listNatCfop, Empresa empresa, List<GrupoTributacaoSPEDFiscal> listGrpTrib) {
		Map<String, GrupoTributacaoSPEDFiscal> map = new LinkedHashMap<String, GrupoTributacaoSPEDFiscal>();
		Map<String, NatOprCfop> mapNat = new LinkedHashMap<String, NatOprCfop>();
		Map<String, Cfop> mapCfop = new LinkedHashMap<String, Cfop>();
		
		for (NatOprCfopSPEDFiscal natCfop : listNatCfop) {
			if (natCfop.getSelecionado() && natCfop.getNaturezaOperacao() != null) {
				NatOprCfop natOprCfop = new NatOprCfop();
				natOprCfop.setNaturezaOperacao(naturezaoperacaoService.load(natCfop.getNaturezaOperacao(), "naturezaoperacao.cdnaturezaoperacao, naturezaoperacao.nome"));
				
				if (natCfop.getCfop() != null) {
					natOprCfop.setCfop(cfopService.load(natCfop.getCfop(), "cfop.cdcfop, cfop.codigo, cfop.descricaoresumida "));
				}
				
				mapNat.put(natCfop.getCodigo(), natOprCfop);
			} else if (natCfop.getSelecionado() && natCfop.getCfop() != null) {
				Cfop c = cfopService.load(natCfop.getCfop(), "cfop.cdcfop, cfop.codigo, cfop.descricaoresumida ");
				
				mapCfop.put(natCfop.getCodigo(), c);
			}
		}
		
		String utilizacao = null;
		int size = this.reader.size(BLOCO_NOTAFISCAL) + this.reader.size(BLOCO_ITEM_NOTA_FISCAL);
		this.reader.resetPointers();
		this.reader.next(BLOCO_NOTAFISCAL);
		this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
		
		if (mapNat != null && mapNat.size() > 0) {
			for (int i = 0; i < size; i++) {
				if (this.reader.pointer(BLOCO_NOTAFISCAL) == this.reader.size(BLOCO_NOTAFISCAL)	|| 
						this.reader.numberLine(BLOCO_NOTAFISCAL) > this.reader.numberLine(BLOCO_ITEM_NOTA_FISCAL)) {
					String codigo = this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 12) + this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 11);
					
					if (!mapNat.containsKey(codigo)) {
						this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
						continue;
					}
					
					GrupoTributacaoSPEDFiscal gt = adapterGrupoTributacaoSPEDFiscalNat(mapNat, utilizacao, codigo);
					String key = gt.key();
					
					if (map.containsKey(key)) {
						String ncm = gt.getNcm();
						gt = map.get(key);
						
						if (!gt.getNcm().contains(ncm)) {
							gt.setNcm(gt.getNcm() + ";" + ncm);
						}
						
						gt.setQuantidadeRegistros(gt.getQuantidadeRegistros() + 1);
					} else {
						map.put(key, gt);
					}
					if(!this.reader.hasNext(BLOCO_ITEM_NOTA_FISCAL)){
						break;
					}
					this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
				} else {
					utilizacao = "0".equals(this.reader.field(BLOCO_NOTAFISCAL, 2)) ? "Entrada"	: "Sa�da";
					this.reader.next(BLOCO_NOTAFISCAL);
				}
			}
		}
		
		this.reader.resetPointers();
		this.reader.next(BLOCO_NOTAFISCAL);
		this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
		
		if (mapCfop != null && mapCfop.size() > 0) {
			for (int i = 0; i < size; i++) {
				if (this.reader.pointer(BLOCO_NOTAFISCAL) == this.reader.size(BLOCO_NOTAFISCAL)	|| 
						this.reader.numberLine(BLOCO_NOTAFISCAL) > this.reader.numberLine(BLOCO_ITEM_NOTA_FISCAL)) {
					String codigo = this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 11);
					
					if (!mapCfop.containsKey(codigo)) {
						this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
						continue;
					}
					
					if (StringUtils.isNotEmpty(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 12))) {
						this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
						continue;
					}
					
					GrupoTributacaoSPEDFiscal gt = adapterGrupoTributacaoSPEDFiscalCfop(mapCfop, utilizacao, codigo);
					String key = gt.key();
					
					if (map.containsKey(key)) {
						String ncm = gt.getNcm();
						gt = map.get(key);
						
						if (!gt.getNcm().contains(ncm)) {
							gt.setNcm(gt.getNcm() + ";" + ncm);
						}
						
						gt.setQuantidadeRegistros(gt.getQuantidadeRegistros() + 1);
					} else {
						map.put(key, gt);
					}
					if(!this.reader.hasNext(BLOCO_ITEM_NOTA_FISCAL)){
						break;
					}
					this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
				} else {
					utilizacao = "0".equals(this.reader.field(BLOCO_NOTAFISCAL, 2)) ? "Entrada"	: "Sa�da";
					this.reader.next(BLOCO_NOTAFISCAL);
				}
			}
		}

		for (GrupoTributacaoSPEDFiscal grpTribSpedFiscal : map.values()) {
			if (StringUtils.isNotEmpty(grpTribSpedFiscal.getNcm())) {
				for (String idMat : StringUtils.split(grpTribSpedFiscal.getNcm(), ";")) {
					this.reader.resetPointers();
					this.reader.next(BLOCO_MATERIAL);

					String id = idMat;

					for (int i = 0; i < this.reader.size(BLOCO_MATERIAL); i++) {
						if (idMat.equals(this.reader.field(BLOCO_MATERIAL, 2))) {
							if (!grpTribSpedFiscal.getNcm().contains(this.reader.field(BLOCO_MATERIAL, 8))) {
								grpTribSpedFiscal.setNcm(grpTribSpedFiscal.getNcm().replace(idMat + ";", this.reader.field(BLOCO_MATERIAL, 8)));
							} else {
								grpTribSpedFiscal.setNcm(grpTribSpedFiscal.getNcm().replace(idMat + ";", ""));
							}

							break;
						}

						this.reader.next(BLOCO_MATERIAL);
					}

					if (id.equals(grpTribSpedFiscal.getNcm())) {
						grpTribSpedFiscal.setNcm("");
					}
				}
			}
			
			grpTribSpedFiscal.setNcm(grpTribSpedFiscal.getNcm().replace(";", " "));
		}

		return new ArrayList<GrupoTributacaoSPEDFiscal>(map.values());
	}
	
	private GrupoTributacaoSPEDFiscal adapterGrupoTributacaoSPEDFiscalNat(Map<String, NatOprCfop> mapNat, String utilizacao, String codigo) {
		GrupoTributacaoSPEDFiscal gt = new GrupoTributacaoSPEDFiscal();
		
		gt.setNcm(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 3) + ";");
		gt.setUtilizacao(utilizacao);
		gt.setIcms(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 10));
		gt.setCfop(mapNat.get(codigo).getCfop());
		gt.setNaturezaoperacao(mapNat.get(codigo).getNaturezaOperacao());
		gt.setAliquotaicms(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 14));
		gt.setIpi(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 20));
		gt.setAliquotaipi(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 23));
		gt.setPis(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 25));
		gt.setAliquotapis(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 27));
		gt.setCofins(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 31));
		gt.setAliquotacofins(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 33));
		gt.setQuantidadeRegistros(1);
		
		return gt;
	}
	
	private GrupoTributacaoSPEDFiscal adapterGrupoTributacaoSPEDFiscalCfop(Map<String, Cfop> mapCfop, String utilizacao, String codigo) {
		GrupoTributacaoSPEDFiscal gt = new GrupoTributacaoSPEDFiscal();
		
		gt.setNcm(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 3) + ";");
		gt.setUtilizacao(utilizacao);
		gt.setIcms(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 10));
		gt.setAliquotaicms(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 14));
		gt.setCfop(mapCfop.get(codigo));
		gt.setIpi(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 20));
		gt.setAliquotaipi(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 23));
		gt.setPis(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 25));
		gt.setAliquotapis(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 27));
		gt.setCofins(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 31));
		gt.setAliquotacofins(this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 33));
		gt.setQuantidadeRegistros(1);
		
		return gt;
	}
	
	private List<NatOprCfopSPEDFiscal> extrairListaNatOprCfop() {
		Map<String, NatOprCfopSPEDFiscal> map = new LinkedHashMap<String, NatOprCfopSPEDFiscal>();
		
		while (this.reader.hasNext(BLOCO_ITEM_NOTA_FISCAL)) {
			this.reader.next(BLOCO_ITEM_NOTA_FISCAL);
			String codigoNatOpr = this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 12);
			String codigoCfop = this.reader.field(BLOCO_ITEM_NOTA_FISCAL, 11);
			String codigoCompleto = codigoNatOpr + codigoCfop;
			
			if (StringUtils.isNotBlank(codigoCompleto)) {
				if (!map.containsKey(codigoCompleto)) {
					map.put(codigoCompleto, new NatOprCfopSPEDFiscal(codigoCfop, codigoNatOpr, 1));
				} else {
					NatOprCfopSPEDFiscal nat = map.get(codigoCompleto);
					nat.setQuantidadeRegistros(nat.getQuantidadeRegistros() + 1);
				}
			}
		}
		
		for (Map.Entry<String, NatOprCfopSPEDFiscal> entry : map.entrySet()) {
			if (StringUtils.isNotEmpty(entry.getValue().getNaturezaOperacaoCodigo())) {
				this.reader.resetPointers();
				
				while (this.reader.hasNext(BLOCO_NATUREZAOPERACAO)) {
					this.reader.next(BLOCO_NATUREZAOPERACAO);
					String codigoNatOpr = this.reader.field(BLOCO_NATUREZAOPERACAO, 2);
					
					if (StringUtils.isNotEmpty(codigoNatOpr) && codigoNatOpr.equals(entry.getValue().getNaturezaOperacaoCodigo())) {
						NatOprCfopSPEDFiscal nat = map.get(entry.getKey());
						nat.setNaturezaOperacaoDescricao(this.reader.field(BLOCO_NATUREZAOPERACAO, 3));
						
						break;
					}
				}
			}
			
			this.associarNatOprCfop(entry.getValue(), entry.getKey());
		}
		
		return new ArrayList<NatOprCfopSPEDFiscal>(map.values());
	}
	
	private void associarNatOprCfop(NatOprCfopSPEDFiscal nat, String codigo) {
		nat.setCodigo(codigo);
		
		if (StringUtils.isNotEmpty(nat.getCfopCodigo())) {
			nat.setCfop(cfopService.findByCodigo(nat.getCfopCodigo()));
		}
		
		if (StringUtils.isNotEmpty(nat.getNaturezaOperacaoDescricao())) {
			nat.setNaturezaOperacao(naturezaoperacaoService.findNaturezaoperacaoConferenciaDeDados(nat.getNaturezaOperacaoDescricao()));
		}
	}
}
