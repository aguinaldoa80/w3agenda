package br.com.linkcom.sined.modulo.sistema.controller.process;

import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;


@Controller(path="/sistema/process/alteraempresaselecionada")
public class AlteraEmpresaSelecionadaProcess extends MultiActionController {
	
	public static final String ATTR_EMPRESASELECIONADA = "empresaSelecionada";
	public static final String ATTR_CLIENTESELECIONADO = "clienteSelecionado";
	
	@DefaultAction
	public void alteraEmpresaSelecionada(WebRequestContext request, Empresa empresa){
		if(empresa != null && empresa.getCdpessoa() != null){
			request.getSession().setAttribute(ATTR_EMPRESASELECIONADA, EmpresaService.getInstance().load(empresa));
			
			Cliente cliente = new Cliente(empresa.getCdpessoa());
			request.getSession().setAttribute(ATTR_CLIENTESELECIONADO, ClienteService.getInstance().load(cliente,"cliente.login, cliente.senha"));
		}
		else {
			request.getSession().removeAttribute(ATTR_EMPRESASELECIONADA);
			request.getSession().removeAttribute(ATTR_CLIENTESELECIONADO);
		}
	}
}
