package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.Input;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.enumeration.TipoArquivoAtualizacaoEstoque;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.InventarioService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MovimentacaoestoqueService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueCsvPadraoBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.bean.AtualizacaoEstoqueTxtColetorBean;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.AtualizacaoEstoqueFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/sistema/process/AtualizacaoEstoque", authorizationModule=ProcessAuthorizationModule.class)
public class AtualizacaoEstoqueProcess extends MultiActionController{

	private EmpresaService empresaService;
	private LocalarmazenagemService localarmazenagemService;
	private MaterialService materialService;
	private MovimentacaoestoqueService movimentacaoestoqueService;
	private ParametrogeralService parametrogeralService;
	private CentrocustoService centroCustoService;
	private InventarioService inventarioService;
	
	public void setMovimentacaoestoqueService(
			MovimentacaoestoqueService movimentacaoestoqueService) {
		this.movimentacaoestoqueService = movimentacaoestoqueService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setLocalarmazenagemService(
			LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setCentroCustoService(CentrocustoService centroCustoService) {
		this.centroCustoService = centroCustoService;
	}
	public void setInventarioService(InventarioService inventarioService) {
		this.inventarioService = inventarioService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, AtualizacaoEstoqueFiltro filtro){
		request.setAttribute("LOCAL_VENDA_OBRIGATORIO", parametrogeralService.getBoolean(Parametrogeral.LOCAL_VENDA_OBRIGATORIO));
		return new ModelAndView("process/atualizacaoEstoque", "filtro", filtro);
	}
	
	@Input("index")
	public ModelAndView buscarEstoque(WebRequestContext request, AtualizacaoEstoqueFiltro filtro){
		AtualizacaoEstoqueBean bean = new AtualizacaoEstoqueBean();
		
		if(filtro.getEmpresa() != null){
			bean.setEmpresa(empresaService.load(filtro.getEmpresa(), "empresa.cdpessoa, empresa.nome, empresa.razaosocial, empresa.nomefantasia"));
		}
		if(filtro.getLocalarmazenagem() != null){
			bean.setLocalarmazenagem(localarmazenagemService.load(filtro.getLocalarmazenagem(), "localarmazenagem.cdlocalarmazenagem, localarmazenagem.nome"));
		}
		bean.setDtestoque(filtro.getDtestoque());
		
		if (inventarioService.hasInventarioNotCanceladoPosteriorDataByEmpresaLocalProjeto(bean.getDtestoque(), bean.getEmpresa(), bean.getLocalarmazenagem(), null)) {
			request.addError("N�o � poss�vel realizar ajuste no estoque,  pois existe um invent�rio com o per�odo atual. Para realizar ajuste, � necess�rio cancelar o invent�rio.");
			return new ModelAndView("process/atualizacaoEstoque", "bean", bean);
		}
		
		List<AtualizacaoEstoqueMaterialBean> lista = new ArrayList<AtualizacaoEstoqueMaterialBean>();
		if(filtro.getGerarSomenteEntrada() == null || !filtro.getGerarSomenteEntrada()){
			lista = materialService.findForAtualizacaoEstoque(filtro, null, null);
		}
		if(filtro.getArquivo() != null){
			Map<String, List<Material>> mapaLotes = null;
			if(filtro.getTipoArquivoAtualizacaoEstoque() != null && filtro.getTipoArquivoAtualizacaoEstoque().equals(TipoArquivoAtualizacaoEstoque.TXT_COLETOR)){
				mapaLotes = this.ajustaListaByArquivoTxtColetor(lista, filtro.getArquivo().getContent(), filtro);
			} else if(filtro.getTipoArquivoAtualizacaoEstoque() != null && filtro.getTipoArquivoAtualizacaoEstoque().equals(TipoArquivoAtualizacaoEstoque.CSV_PADRAO)){
				mapaLotes = this.ajustaListaByArquivoCsvPadrao(lista, filtro.getArquivo().getContent(), filtro);
			}
			request.getSession().setAttribute("MAPA_LOTES_ATUALIZACAO_ESTOQUE", mapaLotes);
		}
		bean.setListaMaterial(lista);
		bean.setGerarSomenteEntrada(filtro.getGerarSomenteEntrada() != null && filtro.getGerarSomenteEntrada());
		bean.setConsiderarLotes(filtro.getConsiderarLotes() != null && filtro.getConsiderarLotes());
		
		request.setAttribute("listaCentroCusto", centroCustoService.findAtivos());
		return new ModelAndView("process/atualizacaoEstoqueListagem", "bean", bean);
	}
	
	@SuppressWarnings("unchecked")
	public void saveAtualizacaoEstoque(WebRequestContext request, AtualizacaoEstoqueBean bean){
		try {
			Object obj = request.getSession().getAttribute("MAPA_LOTES_ATUALIZACAO_ESTOQUE");
			Map<String, List<Material>> mapaLotes = null;
			if(obj != null){
				mapaLotes = (Map<String, List<Material>>) obj;
			}
			movimentacaoestoqueService.atualizarEstoque(bean, mapaLotes);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na atualiza��o do estoque: " + e.getMessage());
		}
	}
	
	public ModelAndView finalizacaoAtualizacaoEstoque(WebRequestContext request){
		request.getSession().removeAttribute("MAPA_LOTES_ATUALIZACAO_ESTOQUE");
		request.addMessage("Atualiza��o de estoque finalizada com sucesso.");
		return sendRedirectToAction("index");
	}
	
	private Map<String, List<Material>> ajustaListaByArquivoCsvPadrao(List<AtualizacaoEstoqueMaterialBean> lista, byte[] content, AtualizacaoEstoqueFiltro filtro) {
		Boolean zerarProdutosNaoEncontrados = filtro.getZerarProdutosNaoEncontrados();
		Boolean gerarSomenteEntrada = filtro.getGerarSomenteEntrada();
		Boolean mostrarSomenteProdutosEncontrados = filtro.getMostrarSomenteProdutosEncontrados();
		Boolean considerarLotes = filtro.getConsiderarLotes() != null && filtro.getConsiderarLotes();
		
		String arquivoStr = new String(content);
		String[] linhas = arquivoStr.split("\\r?\\n");
		StringBuilder whereInMaterial = new StringBuilder();
		
		List<AtualizacaoEstoqueCsvPadraoBean> listaColetor = new ArrayList<AtualizacaoEstoqueCsvPadraoBean>();
		for (String linha : linhas) {
			if(linha.trim().equals("")) continue;
			String[] campos = linha.split(";");
			
			if(campos.length < 5)
				throw new SinedException("Problema no layout do arquivo CSV.");
			
			try {
				Integer.parseInt(campos[0]);
			} catch (Exception e) {
				continue;
			}
			
			try{
				AtualizacaoEstoqueCsvPadraoBean it = new AtualizacaoEstoqueCsvPadraoBean();
				it.setCdmaterial(Integer.parseInt(campos[0]));
				try{
				it.setQuantidade(Double.parseDouble(campos[4].replace(".", "").replace(",", ".")));
				}catch (Exception e) {
					throw e;
				}
				if(considerarLotes){
					it.setLote(campos.length > 5 ? campos[5] : null);
					it.setCnpj(campos.length > 6 ? campos[6] : null);
				}
				if(!listaColetor.contains(it)){
					if(!whereInMaterial.toString().equals("")) whereInMaterial.append(",");
					whereInMaterial.append(it.getCdmaterial());
				}
				listaColetor.add(it);
			} catch (Exception e) {
				e.printStackTrace();
				throw new SinedException("Erro no processamento do arquivo coletor: " + e.getMessage());
			}
		}
		
		if(gerarSomenteEntrada != null && gerarSomenteEntrada && !whereInMaterial.toString().equals("")){
			List<AtualizacaoEstoqueMaterialBean> listaNova = materialService.findForAtualizacaoEstoque(filtro, whereInMaterial.toString(), null);
			if(SinedUtil.isListNotEmpty(listaNova)){
				lista.addAll(listaNova);
				ajustaListaCsvComLote(lista, listaColetor);
			}
		}
		
		Map<String, List<Material>> mapaLotes = new HashMap<String, List<Material>>();
		if(SinedUtil.isListNotEmpty(lista)){
			Iterator<AtualizacaoEstoqueMaterialBean> it = lista.iterator();
			while (it.hasNext()) {
				AtualizacaoEstoqueMaterialBean bean = (AtualizacaoEstoqueMaterialBean) it.next();
				if(bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null){
					AtualizacaoEstoqueCsvPadraoBean aux = new AtualizacaoEstoqueCsvPadraoBean();
					aux.setCdmaterial(bean.getMaterial().getCdmaterial());
					aux.setLote(bean.getLote());
					aux.setCnpj(bean.getCnpj());
					
					if(listaColetor.contains(aux)){
						aux = listaColetor.get(listaColetor.indexOf(aux));
						
						if(bean.getQtdeAtual() == null){
							bean.setQtdeAtual(0.0);
						}
						
						if(gerarSomenteEntrada != null && gerarSomenteEntrada){
							bean.setQtdeAtual((aux.getQuantidade() == null ? 0.0 : aux.getQuantidade())+bean.getQtdeAtual());
						} else{
							bean.setQtdeAtual(aux.getQuantidade());
						}
						
						if(aux.getLote() != null && !aux.getLote().trim().isEmpty() && aux.getCnpj() != null && !aux.getCnpj().trim().isEmpty()){
							if(mapaLotes.containsKey(aux.getKey())){
								mapaLotes.get(aux.getKey()).add(bean.getMaterial());
							} else{
								List<Material> materiais = new ListSet<Material>(Material.class);
								materiais.add(bean.getMaterial());
								mapaLotes.put(aux.getKey(), materiais);
							}
						}
						
						bean.setAtualizado(Boolean.TRUE);
					} else if(mostrarSomenteProdutosEncontrados != null && mostrarSomenteProdutosEncontrados){
						it.remove();
					} else if(zerarProdutosNaoEncontrados != null && zerarProdutosNaoEncontrados){
						bean.setQtdeAtual(0d);
					}
				}
			}
		}
		return mapaLotes;
	}
	
	private void ajustaListaCsvComLote(List<AtualizacaoEstoqueMaterialBean> lista, List<AtualizacaoEstoqueCsvPadraoBean> listaColetor) {
		if(SinedUtil.isListNotEmpty(lista) && SinedUtil.isListNotEmpty(listaColetor)){
			List<Integer> listaMaterialSemLote = new ArrayList<Integer>();
			for(AtualizacaoEstoqueCsvPadraoBean aux : listaColetor){
				if(aux.getCdmaterial() != null && StringUtils.isNotEmpty(aux.getLote())){
					boolean existeMaterialLote = false;
					AtualizacaoEstoqueMaterialBean itemIncluir = null;
					for(AtualizacaoEstoqueMaterialBean bean : lista){
						if(bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null &&
								aux.getCdmaterial().equals(bean.getMaterial().getCdmaterial())){
							itemIncluir = bean;
							if(bean.getLote() != null && bean.getLote().equals(aux.getLote())){
								existeMaterialLote = true;
								break;
							}
						}
					}
					if(!existeMaterialLote){
						lista.add(new AtualizacaoEstoqueMaterialBean(itemIncluir, aux.getLote(), aux.getCnpj()));
					}
				}else {
					listaMaterialSemLote.add(aux.getCdmaterial());
				}
			}
			
			if(SinedUtil.isListNotEmpty(listaMaterialSemLote)){
				for(int i = 0; i < lista.size(); i++){
					AtualizacaoEstoqueMaterialBean bean = lista.get(i);
					if(bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null && 
							(bean.getLote() == null || "".equals(bean.getLote())) &&
							!listaMaterialSemLote.contains(bean.getMaterial().getCdmaterial())){
						lista.remove(i);
						i--;
					}
				}
			}else {
				for(int i = 0; i < lista.size(); i++){
					AtualizacaoEstoqueMaterialBean bean = lista.get(i);
					if(bean.getMaterial() != null && bean.getMaterial().getCdmaterial() != null && 
							(bean.getLote() == null || "".equals(bean.getLote()))){
						lista.remove(i);
						i--;
					}
				}
			}
		}
	}
	
	private void ajustaListaTxtComLote(List<AtualizacaoEstoqueMaterialBean> lista, List<AtualizacaoEstoqueTxtColetorBean> listaColetor) {
		if(SinedUtil.isListNotEmpty(lista) && SinedUtil.isListNotEmpty(listaColetor)){
			List<String> listaMaterialSemLote = new ArrayList<String>();
			for(AtualizacaoEstoqueTxtColetorBean aux : listaColetor){
				if(aux.getCodigobarras() != null && StringUtils.isNotEmpty(aux.getLote())){
					boolean existeMaterialLote = false;
					AtualizacaoEstoqueMaterialBean itemIncluir = null;
					for(AtualizacaoEstoqueMaterialBean bean : lista){
						if(bean.getMaterial() != null && StringUtils.isNotEmpty(bean.getMaterial().getCodigobarras()) &&
								aux.getCodigobarras().equals(bean.getMaterial().getCodigobarras())){
							itemIncluir = bean;
							if(bean.getLote() != null && bean.getLote().equals(aux.getLote())){
								existeMaterialLote = true;
								break;
							}
						}
					}
					if(!existeMaterialLote){
						lista.add(new AtualizacaoEstoqueMaterialBean(itemIncluir, aux.getLote(), aux.getCnpj()));
					}
				}else {
					listaMaterialSemLote.add(aux.getCodigobarras());
				}
			}
			
			if(SinedUtil.isListNotEmpty(listaMaterialSemLote)){
				for(int i = 0; i < lista.size(); i++){
					AtualizacaoEstoqueMaterialBean bean = lista.get(i);
					if(bean.getMaterial() != null && StringUtils.isNotEmpty(bean.getMaterial().getCodigobarras()) && 
							(bean.getLote() == null || "".equals(bean.getLote())) &&
							!listaMaterialSemLote.contains(bean.getMaterial().getCodigobarras())){
						lista.remove(i);
						i--;
					}
				}
			}else {
				for(int i = 0; i < lista.size(); i++){
					AtualizacaoEstoqueMaterialBean bean = lista.get(i);
					if(bean.getMaterial() != null && StringUtils.isNotEmpty(bean.getMaterial().getCodigobarras()) && 
							(bean.getLote() == null || "".equals(bean.getLote()))){
						lista.remove(i);
						i--;
					}
				}
			}
		}
		
	}
	
	private Map<String, List<Material>> ajustaListaByArquivoTxtColetor(List<AtualizacaoEstoqueMaterialBean> lista, byte[] content, AtualizacaoEstoqueFiltro filtro) {
		Boolean zerarProdutosNaoEncontrados = filtro.getZerarProdutosNaoEncontrados();
		Boolean gerarSomenteEntrada = filtro.getGerarSomenteEntrada();
		Boolean mostrarSomenteProdutosEncontrados = filtro.getMostrarSomenteProdutosEncontrados();
		Boolean considerarLotes = filtro.getConsiderarLotes() != null && filtro.getConsiderarLotes();
		
		String arquivoStr = new String(content);
		String[] linhas = arquivoStr.split("\\r?\\n");
		StringBuilder whereInCodigobarras = new StringBuilder();
		
		List<AtualizacaoEstoqueTxtColetorBean> listaColetor = new ArrayList<AtualizacaoEstoqueTxtColetorBean>();
		for (String linha : linhas) {
			if(linha.trim().equals("")) continue;
			
			String[] campos = linha.split(";");
			if(campos.length < 2)
				throw new SinedException("Problema no layout do arquivo coletor.");
			
			try{
				AtualizacaoEstoqueTxtColetorBean it = new AtualizacaoEstoqueTxtColetorBean();
				it.setCodigobarras(campos[0]);
				it.setQuantidade(Double.parseDouble(campos[1]));
				if(considerarLotes){
					it.setLote(campos[2]);
					it.setCnpj(campos[3]);
				}
				if(!listaColetor.contains(it)){
					if(!whereInCodigobarras.toString().equals("")) whereInCodigobarras.append(",");
					whereInCodigobarras.append("'" + it.getCodigobarras() + "'");
				}
				listaColetor.add(it);
			} catch (Exception e) {
				e.printStackTrace();
				throw new SinedException("Erro no processamento do arquivo coletor: " + e.getMessage());
			}
		}
		
		if(gerarSomenteEntrada != null && gerarSomenteEntrada && !whereInCodigobarras.toString().equals("")){
			List<AtualizacaoEstoqueMaterialBean> listaNova = materialService.findForAtualizacaoEstoque(filtro, null, whereInCodigobarras.toString());
			if(SinedUtil.isListNotEmpty(listaNova)){
				lista.addAll(listaNova);
				ajustaListaTxtComLote(lista, listaColetor);
			}
		}
		
		Map<String, List<Material>> mapaLotes = new HashMap<String, List<Material>>();

		for (Iterator<AtualizacaoEstoqueMaterialBean> iterator = lista.iterator(); iterator.hasNext();) {
			AtualizacaoEstoqueMaterialBean bean = (AtualizacaoEstoqueMaterialBean) iterator.next();
			
			if(bean.getMaterial() != null && bean.getMaterial().getCodigobarras() != null && !bean.getMaterial().getCodigobarras().equals("")){
				AtualizacaoEstoqueTxtColetorBean aux = new AtualizacaoEstoqueTxtColetorBean();
				aux.setCodigobarras(bean.getMaterial().getCodigobarras());
				aux.setLote(bean.getLote());
				
				if(listaColetor.contains(aux)){
					aux = listaColetor.get(listaColetor.indexOf(aux));
					
					if(bean.getQtdeAtual() == null){
						bean.setQtdeAtual(0.0);
					}
					
					if(gerarSomenteEntrada != null && gerarSomenteEntrada){
						bean.setQtdeAtual((aux.getQuantidade() == null ? 0.0 : aux.getQuantidade())+bean.getQtdeAtual());
					} else{
						bean.setQtdeAtual(aux.getQuantidade());
					}
					
					if(aux.getLote() != null && !aux.getLote().trim().isEmpty() && aux.getCnpj() != null && !aux.getCnpj().trim().isEmpty()){
						if(mapaLotes.containsKey(aux.getKey())){
							mapaLotes.get(aux.getKey()).add(bean.getMaterial());
						} else{
							List<Material> materiais = new ListSet<Material>(Material.class);
							materiais.add(bean.getMaterial());
							mapaLotes.put(aux.getKey(), materiais);
						}
					}
					
					bean.setAtualizado(Boolean.TRUE);
				} else if(mostrarSomenteProdutosEncontrados != null && mostrarSomenteProdutosEncontrados){
					iterator.remove();
				} else if(zerarProdutosNaoEncontrados != null && zerarProdutosNaoEncontrados){
					bean.setQtdeAtual(0d);
				}
			} else if(mostrarSomenteProdutosEncontrados != null && mostrarSomenteProdutosEncontrados){
				iterator.remove();
			} else if(zerarProdutosNaoEncontrados != null && zerarProdutosNaoEncontrados){
				bean.setQtdeAtual(0d);
			}
		}
		
		return mapaLotes;
	}
}
