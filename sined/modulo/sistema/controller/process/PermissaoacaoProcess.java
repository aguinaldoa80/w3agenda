package br.com.linkcom.sined.modulo.sistema.controller.process;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.Bean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.DefaultAction;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.geral.bean.Acaopapel;
import br.com.linkcom.sined.geral.bean.enumeration.Modulo;
import br.com.linkcom.sined.geral.service.AcaoService;
import br.com.linkcom.sined.geral.service.AcaopapelService;
import br.com.linkcom.sined.modulo.sistema.controller.process.filter.PermissaoacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;

/**
 * 
 * @author Pedro Gon�alves
 */
@Bean
@Controller(path="/sistema/process/permissaoacao", authorizationModule=ProcessAuthorizationModule.class)
public class PermissaoacaoProcess extends MultiActionController {
	private AcaopapelService acaopapelService;
	private AcaoService acaoService;
	
	public void setAcaopapelService(AcaopapelService acaopapelService) {
		this.acaopapelService = acaopapelService;
	}
	
	public void setAcaoService(AcaoService acaoService) {
		this.acaoService = acaoService;
	}
	
	/**
	 * Despacha para o jsp de permissao acao
	 * @param requestContext
	 * @param filtro
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@DefaultAction
	public ModelAndView doFilter(WebRequestContext requestContext, PermissaoacaoFiltro filtro){
		LinkedHashMap<Modulo, List<Acaopapel>> mapaAcaopapel = new LinkedHashMap<Modulo, List<Acaopapel>>();
		if(filtro.getPapel() != null){
			List<Acaopapel> findAllBy = acaopapelService.findAllBy(filtro.getPapel());
			List<Acao> acaoByPapel = (List<Acao>) CollectionsUtil.getListProperty(findAllBy, "acao");
			List<Acao> findAll = acaoService.findAll();
			
			if(findAllBy == null || findAllBy.size() != findAll.size()){
				for (Acao acao : findAll) {
					if(!acaoByPapel.contains(acao)){
						Acaopapel acaopapel = new Acaopapel();
						acaopapel.setAcao(acao);
						findAllBy.add(acaopapel);
					}
				}

				Collections.sort(findAllBy, new Comparator<Acaopapel>() {
					@Override
					public int compare(Acaopapel o1, Acaopapel o2) {
						String moduloAcao1 = (o1.getAcao().getModulo() != null? o1.getAcao().getModulo().toString(): "ZZZZZZ") + o1.getAcao().getDescricao();
						String moduloAcao2 = (o2.getAcao().getModulo() != null? o2.getAcao().getModulo().toString(): "ZZZZZZ") + o2.getAcao().getDescricao();
						return (moduloAcao1)
									.compareTo(moduloAcao2);
					}
				});
			}
			filtro.setListaAcaoPapel(new ListSet<Acaopapel>(Acaopapel.class,findAllBy));
		}
		
		return new ModelAndView("process/permissaoacao","filtro",filtro);
	}
	
	/**
	 * 
	 * @param requestContext
	 * @param filtro
	 * @return
	 */
	public ModelAndView doSalvar(WebRequestContext requestContext, PermissaoacaoFiltro filtro){
		Set<Acaopapel> listaAcaoPapel = filtro.getListaAcaoPapel();
		for (Iterator<Acaopapel> iterator = listaAcaoPapel.iterator(); iterator.hasNext();) {
			Acaopapel acaopapel = iterator.next();
			if(!acaopapel.getPermitido() && acaopapel.getCdacaopapel() != null){
				acaopapelService.delete(acaopapel);
			} else if(acaopapel.getPermitido()){
				acaopapel.setPapel(filtro.getPapel());
				acaopapel.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
				acaopapel.setDtaltera(new Timestamp(System.currentTimeMillis()));
				acaopapelService.saveOrUpdate(acaopapel);
			}
				
		}
		requestContext.getSession().setAttribute(SinedUtil.SINED_CACHE_ACAOPERMISSION, null);
		requestContext.addMessage("Registros atualizados com sucesso!");
		return sendRedirectToAction("doFilter");
	}
}
