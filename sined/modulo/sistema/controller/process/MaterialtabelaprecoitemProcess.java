package br.com.linkcom.sined.modulo.sistema.controller.process;


import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(
		path="/sistema/process/Materialtabelaprecoitem",
		authorizationModule=ProcessAuthorizationModule.class
)
public class MaterialtabelaprecoitemProcess extends MultiActionController{
	
	private MaterialService materialService;
	private UnidademedidaService unidademedidaService;
	private ParametrogeralService parametrogeralService;
	private ComissionamentoService comissionamentoService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {
		this.comissionamentoService = comissionamentoService;
	}
	
	@Action("criar")
	public ModelAndView criar(WebRequestContext request, Materialtabelaprecoitem materialtabelaprecoitem){
		materialtabelaprecoitem.setCdmaterialtabelaprecoParam(request.getParameter("cdmaterialtabelaprecoParam"));
		String tipocalculo = request.getParameter("tipocalculo");
		request.setAttribute("listaUnidademedida", new ArrayList<Unidademedida>());
		request.setAttribute("tipocalculo", tipocalculo);
		request.setAttribute("IDENTIFICADOR_FABRICANTE_TABELA_PRECO", parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_FABRICANTE_TABELA_PRECO));
		request.setAttribute("listaComissionamento", comissionamentoService.findForTabelapreco(materialtabelaprecoitem.getComissionamento()));
		return new ModelAndView("direct:process/popup/criaMaterialtabelaprecoitem","materialtabelaprecoitem",materialtabelaprecoitem);
	}
	
	@SuppressWarnings("unchecked")
	public void excluir(WebRequestContext request){
		try{
			List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = null;
			Object object = request.getParameter("posicaoLista");
			if (object != null) {
				String parameter = object.toString();
				Integer index = Integer.parseInt(parameter)-1;
				Object attr = request.getSession().getAttribute("listaMaterialtabelaprecoitem" + request.getParameter("cdmaterialtabelaprecoParam"));
				if (attr != null) {
					listaMaterialtabelaprecoitem = (List<Materialtabelaprecoitem>) attr;
					if (listaMaterialtabelaprecoitem != null && listaMaterialtabelaprecoitem.size() > index)
						listaMaterialtabelaprecoitem.remove(index.intValue());
				}
			}
			request.getSession().setAttribute("listaMaterialtabelaprecoitem" + request.getParameter("cdmaterialtabelaprecoParam"), listaMaterialtabelaprecoitem);
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	public void limparLista(WebRequestContext request){
		try{
			List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ArrayList<Materialtabelaprecoitem>();
			request.getSession().setAttribute("listaMaterialtabelaprecoitem" + request.getParameter("cdmaterialtabelaprecoParam"), listaMaterialtabelaprecoitem);
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void salva(WebRequestContext request, Materialtabelaprecoitem materialtabelaprecoitem){
		Object attribute = request.getSession().getAttribute("listaMaterialtabelaprecoitem" + (materialtabelaprecoitem.getCdmaterialtabelaprecoParam() != null ? materialtabelaprecoitem.getCdmaterialtabelaprecoParam() : ""));
		List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = null;
		
		if(materialtabelaprecoitem.getMaterial() != null){
			if(materialtabelaprecoitem.getMaterial().getIdentificacao() == null || materialtabelaprecoitem.getMaterial().getNome() == null){
				materialtabelaprecoitem.setMaterial(materialService.loadForOrdemcompra(materialtabelaprecoitem.getMaterial()));
			}
		}
		if(materialtabelaprecoitem.getUnidademedida() != null){
			if(materialtabelaprecoitem.getUnidademedida().getSimbolo() == null){
				materialtabelaprecoitem.setUnidademedida(unidademedidaService.load(materialtabelaprecoitem.getUnidademedida(), "unidademedida.cdunidademedida, unidademedida.nome"));
			}
		}
		if(materialtabelaprecoitem.getComissionamento() != null){
			if(materialtabelaprecoitem.getComissionamento().getNome() == null){
				materialtabelaprecoitem.setComissionamento(comissionamentoService.load(materialtabelaprecoitem.getComissionamento(), "comissionamento.cdcomissionamento, comissionamento.nome"));
			}
		}
		
		if (attribute == null) {
			listaMaterialtabelaprecoitem = new ArrayList<Materialtabelaprecoitem>();
			listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
			
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>" +
										"parent.criarMaterialtabelaprecoitem(" +
										"'"+escapeSingleQuotes(materialtabelaprecoitem.getMaterial().getAutocompleteDescription())+
										"','"+(materialtabelaprecoitem.getValor() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValor()) : "")+
										"','"+(materialtabelaprecoitem.getPercentualdesconto() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getPercentualdesconto()) : "")+
										"','"+(materialtabelaprecoitem.getValorvendaminimo() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValorvendaminimo()) : "")+
										"','"+(materialtabelaprecoitem.getValorvendamaximo() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValorvendamaximo()) : "")+
										"',"+materialtabelaprecoitem.getMaterial().getCdmaterial().toString()+
										",'"+(materialtabelaprecoitem.getUnidademedida() != null ? escapeSingleQuotes(materialtabelaprecoitem.getUnidademedida().getNome()) : "") +
										"',"+(materialtabelaprecoitem.getUnidademedida() != null ? materialtabelaprecoitem.getUnidademedida().getCdunidademedida().toString() : "") +
										",'"+(materialtabelaprecoitem.getIdentificadorespecifico() != null ? materialtabelaprecoitem.getIdentificadorespecifico() : "") +
										"','"+(materialtabelaprecoitem.getIdentificadorfabricante() != null ? materialtabelaprecoitem.getIdentificadorfabricante() : "") +
										"','"+(materialtabelaprecoitem.getComissionamento() != null ? escapeSingleQuotes(materialtabelaprecoitem.getComissionamento().getNome()) : "") +
										"','"+(materialtabelaprecoitem.getComissionamento() != null ? materialtabelaprecoitem.getComissionamento().getCdcomissionamento().toString() : "") +
										"');" +
										"parent.$.akModalRemove(true);" +
										"</script>");
		} else {
			listaMaterialtabelaprecoitem = (List<Materialtabelaprecoitem>)attribute;
			
			if (materialtabelaprecoitem.getIndexlista() != null) {
				listaMaterialtabelaprecoitem.set(materialtabelaprecoitem.getIndexlista(), materialtabelaprecoitem);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"parent.editarMaterialtabelaprecoitem(" +
											"'"+materialtabelaprecoitem.getIndexlista()+
											"','"+escapeSingleQuotes(materialtabelaprecoitem.getMaterial().getAutocompleteDescription())+
											"','"+(materialtabelaprecoitem.getValor() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValor()) : "")+
											"','"+(materialtabelaprecoitem.getPercentualdesconto() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getPercentualdesconto()) : "")+
											"','"+(materialtabelaprecoitem.getValorvendaminimo() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValorvendaminimo()) : "")+
											"','"+(materialtabelaprecoitem.getValorvendamaximo() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValorvendamaximo()) : "")+
											"',"+materialtabelaprecoitem.getMaterial().getCdmaterial().toString()+
											",'"+(materialtabelaprecoitem.getUnidademedida() != null ? escapeSingleQuotes(materialtabelaprecoitem.getUnidademedida().getNome()) : "") +
											"',"+(materialtabelaprecoitem.getUnidademedida() != null ? materialtabelaprecoitem.getUnidademedida().getCdunidademedida().toString() : "''") +
											",'"+(materialtabelaprecoitem.getIdentificadorespecifico() != null ? materialtabelaprecoitem.getIdentificadorespecifico() : "") +
											"','"+(materialtabelaprecoitem.getIdentificadorfabricante() != null ? materialtabelaprecoitem.getIdentificadorfabricante() : "") +
											"','"+(materialtabelaprecoitem.getComissionamento() != null ? escapeSingleQuotes(materialtabelaprecoitem.getComissionamento().getNome()) : "") +
											"','"+(materialtabelaprecoitem.getComissionamento() != null ? materialtabelaprecoitem.getComissionamento().getCdcomissionamento().toString() : "") +
											"');" +
											"parent.$.akModalRemove(true);" +
											"</script>");
			} else {
				listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
				
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>" +
											"parent.criarMaterialtabelaprecoitem(" +
											"'"+escapeSingleQuotes(materialtabelaprecoitem.getMaterial().getAutocompleteDescription())+
											"','"+(materialtabelaprecoitem.getValor() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValor()) : "")+
											"','"+(materialtabelaprecoitem.getPercentualdesconto() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getPercentualdesconto()) : "")+
											"','"+(materialtabelaprecoitem.getValorvendaminimo() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValorvendaminimo()) : "")+
											"','"+(materialtabelaprecoitem.getValorvendamaximo() != null ? SinedUtil.descriptionDecimal(materialtabelaprecoitem.getValorvendamaximo()) : "")+
											"',"+materialtabelaprecoitem.getMaterial().getCdmaterial().toString()+
											",'"+(materialtabelaprecoitem.getUnidademedida() != null ? escapeSingleQuotes(materialtabelaprecoitem.getUnidademedida().getNome()) : "") +
											"',"+(materialtabelaprecoitem.getUnidademedida() != null ? materialtabelaprecoitem.getUnidademedida().getCdunidademedida().toString() : "''") +
											",'"+(materialtabelaprecoitem.getIdentificadorespecifico() != null ? materialtabelaprecoitem.getIdentificadorespecifico() : "") +
											"','"+(materialtabelaprecoitem.getIdentificadorfabricante() != null ? materialtabelaprecoitem.getIdentificadorfabricante() : "") +
											"','"+(materialtabelaprecoitem.getComissionamento() != null ? escapeSingleQuotes(materialtabelaprecoitem.getComissionamento().getNome()) : "") +
											"','"+(materialtabelaprecoitem.getComissionamento() != null ? materialtabelaprecoitem.getComissionamento().getCdcomissionamento().toString() : "") +
											"');" +
											"parent.$.akModalRemove(true);" +
											"</script>");
			}
		}	
		
		request.getSession().setAttribute("listaMaterialtabelaprecoitem" + (materialtabelaprecoitem.getCdmaterialtabelaprecoParam() != null ? materialtabelaprecoitem.getCdmaterialtabelaprecoParam() : ""), listaMaterialtabelaprecoitem);
	}
	
	private String escapeSingleQuotes(String opValue) {
		if(opValue==null) return null;
		return opValue.replaceAll("\\\\", "\\\\\\\\").replaceAll("\'", "\\\\'");
	}
	
	/**
	 * Edita a intera��o da lista na entrada de prospec��o; 
	 * 
	 * @see br.com.linkcom.sined.geral.service.InteracaoService#carregaIndexLista
	 * @param request
	 * @return
	 * @author Rodrigo Freitas
	 */
	@SuppressWarnings("unchecked")
	@Action("editar")
	public ModelAndView editar(WebRequestContext request){
		String tipocalculo = request.getParameter("tipocalculo");
		request.setAttribute("tipocalculo", tipocalculo);		
		Materialtabelaprecoitem materialtabelaprecoitem = null;
		List<Unidademedida> listaUnidademedida = new ArrayList<Unidademedida>();
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			Integer index = Integer.parseInt(parameter)-1;
			String paramId = request.getParameter("cdmaterialtabelaprecoParam");
			Object attr = request.getSession().getAttribute("listaMaterialtabelaprecoitem" + (paramId != null ? paramId : ""));
			List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = null;
			if (attr != null) {
				listaMaterialtabelaprecoitem = (List<Materialtabelaprecoitem>) attr;
				materialtabelaprecoitem = listaMaterialtabelaprecoitem.get(index);
				materialtabelaprecoitem.setIndexlista(index);
				materialtabelaprecoitem.setCdmaterialtabelaprecoParam(request.getParameter("cdmaterialtabelaprecoParam"));
				
				if(materialtabelaprecoitem.getMaterial() != null){
					listaUnidademedida = unidademedidaService.findUnidademedidarelacionado(materialtabelaprecoitem.getMaterial());
				}
			}
			
		}	
		
		request.setAttribute("listaUnidademedida", listaUnidademedida);
		request.setAttribute("IDENTIFICADOR_FABRICANTE_TABELA_PRECO", parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_FABRICANTE_TABELA_PRECO));
		request.setAttribute("listaComissionamento", comissionamentoService.findForTabelapreco(materialtabelaprecoitem.getComissionamento()));
		return new ModelAndView("direct:process/popup/criaMaterialtabelaprecoitem", "materialtabelaprecoitem", materialtabelaprecoitem);
	}
	
	
	/**
	 * M�todo para consultar o item sem ter que editar
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	@SuppressWarnings("unchecked")
	@Action("consultar")
	public ModelAndView consultar(WebRequestContext request){
		String tipocalculo = request.getParameter("tipocalculo");
		request.setAttribute("tipocalculo", tipocalculo);
		Materialtabelaprecoitem materialtabelaprecoitem = null;
		Object object = request.getParameter("posicaoLista");
		if (object != null) {
			String parameter = object.toString();
			Integer index = Integer.parseInt(parameter);
			String paramId = request.getParameter("cdmaterialtabelaprecoParam");
			Object attr = request.getSession().getAttribute("listaMaterialtabelaprecoitem" + (paramId != null ? paramId : ""));
			List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = null;
			if (attr != null) {
				listaMaterialtabelaprecoitem = (List<Materialtabelaprecoitem>) attr;
				materialtabelaprecoitem = listaMaterialtabelaprecoitem.get(index);
				materialtabelaprecoitem.setIndexlista(index);
				materialtabelaprecoitem.setCdmaterialtabelaprecoParam(request.getParameter("cdmaterialtabelaprecoParam"));
			}
		}	
		request.setAttribute("consultaitemmaterialtabelaprecoitem", true);
		return new ModelAndView("direct:process/popup/criaMaterialtabelaprecoitem", "materialtabelaprecoitem", materialtabelaprecoitem);
	}
}
