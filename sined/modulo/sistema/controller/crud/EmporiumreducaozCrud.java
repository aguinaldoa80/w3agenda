package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Emporiumreducaoz;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.EmporiumpdvService;
import br.com.linkcom.sined.geral.service.EmporiumreducaozService;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmporiumreducaozFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Emporiumreducaoz", authorizationModule=CrudAuthorizationModule.class)
public class EmporiumreducaozCrud extends CrudControllerSined<EmporiumreducaozFiltro, Emporiumreducaoz, Emporiumreducaoz> {

	private EmporiumreducaozService emporiumreducaozService;
	private EmporiumpdvService emporiumpdvService;
	private EmporiumvendaService emporiumvendaService;
	
	public void setEmporiumvendaService(
			EmporiumvendaService emporiumvendaService) {
		this.emporiumvendaService = emporiumvendaService;
	}
	public void setEmporiumpdvService(EmporiumpdvService emporiumpdvService) {
		this.emporiumpdvService = emporiumpdvService;
	}
	public void setEmporiumreducaozService(
			EmporiumreducaozService emporiumreducaozService) {
		this.emporiumreducaozService = emporiumreducaozService;
	}
	
	public ModelAndView getReducoesZFaltantes(WebRequestContext request, EmporiumreducaozFiltro filtro){
		Date dataInicio = filtro.getData1();
		Date dataFim = filtro.getData2();
		Empresa empresa = filtro.getEmpresa();
		
		if(dataInicio == null || dataFim == null){
			throw new SinedException("Favor informar os parāmetros data1 e data2 como o intervalo a ser pesquisado.");
		}
		
		List<Emporiumreducaoz> lista = emporiumreducaozService.findForSped(null, dataInicio, dataFim);
		List<Emporiumpdv> listaPdv = emporiumpdvService.findByEmpresa(empresa);
		
		StringBuilder sb = new StringBuilder();
		sb.append("Data;PDV;\n");
		
		while(SinedDateUtils.beforeOrEqualIgnoreHour(dataInicio, dataFim)){
			for (Emporiumpdv emporiumpdv : listaPdv) {
				boolean achou = false;
				for (Emporiumreducaoz emporiumreducaoz : lista) {
					if(emporiumreducaoz.getEmporiumpdv() != null && emporiumpdv.getCdemporiumpdv().equals(emporiumreducaoz.getEmporiumpdv().getCdemporiumpdv()) && 
							SinedDateUtils.equalsIgnoreHour(dataInicio, emporiumreducaoz.getData())){
						achou = true;
						break;
					}
				}
				if(!achou){
					if(emporiumvendaService.haveTicketDia(emporiumpdv, dataInicio)){
						sb
						.append(SinedDateUtils.toString(dataInicio))
						.append(";")
						.append(emporiumpdv.getCodigoemporium())
						.append(";\n");
					}
				}
			}
			dataInicio = SinedDateUtils.addDiasData(dataInicio, 1);
		}
		
		Resource resource = new Resource("text/csv","reducoeszfaltantes_" + SinedUtil.datePatternForReport() + ".csv", sb.toString().getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
	
}
