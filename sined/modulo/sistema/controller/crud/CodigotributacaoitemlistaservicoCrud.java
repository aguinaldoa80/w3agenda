package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Codigotributacaoitemlistaservico;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CodigotributacaoitemlistaservicoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Codigotributacaoitemlistaservico", authorizationModule=CrudAuthorizationModule.class)
public class CodigotributacaoitemlistaservicoCrud extends CrudControllerSined<CodigotributacaoitemlistaservicoFiltro, Codigotributacaoitemlistaservico, Codigotributacaoitemlistaservico> {

	@Override
	protected void entrada(WebRequestContext request, Codigotributacaoitemlistaservico form) throws Exception {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Codigotributacaoitemlistaservico bean) throws Exception {
		throw new SinedException("A��o n�o permitida");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Codigotributacaoitemlistaservico bean) throws Exception {
		throw new SinedException("A��o n�o permitida");
	}

	@Override
	protected void listagem(WebRequestContext request, CodigotributacaoitemlistaservicoFiltro filtro) throws Exception {
		request.setAttribute("INSELECTONE", Boolean.TRUE);
	}
}
