package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path="/sistema/crud/Tipotaxa",authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class TipotaxaCrud extends CrudControllerSined<FiltroListagemSined, Tipotaxa, Tipotaxa>{
	
	private CentrocustoService centrocustoService;
	
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Tipotaxa form)
			throws Exception {
		List<Centrocusto> listaCentroCusto = centrocustoService.findAtivos(form.getCentrocusto());
		request.setAttribute("listaCentroCusto", listaCentroCusto);
	}

}
