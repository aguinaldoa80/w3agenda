package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Tabelaimpostoitem;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TabelaimpostoitemFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Tabelaimpostoitem"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"codigo", "extipi", "tipo", "descricao", "nacionalfederal", "importadofederal", "estadual", "municipal"})
public class TabelaimpostoitemCrud extends CrudControllerSined<TabelaimpostoitemFiltro, Tabelaimpostoitem, Tabelaimpostoitem> {	
	
	@Override
	protected void entrada(WebRequestContext request, Tabelaimpostoitem form) throws Exception {
		throw new SinedException("A��o n�o permitida");
	}
	
}
