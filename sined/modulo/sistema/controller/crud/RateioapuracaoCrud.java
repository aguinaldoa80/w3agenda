package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Rateioapuracao;
import br.com.linkcom.sined.geral.service.RateioapuracaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.RateioapuracaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Rateioapuracao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "centrocusto", "contagerencial"})
public class RateioapuracaoCrud extends CrudControllerSined<RateioapuracaoFiltro, Rateioapuracao, Rateioapuracao>{

	private RateioapuracaoService rateioapuracaoService;
	
	public void setRateioapuracaoService(RateioapuracaoService rateioapuracaoService) {
		this.rateioapuracaoService = rateioapuracaoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected void validateBean(Rateioapuracao bean, BindException errors) {
		Rateioapuracao apuracao = rateioapuracaoService.findByNome(bean.getNome());
		if(apuracao != null && !apuracao.getCdrateioapuracao().equals(bean.getCdrateioapuracao())){
			errors.reject("001","Apura��o de rateio j� cadastrada.");
		}
		
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Rateioapuracao bean) throws Exception {
		if(bean.getListaRateioapuracaoempresa() == null || bean.getListaRateioapuracaoempresa().isEmpty()){
			throw new SinedException("Informe pelo menos uma linha para o apura��o de rateio.");
		}
		super.salvar(request, bean);
	}
}