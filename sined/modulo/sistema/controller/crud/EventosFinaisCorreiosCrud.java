package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.EventosCorreios;
import br.com.linkcom.sined.geral.bean.EventosFinaisCorreios;
import br.com.linkcom.sined.geral.bean.TipoEventoCorreios;
import br.com.linkcom.sined.geral.bean.TipoEventos;
import br.com.linkcom.sined.geral.service.EventosCorreiosService;
import br.com.linkcom.sined.geral.service.TipoEventosService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EventosFinaisCorreiosFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Eventosfinaiscorreios" , authorizationModule=CrudAuthorizationModule.class)
public class EventosFinaisCorreiosCrud extends CrudControllerSined<EventosFinaisCorreiosFiltro, EventosFinaisCorreios, EventosFinaisCorreios>{
	
	private EventosCorreiosService eventosCorreiosService;
	private TipoEventosService tipoEventosService;
	
	public void setEventosCorreiosService(EventosCorreiosService eventosCorreiosService) {this.eventosCorreiosService = eventosCorreiosService;}
	public void setTipoEventosService(TipoEventosService tipoEventosService) {this.tipoEventosService = tipoEventosService;}

	
	public ModelAndView ajaxBuscaDescricao(WebRequestContext request, TipoEventoCorreios tipoEvento){
		List<EventosCorreios>listaDescricao = new ArrayList<EventosCorreios>();
		ModelAndView view = new JsonModelAndView();
		if(tipoEvento != null){
			listaDescricao = eventosCorreiosService.findDescricaoByTipo(tipoEvento);
		}
		return view.addObject("listaDescricoes", listaDescricao);
	}
	public ModelAndView ajaxBuscaCodigo(WebRequestContext request, EventosCorreios bean){
		EventosCorreios evento = new EventosCorreios();
		ModelAndView view = new JsonModelAndView();
		if(Util.objects.isPersistent(bean)){
			evento = eventosCorreiosService.findCodigoByEvento(bean);
		}
		return view.addObject("codigoEvento", evento.getCodigo());
	}
	
	@Override
	protected void entrada(WebRequestContext request, EventosFinaisCorreios form) throws Exception {
		if(Util.objects.isPersistent(form)){
			if(SinedUtil.isListNotEmpty(form.getListaTipoEventos())){
				for(TipoEventos tipoEvento: form.getListaTipoEventos()){
					if(tipoEvento.getEventosCorreios() != null){
						tipoEvento.setCodigo(tipoEvento.getEventosCorreios().getCodigo());
					}
				}
			}
		}
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(EventosFinaisCorreios bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaTipoEventos())){
			if(!Boolean.TRUE.equals(bean.getInsucesso()) && !Boolean.TRUE.equals(bean.getFinalizadoSucesso())){
				for(TipoEventos tipoEventos: bean.getListaTipoEventos()){
					if(Util.objects.isPersistent(tipoEventos.getEventosCorreios()) && tipoEventosService.isEventoFinal(tipoEventos.getEventosCorreios())){
						errors.reject("001", "Foi selecionado um evento final dos correios. Nessa situa��o, deve ser marcada a flag de Insucesso ou Finalizado com Sucesso.");
					}
				}
			}
		}
		super.validateBean(bean, errors);
	}
	
	@Override
	protected ListagemResult<EventosFinaisCorreios> getLista(WebRequestContext request, EventosFinaisCorreiosFiltro filtro) {
		ListagemResult<EventosFinaisCorreios> lista = super.getLista(request, filtro);
		for(EventosFinaisCorreios evento: lista.list()){
			evento.setListaTipoEventos(tipoEventosService.findByEventosFinaisCorreios(evento));
		}
		return lista;
	}
}