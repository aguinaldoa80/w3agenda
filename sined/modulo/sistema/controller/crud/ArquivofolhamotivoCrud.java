package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivofolhamotivo;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ArquivofolhamotivoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Arquivofolhamotivo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class ArquivofolhamotivoCrud extends CrudControllerSined<ArquivofolhamotivoFiltro, Arquivofolhamotivo, Arquivofolhamotivo> {

	@Override
	protected void salvar(WebRequestContext request, Arquivofolhamotivo bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_arquivofolhamotivo_descricao")) 
				throw new SinedException("Motivo da arquivo de folha j� cadastrado no sistema.");
		}
	}
	
}
