package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargodepartamento;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DepartamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Departamento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "codigofolha"})
public class DepartamentoCrud extends CrudControllerSined<DepartamentoFiltro, Departamento, Departamento> {
	
	private CargoService cargoService;
	
	public void setCargoService(CargoService cargoService) {
		this.cargoService = cargoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Departamento form) throws Exception {
		if(form.getListaCargodepartamento() != null){
			List<Cargodepartamento> listaCargodepartamentoNOVO = new ArrayList<Cargodepartamento>(); 
			List<Cargodepartamento> listaCargodepartamento = form.getListaCargodepartamento();
			for (Cargodepartamento cargodepartamento : listaCargodepartamento) {
				if(cargodepartamento.getCargo() != null){
					listaCargodepartamentoNOVO.add(cargodepartamento);
				}
			}
			form.setListaCargodepartamento(listaCargodepartamentoNOVO);
		}
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, DepartamentoFiltro filtro) throws CrudException {
		String cdcargo = request.getParameter("cdcargo");
		if(cdcargo != null){
			filtro.setCargo(cargoService.load(new Cargo(Integer.parseInt(cdcargo)), "cargo.cdcargo, cargo.nome"));
		}
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Departamento bean)throws Exception {
		try {
			if(bean.getCodigofolha() != null && bean.getCodigofolha().equals("")){
				bean.setCodigofolha(null);
			}
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_DEPARTAMENTO_NOME")) throw new SinedException("Departamento j� cadastrado no sistema.");
			if (DatabaseError.isKeyPresent(e, "IDX_DEPARTAMENTO_CODIGOFOLHA")) throw new SinedException("C�digo j� cadastrado no sistema.");
		}
		
	}
	
}
