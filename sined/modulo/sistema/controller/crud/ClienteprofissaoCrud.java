package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Clienteprofissao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ClienteprofissaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(
		path="/sistema/crud/Clienteprofissao",
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = "nome")
public class ClienteprofissaoCrud extends CrudControllerSined<ClienteprofissaoFiltro, Clienteprofissao, Clienteprofissao>{
	
	@Override
	protected void salvar(WebRequestContext request, Clienteprofissao bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_clienteprofissao_nome")) 
				throw new SinedException("Profiss�o j� cadastrada no sistema.");	
		}
	}
	
}
