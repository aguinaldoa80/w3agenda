package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Concorrente;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConcorrenteFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path={"/sistema/crud/Concorrente"},
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"nome", "telefone", "site", "email", "produtos"})
public class ConcorrenteCrud extends CrudControllerSined<ConcorrenteFiltro, Concorrente, Concorrente>{
	
	@Override
	protected void salvar(WebRequestContext request, Concorrente bean)
			throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "concorrente_nome_key")){
				throw new SinedException("Concorrente j� cadastrado no sistema.");
			}
		}
	}
	
}
