package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.ImportacaoProvisaoColuna;
import br.com.linkcom.sined.geral.service.ImportacaoProvisaoColunaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ImportacaoProvisaoColunaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/ImportacaoProvisaoColuna", authorizationModule=CrudAuthorizationModule.class)
public class ImportacaoProvisaoColunaCrud extends CrudControllerSined<ImportacaoProvisaoColunaFiltro, ImportacaoProvisaoColuna, ImportacaoProvisaoColuna> {
	
	private ImportacaoProvisaoColunaService importacaoProvisaoColunaService;
	
	public void setImportacaoProvisaoColunaService(ImportacaoProvisaoColunaService importacaoProvisaoColunaService) {this.importacaoProvisaoColunaService = importacaoProvisaoColunaService;}
	
	@Override
	protected void validateBean(ImportacaoProvisaoColuna bean, BindException errors) {
		if(bean.getColuna() != null && importacaoProvisaoColunaService.existeColunaCadastrada(bean)) {
			errors.reject("001", "N�o � poss�vel cadastrar dois campos com a mesma coluna.");
		}
	}
}
