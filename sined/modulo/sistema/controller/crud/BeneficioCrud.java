	package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Beneficio;
import br.com.linkcom.sined.geral.bean.Beneficiovalor;
import br.com.linkcom.sined.geral.service.BeneficiovalorService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BeneficioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Beneficio", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "beneficiotipo", "ativo", "valores"})
public class BeneficioCrud extends CrudControllerSined<BeneficioFiltro, Beneficio, Beneficio> {
	
	private BeneficiovalorService beneficiovalorService;
	
	public void setBeneficiovalorService(BeneficiovalorService beneficiovalorService) {
		this.beneficiovalorService = beneficiovalorService;
	}
	
	@Override
	protected ListagemResult<Beneficio> getLista(WebRequestContext request, BeneficioFiltro filtro) {
		ListagemResult<Beneficio> listagemResult = super.getLista(request, filtro);
		List<Beneficio> listaBeneficio = listagemResult.list();
		String whereIn = "-1";
		
		for (Beneficio beneficio: listaBeneficio){
			whereIn += ", " + beneficio.getCdbeneficio();
		}
		
		Map<Integer, List<Beneficiovalor>> mapa = new HashMap<Integer, List<Beneficiovalor>>();
		List<Beneficiovalor> listaBeneficiovalor = beneficiovalorService.findByCdsbeneficio(whereIn);
		
		for (Beneficiovalor beneficiovalor: listaBeneficiovalor){
			List<Beneficiovalor> listaAux = mapa.get(beneficiovalor.getBeneficio().getCdbeneficio());
			if (listaAux==null){
				listaAux = new ArrayList<Beneficiovalor>();
			}
			listaAux.add(beneficiovalor);
			mapa.put(beneficiovalor.getBeneficio().getCdbeneficio(), listaAux);
		}
		
		for (Beneficio beneficio: listaBeneficio){
			beneficio.setListaBeneficiovalor(mapa.get(beneficio.getCdbeneficio()));
		}
		
		return listagemResult;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Beneficio bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_beneficio_0")) {
				throw new SinedException("Benef�cio j� cadastrado no sistema.");
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void validateBean(Beneficio bean, BindException errors) {
		List<Beneficiovalor> listaBeneficiovalor = bean.getListaBeneficiovalor();
		
		if (listaBeneficiovalor!=null && !listaBeneficiovalor.isEmpty()){
			int contDataFimVazia = 0;
			boolean inicioMaiorFim = false;
			
			for (Beneficiovalor beneficiovalor: listaBeneficiovalor){
				if (beneficiovalor.getDtfim()==null){
					contDataFimVazia++;
				}else if (SinedDateUtils.diferencaDias(beneficiovalor.getDtinicio(), beneficiovalor.getDtfim())>0){
					inicioMaiorFim = true;
					break;
				}
			}
			
			if (contDataFimVazia>1){
				errors.reject("001", "N�o pode haver mais de um registro com data fim em branco.");
			}else if (inicioMaiorFim){
				errors.reject("001", "N�o pode haver um registro com data de in�cio maior que a data fim.");
			}else {
				Collections.sort(listaBeneficiovalor, new BeanComparator("dtinicio"));
//				
//				for (int i=0; i<listaBeneficiovalor.size(); i++){
//					Beneficiovalor beneficiovalor1 = listaBeneficiovalor.get(i);
//					Date dtinicio1 = beneficiovalor1.getDtinicio();
//					Date dtfim1 = beneficiovalor1.getDtfim();
//					if (i<(listaBeneficiovalor.size()-1)){
//						for (int j=i+1; j<listaBeneficiovalor.size(); j++){
//							Beneficiovalor beneficiovalor2 = listaBeneficiovalor.get(j);
//							Date dtinicio2 = beneficiovalor2.getDtinicio();
//							Date dtfim2 = beneficiovalor2.getDtfim();
//						}
//					}
//				}
				
				for (int i=0; i<listaBeneficiovalor.size(); i++){
					Beneficiovalor beneficiovalor1 = listaBeneficiovalor.get(i);
					Date dtfim = beneficiovalor1.getDtfim();
					if (i<(listaBeneficiovalor.size()-1)){
						if (dtfim==null){
							errors.reject("001", "A mesma data n�o pode estar contida em per�odos diferentes.");
							break;
						}
						Beneficiovalor beneficiovalor2 = listaBeneficiovalor.get(i+1);
						Date dtinicioProximo = beneficiovalor2.getDtinicio();
						if (SinedDateUtils.diferencaDias(dtfim, dtinicioProximo)>=0){
							errors.reject("001", "A mesma data n�o pode estar contida em per�odos diferentes.");
							break;
						}
					}
				}
			}
		}
		
		super.validateBean(bean, errors);
	}
}