package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessa;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRemessaSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaInstrucaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRemessaService;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoSegmentoService;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.geral.service.BancoformapagamentoService;
import br.com.linkcom.sined.geral.service.BancotipopagamentoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRemessaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/BancoConfiguracaoRemessa", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "banco", "tipo", "tamanho"})
public class BancoConfiguracaoRemessaCrud extends CrudControllerSined<BancoConfiguracaoRemessaFiltro, BancoConfiguracaoRemessa, BancoConfiguracaoRemessa> {
	
	private BancoService bancoService;
	private BancoConfiguracaoSegmentoService bancoConfiguracaoSegmentoService;
	private BancotipopagamentoService bancotipopagamentoService;
	private BancoformapagamentoService bancoformapagamentoService;
	private BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService;
	
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setBancoConfiguracaoSegmentoService(BancoConfiguracaoSegmentoService bancoConfiguracaoSegmentoService) {this.bancoConfiguracaoSegmentoService = bancoConfiguracaoSegmentoService;}
	public void setBancotipopagamentoService(BancotipopagamentoService bancotipopagamentoService) {this.bancotipopagamentoService = bancotipopagamentoService;}
	public void setBancoformapagamentoService(BancoformapagamentoService bancoformapagamentoService) {this.bancoformapagamentoService = bancoformapagamentoService;}	
	public void setBancoConfiguracaoRemessaService(BancoConfiguracaoRemessaService bancoConfiguracaoRemessaService) {this.bancoConfiguracaoRemessaService = bancoConfiguracaoRemessaService;}
	
	@Override
	protected void listagem(WebRequestContext request, BancoConfiguracaoRemessaFiltro filtro) throws Exception {
		request.setAttribute("listaBanco", bancoService.findAtivos());
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, BancoConfiguracaoRemessa form) throws Exception {
		request.setAttribute("listaBanco", bancoService.findAtivos());
		request.setAttribute("listaInstrucaoCobranca", BancoConfiguracaoRemessaInstrucaoEnum.getListaInstrucaoCobranca());
		request.setAttribute("listaInstrucaoPagamento", BancoConfiguracaoRemessaInstrucaoEnum.getListaInstrucaoPagamento());
		
		if (form.getBanco() != null && form.getTipo() != null && BancoConfiguracaoRemessaTipoEnum.PAGAMENTO.equals(form.getTipo())){
			request.setAttribute("listaBancoTipoPagamento", bancotipopagamentoService.findByBanco(form.getBanco()));
			request.setAttribute("listaBancoFormaPagamento", bancoformapagamentoService.FindByBanco(form.getBanco()));
			
			form.setListaDocumentoacao(bancoConfiguracaoRemessaService.getListDocumentoacao(form));
			List<Documentoacao> listaDocumentoacao =  new ArrayList<Documentoacao>();
			listaDocumentoacao.add(Documentoacao.PREVISTA);
			listaDocumentoacao.add(Documentoacao.DEFINITIVA);
			listaDocumentoacao.add(Documentoacao.AUTORIZADA);
			listaDocumentoacao.add(Documentoacao.AUTORIZADA_PARCIAL);
			request.setAttribute("listaDocumentoacaoCompleta", listaDocumentoacao);
		}
		
		bancoConfiguracaoSegmentoService.setListaVariaveisDisponiveisOnRequest(request);
		request.setAttribute("listaBancoConfiguracaoSegmento", bancoConfiguracaoSegmentoService.findByBanco(form.getBanco()));
		
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(BancoConfiguracaoRemessa bean, BindException errors) {
		if (bean.getListaBancoConfiguracaoRemessaSegmento() == null || (bean.getListaBancoConfiguracaoRemessaSegmento() != null &&
				bean.getListaBancoConfiguracaoRemessaSegmento().size() == 0))
			errors.reject("000", "Deve ter pelo menos um Header, Detalhe e Trailer.");
		if (bean.getListaBancoConfiguracaoRemessaSegmento() != null){
			boolean header = false;
			boolean detalhe = false;
			boolean trailer = false;
			for (BancoConfiguracaoRemessaSegmento bcrs : bean.getListaBancoConfiguracaoRemessaSegmento()){
				BancoConfiguracaoSegmento bancoConfiguracaoSegmento = bancoConfiguracaoSegmentoService.load(bcrs.getBancoConfiguracaoSegmento());
				if (BancoConfiguracaoTipoSegmentoEnum.HEADER.equals(bancoConfiguracaoSegmento.getTipoSegmento()))
					header = true;
				else if (BancoConfiguracaoTipoSegmentoEnum.DETALHE.equals(bancoConfiguracaoSegmento.getTipoSegmento()))
					detalhe = true;
				else if (BancoConfiguracaoTipoSegmentoEnum.TRAILER.equals(bancoConfiguracaoSegmento.getTipoSegmento()))
					trailer = true;
			}
			if (!header || !detalhe || !trailer){
				errors.reject("000", "Deve ter pelo menos um Header, Detalhe e Trailer.");
			}
		}
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, BancoConfiguracaoRemessa bean) throws Exception {
		bancoConfiguracaoRemessaService.criarListaBancoConfiguracaoRemessaDocumentoacao(bean);
		super.salvar(request, bean);
	}
	
}
