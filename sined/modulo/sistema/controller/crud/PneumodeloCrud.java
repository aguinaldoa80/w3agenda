package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneumodeloFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Pneumodelo"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"pneumarca", "nome", "codigointegracao"})
public class PneumodeloCrud extends CrudControllerSined<PneumodeloFiltro, Pneumodelo, Pneumodelo> {	
	
	@Override
	protected void salvar(WebRequestContext request, Pneumodelo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PNEUMODELO_NOME")) {
				throw new SinedException("Modelo de pneu j� cadastrado no sistema.");
			} else throw e;
		}
	}

	
	@Override
	protected Pneumodelo criar(WebRequestContext request, Pneumodelo form) throws Exception {
		form = super.criar(request, form);
		if("true".equals(request.getParameter("fromInsertOne"))){
			form.setOtr("true".equals(request.getParameter("isOtr")));
			String cdpneumarcaStr = request.getParameter("cdpneumarcaAux");
			if(StringUtils.isNotBlank(cdpneumarcaStr) && !"_".equals(cdpneumarcaStr)){
				cdpneumarcaStr = cdpneumarcaStr.split("_")[0];
				if(StringUtils.isNotBlank(cdpneumarcaStr)){
					form.setPneumarca(new Pneumarca(Integer.parseInt(cdpneumarcaStr)));
				}
			}
		}
		return form;
	}
}
