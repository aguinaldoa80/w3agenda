package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.ContaCarteiraNossoNumero;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Contaempresa;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contaprojeto;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.BoletoCobrancaEnum;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContaCarteiraNossoNumeroService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.ContacarteiraService;
import br.com.linkcom.sined.geral.service.ContacorrenteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.EnderecoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.geral.service.TaxaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.AprovaboletoproducaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.GerarboletohomologacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContacorrenteFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/sistema/crud/Contacorrente", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "banco", "agencia", "numero", "vconta.saldoatual", "desconsiderarsaldoinicialfluxocaixa", "ativo"})
public class ContacorrenteCrud extends CrudControllerSined<ContacorrenteFiltro,Conta,Conta> {

	private ContacorrenteService contacorrenteService;
	private ContaService contaService;
	private BancoService bancoService;
	private EmpresaService empresaService;
	private ProjetoService projetoService;
	private CentrocustoService centrocustoService;
	private EnderecoService enderecoService;
	private TaxaService taxaService;
	private ContacarteiraService contacarteiraService;
	private ContaCarteiraNossoNumeroService contaCarteiraNossoNumeroService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setBancoService(BancoService bancoService) {
		this.bancoService = bancoService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setContacorrenteService(ContacorrenteService contacorrenteService) {
		this.contacorrenteService = contacorrenteService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public void setEnderecoService(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	public void setTaxaService(TaxaService taxaService) {
		this.taxaService = taxaService;
	}
	public void setContacarteiraService(
			ContacarteiraService contacarteiraService) {
		this.contacarteiraService = contacarteiraService;
	}
	public void setContaCarteiraNossoNumeroService(
			ContaCarteiraNossoNumeroService contaCarteiraNossoNumeroService) {
		this.contaCarteiraNossoNumeroService = contaCarteiraNossoNumeroService;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Conta form)	throws CrudException {
		if(!contaService.isUsuarioPermissaoConta(form)){
			request.addError("Usu�rio n�o tem permiss�o para consultar/editar esta conta!");
			SinedUtil.redirecionamento(request, "/sistema/crud/Contacorrente");
			return null;
		}
		return super.doEditar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContacorrenteFiltro filtro) throws Exception {
		request.setAttribute("isUsuarioAdministrador", SinedUtil.isUsuarioLogadoAdministrador());
		request.setAttribute("listaBancos", bancoService.findAtivos());
	}
	
	@Override
	protected Conta criar(WebRequestContext request, Conta form) throws Exception {
		Conta bean = super.criar(request, form);
		form.setDtsaldo(new Date(System.currentTimeMillis()));
		form.setAtivo(true);
		return bean;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Conta form)throws Exception {
		request.setAttribute("hoje", new SimpleDateFormat("dd/MM/yyyy").format(new java.sql.Date(System.currentTimeMillis())));
		
		if(form.getCdconta() == null && request.getParameter("ACAO") != null && CRIAR.equals(request.getParameter("ACAO"))){
			form.setDtsaldo(new Date(System.currentTimeMillis()));
			form.setAtivo(true);
		}
		if(form.getCdconta() != null){
			form.setSaldoatual(contaService.calculaSaldoAtual(form));
			
			if(request.getBindException().getErrorCount() > 0)
				form.setDtsaldo(contaService.load(form, "conta.dtsaldo").getDtsaldo());		
		}
		
		if (form.getContaContabil() != null){
			form.setContaContabil(new ContaContabil(form.getContaContabil().getCdcontacontabil()));
		}
		
		if(SinedUtil.isListNotEmpty(form.getListaContacarteira())){
			for (int i = 0; i < form.getListaContacarteira().size(); i++) {
				if(Boolean.TRUE.equals(form.getListaContacarteira().get(i).getNossonumeroindividual())){
					ContaCarteiraNossoNumero contaCarteiraNossoNumero = contaCarteiraNossoNumeroService.findByContacarteira(form.getListaContacarteira().get(i));
					if(contaCarteiraNossoNumero != null && contaCarteiraNossoNumero.getNossonumero() != null){
						form.getListaContacarteira().get(i).setNossonumero(contaCarteiraNossoNumero.getNossonumero());						
					}
				}
			}
		}
		
		request.setAttribute("listaBancos", bancoService.findAtivos(form.getBanco()));
		
		List<Projeto> projeto = new ArrayList<Projeto>();
		if(form.getListaContaprojeto() != null && !form.getListaContaprojeto().isEmpty()){
			for(Contaprojeto contaprojeto : form.getListaContaprojeto()){
				if(contaprojeto.getProjeto() != null && contaprojeto.getProjeto().getCdprojeto() != null)
					projeto.add(contaprojeto.getProjeto());
			}
		}
		List<Projeto> listaProjeto = projetoService.findForComboByUsuariologado(projeto, true);
		request.setAttribute("isUsuarioAdministrador", SinedUtil.isUsuarioLogadoAdministrador());
		request.setAttribute("listaProjeto", listaProjeto);
	}
	
	@Override
	protected void validateBean(Conta bean, BindException errors) {
		if(camposValidos(bean,errors)){
			if(!contacorrenteService.validateContacorrente(bean)){
				errors.reject("001","Conta corrente j� cadastrada no sistema.");
			}
		}
		if(bean.getListaContacarteira() != null && bean.getListaContacarteira().size() > 0){
			Integer padrao = 0;
			for (Contacarteira contacarteira : bean.getListaContacarteira()){
				if (contacarteira.isPadrao()){
					padrao++;
				}
			}
			if (padrao == 0)
				errors.reject("001","Pelo menos uma carteira deve ser marcada como padr�o.");
			else if (padrao > 1)
				errors.reject("001","S� pode existir uma carteira marcada como padr�o.");
		}
		
		taxaService.validateTaxasByConta(bean, errors);
	}
	
	/**
	 * M�todo para validar os campos do bean.
	 * 
	 * @param bean
	 * @param errors
	 * @return
	 * @author Flavio Tavares
	 */
	private boolean camposValidos(Conta bean, BindException errors){
		boolean retorno = true;
		
		if(!SinedUtil.validaNumeros(bean.getAgencia())){
			errors.reject("001","Caracteres inv�lidos em ag�ncia.");
			retorno = false;
		}
		if(!SinedUtil.validaNumeros(bean.getNumero())){
			errors.reject("001","Caracteres inv�lidos em conta.");
			retorno = false;
		}
		if(bean.getLimite()!=null && bean.getLimite().toLong()<0){
			errors.reject("001","Limite n�o pode ser negativo.");
		}
		if(bean.getSaldo()!=null && bean.getSaldo().getValue().doubleValue()<0){
			double saldoAbsoluto = bean.getSaldo().getValue().doubleValue()*-1; 
			if(bean.getLimite()==null || saldoAbsoluto > bean.getLimite().getValue().doubleValue()){
				errors.reject("001","O valor absoluto do saldo inicial n�o pode ser maior que o limite.");
				retorno = false;
			}			
		}
		
		return retorno;
	}
	
	@Override
	protected void excluir(WebRequestContext request, Conta bean)throws Exception {
		try {			
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Conta Corrente n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,Conta form) {
		return new ModelAndView("crud/contacorrenteEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, ContacorrenteFiltro filtro) {
		return new ModelAndView("crud/contacorrenteListagem");
	}
	
	/**
	 * Retorna o n�mero do banco selecionado em contacorrente.
	 * @param request
	 * @author Taidson
	 * @since 13/08/2010
	 */
	public void ajaxVerificaBanco(WebRequestContext request){
		String description = request.getParameter("description");
		
		Integer cdconta = Integer.valueOf(StringUtils.substringBetween(description, "=", "]"));
		Conta conta = new Conta();
		conta.setCdconta(cdconta);
		
		conta = contaService.load(conta);
		
		Integer numeroBanco = conta.getBanco().getNumero();
		View.getCurrent().println("var numeroBanco = '" + Util.strings.toStringIdStyled(numeroBanco, false)+"';");
	}
	
	/**
	 * Action que busca o n�mero do banco para o cadastro de conta banc�ria.
	 * Feito em outro m�todo por causa de particularidades do m�todo.
	 *
	 * @param request
	 * @param conta
	 * @since 28/10/2011
	 * @author Rodrigo Freitas
	 */
	public void ajaxBanco(WebRequestContext request, Conta conta){
		try{
			if(conta.getBanco() == null && conta.getBanco().getCdbanco() == null)
				throw new SinedException("Banco n�o pode ser nulo.");
			
			Banco banco = bancoService.load(conta.getBanco());
			
			View.getCurrent().println("var numeroBanco = '" + banco.getNumeroString() + "';");
			View.getCurrent().println("var boletoCobrancaOptionsHtml = '" + BoletoCobrancaEnum.getAllByBancoForSelect(banco.getNumeroString()) + "';");
			View.getCurrent().println("var sucesso = true;");
			
		} catch (Exception e) {
			View.getCurrent().println("var sucesso = false;");
			View.getCurrent().println("alert('Erro: " + e.getMessage() + "');");
		}
	}
	
	/**
	 * M�todo que abre popup para gera��o de boletos para homologa��o
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView abrirPopupGerarBoletosHomolocacao(WebRequestContext request, GerarboletohomologacaoBean gerarboletohomologacaoBean){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(whereIn.split(",").length > 1){
			request.addError("S� � permitido gerar boletos de homologa��o de uma conta por vez.");
			SinedUtil.fechaPopUp(request);
			return null;
		}		
		
		if(request.getAttribute("erroRateio") == null || !"true".equals(request.getAttribute("erroRateio"))){
			gerarboletohomologacaoBean = new GerarboletohomologacaoBean();
			gerarboletohomologacaoBean.setValor(new Money(100.00));
		}
		if(gerarboletohomologacaoBean.getEndereco() != null){
			request.setAttribute("enderecoEscolhido", "br.com.linkcom.sined.geral.bean.Endereco[cdendereco=" + gerarboletohomologacaoBean.getEndereco().getCdendereco() + "]");
		}
		Conta conta = contacorrenteService.loadForEntrada(new Conta(Integer.parseInt(whereIn)));
		gerarboletohomologacaoBean.setConta(conta);
		gerarboletohomologacaoBean.setEmpresa(conta.getEmpresa());
		
		List<Empresa> listaEmpresa = new ArrayList<Empresa>();
		if(conta.getListaContaempresa() != null && !conta.getListaContaempresa().isEmpty()){
			for(Contaempresa contaempresa : conta.getListaContaempresa()){
				listaEmpresa.add(contaempresa.getEmpresa());
			}
		}else {
			listaEmpresa.add(empresaService.loadPrincipal());
		}
		request.setAttribute("listaEmpresa", listaEmpresa);
		request.setAttribute("tipooperacao", Tipooperacao.TIPO_CREDITO);
		request.setAttribute("listaContacarteira", conta.getListaContacarteira());
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos());
		request.setAttribute("listaProjeto", projetoService.findProjetosAbertos(null));
		
		return new ModelAndView("direct:/crud/popup/popUpGerarBoletoHomologacao", "bean", gerarboletohomologacaoBean);
	}
	
	/**
	 * M�todo que abre popup para aprova��o de boletos para produ��o
	 *
	 * @param request
	 * @return
	 * @author Jo�o Vitor
	 */
	public ModelAndView abrirPopupAprovarBoletoProducao(WebRequestContext request, AprovaboletoproducaoBean aprovaboletoproducaoBean){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(whereIn.split(",").length > 1){
			request.addError("S� � permitido gerar boletos de homologa��o de uma conta por vez.");
			SinedUtil.fechaPopUp(request);
			return null;
		}		
		
		Conta conta = contacorrenteService.load((new Conta(Integer.parseInt(whereIn))), "conta.cdconta, conta.nome");
		List<Contacarteira> listaContacarteiras = contacarteiraService.findByContaSemAprovacao(conta);
		
		if(listaContacarteiras== null || listaContacarteiras.isEmpty()){
			listaContacarteiras = new ListSet<Contacarteira>(Contacarteira.class);
		}
		
		aprovaboletoproducaoBean.setConta(conta);
		request.setAttribute("listaContacarteiras", listaContacarteiras);
		
		return new ModelAndView("direct:/crud/popup/popUpAprovaBoletoProducao", "bean", aprovaboletoproducaoBean);
	}
	
	/**
	 * M�todo para gerar as contas a receber (homologa��o de boleto)
	 *
	 * @param request
	 * @param gerarboletohomologacaoBean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView gerarBoletosHomolocacao(WebRequestContext request, GerarboletohomologacaoBean gerarboletohomologacaoBean){
		
		if(gerarboletohomologacaoBean.getRateio() == null || gerarboletohomologacaoBean.getRateio().getListaRateioitem() == null ||
			gerarboletohomologacaoBean.getRateio().getListaRateioitem().isEmpty()){
			request.setAttribute("erroRateio", "true");
			return abrirPopupGerarBoletosHomolocacao(request,gerarboletohomologacaoBean);
		}
		
		try {
			contacorrenteService.gerarBoletoHomologacao(gerarboletohomologacaoBean);
		} catch (Exception e) {
			request.addError(e.getMessage());
			SinedUtil.redirecionamento(request, "/sistema/crud/Contacorrente");
			return null;
		}
		
		request.addMessage("Conta(s) � receber gerada com sucesso.");
		SinedUtil.redirecionamento(request, "/sistema/crud/Contacorrente");
		return null;
	}
	
	public ModelAndView comboBoxEndereco(WebRequestContext request, Cliente cliente) {
		if (cliente == null || cliente.getCdpessoa() == null) {
			throw new SinedException("Cliente n�o pode ser nulo.");
		}
		return new JsonModelAndView().addObject("lista", enderecoService.findByCliente(cliente));
	}
	
	
	/**
	 * M�todo para gerar as contas a receber (homologa��o de boleto)
	 *
	 * @param request
	 * @param gerarboletohomologacaoBean
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView aprovarBoletoProducao(WebRequestContext request, AprovaboletoproducaoBean aprovaboletoproducaoBean){
		Contacarteira contacarteira= null;
		if(aprovaboletoproducaoBean == null || aprovaboletoproducaoBean.getContacarteira() == null || aprovaboletoproducaoBean.getConta() == null){
			request.setAttribute("erroAprovacao", "true");
			return abrirPopupAprovarBoletoProducao(request, aprovaboletoproducaoBean);
		} else {
			contacarteira = contacarteiraService.load(aprovaboletoproducaoBean.getContacarteira());
			contacarteira.setAprovadoproducao(true);
		}
		
		if(aprovaboletoproducaoBean.getNossonumeroUltimo() != null){
			ContaCarteiraNossoNumero contaCarteiraNossoNumero;
			contaCarteiraNossoNumero = contaCarteiraNossoNumeroService.findByContacarteira(aprovaboletoproducaoBean.getContacarteira());
			if(contaCarteiraNossoNumero != null){
				contaCarteiraNossoNumeroService.updateContaCarteiraNossoNumero(contaCarteiraNossoNumero, aprovaboletoproducaoBean.getNossonumeroUltimo()+1);
			}else{
				contaCarteiraNossoNumero = new ContaCarteiraNossoNumero();
				contaCarteiraNossoNumero.setContacarteira(aprovaboletoproducaoBean.getContacarteira());
				contaCarteiraNossoNumero.setNossonumero(aprovaboletoproducaoBean.getNossonumeroUltimo() + 1);
				contaCarteiraNossoNumeroService.saveOrUpdate(contaCarteiraNossoNumero);				
			}
		}
		
		try {
			contacarteiraService.aprovaBoletoProducao(contacarteira);
		} catch (Exception e) {
			request.addError(e.getMessage());
			SinedUtil.redirecionamento(request, "/sistema/crud/Contacorrente");
			return null;
		}
		
		request.addMessage("Boleto aprovado com sucesso.");
		SinedUtil.redirecionamento(request, "/sistema/crud/Contacorrente");
		return null;
	}
	
	/**
	* M�todo ajax que busca informa��es da empresa titular
	*
	* @param request
	* @param conta
	* @since 15/03/2016
	* @author Luiz Fernando
	*/
	public void ajaxBuscarInfEmpresatitular(WebRequestContext request, Conta conta){
		String nomefantasiaempresatitular = "";
		if(conta.getEmpresatitular() != null){
			Empresa empresatitular = empresaService.load(conta.getEmpresatitular(), "empresa.cdpessoa, empresa.nomefantasia");
			if(empresatitular != null && StringUtils.isNotBlank(empresatitular.getNomefantasia())){
				nomefantasiaempresatitular = empresatitular.getNomefantasia();
			}
		}
		View.getCurrent().println("var nomefantasiaempresatitular = '"+nomefantasiaempresatitular+"';");
	}
	
	public ModelAndView ajaxBuscarNossoNumero(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		Contacarteira contacarteira = new Contacarteira(Integer.parseInt(request.getParameter("cdcontacarteira")));
		
		ContaCarteiraNossoNumero contaCarteiraNossoNumero = contaCarteiraNossoNumeroService.findByContacarteira(contacarteira);
		
		if(contaCarteiraNossoNumero != null && contaCarteiraNossoNumero.getNossonumero() != null){
			jsonModelAndView.addObject("nossonumero", contaCarteiraNossoNumero.getNossonumero());			
		}
		
		return jsonModelAndView;
	}
	
	public ModelAndView ajaxIsNossoNumeroIndividual(WebRequestContext request){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		Contacarteira contaCarteira = contacarteiraService.isNossoNumeroIndividual(Integer.parseInt(request.getParameter("cdcontacarteira")));
		
		if(contaCarteira != null && contaCarteira.getNossonumeroindividual() != null){
			jsonModelAndView.addObject("nossonumeroindividual", contaCarteira.getNossonumeroindividual());			
		}
		
		return jsonModelAndView;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Conta bean) throws Exception {
		if(SinedUtil.isListNotEmpty(bean.getListaContacarteira())){
			for (int i = 0; i < bean.getListaContacarteira().size(); i++) {
				if(Boolean.TRUE.equals(bean.getListaContacarteira().get(i).getNossonumeroindividual())){
					ContaCarteiraNossoNumero contaCarteiraNossoNumero;
					contaCarteiraNossoNumero = contaCarteiraNossoNumeroService.findByContacarteira(bean.getListaContacarteira().get(i));
					if(contaCarteiraNossoNumero == null){
						contaCarteiraNossoNumero = new ContaCarteiraNossoNumero();
						contaCarteiraNossoNumero.setContacarteira(bean.getListaContacarteira().get(i));
						contaCarteiraNossoNumero.setNossonumero(1);
						contaCarteiraNossoNumeroService.saveOrUpdate(contaCarteiraNossoNumero);
					}
				}
			}
		}
		
		super.salvar(request, bean);
	}
	
}
