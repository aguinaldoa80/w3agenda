package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Boleto;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BoletoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Boleto", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class BoletoCrud extends CrudControllerSined<BoletoFiltro, Boleto, Boleto> {

	@Override
	protected void salvar(WebRequestContext request, Boleto bean) throws Exception {
		try {
			bean.setNome(bean.getNome().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_BOLETO_NOME")) {
				throw new SinedException("Boleto j� cadastrado no sistema.");
			}
		}
	}
	
}
