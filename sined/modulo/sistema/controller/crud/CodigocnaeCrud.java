package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Codigocnae;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CodigocnaeFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Codigocnae", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cnae", "descricaocnae"})
public class CodigocnaeCrud extends CrudControllerSined<CodigocnaeFiltro, Codigocnae, Codigocnae> {
	
}
