package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.MotivoReprovacaoFaturamento;
import br.com.linkcom.sined.geral.service.MotivoReprovacaoFaturamentoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivoReprovacaoFaturamentoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/MotivoReprovacaoFaturamento", authorizationModule = CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "ativo"})
public class MotivoReprovacaoFaturamentoCrud extends CrudControllerSined<MotivoReprovacaoFaturamentoFiltro, MotivoReprovacaoFaturamento, MotivoReprovacaoFaturamento>{

	private MotivoReprovacaoFaturamentoService motivoReprovacaoFaturamentoService;
	public void setMotivoReprovacaoFaturamentoService(
			MotivoReprovacaoFaturamentoService motivoReprovacaoFaturamentoService) {
		this.motivoReprovacaoFaturamentoService = motivoReprovacaoFaturamentoService;
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, MotivoReprovacaoFaturamento bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {	
			 throw new SinedException("Verifique se o motivo de reprova��o j� est� cadastrado no sistema.");			
		}		
	}	
	@Override
	protected void listagem(WebRequestContext request,MotivoReprovacaoFaturamentoFiltro filtro) throws Exception{
		super.listagem(request, filtro);
	}
			

	public ModelAndView gerarCsv(WebRequestContext request, MotivoReprovacaoFaturamentoFiltro filtro){
		StringBuilder StringCSV = motivoReprovacaoFaturamentoService.gerarCsvListagem(filtro);
		Resource resource = new Resource("text/csv","motivoReprovacaoFaturamento"+ SinedUtil.datePatternForReport() +".csv" , StringCSV.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;

	}
	
	
}
