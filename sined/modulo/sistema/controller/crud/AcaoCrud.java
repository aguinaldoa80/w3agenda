package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Acao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AcaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Acao", authorizationModule=CrudAuthorizationModule.class)
public class AcaoCrud extends CrudControllerSined<AcaoFiltro, Acao, Acao> {
	
	@Override
	protected void salvar(WebRequestContext request, Acao bean)throws Exception {
		try {
			bean.setKey(bean.getKey().toUpperCase());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_id_nome")) throw new SinedException("A��o j� cadastrada no sistema");
			
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Acao form)throws CrudException {
		return super.doEditar(request, form);
	}
	
}
