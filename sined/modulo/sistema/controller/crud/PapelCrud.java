package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.service.DashboardespecificoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PapelFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.menu.DashboardBean;

@CrudBean
@Controller(path="/sistema/crud/Papel", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "descricao"})
public class PapelCrud extends CrudControllerSined<PapelFiltro, Papel, Papel> {
	
	private DashboardespecificoService dashboardespecificoService;
	
	public void setDashboardespecificoService(
			DashboardespecificoService dashboardespecificoService) {
		this.dashboardespecificoService = dashboardespecificoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Papel bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_papel_nome")) throw new SinedException("N�vel de acesso j� cadastrado no sistema.");
			
		}
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Papel form)throws CrudException {
		form.setAtualiza("atualiza");
		return super.doEditar(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Papel form) throws Exception {
		List<GenericBean> listaDashboard = new ArrayList<GenericBean>();
		if(SinedUtil.isAmbienteDesenvolvimento()){
			try {
				List<DashboardBean> dashboardInW3controle = dashboardespecificoService.getDashboardInW3controle();
				for (DashboardBean dashboardBean : dashboardInW3controle) {
					listaDashboard.add(new GenericBean(dashboardBean.getCddashboard(), dashboardBean.getNome()));
				}
				request.setAttribute("listaDashboard", listaDashboard);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			List<DashboardBean> dashboardInW3controle = dashboardespecificoService.getDashboardInW3controle();
			for (DashboardBean dashboardBean : dashboardInW3controle) {
				listaDashboard.add(new GenericBean(dashboardBean.getCddashboard(), dashboardBean.getNome()));
			}
			request.setAttribute("listaDashboard", listaDashboard);
		}
	}
	
}
