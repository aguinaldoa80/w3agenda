package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Materiallegenda;
import br.com.linkcom.sined.geral.service.MateriallegendaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MateriallegendaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@CrudBean
@Controller(path="/sistema/crud/Materiallegenda", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"legenda", "processo"})
public class MateriallegendaCrud extends CrudControllerSined<MateriallegendaFiltro, Materiallegenda, Materiallegenda> {
	
	private MateriallegendaService materiallegendaService;
	
	public void setMateriallegendaService(
			MateriallegendaService materiallegendaService) {
		this.materiallegendaService = materiallegendaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Materiallegenda form) throws Exception {
		if(form.getCdmateriallegenda() != null){
			if(form.getSimbolo() != null && form.getSimbolo().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), form.getSimbolo().getCdarquivo());
			}
		}
	}
	
	@Override
	protected void validateBean(Materiallegenda bean, BindException errors) {
		if(materiallegendaService.haveOrdem(bean.getOrdem(), bean.getCdmateriallegenda())){
			errors.reject("001", "J� existe a ordem cadastrada em outra legenda de material.");
		}
	}
	
}