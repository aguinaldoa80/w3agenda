package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.Set;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Configuracaotef;
import br.com.linkcom.sined.geral.bean.Configuracaotefterminal;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/Configuracaotef", authorizationModule=CrudAuthorizationModule.class)
public class ConfiguracaotefCrud extends CrudControllerSined<FiltroListagemSined, Configuracaotef, Configuracaotef> {
	
	@Override
	protected void salvar(WebRequestContext request, Configuracaotef bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "configuracaotef_cdempresa_idx")) {
				throw new SinedException("Configura��o para a empresa j� cadastrada no sistema.");
			}
		}
	}
	
	@Override
	protected void validateBean(Configuracaotef bean, BindException errors) {
		boolean hasImpressora = false;
		Set<Configuracaotefterminal> listaConfiguracaotefterminal = bean.getListaConfiguracaotefterminal();
		if(listaConfiguracaotefterminal == null || listaConfiguracaotefterminal.size() > 0){
			for (Configuracaotefterminal configuracaotefterminal : listaConfiguracaotefterminal) {
				if(configuracaotefterminal.getImpressora() != null && configuracaotefterminal.getImpressora()){
					if(hasImpressora){
						errors.reject("001", "S� � poss�vel ter um terminal com impressora integrada.");
						break;
					}
					hasImpressora = true;
				}
			}
		}
	}
	
}
