package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Veiculousotipo;
import br.com.linkcom.sined.geral.service.VeiculousotipoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VeiculousotipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Veiculousotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "escolta", "normal"})
public class VeiculousotipoCrud extends CrudControllerSined<VeiculousotipoFiltro, Veiculousotipo, Veiculousotipo>{
	
	private VeiculousotipoService veiculousotipoService;
	
	public void setVeiculousotipoService(
			VeiculousotipoService veiculousotipoService) {
		this.veiculousotipoService = veiculousotipoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Veiculousotipo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_VEICULOUSOTIPO_NOME")) {
				throw new SinedException("Tipo de uso j� cadastrado no sistema");
			}
		}
	}
	
	/**
	 * Fun��o ajax que retorna a escala do veiculo
	 * @param request
	 * @param veiculouso
	 * @author Tom�s Rabelo
	 */
	public void existeTipoEscoltaAjax(WebRequestContext request, Veiculousotipo veiculousotipo){
		String veiculoUsoEscolta = "";
		veiculousotipo = veiculousotipoService.veiculoUsoTipoEscolta();
		if(veiculousotipo != null)
			veiculoUsoEscolta = veiculousotipo.getDescricao();
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval("var nome = '"+veiculoUsoEscolta+"';");
	}

	/**
	 * Fun��o ajax que retorna o normal do veiculo
	 * @param request
	 * @param veiculouso
	 * @author Tom�s Rabelo
	 */
	public void existeTipoNormalAjax(WebRequestContext request, Veiculousotipo veiculousotipo){
		String veiculoUsoNormal = "";
		veiculousotipo = veiculousotipoService.veiculoUsoTipoNormal();
		if(veiculousotipo != null)
			veiculoUsoNormal = veiculousotipo.getDescricao();
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().eval("var nome = '"+veiculoUsoNormal+"';");
	}
	

	@Override
	protected void excluir(WebRequestContext request, Veiculousotipo bean) throws Exception {
		try {
			super.excluir(request, bean);
		} catch (SinedException e) {
			throw new SinedException("Tipo de uso do ve�culo n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
}
