package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Colaboradordespesamotivo;
import br.com.linkcom.sined.modulo.rh.controller.crud.filter.ColaboradordespesamotivoFilter;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller (path = "/sistema/crud/Colaboradordespesamotivo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class ColaboradordespesamotivoCrud extends CrudControllerSined<ColaboradordespesamotivoFilter, Colaboradordespesamotivo, Colaboradordespesamotivo>{
		
	
		
}
