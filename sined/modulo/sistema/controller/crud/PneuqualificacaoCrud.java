package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pneuqualificacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneuqualificacaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/sistema/crud/Pneuqualificacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "garantia", "ativo"})
public class PneuqualificacaoCrud extends CrudControllerSined<PneuqualificacaoFiltro, Pneuqualificacao, Pneuqualificacao>{
	
	@Override
	protected void salvar(WebRequestContext request, Pneuqualificacao bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_pneuqualificacao_nome")) {
				throw new SinedException("Qualifica��o de pneu j� cadastrada no sistema.");
			} else {
				e.printStackTrace();
			}			
		}
	}
}