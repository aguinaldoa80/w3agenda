package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Cfoptipo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacaocfop;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaocfopService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CfopFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Cfop", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"codigo", "cfoptipo", "cfopescopo", "descricaoresumida"})
public class CfopCrud extends CrudControllerSined<CfopFiltro, Cfop, Cfop> {
	
	private CfopService cfopService;
	private NaturezaoperacaocfopService naturezaoperacaocfopService;
	private ParametrogeralService parametroGeralService;
	
	public void setParametroGeralService(ParametrogeralService parametroGeralService) {this.parametroGeralService = parametroGeralService;}
	public void setCfopService(CfopService cfopService) {
		this.cfopService = cfopService;
	}
	public void setNaturezaoperacaocfopService(NaturezaoperacaocfopService naturezaoperacaocfopService) {
		this.naturezaoperacaocfopService = naturezaoperacaocfopService;
	}
	
	
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Cfop form) throws CrudException {
		throw new SinedException("Fun��o n�o permitida.");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Cfop form) throws CrudException {
		throw new SinedException("Fun��o n�o permitida.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Cfop bean)
			throws Exception {
		
		if (bean.getListacfoprelacionado().size() > 3){
			throw new SinedException("Um CFOP s� pode ser associado a tr�s refer�ncias.");
		}
		super.salvar(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cfop form)
			throws Exception {
		String valorParaConta = "";
		if(Cfoptipo.ENTRADA.equals(form.getCfoptipo())){
			valorParaConta = "C";
		}else{
			valorParaConta = "D";
		}
		request.setAttribute("TipoConta",valorParaConta);
		request.setAttribute("listaEntrada", cfopService.findByTipoEscopo(Cfoptipo.ENTRADA, null));
		request.setAttribute("RATEIO_MOVIMENTACAO_ESTOQUE", parametroGeralService.getBoolean(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE));
		super.entrada(request, form);
	}
	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, CfopFiltro filtro) throws CrudException {
		
		filtro.setNaturezaoperacao(null);
		
		if (NumberUtils.isNumber(request.getParameter("cdnaturezaoperacao"))){
			Naturezaoperacao naturezaoperacao = new Naturezaoperacao(Integer.valueOf(request.getParameter("cdnaturezaoperacao")));
			List<Naturezaoperacaocfop> listaNaturezaoperacaocfop = naturezaoperacaocfopService.find(naturezaoperacao, null);
			if (!listaNaturezaoperacaocfop.isEmpty()){
				filtro.setNaturezaoperacao(naturezaoperacao);
			}
		}
		
		return super.doListagem(request, filtro);
	}
}
