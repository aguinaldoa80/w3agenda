package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.TipoavisoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivoavisoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Motivoaviso", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "tipoaviso", "emailDiario", "email", "notificacao", "popup", "ativo"})
public class MotivoavisoCrud extends CrudControllerSined<MotivoavisoFiltro, Motivoaviso, Motivoaviso> {

	private UsuarioService usuarioService;
	private TipoavisoService tipoavisoService;
	private MotivoavisoService motivoavisoService;
	
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}
	public void setTipoavisoService(TipoavisoService tipoavisoService) {this.tipoavisoService = tipoavisoService;}
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}

	@Override
	public ModelAndView doListagem(WebRequestContext request, MotivoavisoFiltro filtro) throws CrudException {
		filtro.setPageSize(60);
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected Motivoaviso criar(WebRequestContext request, Motivoaviso form) throws Exception {
		form.setAtivo(Boolean.TRUE);
		return super.criar(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Motivoaviso form)	throws Exception {
		request.setAttribute("usuariosAtivos", usuarioService.findUsuariosDesbloqueados(form != null ? form.getUsuario() : null));
		request.setAttribute("listaTipoaviso", tipoavisoService.getTipoAvisoByMotivoaviso());
	}
	
	@Override
	protected void listagem(WebRequestContext request, MotivoavisoFiltro filtro) throws Exception {
		request.setAttribute("listaTipoaviso", tipoavisoService.getTipoAvisoByMotivoaviso());
		request.setAttribute("listaAvisoOrigem", AvisoOrigem.getListaOrdenada());
	}
	
	public ModelAndView updateChecksMotivoAviso(WebRequestContext request) throws Exception {
		String selectedItens = request.getParameter("selectedItens");
		String acao = request.getParameter("acao");
		
		try {
			motivoavisoService.updateChecksMotivoAviso(selectedItens, acao);
			request.addMessage("Motivo(s) selecionado(s) atualizado(s) com sucesso.");
		} catch (Exception e) {
			request.addError("Erro ao atualizar motivo(s) selecionado(s).");
		}
		
		return new ModelAndView("redirect:/sistema/crud/Motivoaviso");
	}
}
