package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Arquivoemporium;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.geral.service.ArquivoemporiumService;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ArquivoemporiumFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumExportacaoMovimentosUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumImportacaoClientesUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumImportacaoProdutosUtil;
import br.com.linkcom.sined.util.emporium.IntegracaoEmporiumUtil;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/sistema/crud/Arquivoemporium", authorizationModule=CrudAuthorizationModule.class)
public class ArquivoemporiumCrud extends CrudControllerSined<ArquivoemporiumFiltro, Arquivoemporium, Arquivoemporium> {
	
	private ArquivoemporiumService arquivoemporiumService;
	private EmporiumvendaService emporiumvendaService;
	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setEmporiumvendaService(
			EmporiumvendaService emporiumvendaService) {
		this.emporiumvendaService = emporiumvendaService;
	}
	public void setArquivoemporiumService(
			ArquivoemporiumService arquivoemporiumService) {
		this.arquivoemporiumService = arquivoemporiumService;
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Arquivoemporium form) throws CrudException {
		throw new SinedException("Fun��o n�o permitida.");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Arquivoemporium form) throws CrudException {
		throw new SinedException("Fun��o n�o permitida.");
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Arquivoemporium form) throws CrudException {
		throw new SinedException("Fun��o n�o permitida.");
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Arquivoemporium form) throws CrudException {
		throw new SinedException("Fun��o n�o permitida.");
	}
	
	@Override
	public ModelAndView doConsultar(WebRequestContext request, Arquivoemporium form) throws CrudException {
		throw new SinedException("Fun��o n�o permitida.");
	}
	
	@Override
	protected ListagemResult<Arquivoemporium> getLista(WebRequestContext request, ArquivoemporiumFiltro filtro) {
		ListagemResult<Arquivoemporium> listagemResult = super.getLista(request, filtro);
		List<Arquivoemporium> lista = listagemResult.list();
		
		for (Arquivoemporium arquivoemporium : lista) {
			if(arquivoemporium.getArquivo() != null && arquivoemporium.getArquivo().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivoemporium.getArquivo().getCdarquivo());
			}
			if(arquivoemporium.getArquivoresultado() != null && arquivoemporium.getArquivoresultado().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivoemporium.getArquivoresultado().getCdarquivo());
			}
		}
		
		return listagemResult;
	}
	
	public ModelAndView abrirPopUpGeracaoArquivo(WebRequestContext request, ArquivoemporiumFiltro bean){
		return new ModelAndView("direct:/crud/popup/popUpGeracaoArquivo", "bean", bean);
	}
	
	/**
	 * Action que gera o arquivo dependendo do tipo passado via par�metro.
	 *
	 * @param request
	 * @param bean
	 * @return
	 * @author Rodrigo Freitas
	 * @since 02/04/2013
	 */
	public void gerarArquivo(WebRequestContext request, ArquivoemporiumFiltro bean){
		try{
			if(bean.getArquivoemporiumtipo() != null){
				Date dtmovimento1 = bean.getDtmovimento1() != null ? bean.getDtmovimento1() : SinedDateUtils.currentDate();
				Date dtmovimento2 = bean.getDtmovimento2() != null ? bean.getDtmovimento2() : SinedDateUtils.currentDate();
				
				String stringConexaoBanco = SinedUtil.getStringConexaoBanco();
				
				List<Empresa> listaEmpresa = empresaService.findForIntegracaoEmporium();
				if(bean.getEmpresa() == null){
					listaEmpresa = empresaService.findForIntegracaoEmporium();
				} else {
					listaEmpresa = new ArrayList<Empresa>();
					listaEmpresa.add(empresaService.load(bean.getEmpresa(), "empresa.cdpessoa, empresa.integracaopdv"));
				}
				
				
				if(bean.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.IMPORTACAO_PRODUTOS)){
					IntegracaoEmporiumImportacaoProdutosUtil.util.criarArquivoImportacao(stringConexaoBanco, bean.getMaterial() != null ? bean.getMaterial().getCdmaterial() : null, bean.getSubstuicaotabela() != null ? bean.getSubstuicaotabela() : false);
				} else if(bean.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.IMPORTACAO_CLIENTES)){
					IntegracaoEmporiumImportacaoClientesUtil.util.criarArquivoImportacao(stringConexaoBanco, bean.getCliente() != null ? bean.getCliente().getCdpessoa() : null);
				} else {
					while(SinedDateUtils.beforeOrEqualIgnoreHour(dtmovimento1, dtmovimento2)){
						for (Empresa empresa : listaEmpresa) {
							if(bean.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_MOVIMENTODETALHADO)){
								IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoMovimentos(stringConexaoBanco, dtmovimento1, empresa);
							} else if(bean.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISGERAIS)){
								IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoFiscaisGerais(stringConexaoBanco, dtmovimento1, empresa);
							} else if(bean.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_DADOSFISCAISTRIBUTACAO)){
								IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoFiscaisTributacao(stringConexaoBanco, dtmovimento1, empresa);
							} else if(bean.getArquivoemporiumtipo().equals(Arquivoemporiumtipo.EXPORTACAO_CLIENTES)){
								IntegracaoEmporiumExportacaoMovimentosUtil.util.criarArquivoExportacaoClientes(stringConexaoBanco, empresa);
							}
						}
						
						dtmovimento1 = SinedDateUtils.addDiasData(dtmovimento1, 1);
					}
				} 
				
				request.addMessage("Arquivo(s) gerado(s) com sucesso.");
			} else {
				request.addError("N�o foi poss�vel encontrar o tipo de arquivo a ser gerado.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro na gera��o do arquivo: " + e.getMessage());
		}
		
		IntegracaoEmporiumUtil.util.fechaPopUpExecutaEmporium(request);
	}
	
	public ModelAndView processaExportacoesPendentes(WebRequestContext request){
		try{
			arquivoemporiumService.processaExportacoesPendentes();
			request.addMessage("O processamento foi finalizado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro no processamento de arquivo de importa��o: " + e.getMessage());
		}
		return sendRedirectToAction("listagem");
	}
	
	
	public ModelAndView processaVendasPendentes(WebRequestContext request){
		try{
			emporiumvendaService.processaVendasPendentes(null, false, null, null);
			request.addMessage("O processamento foi finalizado com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro no processamento das vendas: " + e.getMessage());
		}
		return sendRedirectToAction("listagem");
	}
	
}
