package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Campolistagem;
import br.com.linkcom.sined.geral.bean.Tela;
import br.com.linkcom.sined.geral.service.TelaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TelaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Tela", authorizationModule=CrudAuthorizationModule.class)
public class TelaCrud extends CrudControllerSined<TelaFiltro, Tela, Tela> {
	
	private TelaService telaService;	
	
	public void setTelaService(TelaService telaService) {
		this.telaService = telaService;
	}	

	@Override
	protected void entrada(WebRequestContext request, Tela form) throws Exception {

	}
	
	@Override
	protected void listagem(WebRequestContext request, TelaFiltro filtro) throws Exception {
		request.setAttribute("listaModulosTela", Tela.getListaModulos());
	}	
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Tela form)throws CrudException {
		throw new RuntimeException("N�o � permitido criar registros de tela.");
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request, Tela form)	throws CrudException {
		throw new RuntimeException("N�o � permitido excluir registros de tela.");
	}
	
	@Override
	protected void salvar(WebRequestContext request, Tela bean)	throws Exception {
		if(bean.getListaCampolistagem() != null && !bean.getListaCampolistagem().isEmpty()){			
			for(Campolistagem campolistagem : bean.getListaCampolistagem()){
				if(campolistagem.getNome() != null){
					campolistagem.setNome(Util.strings.tiraAcento(campolistagem.getNome().toLowerCase()));
					if(campolistagem.getExibir() == null)
						campolistagem.setExibir(Boolean.FALSE);
				}
			}
		}
		super.salvar(request, bean);
		telaService.clearTelaCache();
	}
	
}
