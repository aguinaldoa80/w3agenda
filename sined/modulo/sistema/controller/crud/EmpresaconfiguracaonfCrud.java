package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonf;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmpresaconfiguracaonfFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Empresaconfiguracaonf", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class EmpresaconfiguracaonfCrud extends CrudControllerSined<EmpresaconfiguracaonfFiltro, Empresaconfiguracaonf,Empresaconfiguracaonf> {
	

}
