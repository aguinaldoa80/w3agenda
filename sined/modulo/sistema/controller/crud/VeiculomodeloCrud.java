package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Veiculomodelo;
import br.com.linkcom.sined.geral.service.VeiculomodeloService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VeiculomodeloFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Veiculomodelo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "descricao", "fornecedor", "limiteodometro"})
public class VeiculomodeloCrud extends CrudControllerSined<VeiculomodeloFiltro, Veiculomodelo, Veiculomodelo>{
	
	private VeiculomodeloService veiculomodeloService;
	
	public void setVeiculomodeloService(
			VeiculomodeloService veiculomodeloService) {
		this.veiculomodeloService = veiculomodeloService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Veiculomodelo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_VEICULOMODELO_NOME")) {
				throw new SinedException("Modelo j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Veiculomodelo bean) throws Exception {
		try {
			super.excluir(request, bean);
		} catch (SinedException e) {
			throw new SinedException("Modelo de ve�culo n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected Veiculomodelo criar(WebRequestContext request, Veiculomodelo form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdveiculomodelo() != null){
			form = veiculomodeloService.loadForEntrada(form);
			
			form.setCdveiculomodelo(null);
			form.setDtaltera(null);
			form.setCdusuarioaltera(null);
			
			
			return form;
		} else {
			return super.criar(request, form);
		}
	}
	
}
