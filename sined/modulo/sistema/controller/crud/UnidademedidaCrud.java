package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.UnidadeMedidaHistorico;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.service.UnidadeMedidaHistoricoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UnidademedidaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Unidademedida", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "simbolo", "ativo"})
public class UnidademedidaCrud extends CrudControllerSined<UnidademedidaFiltro, Unidademedida, Unidademedida>{
	
	private UnidademedidaService unidademedidaService;
	private UnidadeMedidaHistoricoService unidadeMedidaHistoricoService;
	
	public void setUnidadeMedidaHistoricoService(
			UnidadeMedidaHistoricoService unidadeMedidaHistoricoService) {
		this.unidadeMedidaHistoricoService = unidadeMedidaHistoricoService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}

	@Override
	protected void entrada(WebRequestContext request, Unidademedida unidademedida)
			throws Exception {
		if(unidademedida != null && unidademedida.getCdunidademedida() != null) {
			unidademedida.setListaUnidadeMedidaHistorico(unidadeMedidaHistoricoService.findByUnidadeMedidaHistorico(unidademedida));
		}
		super.entrada(request, unidademedida);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Unidademedida bean) throws Exception {
		
		boolean criar = bean.getCdunidademedida() == null;
		Unidademedida unidademedidaAntiga = null;
		
		if(!criar){
			unidademedidaAntiga = unidademedidaService.loadForEntrada(bean);
			if (unidademedidaAntiga != null){
			unidadeMedidaHistoricoService.preencherHistorico(bean, unidademedidaAntiga, criar);
			}		
		}

		try {
			bean.setSimbolo(bean.getSimbolo().trim());
			bean.setNome(bean.getNome().trim());
			super.salvar(request, bean);
			if(criar) unidadeMedidaHistoricoService.preencherHistorico(bean, unidademedidaAntiga, criar);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_UNIDADEMEDIDA_NOME")) {
				throw new SinedException("Unidade de Medida j� cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "IDX_UNIDADEMEDIDA_SIMBOLO")) {
				throw new SinedException("S�mbolo j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void validateBean(Unidademedida bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaUnidademedidaconversao())){
			List<Unidademedida> listaUnidade = new ArrayList<Unidademedida>();
			if(bean.getCdunidademedida() != null){
				listaUnidade.add(bean);
			}
			
			for(Unidademedidaconversao unidademedidaconversao : bean.getListaUnidademedidaconversao()){
				if(unidademedidaconversao.getUnidademedidarelacionada() != null){
					if(listaUnidade.contains(unidademedidaconversao.getUnidademedidarelacionada())){
						errors.reject("001", "N�o � permitido cadastrar a mesma unidade de medida na convers�o de unidade");
					}
					listaUnidade.add(unidademedidaconversao.getUnidademedidarelacionada());
				}
				if((unidademedidaconversao.getFracao() == null || unidademedidaconversao.getFracao() == 0) ||
						(unidademedidaconversao.getQtdereferencia() == null || unidademedidaconversao.getQtdereferencia() == 0)){
					errors.reject("001", "N�o � permitido cadastrar a fra��o e/ou qtde refer�ncia com valores zerados.");
				}
			}
		}
		
		if(!errors.hasErrors()){
			unidademedidaService.validaConversoesUnidade(bean, errors);
		}
		super.validateBean(bean, errors);
	}
	
	public ModelAndView visualizarUnidadeMedidaHistorico(WebRequestContext request, UnidadeMedidaHistorico unidadeMedidaHistorico){
		
		unidadeMedidaHistorico = unidadeMedidaHistoricoService.loadForEntrada(unidadeMedidaHistorico);
		
		request.setAttribute("TEMPLATE_beanName", unidadeMedidaHistorico);
		request.setAttribute("TEMPLATE_beanClass", UnidadeMedidaHistorico.class);
		request.setAttribute("descricao", "Hist�rico");
		request.setAttribute("consultar", true);

		return new ModelAndView("direct:crud/popup/visualizacaoHistoricoUnidadeMedida").addObject("unidadeMedidaHistorico", unidadeMedidaHistorico);
	}

}
