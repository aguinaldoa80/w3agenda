package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Ajusteicmstipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AjusteicmstipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Ajusteicmstipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "apuracao", "tipoajuste", "codigo", "uf"})
public class AjusteicmstipoCrud extends CrudControllerSined<AjusteicmstipoFiltro, Ajusteicmstipo, Ajusteicmstipo> {
	
	@Override
	protected void salvar(WebRequestContext request, Ajusteicmstipo bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_ajusteicmstipo_unique")) 
				throw new SinedException("Tipo de ajuste de ICMS j� cadastrado no sistema");			
		}
	}
	
}