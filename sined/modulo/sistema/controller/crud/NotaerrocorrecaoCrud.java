package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.Arrays;
import java.util.List;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.NotaErroCorrecaoHistorico;
import br.com.linkcom.sined.geral.bean.Notaerrocorrecao;
import br.com.linkcom.sined.geral.bean.enumeration.NotaerrocorrecaoHistoricoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Notaerrocorrecaotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.service.NotaErroCorrecaoHistoricoService;
import br.com.linkcom.sined.geral.service.NotaerrocorrecaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.NotaErroCorrecaoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Notaerrocorrecao"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"codigo", "prefixo", "tipo", "url", "mensagem", "correcao"})
public class NotaerrocorrecaoCrud extends CrudControllerSined<NotaErroCorrecaoFiltro, Notaerrocorrecao, Notaerrocorrecao> {

	private NotaerrocorrecaoService notaerrocorrecaoService;
	private NotaErroCorrecaoHistoricoService notaErroCorrecaoHistoricoService;

	public void setNotaerrocorrecaoService(NotaerrocorrecaoService notaerrocorrecaoService) {this.notaerrocorrecaoService = notaerrocorrecaoService;}
	public void setNotaErroCorrecaoHistoricoService(NotaErroCorrecaoHistoricoService notaErroCorrecaoHistoricoService) {this.notaErroCorrecaoHistoricoService = notaErroCorrecaoHistoricoService;}

	
	@Override
	protected void validateBean(Notaerrocorrecao bean, BindException errors) {
		if (notaerrocorrecaoService.existeCodigoPrefixoNotaerrocorrecao(bean) && bean.getCdnotaerrocorrecao() == null) {
			errors.reject("001","Esse link de corre��o j� existe.");
		}
		
		if (bean.getCodigo() == null) {
			errors.reject("001","O campo C�DIGO � obrigat�rio.");
		}else if (bean.getPrefixo() == null && Notaerrocorrecaotipo.NOTA_FISCAL_SERVICO.equals(bean.getTipo())) {
			errors.reject("002","O campo PREFIXO � obrigat�rio.");
		}else if (bean.getUrl() == null) {
			errors.reject("003","O campo URL � obrigat�rio.");
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Notaerrocorrecao notaerrocorrecao)
			throws Exception {
		boolean alterado = notaerrocorrecao.getCdnotaerrocorrecao() != null;

		NotaErroCorrecaoHistorico notaErroCorrecaoHistorico = new NotaErroCorrecaoHistorico();
		notaerrocorrecao.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		notaerrocorrecao.setDtaltera(SinedDateUtils.currentTimestamp());
		notaErroCorrecaoHistorico.setDataaltera(SinedDateUtils.currentTimestamp());
		notaErroCorrecaoHistorico.setResponsavel(SinedUtil.getUsuarioLogado().getNome());
		notaErroCorrecaoHistorico.setNotaerrocorrecao(notaerrocorrecao);
			
		if(alterado){
			Notaerrocorrecao notaerrocorrecaoAntiga = notaerrocorrecaoService.load(notaerrocorrecao);
			StringBuilder campos = new StringBuilder();
			if(!notaerrocorrecaoAntiga.getCodigo().equals(notaerrocorrecao.getCodigo())) {
				campos.append(" C�digo");
			}
			if(!Util.strings.emptyIfNull(notaerrocorrecaoAntiga.getPrefixo()).equals(notaerrocorrecao.getPrefixo())) {
				if(campos.length()>0) campos.append(",");
				campos.append(" Prefixo");
			}
			if(notaerrocorrecaoAntiga.getTipo() != null && !notaerrocorrecaoAntiga.getTipo().equals(notaerrocorrecao.getTipo())) {
				if(campos.length()>0) campos.append(",");
				campos.append(" Tipo");
			}
			if(!Util.strings.emptyIfNull(notaerrocorrecaoAntiga.getUrl()).equals(notaerrocorrecao.getUrl())) {
				if(campos.length()>0) campos.append(",");
				campos.append(" URL");
			}
			if(!Util.strings.emptyIfNull(notaerrocorrecaoAntiga.getMensagem()).equals(notaerrocorrecao.getMensagem())) {
				if(campos.length()>0) campos.append(",");
				campos.append(" Mensagem");
			}
			if(!Util.strings.emptyIfNull(notaerrocorrecaoAntiga.getCorrecao()).equals(notaerrocorrecao.getCorrecao())){
				if(campos.length()>0) campos.append(",");
				campos.append(" Corre��o");
			}
			notaErroCorrecaoHistorico.setAcaoexecutada(NotaerrocorrecaoHistoricoEnum.ALTERADO);
			notaErroCorrecaoHistorico.setObservacao("Altera��o no(s) campo(s)"+campos.toString()+".");
		}else{
			notaErroCorrecaoHistorico.setAcaoexecutada(NotaerrocorrecaoHistoricoEnum.CRIADO);
			notaErroCorrecaoHistorico.setObservacao("Cria��o do Registro");
		}
		notaerrocorrecaoService.saveOrUpdate(notaerrocorrecao);
		notaErroCorrecaoHistoricoService.saveOrUpdate(notaErroCorrecaoHistorico);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Notaerrocorrecao form) throws Exception {
		
		List<Prefixowebservice> prefixos = Arrays.asList(Prefixowebservice.values());
		
		String prefixosString = "Informar o fornecedor do Webservice. Ex: para NFS-e de Belo Horizonte o fornecedor � bhiss.<br>" +
								"Para NF-e o campo prefixo n�o deve ser preenchido.<br><br>" +
								"Lista de Webservices dispon�veis para o Werp:<br>";
		
		prefixosString = prefixosString + Prefixowebservice.getAllPrefixos(prefixos);
		
		System.out.println(prefixosString);
		
		request.setAttribute("prefixos", prefixosString);
		
		super.entrada(request, form);
	}
}	

