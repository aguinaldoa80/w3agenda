package br.com.linkcom.sined.modulo.sistema.controller.crud;


import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.OtrClassificacaoCodigo;
import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.geral.bean.OtrPneuTipoOtrClassificacaoCodigo;
import br.com.linkcom.sined.geral.service.OtrClassificacaoCodigoService;
import br.com.linkcom.sined.geral.service.OtrConstrucaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.OtrPneuTipoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@ExportCSV(fields={"cdOtrPneuTipo", "nome","codigoIntegracao", "codigoStr"})
@Controller(path="/sistema/crud/OtrPneuTipo", authorizationModule=CrudAuthorizationModule.class)
public class OtrPneuTipoCrud extends CrudControllerSined<OtrPneuTipoFiltro, OtrPneuTipo, OtrPneuTipo> {
	
	
	protected OtrConstrucaoService otrConstrucaoService;
	protected OtrClassificacaoCodigoService otrClassificacaoCodigoService;
	
	
	public void setOtrConstrucaoService(OtrConstrucaoService otrConstrucaoService) {
		this.otrConstrucaoService = otrConstrucaoService;
	}

	public void setOtrClassificacaoCodigoService(
			OtrClassificacaoCodigoService otrClassificacaoCodigoService) {
		this.otrClassificacaoCodigoService = otrClassificacaoCodigoService;
	}

	
	@Override
	protected void validateBean(OtrPneuTipo bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaOtrPneuTipoOtrClassificacaoCodigo())){
			for(OtrPneuTipoOtrClassificacaoCodigo otrPneuTipoOtrClassificacaoCodigo : bean.getListaOtrPneuTipoOtrClassificacaoCodigo()){
				if(otrPneuTipoOtrClassificacaoCodigo.getOtrClassificacaoCodigo() == null){
					errors.reject("001","Informe um C�digo de Classifica��o OTR");
				}
			}
		}
	}
	
	
	@Override
	protected void entrada(WebRequestContext request, OtrPneuTipo bean)throws Exception {
		request.setAttribute("construcoes", otrConstrucaoService.findConstrucao());
		OtrClassificacaoCodigo otrClassificacaoCodigo;
		if(bean.getCdOtrPneuTipo() != null){
			if(bean.getListaOtrPneuTipoOtrClassificacaoCodigo()!=null){
				for(OtrPneuTipoOtrClassificacaoCodigo otrpneutipootrclassificacaocodigo : bean.getListaOtrPneuTipoOtrClassificacaoCodigo()){
					if(otrpneutipootrclassificacaocodigo.getOtrClassificacaoCodigo() !=null){
						otrClassificacaoCodigo = otrClassificacaoCodigoService.loadForEntrada(otrpneutipootrclassificacaocodigo.getOtrClassificacaoCodigo());
						if(otrClassificacaoCodigo.getOtrDesenho()!=null){
							otrpneutipootrclassificacaocodigo.setDesenhoStr(otrClassificacaoCodigo.getOtrDesenho().getDesenho());
						}
						if(otrClassificacaoCodigo.getOtrServicoTipo()!=null){
							otrpneutipootrclassificacaocodigo.setTipoServicoStr(otrClassificacaoCodigo.getOtrServicoTipo().getTipoServico());
						}
					}
				}
			}
		}
	}
	
	public ModelAndView ajaxCarregaDesenhoServicoTipo(WebRequestContext request, OtrClassificacaoCodigo bean){
		if(bean != null && bean.getCdOtrClassificacaoCodigo() != null){
			OtrClassificacaoCodigo otrClassificacaoCodigo = otrClassificacaoCodigoService.loadForEntrada(bean);
			JsonModelAndView json = new JsonModelAndView(); 
			json.addObject("desenhoStr", otrClassificacaoCodigo.getOtrDesenho().getDesenho());
			json.addObject("tipoServicoStr", otrClassificacaoCodigo.getOtrServicoTipo().getTipoServico());
		return json;
		}
		return null;
	}
}
