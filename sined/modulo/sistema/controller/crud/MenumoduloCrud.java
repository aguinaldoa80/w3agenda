package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path={"/sistema/crud/Menumodulo"}, authorizationModule=CrudAuthorizationModule.class)
public class MenumoduloCrud extends CrudControllerSined<FiltroListagemSined, Menumodulo, Menumodulo> {	
	
}
