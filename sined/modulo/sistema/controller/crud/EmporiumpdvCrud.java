package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path="/sistema/crud/Emporiumpdv", authorizationModule=CrudAuthorizationModule.class)
public class EmporiumpdvCrud extends CrudControllerSined<FiltroListagemSined, Emporiumpdv, Emporiumpdv> {
	
	@Override
	protected void validateBean(Emporiumpdv bean, BindException errors) {
		if(bean.getDataInicioGerarNfce() != null && SinedDateUtils.beforeIgnoreHour(bean.getDataInicioGerarNfce(), 
				SinedDateUtils.addMesData(new Date(System.currentTimeMillis()), -2))){
			
			errors.reject("001", "A data do campo 'CONSIDERAR NFC-E GERADAS A PARTIR DE' n�o pode ser anterior � 2 meses da data atual.");
		}
	}
}