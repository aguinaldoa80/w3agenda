package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PneuSegmentoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneuSegmentoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@ExportCSV(fields={"cdPneuSegmento", "nome", "ativo", "principal", "otr"})
@Controller(path="/sistema/crud/PneuSegmento", authorizationModule=CrudAuthorizationModule.class)
public class PneuSegmentoCrud extends CrudControllerSined<PneuSegmentoFiltro,PneuSegmento,PneuSegmento> {
	
	PneuSegmentoService pneuSegmentoService;
	ParametrogeralService parametroGeralService;
	
	public void setPneuSegmentoService(PneuSegmentoService pneuSegmentoService) {
		this.pneuSegmentoService = pneuSegmentoService;
	}


	public void setParametroGeralService(ParametrogeralService parametroGeralService) {
		this.parametroGeralService = parametroGeralService;
	}


	@Override
	protected void entrada(WebRequestContext request, PneuSegmento form) throws Exception {
		if(form.getCdPneuSegmento() == null){
			form.setMarca(true);
			form.setModelo(true);
			form.setMedida(true);
			form.setSerieFogo(true);
			form.setAcompanhaRoda(true);
			form.setAtivo(true);
		}
		super.entrada(request, form);
	}

	@Override
	protected void validateBean(PneuSegmento bean, BindException errors) {
		if(bean.getPrincipal()){
			Boolean validacao = pneuSegmentoService.validaPrincipalAtivo(bean);
			if(validacao){
				errors.reject("001","J� existe um segmento ativo e definido como principal.");
			}else{
				if(!bean.getAtivo()){
					errors.reject("003","O principal deve ser ativo.");
				}
			}
		}
		if(bean.getOtr()){
			Boolean configuracaoOtr = SinedUtil.isSegmentoOtr();
			if(!configuracaoOtr){
				errors.reject("002","N�o � poss�vel cadastrar um segmento OTR. Para essa configura��o � necess�rio habilitar o par�metro HABILITAR_CONFIGURACAO_OTR. ATEN��O: Isso s� pode ser feito como aux�lio do setor de produ��o.");
			}
		}
	}
}
