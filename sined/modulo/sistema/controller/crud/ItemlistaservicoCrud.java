package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Itemlistaservico;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ItemlistaservicoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Itemlistaservico", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"codigo", "descricao"})
public class ItemlistaservicoCrud extends CrudControllerSined<ItemlistaservicoFiltro, Itemlistaservico, Itemlistaservico> {
	
}
