package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Campoobrigatorio;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CampoobrigatorioFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Campoobrigatorio", authorizationModule=CrudAuthorizationModule.class)
public class CampoobrigatorioCrud extends CrudControllerSined<CampoobrigatorioFiltro, Campoobrigatorio, Campoobrigatorio> {
	
}
