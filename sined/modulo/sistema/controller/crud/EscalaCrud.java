package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Escala;
import br.com.linkcom.sined.geral.bean.Escalahorario;
import br.com.linkcom.sined.geral.bean.enumeration.DiaSemana;
import br.com.linkcom.sined.geral.service.EscalaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EscalaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Escala", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "diastrabalho", "diasfolga", "ativo"})
public class EscalaCrud extends CrudControllerSined<EscalaFiltro, Escala, Escala> {
	
	private EscalaService escalaService;
	
	public void setEscalaService(EscalaService escalaService) {
		this.escalaService = escalaService;
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Escala form) {
		if(request.getParameter("fromPopUp") != null && request.getParameter("fromPopUp").toUpperCase().equals("TRUE")){
			request.setAttribute("fromPopUp", Boolean.TRUE);
		}
		return super.getEntradaModelAndView(request, form);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request,	Escala bean) {
		if(request.getParameter("fromPopUp") != null && request.getParameter("fromPopUp").toUpperCase().equals("TRUE")){
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = window.opener.location.href;window.close();</script>");
			return null;
		}
		return super.getSalvarModelAndView(request, bean);
	}
	@Override
	protected void salvar(WebRequestContext request, Escala bean)throws Exception {
		try {
			genericService.saveOrUpdate(bean);
			if(request.getParameter("fromPopUp") == null || request.getParameter("fromPopUp").toUpperCase().equals("FALSE")){								
				request.addMessage("Registro salvo com sucesso.");
			}		
			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_ESCALA_NOME")) {
				throw new SinedException("Escala j� cadastrada no sistema.");
			}else{
				throw new SinedException("A(s) escala(s) de hor�rio n�o pode ser exclu�do(a), j� possui refer�ncias em outros registros do sistema.");
			}
		}
		
	}
	
	@Override
	protected void entrada(WebRequestContext request, Escala form)	throws Exception {
		List<DiaSemana> lista = new ArrayList<DiaSemana>();
		lista.add(DiaSemana.DOMINGO);
		lista.add(DiaSemana.SEGUNDA);
		lista.add(DiaSemana.TERCA);
		lista.add(DiaSemana.QUARTA);
		lista.add(DiaSemana.QUINTA);
		lista.add(DiaSemana.SEXTA);
		lista.add(DiaSemana.SABADO);
		request.setAttribute("lista1", lista);
		super.entrada(request, form);
	}
	
	/**
	 * M�todo respons�vel por gerar a escala de hor�rio.
	 * @param request
	 * @param bean
	 * @return
	 * @throws CrudException
	 * @autor Taidson
	 * @since 23/06/2010
	 */
	public ModelAndView gerarEscala(WebRequestContext request, Escala bean) throws CrudException{
		escalaService.prepareGerarEscala(bean);
		return doEntrada(request, bean);
	}
			
		
		
	@Override
	protected void validateBean(Escala bean, BindException errors) {
		if(bean.getListaescalahorario() != null && !bean.getListaescalahorario().isEmpty()){
			boolean achouIntercalado = false;
			for (Escalahorario escalahorario : bean.getListaescalahorario()) {
				if(escalahorario.getHorafim().getTime() <= escalahorario.getHorainicio().getTime()){
					errors.reject("001", "Hora final deve ser maior que hora inicial.");
					break;
				}
				
				for (Escalahorario escalahorarioAux : bean.getListaescalahorario()) {
					if(escalahorario.getDiasemana() == null || (escalahorario.getDiasemana() != null && escalahorario.getDiasemana().equals(escalahorarioAux.getDiasemana()))){
						if((escalahorario.getHorainicio().before(escalahorarioAux.getHorafim()) && escalahorario.getHorainicio().after(escalahorarioAux.getHorainicio())) || 
						   (escalahorario.getHorafim().before(escalahorarioAux.getHorafim()) && escalahorario.getHorafim().after(escalahorarioAux.getHorainicio()))){
							errors.reject("002", "Existem hor�rios intercalados.");
							achouIntercalado = true;
							break;
						}
					}
				}
				if(achouIntercalado)
					break;
			}
		}
	}
	
}
