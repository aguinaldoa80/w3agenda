package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Tabelavalor;
import br.com.linkcom.sined.geral.bean.Tabelavalorhistorico;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.TabelavalorService;
import br.com.linkcom.sined.geral.service.TabelavalorhistoricoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TabelavalorFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Tabelavalor"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"identificador", "descricao", "valor"})
public class TabelavalorCrud extends CrudControllerSined<TabelavalorFiltro, Tabelavalor, Tabelavalor> {	

	private TabelavalorService tabelavalorService; 
	private TabelavalorhistoricoService tabelavalorhistoricoService;
	private MaterialService materialService;
	
	public void setTabelavalorService(TabelavalorService tabelavalorService) {
		this.tabelavalorService = tabelavalorService;
	}
	public void setTabelavalorhistoricoService(TabelavalorhistoricoService tabelavalorhistoricoService) {
		this.tabelavalorhistoricoService = tabelavalorhistoricoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Tabelavalor form)	throws Exception {
		if(form.getCdtabelavalor() != null){
			form.setListaTabelavalorhistorico(tabelavalorhistoricoService.findByTabelavalor(form));
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Tabelavalor bean) throws Exception {
		boolean fromEditar = bean.getCdtabelavalor() != null;
		String observacao = null;
		
		if(fromEditar && bean.getValor() != null){
			Tabelavalor tabelavalorAnterior =  tabelavalorService.load(bean, "tabelavalor.cdtabelavalor, tabelavalor.valor");
			if(tabelavalorAnterior != null && tabelavalorAnterior.getValor() != null && tabelavalorAnterior.getValor().compareTo(bean.getValor()) != 0){
				observacao = "Valor anterior: " + tabelavalorAnterior.getValor();
			}
		}
		
		try {
			super.salvar(request, bean);
			
			Tabelavalorhistorico tabelavalorhistorico = new Tabelavalorhistorico();
			tabelavalorhistorico.setTabelavalor(bean);
			tabelavalorhistorico.setAcao(fromEditar ? "Alterado" : "Criado");
			tabelavalorhistorico.setObservacao(observacao);
			tabelavalorhistoricoService.saveOrUpdate(tabelavalorhistorico);
			
			if(Tabelavalor.CUSTO_OPERACIONAL_ADMINISTRATIVO.equalsIgnoreCase(bean.getIdentificador()) || 
					Tabelavalor.CUSTO_COMERCIAL_ADMINISTRATIVO.equalsIgnoreCase(bean.getIdentificador())){
				materialService.atualizarValorcusto(request, bean.getIdentificador());
			}
			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_TABELAVALOR_IDENTIFICADOR")) {
				throw new SinedException("Identificador j� cadastrado no sistema.");
			} else {
				throw new SinedException(e.getMessage());
			}
		}
		
	}
	
}
