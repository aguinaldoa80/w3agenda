package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmentoImportBean;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRetornoSegmentoService;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRetornoSegmentoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/sistema/crud/BancoConfiguracaoRetornoSegmento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"banco", "nome", "identificador", "tipo"})
public class BancoConfiguracaoRetornoSegmentoCrud extends CrudControllerSined<BancoConfiguracaoRetornoSegmentoFiltro, BancoConfiguracaoRetornoSegmento, BancoConfiguracaoRetornoSegmento> {

	private BancoService bancoService;
	private BancoConfiguracaoRetornoSegmentoService bancoConfiguracaoRetornoSegmentoService;
	
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setBancoConfiguracaoRetornoSegmentoService(BancoConfiguracaoRetornoSegmentoService bancoConfiguracaoRetornoSegmentoService) {
		this.bancoConfiguracaoRetornoSegmentoService = bancoConfiguracaoRetornoSegmentoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, BancoConfiguracaoRetornoSegmento form) throws Exception {
		request.setAttribute("listaBanco", bancoService.findAtivos());
		super.entrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, BancoConfiguracaoRetornoSegmentoFiltro filtro) throws Exception {
		request.setAttribute("listaBanco", bancoService.findAtivos());
		super.listagem(request, filtro);
	}
	
	public ModelAndView exportarArquivo(WebRequestContext request, BancoConfiguracaoRetornoSegmentoFiltro filtro) throws CrudException, IOException {
		ByteArrayOutputStream conteudoZip = new ByteArrayOutputStream();
		bancoConfiguracaoRetornoSegmentoService.processaExportar(request, filtro, conteudoZip);
		
		HttpServletResponse response = request.getServletResponse();
		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", "attachment; filename =\"bancoconfigsegmentoretorno.zip\";");
		response.getOutputStream().write(conteudoZip.toByteArray());
				
		return null;
	}
	
	/**
	 * Abrindo o popup para importa��o de Arquivos
	 * @param request
	 * @since 08/06/2016
	 * @author C�sar
	 */
	public ModelAndView abrePopUpImportar(WebRequestContext request){		
		BancoConfiguracaoSegmentoImportBean bancoConfiguracaoSegmentoArquivosImportBean = new BancoConfiguracaoSegmentoImportBean();
		return new ModelAndView("direct:/crud/popup/bancoConfigSegImportArquivo","BancoConfiguracaoSegmentoImportBean", bancoConfiguracaoSegmentoArquivosImportBean);
	}
	
	/**
	 * Upload/Processando do arquivos
	 * @param request , arquivosImportBean
	 * @author C�sar
	 * @since 08/06/2016
	 */
	public void upload(WebRequestContext request, BancoConfiguracaoSegmentoImportBean arquivosImportBean) throws CrudException, IOException {
		String mensagemErro = "";
		Boolean semArquivos = Boolean.FALSE;
		
		if(arquivosImportBean == null 
				|| arquivosImportBean.getListaAquivosImport() == null 
				|| arquivosImportBean.getListaAquivosImport().get(0).getArquivo() == null){
			semArquivos = Boolean.TRUE;
			request.addError("N�o � poss�vel realizar a importa��o sem selecionar os arquivos.");
		}
		
		if(!semArquivos && arquivosImportBean != null && !arquivosImportBean.getListaAquivosImport().isEmpty())	{
			
			mensagemErro = bancoConfiguracaoRetornoSegmentoService.importacaoDados(arquivosImportBean.getListaAquivosImport(), request);
			
			if(!mensagemErro.isEmpty()){	
				request.addError("Erro ao importar arquivos.");
				for(String erro : mensagemErro.split("<br>")){
					request.addError(erro);
				}
			} else{
				request.addMessage("Arquivos importados com sucesso.");
			}
		}
		
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>parent.doFilter();</script>");				
	}
	
}
