package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CentrocustoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Centrocusto", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class CentrocustoCrud extends CrudControllerSined<CentrocustoFiltro, Centrocusto, Centrocusto> {
	
	private CentrocustoService centrocustoService;
	private ContratoService contratoService;
	private AgendamentoService agendamentoService;
	
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;	}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService; }
	public void setAgendamentoService(AgendamentoService agendamentoService) { this.agendamentoService = agendamentoService; }
	
	@Override
	protected void salvar(WebRequestContext request, Centrocusto bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_centrocusto_nome")) {
				throw new SinedException("Nome de centro de custo j� cadastrado no sistema.");
			}			
		}		
	}	
	
	@Override
	protected void listagem(WebRequestContext request, CentrocustoFiltro filtro) throws Exception {
		//filtro.setAtivo(true);
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Centrocusto form) throws Exception {
		if (form.getCdcentrocusto() == null) {
			form.setAtivo(true);
		}
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(Centrocusto bean, BindException errors) {
		
		if(bean.getAtivo() == null || bean.getAtivo() == false){
			//Valida��es para encontrar relacionamentos do centro de custo
			List<GenericBean> listaResultados  = centrocustoService.ocorrenciaCentrocusto(bean.getCdcentrocusto());
			List<Contrato> listaContratos = contratoService.findByCentroCustoValidacao(bean);
			List<Agendamento> listaAgendamento = agendamentoService.findByCentroCustoValidacao(bean);
			StringBuilder mensagem = new StringBuilder();
			
			if(SinedUtil.isListNotEmpty(listaResultados)){
					mensagem.append("Este centro de custo est� sendo utilizado nas configura��es de : ");		
				for (GenericBean genericBean : listaResultados) {
					mensagem.append("<br>" + genericBean.getId() +" no campo " + genericBean.getValue());
				}
				mensagem.append("<br>Altere o(s) cadastro(s) da(s) configura��o(�es) antes de inativar este centro de custo.<br><br>");
			}
			if(SinedUtil.isListNotEmpty(listaContratos)){
				Integer cont = 0;
				mensagem.append("Este centro de custo est� sendo utilizado no(s) contrato(s) : ");
				for (Contrato contrato : listaContratos) {
					if(contrato.getAux_contrato() != null
							&& contrato.getAux_contrato().getSituacao() != null 
							&& !SituacaoContrato.FINALIZADO.equals(contrato.getAux_contrato().getSituacao())){
						
						mensagem.append(contrato.getIdentificadorOrCdcontrato());
						if(cont >= 0)
							mensagem.append(",");
						if(cont % 28 == 0 && cont != 0)
							mensagem.append("<BR>");
						cont++;
					}
				}
				mensagem.delete(mensagem.length() - 1, mensagem.length());
				mensagem.append(". <BR>Altere o rateio do(s) contrato(s) antes de inativar o centro de custo.<BR><BR>");
			}
			if(SinedUtil.isListNotEmpty(listaAgendamento)){
				Integer cont = 0;
				mensagem.append("Este centro de custo est� sendo utilizado no(s) agendamento(s)  : ");
				for (Agendamento agendamento : listaAgendamento) {
					if(agendamento.getAux_agendamento() != null
							&& agendamento.getAux_agendamento() != null
							&& 5 != agendamento.getAux_agendamento().getSituacao()){
						
						mensagem.append(agendamento.getCdagendamento());
						if(cont >= 0)
							mensagem.append(",");
						if(cont % 28 == 0 && cont != 0)
							mensagem.append("<BR>");
						cont++;
					}
				}
				mensagem.delete(mensagem.length() - 1, mensagem.length());
				mensagem.append(".<BR> Altere o rateio do(s) agendamento(s) antes de inativar o centro de custo.");
			}
			if(mensagem.length() > 0)
				errors.reject("002", mensagem.toString());
		}
		
		super.validateBean(bean, errors);
	}
}
