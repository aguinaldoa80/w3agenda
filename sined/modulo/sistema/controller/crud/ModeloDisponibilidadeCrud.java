package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.ModeloDisponibilidade;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ModeloDisponibilidadeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/ModeloDisponibilidade"}, authorizationModule=CrudAuthorizationModule.class)
public class ModeloDisponibilidadeCrud extends CrudControllerSined<ModeloDisponibilidadeFiltro, ModeloDisponibilidade, ModeloDisponibilidade>{
	
	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {this.materialService = materialService;}
	
	@Override
	protected void excluir(WebRequestContext request, ModeloDisponibilidade bean) throws Exception {
		String whereIn = request.getParameter("itenstodelete");
		if(whereIn == null || whereIn.equals("")){
			whereIn = bean.getCdmodelodisponibilidade() + "";
		}
		
		if(materialService.existsMaterialWithModeloDisponibilidade(whereIn)){
			throw new SinedException("Esse Modelo de Disponibilidade j� est� associado a um material/servi�o.");
		}
		
		try {
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException da) {
			throw new SinedException("Modelo de Disponibilidade n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
}
