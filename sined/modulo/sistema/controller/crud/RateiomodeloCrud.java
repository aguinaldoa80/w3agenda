package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Rateiomodelo;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.RateiomodeloService;
import br.com.linkcom.sined.geral.service.TipooperacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.RateiomodeloFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Rateiomodelo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "tipooperacao"})
public class RateiomodeloCrud extends CrudControllerSined<RateiomodeloFiltro, Rateiomodelo, Rateiomodelo>{

	private TipooperacaoService tipooperacaoService;
	private RateiomodeloService rateiomodeloService;
	private ContagerencialService contagerencialService;
	
	public void setTipooperacaoService(TipooperacaoService tipooperacaoService) {
		this.tipooperacaoService = tipooperacaoService;
	}
	public void setRateiomodeloService(RateiomodeloService rateiomodeloService) {
		this.rateiomodeloService = rateiomodeloService;
	}
	public void setContagerencialService(
			ContagerencialService contagerencialService) {
		this.contagerencialService = contagerencialService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Rateiomodelo form)
			throws Exception {
		request.setAttribute("listaOperacoes", tipooperacaoService.findDebitoCredito());
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("tipoOperacao", Tipooperacao.TIPO_CREDITO.equals(form.getTipooperacao())? Tipooperacao.TIPO_CREDITO.getSigla(): Tipooperacao.TIPO_DEBITO.getSigla());
		super.entrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, RateiomodeloFiltro filtro)
			throws Exception {
		request.setAttribute("listaOperacoes", tipooperacaoService.findDebitoCredito());
		super.listagem(request, filtro);
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Rateiomodelo form) {
		return new ModelAndView("crud/rateiomodeloEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, RateiomodeloFiltro filtro) {
		return new ModelAndView("crud/rateiomodeloListagem");
	}
	
	@Override
	protected void validateBean(Rateiomodelo bean, BindException errors) {
		Rateiomodelo modelo = rateiomodeloService.findByNome(bean.getNome());
		if(modelo != null && !modelo.getCdrateiomodelo().equals(bean.getCdrateiomodelo())){
			errors.reject("001","Modelo de rateio j� cadastrado.");
		}
		
		super.validateBean(bean, errors);
	}
	@Override
	protected void salvar(WebRequestContext request, Rateiomodelo bean)
			throws Exception {
		if(bean.getListaRateiomodeloitem() == null || bean.getListaRateiomodeloitem().isEmpty()){
			throw new SinedException("Informe pelo menos uma linha para o modelo de rateio.");
		}
		super.salvar(request, bean);
	}
}