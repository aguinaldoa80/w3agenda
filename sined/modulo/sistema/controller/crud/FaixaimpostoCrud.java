package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Faixaimposto;
import br.com.linkcom.sined.geral.bean.Faixaimpostocliente;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.FaixaimpostoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FaixaimpostoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Faixaimposto", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "controle", "valoraliquota", "localidade", "ufdestino.sigla", "ativo"})
public class FaixaimpostoCrud extends CrudControllerSined<FaixaimpostoFiltro, Faixaimposto, Faixaimposto> {
	
	private CentrocustoService centrocustoService;
	private FaixaimpostoService faixaimpostoService;

	public void setFaixaimpostoService(FaixaimpostoService faixaimpostoService) {
		this.faixaimpostoService = faixaimpostoService;
	}
	public void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Faixaimposto form) throws Exception {
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(form.getCentrocusto()));
	}
	
	@Override
	protected void listagem(WebRequestContext request, FaixaimpostoFiltro filtro) throws Exception {
		
		List<Faixaimpostocontrole> listaControle = new ArrayList<Faixaimpostocontrole>();
		for (Faixaimpostocontrole fic : Faixaimpostocontrole.values()) {
			listaControle.add(fic);
		}
		request.setAttribute("listaControle", listaControle);
	}
	
	@Override
	protected void validateBean(Faixaimposto bean, BindException errors) {
		Set<Faixaimpostocliente> listaFaixaimpostocliente = bean.getListaFaixaimpostocliente();
		Uf uf = bean.getUf();
		Uf ufdestino = bean.getUfdestino();
		Municipio municipio = bean.getMunicipio();
		Faixaimpostocontrole controle = bean.getControle();
		Integer cdfaixaimposto = bean.getCdfaixaimposto();
		
		if(listaFaixaimpostocliente != null && listaFaixaimpostocliente.size() > 0){
			for (Faixaimpostocliente faixaimpostocliente : listaFaixaimpostocliente) {
				Cliente cliente = faixaimpostocliente.getCliente();
				if(faixaimpostoService.haveFaixaimpostoIgualCliente(cdfaixaimposto, controle, uf, ufdestino, municipio, cliente)){
					errors.reject("001", "J� possui uma faixa de imposto para este cliente com UF/Munic�pio de origem e destino iguais.");
					break;
				}
			}
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Faixaimposto bean) throws Exception {
		if(!bean.getControle().equals(Faixaimpostocontrole.ICMS)){
			bean.setIcmsst(null);
			bean.setMva(null);
		}
		if(!bean.getControle().equals(Faixaimpostocontrole.ICMS) && !bean.getControle().equals(Faixaimpostocontrole.FCP)){
			bean.setUfdestino(null);
		}
		
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_FAIXAIMPOSTO_UNIQUE")) 
				throw new SinedException("Faixa de imposto j� cadastrada no sistema.");
		}
	}
	
}
