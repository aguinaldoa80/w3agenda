package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.geral.bean.Fatorajuste;
import br.com.linkcom.sined.geral.bean.Fatorajustevalor;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FatorajusteFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Fatorajuste", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"sigla", "descricao"})
public class FatorajusteCrud extends CrudControllerSined<FatorajusteFiltro, Fatorajuste, Fatorajuste>{

	ContratoService contratoService;
	
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;}

	@Override
	protected void salvar(WebRequestContext request, Fatorajuste bean)throws Exception {
		try {
			bean.setSigla(Util.strings.tiraAcento(bean.getSigla().trim()));
			if(bean.getDescricao() != null)
				bean.setDescricao(bean.getDescricao().trim());
			super.salvar(request, bean);
			contratoService.atualizaValorContratoFatorAjuste();
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_FATORAJUSTE_SIGLA")) {
				throw new SinedException("Fator de ajuste j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void validateBean(Fatorajuste bean, BindException errors) {
		if(bean.getListaFatorajustevalor() != null && !bean.getListaFatorajustevalor().isEmpty()){
			for (Fatorajustevalor fatorajustevalor : bean.getListaFatorajustevalor()) {
				int count = 0;
				for (Fatorajustevalor fatorajustevalorAux : bean.getListaFatorajustevalor()) {
					if(fatorajustevalor.getMesanoAux().equals(fatorajustevalorAux.getMesanoAux()))
						count++;
					if(count > 1){
						errors.reject("001", "Existe mais de um valor cadastrado para o M�s/ano " + fatorajustevalor.getMesanoAux());
						break;
					}
				}
				if(count > 1)
					break;
			}
		}else{
			errors.reject("002", "Fator de ajuste deve ter pelo menos 1 valor associado a ele.");
		}
		
		super.validateBean(bean, errors);
	}
}
