package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Veiculoferiado;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.VeiculoferiadoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Veiculoferiado", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "data"})
public class VeiculoferiadoCrud extends CrudControllerSined<VeiculoferiadoFiltro, Veiculoferiado, Veiculoferiado>{
	
	@Override
	protected void salvar(WebRequestContext request, Veiculoferiado bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_VEICULOFERIADO_DATA")) {
				throw new SinedException("Feriado j� cadastrado no sistema.");
			}
		}
	}
}
