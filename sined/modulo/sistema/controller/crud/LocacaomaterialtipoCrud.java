	package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Locacaomaterialtipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.LocacaomaterialtipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Locacaomaterialtipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class LocacaomaterialtipoCrud extends CrudControllerSined<LocacaomaterialtipoFiltro, Locacaomaterialtipo, Locacaomaterialtipo>{
	
	@Override
	protected void salvar(WebRequestContext request, Locacaomaterialtipo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_LOCACAOMATERIALTIPO_NOME")) {
				throw new SinedException("Tipo de loca��o j� cadastrado no sistema.");
			}
		}
	}
}
