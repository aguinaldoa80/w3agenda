package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.ControleOrcamentoHistorico;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.ControleOrcamentoHistoricoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Controleorcamentosituacao;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ControleOrcamentoHistoricoService;
import br.com.linkcom.sined.geral.service.ControleorcamentoService;
import br.com.linkcom.sined.geral.service.ControleorcamentoitemService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.bean.ControleorcamentoValidacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ControleorcamentoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Controleorcamento", authorizationModule=CrudAuthorizationModule.class)
public class ControleorcamentoCrud extends CrudControllerSined<ControleorcamentoFiltro, Controleorcamento, Controleorcamento>{

	private ControleorcamentoService controleorcamentoService;
	private ControleorcamentoitemService controleorcamentoitemService;
	private EmpresaService empresaService;
	private ControleOrcamentoHistoricoService controleOrcamentoHistoricoService;
	private ContagerencialService contaGerencialService;
	private ProjetoService projetoService;
	
	public void setContaGerencialService(ContagerencialService contaGerencialService) {this.contaGerencialService = contaGerencialService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setControleorcamentoService(ControleorcamentoService controleorcamentoService) {this.controleorcamentoService = controleorcamentoService;}
	public void setControleorcamentoitemService(ControleorcamentoitemService controleorcamentoitemService) {this.controleorcamentoitemService = controleorcamentoitemService;}
	public void setControleOrcamentoHistoricoService(ControleOrcamentoHistoricoService controleOrcamentoHistoricoService) {this.controleOrcamentoHistoricoService = controleOrcamentoHistoricoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	
	@Override
	protected void entrada(WebRequestContext request, Controleorcamento form) throws Exception {		
		if( form.getCdcontroleorcamento() != null && request.getBindException().hasErrors()) {
			form.setListaControleorcamentoitem(controleorcamentoitemService.findByControleOrcamento(form));		
			form.setListacontroleorcamentohistorico(controleOrcamentoHistoricoService.findByControleOrcamento(form));
		}
		if(form.getSituacao() != null){
			form.setSituacaoTransient(form.getSituacao().getNome());
			if(form.getSituacao().equals(Controleorcamentosituacao.APROVADO) || form.getSituacao().equals(Controleorcamentosituacao.INATIVO))		
				request.setAttribute("orcamentoAprovado", true);
		}else{
			if(form.getCdcontroleorcamento() == null){
				form.setSituacao(Controleorcamentosituacao.ABERTO);
				form.setSituacaoTransient(Controleorcamentosituacao.ABERTO.toString());
			}
		}
		request.setAttribute("listaTipoOperacao", Tipooperacao.getListaTipooperacao(false));
		if(Boolean.TRUE.equals(request.getAttribute("replanejando"))){
			form.setCdReplanejado(form.getCdcontroleorcamento());
			form = limparId(form);
		}
		
		request.setAttribute("listaProjetos", projetoService.findProjetosAtivos(form.getProjeto(), false));
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		super.entrada(request, form);
	}
	
	public ModelAndView incluirItens(WebRequestContext request, Controleorcamento form) throws Exception{
		Integer id = form.getCdcontroleorcamento();
		List<Contagerencial> listaContas = new ArrayList<Contagerencial>();
		List<Controleorcamentoitem> listaItens = form.getListaControleorcamentoitem();
		StringBuilder whereInContas = new StringBuilder();
		if(SinedUtil.isListNotEmpty(listaItens)){
			 for (Controleorcamentoitem item : listaItens) {
				if(item.getContagerencial() !=null && item.getContagerencial().getCdcontagerencial()!=null){
					whereInContas.append(item.getContagerencial().getCdcontagerencial()).append(",");
				}
			}
			listaContas = contaGerencialService.buscarUltimoNivelAtivos(form.getTipoOperacao(), whereInContas.length() > 0 ? whereInContas.substring(0, whereInContas.length()-1) : null);
		}else{
			listaContas = contaGerencialService.buscarUltimoNivelAtivos(form.getTipoOperacao(),null);	
		}
		if(SinedUtil.isListNotEmpty(listaContas)){
			for (Contagerencial conta : listaContas) {
				Controleorcamentoitem item = new Controleorcamentoitem();
				item.setContagerencial(conta);
				listaItens.add(item);
			}
		}
		form.setListaControleorcamentoitem(listaItens);
		
		if(SinedUtil.isListNotEmpty(listaItens)){
			request.addMessage("itens adicionados com sucesso.");
		}else {
			request.addMessage("Nenhum item encontrado.", MessageType.INFO);
		}
		if(form.getCdcontroleorcamento()!=null){
			return getEditarModelAndView(request, form);
		}
		return doEntrada(request, form);
	}
	
	private Controleorcamento limparId(Controleorcamento bean){
		bean.setCdcontroleorcamento(null);
		bean.setListacontroleorcamentohistorico(null);
		if(SinedUtil.isListNotEmpty(bean.getListaControleorcamentoitem())){
			for(Controleorcamentoitem item : bean.getListaControleorcamentoitem()){
				item.setCdcontroleorcamentoitem(null);
				item.setValorOrigem(item.getValor());
			}
		}
		return bean;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request,Controleorcamento form) throws CrudException {
		if(Boolean.TRUE.equals(request.getAttribute("replanejando"))){
			request.setAttribute("replanejar", true);
		}
		return super.doEditar(request, form);
	}
	
	
	@Override
	protected void listagem(WebRequestContext request, ControleorcamentoFiltro filtro) throws Exception {
		List<Controleorcamentosituacao> listaControleorcamentosituacao = new ListSet<Controleorcamentosituacao>(Controleorcamentosituacao.class);
		listaControleorcamentosituacao.add(Controleorcamentosituacao.ABERTO);
		listaControleorcamentosituacao.add(Controleorcamentosituacao.APROVADO);
		listaControleorcamentosituacao.add(Controleorcamentosituacao.INATIVO);
		request.setAttribute("listaControleorcamentosituacao", listaControleorcamentosituacao);
		super.listagem(request, filtro);
	}

	@Override
	protected ListagemResult<Controleorcamento> getLista(WebRequestContext request, ControleorcamentoFiltro filtro) {
		
		ListagemResult<Controleorcamento> listagemResult = super.getLista(request, filtro);
		List<Controleorcamento> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcontroleorcamento", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(controleorcamentoService.loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));			
		}

		return listagemResult;
	}
	
	@Override
	protected Controleorcamento criar(WebRequestContext request, Controleorcamento form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdcontroleorcamento() != null){
			return controleorcamentoService.criarCopia(form);
		}
		return super.criar(request, form);
	}
	
	@Override
	protected ModelAndView getEditarModelAndView(WebRequestContext request,	Controleorcamento form) throws CrudException {
		if("editar".equals(request.getParameter("ACAO")) && form.getSituacao() != null && form.getSituacao().equals(Controleorcamentosituacao.APROVADO))						
			throw new SinedException("O Controle de Or�amento encontra-se 'Aprovado', por isso n�o poder� ser editado.");
		return super.getEditarModelAndView(request, form);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Controleorcamento bean) throws Exception {
		String itens = request.getParameter("itenstodelete");		
		if(itens != null && !itens.equals("")){		
			List<Controleorcamento> lista = controleorcamentoService.findSituacaoByWhereIn(itens);
			if(lista != null && !lista.isEmpty()){
				for(Controleorcamento controleorcamento : lista){				
					if(controleorcamento.getSituacao() != null && controleorcamento.getSituacao().equals(Controleorcamentosituacao.APROVADO))
						throw new SinedException("Existe(em) Controle de Or�amento que encontra(m)-se 'Aprovado', por isso nenhum registro foi exclu�do.");
				}
			}
		}else {
			String whereIn = bean.getCdcontroleorcamento().toString();
			List<Controleorcamento> lista = controleorcamentoService.findSituacaoByWhereIn(whereIn);
			if(lista != null && !lista.isEmpty()){
				for(Controleorcamento controleorcamento : lista){				
					if(controleorcamento.getSituacao() != null && controleorcamento.getSituacao().equals(Controleorcamentosituacao.APROVADO))						
						throw new SinedException("O Controle de Or�amento encontra-se 'Aprovado', por isso n�o poder� ser exclu�do.");
				}
			}
		}
		super.excluir(request, bean);
	}
	
	
	
	@Override
	protected void validateBean(Controleorcamento bean, BindException errors) {
		boolean isReplanejando = bean.getCdReplanejado() !=null;
		
		if(bean.getDtExercicioInicio().after(bean.getDtExercicioFim())){
			errors.reject("010","A data in�cio do exerc�cio deve ser menor que a data fim do exerc�cio.");
		}
		if(!isReplanejando){
			List<Controleorcamento> listaControle = controleorcamentoService.findByEmpresaAndExercicio(bean.getEmpresa(),bean.getDtExercicioInicio(),bean.getDtExercicioFim(),bean.getCdcontroleorcamento(), bean.getProjeto(), bean.getProjetoTipo());
			if(SinedUtil.isListNotEmpty(listaControle)){
				errors.reject("001","Per�odo do Exerc�cio de Controle de Or�amento j� cadastrado no sistema.");
			}
		}
		if(bean.getCdcontroleorcamento() != null && (bean.getListaControleorcamentoitem().size()> 0 && bean.getTipolancamento().getNome().equals("Mensal"))){
			if(controleorcamentoService.existeItemDataSuperiorOrcamento(bean)) {
				errors.reject("001","A data fim do exercicio n�o pode ser menor que o m�s/ano dos itens.");
			}
		} else if(bean.getCdcontroleorcamento() == null && (bean.getListaControleorcamentoitem().size()> 0 && bean.getTipolancamento().getNome().equals("Mensal"))) {
			for(Controleorcamentoitem itens : bean.getListaControleorcamentoitem()) {
				if(bean.getDtExercicioInicio().after(itens.getMesano())) {
					errors.reject("001","A data in�cio do exercicio n�o pode ser maior que o m�s/ano dos itens.");
					break;
				}
				if(bean.getDtExercicioFim().before(itens.getMesano())){
					errors.reject("002","A data fim do exercicio n�o pode ser menor que o m�s/ano dos itens.");	
					break;
				}
			}
		}
		super.validateBean(bean, errors);
	}
	
	
	public Controleorcamento organizarDadosProjetoItem(Controleorcamento bean){
		if(bean.getProjeto() != null){
			for (Controleorcamentoitem item : bean.getListaControleorcamentoitem()) {
				item.setProjeto(bean.getProjeto());
				item.setProjetoTipo(null);
			}
		}else if(bean.getProjetoTipo() != null){
			for (Controleorcamentoitem item : bean.getListaControleorcamentoitem()) {
				item.setProjetoTipo(bean.getProjetoTipo());
				item.setProjeto(null);
			}
		}
		return bean;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Controleorcamento bean) throws Exception {
		boolean replanejando = bean.getCdReplanejado() !=null;
		boolean alterado = bean.getCdcontroleorcamento()!= null;
		if(alterado){
			bean.setListaControleorcamentoitem(controleorcamentoitemService.findByControleOrcamento(bean));
		}
		if(bean.getSituacaoTransient() != null){
			if(bean.getSituacaoTransient().toUpperCase().equals("ABERTO"))
				bean.setSituacao(Controleorcamentosituacao.ABERTO);
			else if(bean.getSituacaoTransient().toUpperCase().equals("APROVADO"))
				bean.setSituacao(Controleorcamentosituacao.APROVADO);
		}
		if(bean.getDtExercicioFim() != null){
			bean.setDtExercicioFim(SinedDateUtils.lastDateOfMonth(bean.getDtExercicioFim()));
		}
		if(bean.getProjeto() != null || bean.getProjetoTipo() !=null){
			bean = organizarDadosProjetoItem(bean);
		}
		verificaContaGerencial(bean,request);
		try {
			super.salvar(request, bean);
			if(replanejando){
				String obsHistorico = "Controle de Or�amento Replanejado <a href=/w3erp/sistema/crud/Controleorcamento?ACAO=consultar&cdcontroleorcamento="+bean.getCdcontroleorcamento()+" ' >"+bean.getCdcontroleorcamento()+"</a> ";
				controleorcamentoService.updateSituacaoAndSaveHistorico(Controleorcamentosituacao.INATIVO,bean.getCdReplanejado(),obsHistorico);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
//			if (DatabaseError.isKeyPresent(e, "uk_controleorcamento")) {
//				throw new SinedException("Exerc�cio de Controle de Or�amento j� cadastrado no sistema.");
//			}
		}
		
		ControleOrcamentoHistorico controleOrcamentoHistorico = new ControleOrcamentoHistorico();
		controleOrcamentoHistorico.setDataaltera(SinedDateUtils.currentTimestamp());
		controleOrcamentoHistorico.setResponsavel(SinedUtil.getUsuarioLogado().getNome());
		controleOrcamentoHistorico.setControleorcamento(bean);
		if(replanejando){
			String obsParaHistorico = " Criado a partir de replanejamento or�ament�rio <a href=/w3erp/sistema/crud/Controleorcamento?ACAO=consultar&cdcontroleorcamento="+bean.getCdReplanejado()+" ' >"+bean.getCdReplanejado()+"</a> ";
			controleOrcamentoHistorico.setObservacao(obsParaHistorico);
		}
				
		if(alterado){
			controleOrcamentoHistorico.setAcaoexecutada(ControleOrcamentoHistoricoEnum.ALTERADO);
		}else{
			controleOrcamentoHistorico.setAcaoexecutada(ControleOrcamentoHistoricoEnum.CRIADO);
		}
		controleOrcamentoHistoricoService.saveOrUpdate(controleOrcamentoHistorico);
	}
	
	private void verificaContaGerencial(Controleorcamento bean, WebRequestContext request) {
		if(bean.getListaControleorcamentoitem() != null && bean.getListaControleorcamentoitem().size() > 1){
			List<ControleorcamentoValidacaoBean> lista = new ArrayList<ControleorcamentoValidacaoBean>();
			for(Controleorcamentoitem controleorcamentoitem : bean.getListaControleorcamentoitem()){
				ControleorcamentoValidacaoBean validacaoBean = new ControleorcamentoValidacaoBean();
				if(controleorcamentoitem.getValorOrigem()!=null && controleorcamentoitem.getValorOrigem().compareTo(0.0)!=0){
					request.setAttribute("replanejar", true);
				}
				if(controleorcamentoitem.getMesano() != null){
					validacaoBean.setMesano(controleorcamentoitem.getMesanoAux());
				}
				if(controleorcamentoitem.getContagerencial() != null && controleorcamentoitem.getContagerencial().getCdcontagerencial() != null){
					validacaoBean.setCdcontagerencial(controleorcamentoitem.getContagerencial().getCdcontagerencial());
				}
				if(controleorcamentoitem.getCentrocusto() != null && controleorcamentoitem.getCentrocusto().getCdcentrocusto() != null){
					validacaoBean.setCdcentrocusto(controleorcamentoitem.getCentrocusto().getCdcentrocusto());
				}
				if(controleorcamentoitem.getProjeto() != null && controleorcamentoitem.getProjeto().getCdprojeto() != null){
					validacaoBean.setCdprojeto(controleorcamentoitem.getProjeto().getCdprojeto());
				}
				
				if(lista.contains(validacaoBean)){
					throw new SinedException("Existe(m) conta(s) gerencia(is) cadastrada(s) mais de uma vez para o mesmo centro de custo no Controle de Or�amentos.");
				}
				lista.add(validacaoBean);
			}
		}
	}
	
	/**
	 * M�todo para aprovar Controle de Or�amento
	 *
	 * @param request
	 * @return
	 * @author Luiz Fernando
	 */
	public ModelAndView aprovarOrcamento(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		String action = request.getParameter("action");
		
		validaParameter(request);
		
		Integer erros = 0;
		Integer sucessos = 0;
		Boolean erro = Boolean.FALSE;
		Boolean sucesso = Boolean.FALSE;
		StringBuilder mgsErro = new StringBuilder();
		
		List<Controleorcamento> listaControleorcamento = controleorcamentoService.findForAprovar(whereIn);
		if(listaControleorcamento != null && !listaControleorcamento.isEmpty()){
			for(Controleorcamento controleorcamento : listaControleorcamento){
				if(controleorcamento.getListaControleorcamentoitem() != null && !controleorcamento.getListaControleorcamentoitem().isEmpty()){					
					controleorcamento.setSituacao(Controleorcamentosituacao.APROVADO);
					controleorcamentoService.atualizarSituacao(controleorcamento);
					insereHistorico(controleorcamento,ControleOrcamentoHistoricoEnum.APROVADO);
					sucesso = Boolean.TRUE;
					sucessos++;
				}else{
					erros++;
					mgsErro.append("".equals(mgsErro.toString()) ? controleorcamento.getCdcontroleorcamento() : "," + controleorcamento.getCdcontroleorcamento());
					erro = Boolean.TRUE;
				}
			}
			if(erro)
				request.addError(erros + " Controle de Or�amento n�o aprovado(s) por n�o ter(em) Conta Gerencial e/ou Centro de Custo definido(s).");
			if(sucesso)
				request.addMessage(sucessos + " Controle(s) de Or�amento(s) aprovado(s) com sucesso.");
		}else
			request.addMessage("O(s) Controle(s) de Or�amento(s) j� est�(�o) como 'APROVADO(S)'.");
		
		
		if(action != null && action.equals("entrada"))				
			return new ModelAndView("redirect:/financeiro/crud/Controleorcamento?ACAO=consultar&cdcontroleorcamento=" + whereIn);
		else
			return sendRedirectToAction("listagem");
	}
	
	public ModelAndView replanejarOrcamento(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		List<Controleorcamento> listaOrcamentos = controleorcamentoService.findSituacaoByWhereIn(whereIn);
		if(listaOrcamentos.size() != 1){
			request.addError("S� � permitido replanejar um Controle de Or�amento por vez.");
			return sendRedirectToAction("listagem");
		}
		Controleorcamento controleOrcamento = listaOrcamentos.get(0);
		if(!Controleorcamentosituacao.APROVADO.equals(controleOrcamento.getSituacao())){
			request.addError("S� � permitido replanejar um Controle de Or�amento com situa��o Aprovado.");
			return sendRedirectToAction("listagem");
		} 
		try {
			request.setAttribute("replanejando",true);
			return doEditar(request, controleOrcamento);
		} catch (CrudException e) {
			e.printStackTrace();
		}
		request.addError("Erro ao executar o processo.tente novamente caso percista por favor contate o administrador do sistema.");
		return sendRedirectToAction("listagem");
	}	
	
	public ModelAndView estornarOrcamento(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		String action = request.getParameter("action");
		
		validaParameter(request);
		
		Integer sucessos = 0;
		Boolean sucesso = Boolean.FALSE;
			
		List<Controleorcamento> listaControleorcamento = controleorcamentoService.findForEstornar(whereIn);
		if(listaControleorcamento != null && !listaControleorcamento.isEmpty()){
			for(Controleorcamento controleorcamento : listaControleorcamento){
				if(controleorcamento.getListaControleorcamentoitem() != null && !controleorcamento.getListaControleorcamentoitem().isEmpty()){					
					controleorcamento.setSituacao(Controleorcamentosituacao.ABERTO);
					controleorcamentoService.atualizarSituacao(controleorcamento);
					insereHistorico(controleorcamento,ControleOrcamentoHistoricoEnum.ESTORNADO);
					sucesso = Boolean.TRUE;
					sucessos++;
				}
			}
		if(sucesso)
			request.addMessage(sucessos + " Controle(s) de Or�amento(s) estornado(s) com sucesso.");
		}else
			request.addMessage("O(s) Controle(s) de Or�amento(s) j� est�(�o) como 'ABERTO'.");
		
		
		if(action != null && action.equals("entrada"))				
			return new ModelAndView("redirect:/financeiro/crud/Controleorcamento?ACAO=consultar&cdcontroleorcamento=" + whereIn);
		else
			return sendRedirectToAction("listagem");
	}
	
	public ModelAndView validaParameter(WebRequestContext request){
		
		String whereIn = request.getParameter("selectedItens");
		String action = request.getParameter("action");
		
		if(whereIn == null || whereIn.equals("")){
			request.addError("Nenhum item selecionado.");
			if(action != null && action.equals("entrada"))				
				return new ModelAndView("redirect:/financeiro/crud/Controleorcamento?ACAO=consultar&cdcontroleorcamento=" + whereIn);
			else
				return sendRedirectToAction("listagem");
		}
		return null;
	}
	
	
	
	public void insereHistorico(Controleorcamento controleorcamento, ControleOrcamentoHistoricoEnum controleOrcamentoHistoricoEnum){
	
		boolean aprovadoOrNo = controleOrcamentoHistoricoEnum.equals(ControleOrcamentoHistoricoEnum.APROVADO);
		
		ControleOrcamentoHistorico controleOrcamentoHistorico = new ControleOrcamentoHistorico();
		controleOrcamentoHistorico.setDataaltera(SinedDateUtils.currentTimestamp());
		controleOrcamentoHistorico.setResponsavel(SinedUtil.getUsuarioLogado().getNome());
		controleOrcamentoHistorico.setControleorcamento(controleorcamento);
				
		if(aprovadoOrNo){
			controleOrcamentoHistorico.setAcaoexecutada(ControleOrcamentoHistoricoEnum.APROVADO);
		}else{
			controleOrcamentoHistorico.setAcaoexecutada(ControleOrcamentoHistoricoEnum.ESTORNADO);
		}
		controleOrcamentoHistoricoService.saveOrUpdate(controleOrcamentoHistorico);
	}
	
	public ModelAndView gerarCsv(WebRequestContext request, ControleorcamentoFiltro filtro) throws Exception {
		StringBuilder csv = new StringBuilder();
		List<Controleorcamento> listaOrcamentos = controleorcamentoService.findOrcamentosForCsv(filtro);
		for (Controleorcamento controleorcamento : listaOrcamentos) {
			csv.append("OR�AMENTO: \n");
			csv.append("PER�ODO DO EXERC�CIO; EMPRESA; DESCRI��O; PROJETO; TIPO PROJETO; TIPO LAN�AMENTO; SITUA��O\n");
			
			if(controleorcamento.getDtOrcamento() != null){
				csv.append(controleorcamento.getDtOrcamento() + ";");				
			} else {
				csv.append(";");
			}
			
			if(controleorcamento.getEmpresa() != null && controleorcamento.getEmpresa().getNome() != null){
				csv.append(controleorcamento.getEmpresa().getNome() + ";");				
			}else {
				csv.append(";");
			}
			
			if(controleorcamento.getDescricao() != null){
				csv.append(controleorcamento.getDescricao() + ";");				
			}else {
				csv.append(";");
			}
			
			if(controleorcamento.getProjeto() != null && controleorcamento.getProjeto().getNome() != null){
				csv.append(controleorcamento.getProjeto().getNome() + ";");				
			}else {
				csv.append(";");
			}
			
			if(controleorcamento.getProjeto() != null && controleorcamento.getProjeto().getNome() != null){
				csv.append(controleorcamento.getProjetoTipo().getNome() + ";");				
			}else {
				csv.append(";");
			}
			
			if(controleorcamento.getTipolancamento() != null && controleorcamento.getTipolancamento().getNome() != null){
				csv.append(controleorcamento.getTipolancamento().getNome() + ";");				
			}else {
				csv.append(";");
			}
			
			if(controleorcamento.getSituacao() != null && controleorcamento.getSituacao().getNome() != null){
				csv.append(controleorcamento.getSituacao().getNome() + "\n");				
			}else {
				csv.append("\n");
			}
			
			csv.append("ITENS DO OR�AMENTO: \n");
			csv.append("M�S/ANO; CENTRO DE CUSTO; PROJETO; TIPO PROJETO; CONTA GERENCIAL; VALOR\n");
			for (Controleorcamentoitem controleorcamentoitem : controleorcamento.getListaControleorcamentoitem()) {
				
				if("Mensal".equalsIgnoreCase(controleorcamento.getTipolancamento().getNome()) && controleorcamentoitem.getMesanoAux() != null){
					csv.append(controleorcamentoitem.getMesanoAux() + ";");
				}else {
					csv.append(controleorcamento.getDtOrcamento() + ";");
				}
				
				if(controleorcamentoitem.getCentrocusto() != null && controleorcamentoitem.getCentrocusto().getNome() != null){
					csv.append(controleorcamentoitem.getCentrocusto().getNome() + ";");
				}else {
					csv.append(";");
				}
				
				if(controleorcamentoitem.getProjeto() != null && controleorcamentoitem.getProjeto().getNome() != null){
					csv.append(controleorcamentoitem.getProjeto().getNome() + ";");
				}else {
					csv.append(";");
				}
				
				if(controleorcamentoitem.getProjetoTipo() != null && controleorcamentoitem.getProjetoTipo().getNome() != null){
					csv.append(controleorcamentoitem.getProjetoTipo().getNome() + ";");
				}else {
					csv.append(";");
				}
				
				if(controleorcamentoitem.getContagerencial() != null && controleorcamentoitem.getContagerencial().getVcontagerencial() != null){
					csv.append(controleorcamentoitem.getIdentificadorNomeContaGerencial() + ";");
				}else {
					csv.append(";");
				}
				
				if(controleorcamentoitem.getValor() != null){
					csv.append("R$" + controleorcamentoitem.getValor() + ";");
				}else {
					csv.append(";");
				}
				csv.append("\n");
			}
			csv.append("\n\n");
		}
		Resource resource = new Resource("text/csv","controle_orcamento" + SinedUtil.datePatternForReport() + ".csv", csv.toString().replaceAll(";null;", ";;").getBytes());
		ResourceModelAndView resourceModelAndView = new ResourceModelAndView(resource);
		return resourceModelAndView;
	}
}
