package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.validator.GenericValidator;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Atividadetiporesponsavel;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.AtividadetipomodeloService;
import br.com.linkcom.sined.geral.service.AtividadetiporesponsavelService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AtividadetipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Atividadetipo"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class AtividadetipoCrud extends CrudControllerSined<AtividadetipoFiltro, Atividadetipo, Atividadetipo>{
	
	private AtividadetipoService atividadetipoService;
	private ReportTemplateService reportTemplateService;
	private AtividadetipomodeloService atividadetipomodeloService;
	private AtividadetiporesponsavelService atividadetiporesponsavelService;
	
	public void setAtividadetipoService(AtividadetipoService atividadetipoService) {this.atividadetipoService = atividadetipoService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setAtividadetipomodeloService(AtividadetipomodeloService atividadetipomodeloService) {this.atividadetipomodeloService = atividadetipomodeloService;}
	public void setAtividadetiporesponsavelService(AtividadetiporesponsavelService atividadetiporesponsavelService) {this.atividadetiporesponsavelService = atividadetiporesponsavelService;}
	
	@Override
	protected void entrada(WebRequestContext request, Atividadetipo form) throws Exception {
		
		if (form.getCdatividadetipo()!=null &&
			!request.getBindException().hasErrors()){
			form.setListaAtividadetipomodelo(atividadetipomodeloService.findByAtividadetipo(form));
			form.setListaAtividadetiporesponsavel(atividadetiporesponsavelService.findByAtividadetipo(form));			
		}
		
		if (form.getCdatividadetipo()==null){
			form.setAplicacaocrm(Boolean.TRUE);
			form.setAplicacaoservico(Boolean.TRUE);
		}
		
		request.setAttribute("listaReportTemplate", reportTemplateService.loadTemplatesByCategoria(EnumCategoriaReportTemplate.EMITIR_ORDEM_SERVICO));
		
		super.entrada(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Atividadetipo bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if(DatabaseError.isKeyPresent(e, "IDX_ATIVIDADETIPO_NOME")){
				throw new SinedException("Tipo de atividade j� cadastrada.");
			} else if(DatabaseError.isKeyPresent(e, "idx_atividadetipomodelo_reporttemplate")){
				throw new SinedException("Modelo j� cadastrado.");
			} else if(DatabaseError.isKeyPresent(e, "atividadetipoitem")){
				throw new SinedException("N�o � poss�vel excluir o(s) campo(s) extra para O.S., pois est�o sendo usado no sistema. ");
			} else throw e;
		}
	}
	
	@Override
	protected void validateBean(Atividadetipo bean, BindException errors) {
		if(bean.getAtividadepadrao() != null && bean.getAtividadepadrao()){
			Atividadetipo atividadetipo = atividadetipoService.findAtividadePadrao();
			if(atividadetipo != null && atividadetipo.getAtividadepadrao() != null && atividadetipo.getAtividadepadrao()){
				boolean existAtividadepadrao = false;
				if(bean.getCdatividadetipo() != null){
					if(!atividadetipo.getCdatividadetipo().equals(bean.getCdatividadetipo())){
						existAtividadepadrao = true;
					}
				}else {
					existAtividadepadrao = true;
				}
				if(existAtividadepadrao){
					errors.reject("001", "Existe um tipo de atividade marcada como padr�o (" + atividadetipo.getNome() + ").");
				}
			}
		}
		
		if (bean.getEmailextraenvioos() != null && !bean.getEmailextraenvioos().isEmpty()) {
			StringBuilder erroEmail = new StringBuilder();
			String emails[] = bean.getEmailextraenvioos().trim().split(",|;");
			for (String email : emails) {
				if (!GenericValidator.isEmail(email)) {
					erroEmail.append(email).append(", ");
				}
			}
			if (erroEmail.length() > 0) {
				errors.reject("002", "Email(s) inv�lido(s): " + erroEmail.delete(erroEmail.length() - 2, erroEmail.length()).toString() + ".");
			}
		}
		if(bean.getListaAtividadetiporesponsavel() != null && bean.getListaAtividadetiporesponsavel().size() > 1){
			List<Projeto> projetos = new ArrayList<Projeto>();
			Integer responsavelSemProjeto = 0;
			for(Atividadetiporesponsavel ativTipoResp: bean.getListaAtividadetiporesponsavel()){
				if(ativTipoResp.getProjeto() != null && ativTipoResp.getProjeto().getCdprojeto() != null){
					if(projetos.contains(ativTipoResp.getProjeto())){
						errors.reject("003", "N�o � permitido vincular o projeto para mais de um respons�vel.");
						break;
					}else{
						projetos.add(ativTipoResp.getProjeto());
					}
				}else {
					responsavelSemProjeto++;
					if(responsavelSemProjeto > 1){
						errors.reject("003", "� permitido apenas um registro de respons�vel sem projeto.");
						break;
					}
				}
			}
		}
		
//		if(!(Boolean.TRUE.equals(bean.getAplicacaocrm()) || Boolean.TRUE.equals(bean.getAplicacaoservico()))){
//			errors.reject("004", "Tipo de atividade deve estar associado a algum campo da se��o Aplica��o.");
//		}
		
		
		super.validateBean(bean, errors);
	}
}
