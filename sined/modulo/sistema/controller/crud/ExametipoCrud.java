package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Exametipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExametipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Exametipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class ExametipoCrud extends CrudControllerSined<ExametipoFiltro, Exametipo, Exametipo> {

	@Override
	protected void salvar(WebRequestContext request, Exametipo bean)
			throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_exametipo_nome")) 
				throw new SinedException("Tipo de Exame j� cadastrado no sistema.");	
		}
	}
}
