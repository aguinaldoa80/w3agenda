package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivodevolucaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Motivodevolucao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "constatacao", "motivodevolucao"})
public class MotivodevolucaoCrud extends CrudControllerSined<MotivodevolucaoFiltro, Motivodevolucao, Motivodevolucao> {

	@Override
	protected void salvar(WebRequestContext request, Motivodevolucao bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_MOTIVODEVOLUCAO_DESCRICAO")) {
				throw new SinedException("Motivo j� cadastrado no sistema.");
			} else {
				throw new SinedException(e.getMessage());
			}
			
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Motivodevolucao bean)
			throws Exception {
		try {
			super.excluir(request, bean);	
		} catch (Exception e) {
			throw new SinedException("Motivo de devolu��o n�o pode ser exclu�do, j� possui registros referenciados no sistema.");
		}
	}
}
