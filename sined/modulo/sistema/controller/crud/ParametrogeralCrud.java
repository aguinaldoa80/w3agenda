package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.ParametrogeralHistorico;
import br.com.linkcom.sined.geral.bean.enumeration.ParametrogeralHistoricoEnum;
import br.com.linkcom.sined.geral.service.ParametrogeralHistoricoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ParametrogeralFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.ParametroCacheUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Parametro", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "valor", "objetivo"})
public class ParametrogeralCrud extends CrudControllerSined<ParametrogeralFiltro, Parametrogeral, Parametrogeral> {
	
	protected ParametrogeralHistoricoService parametrogeralhistoricoservice;
	protected ParametrogeralService parametrogeralService;
	protected PedidovendatipoService pedidovendatipoService;
	
	public void setParametrogeralhistoricoservice(ParametrogeralHistoricoService parametrogeralhistoricoservice) {
		this.parametrogeralhistoricoservice = parametrogeralhistoricoservice;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {
		this.pedidovendatipoService = pedidovendatipoService;
	}

	@Override
	public ModelAndView doEditar(WebRequestContext request, Parametrogeral form)
			throws CrudException {
		
		if(form.getParametrointerno() != null && form.getParametrointerno()){
			if(parametrogeralService.isUsuarioAdmin()){
				return super.doEditar(request, form);
			}else{
				request.addError("Altera��o de valor do par�metro permitido apenas pelo suporte do W3ERP");
				return continueOnAction("doConsultar", form);
			}
		}	
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Parametrogeral form)
			throws CrudException {
		
		if(!parametrogeralService.isUsuarioAdmin() && Boolean.TRUE.equals(form.getParametrointerno())){	
			request.addError("Somente administrador pode editar esse par�metro");
			request.setAttribute("isAdministrador", true);	
			return continueOnAction("entrada", form);
		}
		
		return super.doSalvar(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Parametrogeral bean)throws Exception {
		
		String valoranterior = null;
		Boolean alterado = bean.getCdparametrogeral() != null;
		if(bean.getCdparametrogeral() != null){
			Parametrogeral parametrogeral = parametrogeralService.load(bean);
			if(parametrogeral != null){
				valoranterior = parametrogeral.getValor();
				request.setAttribute("isAdministrador", true);	
			}
		}
		
		
		
		try {
			super.salvar(request, bean);			
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PARAMETROGERAL_NOME")) throw new SinedException("Par�metro j� cadastrado no sistema.");
		}finally{
			ParametroCacheUtil.put(bean.getNome(), bean.getValor());
			if (bean.getNome().equalsIgnoreCase(Parametrogeral.RECAPAGEM)){
				NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RECAPAGEM, "TRUE".equalsIgnoreCase(bean.getValor()));
			}
			if (bean.getNome().equalsIgnoreCase(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE)){
				NeoWeb.getRequestContext().getSession().setAttribute(Parametrogeral.RATEIO_MOVIMENTACAO_ESTOQUE, "TRUE".equalsIgnoreCase(bean.getValor()));
			}
		}
		//Preenchimento do Hist�rico
		ParametrogeralHistorico parametrogeralhistorico = new ParametrogeralHistorico();
		parametrogeralhistorico.setAcaoexecutada(alterado ? ParametrogeralHistoricoEnum.ALTERADO : ParametrogeralHistoricoEnum.CRIADO);
		parametrogeralhistorico.setDataaltera(SinedDateUtils.currentTimestamp());
		parametrogeralhistorico.setObservacao(valoranterior != null ? "Valor anterior : " +  valoranterior : "");
		parametrogeralhistorico.setResponsavel(SinedUtil.getUsuarioLogado().getNome());
		parametrogeralhistorico.setParametrogeral(bean);
		
		parametrogeralhistoricoservice.saveOrUpdate(parametrogeralhistorico);
		
		if(Parametrogeral.SEPARACAO_EXPEDICAO.equals(bean.getNome()) && !"TRUE".equalsIgnoreCase(bean.getValor())){
			pedidovendatipoService.desmarcarCriarExpedicaoComConferencia();
		}
		
	}
	
	@Override
	protected void excluir(WebRequestContext request, Parametrogeral bean)	throws Exception {
		throw new SinedException("O par�metro n�o pode ser excluido.");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Parametrogeral form) throws Exception {
		//Valida��o de ADM LinkCom para apresentar campo parametrointerno 
		if(parametrogeralService.isUsuarioAdmin()){	
			request.setAttribute("isAdministrador", true);			
		}
		if(form.getParametrointerno() != null && form.getParametrointerno()){
			if(!parametrogeralService.isUsuarioAdmin()){
				doConsultar(request, form);
			}
		}	
		if (request.getParameter(ACTION_PARAMETER).equals(EDITAR)){
			request.setAttribute("editar", true);
		}
		super.entrada(request, form);
	}
	@Override
	public ModelAndView doConsultar(WebRequestContext request,Parametrogeral form) throws CrudException {
		
		Parametrogeral parametrogeral =	parametrogeralService.findForParametroInterno(form);
		
		if(parametrogeral.getParametrointerno() != null && parametrogeral.getParametrointerno()){
			if(parametrogeralService.isUsuarioAdmin()){
				return super.doConsultar(request, form);
			}else{
				throw new SinedException("Altera��o de valor do par�metro permitido apenas pelo suporte do W3ERP");
			}
		}else{
			return super.doConsultar(request, form);
		}		
	}
}
