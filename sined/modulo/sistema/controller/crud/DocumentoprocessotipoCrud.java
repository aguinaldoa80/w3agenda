package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documentoprocessotipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentoprocessotipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Documentoprocessotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class DocumentoprocessotipoCrud extends CrudControllerSined<DocumentoprocessotipoFiltro, Documentoprocessotipo, Documentoprocessotipo>  {

	@Override
	protected void salvar(WebRequestContext request, Documentoprocessotipo bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_documentoprocessotipo_nome")) {
				throw new SinedException("Tipo de Documento/Processo j� cadastrado no sistema.");
			}
		}
		
	}
}
