package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documentodigitalmodelo;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/Documentodigitalmodelo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class DocumentodigitalmodeloCrud extends CrudControllerSined<FiltroListagemSined, Documentodigitalmodelo, Documentodigitalmodelo> {
	
	private ReportTemplateService reportTemplateService;
	
	public void setReportTemplateService(
			ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Documentodigitalmodelo form) throws Exception {
		request.setAttribute("listaReporttemplate", reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMAIL_ACEITE_DIGITAL));
	}
	
}
