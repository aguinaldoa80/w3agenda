package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneumedidaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Pneumedida"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "circunferencia", "codigointegracao"})
public class PneumedidaCrud extends CrudControllerSined<PneumedidaFiltro, Pneumedida, Pneumedida> {	
	
	@Override
	protected void salvar(WebRequestContext request, Pneumedida bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PNEUMEDIDA_NOME")) {
				throw new SinedException("Medida de pneu j� cadastrada no sistema.");
			} else throw e;
		}
	}

	@Override
	protected Pneumedida criar(WebRequestContext request, Pneumedida form) throws Exception {
		form = super.criar(request, form);
		if("true".equals(request.getParameter("fromInsertOne"))){
			form.setOtr("true".equals(request.getParameter("isOtr")));
		}
		return form;
	}
}
