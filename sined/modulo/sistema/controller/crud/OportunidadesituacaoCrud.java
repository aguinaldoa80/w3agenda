package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Timestamp;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.OportunidadesituacaoService;
import br.com.linkcom.sined.modulo.crm.controller.crud.filter.OportunidadesituacaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(
		path="/sistema/crud/Oportunidadesituacao", authorizationModule=CrudAuthorizationModule.class		
)
@ExportCSV(fields = {"nome", "letra"})
public class OportunidadesituacaoCrud extends CrudControllerSined<OportunidadesituacaoFiltro, Oportunidadesituacao, Oportunidadesituacao>{

	private ArquivoService arquivoService;
	private OportunidadesituacaoService oportunidadesituacaoService;
		
	public void setArquivoService(ArquivoService arquivoService) {this.arquivoService = arquivoService;}
	public void setOportunidadesituacaoService(OportunidadesituacaoService oportunidadesituacaoService) {this.oportunidadesituacaoService = oportunidadesituacaoService;}

	
	@Override
	protected ListagemResult<Oportunidadesituacao> getLista(WebRequestContext request, OportunidadesituacaoFiltro filtro) {
		
		ListagemResult<Oportunidadesituacao> listResult =  super.getLista(request, filtro);
		
		if(listResult != null && listResult.list() != null && !listResult.list().isEmpty()){
			for(Oportunidadesituacao os : listResult.list()){
				try {					
					arquivoService.loadAsImage(os.getImagem());
					if(os.getImagem().getCdarquivo() != null){
						DownloadFileServlet.addCdfile(request.getSession(),os.getImagem().getCdarquivo());
					}
				} catch (Exception e) {
					e.printStackTrace();
//					System.out.println(e);
				}
			}
		}
		
		return listResult;
	}
	
	@Override
	protected void validateBean(Oportunidadesituacao bean, BindException errors) {
		Boolean existeSituacao = Boolean.FALSE;
		if(bean.getSituacaoinicial() != null && bean.getSituacaoinicial()){
			existeSituacao = oportunidadesituacaoService.existeOportunidadesituacao(bean.getCdoportunidadesituacao(), "situacaoinicial");
			if(existeSituacao != null && existeSituacao){
				errors.reject("001", "Existe uma situa��o cadastrada como inicial.");
			}
		}else if(bean.getSituacaocancelada() != null && bean.getSituacaocancelada()){
			existeSituacao = oportunidadesituacaoService.existeOportunidadesituacao(bean.getCdoportunidadesituacao(), "situacaocancelada");
			if(existeSituacao != null && existeSituacao){
				errors.reject("001", "Existe uma situa��o cadastrada como cancelada.");
			}
		}else if(bean.getSituacaoantesfinal() != null && bean.getSituacaoantesfinal()){
			existeSituacao = oportunidadesituacaoService.existeOportunidadesituacao(bean.getCdoportunidadesituacao(), "situacaoantesfinal");
			if(existeSituacao != null && existeSituacao){
				errors.reject("001", "Existe uma situa��o cadastrada como antes da final.");
			}
		}else if(bean.getSituacaofinal() != null && bean.getSituacaofinal()){
			existeSituacao = oportunidadesituacaoService.existeOportunidadesituacao(bean.getCdoportunidadesituacao(), "situacaofinal");
			if(existeSituacao != null && existeSituacao){
				errors.reject("001", "Existe uma situa��o cadastrada como final.");
			}
		}
		
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Oportunidadesituacao bean)	throws Exception {
		
		try {
			bean.setDtaltera(new Timestamp(System.currentTimeMillis()));
			bean.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if(DatabaseError.isKeyPresent(e, "IDX_OPORTUNIDADESITUACAO_NOME")){
				throw new SinedException("Situa��o j� cadastrada no sistema.");
			}
		}
		
	}
}
