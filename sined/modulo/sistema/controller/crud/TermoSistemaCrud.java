package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.TermoSistema;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TermoSistemaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/TermoSistema", authorizationModule=CrudAuthorizationModule.class)
public class TermoSistemaCrud extends CrudControllerSined<TermoSistemaFiltro, TermoSistema, TermoSistema>{

}
