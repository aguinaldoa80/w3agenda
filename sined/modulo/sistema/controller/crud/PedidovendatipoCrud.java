package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.ReflectionCache;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.enumeration.BaixaestoqueEnum;
import br.com.linkcom.sined.geral.bean.enumeration.PedidoVendaTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.Presencacompradornfe;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.DocumentoacaoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidoVendaTipoHistoricoService;
import br.com.linkcom.sined.geral.service.PedidovendatipoService;
import br.com.linkcom.sined.geral.service.ProducaoagendatipoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PedidovendatipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Pedidovendatipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "sigla", "bonificacao", "naturezaoperacao"})
public class PedidovendatipoCrud extends CrudControllerSined<PedidovendatipoFiltro, Pedidovendatipo, Pedidovendatipo>{

	private EmpresaService empresaService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private ReportTemplateService reportTemplateService;
	private PedidovendatipoService pedidovendatipoService;
	private ProjetoService projetoService;
	private PedidoVendaTipoHistoricoService pedidoVendaTipoHistoricoService;
	private CfopService cfopService;
	private DocumentoacaoService documentoacaoService;
	private LocalarmazenagemService localarmazenagemService;
	private ProducaoagendatipoService producaoagendatipoService;
	private ParametrogeralService parametrogeralService;
	 
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {this.naturezaoperacaoService = naturezaoperacaoService;}
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}
	public void setPedidovendatipoService(PedidovendatipoService pedidovendatipoService) {this.pedidovendatipoService = pedidovendatipoService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setPedidoVendaTipoHistoricoService(PedidoVendaTipoHistoricoService pedidoVendaTipoHistoricoService) {this.pedidoVendaTipoHistoricoService = pedidoVendaTipoHistoricoService;}
	public void setCfopService(CfopService cfopService) {this.cfopService = cfopService;}
	public void setDocumentoacaoService(DocumentoacaoService documentoacaoService) {this.documentoacaoService = documentoacaoService;}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {this.localarmazenagemService = localarmazenagemService;}
	public void setProducaoagendatipoService(ProducaoagendatipoService producaoagendatipoService) {this.producaoagendatipoService = producaoagendatipoService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@Override
	protected Pedidovendatipo criar(WebRequestContext request, Pedidovendatipo form) throws Exception {
		form.setObrigarLoteOrcamento(Boolean.TRUE);
		form.setObrigarLotePedidovenda(Boolean.TRUE);
		form.setObrigarLoteConfirmarpedido(Boolean.TRUE);
		form.setObrigarLoteVenda(Boolean.TRUE);
		
		return super.criar(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Pedidovendatipo form) throws Exception {
		Boolean isWms = empresaService.isIntegracaoWms(empresaService.loadPrincipal());
		boolean podeEditar = false;
		if(isWms != null && isWms)
			request.setAttribute("WMS_INTEGRACAO", "TRUE");
		else
			request.setAttribute("WMS_INTEGRACAO", "FALSE");
		
		if (form.getCdpedidovendatipo() == null){
			podeEditar = true;
			if(form.getNaturezaoperacao() == null) {
				form.setNaturezaoperacao(naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_PRODUTO));
			}
			if(form.getNaturezaoperacaoservico() == null) {
				form.setNaturezaoperacaoservico(naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_SERVICO));
			}
			if(form.getNaturezaoperacaoexpedicao() == null) {
				form.setNaturezaoperacaoexpedicao(naturezaoperacaoService.loadPadrao(NotaTipo.NOTA_FISCAL_PRODUTO));
			}
			if(form.getDocumentoacao() == null){
				form.setDocumentoacao(Documentoacao.PREVISTA);
			}
		} else {
			form.setListaPedidoVendaTipoHistorico(pedidoVendaTipoHistoricoService.findByPedidoVendaTipo(form));
			if(SinedUtil.getUsuarioLogado().getCdpessoa() == 1){
				podeEditar = true;
			} else {
				Boolean possuiRegistros = pedidovendatipoService.existeVendaOrcamentoPedido(form.getCdpedidovendatipo());
				if(!possuiRegistros){
					podeEditar = true;
				}
			}
		}
		
		List<BaixaestoqueEnum> listaBaixaestoqueEnum = new ArrayList<BaixaestoqueEnum>();
		listaBaixaestoqueEnum.add(BaixaestoqueEnum.APOS_EMISSAONOTA);
		listaBaixaestoqueEnum.add(BaixaestoqueEnum.APOS_EXPEDICAOCONFIRMADA);
		listaBaixaestoqueEnum.add(BaixaestoqueEnum.APOS_EXPEDICAOWMS);
		listaBaixaestoqueEnum.add(BaixaestoqueEnum.APOS_VENDAREALIZADA);
		listaBaixaestoqueEnum.add(BaixaestoqueEnum.NAO_BAIXAR);
		request.setAttribute("listaBaixaestoqueEnum", listaBaixaestoqueEnum);
		request.setAttribute("listaPresencacompradornfe", Presencacompradornfe.getListaPresencacompradornfeForVenda());
		
		List<Naturezaoperacao> listaNaturezaoperacaoEntradaSaidaFiscal = naturezaoperacaoService.find(form.getNaturezaoperacao(), NotaTipo.NOTA_FISCAL_PRODUTO, Boolean.TRUE);
		request.setAttribute("listaNaturezaoperacaonotafiscalentrada", listaNaturezaoperacaoEntradaSaidaFiscal);
		request.setAttribute("listaNaturezaoperacaosaidafiscal", listaNaturezaoperacaoEntradaSaidaFiscal);
		
		List<ReportTemplateBean> reportTemplateBean = null;
		try {
			reportTemplateBean = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.DISCRIMINACAO_DOS_SERVICOS);
		} catch (Exception e){
			e.printStackTrace();
		}
		request.setAttribute("listaCategoriaTemplate", reportTemplateBean != null ? reportTemplateBean : new ListSet<ReportTemplateBean>(ReportTemplateBean.class));
		
		List<Documentoacao> listaDocumentoacao = new ArrayList<Documentoacao>();
		listaDocumentoacao.add(Documentoacao.PREVISTA);
		listaDocumentoacao.add(Documentoacao.DEFINITIVA);
		request.setAttribute("listaDocumentoacao", listaDocumentoacao);
		request.setAttribute("listaProjeto", projetoService.findProjetosAtivos(form.getProjetoprincipal(), true));
		request.setAttribute("podeEditar", podeEditar);
		request.setAttribute("SEPARACAO_EXPEDICAO", parametrogeralService.getBoolean(Parametrogeral.SEPARACAO_EXPEDICAO));
		request.setAttribute("isTrayCorp", SinedUtil.isIntegracaoTrayCorp());
		request.setAttribute("isEcompleto", SinedUtil.isIntegracaoEcompleto());
		request.setAttribute("INTEGRACAO_ECOMMERCE", parametrogeralService.getBoolean(Parametrogeral.INTEGRACAO_ECOMMERCE));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Pedidovendatipo bean)throws Exception {
		boolean criar = bean.getCdpedidovendatipo() == null;
		Pedidovendatipo beanAntigo = null;
		String obs = null;
		
		if(bean.getCdpedidovendatipo() != null) {
		beanAntigo = pedidovendatipoService.loadForEntrada(bean);
		}
		if(beanAntigo != null) {
		obs = pedidoVendaTipoHistoricoService.getCamposAlterados(beanAntigo, bean) + pedidoVendaTipoHistoricoService.getCampoExtraAlterado(beanAntigo, bean);
		obs = compararPedidoVendaTipoSelectsPorObjeto(obs, beanAntigo, bean);
		}
		
		try{
			super.salvar(request, bean);
			if(criar){
				pedidoVendaTipoHistoricoService.preencherHistorico(bean, PedidoVendaTipoEnum.CRIADO, "Cria��o do Tipo de Pedido de Venda", true);
			}else {
				pedidoVendaTipoHistoricoService.preencherHistorico(bean, PedidoVendaTipoEnum.ALTERADO, obs, true);
			}
			if(obs != null && obs.contains("Principal de: Sim para: N�o")){
				request.addMessage("N�o existe nenhum tipo de pedido de venda marcado como 'principal', com isso, o sistema n�o ir� sugerir automaticamente "+
								   "o tipo de pedido de venda nas telas de Venda e/ou Pedido de Venda");
			}
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "idx_pedidovendatipo_descricao")){
				throw new SinedException("Tipo de pedido de venda j� cadastrado no sistema.");
			}else if(DatabaseError.isKeyPresent(e, "fk_pedidovendavalorcampoextra") || DatabaseError.isKeyPresent(e, "fk_vendavalorcampoextra_1") ||
					DatabaseError.isKeyPresent(e, "fk_orcamentovalorcampoextra_1")){
				throw new SinedException("Campo extra n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema");
			}else {
				throw new SinedException(e.getMessage());
			}
		}
	}
	
	private String compararPedidoVendaTipoSelectsPorObjeto(String obs, Pedidovendatipo beanAntigo, Pedidovendatipo bean) throws SecurityException, NoSuchMethodException {
		ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
		String descricaoAntigo = "";
		String descricaoCorrente = "";
		String campo = "";
		String de = " de: ";
		String para = " para: ";
		StringBuilder descricaoCompleta = new StringBuilder(obs);
		
		//compara o projeto principal
		if(bean.getProjetoprincipal() != null && bean.getProjetoprincipal().getCdprojeto() != null){
			descricaoCorrente = projetoService.load(bean.getProjetoprincipal(), "projeto.nome").getNome();
		} 
		if(beanAntigo.getProjetoprincipal() != null && beanAntigo.getProjetoprincipal().getCdprojeto() != null){
			descricaoAntigo = beanAntigo.getProjetoprincipal().getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getProjetoprincipal");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = "";
		
		//compara a natureza de opera��o (venda)
		if(bean.getNaturezaoperacao() != null && bean.getNaturezaoperacao().getCdnaturezaoperacao() != null){
			descricaoCorrente = naturezaoperacaoService.load(bean.getNaturezaoperacao(), "naturezaoperacao.nome").getNome();
		} 
		if(beanAntigo.getNaturezaoperacao() != null && beanAntigo.getNaturezaoperacao().getCdnaturezaoperacao() != null){
			descricaoAntigo = beanAntigo.getNaturezaoperacao().getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getNaturezaoperacao");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = "";
		
		//compara a natureza de opera��o (Servi�o)
		if(bean.getNaturezaoperacaoservico() != null && bean.getNaturezaoperacaoservico().getCdnaturezaoperacao() != null){
			descricaoCorrente = naturezaoperacaoService.load(bean.getNaturezaoperacaoservico(), "naturezaoperacao.nome").getNome();
		} 
		if(beanAntigo.getNaturezaoperacaoservico() != null && beanAntigo.getNaturezaoperacaoservico().getCdnaturezaoperacao() != null){
			descricaoAntigo = beanAntigo.getNaturezaoperacaoservico().getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getNaturezaoperacaoservico");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = "";		
		
		//compara a natureza de opera��o (Expedi��o)
		if(bean.getNaturezaoperacaoexpedicao() != null && bean.getNaturezaoperacaoexpedicao().getCdnaturezaoperacao() != null){
			descricaoCorrente = naturezaoperacaoService.load(bean.getNaturezaoperacaoexpedicao(), "naturezaoperacao.nome").getNome();
		} 
		if(beanAntigo.getNaturezaoperacaoexpedicao() != null && beanAntigo.getNaturezaoperacaoexpedicao().getCdnaturezaoperacao() != null){
			descricaoAntigo = beanAntigo.getNaturezaoperacaoexpedicao().getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getNaturezaoperacaoexpedicao");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		} 
		descricaoAntigo = "";
		descricaoCorrente = "";
		
		//compara o cfop de insdustrializa��o
		if(bean.getCfopindustrializacao() != null && bean.getCfopindustrializacao().getCdcfop() != null){
			descricaoCorrente = cfopService.load(bean.getCfopindustrializacao(), "cfop.descricaocompleta").getDescricaocompleta();
		} 
		if(beanAntigo.getCfopindustrializacao() != null && beanAntigo.getCfopindustrializacao().getCdcfop() != null){
			descricaoAntigo = beanAntigo.getCfopindustrializacao().getDescricaocompleta();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getCfopindustrializacao");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = "";
		
		//compara o cfop de retorno
		if(bean.getCfopretorno() != null && bean.getCfopretorno().getCdcfop() != null){
			descricaoCorrente = cfopService.load(bean.getCfopretorno(), "cfop.descricaocompleta").getDescricaocompleta();
		} 
		if(beanAntigo.getCfopretorno() != null && beanAntigo.getCfopretorno().getCdcfop() != null){
			descricaoAntigo = beanAntigo.getCfopretorno().getDescricaocompleta();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getCfopretorno");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = "";
		
		//compara a situa��o da conta a receber
		if(bean.getDocumentoacao() != null && bean.getDocumentoacao().getCddocumentoacao() != null){
			descricaoCorrente = documentoacaoService.load(bean.getDocumentoacao(), "documentoacao.nome").getNome();
		} 
		if(beanAntigo.getDocumentoacao() != null && beanAntigo.getDocumentoacao().getCddocumentoacao() != null){
			descricaoAntigo =  documentoacaoService.load(beanAntigo.getDocumentoacao(), "documentoacao.nome").getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getDocumentoacao");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
			descricaoAntigo = "";
			descricaoCorrente = "";
		}
		
		//compara local de entrada automatica da coleta/troca na confirma��o do periodo
		if(bean.getLocalarmazenagemcoleta() != null && bean.getLocalarmazenagemcoleta().getCdlocalarmazenagem() != null){
			descricaoCorrente = localarmazenagemService.load(bean.getLocalarmazenagemcoleta(), "localarmazenagem.nome").getNome();
		} 
		if(beanAntigo.getLocalarmazenagemcoleta() != null && beanAntigo.getLocalarmazenagemcoleta().getCdlocalarmazenagem() != null){
			descricaoAntigo = localarmazenagemService.load(beanAntigo.getLocalarmazenagemcoleta(), "localarmazenagem.nome").getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getLocalarmazenagemcoleta");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = "";
		
		//compara natureza de opera��o da nota fiscal de entrada(emiss�o propria)
		if(bean.getNaturezaoperacaonotafiscalentrada() != null && bean.getNaturezaoperacaonotafiscalentrada().getCdnaturezaoperacao() != null){
			descricaoCorrente = naturezaoperacaoService.load(bean.getNaturezaoperacaonotafiscalentrada(), "naturezaoperacao.nome").getNome();
		} 
		if(beanAntigo.getNaturezaoperacaonotafiscalentrada() != null && beanAntigo.getNaturezaoperacaonotafiscalentrada().getCdnaturezaoperacao() != null){
			descricaoAntigo = naturezaoperacaoService.load(beanAntigo.getNaturezaoperacaonotafiscalentrada(), "naturezaoperacao.nome").getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getNaturezaoperacaonotafiscalentrada");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = ""; 
		
		//compara natureza de opera��o de saida fiscal
		if(bean.getNaturezaoperacaosaidafiscal() != null && bean.getNaturezaoperacaosaidafiscal().getCdnaturezaoperacao() != null){
			descricaoCorrente = naturezaoperacaoService.load(bean.getNaturezaoperacaosaidafiscal(), "naturezaoperacao.nome").getNome();
		} 
		if(beanAntigo.getNaturezaoperacaosaidafiscal() != null && beanAntigo.getNaturezaoperacaosaidafiscal().getCdnaturezaoperacao() != null){
			descricaoAntigo = naturezaoperacaoService.load(beanAntigo.getNaturezaoperacaosaidafiscal(), "naturezaoperacao.nome").getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getNaturezaoperacaosaidafiscal");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = ""; 
		
		if(bean.getProducaoagendatipo() != null && bean.getProducaoagendatipo().getCdproducaoagendatipo() != null){
			descricaoCorrente = producaoagendatipoService.load(bean.getProducaoagendatipo(), "producaoagendatipo.nome").getNome();
		} 
		if(beanAntigo.getProducaoagendatipo() != null && beanAntigo.getProducaoagendatipo().getCdproducaoagendatipo() != null){
			descricaoAntigo = producaoagendatipoService.load(beanAntigo.getProducaoagendatipo(), "producaoagendatipo.nome").getNome();
		} 
		if(!descricaoAntigo.equals(descricaoCorrente)){
			Method method = reflectionCache.getMethod(bean.getClass(), "getProducaoagendatipo");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigo.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrente).append("<br>");
			} else if(descricaoCorrente.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigo).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigo).append(para).append(descricaoCorrente).append("<br>");
			}
		}
		descricaoAntigo = "";
		descricaoCorrente = "";
		
		return descricaoCompleta.toString();
	}
	@Override
	protected void excluir(WebRequestContext request, Pedidovendatipo bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Tipo de pedido de venda n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected void validateBean(Pedidovendatipo bean, BindException errors) {
		if(bean.getPrincipal() != null && bean.getPrincipal() && pedidovendatipoService.existePedidovendatipoPrincipal(bean)){
			errors.reject("001", "J� existe um tipo de pedido de venda como principal.");
		}
		
		if (Boolean.TRUE.equals(bean.getGerarNotaIndustrializacaoRetorno()) && (bean.getCfopindustrializacao()==null || bean.getCfopretorno()==null)){
			errors.reject("001", "Os campos \"CFOP de Industrializa��o\" e \"CFOP de Retorno\" s�o obrigat�rios quando for Nota de Industrializa��o com Retorno.");
		}
		
		if(BaixaestoqueEnum.APOS_EXPEDICAOWMS.equals(bean.getBaixaestoqueEnum()) && (bean.getSincronizarComWMS() == null || !bean.getSincronizarComWMS())){
			errors.reject("001", "A op��o de baixar estoque 'Ap�s expedi��o no WMS' s� pode ser selecionada se o check 'Sincronizar com WMS' estiver marcado.");
		}
		
		super.validateBean(bean, errors);
	}
	
	public ModelAndView ajaxBuscarObrigatoriedadeLote(WebRequestContext request){
		String cdpedidovendatipo = request.getParameter("cdpedidovendatipo");
		Pedidovendatipo pedidovendatipo = null;
		boolean obrigarLoteOrcamento = false;
		boolean obrigarLotePedidovenda = false;
		boolean obrigarLoteVenda = false;
		
		if(!org.apache.commons.lang.StringUtils.isBlank(cdpedidovendatipo)){
			pedidovendatipo = pedidovendatipoService.load(new Pedidovendatipo(Integer.parseInt(cdpedidovendatipo)), "pedidovendatipo.obrigarLoteOrcamento ,pedidovendatipo.obrigarLotePedidovenda ,pedidovendatipo.obrigarLoteVenda"); 
		}
		
		if(pedidovendatipo != null){
			obrigarLoteOrcamento = pedidovendatipo.getObrigarLoteOrcamento();
			obrigarLotePedidovenda = pedidovendatipo.getObrigarLotePedidovenda();
			obrigarLoteVenda = pedidovendatipo.getObrigarLoteVenda();
		}
		
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("obrigarLoteOrcamento", obrigarLoteOrcamento);
		json.addObject("obrigarLotePedidovenda", obrigarLotePedidovenda);
		json.addObject("obrigarLoteVenda", obrigarLoteVenda);
		
		return json;
	}
	
}