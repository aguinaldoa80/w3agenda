package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Exameresponsavel;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExameresponsavelFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Exameresponsavel", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "nit", "registroconselho"})
public class ExameresponsavelCrud extends CrudControllerSined<ExameresponsavelFiltro, Exameresponsavel, Exameresponsavel> {
	
	@Override
	protected void salvar(WebRequestContext request, Exameresponsavel bean)throws Exception {
		try {
			bean.setNome(bean.getNome().trim());
			if(bean.getNit() != null)
				bean.setNit(bean.getNit().trim());
			if(bean.getRegistroconselho() != null)
				bean.setRegistroconselho(bean.getRegistroconselho().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_EXAMERESPONSAVEL_NOME")) {
				throw new SinedException("Exame profissional j� cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "idx_exameconvresp_unique")) 
				throw new SinedException("N�o � permitido cadastrar Conv�nios repetidos.");
		}
	}
	
}
