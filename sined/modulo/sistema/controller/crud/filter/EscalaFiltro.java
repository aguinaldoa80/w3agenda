package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EscalaFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected Boolean mostrar = Boolean.TRUE;
	
	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}

	@DisplayName("Mostrar")
	public Boolean getMostrar() {
		return mostrar;
	}

	public void setMostrar(Boolean mostrar) {
		this.mostrar = mostrar;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
