package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TaxaparcelaFiltro extends FiltroListagemSined{
	private Integer numeroparcelas;
	
	@DisplayName("N�mero de parcelas")
	public Integer getNumeroparcelas() {
		return numeroparcelas;
	}
	public void setNumeroparcelas(Integer numeroparcelas) {
		this.numeroparcelas = numeroparcelas;
	}
}
