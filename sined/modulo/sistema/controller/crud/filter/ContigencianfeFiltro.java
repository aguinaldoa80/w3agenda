package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContigencianfeFiltro extends FiltroListagemSined {
	
	protected Uf uf;
	protected Boolean ativo = Boolean.TRUE;
	
	public Uf getUf() {
		return uf;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
}
