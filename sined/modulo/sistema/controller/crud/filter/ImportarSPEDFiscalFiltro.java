package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.modulo.sistema.controller.process.GrupoTributacaoSPEDFiscal;
import br.com.linkcom.sined.modulo.sistema.controller.process.NatOprCfopSPEDFiscal;

public class ImportarSPEDFiscalFiltro{
	
	private Arquivo arquivo;
	private Empresa empresa;
	private String cnpj;
	private List<NatOprCfopSPEDFiscal> listaNatOprCfopSPEDFiscal;
	private List<GrupoTributacaoSPEDFiscal> listaGrupoTributacao;
	
	@Required
	@DisplayName("Arquivo TXT")
	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	@Required
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<NatOprCfopSPEDFiscal> getListaNatOprCfopSPEDFiscal() {
		return listaNatOprCfopSPEDFiscal;
	}
	
	public void setListaNatOprCfopSPEDFiscal(List<NatOprCfopSPEDFiscal> listaNatOprCfopSPEDFiscal) {
		this.listaNatOprCfopSPEDFiscal = listaNatOprCfopSPEDFiscal;
	}

	public List<GrupoTributacaoSPEDFiscal> getListaGrupoTributacao() {
		return listaGrupoTributacao;
	}

	public void setListaGrupoTributacao(List<GrupoTributacaoSPEDFiscal> listaGrupoTributacao) {
		this.listaGrupoTributacao = listaGrupoTributacao;
	}
	
	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
