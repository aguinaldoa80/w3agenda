package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ConfiguracaodreFiltro extends FiltroListagemSined {

	protected String descricao;
	protected Boolean ativo;
	
	@MaxLength(200)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
