package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.MinValue;
import br.com.linkcom.sined.geral.bean.Cbo;
import br.com.linkcom.sined.geral.bean.Departamento;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CargoFiltro extends FiltroListagemSined {
	protected String nome;
	protected Departamento departamento;
	protected Cbo cbo;
	protected String descricao;
	protected Integer codigoFolha;
	protected Boolean mostrar;
	
	@DisplayName("Nome")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	public Cbo getCbo() {
		return cbo;
	}
	
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setCbo(Cbo cbo) {
		this.cbo = cbo;
	}
	
	@DisplayName("C�DIGO FOLHA")
	@MaxLength(9)
	@MinValue(0)
	public Integer getCodigoFolha() {
		return codigoFolha;
	}
	
	public void setCodigoFolha(Integer codigoFolha) {
		this.codigoFolha = codigoFolha;
	}

	@DisplayName("Situa��o")
	public Boolean getMostrar() {
		return mostrar;
	}

	public void setMostrar(Boolean mostrar) {
		this.mostrar = mostrar;
	}
	
 	
	
}
