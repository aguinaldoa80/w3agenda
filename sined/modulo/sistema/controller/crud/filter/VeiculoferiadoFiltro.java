package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculoferiadoFiltro extends FiltroListagemSined{

	protected String nome;
	protected Date data1;
	protected Date data2;
	
	public String getNome() {
		return nome;
	}
	public Date getData1() {
		return data1;
	}
	public Date getData2() {
		return data2;
	}
	public void setData1(Date data1) {
		this.data1 = data1;
	}
	public void setData2(Date data2) {
		this.data2 = data2;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
