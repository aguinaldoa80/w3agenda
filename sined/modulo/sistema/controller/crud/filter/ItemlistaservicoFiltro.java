package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ItemlistaservicoFiltro extends FiltroListagemSined {
	
	protected String codigo;
	protected String descricao;
	
	@DisplayName("C�digo")
	@MaxLength(20)
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Descri��o")
	@MaxLength(1000)
	public String getDescricao() {
		return descricao;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
