package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CategoriaFiltro extends FiltroListagemSined{

	protected String nome;
	protected Boolean cliente = true;
	protected Boolean fornecedor = true;
	protected Categoria categoriapai;

	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Boolean getCliente() {
		return cliente;
	}

	public Boolean getFornecedor() {
		return fornecedor;
	}
	
	@DisplayName("Categoria superior")
	public Categoria getCategoriapai() {
		return categoriapai;
	}
	
	public void setCliente(Boolean cliente) {
		this.cliente = cliente;
	}
	
	public void setFornecedor(Boolean fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setCategoriapai(Categoria categoriapai) {
		this.categoriapai = categoriapai;
	}

}
