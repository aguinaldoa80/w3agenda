package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RegiaoFiltro extends FiltroListagemSined {
	
	private String nome;
	private Boolean ativo;
	private Cep cepinicio;
	private Cep cepfinal;
	private Colaborador colaborador;
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public Cep getCepinicio() {
		return cepinicio;
	}
	public Cep getCepfinal() {
		return cepfinal;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCepinicio(Cep cepinicio) {
		this.cepinicio = cepinicio;
	}
	public void setCepfinal(Cep cepfinal) {
		this.cepfinal = cepfinal;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	
}
