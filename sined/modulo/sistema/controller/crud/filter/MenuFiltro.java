package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Menumodulo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MenuFiltro extends FiltroListagemSined {
	
	private Menumodulo menumodulo;
	private String nome;
	private String url;
	
	@DisplayName("M�dulo")
	public Menumodulo getMenumodulo() {
		return menumodulo;
	}
	public String getNome() {
		return nome;
	}
	@DisplayName("URL")
	public String getUrl() {
		return url;
	}
	public void setMenumodulo(Menumodulo menumodulo) {
		this.menumodulo = menumodulo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
