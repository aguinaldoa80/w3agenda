package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Categoriaveiculo;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Veiculotipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VeiculomodeloFiltro extends FiltroListagemSined{

	protected String nome;
	protected Fornecedor fornecedor;
	protected Veiculotipo veiculotipo;
	protected Categoriaveiculo categoriaveiculo;

	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Fabricante")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	@DisplayName("Tipo de ve�culo")
	public Veiculotipo getVeiculotipo() {
		return veiculotipo;
	}

	@DisplayName("Categoria de ve�culo")
	public Categoriaveiculo getCategoriaveiculo() {
		return categoriaveiculo;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setVeiculotipo(Veiculotipo veiculotipo) {
		this.veiculotipo = veiculotipo;
	}

	public void setCategoriaveiculo(Categoriaveiculo categoriaveiculo) {
		this.categoriaveiculo = categoriaveiculo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}