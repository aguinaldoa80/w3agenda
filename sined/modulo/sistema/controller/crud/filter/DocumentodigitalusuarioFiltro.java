package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentodigitalusuarioFiltro extends FiltroListagemSined {

	private String email;
	private String nome;
	
	public String getEmail() {
		return email;
	}
	public String getNome() {
		return nome;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
