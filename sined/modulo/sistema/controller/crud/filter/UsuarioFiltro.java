package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class UsuarioFiltro extends FiltroListagemSined {
	protected String nome;
	protected Boolean bloqueado;
	protected Papel papel;
	protected String login;
	protected Empresa empresa;

	
	@DisplayName("Mostrar")
	public Boolean getBloqueado() {
		return bloqueado;
	}

	public Empresa getEmpresa() {
		return empresa;
	}
	
	@MaxLength(50)
	public String getLogin() {
		return login;
	}
	
	@DisplayName("Nome")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("N�vel")
	public Papel getPapel() {
		return papel;
	}
	
	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setPapel(Papel papel) {
		this.papel = papel;
	}

}
