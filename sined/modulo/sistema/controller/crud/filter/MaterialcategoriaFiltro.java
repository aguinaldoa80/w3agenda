package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaterialcategoriaFiltro extends FiltroListagemSined{

	protected String identificador;
	protected String descricao;
	protected Boolean ativo = Boolean.TRUE;

	@DisplayName("Identificador")
	@MaxLength(50)
	public String getIdentificador() {
		return identificador;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}