package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PrazopagamentoFiltro extends FiltroListagemSined{

	protected String nome;
	protected Boolean ativo;
	protected Boolean prazomedio;
	protected Money valorminimo;
	
	public PrazopagamentoFiltro(){
		if (!isNotFirstTime()) {
			this.ativo = true;
		}
	}
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@DisplayName("Prazo m�dio")
	public Boolean getPrazomedio() {
		return prazomedio;
	}
	@DisplayName("Valor m�nimo")
	public Money getValorminimo() {
		return valorminimo;
	}	


	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setPrazomedio(Boolean prazomedio) {
		this.prazomedio = prazomedio;
	}
	public void setValorminimo(Money valorminimo) {
		this.valorminimo = valorminimo;
	}
	
}
