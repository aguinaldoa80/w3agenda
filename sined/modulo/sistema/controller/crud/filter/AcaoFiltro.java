package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AcaoFiltro extends FiltroListagemSined{

	protected String key;
	protected String descricao;
	
	@DisplayName("Chave")
	public String getKey() {
		return key;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public void setKey(String key) {
		this.key = StringUtils.trimToNull(key);
	}
	public void setDescricao(String descricao) {
		this.descricao = StringUtils.trimToNull(descricao);
	}

	
	@Override
	public int getPageSize() {
		return 100;
	}
}
