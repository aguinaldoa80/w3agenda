package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Controleorcamentosituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ControleorcamentoFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected String descricao;
	protected Centrocusto centrocusto;
	protected Projeto projeto;
	protected Projetotipo projetoTipo;
	protected Contagerencial contagerencial;
	protected Money valor;
	protected List<Controleorcamentosituacao> listaControleoportunidadesituacao = new ListSet<Controleorcamentosituacao>(Controleorcamentosituacao.class);
	protected Boolean contaGerencialSemOrcamento = Boolean.TRUE;
	protected Date dtExercicioInicio;
	protected Date dtExercicioFim;
	
	
	public ControleorcamentoFiltro(){
		listaControleoportunidadesituacao.add(Controleorcamentosituacao.ABERTO);
	}
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	@DisplayName("Conta Gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Valor")
	@MaxLength(10)
	public Money getValor() {
		return valor;
	}
	@DisplayName("Situa��o")
	public List<Controleorcamentosituacao> getListaControleoportunidadesituacao() {
		return listaControleoportunidadesituacao;
	}
	@DisplayName("Relatar Conta Gerencial sem or�amento")
	public Boolean getContaGerencialSemOrcamento() {
		return contaGerencialSemOrcamento;
	}
	public Date getDtExercicioFim() {
		return dtExercicioFim;
	}
	public Date getDtExercicioInicio() {
		return dtExercicioInicio;
	}
	@DisplayName("Tipo Projeto")
	public Projetotipo getProjetoTipo() {
		return projetoTipo;
	}
	//////////////////////////////////////////////|
	//  INICIO SET						 /////////|
	//////////////////////////////////////////////|
	public void setDtExercicioFim(Date dtExercicioFim) {
		this.dtExercicioFim = dtExercicioFim;
	}
	public void setProjetoTipo(Projetotipo projetoTipo) {
		this.projetoTipo = projetoTipo;
	}
	public void setDtExercicioInicio(Date dtExercicioInicio) {
		this.dtExercicioInicio = dtExercicioInicio;
	}
	public void setContaGerencialSemOrcamento(Boolean contaGerencialSemOrcamento) {
		this.contaGerencialSemOrcamento = contaGerencialSemOrcamento;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}	
	public void setListaControleoportunidadesituacao(List<Controleorcamentosituacao> listaControleoportunidadesituacao) {
		this.listaControleoportunidadesituacao = listaControleoportunidadesituacao;
	}
	
	///Auxiliares
	@DisplayName("Inicio do Exercicio")
	@Transient
	public String getDtInicio() {
		if(this.dtExercicioInicio != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtExercicioInicio);
		return null;
	}
	public void setDtInicio(String dtInicio) {
		if(dtInicio!=null){
			try {
				this.dtExercicioInicio = new Date(new SimpleDateFormat("MM/yyyy").parse(dtInicio).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}else{
			setDtExercicioInicio(null);
		}
	}
	@DisplayName("Fim do Exercicio")
	@Transient
	public String getDtFim() {
		if(this.dtExercicioFim != null)
			return new SimpleDateFormat("MM/yyyy").format(this.dtExercicioFim);
		return null;
	}
	public void setDtFim(String dtFim) {
		if(dtFim!=null){
			try {
				this.dtExercicioFim = new Date(new SimpleDateFormat("MM/yyyy").parse(dtFim).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};
		}else{
			setDtExercicioFim(null);
		}
	}
	
}
