package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CodigotributacaoitemlistaservicoFiltro extends FiltroListagemSined {
	
	protected String codigo;
	protected String descricao;
	protected Uf uf;
	protected Municipio municipio;
	
	@DisplayName("C�digo")
	@MaxLength(20)
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Descri��o")
	@MaxLength(1000)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("UF")
	public Uf getUf() {
		return uf;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	
}
