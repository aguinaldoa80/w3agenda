package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ClassificacaoFiltro extends FiltroListagemSined {

	protected String identificador;
	protected String nome;
	protected Integer nivel;
	
	public String getIdentificador() {
		return identificador;
	}
	public String getNome() {
		return nome;
	}
	@MaxLength(2)
	@DisplayName("N�vel")
	public Integer getNivel() {
		return nivel;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	
}
