package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.TermoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TermoSistemaFiltro extends FiltroListagemSined{

	protected Integer cdtermosistema;
	protected TermoEnum termo;
	protected String novotermo;
	protected String urlcadastro;
	
	public Integer getCdtermosistema() {
		return cdtermosistema;
	}
	public void setCdtermosistema(Integer cdtermosistema) {
		this.cdtermosistema = cdtermosistema;
	}
	public TermoEnum getTermo() {
		return termo;
	}
	public void setTermo(TermoEnum termo) {
		this.termo = termo;
	}
	@DisplayName("Novo termo")
	public String getNovotermo() {
		return novotermo;
	}
	public void setNovotermo(String novotermo) {
		this.novotermo = novotermo;
	}
	@DisplayName("Url do cadastro")
	public String getUrlcadastro() {
		return urlcadastro;
	}
	public void setUrlcadastro(String urlcadastro) {
		this.urlcadastro = urlcadastro;
	}	
}
