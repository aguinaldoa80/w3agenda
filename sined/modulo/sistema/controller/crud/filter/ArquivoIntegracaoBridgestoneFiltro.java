package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.TipoArquivoIntegracaoBridgestone;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivoIntegracaoBridgestoneFiltro extends FiltroListagemSined {
	
	private TipoArquivoIntegracaoBridgestone tipo;
	private Boolean processada;
	private Date dtCriacao;
	
	
	public TipoArquivoIntegracaoBridgestone getTipo() {
		return tipo;
	}
	public Boolean getProcessada() {
		return processada;
	}
	@DisplayName("Data de cria��o")
	public Date getDtCriacao() {
		return dtCriacao;
	}
	public void setTipo(TipoArquivoIntegracaoBridgestone tipo) {
		this.tipo = tipo;
	}
	public void setProcessada(Boolean processada) {
		this.processada = processada;
	}
	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	} 

	
}
