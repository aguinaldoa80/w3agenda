package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.OtrPneuTipo;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PneuFiltro extends FiltroListagemSined {
	
	private Pneumarca pneumarca;
	private Pneumedida pneumedida;
	private Pneumodelo pneumodelo;
	protected String serie;
	protected String dot;
	protected OtrPneuTipo tipoPneuOtr; 
	protected PneuSegmento pneuSegmento; 
	
	private Boolean somenteclientepedido;
	private Cliente clientepedidovenda;
	private Boolean resgatarPneu;

	@DisplayName("Marca")
	public Pneumarca getPneumarca() {
		return pneumarca;
	}

	@DisplayName("Medida")
	public Pneumedida getPneumedida() {
		return pneumedida;
	}

	@DisplayName("Modelo")
	public Pneumodelo getPneumodelo() {
		return pneumodelo;
	}

	@DisplayName("S�rie")
	public String getSerie() {
		return serie;
	}

	public String getDot() {
		return dot;
	}

	public void setPneumedida(Pneumedida pneumedida) {
		this.pneumedida = pneumedida;
	}

	public void setPneumodelo(Pneumodelo pneumodelo) {
		this.pneumodelo = pneumodelo;
	}

	public void setPneumarca(Pneumarca pneumarca) {
		this.pneumarca = pneumarca;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public void setDot(String dot) {
		this.dot = dot;
	}

	public Boolean getSomenteclientepedido() {
		return somenteclientepedido;
	}

	public Cliente getClientepedidovenda() {
		return clientepedidovenda;
	}
	
	public Boolean getResgatarPneu() {
		return resgatarPneu;
	}

	public void setSomenteclientepedido(Boolean somenteclientepedido) {
		this.somenteclientepedido = somenteclientepedido;
	}

	public void setClientepedidovenda(Cliente clientepedidovenda) {
		this.clientepedidovenda = clientepedidovenda;
	}

	public void setResgatarPneu(Boolean resgatarPneu) {
		this.resgatarPneu = resgatarPneu;
	}

	public OtrPneuTipo getTipoPneuOtr() {
		return tipoPneuOtr;
	}

	public void setTipoPneuOtr(OtrPneuTipo tipoPneuOtr) {
		this.tipoPneuOtr = tipoPneuOtr;
	}

	public PneuSegmento getPneuSegmento() {
		return pneuSegmento;
	}

	public void setPneuSegmento(PneuSegmento pneuSegmento) {
		this.pneuSegmento = pneuSegmento;
	}
	
	
}
