package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.Faixaimpostocontrole;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class FaixaimpostoFiltro extends FiltroListagemSined {
	
	private String nome;
	private List<Faixaimpostocontrole> listaControle;
	private Money valoraliquota1;
	private Money valoraliquota2;
	private Boolean ativo = Boolean.TRUE;
	private Uf uf;
	private Municipio municipio;
	private Uf ufdestino;
	private Cliente cliente;
	private Empresa empresa;
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@DisplayName("Controle")
	public List<Faixaimpostocontrole> getListaControle() {
		return listaControle;
	}
	public Money getValoraliquota1() {
		return valoraliquota1;
	}
	public Money getValoraliquota2() {
		return valoraliquota2;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Município")
	public Municipio getMunicipio() {
		return municipio;
	}
	@DisplayName("UF Destino")
	public Uf getUfdestino() {
		return ufdestino;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setListaControle(List<Faixaimpostocontrole> listaControle) {
		this.listaControle = listaControle;
	}
	public void setValoraliquota1(Money valoraliquota1) {
		this.valoraliquota1 = valoraliquota1;
	}
	public void setValoraliquota2(Money valoraliquota2) {
		this.valoraliquota2 = valoraliquota2;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setUfdestino(Uf ufdestino) {
		this.ufdestino = ufdestino;
	}
}
