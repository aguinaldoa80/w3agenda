package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PedidovendatipoFiltro extends FiltroListagemSined{

	protected String descricao;
	protected Naturezaoperacao naturezaoperacao;

	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Natureza da Opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}	
}
