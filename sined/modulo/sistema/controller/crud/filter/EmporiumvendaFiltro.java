package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmporiumvendaFiltro extends FiltroListagemSined {

	protected String numero_ticket;
	protected Date data1;
	protected Date data2;
	protected Double valor1;
	protected Double valor2;
	protected Boolean vendacriada;
	protected Integer venda;
	protected String loja;
	protected String pdv;
	protected String pedidovenda;
	
	@DisplayName("N�mero do ticket")
	public String getNumero_ticket() {
		return numero_ticket;
	}
	public Double getValor1() {
		return valor1;
	}
	public Double getValor2() {
		return valor2;
	}
	public Integer getVenda() {
		return venda;
	}
	public Date getData1() {
		return data1;
	}
	public Date getData2() {
		return data2;
	}
	public String getPdv() {
		return pdv;
	}
	public Boolean getVendacriada() {
		return vendacriada;
	}
	public String getLoja() {
		return loja;
	}
	public void setLoja(String loja) {
		this.loja = loja;
	}
	public void setVendacriada(Boolean vendacriada) {
		this.vendacriada = vendacriada;
	}
	public void setPdv(String pdv) {
		this.pdv = pdv;
	}
	public void setData1(Date data1) {
		this.data1 = data1;
	}
	public void setData2(Date data2) {
		this.data2 = data2;
	}
	public void setNumero_ticket(String numeroTicket) {
		numero_ticket = numeroTicket;
	}
	public void setValor1(Double valor1) {
		this.valor1 = valor1;
	}
	public void setValor2(Double valor2) {
		this.valor2 = valor2;
	}
	public void setVenda(Integer venda) {
		this.venda = venda;
	}
	@DisplayName("Pedido de Venda")
	public String getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(String pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
}
