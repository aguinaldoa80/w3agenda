package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BancoConfiguracaoSegmentoFiltro extends FiltroListagemSined{

	private String nome;
	private Banco banco;
	private BancoConfiguracaoTipoSegmentoEnum tipoSegmento;
	private String selecteditens;

	public String getNome() {
		return nome;
	}
	public Banco getBanco() {
		return banco;
	}
	@DisplayName("Tipo de segmento")
	public BancoConfiguracaoTipoSegmentoEnum getTipoSegmento() {
		return tipoSegmento;
	}
	public String getSelecteditens() {
		return selecteditens;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setTipoSegmento(BancoConfiguracaoTipoSegmentoEnum tipoSegmento) {
		this.tipoSegmento = tipoSegmento;
	}
	public void setSelecteditens(String selecteditens) {
		this.selecteditens = selecteditens;
	}
}
