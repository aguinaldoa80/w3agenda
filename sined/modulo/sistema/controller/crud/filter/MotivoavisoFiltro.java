package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Tipoaviso;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MotivoavisoFiltro extends FiltroListagemSined {
	
	private AvisoOrigem avisoorigem;
	private Tipoaviso tipoaviso;
	private Papel papel;
	private Usuario usuario;
	private Boolean ativo = Boolean.TRUE;
	
	@DisplayName("Origem")
	public AvisoOrigem getAvisoorigem() {
		return avisoorigem;
	}
	@DisplayName("Tipo de Aviso")
	public Tipoaviso getTipoaviso() {
		return tipoaviso;
	}
	@DisplayName("N�vel")
	public Papel getPapel() {
		return papel;
	}
	@DisplayName("Usu�rio")
	public Usuario getUsuario() {
		return usuario;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setAvisoorigem(AvisoOrigem avisoorigem) {
		this.avisoorigem = avisoorigem;
	}
	public void setTipoaviso(Tipoaviso tipoaviso) {
		this.tipoaviso = tipoaviso;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
