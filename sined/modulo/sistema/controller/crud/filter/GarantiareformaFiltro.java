package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Garantiareformaresultado;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GarantiareformaFiltro extends FiltroListagemSined{

	private Empresa empresa;
	private Cliente cliente;
	private Pedidovenda pedidovenda;
	private Pedidovenda pedidovendaorigem;
	private String identificadorexternopedidovenda;
	private Colaborador vendedor;
	private Boolean pedidoexterno;
	private Garantiasituacao garantiasituacao;
	private Material servicogarantido;
	private Integer pneu;
	private Garantiareformaresultado garantiareformaresultado;
	
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	@DisplayName("Pedido de Venda")
	public Pedidovenda getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(Pedidovenda pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
	
	public Pedidovenda getPedidovendaorigem() {
		return pedidovendaorigem;
	}
	
	@DisplayName("Pedido de Venda Origem")
	public void setPedidovendaorigem(Pedidovenda pedidovendaorigem) {
		this.pedidovendaorigem = pedidovendaorigem;
	}
	
	@DisplayName("Id. Externo Pedido Venda")
	public String getIdentificadorexternopedidovenda() {
		return identificadorexternopedidovenda;
	}
	public void setIdentificadorexternopedidovenda(
			String identificadorexternopedidovenda) {
		this.identificadorexternopedidovenda = identificadorexternopedidovenda;
	}
	
	@DisplayName("Vendedor")
	public Colaborador getVendedor() {
		return vendedor;
	}
	public void setVendedor(Colaborador vendedor) {
		this.vendedor = vendedor;
	}
	
	@DisplayName("Pedido externo?")
	public Boolean getPedidoexterno() {
		return pedidoexterno;
	}
	public void setPedidoexterno(Boolean pedidoexterno) {
		this.pedidoexterno = pedidoexterno;
	}
	
	@DisplayName("Situa��o")
	public Garantiasituacao getGarantiasituacao() {
		return garantiasituacao;
	}
	public void setGarantiasituacao(Garantiasituacao garantiasituacao) {
		this.garantiasituacao = garantiasituacao;
	}
	
	@DisplayName("Servi�o garantido")
	public Material getServicogarantido() {
		return servicogarantido;
	}
	public void setServicogarantido(Material servicogarantido) {
		this.servicogarantido = servicogarantido;
	}
	
	@DisplayName("Pneu")
	public Integer getPneu() {
		return pneu;
	}
	public void setPneu(Integer pneu) {
		this.pneu = pneu;
	}
	
	@DisplayName("Resultado da garantia")
	public Garantiareformaresultado getGarantiareformaresultado() {
		return garantiareformaresultado;
	}
	public void setGarantiareformaresultado(
			Garantiareformaresultado garantiareformaresultado) {
		this.garantiareformaresultado = garantiareformaresultado;
	}
}
