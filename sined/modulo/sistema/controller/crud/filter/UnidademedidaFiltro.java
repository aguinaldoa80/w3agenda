package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class UnidademedidaFiltro extends FiltroListagemSined{

	protected String nome;
	protected String simbolo;
	protected Boolean ativo;
	
	public UnidademedidaFiltro(){
		if (!isNotFirstTime()) {
			this.ativo = true;
		}
	}
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@MaxLength(10)
	@DisplayName("S�mbolo")
	public String getSimbolo() {
		return simbolo;
	}
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
