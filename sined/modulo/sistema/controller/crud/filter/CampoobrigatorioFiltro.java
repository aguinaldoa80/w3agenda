package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CampoobrigatorioFiltro extends FiltroListagemSined {

	private String path;
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
}
