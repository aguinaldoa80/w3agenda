package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RateioapuracaoFiltro extends FiltroListagemSined {

	private String nome;
	private Centrocusto centrocusto;
	private Contagerencial contagerencial;
	private Empresa empresa;
	
	public String getNome() {
		return nome;
	}

	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}

	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}

	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
}
