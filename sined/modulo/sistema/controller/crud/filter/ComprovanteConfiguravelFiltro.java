package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ComprovanteConfiguravelFiltro extends FiltroListagemSined{
	
	protected String descricao;
	protected String layout;
	protected Boolean venda;
	protected Boolean pedidoVenda;
	protected Boolean orcamento;
	protected Boolean coleta;
	protected Boolean ativo;
	protected Boolean txt;
	
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Venda")
	public Boolean getVenda() {
		return venda;
	}

	@DisplayName("Pedido de Venda")
	public Boolean getPedidoVenda() {
		return pedidoVenda;
	}

	@DisplayName("Or�amento")
	public Boolean getOrcamento() {
		return orcamento;
	}
	
	@DisplayName("Coleta")
	public Boolean getColeta() {
		return coleta;
	}

	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("Gerar Txt")
	public Boolean getTxt() {
		return txt;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}


	public void setVenda(Boolean venda) {
		this.venda = venda;
	}

	public void setPedidoVenda(Boolean pedidoVenda) {
		this.pedidoVenda = pedidoVenda;
	}

	public void setOrcamento(Boolean orcamento) {
		this.orcamento = orcamento;
	}
	
	public void setColeta(Boolean coleta) {
		this.coleta = coleta;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setTxt(Boolean txt) {
		this.txt = txt;
	}
}
