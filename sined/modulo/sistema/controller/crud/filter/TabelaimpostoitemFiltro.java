package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tabelaimposto;
import br.com.linkcom.sined.geral.bean.enumeration.Tabelaimpostoitemtipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TabelaimpostoitemFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private Tabelaimposto tabelaimposto;
	private Tabelaimpostoitemtipo tipo;
	private String codigo;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	@DisplayName("Tabela IBPT")
	public Tabelaimposto getTabelaimposto() {
		return tabelaimposto;
	}
	
	public Tabelaimpostoitemtipo getTipo() {
		return tipo;
	}
	
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public void setTipo(Tabelaimpostoitemtipo tipo) {
		this.tipo = tipo;
	}
	
	public void setTabelaimposto(Tabelaimposto tabelaimposto) {
		this.tabelaimposto = tabelaimposto;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
}
