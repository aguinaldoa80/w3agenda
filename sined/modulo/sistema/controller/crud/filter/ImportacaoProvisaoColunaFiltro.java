package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.ImportacaoProvisaoCampo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ImportacaoProvisaoColunaFiltro extends FiltroListagemSined {
	private Integer coluna;
	private ImportacaoProvisaoCampo tipo;
	
	@DisplayName("Coluna")
	public Integer getColuna() {
		return coluna;
	}
	
	@DisplayName("Campo")
	public ImportacaoProvisaoCampo getTipo() {
		return tipo;
	}
	public void setColuna(Integer coluna) {
		this.coluna = coluna;
	}
	public void setTipo(ImportacaoProvisaoCampo tipo) {
		this.tipo = tipo;
	}
}
