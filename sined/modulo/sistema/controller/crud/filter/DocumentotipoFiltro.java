package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentotipoFiltro extends FiltroListagemSined{

	protected String nome;
	protected Boolean notafiscal;

	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Mostrar")
	public Boolean getNotafiscal() {
		return notafiscal;
	}
	
	public void setNotafiscal(Boolean notafiscal) {
		this.notafiscal = notafiscal;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
