package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmporiumpedidovendaFiltro extends FiltroListagemSined {

	protected String pedidovenda;
	
	@DisplayName("Pedido de Venda")
	public String getPedidovenda() {
		return pedidovenda;
	}
	public void setPedidovenda(String pedidovenda) {
		this.pedidovenda = pedidovenda;
	}
}
