package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MeiocontatoFiltro extends FiltroListagemSined{

	protected String nome;
	protected Boolean ativo = Boolean.TRUE;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
