package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AreaFiltro extends FiltroListagemSined {
	
	protected String descricao;
	protected Projeto projeto;
	protected Boolean ativo = true;

	@DisplayName("Nome")
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
