package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RateiomodeloFiltro extends FiltroListagemSined{

	private String nome;
	private Tipooperacao tipooperacao;
	
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("Opera��o")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
}
