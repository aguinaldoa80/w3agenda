package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TabelaimpostoFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
}
