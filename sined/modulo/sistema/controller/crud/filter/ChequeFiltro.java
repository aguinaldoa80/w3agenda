package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Chequedevolucaomotivo;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.bean.enumeration.ExibirChequesEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ChequeFiltro extends FiltroListagemSined{

	protected String emitente;
	protected String numero;
	protected Money minValor;
	protected Money maxValor;
	protected Boolean chequeusados;
	protected String observacao;
	protected Date dtbomparade;
	protected Date dtbomparaate;
	protected Date dtentradade;
	protected Date dtentradaate;
	protected Integer banco;
	protected Integer agencia;
	protected Integer conta;
	protected Empresa empresa;
	protected List<Chequesituacao> listaChequesituacao;
	protected Chequesituacao chequesituacao;
	protected Chequedevolucaomotivo chequedevolucaomotivo;
	protected String vendaNota;
	protected String whereInChequeClienteVendaNota;
	protected String recebidode;
	protected String pagoa;
	protected Conta contamovimentacao;
	protected Contatipo contatipomovimentacao;
	protected ExibirChequesEnum exibirCheques;
	protected Boolean chequeProprio;
	
	//totalizadores
	Money valorTotal = new Money(0);
	Money totalPrevisto = new Money(0);
	Money totalDevolvido = new Money(0);
	Money totalBaixado = new Money(0);
	Money totalCancelado = new Money(0);
	Money totalSubstituido = new Money(0);
	Money totalCompensado = new Money(0);
	
	public ChequeFiltro() {
		listaChequesituacao = new ArrayList<Chequesituacao>();
		listaChequesituacao.add(Chequesituacao.PREVISTO);
		listaChequesituacao.add(Chequesituacao.BAIXADO);
	}
	
	@DisplayName("Emitente")
	@MaxLength(100)
	public String getEmitente() {
		return emitente;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	
	@DisplayName("N�mero")
	@MaxLength(100)
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	@DisplayName("Valor")
	public Money getMinValor() {
		return minValor;
	}
	public void setMinValor(Money minValor) {
		this.minValor = minValor;
	}
	@DisplayName("Valor")
	public Money getMaxValor() {
		return maxValor;
	}
	@DisplayName("Observa��o")
	@MaxLength(100)
	public String getObservacao() {
		return observacao;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public void setMaxValor(Money maxValor) {
		this.maxValor = maxValor;
	}
	public Boolean getChequeusados() {
		return chequeusados;
	}
	public void setChequeusados(Boolean chequeusados) {
		this.chequeusados = chequeusados;
	}
	public Date getDtbomparade() {
		return dtbomparade;
	}
	public Date getDtbomparaate() {
		return dtbomparaate;
	}
	public Date getDtentradaate() {
		return dtentradaate;
	}
	public Date getDtentradade() {
		return dtentradade;
	}
	public void setDtentradaate(Date dtentradaate) {
		this.dtentradaate = dtentradaate;
	}
	public void setDtentradade(Date dtentradade) {
		this.dtentradade = dtentradade;
	}
	public void setDtbomparade(Date dtbomparade) {
		this.dtbomparade = dtbomparade;
	}
	public void setDtbomparaate(Date dtbomparaate) {
		this.dtbomparaate = dtbomparaate;
	}
	@DisplayName("Banco")
	@MaxLength(9)
	public Integer getBanco() {
		return banco;
	}
	@DisplayName("Ag�ncia")
	@MaxLength(9)
	public Integer getAgencia() {
		return agencia;
	}
	@DisplayName("Conta")
	@MaxLength(9)
	public Integer getConta() {
		return conta;
	}
	public void setBanco(Integer banco) {
		this.banco = banco;
	}
	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	
	@DisplayName("Situa��o")
	public List<Chequesituacao> getListaChequesituacao() {
		return listaChequesituacao;
	}
	@DisplayName("Situa��o (Hist�rico)")
	public Chequesituacao getChequesituacao() {
		return chequesituacao;
	}
	@DisplayName("Motivo de Devolu��o")
	public Chequedevolucaomotivo getChequedevolucaomotivo() {
		return chequedevolucaomotivo;
	}
	
	public void setListaChequesituacao(List<Chequesituacao> listaChequesituacao) {
		this.listaChequesituacao = listaChequesituacao;
	}
	public void setChequesituacao(Chequesituacao chequesituacao) {
		this.chequesituacao = chequesituacao;
	}
	public void setChequedevolucaomotivo(Chequedevolucaomotivo chequedevolucaomotivo) {
		this.chequedevolucaomotivo = chequedevolucaomotivo;
	}
	@DisplayName("Venda/Nota")
	public String getVendaNota() {
		return vendaNota;
	}
	public void setVendaNota(String vendaNota) {
		this.vendaNota = vendaNota;
	}

	public String getWhereInChequeClienteVendaNota() {
		return whereInChequeClienteVendaNota;
	}

	public void setWhereInChequeClienteVendaNota(String whereInChequeClienteVendaNota) {
		this.whereInChequeClienteVendaNota = whereInChequeClienteVendaNota;
	}
	
	@DisplayName("Recebido de")
	public String getRecebidode() {
		return recebidode;
	}
	@DisplayName("Pago a")
	public String getPagoa() {
		return pagoa;
	}
	@DisplayName("Conta")
	public Conta getContamovimentacao() {
		return contamovimentacao;
	}
	@DisplayName("V�nculo")
	public Contatipo getContatipomovimentacao() {
		return contatipomovimentacao;
	}

	public void setRecebidode(String recebidode) {
		this.recebidode = recebidode;
	}
	public void setPagoa(String pagoa) {
		this.pagoa = pagoa;
	}
	public void setContamovimentacao(Conta contamovimentacao) {
		this.contamovimentacao = contamovimentacao;
	}
	public void setContatipomovimentacao(Contatipo contatipomovimentacao) {
		this.contatipomovimentacao = contatipomovimentacao;
	}
	
	@DisplayName("Exibir cheques")
	public ExibirChequesEnum getExibirCheques() {
		return exibirCheques;
	}
	
	@DisplayName("Cheque pr�prio")
	public Boolean getChequeProprio() {
		return chequeProprio;
	}
	
	public void setExibirCheques(ExibirChequesEnum exibirCheques) {
		this.exibirCheques = exibirCheques;
	}
	
	public void setChequeProprio(Boolean chequeProprio) {
		this.chequeProprio = chequeProprio;
	}
	
	@DisplayName("Total (geral)")
	public Money getValorTotal() {
		return valorTotal;
	}

	@DisplayName("Total (previsto)")
	public Money getTotalPrevisto() {
		return totalPrevisto;
	}
	
	@DisplayName("Total (devolvido)")
	public Money getTotalDevolvido() {
		return totalDevolvido;
	}
	
	@DisplayName("Total (baixado)")
	public Money getTotalBaixado() {
		return totalBaixado;
	}
	
	@DisplayName("Total (cancelado)")
	public Money getTotalCancelado() {
		return totalCancelado;
	}
	
	@DisplayName("Total (substituido)")
	public Money getTotalSubstituido() {
		return totalSubstituido;
	}
	
	@DisplayName("Total (compensado)")
	public Money getTotalCompensado() {
		return totalCompensado;
	}

	public void setValorTotal(Money valorTotal) {
		this.valorTotal = valorTotal;
	}

	public void setTotalPrevisto(Money totalPrevisto) {
		this.totalPrevisto = totalPrevisto;
	}

	public void setTotalDevolvido(Money totalDevolvido) {
		this.totalDevolvido = totalDevolvido;
	}

	public void setTotalBaixado(Money totalBaixado) {
		this.totalBaixado = totalBaixado;
	}

	public void setTotalCancelado(Money totalCancelado) {
		this.totalCancelado = totalCancelado;
	}

	public void setTotalSubstituido(Money totalSubstituido) {
		this.totalSubstituido = totalSubstituido;
	}

	public void setTotalCompensado(Money totalCompensado) {
		this.totalCompensado = totalCompensado;
	}
	
	
}