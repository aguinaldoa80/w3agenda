package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BancoConfiguracaoRetornoFiltro extends FiltroListagemSined{

	private String nome;
	private Banco banco;
	private String selecteditens;

	public String getNome() {
		return nome;
	}
	public Banco getBanco() {
		return banco;
	}
	public String getSelecteditens() {
		return selecteditens;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setSelecteditens(String selecteditens) {
		this.selecteditens = selecteditens;
	}
}
