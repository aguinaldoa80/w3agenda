package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NaturezaoperacaoFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected String descricao;
	protected String codigonfse;
	protected Operacaocontabil operacaocontabilavista;
	protected Operacaocontabil operacaocontabilaprazo;
	protected Operacaocontabil operacaocontabilpagamento;
	protected NotaTipo notaTipo;
	protected Boolean ativo = Boolean.TRUE;
	protected Cfop cfop;
	
	@MaxLength(100)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	@MaxLength(100)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@MaxLength(50)
	@DisplayName("C�digo para NFS-e")
	public String getCodigonfse() {
		return codigonfse;
	}
	@DisplayName("Opera��o Cont�bil - A vista")
	public Operacaocontabil getOperacaocontabilavista() {
		return operacaocontabilavista;
	}
	@DisplayName("Opera��o Cont�bil - A prazo")
	public Operacaocontabil getOperacaocontabilaprazo() {
		return operacaocontabilaprazo;
	}
	@DisplayName("Opera��o Cont�bil - Pagamento")
	public Operacaocontabil getOperacaocontabilpagamento() {
		return operacaocontabilpagamento;
	}
	@DisplayName("Aplica��o em")
	public NotaTipo getNotaTipo() {
		return notaTipo;
	}
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("CFOP")
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCodigonfse(String codigonfse) {
		this.codigonfse = codigonfse;
	}
	public void setOperacaocontabilavista(Operacaocontabil operacaocontabilavista) {
		this.operacaocontabilavista = operacaocontabilavista;
	}
	public void setOperacaocontabilaprazo(Operacaocontabil operacaocontabilaprazo) {
		this.operacaocontabilaprazo = operacaocontabilaprazo;
	}
	public void setOperacaocontabilpagamento(Operacaocontabil operacaocontabilpagamento) {
		this.operacaocontabilpagamento = operacaocontabilpagamento;
	}
	public void setNotaTipo(NotaTipo notaTipo) {
		this.notaTipo = notaTipo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}	
}