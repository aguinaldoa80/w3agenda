package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ControleorcamentoitemFiltro extends FiltroListagemSined {
	
	protected Controleorcamento controleOrcamento;
	protected Centrocusto centroCusto;
	protected Projeto projeto;
	protected Contagerencial contraGerencial;
	protected String mesAno;
	
	protected Date mesAnoDate;
	
	@DisplayName("Or�amento")
	public Controleorcamento getControleOrcamento() {
		return controleOrcamento;
	}
	
	@DisplayName("Centro de custo")
	public Centrocusto getCentroCusto() {
		return centroCusto;
	}
	
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Conta gerencial")
	public Contagerencial getContraGerencial() {
		return contraGerencial;
	}
	@DisplayName("M�s/Ano")
	public String getMesAno() {
		return mesAno;
	}
	public void setControleOrcamento(Controleorcamento controleOrcamento) {
		this.controleOrcamento = controleOrcamento;
	}
	public void setCentroCusto(Centrocusto centroCusto) {
		this.centroCusto = centroCusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setContraGerencial(Contagerencial contraGerencial) {
		this.contraGerencial = contraGerencial;
	}
	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}
	
	public Date getMesAnoDate() {
		if(this.mesAno != null){
			try {
				this.mesAnoDate = new Date(new SimpleDateFormat("MM/yyyy").parse(mesAno).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			};	
			return this.mesAnoDate;
		}
		return null;
	}
}
