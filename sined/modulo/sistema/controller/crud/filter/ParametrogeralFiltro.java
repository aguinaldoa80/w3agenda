package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ParametrogeralFiltro extends FiltroListagemSined {
	protected String nome;
	protected String valor;
	
	@DisplayName("Nome")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Valor")
	@MaxLength(250)
	public String getValor() {
		return valor;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
