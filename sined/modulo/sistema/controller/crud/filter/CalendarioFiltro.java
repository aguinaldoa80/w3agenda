package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Orcamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CalendarioFiltro extends FiltroListagemSined {
	
	protected String descricao;
	protected Projeto projeto; 
	protected Orcamento orcamento;
	
	
	public Projeto getProjeto() {
		return projeto;
	}
	
	@DisplayName("Or�amento")
	public Orcamento getOrcamento() {
		return orcamento;
	}
	
	@MaxLength(40)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setOrcamento(Orcamento orcamento) {
		this.orcamento = orcamento;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
}
