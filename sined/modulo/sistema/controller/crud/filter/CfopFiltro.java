package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cfopescopo;
import br.com.linkcom.sined.geral.bean.Cfoptipo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CfopFiltro extends FiltroListagemSined {
	
	protected String codigo;
	protected String descricaoresumida;
	protected Cfoptipo cfoptipo;
	protected Cfopescopo cfopescopo;
	protected Naturezaoperacao naturezaoperacao; 
	
	@MaxLength(4)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}
	@DisplayName("Tipo")
	public Cfoptipo getCfoptipo() {
		return cfoptipo;
	}
	@DisplayName("Escopo")
	public Cfopescopo getCfopescopo() {
		return cfopescopo;
	}
	@MaxLength(255)
	@DisplayName("Descri��o")
	public String getDescricaoresumida() {
		return descricaoresumida;
	}
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setCfoptipo(Cfoptipo cfoptipo) {
		this.cfoptipo = cfoptipo;
	}
	public void setCfopescopo(Cfopescopo cfopescopo) {
		this.cfopescopo = cfopescopo;
	}
	public void setDescricaoresumida(String descricaoresumida) {
		this.descricaoresumida = descricaoresumida;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
}