package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CargoriscotecnicaFiltro extends FiltroListagemSined {
	
 	private String nome;
 	
 	@MaxLength(40)
 	public String getNome() {
		return nome;
	}
 	
 	public void setNome(String nome) {
		this.nome = nome;
	}
 	
}
