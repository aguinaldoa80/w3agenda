package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GrupoquestionarioFiltro extends FiltroListagemSined {
	protected String descricao;
	protected Boolean ativo;
	protected Atividadetipo atividadetipo;

	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Tipo de atividade")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
}
