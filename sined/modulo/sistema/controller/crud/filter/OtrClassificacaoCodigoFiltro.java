package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;


import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.OtrDesenho;
import br.com.linkcom.sined.geral.bean.OtrServicoTipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OtrClassificacaoCodigoFiltro extends FiltroListagemSined{

		protected String codigo;
		protected OtrDesenho otrDesenho;
		protected OtrServicoTipo otrServicoTipo;
		

		@DisplayName("C�DIGO")
		public String getCodigo() {
			return codigo;
		}
		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}
		@DisplayName("DESENHO")
		public OtrDesenho getOtrDesenho() {
			return otrDesenho;
		}
		public void setOtrDesenho(OtrDesenho otrDesenho) {
			this.otrDesenho = otrDesenho;
		}
		@DisplayName("TIPO DE SERVI�O")
		public OtrServicoTipo getOtrServicoTipo() {
			return otrServicoTipo;
		}
		public void setOtrServicoTipo(OtrServicoTipo otrServicoTipo) {
			this.otrServicoTipo = otrServicoTipo;
		}
	
	
		
		
	
		
		
		
}
