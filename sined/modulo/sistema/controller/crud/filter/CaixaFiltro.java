package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CaixaFiltro extends FiltroListagemSined{
	
	protected Empresa empresa;
	protected String nome;
	protected Boolean ativo = new Boolean(true);
	protected Boolean desconsiderarsaldoinicialfluxocaixa;

	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Desconsiderar saldo inicial no fluxo de caixa")
	public Boolean getDesconsiderarsaldoinicialfluxocaixa() {
		return desconsiderarsaldoinicialfluxocaixa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setDesconsiderarsaldoinicialfluxocaixa(
			Boolean desconsiderarsaldoinicialfluxocaixa) {
		this.desconsiderarsaldoinicialfluxocaixa = desconsiderarsaldoinicialfluxocaixa;
	}
}
