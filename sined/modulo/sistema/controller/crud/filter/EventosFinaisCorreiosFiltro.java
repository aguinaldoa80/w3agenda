package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;


import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.TipoEventoCorreios;
//import br.com.linkcom.sined.geral.bean.enumeration.TipoEventoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EventosFinaisCorreiosFiltro extends FiltroListagemSined{
	protected String nome;
	protected String letra;
	protected TipoEventoCorreios tipoEventos;
	
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@MaxLength(2)
	public String getLetra() {
		return letra;
	}
	public void setLetra(String letra) {
		this.letra = letra;
	}
	public TipoEventoCorreios getTipoEventos() {
		return tipoEventos;
	}
	public void setTipoEventos(TipoEventoCorreios tipoEventos) {
		this.tipoEventos = tipoEventos;
	}

	
	
	
	

}
