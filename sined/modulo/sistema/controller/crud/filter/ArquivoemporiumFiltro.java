package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivoemporiumtipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivoemporiumFiltro extends FiltroListagemSined {
	
	protected Arquivoemporiumtipo arquivoemporiumtipo;
	protected Date dtgeracao1;
	protected Date dtgeracao2;
	protected Date dtmovimento1;
	protected Date dtmovimento2;
	protected Boolean comresultados;
	
	protected String whereIn;
	protected Empresa empresa;
	protected Material material;
	protected Cliente cliente;
	protected Boolean substuicaotabela;
	
	public Arquivoemporiumtipo getArquivoemporiumtipo() {
		return arquivoemporiumtipo;
	}
	public Date getDtgeracao1() {
		return dtgeracao1;
	}
	public Date getDtgeracao2() {
		return dtgeracao2;
	}
	public Date getDtmovimento1() {
		return dtmovimento1;
	}
	public Date getDtmovimento2() {
		return dtmovimento2;
	}
	public Boolean getComresultados() {
		return comresultados;
	}
	public String getWhereIn() {
		return whereIn;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Material getMaterial() {
		return material;
	}
	public Boolean getSubstuicaotabela() {
		return substuicaotabela;
	}
	public void setSubstuicaotabela(Boolean substuicaotabela) {
		this.substuicaotabela = substuicaotabela;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setWhereIn(String whereIn) {
		this.whereIn = whereIn;
	}
	public void setComresultados(Boolean comresultados) {
		this.comresultados = comresultados;
	}
	public void setDtmovimento1(Date dtmovimento1) {
		this.dtmovimento1 = dtmovimento1;
	}
	public void setDtmovimento2(Date dtmovimento2) {
		this.dtmovimento2 = dtmovimento2;
	}
	public void setArquivoemporiumtipo(Arquivoemporiumtipo arquivoemporiumtipo) {
		this.arquivoemporiumtipo = arquivoemporiumtipo;
	}
	public void setDtgeracao1(Date dtgeracao1) {
		this.dtgeracao1 = dtgeracao1;
	}
	public void setDtgeracao2(Date dtgeracao2) {
		this.dtgeracao2 = dtgeracao2;
	}
	
}
