package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BancoConfiguracaoRemessaFiltro extends FiltroListagemSined{

	private String nome;
	private Banco banco;
	private BancoConfiguracaoRemessaTipoEnum bancoConfiguracaoRemessaTipo;

	public String getNome() {
		return nome;
	}
	public Banco getBanco() {
		return banco;
	}
	@DisplayName("Tipo")
	public BancoConfiguracaoRemessaTipoEnum getBancoConfiguracaoRemessaTipo() {
		return bancoConfiguracaoRemessaTipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setBancoConfiguracaoRemessaTipo(
			BancoConfiguracaoRemessaTipoEnum bancoConfiguracaoRemessaTipo) {
		this.bancoConfiguracaoRemessaTipo = bancoConfiguracaoRemessaTipo;
	}
	
}
