package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Planejamento;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ValorreferenciaFiltro extends FiltroListagemSined {
	
	protected Projeto projeto;
	protected Planejamento planejamento;
	protected Material material;
	protected Cargo cargo;
	protected Boolean fromOrcamento;
	
	//|..GET..|
	public Projeto getProjeto() {
		return projeto;
	}
	public Planejamento getPlanejamento() {
		return planejamento;
	}
	public Material getMaterial() {
		return material;
	}
	@DisplayName("Fun��o")
	public Cargo getCargo() {
		return cargo;
	}
	public Boolean getFromOrcamento() {
		return fromOrcamento;
	}
	
	//|..SET..|
	public void setFromOrcamento(Boolean fromOrcamento) {
		this.fromOrcamento = fromOrcamento;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	public void setPlanejamento(Planejamento planejamento) {
		this.planejamento = planejamento;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
}
