package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CartaocreditoFiltro extends FiltroListagemSined{

	protected Empresa empresa;
	protected String nome;
	protected String numero;
	protected Boolean ativo= new Boolean(true);
	
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Descri��o")
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@DisplayName("N�mero")
	@MaxLength(25)
	public String getNumero() {
		return numero;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
