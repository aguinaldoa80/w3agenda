package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DepartamentoFiltro extends FiltroListagemSined {
	protected String nome;
	protected Cargo cargo;
	protected String codigofolha;
	
	@DisplayName("Nome")
	@MaxLength(20)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	@DisplayName("C�digo folha")
	@MaxLength(9)
	public String getCodigofolha() {
		return codigofolha;
	}
	
	public void setCodigofolha(String codigofolha) {
		this.codigofolha = codigofolha;
	}
	
	
}
