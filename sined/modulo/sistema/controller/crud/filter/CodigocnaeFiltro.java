package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class CodigocnaeFiltro extends FiltroListagemSined {
	
	private String cnae;
	private String descricaocnae;
	
	@DisplayName("C�digo")
	public String getCnae() {
		return cnae;
	}
	@DisplayName("Descri��o")
	public String getDescricaocnae() {
		return descricaocnae;
	}
	public void setCnae(String cnae) {
		this.cnae = cnae;
	}
	public void setDescricaocnae(String descricaocnae) {
		this.descricaocnae = descricaocnae;
	}
	
}
