package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BancoFiltro extends FiltroListagemSined {
	protected String nome;
	protected Integer numero;
	protected Boolean mostrar;

	public BancoFiltro() {
	}

	@DisplayName("Nome")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	@DisplayName("N�mero")
	@MaxLength(3)
	public Integer getNumero() {
		return numero;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	@DisplayName("Situa��o")
	public Boolean getMostrar() {
		return mostrar;
	}

	public void setMostrar(Boolean mostrar) {
		this.mostrar = mostrar;
	}

	

}
