package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.geral.bean.enumeration.Notaerrocorrecaotipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class NotaErroCorrecaoFiltro extends FiltroListagemSined{

	private Notaerrocorrecaotipo tipo;
	private String prefixo;
	private String codigo;
	private String mensagem;
	private String correcao;
	
	public Notaerrocorrecaotipo getTipo() {
		return tipo;
	}
	public void setTipo(Notaerrocorrecaotipo tipo) {
		this.tipo = tipo;
	}
	public String getPrefixo() {
		return prefixo;
	}
	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getCorrecao() {
		return correcao;
	}
	public void setCorrecao(String correcao) {
		this.correcao = correcao;
	}
}
