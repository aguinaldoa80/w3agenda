package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContratotipoFiltro extends FiltroListagemSined{

	protected String nome;
	protected Frequencia frequencia;
	
	@MaxLength(30)
	public String getNome() {
		return nome;
	}
	
	@DisplayName("Periodicidade")
	public Frequencia getFrequencia() {
		return frequencia;
	}


	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
