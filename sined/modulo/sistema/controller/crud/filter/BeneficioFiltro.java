package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Beneficiotipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BeneficioFiltro extends FiltroListagemSined {
	
	private String nome;
	private Beneficiotipo beneficiotipo;
	private Boolean ativo;
	
	@MaxLength(100)
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	@DisplayName("Tipo")
	public Beneficiotipo getBeneficiotipo() {
		return beneficiotipo;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setBeneficiotipo(Beneficiotipo beneficiotipo) {
		this.beneficiotipo = beneficiotipo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}	
}