package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class IndicecorrecaoFiltro extends FiltroListagemSined {
	
	protected String sigla;
	protected String descricao;
	
	@MaxLength(10)
	public String getSigla() {
		return sigla;
	}
	@DisplayName("Descri��o")
	@MaxLength(80)
	public String getDescricao() {
		return descricao;
	}
	
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
