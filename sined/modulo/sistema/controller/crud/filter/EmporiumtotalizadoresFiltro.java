package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Emporiumpdv;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Tributacaoecf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EmporiumtotalizadoresFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Emporiumpdv emporiumpdv;
	protected Tributacaoecf tributacaoecf;
	protected Date data1;
	protected Date data2;
	
	public Date getData1() {
		return data1;
	}
	public Date getData2() {
		return data2;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("PDV")
	public Emporiumpdv getEmporiumpdv() {
		return emporiumpdv;
	}
	@DisplayName("Tributação ECF")
	public Tributacaoecf getTributacaoecf() {
		return tributacaoecf;
	}
	public void setTributacaoecf(Tributacaoecf tributacaoecf) {
		this.tributacaoecf = tributacaoecf;
	}
	public void setEmporiumpdv(Emporiumpdv emporiumpdv) {
		this.emporiumpdv = emporiumpdv;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setData1(Date data1) {
		this.data1 = data1;
	}
	public void setData2(Date data2) {
		this.data2 = data2;
	}
	
}
