package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class LocalarmazenagemFiltro extends FiltroListagemSined{

	protected String nome;
	protected Municipio municipio;
	protected Uf uf;
	protected Boolean ativo;
	
	public LocalarmazenagemFiltro(){
		if (!isNotFirstTime()) {
			this.ativo = true;
		}
	}
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@DisplayName("Município")
	public Municipio getMunicipio() {
		return municipio;
	}
	public Uf getUf() {
		return uf;
	}
	public Boolean getAtivo() {
		return ativo;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
