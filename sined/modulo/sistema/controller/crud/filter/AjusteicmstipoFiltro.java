package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.bean.enumeration.AjusteIcmsTipo;
import br.com.linkcom.sined.geral.bean.enumeration.TipoAjuste;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AjusteicmstipoFiltro extends FiltroListagemSined {

	private String descricao;
	private AjusteIcmsTipo apuracao;
	private TipoAjuste tipoajuste;
	private String codigo;
	private Uf uf;
	

	@MaxLength(200)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}

	@DisplayName("Apura��o")
	public AjusteIcmsTipo getApuracao() {
		return apuracao;
	}

	@DisplayName("Tipo de ajuste")
	public TipoAjuste getTipoajuste() {
		return tipoajuste;
	}

	@MaxLength(4)
	@DisplayName("C�digo")
	public String getCodigo() {
		return codigo;
	}

	@DisplayName("UF correspondente")
	public Uf getUf() {
		return uf;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setApuracao(AjusteIcmsTipo apuracao) {
		this.apuracao = apuracao;
	}

	public void setTipoajuste(TipoAjuste tipoajuste) {
		this.tipoajuste = tipoajuste;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}
	
}
