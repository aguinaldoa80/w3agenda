package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PneuSegmentoFiltro extends FiltroListagemSined{
	protected String nome;
	protected Boolean ativo;
	protected Boolean otr;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getOtr() {
		return otr;
	}

	public void setOtr(Boolean otr) {
		this.otr = otr;
	}
	
	
}
