package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.enumeration.Tipometacomercial;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MetacomercialFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected Date dtinicio;
	protected Date dtfim;
	protected Tipometacomercial tipometacomercial;
	protected Cargo cargo;
	protected Colaborador colaborador;
	
	protected String dtinicioAux;
	protected String dtfimAux;
	
	public String getNome() {
		return nome;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	@DisplayName("Tipo de Meta Comercial")
	public Tipometacomercial getTipometacomercial() {
		return tipometacomercial;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public String getDtinicioAux() {
		dtinicioAux = "";
		if(this.dtinicio != null)
			dtinicioAux = new SimpleDateFormat("MM/yyyy").format(this.dtinicio);
		return dtinicioAux;
	}
	public String getDtfimAux() {
		dtfimAux = "";
		if(this.dtfim != null)
			dtfimAux = new SimpleDateFormat("MM/yyyy").format(this.dtfim);
		return dtfimAux;
	}
	
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setTipometacomercial(Tipometacomercial tipometacomercial) {
		this.tipometacomercial = tipometacomercial;
	}
	public void setDtinicioAux(String dtinicioAux) {
		try {
			if(dtinicioAux!=null){
				this.dtinicio = new Date(new SimpleDateFormat("MM/yyyy").parse(dtinicioAux).getTime());	
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
	public void setDtfimAux(String dtfimAux) {
		try {
			if(dtfimAux != null){
				this.dtfim = new Date(new SimpleDateFormat("MM/yyyy").parse(dtfimAux).getTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		};
	}
}
