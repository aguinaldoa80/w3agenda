package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TabelavalorFiltro extends FiltroListagemSined{

	protected String identificador;
	protected String descricao;
	
	@MaxLength(20)
	public String getIdentificador() {
		return identificador;
	}
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
