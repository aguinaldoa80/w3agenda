package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PaisFiltro extends FiltroListagemSined {
	protected Integer cdpais;
	protected String nome;
	
	@DisplayName("Identificador")
	public Integer getCdpais() {
		return cdpais;
	}
	public void setCdpais(Integer cdpais) {
		this.cdpais = cdpais;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
