package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GarantiatipoFiltro extends FiltroListagemSined{
	
	private String descricao;
	private Boolean ativo;
	private Boolean geragarantia;
	
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@DisplayName("Ativo")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@DisplayName("Gera garantia?")
	public Boolean getGeragarantia() {
		return geragarantia;
	}
	public void setGeragarantia(Boolean geragarantia) {
		this.geragarantia = geragarantia;
	}
}
