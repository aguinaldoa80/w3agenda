package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class UfFiltro extends FiltroListagemSined {

	protected String nome;
	protected String sigla;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
