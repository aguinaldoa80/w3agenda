package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Centrocusto;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.enumeration.ModelodocumentoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class DocumentoreferenciadoFiltro extends FiltroListagemSined {
	
	private String nome;
	private Empresa empresa;
	private ModelodocumentoEnum modelodocumento;
	private Fornecedor fornecedor;
	private Contagerencial contagerencial;
	private Centrocusto centrocusto;
	private Projeto projeto;
	
	public String getNome() {
		return nome;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Modelo do documento")
	public ModelodocumentoEnum getModelodocumento() {
		return modelodocumento;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Conta gerencial")
	public Contagerencial getContagerencial() {
		return contagerencial;
	}
	@DisplayName("Centro de custo")
	public Centrocusto getCentrocusto() {
		return centrocusto;
	}
	@DisplayName("Projeto")
	public Projeto getProjeto() {
		return projeto;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setModelodocumento(ModelodocumentoEnum modelodocumento) {
		this.modelodocumento = modelodocumento;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setContagerencial(Contagerencial contagerencial) {
		this.contagerencial = contagerencial;
	}
	public void setCentrocusto(Centrocusto centrocusto) {
		this.centrocusto = centrocusto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
}
