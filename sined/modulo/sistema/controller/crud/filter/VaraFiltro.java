package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Orgaojuridico;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class VaraFiltro extends FiltroListagemSined {
	
	protected String nome;
	protected Municipio municipio;
	protected Uf uf;
	protected Orgaojuridico orgaojuridico;

	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@DisplayName("Munic�pio")
	public Municipio getMunicipio() {
		return municipio;
	}
	public Uf getUf() {
		return uf;
	}
	@DisplayName("Org�o")
	public Orgaojuridico getOrgaojuridico() {
		return orgaojuridico;
	}
	public void setOrgaojuridico(Orgaojuridico orgaojuridico) {
		this.orgaojuridico = orgaojuridico;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setUf(Uf uf) {
		this.uf = uf;
	}
}
