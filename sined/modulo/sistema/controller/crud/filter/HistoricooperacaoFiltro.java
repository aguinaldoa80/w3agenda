package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.enumeration.Tipohistoricooperacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class HistoricooperacaoFiltro extends FiltroListagemSined{

	private Tipohistoricooperacao tipohistoricooperacao;
	
	@DisplayName("Tipo de lanšamento")
	public Tipohistoricooperacao getTipohistoricooperacao() {
		return tipohistoricooperacao;
	}
	public void setTipohistoricooperacao(Tipohistoricooperacao tipohistoricooperacao) {
		this.tipohistoricooperacao = tipohistoricooperacao;
	}
}
