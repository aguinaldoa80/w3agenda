package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Inspecaoitemtipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class InspecaoitemFiltro extends FiltroListagemSined {

	protected String nome;
	protected Inspecaoitemtipo inspecaoitemtipo;
	protected Boolean ativo = Boolean.TRUE;
	
	@MaxLength(150)
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	@DisplayName("Tipo de item de inspe��o")
	public Inspecaoitemtipo getInspecaoitemtipo() {
		return inspecaoitemtipo;
	}
	public void setInspecaoitemtipo(Inspecaoitemtipo inspecaoitemtipo) {
		this.inspecaoitemtipo = inspecaoitemtipo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}
