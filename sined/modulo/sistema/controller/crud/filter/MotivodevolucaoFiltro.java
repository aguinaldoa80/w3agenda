package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MotivodevolucaoFiltro extends FiltroListagemSined {
	
	protected String descricao;
	protected Boolean constatacao;
	protected Boolean motivodevolucao;

	@DisplayName("Descrição")
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@DisplayName("Constatação")
	public Boolean getConstatacao() {
		return constatacao;
	}
	
	public void setConstatacao(Boolean constatacao) {
		this.constatacao = constatacao;
	}
	
	@DisplayName("Motivo de devolução")
	public Boolean getMotivodevolucao() {
		return motivodevolucao;
	}
	
	public void setMotivodevolucao(Boolean motivodevolucao) {
		this.motivodevolucao = motivodevolucao;
	}
}
