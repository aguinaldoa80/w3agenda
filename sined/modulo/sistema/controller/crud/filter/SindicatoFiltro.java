package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class SindicatoFiltro extends FiltroListagemSined {

	protected String nome;
	protected Integer codigofolha;
	protected Boolean mostrar;

	@MaxLength(50)
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@MaxLength(9)
	@DisplayName("C�digo Folha")
	public Integer getCodigofolha() {
		return codigofolha;
	}

	public void setCodigofolha(Integer codigofolha) {
		this.codigofolha = codigofolha;
	}

	@DisplayName("Situa��o")
	public Boolean getMostrar() {
		return mostrar;
	}

	public void setMostrar(Boolean mostrar) {
		this.mostrar = mostrar;
	}
		
}
