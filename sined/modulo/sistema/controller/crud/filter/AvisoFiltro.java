package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Tipoaviso;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class AvisoFiltro extends FiltroListagemSined {
	
	protected String assunto;
	protected String complemento;
	protected Date dtavisode;
	protected Date dtavisoate;
	protected AvisoOrigem avisoOrigem;
	protected Tipoaviso tipoaviso;
	protected String papel;
	protected String usuario;
	protected String categoria;
	protected String cliente;
	protected Boolean lido;
	protected Motivoaviso motivoaviso;
	
	//Par�metros
	protected Boolean notificacao;
	protected Integer cdmotivoaviso;
	protected Integer cdusuario;
	
	@MaxLength(100)
	public String getAssunto() {
		return assunto;
	}
	@MaxLength(100)
	public String getComplemento() {
		return complemento;
	}
	@DisplayName("Data aviso")
	public Date getDtavisode() {
		return dtavisode;
	}
	public Date getDtavisoate() {
		return dtavisoate;
	}
	@DisplayName("Origem")
	public AvisoOrigem getAvisoOrigem() {
		return avisoOrigem;
	}
	@DisplayName("Tipo de aviso")
	public Tipoaviso getTipoaviso() {
		return tipoaviso;
	}
	@MaxLength(100)
	@DisplayName("N�vel")
	public String getPapel() {
		return papel;
	}
	@MaxLength(100)
	@DisplayName("Usu�rio")
	public String getUsuario() {
		return usuario;
	}
	@MaxLength(100)
	public String getCategoria() {
		return categoria;
	}
	@MaxLength(100)
	public String getCliente() {
		return cliente;
	}
	@DisplayName("Mostrar")
	public Boolean getLido() {
		return lido;
	}
	
	@DisplayName("Motivo do Aviso")
	public Motivoaviso getMotivoaviso() {
		return motivoaviso;
	}
	
	public void setMotivoaviso(Motivoaviso motivoaviso) {
		this.motivoaviso = motivoaviso;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setTipoaviso(Tipoaviso tipoaviso) {
		this.tipoaviso = tipoaviso;
	}
	public void setAvisoOrigem(AvisoOrigem avisoOrigem) {
		this.avisoOrigem = avisoOrigem;
	}
	public void setPapel(String papel) {
		this.papel = papel;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public void setLido(Boolean lido) {
		this.lido = lido;
	}
	public void setDtavisode(Date dtavisode) {
		this.dtavisode = dtavisode;
	}
	public void setDtavisoate(Date dtavisoate) {
		this.dtavisoate = dtavisoate;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	
	public Boolean getNotificacao() {
		return notificacao;
	}
	
	public Integer getCdmotivoaviso() {
		return cdmotivoaviso;
	}
	
	public Integer getCdusuario() {
		return cdusuario;
	}
	
	public void setNotificacao(Boolean notificacao) {
		this.notificacao = notificacao;
	}
	
	public void setCdmotivoaviso(Integer cdmotivoaviso) {
		this.cdmotivoaviso = cdmotivoaviso;
	}
	
	public void setCdusuario(Integer cdusuario) {
		this.cdusuario = cdusuario;
	}
}
