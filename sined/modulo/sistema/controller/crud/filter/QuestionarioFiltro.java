package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Atividadetipo;
import br.com.linkcom.sined.geral.bean.Tipopessoaquestionario;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class QuestionarioFiltro extends FiltroListagemSined{
	
	protected String nome;
	protected Tipopessoaquestionario tipopessoaquestionario;
	protected Atividadetipo atividadetipo;

	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Tipopessoaquestionario getTipopessoaquestionario() {
		return tipopessoaquestionario;
	}
	public void setTipopessoaquestionario(Tipopessoaquestionario tipopessoaquestionario) {
		this.tipopessoaquestionario = tipopessoaquestionario;
	}
	@DisplayName("Tipo de atividade")
	public Atividadetipo getAtividadetipo() {
		return atividadetipo;
	}
	public void setAtividadetipo(Atividadetipo atividadetipo) {
		this.atividadetipo = atividadetipo;
	}
}
