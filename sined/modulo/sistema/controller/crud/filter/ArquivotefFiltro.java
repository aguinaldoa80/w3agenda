package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivotefsituacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ArquivotefFiltro extends FiltroListagemSined {
	
	private Date dtenvio1;
	private Date dtenvio2;
	private Empresa empresa;
	
	private List<Arquivotefsituacao> listaSituacao;
	
	public ArquivotefFiltro() {
		if(listaSituacao == null){
			listaSituacao = new ArrayList<Arquivotefsituacao>();
//			listaSituacao.add(Arquivotefsituacao.GERADO);
//			listaSituacao.add(Arquivotefsituacao.PROCESSADO_COM_ERRO);
//			listaSituacao.add(Arquivotefsituacao.PROCESSADO_COM_SUCESSO);
		}
	}
	
	public List<Arquivotefsituacao> getListaSituacao() {
		return listaSituacao;
	}
	
	public Date getDtenvio1() {
		return dtenvio1;
	}

	public Date getDtenvio2() {
		return dtenvio2;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setDtenvio1(Date dtenvio1) {
		this.dtenvio1 = dtenvio1;
	}

	public void setDtenvio2(Date dtenvio2) {
		this.dtenvio2 = dtenvio2;
	}

	public void setListaSituacao(List<Arquivotefsituacao> listaSituacao) {
		this.listaSituacao = listaSituacao;
	}
	
}
