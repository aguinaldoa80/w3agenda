package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ConfiguracaonfeFiltro extends FiltroListagemSined {
	
	private Empresa empresa;
	private String descricao;
	private Tipoconfiguracaonfe tipoconfiguracaonfe;
	private Boolean ativo = Boolean.TRUE;

	@MaxLength(200)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("Tipo")
	public Tipoconfiguracaonfe getTipoconfiguracaonfe() {
		return tipoconfiguracaonfe;
	}
	
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	
	public Empresa getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public void setTipoconfiguracaonfe(Tipoconfiguracaonfe tipoconfiguracaonfe) {
		this.tipoconfiguracaonfe = tipoconfiguracaonfe;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
 	
	
}
