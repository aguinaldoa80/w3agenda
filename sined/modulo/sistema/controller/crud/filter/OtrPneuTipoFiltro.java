package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class OtrPneuTipoFiltro extends FiltroListagemSined{
	protected String nome;
	protected String codigo;
	
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@DisplayName("C�DIGO DE CLASSIFICA��O OTR")
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
