package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Operacaocontabil;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContagerencialFiltro extends FiltroListagemSined {

	protected Boolean mostraativo = Boolean.TRUE;
	protected Boolean mostramovimentacao;
	protected Tipooperacao tipooperacao;
	protected String nome;
	protected Integer nivel;
	protected Boolean ordenaidentificador;
	protected Boolean ordenanivel;
	protected String identificador;
	protected NaturezaContagerencial natureza;
	protected Operacaocontabil operacaocontabil;
	protected Integer codigoalternativo;
	
	@MaxLength(9)
	@DisplayName("C�digo Alternativo")
	public Integer getCodigoalternativo() {
		return codigoalternativo;
	}

	public void setCodigoalternativo(Integer codigoalternativo) {
		this.codigoalternativo = codigoalternativo;
	}

	@DisplayName("Mostrar ativos?")
	public Boolean getMostraativo() {
		return mostraativo;
	}
	
	@DisplayName("Opera��o Contabil")
	public Operacaocontabil getOperacaocontabil() {
		return operacaocontabil;
	}
	public void setOperacaocontabil(Operacaocontabil operacaocontabil) {
		this.operacaocontabil = operacaocontabil;
	}
	public void setMostraativo(Boolean mostraativo) {
		this.mostraativo = mostraativo;
	}

	@DisplayName("Mostrar mov.financeira?")
	public Boolean getMostramovimentacao() {
		return mostramovimentacao;
	}
	
	public NaturezaContagerencial getNatureza() {
		return natureza;
	}
	
	public void setNatureza(NaturezaContagerencial natureza) {
		this.natureza = natureza;
	}
	
	public void setMostramovimentacao(Boolean mostramovimentacao) {
		this.mostramovimentacao = mostramovimentacao;
	}
	
	@DisplayName("Opera��o")
	public Tipooperacao getTipooperacao() {
		return tipooperacao;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setTipooperacao(Tipooperacao tipooperacao) {
		this.tipooperacao = tipooperacao;
	}
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("N�vel")
	@MaxLength(2)
	public Integer getNivel() {
		return nivel;
	}
	
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	
	public Boolean getOrdenaidentificador() {
		return ordenaidentificador;
	}
	
	public void setOrdenaidentificador(Boolean ordenaidentificador) {
		this.ordenaidentificador = ordenaidentificador;
	}
	
	public Boolean getOrdenanivel() {
		return ordenanivel;
	}
	
	public void setOrdenanivel(Boolean ordenanivel) {
		this.ordenanivel = ordenanivel;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	
	
}
