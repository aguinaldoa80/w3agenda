package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ObslancamentofiscalFiltro extends FiltroListagemSined {

	private Integer codigo;
	private String descricao;

	@DisplayName("C�digo")
	public Integer getCodigo() {
		return codigo;
	}
	@MaxLength(255)
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
