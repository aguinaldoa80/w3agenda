package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaterialtabelaprecoFiltro extends FiltroListagemSined {
	
	private String nome;
	private Date dtinicio;
	private Date dtfim;
	private Cliente cliente;
	private Material material;
	private Empresa empresa;
	private Fornecedor fornecedor;
	private Materialgrupo materialgrupo;
	private Boolean limitarAcessoClienteVendedor;
	
	@MaxLength(30)
	public String getNome() {
		return nome;
	}
	public Date getDtinicio() {
		return dtinicio;
	}
	public Date getDtfim() {
		return dtfim;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public Material getMaterial() {
		return material;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}		
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Boolean getLimitarAcessoClienteVendedor() {
		return limitarAcessoClienteVendedor;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}
	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setLimitarAcessoClienteVendedor(
			Boolean limitarAcessoClienteVendedor) {
		this.limitarAcessoClienteVendedor = limitarAcessoClienteVendedor;
	}
	
}
