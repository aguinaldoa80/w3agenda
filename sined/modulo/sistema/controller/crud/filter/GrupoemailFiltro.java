package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GrupoemailFiltro extends FiltroListagemSined{

	protected String nome;
	protected Boolean ativo;
	
	{
		this.ativo = Boolean.TRUE;
	}
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}
