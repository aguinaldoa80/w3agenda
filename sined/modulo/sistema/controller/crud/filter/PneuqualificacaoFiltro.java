package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PneuqualificacaoFiltro extends FiltroListagemSined {
	
	private String nome;
	private Boolean garantia;
	private Boolean ativo;
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	public Boolean getGarantia() {
		return garantia;
	}
	public Boolean getAtivo() {
		return ativo;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setGarantia(Boolean garantia) {
		this.garantia = garantia;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
}