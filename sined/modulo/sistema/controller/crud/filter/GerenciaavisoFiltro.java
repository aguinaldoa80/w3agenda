package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import java.sql.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GerenciaavisoFiltro extends FiltroListagemSined {
	
	protected String assunto;
	protected String complemento;
	protected Date dtavisode;
	protected Date dtavisoate;
	protected String lidos;
	protected Usuario usuario;
	
	@MaxLength(100)
	public String getAssunto() {
		return assunto;
	}
	@MaxLength(100)
	public String getComplemento() {
		return complemento;
	}
	@DisplayName("Data aviso")
	public Date getDtavisode() {
		return dtavisode;
	}
	public Date getDtavisoate() {
		return dtavisoate;
	}
	public String getLidos() {
		return lidos;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public void setLidos(String lidos) {
		this.lidos = lidos;
	}
	public void setDtavisode(Date dtavisode) {
		this.dtavisode = dtavisode;
	}
	public void setDtavisoate(Date dtavisoate) {
		this.dtavisoate = dtavisoate;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
