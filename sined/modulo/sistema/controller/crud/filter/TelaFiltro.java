package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class TelaFiltro extends FiltroListagemSined {
	
	protected String modulo;
	protected String descricao;
	
	@DisplayName("Nome")
	public String getDescricao() {
		return descricao;
	}
	
	@DisplayName("M�dulo")
	public String getModulo() {
		return modulo;
	}
	
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
