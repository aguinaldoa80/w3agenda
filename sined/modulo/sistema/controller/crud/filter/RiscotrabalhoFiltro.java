package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Riscotrabalhotipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class RiscotrabalhoFiltro extends FiltroListagemSined {
	
 	private String nome;
 	private Riscotrabalhotipo riscotrabalhotipo;
 	
 	@MaxLength(100)
 	public String getNome() {
		return nome;
	}
 	
 	@DisplayName("Tipo")
 	public Riscotrabalhotipo getRiscotrabalhotipo() {
		return riscotrabalhotipo;
	}
 	
 	public void setNome(String nome) {
		this.nome = nome;
	}
 	
 	public void setRiscotrabalhotipo(Riscotrabalhotipo riscotrabalhotipo) {
		this.riscotrabalhotipo = riscotrabalhotipo;
	}
 	
	
}
