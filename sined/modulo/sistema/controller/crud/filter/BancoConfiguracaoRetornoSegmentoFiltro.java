package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoTipoSegmentoEnum;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class BancoConfiguracaoRetornoSegmentoFiltro extends FiltroListagemSined{

	private String identificador;
	private String nome;
	private BancoConfiguracaoTipoSegmentoEnum tipoSegmento;
	private Banco banco;
	private String selecteditens;

	public String getNome() {
		return nome;
	}
	public String getIdentificador() {
		return identificador;
	}
	@DisplayName("Tipo de segmento")
	public BancoConfiguracaoTipoSegmentoEnum getTipoSegmento() {
		return tipoSegmento;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdbanco")
	public Banco getBanco() {
		return banco;
	}
	public String getSelecteditens() {
		return selecteditens;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public void setTipoSegmento(BancoConfiguracaoTipoSegmentoEnum tipoSegmento) {
		this.tipoSegmento = tipoSegmento;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setSelecteditens(String selecteditens) {
		this.selecteditens = selecteditens;
	}
}
