package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class ContacorrenteFiltro extends FiltroListagemSined {

	protected Empresa empresa;
	protected Banco banco;
	protected String agencia;
	protected String numero;
	protected Boolean ativo= Boolean.TRUE;
	protected Boolean desconsiderarsaldoinicialfluxocaixa;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public Banco getBanco() {
		return banco;
	}
	@DisplayName("Ag�ncia")
	@MaxLength(5)
	public String getAgencia() {
		return agencia;
	}
	@DisplayName("N�mero")
	@MaxLength(25)
	public String getNumero() {
		return numero;
	}
	@DisplayName("Mostrar")
	public Boolean getAtivo() {
		return ativo;
	}
	@DisplayName("Desconsiderar saldo inicial no fluxo de caixa")
	public Boolean getDesconsiderarsaldoinicialfluxocaixa() {
		return desconsiderarsaldoinicialfluxocaixa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setDesconsiderarsaldoinicialfluxocaixa(
			Boolean desconsiderarsaldoinicialfluxocaixa) {
		this.desconsiderarsaldoinicialfluxocaixa = desconsiderarsaldoinicialfluxocaixa;
	}
	
}
