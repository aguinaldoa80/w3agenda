package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PropostacaixaFiltro extends FiltroListagemSined{
	protected String nome;
	protected Integer numero;
	protected String sufixo;
	protected Cliente cliente;
	
	@DisplayName("Descri��o")
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	
	@MaxLength(9)
	public Integer getNumero() {
		return numero;
	}

	@MaxLength(2)
	public String getSufixo() {
		return sufixo;
	}

	public Cliente getCliente() {
		return cliente;
	}


	public void setNumero(Integer numero) {
		this.numero = numero;
	}


	public void setSufixo(String sufixo) {
		this.sufixo = sufixo;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
}
