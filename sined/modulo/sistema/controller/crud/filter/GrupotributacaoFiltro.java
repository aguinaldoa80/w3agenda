package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Contribuinteicmstipo;
import br.com.linkcom.sined.geral.bean.enumeration.Localdestinonfe;
import br.com.linkcom.sined.geral.bean.enumeration.Operacao;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GrupotributacaoFiltro extends FiltroListagemSined {
	
	private String nome;
	private Operacao operacao;
	private Cfop cfopgrupo;
	protected Boolean tributadoicms;
	protected Boolean tributadoissqn;
	protected Boolean ativo = true;
	protected Naturezaoperacao naturezaoperacao;
	protected Materialgrupo materialgrupo;
	protected Empresa empresa;
	protected Localdestinonfe localDestinoNfe;
	protected Contribuinteicmstipo contribuinteIcmsTipo;

	public String getNome() {
		return nome;
	}
	@DisplayName("Opera��o")
	public Operacao getOperacao() {
		return operacao;
	}
	@DisplayName("Cfop")
	public Cfop getCfopgrupo() {
		return cfopgrupo;
	}
	@DisplayName("Produto tributado pelo:")
	public Boolean getTributadoicms() {
		return tributadoicms;
	}
	@DisplayName("Produto tributado pelo:")
	public Boolean getTributadoissqn() {
		return tributadoissqn;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}
	public void setCfopgrupo(Cfop cfopgrupo) {
		this.cfopgrupo = cfopgrupo;
	}
	public void setTributadoicms(Boolean tributadoicms) {
		this.tributadoicms = tributadoicms;
	}
	public void setTributadoissqn(Boolean tributadoissqn) {
		this.tributadoissqn = tributadoissqn;
	}
	@DisplayName("Natureza de Opera��o")
	public Naturezaoperacao getNaturezaoperacao() {
		return naturezaoperacao;
	}
	@DisplayName("Grupo de Material")
	public Materialgrupo getMaterialgrupo() {
		return materialgrupo;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setNaturezaoperacao(Naturezaoperacao naturezaoperacao) {
		this.naturezaoperacao = naturezaoperacao;
	}
	public void setMaterialgrupo(Materialgrupo materialgrupo) {
		this.materialgrupo = materialgrupo;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	@DisplayName("Tipo de opera��o")
	public Localdestinonfe getLocalDestinoNfe() {
		return localDestinoNfe;
	}
	public void setLocalDestinoNfe(Localdestinonfe localDestinoNfe) {
		this.localDestinoNfe = localDestinoNfe;
	}
	
	@DisplayName("Contribuinte ICMS")
	public Contribuinteicmstipo getContribuinteIcmsTipo() {
		return contribuinteIcmsTipo;
	}
	public void setContribuinteIcmsTipo(
			Contribuinteicmstipo contribuinteIcmsTipo) {
		this.contribuinteIcmsTipo = contribuinteIcmsTipo;
	}
}
