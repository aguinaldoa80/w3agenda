package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PneumodeloFiltro extends FiltroListagemSined {
	
	private Pneumarca pneumarca;
	private Boolean otr;

	@DisplayName("Marca")
	public Pneumarca getPneumarca() {
		return pneumarca;
	}

	public void setPneumarca(Pneumarca pneumarca) {
		this.pneumarca = pneumarca;
	}

	public Boolean getOtr() {
		return otr;
	}

	public void setOtr(Boolean otr) {
		this.otr = otr;
	}
	
	
	
}
