package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MaterialgrupoFiltro extends FiltroListagemSined{

	protected String nome;
	protected Boolean ativo = Boolean.TRUE;
	protected Boolean gerarBlocoH;
	protected Boolean gerarBlocok;
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getGerarBlocoH() {
		return gerarBlocoH;
	}
	public void setGerarBlocoH(Boolean gerarBlocoH) {
		this.gerarBlocoH = gerarBlocoH;
	}
	public Boolean getGerarBlocok() {
		return gerarBlocok;
	}
	public void setGerarBlocok(Boolean gerarBlocok) {
		this.gerarBlocok = gerarBlocok;
	}
	
	
}
