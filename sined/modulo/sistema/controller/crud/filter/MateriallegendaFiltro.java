package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.sined.geral.bean.Materialtipo;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class MateriallegendaFiltro extends FiltroListagemSined {

	private String legenda;
	private Materialtipo materialtipo;
	
	@MaxLength(5)
	public String getLegenda() {
		return legenda;
	}
	@DisplayName("Tipo de material")
	public Materialtipo getMaterialtipo() {
		return materialtipo;
	}
	public void setLegenda(String legenda) {
		this.legenda = legenda;
	}
	public void setMaterialtipo(Materialtipo materialtipo) {
		this.materialtipo = materialtipo;
	}
	
}
