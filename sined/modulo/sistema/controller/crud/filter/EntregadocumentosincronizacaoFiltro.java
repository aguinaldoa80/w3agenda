package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class EntregadocumentosincronizacaoFiltro extends FiltroListagemSined{

	protected String cdentrega;
	protected Fornecedor fornecedor;
	protected Boolean sincronizado;
	
	@DisplayName("C�digo do Recebimento")
	public String getCdentrega() {
		return cdentrega;
	}
	@DisplayName("Fornecedor")
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	@DisplayName("Mostrar")
	public Boolean getSincronizado() {
		return sincronizado;
	}
	public void setCdentrega(String cdentrega) {
		this.cdentrega = cdentrega;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}
	
	public String getWhereInCdEntrega(){
		if(cdentrega != null && !"".equals(cdentrega)){
			StringBuilder s = new StringBuilder();
			String cds[] = cdentrega.split(",");
			Integer aux = 0;
			if(cds != null && cds.length > 0){
				for(int i = 0; i < cds.length; i++){
					try {
						aux = Integer.parseInt(cds[i]);
						if(!"".equals(s.toString()))
							s.append(",");
						s.append(aux);
					} catch (NumberFormatException e) {}
				}
				if(s != null && !"".equals(s.toString()))
					return s.toString();
			}
		}
		
		return null;
	}
}
