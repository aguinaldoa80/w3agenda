package br.com.linkcom.sined.modulo.sistema.controller.crud.filter;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class PedidovendasincronizacaoFiltro extends FiltroListagemSined{

	protected String cdpedidovenda;
	protected Cliente cliente;
	protected Empresa empresa;
	protected Boolean sincronizado;
	
	@DisplayName("C�digo do Pedido de Venda")
	public String getCdpedidovenda() {
		return cdpedidovenda;
	}
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@DisplayName("Empresa")
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Mostrar")
	public Boolean getSincronizado() {
		return sincronizado;
	}
	public void setCdpedidovenda(String cdpedidovenda) {
		this.cdpedidovenda = cdpedidovenda;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setSincronizado(Boolean sincronizado) {
		this.sincronizado = sincronizado;
	}
	
	public String getWhereInCdPedidoVenda(){
		if(cdpedidovenda != null && !"".equals(cdpedidovenda)){
			StringBuilder s = new StringBuilder();
			String cds[] = cdpedidovenda.split(",");
			Integer aux = 0;
			if(cds != null && cds.length > 0){
				for(int i = 0; i < cds.length; i++){
					try {
						aux = Integer.parseInt(cds[i]);
						if(!"".equals(s.toString()))
							s.append(",");
						s.append(aux);
					} catch (NumberFormatException e) {}
				}
				if(s != null && !"".equals(s.toString()))
					return s.toString();
			}
		}
		
		return null;
	}
}
