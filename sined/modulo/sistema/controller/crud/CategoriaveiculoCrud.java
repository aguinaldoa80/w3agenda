package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Categoriaiteminspecao;
import br.com.linkcom.sined.geral.bean.Categoriaveiculo;
import br.com.linkcom.sined.geral.service.CategoriaveiculoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CategoriaveiculoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Categoriaveiculo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class CategoriaveiculoCrud extends CrudControllerSined<CategoriaveiculoFiltro, Categoriaveiculo, Categoriaveiculo>{
	
	private CategoriaveiculoService categoriaveiculoService;
	
	public void setCategoriaveiculoService(
			CategoriaveiculoService categoriaveiculoService) {
		this.categoriaveiculoService = categoriaveiculoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Categoriaveiculo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CATEGORIAVEICULO_NOME")) {
				throw new SinedException("Categoria j� cadastrada no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Categoriaveiculo bean) throws Exception {
		try {
			super.excluir(request, bean);
		} catch (SinedException e) {
			throw new SinedException("Categoria do ve�culo n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected Categoriaveiculo criar(WebRequestContext request, Categoriaveiculo form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdcategoriaveiculo() != null){
			form = categoriaveiculoService.loadForEntrada(form);
			
			form.setCdcategoriaveiculo(null);
			form.setDtaltera(null);
			form.setCdusuarioaltera(null);
			
			if(form.getListaCategoriaiteminspecao() != null){
				for (Categoriaiteminspecao categoriaiteminspecao : form.getListaCategoriaiteminspecao()) {
					categoriaiteminspecao.setCdcategoriaiteminspecao(null);
				}
			}
			
			return form;
		} else {
			return super.criar(request, form);
		}
	}
	
}
