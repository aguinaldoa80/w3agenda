package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pedidovendasincronizacao;
import br.com.linkcom.sined.geral.service.PedidovendasincronizacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PedidovendasincronizacaoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Pedidovendasincronizacao", authorizationModule=CrudAuthorizationModule.class)
public class PedidovendasincronizacaoCrud extends CrudControllerSined<PedidovendasincronizacaoFiltro, Pedidovendasincronizacao, Pedidovendasincronizacao> {
	
	private PedidovendasincronizacaoService pedidovendasincronizacaoService;
	
	public void setPedidovendasincronizacaoService(PedidovendasincronizacaoService pedidovendasincronizacaoService) {
		this.pedidovendasincronizacaoService = pedidovendasincronizacaoService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}

	@Override
	public ModelAndView doCriar(WebRequestContext request, Pedidovendasincronizacao form) throws CrudException {
		throw new RuntimeException("N�o � permitido executar esta a��o.");
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request,	Pedidovendasincronizacao form) throws CrudException {
		String acao = request.getParameter("ACAO");
		if(acao == null || !"consultar".equals(acao)){
			throw new RuntimeException("N�o � permitido executar esta a��o.");
		}
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Pedidovendasincronizacao form) throws CrudException {
		throw new RuntimeException("N�o � permitido executar esta a��o.");
	}
	
	public ModelAndView alterarcontador(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn != null && !"".equals(whereIn)){
			pedidovendasincronizacaoService.updateValorcontadorenvio(0, whereIn);
			request.addMessage("Os pedidos de venda ser�o sincronizados pelo sistema.");
			return new ModelAndView("redirect:/sistema/crud/Pedidovendasincronizacao");
		}else {
			request.addError("Nenhum item selecionado.");
			return null;
		}
	}
	
}
