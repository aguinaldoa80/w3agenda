package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Controleorcamento;
import br.com.linkcom.sined.geral.bean.Controleorcamentoitem;
import br.com.linkcom.sined.geral.bean.enumeration.Tipolancamento;
import br.com.linkcom.sined.geral.service.ControleorcamentoService;
import br.com.linkcom.sined.geral.service.ControleorcamentoitemService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ControleorcamentoitemFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/sistema/crud/Controleorcamentoitem", authorizationModule=CrudAuthorizationModule.class)
public class ControleorcamentoitemCrud extends CrudControllerSined<ControleorcamentoitemFiltro, Controleorcamentoitem, Controleorcamentoitem>{
	
	private ControleorcamentoitemService controleOrcamentoItemService;
	private ControleorcamentoService controleorcamentoService;
	private ProjetoService projetoService;
	
	public void setControleOrcamentoItemService(ControleorcamentoitemService controleOrcamentoItemService) {
		this.controleOrcamentoItemService = controleOrcamentoItemService;
	}
	
	public void setControleorcamentoService(ControleorcamentoService controleorcamentoService) {
		this.controleorcamentoService = controleorcamentoService;
	}
	
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Controleorcamentoitem form) throws Exception {
		if(form.getControleorcamento() != null && form.getCdcontroleorcamentoitem() == null){
			form.setControleorcamento(controleorcamentoService.findOrcamentoById(form.getControleorcamento()));
		}	
		request.setAttribute("listaProjetos", projetoService.findProjetosAtivos(form.getControleorcamento() != null && form.getControleorcamento().getProjeto() != null ? form.getControleorcamento().getProjeto() : null, false));
		
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(Controleorcamentoitem bean, BindException errors) {
		
		if (Tipolancamento.MENSAL.equals(bean.getControleorcamento().getTipolancamento())){
			Boolean verificaContaGerencialMensal = controleOrcamentoItemService.verificaContaGerencialMensal(bean);
			if(verificaContaGerencialMensal){
				errors.reject("001","J� existe essa Conta Gerencial cadastrada para esse m�s no or�amento.");
			}
		}
		
		if (Tipolancamento.ANUAL.equals(bean.getControleorcamento().getTipolancamento())){
			Boolean verificaContaGerencialAnual = controleOrcamentoItemService.verificaContaGerencialAnual(bean);
			if(verificaContaGerencialAnual){
				errors.reject("002","J� existe essa Conta Gerencial no or�amento.");
			}
		}
		
			
		if(bean.getControleorcamento() != null){
			Controleorcamento beanOrcamento = controleorcamentoService.loadOrcamentoForDateValidation(bean.getControleorcamento());
			
			if( beanOrcamento != null && bean.getMesano() != null){	
				if (bean.getMesano().after(beanOrcamento.getDtExercicioFim())) {
					errors.reject("003","O campo m�s/ano deve ser menor que a data fim do or�amento.");	
				} else if (beanOrcamento.getDtExercicioInicio().after(bean.getMesano())) {
					errors.reject("004","O campo m�s/ano deve ser maior que a data de in�cio do or�amento.");
				}
			}
		}
				
		super.validateBean(bean, errors);
	}
	
}

