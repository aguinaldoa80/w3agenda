package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Motivocancelamento;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path="/sistema/crud/Motivocancelamento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao"})
public class MotivocancelamentoCrud extends CrudControllerSined<FiltroListagemSined, Motivocancelamento, Motivocancelamento> {
	
}
