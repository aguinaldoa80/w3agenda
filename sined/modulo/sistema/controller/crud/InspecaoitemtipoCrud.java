package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Inspecaoitemtipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.InspecaoitemtipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path= {"/sistema/crud/Inspecaoitemtipo"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class InspecaoitemtipoCrud extends CrudControllerSined<InspecaoitemtipoFiltro, Inspecaoitemtipo, Inspecaoitemtipo>{
	
	@Override
	protected void salvar(WebRequestContext request, Inspecaoitemtipo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_INSPECAOITEMTIPO_NOME")) {
				throw new SinedException("Tipo de item j� cadastrado no sistema.");
			}
		}
	}
}
