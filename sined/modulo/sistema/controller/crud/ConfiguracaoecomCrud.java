package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.Date;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Configuracaoecom;
import br.com.linkcom.sined.geral.service.ConfiguracaoecomService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.job.IntegracaoEcomErroJob;
import br.com.linkcom.sined.util.job.IntegracaoEcomJob;

@CrudBean
@Controller(path="/sistema/crud/Configuracaoecom", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "url", "principal", "localarmazenagem", "materialtabelapreco", "conta"})
public class ConfiguracaoecomCrud extends CrudControllerSined<FiltroListagemSined, Configuracaoecom, Configuracaoecom> {
	
	private EmpresaService empresaService;
	private ConfiguracaoecomService configuracaoecomService;
	
	public void setConfiguracaoecomService(
			ConfiguracaoecomService configuracaoecomService) {
		this.configuracaoecomService = configuracaoecomService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Configuracaoecom form) throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}
	
	@Override
	protected void validateBean(Configuracaoecom bean, BindException errors) {
		if(configuracaoecomService.haveConfiguracaoPrincipalEmpresa(bean.getEmpresa(), bean.getCdconfiguracaoecom())){
			errors.reject("001", "J� existe configura��o de E-commerce principal para esta empresa.");
		}
	}
	
	public ModelAndView iniciarJobEcom(WebRequestContext request){
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			
			String banco = SinedUtil.getStringConexaoBanco();
			
			SimpleTrigger trigger_erro = new SimpleTrigger("trigger_integracaoecom_erro_" + banco, Scheduler.DEFAULT_GROUP, new Date(), null, SimpleTrigger.REPEAT_INDEFINITELY, 1000 * 60 * 5);
			
			JobDetail job_erro = new JobDetail("Job_Integracao_Ecom_Erro_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoEcomErroJob.class);
			
			CronTrigger cronTrigger = new CronTrigger("trigger_integracaoecom_" + banco, Scheduler.DEFAULT_GROUP, "0  0/1 * * * ?");
			cronTrigger.setStartTime(new Date());
			
			JobDetail job = new JobDetail("Job_Integracao_Ecom_" + banco, Scheduler.DEFAULT_GROUP, IntegracaoEcomJob.class);
			
			scheduler.scheduleJob(job, cronTrigger);
			scheduler.scheduleJob(job_erro, trigger_erro);
			
			scheduler.start();
		} catch (Exception e) {
			e.printStackTrace();
			request.addError(e.getMessage());
		}
		
		return sendRedirectToAction("listagem");
	}
	
}
