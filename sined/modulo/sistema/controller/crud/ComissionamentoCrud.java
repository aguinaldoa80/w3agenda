	package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.ComissionamentoFaixaMarkup;
import br.com.linkcom.sined.geral.bean.Comissionamentopessoa;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.bean.enumeration.Comissionamentotipo;
import br.com.linkcom.sined.geral.bean.enumeration.Criteriocomissionamento;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ComissionamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Comissionamento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class ComissionamentoCrud extends CrudControllerSined<ComissionamentoFiltro, Comissionamento, Comissionamento> {
	
	private ComissionamentoService comissionamentoService;
	
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {this.comissionamentoService = comissionamentoService;}

	@Override
	protected void entrada(WebRequestContext request, Comissionamento form) throws Exception {
		List<Criteriocomissionamento> listaCriteriocomissionamento = new ArrayList<Criteriocomissionamento>();
		listaCriteriocomissionamento.add(Criteriocomissionamento.SEM_CRITERIO);
		listaCriteriocomissionamento.add(Criteriocomissionamento.DIVIDIR_VENDEDOR_PRINCIPAL);
		listaCriteriocomissionamento.add(Criteriocomissionamento.IGUAL_VALOR_VENDA);
		listaCriteriocomissionamento.add(Criteriocomissionamento.ABAIXO_VALOR_VENDA);
		listaCriteriocomissionamento.add(Criteriocomissionamento.ACIMA_VALOR_VENDA);
		listaCriteriocomissionamento.add(Criteriocomissionamento.PRIMEIRA_VENDA_CLIENTE);
		listaCriteriocomissionamento.add(Criteriocomissionamento.PRIMEIRA_VENDA_GRUPO_CLIENTE);
		listaCriteriocomissionamento.add(Criteriocomissionamento.INDICADOR_VENDA);
		listaCriteriocomissionamento.add(Criteriocomissionamento.FAIXA_DESCONTO);
		listaCriteriocomissionamento.add(Criteriocomissionamento.FAIXA_MARKUP);
		request.setAttribute("listaCriteriocomissionamento", listaCriteriocomissionamento);
		
	}
	
	@Override
	protected void validateBean(Comissionamento bean, BindException errors) {
		List<Comissionamento> lista = comissionamentoService.findForComissionamentodesempenho(bean);
		if(lista != null && !lista.isEmpty()){
			if(bean.getListaComissionamentopessoa() != null && !bean.getListaComissionamentopessoa().isEmpty()){			
				Boolean erro = Boolean.FALSE;
				for(Comissionamentopessoa beancp : bean.getListaComissionamentopessoa()){
					for(Comissionamento comissionamento : lista){
						if(comissionamento.getListaComissionamentopessoa() != null && !comissionamento.getListaComissionamentopessoa().isEmpty()){
							for(Comissionamentopessoa cp : comissionamento.getListaComissionamentopessoa()){
								if(beancp.getColaborador() != null && cp.getColaborador() != null && beancp.getColaborador().equals(cp.getColaborador())){
									errors.reject("001", "O colaborador " + beancp.getColaborador().getNome() + 
											" j� recebe em outro comissionamento por desempenho (" + comissionamento.getNome() +").");
									erro = Boolean.TRUE;
									break;
								}
							}
						}
						if(erro) break;
					}
					erro = Boolean.FALSE;
				}				
			}
			//AT 42056
//			for(Comissionamento comissionamento : lista){
//				if(comissionamento.getAtivo() != null && comissionamento.getAtivo() && bean.getAtivo() != null && bean.getAtivo() && bean.getComissionamentotipo().equals(Comissionamentotipo.DESEMPENHO)){
//					errors.reject("001", "Existe um comissionamento por desempenho ativo j� cadastrado.");
//				}
//			}
		}
		
		Comissionamento comissionamento = comissionamentoService.findByCriterio(Criteriocomissionamento.FAIXA_MARKUP);
		if(bean.getCdcomissionamento() == null) {
			if(bean.getCriteriocomissionamento() != null && bean.getCriteriocomissionamento().equals(Criteriocomissionamento.FAIXA_MARKUP) && comissionamento != null) {
				errors.reject("001", "J� existe um registro de comissionamento com o crit�rio Por Faixa.");
			}
		} else {
			if(comissionamento != null && !bean.getCdcomissionamento().equals(comissionamento.getCdcomissionamento())
					&& bean.getCriteriocomissionamento() != null && bean.getCriteriocomissionamento().equals(Criteriocomissionamento.FAIXA_MARKUP)) {
				errors.reject("001", "J� existe um registro de comissionamento com o crit�rio Por Faixa.");
			}
		}
		
		if(comissionamentoService.possuiFaixasMarkupRepetidas(bean.getListaComissionamentoFaixaMarkup())) {
			errors.reject("001", "N�o � permitido o cadastro de duas faixas iguais.");
		}
			
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Comissionamento bean) throws Exception {
		try {
			bean.setNome(bean.getNome().trim());
			if(bean.getComissionamentotipo() != null && Comissionamentotipo.VENDA.equals(bean.getComissionamentotipo()) && 
					bean.getCriteriocomissionamento() == null){
				bean.setCriteriocomissionamento(Criteriocomissionamento.SEM_CRITERIO);
			}
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_COMISSIONAMENTO_NOME")) {
				throw new SinedException("Comissionamento j� cadastrado no sistema.");
			}
		}
	}
	
	/**
	* M�todo ajax que verifica a comissao com rela��o a dedu��o  
	*
	* @param request
	* @since Sep 6, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxVerificaDeducao(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");
		
		boolean sucesso = false;
		StringBuilder s = new StringBuilder("");
		
		try {
			Integer cdcomissionamento = Integer.parseInt(request.getParameter("cdcomissionamento"));
			Comissionamento c = comissionamentoService.verificaDeducao(cdcomissionamento);
			
			if(c != null){
				s.append("var deduzirvalor = '").append(c.getDeduzirvalor() != null ? c.getDeduzirvalor() : "false").append("'; ");
				sucesso = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
//			System.out.println(e);
		}
		
		if(sucesso)
			s.append("var sucesso = true;");			
		else
			s.append("var sucesso = false;");
		
		view.println(s.toString());
	}
	
}
