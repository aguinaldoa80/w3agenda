package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Questionarioresposta;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path="/sistema/crud/Questionarioresposta", authorizationModule=CrudAuthorizationModule.class)
public class QuestionariorespostaCrud extends CrudControllerSined<FiltroListagemSined, Questionarioresposta, Questionarioresposta> {
	
	public ModelAndView abrirQuestionarioresposta(WebRequestContext request){
		request.setAttribute("indexDetalhe", request.getParameter("index"));
		if(request.getParameter("cdquestionarioquestao") != null){
			request.setAttribute("cdquestionarioquestao", request.getParameter("cdquestionarioquestao"));
		}
		request.setAttribute("teste1", request.getParameter("div"));
		return new ModelAndView("direct:crud/popup/popUpquestionarioresposta");
	}
	
}