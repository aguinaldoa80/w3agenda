package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Cargoriscointensidade;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CargoriscointensidadeFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Cargoriscointensidade", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class CargoriscointensidadeCrud extends CrudControllerSined<CargoriscointensidadeFiltro, Cargoriscointensidade, Cargoriscointensidade> {
	
	@Override
	protected void salvar(WebRequestContext request, Cargoriscointensidade bean)throws Exception {
		try {
			bean.setNome(bean.getNome().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CARGORISCOINTENSIDADE_NOME")) {
				throw new SinedException("Risco intensidade j� cadastrado no sistema.");
			}
		}
		
	}

	
	
}
