package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.SelecionarCadastrarServlet;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cheque;
import br.com.linkcom.sined.geral.bean.Chequedevolucaomotivo;
import br.com.linkcom.sined.geral.bean.Chequehistorico;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Documento;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoclasse;
import br.com.linkcom.sined.geral.bean.Documentohistorico;
import br.com.linkcom.sined.geral.bean.Documentoorigem;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Movimentacao;
import br.com.linkcom.sined.geral.bean.Movimentacaoacao;
import br.com.linkcom.sined.geral.bean.Movimentacaohistorico;
import br.com.linkcom.sined.geral.bean.Movimentacaoorigem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.Chequesituacao;
import br.com.linkcom.sined.geral.service.ChequeService;
import br.com.linkcom.sined.geral.service.ChequehistoricoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.DocumentoService;
import br.com.linkcom.sined.geral.service.DocumentohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoorigemService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MovimentacaoService;
import br.com.linkcom.sined.geral.service.MovimentacaohistoricoService;
import br.com.linkcom.sined.geral.service.MovimentacaoorigemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.RateioService;
import br.com.linkcom.sined.geral.service.RateioitemService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.filter.ProtocoloRetiradaChequeFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ChequeFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Cheque", "/financeiro/crud/Cheque"}, authorizationModule=CrudAuthorizationModule.class)
public class ChequeCrud extends CrudControllerSined<ChequeFiltro, Cheque, Cheque>{

	private ChequeService chequeService;
	private ChequehistoricoService chequehistoricoService;
	private EmpresaService empresaService;
	private MovimentacaoService movimentacaoService;
	private RateioService rateioService;
	private MovimentacaohistoricoService movimentacaohistoricoService;
	private DocumentoService documentoService;
	private DocumentohistoricoService documentohistoricoService;
	private MovimentacaoorigemService movimentacaoorigemService;
	private ContaService contaService;
	private ParametrogeralService parametrogeralService;
	private DocumentoorigemService documentoorigemService;
	private RateioitemService rateioitemService;
	
	public void setDocumentohistoricoService(DocumentohistoricoService documentohistoricoService) {this.documentohistoricoService = documentohistoricoService;}
	public void setDocumentoService(DocumentoService documentoService) {this.documentoService = documentoService;}
	public void setMovimentacaohistoricoService(MovimentacaohistoricoService movimentacaohistoricoService) {this.movimentacaohistoricoService = movimentacaohistoricoService;}
	public void setRateioService(RateioService rateioService) {this.rateioService = rateioService;}
	public void setChequeService(ChequeService chequeService) {this.chequeService = chequeService;}
	public void setChequehistoricoService(ChequehistoricoService chequehistoricoService) {this.chequehistoricoService = chequehistoricoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setMovimentacaoService(MovimentacaoService movimentacaoService) {this.movimentacaoService = movimentacaoService;}
	public void setMovimentacaoorigemService(MovimentacaoorigemService movimentacaoorigemService) {this.movimentacaoorigemService = movimentacaoorigemService;}
	public void setContaService(ContaService contaService) {this.contaService = contaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	public void setDocumentoorigemService(DocumentoorigemService documentoorigemService) {this.documentoorigemService = documentoorigemService;}
	public void setRateioitemService(RateioitemService rateioitemService) {this.rateioitemService = rateioitemService;}
	
	@Override
	protected Cheque criar(WebRequestContext request, Cheque form) throws Exception {
		Cheque bean = super.criar(request, form);
		
		String dtbomparaParam = request.getParameter("dtbompara");
		if(dtbomparaParam != null){
			bean.setDtbompara(SinedDateUtils.stringToDate(dtbomparaParam));
		}
		bean.setIsPopup("true".equals(request.getParameter("isPopup")));
		bean.setChequesituacao(Chequesituacao.PREVISTO);
		
		return bean;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Cheque form) throws CrudException {
		if(form.getCdcheque() != null && chequeService.isResgatado(form) && EDITAR.equals(request.getParameter("ACAO"))){
			request.addError("N�o � poss�vel editar um cheque resgatado.");
			SinedUtil.redirecionamento(request, "/sistema/crud/Cheque?ACAO=consultar&cdcheque=" + form.getCdcheque());
			return null;
		}
		return super.doEditar(request, form);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Cheque bean) {
		if(bean.getIsPopup() != null && bean.getIsPopup()){
			NeoWeb.getRequestContext().clearMessages();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>parent.buscarCheques('" + bean.getCdcheque() + "'); parent.$.akModalRemove(true);</script>");
			return null;
		} else {
			return super.getSalvarModelAndView(request, bean);
		}
	}
	
	@Override
	public void calcularTotalRegistrosPaginacaoAJax(WebRequestContext request, ChequeFiltro filtro) {
		filtro.setWhereInChequeClienteVendaNota(chequeService.getWhereInChequeClienteVendaNota(filtro));
		super.calcularTotalRegistrosPaginacaoAJax(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, ChequeFiltro filtro) throws CrudException {
		// SE FOR SELECT ON PATH ELE S� MOSTRA OS CHEQUES QUE FORAM DEVOLVIDOS OU QUE N�O PERTENCE A UMA MOVIMENTA��O CONCILIADA 
		filtro.setChequeusados("true".equals(request.getParameter(SelecionarCadastrarServlet.INSELECTONE)));
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, ChequeFiltro filtro) throws Exception {
		List<Chequesituacao> listaSituacao = new ArrayList<Chequesituacao>();
		listaSituacao.add(Chequesituacao.PREVISTO);
		listaSituacao.add(Chequesituacao.DEVOLVIDO);
		listaSituacao.add(Chequesituacao.BAIXADO);
		listaSituacao.add(Chequesituacao.CANCELADO);		
		listaSituacao.add(Chequesituacao.SUBSTITUIDO);
		listaSituacao.add(Chequesituacao.COMPENSADO);
		request.setAttribute("listaSituacao", listaSituacao);
		request.setAttribute("isPopupTelaBaixa", "true".equalsIgnoreCase(request.getParameter("isPopupTelaBaixa")));
		
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Cheque> getLista(WebRequestContext request, ChequeFiltro filtro) {
		filtro.setWhereInChequeClienteVendaNota(chequeService.getWhereInChequeClienteVendaNota(filtro));
		ListagemResult<Cheque> listagemResult = super.getLista(request, filtro);
		
		Money valorTotal = new Money(0);
		Money totalPrevisto = new Money(0);
		Money totalDevolvido = new Money(0);
		Money totalBaixado = new Money(0);
		Money totalCancelado = new Money(0);
		Money totalSubstituido = new Money(0);
		Money totalCompensado = new Money(0);
		for (Cheque cheque: listagemResult.list()){
			if (cheque.getValor()!=null){
				valorTotal = valorTotal.add(cheque.getValor());
			}
			if(Chequesituacao.PREVISTO.equals(cheque.getChequesituacao())){
				totalPrevisto = totalPrevisto.add(cheque.getValor());
			}
			if(Chequesituacao.DEVOLVIDO.equals(cheque.getChequesituacao())){
				totalDevolvido = totalDevolvido.add(cheque.getValor());
			}
			if(Chequesituacao.BAIXADO.equals(cheque.getChequesituacao())){
				totalBaixado = totalBaixado.add(cheque.getValor());
			}
			if(Chequesituacao.CANCELADO.equals(cheque.getChequesituacao())){
				totalCancelado = totalCancelado.add(cheque.getValor());
			}
			if(Chequesituacao.SUBSTITUIDO.equals(cheque.getChequesituacao())){
				totalSubstituido = totalSubstituido.add(cheque.getValor());
			}
			if(Chequesituacao.COMPENSADO.equals(cheque.getChequesituacao())){
				totalCompensado = totalCompensado.add(cheque.getValor());
			}
		}
		chequeService.setInformacoesRecebidoPago(listagemResult.list());
		filtro.setValorTotal(valorTotal);
		filtro.setTotalPrevisto(totalPrevisto);
		filtro.setTotalDevolvido(totalDevolvido);
		filtro.setTotalBaixado(totalBaixado);
		filtro.setTotalCancelado(totalCancelado);
		filtro.setTotalSubstituido(totalSubstituido);
		filtro.setTotalCompensado(totalCompensado);
		
		return listagemResult;
	}
	
	@Override
	protected void validateBean(Cheque bean, BindException errors) {
		if(chequeService.verificarDuplicidadeCheque(bean)){
			errors.reject("001","Cheque j� lan�ado no sistema.");
		}
	}	
	
	@Override
	protected void salvar(WebRequestContext request, Cheque bean) throws Exception {
		boolean isCriar = bean.getCdcheque() == null;
		super.salvar(request, bean);
		
		if(isCriar || StringUtils.isNotEmpty(bean.getObservacao())){
			Chequehistorico chequehistorico = new Chequehistorico();
			chequehistorico.setCheque(bean);
			chequehistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			chequehistorico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
			chequehistorico.setChequesituacao(bean.getChequesituacao());
			chequehistorico.setObservacao(bean.getObservacao());
			chequehistorico.setObservacaorelatorio(bean.getObservacao());
			chequehistoricoService.saveOrUpdate(chequehistorico);
		}
	}
	
	public void ajaxIsDuplicidadeCheque(WebRequestContext request){
		Cheque bean = new Cheque();
		bean.setBanco(request.getParameter("cdcheque") != null && !"".equals(request.getParameter("cdcheque")) ? Integer.parseInt(request.getParameter("cdcheque")) : null);
		bean.setBanco(request.getParameter("banco") != null && !"".equals(request.getParameter("banco")) ? Integer.parseInt(request.getParameter("banco")) : null);
		bean.setAgencia(request.getParameter("agencia") != null && !"".equals(request.getParameter("agencia")) ? Integer.parseInt(request.getParameter("agencia")) : null);
		bean.setConta(request.getParameter("conta") != null && !"".equals(request.getParameter("conta")) ? Integer.parseInt(request.getParameter("conta")) : null);
		bean.setNumero(request.getParameter("numero") != null && !"".equals(request.getParameter("numero")) ? request.getParameter("numero") : null);
		Empresa empresa = new Empresa(request.getParameter("empresa") != null && !"".equals(request.getParameter("empresa")) ? Integer.parseInt(request.getParameter("empresa")) : null);
		bean.setEmpresa(empresa);
		
		
		if(chequeService.verificarDuplicidadeCheque(bean))
			View.getCurrent().println("var isDuplicidadeCheque = 'TRUE';");
		else
			View.getCurrent().println("var isDuplicidadeCheque = 'FALSE';");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cheque form) throws Exception {
		boolean existMotivoHistorico = false;
		if(form.getCdcheque() != null){
			form.setListaChequehistorico(chequehistoricoService.findByCheque(form));
			if(SinedUtil.isListNotEmpty(form.getListaChequehistorico())){
				for(Chequehistorico chequehistorico : form.getListaChequehistorico()){
					if(chequehistorico.getChequedevolucaomotivo() != null){
						existMotivoHistorico = true;
						break;
					}
				}
			}
		}
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("existMotivoHistorico", existMotivoHistorico);
		request.setAttribute("consultaPopup", "true".equalsIgnoreCase(request.getParameter("consultaPopup")));
		request.setAttribute("listaContabancaria", contaService.findContasAtivasComPermissao(form.getEmpresa() != null ? form.getEmpresa() : null));
	}
	
	/**
	* M�todo que abre a popup para registrar devolu��o de cheque
	*
	* @param request
	* @return
	* @since 23/07/2014
	* @author Luiz Fernando
	*/
	public ModelAndView abrirRegistrardevolucao(WebRequestContext request){
		boolean fromMovimentacao = false;
		String whereIn = null;
		String whereInMovimentacao = request.getParameter("whereInMovimentacao");
		Empresa empresaPrincipal = empresaService.loadPrincipalWithContagerecialDevolucao();
		if(!StringUtils.isBlank(whereInMovimentacao)){
			fromMovimentacao = true;
			List<Movimentacao> listaMovimentacao = movimentacaoService.findWithCheque(whereInMovimentacao);
			if(listaMovimentacao != null && listaMovimentacao.size() > 0){
				List<Cheque> listaCheque = new ArrayList<Cheque>();
				Empresa empresa;
				for (Movimentacao movimentacao : listaMovimentacao) {
					if(movimentacao != null && movimentacao.getCheque() != null){
						listaCheque.add(movimentacao.getCheque());
//						empresa  = movimentacao.getEmpresa() != null ? movimentacao.getEmpresa() : movimentacao.getCheque().getEmpresa();
						empresa  = movimentacao.getCheque().getEmpresa();
						if((empresa != null && (empresa.getContagerencialdevolucaocredito() == null ||
								empresa.getContagerencialdevolucaodebito() == null)) || 
							(empresa == null && (empresaPrincipal == null || empresaPrincipal.getContagerencialdevolucaocredito() == null ||
									empresaPrincipal.getContagerencialdevolucaodebito() == null))){
							request.addError("Os campos 'Devolu��o Cr�dito' e 'Devolu��o D�bito' do cadastro de empresa devem estar preenchidos para executar esta opera��o.");
							SinedUtil.fechaPopUp(request);
							return null;
						}
					}
				}
				if(listaCheque.size() == 0){
					request.addError("Movimenta��o selecionada n�o vinculada a nenhum cheque.");
					SinedUtil.fechaPopUp(request);
					return null;
				}
				whereIn = CollectionsUtil.listAndConcatenate(listaCheque, "cdcheque", ",");
			}
		} else {
			whereIn = SinedUtil.getItensSelecionados(request);
		}
		
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if(chequeService.isNotSituacao(whereIn, Chequesituacao.COMPENSADO)){
			request.addError("S� � poss�vel registrar devolu��o de cheque(s) compensado(s).");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		boolean isVinculoCredito = false;
		boolean isVinculoDebito = false;
		
		List<Movimentacao> listaMovByCheque = movimentacaoService.findByCheque(whereIn);
		if(SinedUtil.isListNotEmpty(listaMovByCheque)){
			String[] idsCheques = whereIn.split(",");
			for (String idString : idsCheques) {
				Integer cdcheque = Integer.parseInt(idString);
				
				// PEGA A PRIMEIRA MOVIMENTA��O DO CHEQUE
				for (int i = 0; i < listaMovByCheque.size(); i++) {
					Movimentacao movimentacao = listaMovByCheque.get(i);
					if(movimentacao.getCheque() != null && 
							movimentacao.getCheque().getCdcheque() != null &&
							movimentacao.getCheque().getCdcheque().equals(cdcheque)){
						if(movimentacao.getListaMovimentacaoorigem() != null && 
								movimentacao.getListaMovimentacaoorigem().size() > 0){
							for (Movimentacaoorigem movimentacaoorigem : movimentacao.getListaMovimentacaoorigem()) {
								if(movimentacaoorigem.getDocumento() != null && 
										movimentacaoorigem.getDocumento().getDocumentoclasse() != null &&
										movimentacaoorigem.getDocumento().getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
									isVinculoCredito = true;
									break;
								}
							}
						}
						if(!fromMovimentacao){
//							Empresa empresa = movimentacao.getEmpresa() != null ? movimentacao.getEmpresa() : movimentacao.getCheque().getEmpresa();
							Empresa empresa = movimentacao.getCheque().getEmpresa();
							if((empresa != null && (empresa.getContagerencialdevolucaocredito() == null ||
									empresa.getContagerencialdevolucaodebito() == null)) || 
								(empresa == null && (empresaPrincipal == null || empresaPrincipal.getContagerencialdevolucaocredito() == null ||
										empresaPrincipal.getContagerencialdevolucaodebito() == null))){
								request.addError("Os campos 'Devolu��o Cr�dito' e 'Devolu��o D�bito' do cadastro de empresa devem estar preenchidos para executar esta opera��o.");
								SinedUtil.fechaPopUp(request);
								return null;
							}
						}
						break;
					}
				}
				
				// PEGA A �LTIMA MOVIMENTA��O DO CHEQUE
				for (int i = listaMovByCheque.size() - 1; i >= 0; i--) {
					Movimentacao movimentacao = listaMovByCheque.get(i);
					if(movimentacao.getCheque() != null && 
							movimentacao.getCheque().getCdcheque() != null &&
							movimentacao.getCheque().getCdcheque().equals(cdcheque)){
						if(movimentacao.getListaMovimentacaoorigem() != null && 
								movimentacao.getListaMovimentacaoorigem().size() > 0){
							for (Movimentacaoorigem movimentacaoorigem : movimentacao.getListaMovimentacaoorigem()) {
								if(movimentacaoorigem.getDocumento() != null && 
										movimentacaoorigem.getDocumento().getDocumentoclasse() != null &&
										movimentacaoorigem.getDocumento().getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)){
									isVinculoDebito = true;
									break;
								}
							}
						}
						if(!fromMovimentacao){
//							Empresa empresa = movimentacao.getEmpresa() != null ? movimentacao.getEmpresa() : movimentacao.getCheque().getEmpresa();
							Empresa empresa = movimentacao.getCheque().getEmpresa();
							if((empresa != null && (empresa.getContagerencialdevolucaocredito() == null ||
									empresa.getContagerencialdevolucaodebito() == null)) || 
								(empresa == null && (empresaPrincipal == null || empresaPrincipal.getContagerencialdevolucaocredito() == null ||
										empresaPrincipal.getContagerencialdevolucaodebito() == null))){
								request.addError("Os campos 'Devolu��o Cr�dito' e 'Devolu��o D�bito' do cadastro de empresa devem estar preenchidos para executar esta opera��o.");
								SinedUtil.fechaPopUp(request);
								return null;
							}
						}
						break;
					}
				}
				
			}
		}
		
		Cheque cheque = new Cheque();
		cheque.setWhereIn(whereIn);
		
		request.setAttribute("fromMovimentacao", fromMovimentacao);
		request.setAttribute("entrada", entrada);
		request.setAttribute("isVinculoCredito", isVinculoCredito);
		request.setAttribute("isVinculoDebito", isVinculoDebito);
		return new ModelAndView("direct:/crud/popup/popUpRegistrardevolucaoCheque", "bean", cheque);
	}
	
	/**
	* M�todo que cria e salva movimenta��es/contas no processo de registrar devolu��o de cheque
	*
	* @param request
	* @param cheque
	* @since 23/07/2014
	* @author Luiz Fernando
	 * @throws UnsupportedEncodingException 
	*/
	public void saveRegistrardevolucao(WebRequestContext request, Cheque cheque) throws UnsupportedEncodingException{
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		boolean fromMovimentacao = Boolean.valueOf(request.getParameter("fromMovimentacao") != null ? request.getParameter("fromMovimentacao") : "false");
		String whereIn = cheque.getWhereIn();
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/sistema/crud/Cheque");
			return;
		}
		
		Boolean gerarcontareceber = cheque.getGerarcontareceber() != null && cheque.getGerarcontareceber();
		Boolean gerarcontapagar = cheque.getGerarcontapagar() != null && cheque.getGerarcontapagar();
		
		String urlRedirecionamento = "/sistema/crud/Cheque" + (entrada ? "?ACAO=consultar&cdcheque=" + whereIn : "");
		if(fromMovimentacao){
			urlRedirecionamento = "/financeiro/crud/Movimentacao";
		}
		
		List<Movimentacaoorigem> listaMovimentacaoorigemSalvar = new ArrayList<Movimentacaoorigem>();
		List<Movimentacao> listaMovimentacaoACriar = new ArrayList<Movimentacao>();
		List<Documento> listaDocumentoACriar = new ArrayList<Documento>();
		
		List<String> idsChequesGerarDocumento_Receber = new ArrayList<String>();
		List<String> idsChequesGerarDocumento_Pagar = new ArrayList<String>();
		
		Empresa empresaPrincipal = empresaService.loadPrincipalWithContagerecialDevolucao();
		
		String[] idsCheques = whereIn.split(",");
		for (String idString : idsCheques) {
			// BUSCA TODAS AS MOVIMENTA��ES N�O CANCELADAS DO CHEQUE
			List<Movimentacao> listaMovByCheque = movimentacaoService.findByCheque(idString);
			
			if(listaMovByCheque != null && listaMovByCheque.size() > 0){
				if(listaMovByCheque.size() == 1){ // V�NCULO COM UMA MOVIMENTA��O
					Movimentacao movimentacao = listaMovByCheque.get(0);
					
					List<Documento> listaDocumentoNovo = new ArrayList<Documento>();
					
					// CASO OS CHECKS DE GERAR CONTA RECEBER/PAGAR 
					if(gerarcontapagar || gerarcontareceber){
						Documento documentoVinculo = null;
						
						List<Movimentacaoorigem> listaMovimentacaoorigem = movimentacao.getListaMovimentacaoorigem();
						if(listaMovimentacaoorigem != null && listaMovimentacaoorigem.size() > 0){
							for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
								if(movimentacaoorigem.getDocumento() != null){
									documentoVinculo = movimentacaoorigem.getDocumento();
									break;
								}
							}
						}
						
						if(documentoVinculo != null){
							boolean documentoCriado = false;
							boolean existeVenda = false;
							if(gerarcontareceber && parametrogeralService.getBoolean(Parametrogeral.DEV_CHEQUE_GERAR_UMA_CONTA_POR_VENDA)){
								List<Documento> listaDocumentoNovoPorVenda = new ArrayList<Documento>();
								
								Double totalDocumento = 0d;
								for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
									if(movimentacaoorigem.getDocumento() != null && movimentacaoorigem.getDocumento().getAux_documento() != null && 
											movimentacaoorigem.getDocumento().getAux_documento().getValoratual() != null){
										totalDocumento += movimentacaoorigem.getDocumento().getAux_documento().getValoratual().getValue().doubleValue();
									}
								}
								
								for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
									if(movimentacaoorigem.getDocumento() != null){
										documentoVinculo = movimentacaoorigem.getDocumento();
								
										if(SinedUtil.isListNotEmpty(documentoVinculo.getListaDocumentoOrigem())){
											boolean docVendaCriado = false;
											for(Documentoorigem documentoorigem : documentoVinculo.getListaDocumentoOrigem()){
												Documento documento_novo = criaDocumentoDevolucaoCheque(documentoVinculo, movimentacao, documentoVinculo.getDocumentoclasse(), null, totalDocumento);
												
												docVendaCriado = false;
												if((documentoorigem.getPedidovenda() != null || documentoorigem.getVenda() != null)){
													existeVenda = true;
													documento_novo.setOrigemPedidovenda(documentoorigem.getPedidovenda());
													documento_novo.setOrigemVenda(documentoorigem.getVenda());
													documento_novo.setCddocumentoOrigemVenda(documentoVinculo.getCddocumento());
													
													for(Documento docPorVenda : listaDocumentoNovoPorVenda){
														if((docPorVenda.getOrigemPedidovenda() != null && docPorVenda.getOrigemPedidovenda().getCdpedidovenda() != null && documento_novo.getOrigemPedidovenda() != null && 
																docPorVenda.getOrigemPedidovenda().getCdpedidovenda().equals(documento_novo.getOrigemPedidovenda().getCdpedidovenda())) || 
																(docPorVenda.getOrigemVenda() != null && docPorVenda.getOrigemVenda().getCdvenda() != null && documento_novo.getOrigemVenda() != null && 
																		docPorVenda.getOrigemVenda().getCdvenda().equals(documento_novo.getOrigemVenda().getCdvenda()))){
															if(docPorVenda.getCddocumentoOrigemVenda() == null || !docPorVenda.getCddocumentoOrigemVenda().equals(documento_novo.getCddocumentoOrigemVenda())){
																docPorVenda.setValor(docPorVenda.getValor().add(documento_novo.getValor()));
																rateioService.atualizaValorRateio(docPorVenda.getRateio(), docPorVenda.getValor());
															}
															
															if(docPorVenda.getOrigemVenda() != null){
																String idExterno = "";
																if (StringUtils.isNotBlank(docPorVenda.getOrigemVenda().getIdentificadorexterno())) {
																	idExterno = " - Ident. Externo: " + docPorVenda.getOrigemVenda().getIdentificadorexterno();
																	if (idExterno.length() > 80) {
																		idExterno = idExterno.substring(0, 80);
																	}
																}
																docPorVenda.setDescricao("Referente a venda " + docPorVenda.getOrigemVenda().getCdvenda() + ". " + idExterno);
															}
															
															if(docPorVenda.getOrigemPedidovenda() != null){
																String idExterno = "";
																if (StringUtils.isNotBlank(docPorVenda.getOrigemPedidovenda().getIdentificacaoexterna())) {
																	idExterno = " - Ident. Externo: " + docPorVenda.getOrigemPedidovenda().getIdentificacaoexterna();
																	if (idExterno.length() > 80) {
																		idExterno = idExterno.substring(0, 80);
																	}
																}
																docPorVenda.setDescricao("Conta a receber referente ao pedido de venda " + docPorVenda.getOrigemPedidovenda().getCdpedidovenda() + ". " + idExterno);
															}
															
															docVendaCriado = true;
															break;
														}
													}
												}
												
												if(docVendaCriado){
													continue;
												}
												
												listaDocumentoNovoPorVenda.add(documento_novo);
											}
										}else {
											Documento documento_novo = criaDocumentoDevolucaoCheque(documentoVinculo, movimentacao, documentoVinculo.getDocumentoclasse(), null, totalDocumento);
											listaDocumentoNovoPorVenda.add(documento_novo);
										}
										
										documentoCriado = true;
									}
								}
								
								if(existeVenda){
									listaDocumentoNovo.addAll(listaDocumentoNovoPorVenda);
								}
							}
							
							if(!documentoCriado || !existeVenda){
								Documento documento_novo = criaDocumentoDevolucaoCheque(documentoVinculo, movimentacao, documentoVinculo.getDocumentoclasse(), null, null);
								listaDocumentoNovo.add(documento_novo);
							}
						} else {
							if(movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO) && gerarcontareceber){
								idsChequesGerarDocumento_Receber.add(idString);
							} else if(movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO) && gerarcontapagar){
								idsChequesGerarDocumento_Pagar.add(idString);
							}
						}
					}
					
					movimentacao = movimentacaoService.loadForEntrada(movimentacao);
					
					// CRIA UMA MOVIMENTA��O COM OS MESMOS DADOS DA ANTERIOR COM MUDAN�A NO TIPO DE OPERA��O
					Movimentacao movimentacao_nova = movimentacaoService.criaCopia(movimentacao);
					movimentacao_nova.setMovimentacaoacao(Movimentacaoacao.NORMAL);
					
					movimentacao_nova.setObservacaoConciliacao("Cheque <a href=\"javascript:visualizaCheque(" + idString + ")\">" + idString + "</a> devolvido.");
					movimentacao_nova.setListaDocumentoNovo(listaDocumentoNovo);
					
					// MUDAN�A NO TIPO DE OPERA��O DA NOVA MOVIMENTA��O
					if(movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_CREDITO)){ // TIPO CR�DITO
						movimentacao_nova.setTipooperacao(Tipooperacao.TIPO_DEBITO);
//						Empresa empresa = movimentacao.getEmpresa() != null ? movimentacao.getEmpresa() : movimentacao.getCheque().getEmpresa();
						Empresa empresa = movimentacao.getCheque().getEmpresa();
						if(empresa == null){
							empresa = empresaPrincipal;
						}
						if(empresa != null && movimentacao.getRateio() != null){
							rateioitemService.atualizaContagerencial(movimentacao.getRateio(), empresa.getContagerencialdevolucaodebito());
						}
					} else if(movimentacao.getTipooperacao() != null && movimentacao.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){ // TIPO D�BITO
						movimentacao_nova.setTipooperacao(Tipooperacao.TIPO_CREDITO);
						if(movimentacao.getCheque() != null && movimentacao.getCheque().getEmpresa() != null){
							preencheContadevolucaoByDevolucaocheque(movimentacao.getCheque().getEmpresa(), movimentacao_nova);
						}
//						Empresa empresa = movimentacao.getEmpresa() != null ? movimentacao.getEmpresa() : movimentacao.getCheque().getEmpresa();
						Empresa empresa = movimentacao.getCheque().getEmpresa();
						if(empresa == null){
							empresa = empresaPrincipal;
						}
						if(empresa != null && movimentacao.getRateio() != null){
							rateioitemService.atualizaContagerencial(movimentacao.getRateio(), empresa.getContagerencialdevolucaocredito());
						}
					}
					
					Movimentacaoorigem movimentacaoorigem = new Movimentacaoorigem();
					movimentacaoorigem.setMovimentacao(movimentacao_nova);
					movimentacaoorigem.setMovimentacaodevolucao(movimentacao);
					listaMovimentacaoorigemSalvar.add(movimentacaoorigem);
					
					listaMovimentacaoACriar.add(movimentacao_nova);
					
					if(Tipooperacao.TIPO_CREDITO.equals(movimentacao.getTipooperacao())){
						if(movimentacaoService.isTransferencia(movimentacao)){
							List<Movimentacao> listaMovimentacaoRelacionada = movimentacaoService.findByMovimentacaorelacionada(movimentacao);
							
							Movimentacao movimentacaorelacionada = listaMovimentacaoRelacionada.get(0);
							
							Movimentacao movimentacao_nova_credito = movimentacaoService.criaCopia(movimentacao);
							movimentacao_nova_credito.setConta(movimentacaorelacionada.getConta());
							movimentacao_nova_credito.setMovimentacaoacao(Movimentacaoacao.NORMAL);
							movimentacao_nova_credito.setObservacaoConciliacao("Cheque <a href=\"javascript:visualizaCheque(" + idString + ")\">" + idString + "</a> devolvido.");
							movimentacao_nova_credito.setTipooperacao(Tipooperacao.TIPO_CREDITO);
							if(movimentacao.getCheque() != null && movimentacao.getCheque().getEmpresa() != null){
								preencheContadevolucaoByDevolucaocheque(movimentacao.getCheque().getEmpresa(), movimentacao_nova_credito);
							}
							
							Movimentacaoorigem movimentacaoorigem_credito = new Movimentacaoorigem();
							movimentacaoorigem_credito.setMovimentacao(movimentacao_nova_credito);
							movimentacaoorigem_credito.setMovimentacaodevolucao(movimentacao);
							listaMovimentacaoorigemSalvar.add(movimentacaoorigem_credito);

							listaMovimentacaoACriar.add(movimentacao_nova_credito);
						}
					}
				} else { // V�NCULO COM MAIS DE UMA MOVIMENTA��O
					Movimentacao movimentacao_primeira = listaMovByCheque.get(0);
					Movimentacao movimentacao_ultima  = listaMovByCheque.get(listaMovByCheque.size() - 1);
					
					// VERIFICA SE A PRIMEIRA MOVIMENTA��O TEM VINCULO COM CONTA A RECEBER
					if(gerarcontareceber){
						Documento documento_primeira_receber = null;
						
						List<Movimentacaoorigem> listaMovimentacaoorigem = movimentacao_primeira.getListaMovimentacaoorigem();
						if(listaMovimentacaoorigem != null && listaMovimentacaoorigem.size() > 0){
							for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
								if(movimentacaoorigem.getDocumento() != null &&
										movimentacaoorigem.getDocumento().getDocumentoclasse() != null &&
										movimentacaoorigem.getDocumento().getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
									documento_primeira_receber = movimentacaoorigem.getDocumento();
									break;
								}
							}
						}
						
						if(documento_primeira_receber != null){
							boolean documentoCriado = false;
							boolean existeVenda = false;
							if(gerarcontareceber && parametrogeralService.getBoolean(Parametrogeral.DEV_CHEQUE_GERAR_UMA_CONTA_POR_VENDA)){
								List<Documento> listaDocumentoNovoPorVenda = new ArrayList<Documento>();
								
								Double totalDocumento = 0d;
								for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
									if(movimentacaoorigem.getDocumento() != null && movimentacaoorigem.getDocumento().getAux_documento() != null && 
											movimentacaoorigem.getDocumento().getAux_documento().getValoratual() != null){
										totalDocumento += movimentacaoorigem.getDocumento().getAux_documento().getValoratual().getValue().doubleValue();
									}
								}
								
								for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
									if(movimentacaoorigem.getDocumento() != null &&
											movimentacaoorigem.getDocumento().getDocumentoclasse() != null &&
											movimentacaoorigem.getDocumento().getDocumentoclasse().equals(Documentoclasse.OBJ_RECEBER)){
										documento_primeira_receber = movimentacaoorigem.getDocumento();
										
										if(SinedUtil.isListNotEmpty(documento_primeira_receber.getListaDocumentoOrigem())){
											boolean docVendaCriado = false;
											for(Documentoorigem documentoorigem : documento_primeira_receber.getListaDocumentoOrigem()){
												Documento documento_novo = criaDocumentoDevolucaoCheque(documento_primeira_receber, movimentacao_primeira, Documentoclasse.OBJ_RECEBER, movimentacao_primeira.getCheque(), totalDocumento);
												
												docVendaCriado = false;
												if((documentoorigem.getPedidovenda() != null || documentoorigem.getVenda() != null)){
													existeVenda = true;
													documento_novo.setOrigemPedidovenda(documentoorigem.getPedidovenda());
													documento_novo.setOrigemVenda(documentoorigem.getVenda());
													documento_novo.setCddocumentoOrigemVenda(documento_primeira_receber.getCddocumento());
													
													for(Documento docPorVenda : listaDocumentoNovoPorVenda){
														if((docPorVenda.getOrigemPedidovenda() != null && docPorVenda.getOrigemPedidovenda().getCdpedidovenda() != null && documento_novo.getOrigemPedidovenda() != null && 
																docPorVenda.getOrigemPedidovenda().getCdpedidovenda().equals(documento_novo.getOrigemPedidovenda().getCdpedidovenda())) || 
																(docPorVenda.getOrigemVenda() != null && docPorVenda.getOrigemVenda().getCdvenda() != null && documento_novo.getOrigemVenda() != null && 
																		docPorVenda.getOrigemVenda().getCdvenda().equals(documento_novo.getOrigemVenda().getCdvenda()))){
															if(docPorVenda.getCddocumentoOrigemVenda() == null || !docPorVenda.getCddocumentoOrigemVenda().equals(documento_novo.getCddocumentoOrigemVenda())){
																docPorVenda.setValor(docPorVenda.getValor().add(documento_novo.getValor()));
																rateioService.atualizaValorRateio(docPorVenda.getRateio(), docPorVenda.getValor());
															}
															
															if(docPorVenda.getOrigemVenda() != null){
																String idExterno = "";
																if (StringUtils.isNotBlank(docPorVenda.getOrigemVenda().getIdentificadorexterno())) {
																	idExterno = " - Ident. Externo: " + docPorVenda.getOrigemVenda().getIdentificadorexterno();
																	if (idExterno.length() > 80) {
																		idExterno = idExterno.substring(0, 80);
																	}
																}
																docPorVenda.setDescricao("Referente a venda " + docPorVenda.getOrigemVenda().getCdvenda() + ". " + idExterno);
															}
															
															if(docPorVenda.getOrigemPedidovenda() != null){
																String idExterno = "";
																if (StringUtils.isNotBlank(docPorVenda.getOrigemPedidovenda().getIdentificacaoexterna())) {
																	idExterno = " - Ident. Externo: " + docPorVenda.getOrigemPedidovenda().getIdentificacaoexterna();
																	if (idExterno.length() > 80) {
																		idExterno = idExterno.substring(0, 80);
																	}
																}
																docPorVenda.setDescricao("Conta a receber referente ao pedido de venda " + docPorVenda.getOrigemPedidovenda().getCdpedidovenda() + ". " + idExterno);
															}
															
															docVendaCriado = true;
															break;
														}
													}
												}
												
												if(docVendaCriado){
													continue;
												}
												
												listaDocumentoNovoPorVenda.add(documento_novo);
												documentoCriado = true;
											}
											
										}else {
											Documento documento_novo = criaDocumentoDevolucaoCheque(documento_primeira_receber, movimentacao_primeira, Documentoclasse.OBJ_RECEBER, movimentacao_primeira.getCheque(), totalDocumento);
											listaDocumentoNovoPorVenda.add(documento_novo);
										}
										
										documentoCriado = true;
									}
								}
								
								if(existeVenda){
									listaDocumentoACriar.addAll(listaDocumentoNovoPorVenda);
								}
							}
							
							if(!documentoCriado || !existeVenda){
								Documento documento_novo = criaDocumentoDevolucaoCheque(documento_primeira_receber, movimentacao_primeira, Documentoclasse.OBJ_RECEBER, movimentacao_primeira.getCheque(), null);
								listaDocumentoACriar.add(documento_novo);
							}
						}
					}
					
					
					// VERIFICA SE A �LTIMA MOVIMENTA��O TEM V�NCULO COM CONTA A PAGAR
					Documento documento_ultima_pagar = null;
					
					List<Movimentacaoorigem> listaMovimentacaoorigem = movimentacao_ultima.getListaMovimentacaoorigem();
					if(listaMovimentacaoorigem != null && listaMovimentacaoorigem.size() > 0){
						for (Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigem) {
							if(movimentacaoorigem.getDocumento() != null &&
									movimentacaoorigem.getDocumento().getDocumentoclasse() != null &&
									movimentacaoorigem.getDocumento().getDocumentoclasse().equals(Documentoclasse.OBJ_PAGAR)){
								documento_ultima_pagar = movimentacaoorigem.getDocumento();
								break;
							}
						}
					}
					
					if(documento_ultima_pagar != null){
						movimentacao_ultima = movimentacaoService.loadForEntrada(movimentacao_ultima);
						
						Movimentacao movimentacao_nova = movimentacaoService.criaCopia(movimentacao_ultima);
						
						movimentacao_nova.setObservacaoConciliacao("Cheque <a href=\"javascript:visualizaCheque(" + idString + ")\">" + idString + "</a> devolvido. ");
						movimentacao_nova.setTipooperacao(Tipooperacao.TIPO_CREDITO);
						preencheContadevolucaoByDevolucaocheque(movimentacao_ultima.getCheque().getEmpresa(), movimentacao_nova);
						
						if(gerarcontapagar){
							List<Documento> listaDocumentoNovo = new ArrayList<Documento>();
							
							Documento documento_novo = documentoService.criarCopia(documento_ultima_pagar, true);
							documento_novo.setDocumentoclasse(Documentoclasse.OBJ_PAGAR);
							documento_novo.setDocumentoacao(Documentoacao.DEFINITIVA);
							documento_novo.setDtvencimento(SinedDateUtils.currentDate());
							
							Money valor = movimentacao_ultima.getValor();
							if(movimentacao_ultima.getCheque() != null && movimentacao_ultima.getCheque().getValor() != null && movimentacao_ultima.getCheque().getValor().getValue().doubleValue() > 0){
								valor = movimentacao_ultima.getCheque().getValor();
							}
							documento_novo.setValor(valor);
							
							rateioService.atualizaValorRateio(documento_novo.getRateio(), documento_novo.getValor());
							
							listaDocumentoNovo.add(documento_novo);
							
							movimentacao_nova.setListaDocumentoNovo(listaDocumentoNovo);
						}
						
						listaMovimentacaoACriar.add(movimentacao_nova);
					}
					
					// VERIFICA SE A CONTA � DIFERENTE ENTRE A PRIMEIRA E �LTIMA MOVIMENTA��O
					if(movimentacao_primeira.getConta() != null && 
							movimentacao_ultima.getConta() != null && 
							!movimentacao_primeira.getConta().equals(movimentacao_ultima.getConta())){
						
						movimentacao_ultima = movimentacaoService.loadForEntrada(movimentacao_ultima);
						movimentacao_primeira = movimentacaoService.loadForEntrada(movimentacao_primeira);
						
						Movimentacao movimentacao_ultima_nova = movimentacaoService.criaCopia(movimentacao_ultima);
						movimentacao_ultima_nova.setMovimentacaoacao(Movimentacaoacao.NORMAL);
						movimentacao_ultima_nova.setObservacaoConciliacao("Cheque <a href=\"javascript:visualizaCheque(" + idString + ")\">" + idString + "</a> devolvido. ");
						
						if(Tipooperacao.TIPO_CREDITO.equals(movimentacao_ultima.getTipooperacao())){ // TIPO CR�DITO
							movimentacao_ultima_nova.setTipooperacao(Tipooperacao.TIPO_DEBITO);
//							Empresa empresa = movimentacao_ultima_nova.getEmpresa() != null ? movimentacao_ultima_nova.getEmpresa() : movimentacao_ultima_nova.getCheque().getEmpresa();
							Empresa empresa = movimentacao_ultima_nova.getCheque().getEmpresa();
							if(empresa == null){
								empresa = empresaPrincipal;
							}
							if(empresa != null && movimentacao_ultima_nova.getRateio() != null){
								rateioitemService.atualizaContagerencial(movimentacao_ultima_nova.getRateio(), empresa.getContagerencialdevolucaodebito());
							}
						} else if(movimentacao_ultima.getTipooperacao() != null && movimentacao_ultima.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){ // TIPO D�BITO
							movimentacao_ultima_nova.setTipooperacao(Tipooperacao.TIPO_CREDITO);

							preencheContadevolucaoByDevolucaocheque(movimentacao_ultima.getCheque().getEmpresa(), movimentacao_ultima_nova);
//							Empresa empresa = movimentacao_ultima_nova.getEmpresa() != null ? movimentacao_ultima_nova.getEmpresa() : movimentacao_ultima_nova.getCheque().getEmpresa();
							Empresa empresa = movimentacao_ultima_nova.getCheque().getEmpresa();
							if(empresa == null){
								empresa = empresaPrincipal;
							}
							if(empresa != null && movimentacao_ultima_nova.getRateio() != null){
								rateioitemService.atualizaContagerencial(movimentacao_ultima_nova.getRateio(), empresa.getContagerencialdevolucaocredito());
							}
						}
						
						Movimentacao movimentacao_primeira_nova = movimentacaoService.criaCopia(movimentacao_primeira);
						movimentacao_primeira_nova.setMovimentacaoacao(Movimentacaoacao.NORMAL);
						
						String msgCheque = "Cheque <a href=\"javascript:visualizaCheque(" + idString + ")\">" + idString + "</a> devolvido. ";

						movimentacao_primeira_nova.setTipooperacao(movimentacao_ultima.getTipooperacao());
						if(Tipooperacao.TIPO_CREDITO.equals(movimentacao_primeira_nova.getTipooperacao())){
							Conta contaanterior = movimentacao_primeira_nova.getConta();
							preencheContadevolucaoByDevolucaocheque(movimentacao_primeira.getCheque().getEmpresa(), movimentacao_primeira_nova);
							if(contaanterior != null && movimentacao_primeira_nova.getConta() != null && contaanterior.equals(movimentacao_primeira_nova.getConta())){
								msgCheque += "Retornando o valor para o v�nculo de origem do cheque.";
							}
						}else{
							msgCheque += "Retornando o valor para o v�nculo de origem do cheque.";
						}
						movimentacao_primeira_nova.setObservacaoConciliacao(msgCheque);
						
						Movimentacaoorigem movimentacaoorigem = new Movimentacaoorigem();
						movimentacaoorigem.setMovimentacao(movimentacao_ultima_nova);
						movimentacaoorigem.setMovimentacaodevolucao(movimentacao_primeira_nova);
						listaMovimentacaoorigemSalvar.add(movimentacaoorigem);
						
						listaMovimentacaoACriar.add(movimentacao_ultima_nova);
						listaMovimentacaoACriar.add(movimentacao_primeira_nova);
					}else {
						//SOMENTE CRIA A MOVIMENTA��O SE A �LTIMA MOVIMENTA��O N�O TIVER VINCULADA A UMA CONTA A PAGAR
						if(documento_ultima_pagar == null){
							movimentacao_ultima = movimentacaoService.loadForEntrada(movimentacao_ultima);
							
							Movimentacao movimentacao_ultima_nova = movimentacaoService.criaCopia(movimentacao_ultima);
							movimentacao_ultima_nova.setMovimentacaoacao(Movimentacaoacao.NORMAL);
							movimentacao_ultima_nova.setObservacaoConciliacao("Cheque <a href=\"javascript:visualizaCheque(" + idString + ")\">" + idString + "</a> devolvido. ");
							
							if(Tipooperacao.TIPO_CREDITO.equals(movimentacao_ultima.getTipooperacao())){ // TIPO CR�DITO
								movimentacao_ultima_nova.setTipooperacao(Tipooperacao.TIPO_DEBITO);
//								Empresa empresa = movimentacao_ultima_nova.getEmpresa() != null ? movimentacao_ultima_nova.getEmpresa() : movimentacao_ultima_nova.getCheque().getEmpresa();
								Empresa empresa = movimentacao_ultima_nova.getCheque().getEmpresa();
								if(empresa == null){
									empresa = empresaPrincipal;
								}
								if(empresa != null && movimentacao_ultima_nova.getRateio() != null){
									rateioitemService.atualizaContagerencial(movimentacao_ultima_nova.getRateio(), empresa.getContagerencialdevolucaodebito());
								}
							} else if(movimentacao_ultima.getTipooperacao() != null && movimentacao_ultima.getTipooperacao().equals(Tipooperacao.TIPO_DEBITO)){ // TIPO D�BITO
								movimentacao_ultima_nova.setTipooperacao(Tipooperacao.TIPO_CREDITO);
								
								preencheContadevolucaoByDevolucaocheque(movimentacao_ultima.getCheque().getEmpresa(), movimentacao_ultima_nova);
//								Empresa empresa = movimentacao_ultima_nova.getEmpresa() != null ? movimentacao_ultima_nova.getEmpresa() : movimentacao_ultima_nova.getCheque().getEmpresa();
								Empresa empresa = movimentacao_ultima_nova.getCheque().getEmpresa();
								if(empresa == null){
									empresa = empresaPrincipal;
								}
								if(empresa != null && movimentacao_ultima_nova.getRateio() != null){
									rateioitemService.atualizaContagerencial(movimentacao_ultima_nova.getRateio(), empresa.getContagerencialdevolucaocredito());
								}
							}
							
							listaMovimentacaoACriar.add(movimentacao_ultima_nova);
						}
					}
				}
			}
		}
		
		if(idsChequesGerarDocumento_Pagar.size() > 0 && idsChequesGerarDocumento_Receber.size() > 0){
			request.addError("N�o � possivel redirecionar para tela de conta a pagar ou receber pois est�o sendo devolvidos cheques com v�nculo com movimenta��o de cr�dito e d�bito.");
			SinedUtil.redirecionamento(request, urlRedirecionamento);
			return;
		}
		
		if(idsChequesGerarDocumento_Pagar.size() > 0){
			String whereInCheque = CollectionsUtil.concatenate(idsChequesGerarDocumento_Pagar, ",");
			List<Cheque> listaCheque = chequeService.findCheques(whereInCheque);
			Money valorTotalGerarConta = new Money();
			for (Cheque cheque_aux : listaCheque) {
				valorTotalGerarConta = valorTotalGerarConta.add(cheque_aux.getValor());
			}
			
			urlRedirecionamento = "/financeiro/crud/Contapagar?ACAO=criar" +
					"&gerarContaByDevolucaoCheque=true" +
					"&valor=" + URLEncoder.encode(valorTotalGerarConta.toString(), "UTF-8") + 
					"&numero=" + URLEncoder.encode(CollectionsUtil.listAndConcatenate(listaCheque, "numero", ", "), "UTF-8") +
					"&whereInCheque=" + URLEncoder.encode(whereInCheque, "UTF-8");
		} else if(idsChequesGerarDocumento_Receber.size() > 0){
			String whereInCheque = CollectionsUtil.concatenate(idsChequesGerarDocumento_Receber, ",");
			List<Cheque> listaCheque = chequeService.findCheques(whereInCheque);
			Money valorTotalGerarConta = new Money();
			for (Cheque cheque_aux : listaCheque) {
				valorTotalGerarConta = valorTotalGerarConta.add(cheque_aux.getValor());
			}
			
			urlRedirecionamento = "/financeiro/crud/Contareceber?ACAO=criar" +
					"&gerarContaByDevolucaoCheque=true" +
					"&valor=" + URLEncoder.encode(valorTotalGerarConta.toString(), "UTF-8") + 
					"&numero=" + URLEncoder.encode(CollectionsUtil.listAndConcatenate(listaCheque, "numero", ", "), "UTF-8") +
					"&whereInCheque=" + URLEncoder.encode(whereInCheque, "UTF-8");
		}
		
		Chequedevolucaomotivo chequedevolucaomotivo = cheque.getChequedevolucaomotivo();
		String observacao = cheque.getObservacao();
		
		for (Documento documento : listaDocumentoACriar) {
			Cheque cheque_vinculado = documento.getChequeBaixarconta();
			String obsChequeDevolvido = "Cheque <a href=\"javascript:visualizaCheque(" + cheque_vinculado.getCdcheque()+");\">" + cheque_vinculado.getCdcheque()+"</a> devolvido. ";
			
			Documentoclasse documentoclasse = documento.getDocumentoclasse();
			
			documentoService.saveOrUpdate(documento);
			
			if(documento.getOrigemPedidovenda() != null || documento.getOrigemVenda() != null){
				Documentoorigem documentoorigem = new Documentoorigem();
				documentoorigem.setDocumento(documento);
				documentoorigem.setPedidovenda(documento.getOrigemPedidovenda());
				documentoorigem.setVenda(documento.getOrigemVenda());
				documentoorigem.setDevolucao(Boolean.TRUE);
				documentoorigemService.saveOrUpdate(documentoorigem);
				
				if(documento.getOrigemPedidovenda() != null){
					obsChequeDevolvido += " Origem Pedido de Venda: <a href=\"javascript:visualizaPedidoVenda("+ documento.getOrigemPedidovenda().getCdpedidovenda()+");\">"+documento.getOrigemPedidovenda().getCdpedidovenda() +"</a>";
				}
				if(documento.getOrigemVenda() != null){
					obsChequeDevolvido += " Origem Venda: <a href=\"javascript:visualizaVenda(" + documento.getOrigemVenda().getCdvenda() + ");\">" + documento.getOrigemVenda().getCdvenda() + "</a>.";
				}
			}
			
			Documentohistorico documentohistorico = new Documentohistorico();
			documentohistorico.setDocumento(documento);
			documentohistorico.setDocumentoacao(documento.getDocumentoacao());
			documentohistorico.setObservacao(obsChequeDevolvido);
			documentohistoricoService.saveOrUpdate(documentohistorico);
			
			Chequehistorico chequehistorico_doc = new Chequehistorico();
			chequehistorico_doc.setChequesituacao(Chequesituacao.DEVOLVIDO);
			chequehistorico_doc.setCheque(cheque_vinculado);
			if(documentoclasse != null && documentoclasse.equals(Documentoclasse.OBJ_PAGAR)){
				chequehistorico_doc.setObservacao("Conta a pagar <a href=\"javascript:visualizaContapagar(" + documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a> gerada a partir da devolu��o do cheque. ");
				chequehistorico_doc.setObservacaorelatorio("Conta a pagar: " + documento.getCddocumento() + ". ");
			} else if(documentoclasse != null && documentoclasse.equals(Documentoclasse.OBJ_RECEBER)){
				chequehistorico_doc.setObservacao("Conta a receber <a href=\"javascript:visualizaContareceber(" + documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a> gerada a partir da devolu��o do cheque. ");
				chequehistorico_doc.setObservacaorelatorio("Conta a receber: " + documento.getCddocumento() + ". ");
			}
			chequehistoricoService.saveOrUpdate(chequehistorico_doc);
		}
		
		for (Movimentacao movimentacao : listaMovimentacaoACriar) {
			Cheque cheque_vinculado = movimentacao.getCheque();
			Movimentacao movimentacaoAnterior = movimentacao.getMovimentacaoAnterior();
			
			movimentacaoService.saveOrUpdate(movimentacao);
			
			if(cheque_vinculado != null){
				Chequehistorico chequehistorico = new Chequehistorico();
				chequehistorico.setChequesituacao(Chequesituacao.DEVOLVIDO);
				chequehistorico.setChequedevolucaomotivo(chequedevolucaomotivo);
				chequehistorico.setCheque(cheque_vinculado);
				chequehistorico.setObservacao(
					"Movimenta��o: <a href=\"javascript:visualizaMovimentacao("+movimentacao.getCdmovimentacao()+");\">"+
					movimentacao.getCdmovimentacao()+"</a>." + (StringUtils.isNotEmpty(observacao) ? ("<br>" + observacao): "")
				);
				chequehistorico.setObservacaorelatorio(
					"Movimenta��o: "+movimentacao.getCdmovimentacao()+ ". " + (StringUtils.isNotEmpty(observacao) ? ("\n" + observacao): "")
				);
				chequehistoricoService.saveOrUpdate(chequehistorico);
				
				String obsChequeDevolvido = "Cheque <a href=\"javascript:visualizaCheque(" + cheque_vinculado.getCdcheque()+");\">" + cheque_vinculado.getCdcheque()+"</a> devolvido. ";
				
				if(movimentacaoAnterior != null){
					Movimentacaohistorico movimentacaohistorico = new Movimentacaohistorico();
					movimentacaohistorico.setMovimentacao(movimentacaoAnterior);
					movimentacaohistorico.setMovimentacaoacao(movimentacaoAnterior.getMovimentacaoacao());
					movimentacaohistorico.setObservacao(
							obsChequeDevolvido +
							"Movimenta��o gerada na devolu��o <a href=\"javascript:visualizaMovimentacao("+movimentacao.getCdmovimentacao()+");\">"+ movimentacao.getCdmovimentacao()+"</a>. ");
					movimentacaohistoricoService.saveOrUpdate(movimentacaohistorico);
				}
				
				if(movimentacao.getListaDocumentoNovo() != null && movimentacao.getListaDocumentoNovo().size() > 0){
					for(Documento documento : movimentacao.getListaDocumentoNovo()){
						Documentoclasse documentoclasse = documento.getDocumentoclasse();
						
						documentoService.saveOrUpdate(documento);
						
						if(documento.getOrigemPedidovenda() != null || documento.getOrigemVenda() != null){
							Documentoorigem documentoorigem = new Documentoorigem();
							documentoorigem.setDocumento(documento);
							documentoorigem.setPedidovenda(documento.getOrigemPedidovenda());
							documentoorigem.setVenda(documento.getOrigemVenda());
							documentoorigem.setDevolucao(Boolean.TRUE);
							documentoorigemService.saveOrUpdate(documentoorigem);
							
							if(documento.getOrigemPedidovenda() != null){
								obsChequeDevolvido += " Origem Pedido de Venda: <a href=\"javascript:visualizaPedidoVenda("+ documento.getOrigemPedidovenda().getCdpedidovenda()+");\">"+documento.getOrigemPedidovenda().getCdpedidovenda() +"</a>";
							}
							if(documento.getOrigemVenda() != null){
								obsChequeDevolvido += " Origem Venda: <a href=\"javascript:visualizaVenda(" + documento.getOrigemVenda().getCdvenda() + ");\">" + documento.getOrigemVenda().getCdvenda() + "</a>.";
							}
						}
						
						Documentohistorico documentohistorico = new Documentohistorico();
						documentohistorico.setDocumento(documento);
						documentohistorico.setDocumentoacao(documento.getDocumentoacao());
						documentohistorico.setObservacao(obsChequeDevolvido);
						documentohistoricoService.saveOrUpdate(documentohistorico);
						
						Chequehistorico chequehistorico_doc = new Chequehistorico();
						chequehistorico_doc.setChequesituacao(Chequesituacao.DEVOLVIDO);
						chequehistorico_doc.setCheque(cheque_vinculado);
						if(documentoclasse != null && documentoclasse.equals(Documentoclasse.OBJ_PAGAR)){
							chequehistorico_doc.setObservacao("Conta a pagar <a href=\"javascript:visualizaContapagar(" + documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a> gerada a partir da devolu��o do cheque. ");
							chequehistorico_doc.setObservacaorelatorio("Conta a pagar: " + documento.getCddocumento() + ". ");
						} else if(documentoclasse != null && documentoclasse.equals(Documentoclasse.OBJ_RECEBER)){
							chequehistorico_doc.setObservacao("Conta a receber <a href=\"javascript:visualizaContareceber(" + documento.getCddocumento() + ");\">" + documento.getCddocumento() + "</a> gerada a partir da devolu��o do cheque. ");
							chequehistorico_doc.setObservacaorelatorio("Conta a receber: " + documento.getCddocumento() + ". ");
						}
						chequehistoricoService.saveOrUpdate(chequehistorico_doc);
					}
				}
			}
		}
		
		if(SinedUtil.isListNotEmpty(listaMovimentacaoorigemSalvar)){
			for(Movimentacaoorigem movimentacaoorigem : listaMovimentacaoorigemSalvar){
				movimentacaoorigemService.saveOrUpdate(movimentacaoorigem);
			}
		}
		
		
		// ATUALIZA A SITUA��O DO CHEQUE
		chequeService.updateSituacao(whereIn, Chequesituacao.DEVOLVIDO);
		
		request.addMessage("Devolu��o registrada com sucesso.");
		SinedUtil.redirecionamento(request, urlRedirecionamento);
		return;
	}
	
	private void preencheContadevolucaoByDevolucaocheque(Empresa empresaCheque, Movimentacao movimentacao){
		if(empresaCheque != null){
			Empresa empresa = empresaService.load(empresaCheque, "empresa.cdpessoa, empresa.contadevolucao");
			if(empresa != null && empresa.getContadevolucao() != null){
				movimentacao.setConta(empresa.getContadevolucao());
			}
		}
	}
	
	private Documento criaDocumentoDevolucaoCheque(Documento documentoVinculo, Movimentacao movimentacao, Documentoclasse documentoclasse, Cheque cheque, Double totalDocumentos) {
		Money valorAtual = documentoVinculo.getAux_documento() != null && documentoVinculo.getAux_documento().getValoratual() != null ?
				documentoVinculo.getAux_documento().getValoratual() : documentoVinculo.getValor();
		
		Documento documento_novo = documentoService.criarCopia(documentoVinculo, true);
		documento_novo.setDocumentoclasse(documentoclasse);
		documento_novo.setDocumentoacao(Documentoacao.DEFINITIVA);
		documento_novo.setDtvencimento(SinedDateUtils.currentDate());
		documento_novo.setTaxa(null);
		
		Money valor = movimentacao.getValor();
		if(valor != null && totalDocumentos != null && totalDocumentos > 0 && valorAtual != null){
			valor = new Money(valorAtual.getValue().doubleValue() / totalDocumentos * valor.getValue().doubleValue());
		}else {
			if(movimentacao.getCheque() != null && movimentacao.getCheque().getValor() != null && movimentacao.getCheque().getValor().getValue().doubleValue() > 0){
				valor = movimentacao.getCheque().getValor();
			}
		}
		documento_novo.setValor(valor);
		documento_novo.setChequeBaixarconta(cheque);
		
		rateioService.atualizaValorRateio(documento_novo.getRateio(), documento_novo.getValor());
		
		return documento_novo;
	}
	
	
	/**
	* M�todo que abre uma popup para substituir cheque
	*
	* @param request
	* @return
	* @since 28/07/2014
	* @author Luiz Fernando
	*/
	public ModelAndView abrirSubstituicao(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		if(chequeService.isNotSituacao(whereIn, Chequesituacao.PREVISTO, Chequesituacao.DEVOLVIDO, Chequesituacao.BAIXADO)){
			request.addError("S� � poss�vel substituir cheque na situa��o (PREVISTO, DEVOLVIDO ou BAIXADO).");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(whereIn.split(",").length > 1 && chequeService.isClienteDiferente(whereIn)){
			request.addError("S� � poss�vel substituir cheques de um mesmo cliente ou avulso.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		List<Cheque> listaCheque = chequeService.findCheques(whereIn);
		Money valorTotal = new Money();
		if(SinedUtil.isListNotEmpty(listaCheque)){
			for(Cheque cheque : listaCheque){
				if(cheque.getValor() != null)
					valorTotal = valorTotal.add(cheque.getValor());
			}
		}
		
		Cheque cheque = new Cheque();
		cheque.setWhereIn(whereIn);
		cheque.setValorTotal(valorTotal);
		cheque.setQuantidadeNovocheque(1);		
		
		request.setAttribute("entrada", entrada);
		return new ModelAndView("direct:/crud/popup/popUpSubstituirCheque", "bean", cheque);
	}
	
	/**
	* M�todo que cria e salva movimenta��es/contas no processo de registrar devolu��o de cheque
	*
	* @param request
	* @param cheque
	* @since 23/07/2014
	* @author Luiz Fernando
	*/
	public void saveSubstituicao(WebRequestContext request, Cheque cheque){
		boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = cheque.getWhereIn();
		if(StringUtils.isEmpty(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.redirecionamento(request, "/sistema/crud/Cheque");
			return;
		}
		
		if(SinedUtil.isListEmpty(cheque.getListaChequesubstituto())){
			request.addError("Nenhuma cheque de substitui��o encontrado.");
			SinedUtil.redirecionamento(request, "/sistema/crud/Cheque" + (entrada ? "?ACAO=consultar&cdcheque=" + whereIn : ""));
			return;
		}
		
		Empresa empresa = null;
		List<Cheque> listaCheque = chequeService.findChequeWithEmpresa(whereIn);
		if(SinedUtil.isListNotEmpty(listaCheque)){
			for(Cheque cq : listaCheque){
				if(cq.getEmpresa() != null){
					if(empresa == null){
						empresa = cq.getEmpresa();
					}else if(!empresa.equals(cq.getEmpresa())){
						empresa = null;
						break;
					}
				}
			}
			for(Cheque chequeSubstituto : cheque.getListaChequesubstituto()){
				chequeSubstituto.setEmpresa(empresa);
			}
		}
		
		boolean duplicidadeCheque = false;
		if(chequeService.isDuplicidadeCheque(cheque.getListaChequesubstituto())){
			duplicidadeCheque = true;
		}else {
			for(Cheque cq : cheque.getListaChequesubstituto()){
				if(chequeService.verificarDuplicidadeCheque(cq)){
					duplicidadeCheque = true;
					break;
				}
			}
		}
		
		if(duplicidadeCheque){
			request.addError("Existe cheque duplicado na substitui��o.");
			SinedUtil.redirecionamento(request, "/sistema/crud/Cheque" + (entrada ? "?ACAO=consultar&cdcheque=" + whereIn : ""));
			return;
		}
		
		try {
			chequeService.saveAndExecuteSubstituicaoCheque(whereIn, cheque.getListaChequesubstituto());
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Erro ao substituir cheque(s). " + e.getMessage());
			SinedUtil.redirecionamento(request, "/sistema/crud/Cheque" + (entrada ? "?ACAO=consultar&cdcheque=" + whereIn : ""));
			return;
		}
		
		request.addMessage("Substitui��o registrada com sucesso.");
		SinedUtil.redirecionamento(request, "/sistema/crud/Cheque" + (entrada ? "?ACAO=consultar&cdcheque=" + whereIn : ""));
	}

	/**
	* M�todo que abre uma popup para justificativa do cancelamento do cheque
	*
	* @param request
	* @return
	* @since 29/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView abrirJustificativaCancelamento(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(chequeService.isNotSituacao(whereIn, Chequesituacao.PREVISTO)){
			request.addError("N�o � poss�vel cancelar o(s) cheque(s)! Necess�rio cancelar a(s) movimenta��o(�es) e conta(s) vinculada(s).");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Cheque cheque = new Cheque();
		cheque.setWhereIn(whereIn);		
		request.setAttribute("descricao", "CANCELAR CHEQUE");
		request.setAttribute("msg", "Justificativa para o cancelamento");
		request.setAttribute("acao", "cancelar");
		request.setAttribute("entrada", Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"));
		
		return new ModelAndView("direct:/crud/popup/chequeJustificativa").addObject("cheque", cheque);
	}
	
	/**
	* M�todo que cancela um cheque e registra no hist�rico a justificativa do cancelamento
	*
	* @param request
	* @param cheque
	* @since 29/10/2015
	* @author Luiz Fernando
	*/
	public void cancelar(WebRequestContext request, Cheque cheque){
		String whereIn = cheque.getWhereIn();
		if (StringUtils.isBlank(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		try {
			chequeService.registrarCancelamentoOuEstorno(whereIn, Chequesituacao.CANCELADO, cheque.getJustificativahistorico());
			request.addMessage("Cheque(s) Cancelado(s) com Sucesso.");
		} catch (Exception e) {
			request.addError("Erro no cancelamento de cheque: " + e.getMessage());
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		SinedUtil.redirecionamento(request, "/financeiro/crud/Cheque" + (entrada ? "?ACAO=consultar&cdcheque="+whereIn : ""));
	}
	
	/**
	* M�todo que abre uma popup para justificativa de estorno de cancelamento do cheque
	*
	* @param request
	* @return
	* @since 29/10/2015
	* @author Luiz Fernando
	*/
	public ModelAndView abrirJustificativaEstornoCancelamento(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(whereIn == null || "".equals(whereIn)){
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		if(chequeService.isNotSituacao(whereIn, Chequesituacao.CANCELADO)){
			request.addError("A��o permitida somente para cheque na situa��o de 'Cancelado'.");
			SinedUtil.fechaPopUp(request);
			return null;
		}
		
		Cheque cheque = new Cheque();
		cheque.setWhereIn(whereIn);		
		request.setAttribute("descricao", "ESTORNAR CANCELAMENTO");
		request.setAttribute("msg", "Justificativa para o estorno do cancelamento");
		request.setAttribute("acao", "estornarCancelamento");
		request.setAttribute("entrada", Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false"));
		
		return new ModelAndView("direct:/crud/popup/chequeJustificativa").addObject("cheque", cheque);
	}
	
	/**
	* M�todo que estorna um cancelamento de cheque
	*
	* @param request
	* @param cheque
	* @since 29/10/2015
	* @author Luiz Fernando
	*/
	public void estornarCancelamento(WebRequestContext request, Cheque cheque){
		String whereIn = cheque.getWhereIn();
		if (StringUtils.isBlank(whereIn)) {
			request.addError("Nenhum item selecionado.");
			SinedUtil.fechaPopUp(request);
			return;
		}
		
		try {
			chequeService.registrarCancelamentoOuEstorno(whereIn, Chequesituacao.PREVISTO, cheque.getJustificativahistorico());
			request.addMessage("Cheque(s) Estornado(s) com Sucesso.");
		} catch (Exception e) {
			request.addError("Erro no estorno de cancelamento de cheque: " + e.getMessage());
		}
		
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		SinedUtil.redirecionamento(request, "/financeiro/crud/Cheque" + (entrada ? "?ACAO=consultar&cdcheque="+whereIn : ""));
	}
	
	public void carregaContaByEmpresa(WebRequestContext request, Cheque cheque) throws Exception{
		View.getCurrent().convertObjectToJs(contaService.findContasAtivasComPermissao(cheque.getEmpresa() != null ? cheque.getEmpresa() : empresaService.loadPrincipal()), "lista");
	}
	
	public void buscarInfoConta(WebRequestContext request, Conta conta) throws Exception{
		chequeService.buscarInfoConta(request, conta);
	}
	
	public ModelAndView ajaxTemplatesForCopiaCheque(WebRequestContext request){
		return chequeService.ajaxTemplatesForCopiaCheque(request);
	}
	
	public ModelAndView abrirSelecaoTemplateCopiaCheque(WebRequestContext request){
		return chequeService.abrirSelecaoTemplateCopiaCheque(request);
	}
	
	public ModelAndView abrirPopupCodigobarrascheque(WebRequestContext request){
		return documentoService.abrirPopupCodigobarrascheque(request);
	}

	public ModelAndView abrirPopupProtocoloRetiradaCheque(WebRequestContext request, ProtocoloRetiradaChequeFiltro filtro){
		filtro.setWhereInCdcheque(SinedUtil.getItensSelecionados(request));
		filtro.setWhereInCdmovimentacao(null);
		return chequeService.abrirPopupProtocoloRetiradaCheque(request, filtro);
	}
}