package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

public class VinculoPneuBean {
	
	public static final Integer PRODUCAOAGENDA = 1;
	public static final Integer PRODUCAOORDEM = 2;
	public static final Integer PEDIDOVENDA = 3;
	public static final Integer VENDA = 4;
	
	protected Integer typeorigem;
	protected String tipo;
	protected Integer id;
	
	public VinculoPneuBean(Integer typeorigem, String tipo, Integer id) {
		this.typeorigem = typeorigem;
		this.tipo = tipo;
		this.id = id;
	}
	
	public Integer getTypeorigem() {
		return typeorigem;
	}
	public String getTipo() {
		return tipo;
	}
	public Integer getId() {
		return id;
	}
	
	public void setTypeorigem(Integer typeorigem) {
		this.typeorigem = typeorigem;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
