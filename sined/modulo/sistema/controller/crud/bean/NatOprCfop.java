package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Cfop;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;

public class NatOprCfop {

	private Naturezaoperacao naturezaOperacao;
	private Cfop cfop;
	
	public Cfop getCfop() {
		return cfop;
	}
	
	public void setCfop(Cfop cfop) {
		this.cfop = cfop;
	}
	
	public Naturezaoperacao getNaturezaOperacao() {
		return naturezaOperacao;
	}
	
	public void setNaturezaOperacao(Naturezaoperacao naturezaOperacao) {
		this.naturezaOperacao = naturezaOperacao;
	}
}
