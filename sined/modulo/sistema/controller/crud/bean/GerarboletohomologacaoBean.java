package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

import java.util.Date;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Rateio;

public class GerarboletohomologacaoBean {
	
	protected Empresa empresa;
	protected Conta conta;
	protected Contacarteira contacarteira;
	protected Cliente cliente;
	protected Endereco endereco;
	protected Date dtemissao;
	protected boolean percentualJuros;
	protected boolean percentualDesconto;
	protected boolean percentualMulta;
	protected Money valorjuros;
	protected Money valordesconto;
	protected Money valormulta;
	protected String descricao;
	protected Rateio rateio;
	protected Money valor;
	protected Date dtvencimento;
	protected String nossonumero;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	@DisplayName("Conta Banc�ria")
	public Conta getConta() {
		return conta;
	}
	@Required
	@DisplayName("Carteira")
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	@Required
	@DisplayName("Cliente")
	public Cliente getCliente() {
		return cliente;
	}
	@Required
	@DisplayName("Endere�o")
	public Endereco getEndereco() {
		return endereco;
	}
	@Required
	@DisplayName("Data de Emiss�o")
	public Date getDtemissao() {
		return dtemissao;
	}
	@DisplayName("Valor do Juros")
	public Money getValorjuros() {
		return valorjuros;
	}
	@DisplayName("Valor do Desconto")
	public Money getValordesconto() {
		return valordesconto;
	}
	@DisplayName("Valor da multa")
	public Money getValormulta() {
		return valormulta;
	}
	@DisplayName("Descri��o")
	public String getDescricao() {
		return descricao;
	}
	@DisplayName("Rateio")
	public Rateio getRateio() {
		return rateio;
	}
	@DisplayName("valor")
	public Money getValor() {
		return valor;
	}
	@DisplayName("Vencimento")
	public Date getDtvencimento() {
		return dtvencimento;
	}
	@DisplayName("Nosso n�mero")
	public String getNossonumero() {
		return nossonumero;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public void setDtemissao(Date dtemissao) {
		this.dtemissao = dtemissao;
	}
	public void setValorjuros(Money valorjuros) {
		this.valorjuros = valorjuros;
	}
	public void setValordesconto(Money valordesconto) {
		this.valordesconto = valordesconto;
	}
	public void setValormulta(Money valormulta) {
		this.valormulta = valormulta;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setRateio(Rateio rateio) {
		this.rateio = rateio;
	}
	public void setValor(Money valor) {
		this.valor = valor;
	}
	public void setDtvencimento(Date dtvencimento) {
		this.dtvencimento = dtvencimento;
	}
	public void setNossonumero(String nossonumero) {
		this.nossonumero = nossonumero;
	}
	
	@DisplayName("Tipo de c�lculo - Multa")
	public boolean isPercentualMulta() {
		return percentualMulta;
	}
	public void setPercentualMulta(boolean percentualMulta) {
		this.percentualMulta = percentualMulta;
	}
	@DisplayName("Tipo de c�lculo - Desconto")
	public boolean isPercentualDesconto() {
		return percentualDesconto;
	}
	public void setPercentualDesconto(boolean percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}
	@DisplayName("Tipo de c�lculo - Juros")
	public boolean isPercentualJuros() {
		return percentualJuros;
	}
	public void setPercentualJuros(boolean percentualJuros) {
		this.percentualJuros = percentualJuros;
	}
}
