package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

import java.sql.Array;
import java.sql.SQLException;


public class ChequeBean {
	
	private Integer cdcheque;
	private String recebidode;
	private String pagoa;
	
	public ChequeBean(Integer cdcheque, Array arrayPagoa, Array arrayRecebidode) {
		this.cdcheque = cdcheque;
		this.pagoa = getStringByArray(arrayPagoa);
		this.recebidode = getStringByArray(arrayRecebidode);
	}
	
	public Integer getCdcheque() {
		return cdcheque;
	}
	public String getRecebidode() {
		return recebidode;
	}
	public String getPagoa() {
		return pagoa;
	}
	public void setCdcheque(Integer cdcheque) {
		this.cdcheque = cdcheque;
	}
	public void setRecebidode(String recebidode) {
		this.recebidode = recebidode;
	}
	public void setPagoa(String pagoa) {
		this.pagoa = pagoa;
	}
	
	private String getStringByArray(Array array){
		StringBuilder sb = new StringBuilder();
		if(array != null){
			try {
				for(String obj : (String[])array.getArray()){
					if(!sb.toString().contains(obj))
					sb.append((String) obj).append("<br>");
				}
				Integer index = sb.lastIndexOf("<br>"); 
				if(index != -1){
					return sb.substring(0, index);
				}
			} catch (SQLException e) {}
		}
		return sb.toString();
	}
}
