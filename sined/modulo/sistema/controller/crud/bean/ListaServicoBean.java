package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

public class ListaServicoBean {
	
	private Integer codigo;
	private String descricao;
	
	public ListaServicoBean() {
	}
	
	public ListaServicoBean(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
