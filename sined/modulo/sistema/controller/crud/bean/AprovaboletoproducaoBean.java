package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contacarteira;

public class AprovaboletoproducaoBean {

	protected Conta conta;
	protected Contacarteira contacarteira;
	protected Integer nossonumeroUltimo;
	
	@Required
	@DisplayName("Carteira")
	public Contacarteira getContacarteira() {
		return contacarteira;
	}
	@DisplayName("Conta")
	public Conta getConta() {
		return conta;
	}
	@DisplayName("�ltimo nosso n�mero")
	public Integer getNossonumeroUltimo() {
		return nossonumeroUltimo;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public void setContacarteira(Contacarteira contacarteira) {
		this.contacarteira = contacarteira;
	}
	public void setNossonumeroUltimo(Integer nossonumeroUltimo) {
		this.nossonumeroUltimo = nossonumeroUltimo;
	}
}
