package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

import br.com.linkcom.sined.geral.bean.Material;

public class WmsBean {
	
	private Boolean erro;
	private String descricaoErro;
	private String descricaoErrolog;
	private Material material;
	
	public Boolean getErro() {
		return erro;
	}
	public String getDescricaoErro() {
		return descricaoErro;
	}
	public String getDescricaoErrolog() {
		return descricaoErrolog;
	}
	public Material getMaterial() {
		return material;
	}
	
	public void setErro(Boolean erro) {
		this.erro = erro;
	}
	public void setDescricaoErro(String descricaoErro) {
		this.descricaoErro = descricaoErro;
	}
	public void setDescricaoErrolog(String descricaoErrolog) {
		this.descricaoErrolog = descricaoErrolog;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
}
