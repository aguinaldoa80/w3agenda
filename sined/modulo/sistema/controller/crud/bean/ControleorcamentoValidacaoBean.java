package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

public class ControleorcamentoValidacaoBean {

	private String mesano;
	private Integer cdcentrocusto;
	private Integer cdcontagerencial;
	private Integer cdprojeto;
	
	public String getMesano() {
		return mesano;
	}
	public Integer getCdcentrocusto() {
		return cdcentrocusto;
	}
	public Integer getCdcontagerencial() {
		return cdcontagerencial;
	}
	public Integer getCdprojeto() {
		return cdprojeto;
	}
	public void setMesano(String mesano) {
		this.mesano = mesano;
	}
	public void setCdcentrocusto(Integer cdcentrocusto) {
		this.cdcentrocusto = cdcentrocusto;
	}
	public void setCdcontagerencial(Integer cdcontagerencial) {
		this.cdcontagerencial = cdcontagerencial;
	}
	public void setCdprojeto(Integer cdprojeto) {
		this.cdprojeto = cdprojeto;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cdcentrocusto == null) ? 0 : cdcentrocusto.hashCode());
		result = prime * result
				+ ((cdcontagerencial == null) ? 0 : cdcontagerencial.hashCode());
		result = prime * result + ((mesano == null) ? 0 : mesano.hashCode());
		result = prime * result + ((cdprojeto == null) ? 0 : cdprojeto.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ControleorcamentoValidacaoBean other = (ControleorcamentoValidacaoBean) obj;
		if (cdcentrocusto == null) {
			if (other.cdcentrocusto != null)
				return false;
		} else if (!cdcentrocusto.equals(other.cdcentrocusto))
			return false;
		if (cdcontagerencial == null) {
			if (other.cdcontagerencial != null)
				return false;
		} else if (!cdcontagerencial.equals(other.cdcontagerencial))
			return false;
		if (mesano == null) {
			if (other.mesano != null)
				return false;
		} else if (!mesano.equals(other.mesano))
			return false;
		if (cdprojeto == null) {
			if (other.cdprojeto != null)
				return false;
		} else if (!cdprojeto.equals(other.cdprojeto))
			return false;
		return true;
	}

}