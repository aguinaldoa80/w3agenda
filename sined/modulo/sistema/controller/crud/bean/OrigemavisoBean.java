package br.com.linkcom.sined.modulo.sistema.controller.crud.bean;

public class OrigemavisoBean {
	
	public static final Integer CADASTRO = 0; 		
	public static final Integer SOLICITACAO_DE_COMPRA = 1; 
	public static final Integer COTACAO = 2;
	public static final Integer ORDEM_DE_COMPRA = 3;
	public static final Integer ENTREGA = 4;
	public static final Integer ROMANEIO = 5;
	public static final Integer AGENDAMENTO = 6;
	public static final Integer CONTA_A_PAGAR = 7;			
	public static final Integer PLANEJAMENTO = 8;
	public static final Integer PEDIDOVENDA = 9;
	public static final Integer RECEBIMENTO = 10;
	public static final Integer LOTE_NOTA_FISCAL = 11;
	public static final Integer ESTOQUE = 12;
	public static final Integer CONTA_A_RECEBER = 13;
	public static final Integer VENDA = 14;
	public static final Integer NOTA_FISCAL = 15;
	public static final Integer COLETA = 16;
	public static final Integer FATURA_LOCACAO = 17;
	public static final Integer FORNECIMENTO = 18;
	public static final Integer ENTRADA_FISCAL = 19;
	public static final Integer AGENDA_INTERACAO = 20;
	public static final Integer ORDEM_SERVICO = 21;
	public static final Integer SPED_FISCAL = 22;
	public static final Integer EFD_CONTRIBUICOES = 23;
	public static final Integer CONCILIACAO_BANCARIA = 24;
	public static final Integer FECHAMENTO_FINANCEIRO = 25;
	public static final Integer REQUISICAO_MATERIAL = 26;
	public static final Integer SOLICITACAO_SERVICO = 27;
	
	protected Integer typeorigem;
	protected String tipo;
	protected String descricao;
	protected String id;
	
	public OrigemavisoBean(){}
	
	public OrigemavisoBean(Integer typeorigem, String descricao, String id){
		this.typeorigem = typeorigem;
		this.descricao = descricao;
		this.id = id;
	}

	public Integer getTypeorigem() {
		return typeorigem;
	}

	public String getTipo() {
		return tipo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getId() {
		return id;
	}

	public void setTypeorigem(Integer typeorigem) {
		this.typeorigem = typeorigem;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setId(String id) {
		this.id = id;
	}
}
