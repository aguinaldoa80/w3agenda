package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSON;

import org.apache.commons.lang.StringUtils;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Grupoquestionario;
import br.com.linkcom.sined.geral.bean.Questionario;
import br.com.linkcom.sined.geral.bean.Questionarioquestao;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.GrupoquestionarioService;
import br.com.linkcom.sined.geral.service.QuestionarioService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.QuestionarioFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Questionario", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "tipopessoaquestionario"})
public class QuestionarioCrud extends CrudControllerSined<QuestionarioFiltro, Questionario, Questionario>{

	private QuestionarioService questionarioService;
	private GrupoquestionarioService grupoquestionarioService;
	private AtividadetipoService atividadetipoService;
	
	
	public void setQuestionarioService(QuestionarioService questionarioService) {
		this.questionarioService = questionarioService;
	}
	
	public void setGrupoquestionarioService(GrupoquestionarioService grupoquestionarioService) {
		this.grupoquestionarioService = grupoquestionarioService;
	}
	
	public void setAtividadetipoService(
			AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Questionario bean)
			throws Exception {
		if (this.verificaQuestionarioAtivo(bean)){
			throw new SinedException("S� pode existir um question�rio ativo para cada tipo de pessoa.");
		}
		super.salvar(request, bean);
	}

	private boolean verificaQuestionarioAtivo(Questionario bean) {
		boolean retorno = false;
		List<Questionario> listaQuestionario = new ArrayList<Questionario>();
		if (bean.getAtivo()){
			listaQuestionario =  questionarioService.carregaListaQuestionario();
			for (Questionario q : listaQuestionario) {
				if (q.getTipopessoaquestionario() == bean.getTipopessoaquestionario() && q.getAtivo()){
					if (bean.getCdquestionario() != null && !bean.getCdquestionario().equals(q.getCdquestionario())){
						retorno = true;
					} else if (bean.getCdquestionario() == null && q.getAtivo()){
						retorno = true;
					}
				}
			}
		}
		return retorno;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Questionario form) throws Exception {
		if(form.getCdquestionario() == null){
			form.setAtivo(true);
		}
		if(form.getCdquestionario() != null && form.getListaquestionarioquestao() != null && !form.getListaquestionarioquestao().isEmpty()){
			List<JSON> array = new ArrayList<JSON>();
			for(Questionarioquestao qq : form.getListaquestionarioquestao()){
				if(qq.getListaquestionarioresposta() != null && !qq.getListaquestionarioresposta().isEmpty()){
					array.add(View.convertToJson(qq.getListaquestionarioresposta()));
				}else{
					//add um json vazio somente pra n�o pular os itens de questionarioquestao
					array.add(null);
				}
			}
			
			request.setAttribute("listaQuestionarioReposta", array);
		}
		request.setAttribute("listaAtividadetipo", atividadetipoService.findAtivos());
	}
	
	public void ajaxCalculaValorMedioAprovacao(WebRequestContext request){
		View view = View.getCurrent();
		request.getServletResponse().setContentType("text/html");		
		String whereIn = request.getParameter("whereIn");
		Integer valorMediaAprovacao = 0;
		if (StringUtils.isNotBlank(whereIn)) {
			List<Grupoquestionario> lista = grupoquestionarioService.carregaGrupoquestionarioForCalculoMediaAprovacao(whereIn.substring(0, whereIn.length() - 1));
			if(SinedUtil.isListNotEmpty(lista)){
				for(Grupoquestionario grupoquestionario : lista){			
					if(grupoquestionario != null && grupoquestionario.getMediapontuacao() != null){
						valorMediaAprovacao += grupoquestionario.getMediapontuacao();
					}
				}
				if (valorMediaAprovacao > 0) {
					valorMediaAprovacao = valorMediaAprovacao / lista.size();
				}
			}			
		}
		view.println("var valorMediaAprovacao = '" + valorMediaAprovacao + "'; ");
	}
}
