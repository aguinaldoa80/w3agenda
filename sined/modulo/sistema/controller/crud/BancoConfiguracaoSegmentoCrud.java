package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmentoCampo;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmentoImportBean;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoSegmentoService;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoSegmentoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/sistema/crud/BancoConfiguracaoSegmento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "banco", "tipoSegmento", "tamanho"})
public class BancoConfiguracaoSegmentoCrud extends CrudControllerSined<BancoConfiguracaoSegmentoFiltro, BancoConfiguracaoSegmento, BancoConfiguracaoSegmento> {
	
	private BancoService bancoService;
	private BancoConfiguracaoSegmentoService bancoConfiguracaoSegmentoService;
	
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setBancoConfiguracaoSegmentoService(BancoConfiguracaoSegmentoService bancoConfiguracaoSegmentoService) {this.bancoConfiguracaoSegmentoService = bancoConfiguracaoSegmentoService;}
	
	@Override
	protected BancoConfiguracaoSegmento criar(WebRequestContext request, BancoConfiguracaoSegmento form) throws Exception {
		if("true".equals(request.getParameter("copiar")) && form.getCdbancoconfiguracaosegmento() != null){
			form = bancoConfiguracaoSegmentoService.criarCopiaSegmento(form);
			return form;
		}		
		return super.criar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, BancoConfiguracaoSegmentoFiltro filtro) throws Exception {
		request.setAttribute("listaBanco", bancoService.findAtivos());
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, BancoConfiguracaoSegmento form) throws Exception {
		request.setAttribute("listaBanco", bancoService.findAtivos());
		bancoConfiguracaoSegmentoService.setListaVariaveisDisponiveisOnRequest(request);		
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(BancoConfiguracaoSegmento bean, BindException errors) {
		if (bean.getListaBancoConfiguracaoSegmentoCampo() == null || (bean.getListaBancoConfiguracaoSegmentoCampo() != null &&
				bean.getListaBancoConfiguracaoSegmentoCampo().size() == 0))
			errors.reject("000", "� obrigat�rio detalhar os segmentos.");
		if (bean.getListaBancoConfiguracaoSegmentoCampo() != null && bean.getTamanho() != null){
			Integer tamanhoTotal = 0;
			for (BancoConfiguracaoSegmentoCampo bcsc : bean.getListaBancoConfiguracaoSegmentoCampo()){
				if (bcsc.getTamanho() != null)
					tamanhoTotal = tamanhoTotal + bcsc.getTamanho();
			}
			if (!bean.getTamanho().equals(tamanhoTotal)){
				errors.reject("000", "A soma do tamanho dos segmentos deve ser igual ao tamanho total informado.");
			}
		}
		if(bancoConfiguracaoSegmentoService.validaBancoConfiguracaSeg(bean)){
			errors.reject("000", "N�o � poss�vel duplicar configura��o segmento.");
		}
		super.validateBean(bean, errors);
	}
	
	public ModelAndView exportarArquivo(WebRequestContext request, BancoConfiguracaoSegmentoFiltro filtro) throws CrudException, IOException {
		ByteArrayOutputStream conteudoZip = new ByteArrayOutputStream();
		bancoConfiguracaoSegmentoService.processaExportar(request, filtro, conteudoZip);
		
		HttpServletResponse response = request.getServletResponse();
		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", "attachment; filename =\"bancoconfigsegmento.zip\";");
		response.getOutputStream().write(conteudoZip.toByteArray());
				
		return null;
	}
	/**
	 * Abrindo o popup para importa��o de Arquivos
	 * @param request
	 * @since 07/06/2016
	 * @author C�sar
	 */
	public ModelAndView abrePopUpImportar(WebRequestContext request){		
		BancoConfiguracaoSegmentoImportBean bancoConfiguracaoSegmentoArquivosImportBean = new BancoConfiguracaoSegmentoImportBean();		
		return new ModelAndView("direct:/crud/popup/bancoConfigSegImportArquivo","BancoConfiguracaoSegmentoImportBean", bancoConfiguracaoSegmentoArquivosImportBean);
	}
	
	/**
	 * Upload/Processando do arquivos
	 * @param request , arquivosImportBean
	 * @author C�sar
	 * @since 07/06/2016
	 */
	public void upload(WebRequestContext request, BancoConfiguracaoSegmentoImportBean arquivosImportBean) throws CrudException, IOException {
		String mensagemErro = "";
		Boolean semArquivos = Boolean.FALSE;
		
		if(arquivosImportBean == null 
				|| arquivosImportBean.getListaAquivosImport() == null 
				|| arquivosImportBean.getListaAquivosImport().get(0).getArquivo() == null){
			semArquivos = Boolean.TRUE;
			request.addError("N�o � poss�vel realizar a importa��o sem selecionar os arquivos.");
		}

		if(!semArquivos && arquivosImportBean != null && SinedUtil.isListNotEmpty(arquivosImportBean.getListaAquivosImport())){
			mensagemErro = bancoConfiguracaoSegmentoService.importacaoDados(arquivosImportBean.getListaAquivosImport());
		
			if(!mensagemErro.isEmpty()){	
				request.addError("Erro ao importar arquivos.");
				for(String erro : mensagemErro.split("<br>")){
					request.addError(erro);
				}
			} else{
				request.addMessage("Arquivos importados com sucesso.");
			}
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>parent.doFilter();</script>");				
	}
}
