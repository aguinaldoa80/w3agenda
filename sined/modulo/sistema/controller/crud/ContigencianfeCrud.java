package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Contigencianfe;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContigencianfeFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Contigencianfe", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"tipocontigencia", "uf", "dtentrada", "dtsaida", "justificativa", "ativo"})
public class ContigencianfeCrud extends CrudControllerSined<ContigencianfeFiltro, Contigencianfe, Contigencianfe> {
	
}
