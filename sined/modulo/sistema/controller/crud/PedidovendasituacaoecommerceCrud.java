package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Pedidovendasituacaoecommerce;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path="/sistema/crud/Pedidovendasituacaoecommerce", authorizationModule=CrudAuthorizationModule.class)
public class PedidovendasituacaoecommerceCrud extends CrudControllerSined<FiltroListagemSined, Pedidovendasituacaoecommerce, Pedidovendasituacaoecommerce>{

	
}
