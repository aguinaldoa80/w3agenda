package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Veiculocor;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculotipomarcacorcombustivelFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Veiculocor", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class VeiculocorCrud extends CrudControllerSined<VeiculotipomarcacorcombustivelFiltro, Veiculocor, Veiculocor>{
	
	@Override
	protected void salvar(WebRequestContext request, Veiculocor bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_VEICULOCOR_DESCRICAO")) {
				throw new SinedException("Descri��o de cor de ve�culo j� cadastrado no sistema.");
			}
		}
	}
}
