package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.lkutil.csv.CSVWriter;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Comissionamento;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Frequencia;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.MaterialTabelaPrecoHistorico;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Materialtabelapreco;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecocliente;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoempresa;
import br.com.linkcom.sined.geral.bean.Materialtabelaprecoitem;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovendatipo;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Unidademedida;
import br.com.linkcom.sined.geral.bean.Unidademedidaconversao;
import br.com.linkcom.sined.geral.bean.enumeration.Tipocalculo;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.ComissionamentoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialTabelaPrecoHistoricoService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoService;
import br.com.linkcom.sined.geral.service.MaterialtabelaprecoitemService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.UnidademedidaService;
import br.com.linkcom.sined.geral.service.UnidademedidaconversaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MaterialtabelaprecoFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@CrudBean
@Controller(path={"/sistema/crud/Materialtabelapreco"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "dtinicio", "dtfim"})
public class MaterialtabelaprecoCrud extends CrudControllerSined<MaterialtabelaprecoFiltro, Materialtabelapreco, Materialtabelapreco> {	

	private static final String REGEX_LINHA_VALIDA = "(.*\\;)+(.*)+";
	private static final String NOME_ARQUIVO_IMPORTACAO_SESSAO = "NOME_ARQUIVO_IMPORTACAO_SESSAO";
	private MaterialService materialService;
	private MaterialtabelaprecoService materialtabelaprecoService;
	private ClienteService clienteService;
	private MaterialtabelaprecoitemService materialtabelaprecoitemService;
	private EmpresaService empresaService;
	private UnidademedidaService unidademedidaService; 
	private ComissionamentoService comissionamentoService;
	private UnidademedidaconversaoService unidademedidaconversaoService;
	private PrazopagamentoService prazopagamentoService;
	private ParametrogeralService parametrogeralService;
	private CategoriaService categoriaService;
	
	public void setMaterialtabelaprecoService(
			MaterialtabelaprecoService materialtabelaprecoService) {
		this.materialtabelaprecoService = materialtabelaprecoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public void setMaterialtabelaprecoitemService(
			MaterialtabelaprecoitemService materialtabelaprecoitemService) {
		this.materialtabelaprecoitemService = materialtabelaprecoitemService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setUnidademedidaService(UnidademedidaService unidademedidaService) {
		this.unidademedidaService = unidademedidaService;
	}
	public void setComissionamentoService(ComissionamentoService comissionamentoService) {
		this.comissionamentoService = comissionamentoService;
	}
	public void setUnidademedidaconversaoService(UnidademedidaconversaoService unidademedidaconversaoService) {
		this.unidademedidaconversaoService = unidademedidaconversaoService;
	}
	public void setPrazopagamentoService(
			PrazopagamentoService prazopagamentoService) {
		this.prazopagamentoService = prazopagamentoService;
	}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@Override
	protected Materialtabelapreco criar(WebRequestContext request, Materialtabelapreco form) throws Exception {
		Materialtabelapreco bean = super.criar(request, form);
		bean.setDtinicio(SinedDateUtils.currentDate());
		return bean;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Materialtabelapreco form)	throws CrudException {
		String cdmaterialtabelapreco = request.getParameter("cdmaterialtabelapreco");
		String stCopiar = request.getParameter("copiar");
		if (StringUtils.isNotEmpty(cdmaterialtabelapreco) && StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
			form.setCdmaterialtabelapreco((Integer.parseInt(cdmaterialtabelapreco)));
			form = materialtabelaprecoService.loadForEntrada(form);
			materialtabelaprecoService.limparReferenciasForCopiar(form);
		}
		
		if(form.getCdmaterialtabelapreco() != null)	request.setAttribute("cdmaterialtabelaprecoParam", form.getCdmaterialtabelapreco());
		
		List<Materialtabelaprecoitem> lista = new ArrayList<Materialtabelaprecoitem>();
		lista.addAll(form.getListaMaterialtabelaprecoitem());
		
		Collections.sort(lista,new Comparator<Materialtabelaprecoitem>(){
			public int compare(Materialtabelaprecoitem item1, Materialtabelaprecoitem item2) {
				try{
					Integer o1Id = Integer.parseInt(item1.getMaterial().getIdentificacao());
					Integer o2Id = Integer.parseInt(item2.getMaterial().getIdentificacao());
					return o1Id.compareTo(o2Id);
				} catch (Exception e) {
					return item1.getMaterial().getAutocompleteDescription().compareTo(item2.getMaterial().getAutocompleteDescription());
				}
			}
		});
		
		form.setListaMaterialtabelaprecoitemTrans(lista);
		
		if(request.getBindException().hasErrors()){
			Object attribute = request.getSession().getAttribute("listaMaterialtabelaprecoitem" + (form.getCdmaterialtabelapreco() != null ? form.getCdmaterialtabelapreco() : ""));
			if (attribute != null) {
				form.setListaMaterialtabelaprecoitemTrans((List<Materialtabelaprecoitem>)attribute);		
			}
		}
		request.getSession().setAttribute("listaMaterialtabelaprecoitem"  + (form.getCdmaterialtabelapreco() != null ? form.getCdmaterialtabelapreco() : ""), form.getListaMaterialtabelaprecoitemTrans());
		request.setAttribute("listaComissionamento", comissionamentoService.findAtivos(form.getComissionamento()));
		
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Materialtabelapreco form) throws Exception {
		Set<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = form.getListaMaterialtabelaprecoitem();
		if(listaMaterialtabelaprecoitem != null && listaMaterialtabelaprecoitem.size() > 0 && 
				request.getBindException() != null && request.getBindException().hasErrors()){
			String whereIn = CollectionsUtil.listAndConcatenate(listaMaterialtabelaprecoitem, "material.cdmaterial", ",");
			List<Material> listaMaterial = materialService.findForTabelaPreco(whereIn);
			for (Materialtabelaprecoitem materialtabelaprecoitem : listaMaterialtabelaprecoitem) {
				materialtabelaprecoitem.setMaterial(listaMaterial.get(listaMaterial.indexOf(materialtabelaprecoitem.getMaterial())));
			}
		}
		
		request.setAttribute("listaEmpresa", empresaService.findAtivos());
		request.setAttribute("listaPrazopagamento", prazopagamentoService.findForProcessoVenda());
		List<Tipocalculo> listaTabelaprecotipo = new ArrayList<Tipocalculo>();
		listaTabelaprecotipo.add(Tipocalculo.EM_VALOR);
		listaTabelaprecotipo.add(Tipocalculo.PERCENTUAL);
		listaTabelaprecotipo.get(0).setDescricao("Por valor");
		listaTabelaprecotipo.get(1).setDescricao("Por percentual");
		request.setAttribute("listaTabelaprecotipo", listaTabelaprecotipo);
		request.setAttribute("IDENTIFICADOR_FABRICANTE_TABELA_PRECO", parametrogeralService.getBoolean(Parametrogeral.IDENTIFICADOR_FABRICANTE_TABELA_PRECO));
		
		if (form.getCdmaterialtabelapreco() != null) {
			form.setListaMaterialTabelaPrecoHistorico(new ListSet<MaterialTabelaPrecoHistorico>(MaterialTabelaPrecoHistorico.class, MaterialTabelaPrecoHistoricoService.getInstance().findByMaterialtabelapreco(form)));

			if (form.getListaMaterialTabelaPrecoHistorico() != null) {
				for (MaterialTabelaPrecoHistorico m : form.getListaMaterialTabelaPrecoHistorico()) {
					if (m.getArquivoImportacaoProduto() != null) {
						DownloadFileServlet.addCdfile(request.getSession(), m.getArquivoImportacaoProduto().getCdarquivo());
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void validateBean(Materialtabelapreco bean, BindException errors) {
		Object attribute = NeoWeb.getRequestContext().getSession().getAttribute("listaMaterialtabelaprecoitem" + (bean.getCdmaterialtabelapreco() != null ? bean.getCdmaterialtabelapreco() : ""));
		if (attribute != null) {
			bean.setListaMaterialtabelaprecoitem(SinedUtil.listToSet((List<Materialtabelaprecoitem>)attribute, Materialtabelaprecoitem.class));
		}else if(bean.getCdmaterialtabelapreco() != null && materialtabelaprecoitemService.existeMaterialtabelaprecoitem(bean) && bean.getMaterialgrupo() == null){
			errors.reject("001", "A sess�o do usu�rio pode ter expirado durante o processo de edi��o da tabela de pre�o. � preciso atualizar p�gina e editar a tabela de pre�o novamente."); 
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaMaterialtabelaprecoitem())){
			boolean repetido = false;
			int index1 = 0;
			int index2 = 0;
			for(Materialtabelaprecoitem item1 : bean.getListaMaterialtabelaprecoitem()){
				if(Tipocalculo.PERCENTUAL.equals(bean.getTabelaprecotipo())){
					item1.setValor(null);
				}
				index2 = 0;
				Material material1 = item1.getMaterial();
				if(material1 != null){
					for(Materialtabelaprecoitem item2 : bean.getListaMaterialtabelaprecoitem()){
						Material material2 = item2.getMaterial();
						if(!repetido && index1 != index2 && material1.equals(material2)){
							boolean unidadeMedidaIgual = (item1.getUnidademedida() == null && item2.getUnidademedida() == null) || (item1.getUnidademedida() != null && item2.getUnidademedida() != null && item1.getUnidademedida().equals(item2.getUnidademedida()));
							boolean identificadorIgual = (item1.getIdentificadorespecifico() == null && item2.getIdentificadorespecifico() == null) || (item1.getIdentificadorespecifico() != null && item2.getIdentificadorespecifico() != null && item1.getIdentificadorespecifico().equals(item2.getIdentificadorespecifico()));
							
							if(unidadeMedidaIgual && identificadorIgual){
									errors.reject("001", "Material repetido. (" + item1.getMaterial().getNome() + ")");
								repetido = true;
								break;
							}
						}
						index2++;
					}
				}else {
					errors.reject("001", "Existe item de produto sem o material informado."); 
				}
				if(repetido){
					break;
				}
				index1++;
			}
		}
		
		Integer cdmaterialtabelapreco = bean.getCdmaterialtabelapreco();
		Pedidovendatipo pedidovendatipo = bean.getPedidovendatipo();
		Prazopagamento prazopagamento = bean.getPrazopagamento();
		Frequencia frequencia = bean.getFrequencia();
		Materialgrupo materialgrupo = bean.getMaterialgrupo();
		Categoria categoria = bean.getCategoria();
		Date dtinicio = bean.getDtinicio();
		Date dtfim = bean.getDtfim();
		
		Set<Materialtabelaprecoitem> lista = bean.getListaMaterialtabelaprecoitem();
		Set<Materialtabelaprecocliente> listaCliente = bean.getListaMaterialtabelaprecocliente();
		Set<Materialtabelaprecoempresa> listaEmpresa = bean.getListaMaterialtabelaprecoempresa();
		
		Empresa[] empresas = new Empresa[]{};
		if(SinedUtil.isListNotEmpty(listaEmpresa)){
			Object[] objetos = CollectionsUtil.getListProperty(listaEmpresa, "empresa").toArray();
			empresas = new Empresa[objetos.length];
			for (int i = 0; i < objetos.length; i++) {
				empresas[i] = (Empresa)objetos[i];
			}
		}
		
		if(dtfim != null && SinedDateUtils.beforeIgnoreHour(dtfim, new Date(System.currentTimeMillis()))){
			errors.reject("001", "Data limite n�o pode ser anterior a data atual.");
		}else {
			if(listaCliente != null && !listaCliente.isEmpty()){
				for(Materialtabelaprecocliente materialtabelaprecocliente : listaCliente){
					if(materialtabelaprecocliente.getCliente() != null && materialtabelaprecocliente.getCliente().getCdpessoa() != null){
						List<Materialtabelapreco> listaMaterialtabelapreco =  materialtabelaprecoService.findTabelaCliente(cdmaterialtabelapreco, materialtabelaprecocliente.getCliente(), prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, categoria, false, empresas);
						if(SinedUtil.isListNotEmpty(listaMaterialtabelapreco)){
							errors.reject("001", "J� existe tabela vigente para o per�odo e o cliente " + clienteService.load(materialtabelaprecocliente.getCliente(), "cliente.cdpessoa, cliente.nome").getNome() + "." + materialtabelaprecoService.getNomesTabelaPreco(listaMaterialtabelapreco));
						}else {
							List<Categoria> listaCategoria = categoriaService.findByPessoa(materialtabelaprecocliente.getCliente());
							if(SinedUtil.isListNotEmpty(listaCategoria)){
								for(Categoria categoriaCliente : listaCategoria){
									listaMaterialtabelapreco =  materialtabelaprecoService.findTabelaCliente(cdmaterialtabelapreco, null, prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, categoriaCliente, true, empresas);
									if(SinedUtil.isListNotEmpty(listaMaterialtabelapreco)){
										errors.reject("001", "J� existe tabela vigente para o per�odo e o cliente " + clienteService.load(materialtabelaprecocliente.getCliente(), "cliente.cdpessoa, cliente.nome").getNome() + "." + materialtabelaprecoService.getNomesTabelaPreco(listaMaterialtabelapreco));
									}
								}
							}
						}
					}
				}
			} else {
				if(categoria != null){
					List<Materialtabelapreco> listaMaterialtabelapreco =  materialtabelaprecoService.findTabelaCliente(cdmaterialtabelapreco, null, prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, categoria, false, empresas);
					if(SinedUtil.isListNotEmpty(listaMaterialtabelapreco)){
						errors.reject("001", "J� existe tabela vigente para este per�odo." + materialtabelaprecoService.getNomesTabelaPreco(listaMaterialtabelapreco));
					}	
				}
				
				List<Materialtabelapreco> listaMaterialtabelapreco =  materialtabelaprecoService.findTabelaCliente(cdmaterialtabelapreco, null, prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, materialgrupo, null, true, empresas);
				if(SinedUtil.isListNotEmpty(listaMaterialtabelapreco)){
					errors.reject("001", "J� existe tabela vigente para este per�odo." + materialtabelaprecoService.getNomesTabelaPreco(listaMaterialtabelapreco));
				}
			}
			
			if(lista != null){
				if(listaCliente != null && !listaCliente.isEmpty()){
					String whereInCliente = CollectionsUtil.listAndConcatenate(listaCliente, "cliente.cdpessoa", ",");
					for (Materialtabelaprecoitem materialtabelaprecoitem : lista) {
						if(materialtabelaprecoitem.getMaterial() != null){
							List<Materialtabelapreco> listaMaterialtabelapreco =  materialtabelaprecoService.findTabelaMaterial(cdmaterialtabelapreco, whereInCliente, 
																																materialtabelaprecoitem.getMaterial(), 
																																materialtabelaprecoitem.getIdentificadorespecifico(), 
																																prazopagamento, frequencia, pedidovendatipo, 
																																dtinicio, dtfim, materialgrupo, categoria, empresas);
							if(SinedUtil.isListNotEmpty(listaMaterialtabelapreco)){
								errors.reject("002", "O material " + materialService.load(materialtabelaprecoitem.getMaterial(), "material.cdmaterial, material.nome").getNome() + " j� est� em outra tabela vigente com este prazo de pagamento e/ou periodicidade." + materialtabelaprecoService.getNomesTabelaPreco(listaMaterialtabelapreco));
							}
						}
					}
				}else {
					for (Materialtabelaprecoitem materialtabelaprecoitem : lista) {
						if(materialtabelaprecoitem.getMaterial() != null){
							List<Materialtabelapreco> listaMaterialtabelapreco = materialtabelaprecoService.findTabelaMaterial(cdmaterialtabelapreco, null, 
																															   materialtabelaprecoitem.getMaterial(), materialtabelaprecoitem.getIdentificadorespecifico(), 
																															   prazopagamento, frequencia, pedidovendatipo, dtinicio, dtfim, 
																															   materialgrupo, categoria, empresas);
							if(SinedUtil.isListNotEmpty(listaMaterialtabelapreco)){
								errors.reject("002", "O material " + materialService.load(materialtabelaprecoitem.getMaterial(), "material.cdmaterial, material.nome").getNome() + " j� est� em outra tabela vigente com este prazo de pagamento e/ou periodicidade." + materialtabelaprecoService.getNomesTabelaPreco(listaMaterialtabelapreco));
							}
						}
					}
				}
			}
		}
			
	}
	
	/**
	 * Action que em ajax busca informa��es do material selecionado.
	 *
	 * @param request
	 * @param materialtabelapreco
	 * @since 20/07/2012
	 * @author Rodrigo Freitas
	 */
	public void ajaxCarregaInfoMaterial(WebRequestContext request, Materialtabelaprecoitem materialtabelaprecoitem){
		if(materialtabelaprecoitem != null && materialtabelaprecoitem.getMaterial() != null){
			Material material = materialService.load(materialtabelaprecoitem.getMaterial(), "material.cdmaterial, material.valorvenda, " +
					"material.valorvendaminimo, material.valorvendamaximo, material.unidademedida");
			
			StringBuilder html = new StringBuilder();
			html
				.append("var valorvenda = '" + (material.getValorvenda() != null ? material.getValorvenda() : new Money()) + "';")
				.append("var valorvendaminimo = '" + (material.getValorvendaminimo() != null ? material.getValorvendaminimo() : new Money()) + "';")
				.append("var valorvendamaximo = '" + (material.getValorvendamaximo() != null ? material.getValorvendamaximo() : new Money()) + "';")
				.append("var unidadematerial = 'br.com.linkcom.sined.geral.bean.Unidademedida[cdunidademedida=" + material.getUnidademedida().getCdunidademedida() + "]';");
			
			List<Unidademedida> listaUnidademedida = unidademedidaService.findUnidademedidarelacionado(materialtabelaprecoitem.getMaterial());
			html.append(SinedUtil.convertToJavaScript(listaUnidademedida, "listaUnidademedida", ""));
			View.getCurrent().println(html.toString());
		}
	}
	
	/**
	* M�todo que abre popup para ajuste de pre�os 
	*
	*
	* @param request
	* @return
	* @author Rafael Salvio
	*/
	public ModelAndView abrirAjustarPrecos(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		List<Materialtabelapreco> listaMaterialTabelaPreco = materialtabelaprecoService.findForAjustarPreco(whereIn);
		if(listaMaterialTabelaPreco == null || listaMaterialTabelaPreco.isEmpty()){
			SinedUtil.fechaPopUp(request);
			request.addError("A(s) tabela(s) selecionada(s) n�o possui(em) produtos a serem reajustados.");			
			SinedUtil.redirecionamento(request, "/sistema/crud/Materialtabelapreco");
			return new ModelAndView();
		}
		request.setAttribute("numberOfRows", listaMaterialTabelaPreco.size());
		return new ModelAndView("direct:/crud/popup/ajustarPrecosMaterialtabelapreco", "listamaterialtabelapreco",listaMaterialTabelaPreco);
	}
	
	/**
	 * M�todo que recebe uma lista de materiatabelaprecoitem com seus respectivos reajustes e 
	 * realiza o update no banco com os novos valores de venda.
	 * 
	 * @param request
	 * @return
	 * @author Rafael Salvio
	 */
	public ModelAndView ajustarPrecos(WebRequestContext request){
		if(request.getParameter("reajusteArray") != null){
			try{
				String[] listaTabelas = request.getParameter("reajusteArray").split(",;");
				
				for(int i=0; i<listaTabelas.length; i++){
					String[] tabela = listaTabelas[i].split(",:,");
					String whereIn = tabela[0];
					Double reajuste = 1 + (Double.parseDouble(tabela[1].replaceAll("\\.", "").replace(",", ".")) / 100);
					materialtabelaprecoitemService.ajustarPreco(whereIn, reajuste);
				}
				
				request.addMessage("Os pre�os de venda foram corretamente reajustados!");		
			}catch (Exception e) {
				request.addError("Erro ao processar as tabelas selecionadas.");		
			}
		}
		else{
			request.addError("Erro ao processar as tabelas selecionadas.");			
		}
	
		return new ModelAndView();
	}
	
	public ModelAndView converteUnidademedida(WebRequestContext request, Materialtabelaprecoitem materialtabelaprecoitem){
		Double valor = materialtabelaprecoitem.getValor();
		Double valorvendaminimo = materialtabelaprecoitem.getValorvendaminimo();
		Double valorvendamaximo = materialtabelaprecoitem.getValorvendamaximo();
		Unidademedida unidademedida = materialtabelaprecoitem.getUnidademedida();
		Unidademedida unidademedidaAntiga = materialtabelaprecoitem.getUnidademedidaAntiga();
		
		Material material = materialService.findListaMaterialunidademedida(materialtabelaprecoitem.getMaterial());
		
		Double fracao1 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedidaAntiga);
		Double fracao2 = unidademedidaService.getFracaoConversaoUnidademedida(material, unidademedida);
		
		if(fracao1 != null && fracao2 != null){
			valor = (valor / fracao2) * fracao1;
			if(valorvendaminimo != null)
				valorvendaminimo = (valorvendaminimo / fracao2) * fracao1;
			if(valorvendamaximo != null)
				valorvendamaximo = (valorvendamaximo / fracao2) * fracao1;
		} else {
			Double fracao = null;
			
			List<Unidademedidaconversao> listaUnidademedidaconversao = unidademedidaconversaoService.conversoesByUnidademedida(unidademedida);		
			if(listaUnidademedidaconversao != null && listaUnidademedidaconversao.size() > 0){
				for (Unidademedidaconversao item : listaUnidademedidaconversao) {
					if(item.getUnidademedida() != null && 
						item.getUnidademedida().getCdunidademedida() != null && 
						item.getFracao() != null){
						
						if(item.getUnidademedidarelacionada().equals(unidademedida)){
							fracao = item.getFracaoQtdereferencia();
							break;
						}
					}
				}
			}
		
			if(fracao != null && fracao > 0){
				valor = valor * fracao;
				if(valorvendaminimo != null)
					valorvendaminimo = valorvendaminimo * fracao;
				if(valorvendamaximo != null)
					valorvendamaximo = valorvendamaximo * fracao;
			}
		}
		
		return new JsonModelAndView()
					.addObject("valor", SinedUtil.descriptionDecimal(valor))
					.addObject("valorvendaminimo", SinedUtil.descriptionDecimal(valorvendaminimo))
					.addObject("valorvendamaximo", SinedUtil.descriptionDecimal(valorvendamaximo));
	}
//	
//	/**
//	* M�todo para ajustar valores do material
//	*
//	* @param request
//	* @param bean
//	* @since Jun 15, 2011
//	* @author Luiz Fernando F Silva
//	*/
//	public void ajustarpreco(WebRequestContext request, Materialajustarpreco bean){
//				
//		String whereIn = bean.getWhereIn();		
//		
//		if(bean.getPercentual() <= 0){
//			request.addError("Percentual tem que ser maior que 0.");
//			SinedUtil.fechaPopUp(request);
//			return;
//		}
//		
//		List<Material> listamaterial = materialService.findForAtualizarmaterial(whereIn);		
//		List<Materialproducao> listamaterialproducao = materialproducaoService.findForAtualizar(whereIn);
//		
//		// atualiza os materiais
//		if(listamaterial != null && !listamaterial.isEmpty()){
//			for(Material material : listamaterial){
//				if(material.getProducao() == null || !material.getProducao())
//					materialService.updateMaterialvalor(material, bean);
//					
//			}	
//		}
//		listamaterial = materialService.findForAtualizarmaterial(whereIn);
//		//atualiza os materiaisproducao
//		if(listamaterialproducao != null && !listamaterialproducao.isEmpty()){
//			for(Materialproducao materialproducao : listamaterialproducao){
//				if(materialproducao.getMaterialmestre() != null && materialproducao.getCdmaterialproducao() != null){
//					for(Material material : listamaterial){
//						if(material.getCdmaterial().equals(materialproducao.getMaterial().getCdmaterial())){
//							materialproducao.setPreco(material.getValorcusto());
//							materialproducaoService.updatePreco(materialproducao, bean);
//							break;
//						}
//					}
//										
//				}
//			}
//		}
//		
//		List<Material> listamaterialmestre = materialService.findForAtualizarmaterialmestre(whereIn);		
//		Double valorcusto = 0.0;
//		if(listamaterialmestre != null && !listamaterialmestre.isEmpty()){
//			for(Material materialmestre : listamaterialmestre){		
//				if(materialmestre.getListaProducao() != null && !materialmestre.getListaProducao().isEmpty()){
//					listamaterialproducao = materialproducaoService.findListaProducao(materialmestre.getCdmaterial());					
//					if(listamaterialproducao != null && !listamaterialproducao.isEmpty()){
//						for(Materialproducao materialproducao : listamaterialproducao){
//							valorcusto += materialproducao.getValorconsumo();
//						}											
//						materialmestre.setValorcusto(valorcusto);
//						materialService.updateMaterialmestrevalor(materialmestre, bean);
//						valorcusto = 0.0;
//						
//					}
//				}
//		}
//	}
//		
//
//		
//		request.addMessage("Material/Servi�o atualizado(s) com sucesso");
//		SinedUtil.fechaPopUp(request);
//		SinedUtil.recarregarPaginaWithClose(request);
//	}
//
	@Override
	public ModelAndView doEditar(WebRequestContext request,
			Materialtabelapreco form) throws CrudException {
		if("editar".equals(request.getParameter("ACAO")) && SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado())){
			if(!materialtabelaprecoService.isPermitidoAlterarTabela(form)){
				throw new SinedException("Edi��o n�o permitida. Existe(m) cliente(s) na tabela de pre�os para os quais o usu�rio logado n�o possui permiss�o.");
			}
		}
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,
			MaterialtabelaprecoFiltro filtro) throws CrudException {
		filtro.setLimitarAcessoClienteVendedor(SinedUtil.isRestricaoClienteVendedor(SinedUtil.getUsuarioLogado()) && parametrogeralService.getBoolean(Parametrogeral.LIMITAR_ACESSO_CLIENTEVENDEDOR));
		return super.doListagem(request, filtro);
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request,
			Materialtabelapreco form) throws CrudException {
		String whereIn = request.getParameter("itenstodelete");
		
		if (whereIn == null) {
			if (form != null && form.getCdmaterialtabelapreco() != null) {
				whereIn = form.getCdmaterialtabelapreco().toString();
			} else {
				throw new SinedException("Erro ao excluir tabela de pre�o. Nenhuma tabela de pre�o foi selecionada.");
			}
		}

		for(String cdmaterialtabelapreco: whereIn.split(",")){
			Materialtabelapreco tabela = new Materialtabelapreco(Integer.parseInt(cdmaterialtabelapreco.trim()));
			if(!materialtabelaprecoService.isPermitidoAlterarTabela(tabela)){
				throw new SinedException("Exclus�o n�o permitida. Existe(m) cliente(s) na tabela de pre�os para os quais o usu�rio logado n�o possui permiss�o.");
			}
		}

		return super.doExcluir(request, form);
	}

	@Override
	protected void salvar(WebRequestContext request, Materialtabelapreco bean) throws Exception {
		String cdmaterialtabelaprecoStr = bean.getCdmaterialtabelapreco() != null ? bean.getCdmaterialtabelapreco().toString() : "";
		String observacaoHistorico = null;

		if (bean.getCdmaterialtabelapreco() != null) {
			Materialtabelapreco beanAntigo = materialtabelaprecoService.loadForEntrada(bean);
			observacaoHistorico = materialtabelaprecoService.getCamposAlterados(beanAntigo, bean);
		}

		super.salvar(request, bean);

		MaterialTabelaPrecoHistoricoService.getInstance().salvar(bean, observacaoHistorico, (Arquivo) request.getSession().getAttribute(NOME_ARQUIVO_IMPORTACAO_SESSAO + cdmaterialtabelaprecoStr));
		request.getSession().setAttribute(NOME_ARQUIVO_IMPORTACAO_SESSAO + cdmaterialtabelaprecoStr, null);
	}

	public ModelAndView importarProdutos(WebRequestContext request, Materialtabelapreco form) throws Exception {
		String cdmaterialtabelaprecoStr = form.getCdmaterialtabelapreco() != null ? form.getCdmaterialtabelapreco().toString() : "";

		if (validarImportarProdutos(request, form)) {

			List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem = new ArrayList<Materialtabelaprecoitem>();

			Arquivo arquivo = form.getArquivoImportacaoProduto();

			String strArquivo = new String(arquivo.getContent(), Charset.forName("UTF-8"));
			String[] linhas = strArquivo.split("\\r?\\n");

			if (linhas.length > 1) {
				for (int i = 1; i < linhas.length; i++) {
					String linha = linhas[i].trim();

					if (linha.matches(REGEX_LINHA_VALIDA)) {
						String[] colunas = linha.split("\\;");

						Material material = getMaterialImportacaoProduto(colunas);
						Unidademedida unidademedida = getUnidademedidaImportacaoProduto(colunas);
						Double valor = getValorImportacaoProduto(colunas, 4);
						Double valorvendaminimo = getValorImportacaoProduto(colunas, 5);
						Double valorvendamaximo = getValorImportacaoProduto(colunas, 6);
						String identificadorespecifico = colunas.length >= 8 ? colunas[7].trim() : null;
						Comissionamento comissionamento = getComissionamentoImportacaoProduto(colunas);

						if (material == null || unidademedida == null || valor == null) {
							request.addError(getMsgNaoImportacaoProduto(i, "Material/Unidade de medida (Sigla)/Valor de venda n�o encontrados"));
							continue;
						} else {
							if (identificadorespecifico != null && identificadorespecifico.length() > 30) {
								request.addError("Identificador espec�fico com tamanho maior que 30 na linha " + i + ".");
								identificadorespecifico = null;
							}
						}

						Materialtabelaprecoitem materialtabelaprecoitem = new Materialtabelaprecoitem();
						materialtabelaprecoitem.setMaterial(material);
						materialtabelaprecoitem.setUnidademedida(unidademedida);

						if (Tipocalculo.EM_VALOR.equals(form.getTabelaprecotipo())) {
							materialtabelaprecoitem.setValor(valor);
						} else if (Tipocalculo.PERCENTUAL.equals(form.getTabelaprecotipo())) {
							materialtabelaprecoitem.setPercentualdesconto(valor);
						}

						materialtabelaprecoitem.setValorvendamaximo(valorvendamaximo);
						materialtabelaprecoitem.setValorvendaminimo(valorvendaminimo);
						materialtabelaprecoitem.setIdentificadorespecifico(identificadorespecifico);
						materialtabelaprecoitem.setComissionamento(comissionamento);

						listaMaterialtabelaprecoitem.add(materialtabelaprecoitem);
					}
				}
			}

			form.setListaMaterialtabelaprecoitemTrans(listaMaterialtabelaprecoitem);

			request.getSession().setAttribute("listaMaterialtabelaprecoitem" + cdmaterialtabelaprecoStr, listaMaterialtabelaprecoitem);
			request.getSession().setAttribute(NOME_ARQUIVO_IMPORTACAO_SESSAO + cdmaterialtabelaprecoStr, arquivo);

			return super.doEntrada(request, form);
		} else {
			request.getSession().setAttribute(NOME_ARQUIVO_IMPORTACAO_SESSAO + cdmaterialtabelaprecoStr, null);

			if (form.getCdmaterialtabelapreco() == null) {
				return super.doEntrada(request, form);
			} else {
				return new ModelAndView("redirect:/sistema/crud/Materialtabelapreco?ACAO=editar&cdmaterialtabelapreco=" + form.getCdmaterialtabelapreco());
			}
		}
	}

	private boolean validarImportarProdutos(WebRequestContext request, Materialtabelapreco form) {
		String msg = null;
		Arquivo arquivo = form.getArquivoImportacaoProduto();

		if (arquivo == null || arquivo.getSize() == null || arquivo.getSize() == 0L || arquivo.getContent() == null || arquivo.getContent().length == 0 || (!arquivo.getNome().endsWith(".csv") && !arquivo.getNome().endsWith(".CSV"))) {
			msg = "Arquivo de importa��o inv�lido.";
		} else {
			boolean colunasValidas = false;
			String strArquivo = new String(arquivo.getContent(), Charset.forName("UTF-8"));
			String[] linhas = strArquivo.split("\\r?\\n");

			for (int i = 0; i < linhas.length; i++) {
				String linha = linhas[i].trim();

				if (linha.matches(REGEX_LINHA_VALIDA)) {
					if (StringUtils.countMatches(linha, ";") >= 8) { // Qdo a �ltima coluna est� vazia o split retorna com 8 colunas
						colunasValidas = true;
					} else {
						colunasValidas = false;
						break;
					}
				}
			}

			if (!colunasValidas) {
				msg = "O arquivo de importa��o deve conter 9 colunas.";
			}
		}

		if (msg != null) {
			request.addError(msg);
			return false;
		} else {
			return true;
		}
	}

	private Material getMaterialImportacaoProduto(String[] colunas) {
		try {
			return materialService.findByCdmaterial(Integer.valueOf(colunas[0].trim()));
		} catch (Exception e) {
			return null;
		}
	}

	private Unidademedida getUnidademedidaImportacaoProduto(String[] colunas) {
		try {
			return unidademedidaService.findBySimbolo(colunas[3].trim());
		} catch (Exception e) {
			return null;
		}
	}

	private Double getValorImportacaoProduto(String[] colunas, int index) {
		try {
			String valor = colunas[index].trim();
			valor = valor.replace(".", "");
			valor = valor.replace(",", ".");
			valor = valor.trim();
			return new BigDecimal(valor).doubleValue();
		} catch (Exception e) {
			return null;
		}
	}

	private Comissionamento getComissionamentoImportacaoProduto(String[] colunas) {
		try {
			return comissionamentoService.findByNome(colunas[8].trim());
		} catch (Exception e) {
			return null;
		}
	}

	private String getMsgNaoImportacaoProduto(int linha, String motivo) {
		return "N�o foi importado o produto da linha " + (linha + 1) + ". Motivo: " + motivo + ".";
	}

	@SuppressWarnings("unchecked")
	public ModelAndView extrairProdutos(WebRequestContext request, Materialtabelapreco form) throws Exception {
		String cdmaterialtabelaprecoStr = form.getCdmaterialtabelapreco() != null ? form.getCdmaterialtabelapreco().toString() : "";

		Object attribute = request.getSession().getAttribute("listaMaterialtabelaprecoitem" + cdmaterialtabelaprecoStr);
		List<Materialtabelaprecoitem> listaMaterialtabelaprecoitem;

		if (attribute == null) {
			listaMaterialtabelaprecoitem = new ArrayList<Materialtabelaprecoitem>();
		} else {
			listaMaterialtabelaprecoitem = (List<Materialtabelaprecoitem>) attribute;
		}

		final DecimalFormat formatadorReal = new DecimalFormat("#,##0.00");

		CSVWriter csv = new CSVWriter();
		csv.setDelimiter(";");

		csv.add("C�digo interno");
		csv.add("Identificador do material");
		csv.add("Nome do material");
		csv.add("Unidade de medida (Sigla)");
		csv.add("Valor de venda");
		csv.add("Valor m�nimo");
		csv.add("Valor m�ximo");
		csv.add("Identificador espec�fico");
		csv.add("Comissionamento");

		for (Materialtabelaprecoitem m : listaMaterialtabelaprecoitem) {
			csv.newLine();
			csv.add(m.getMaterial() != null ? m.getMaterial().getCdmaterial() : null);
			csv.add(m.getMaterial() != null ? m.getMaterial().getIdentificacao() : null);
			csv.add(m.getMaterial() != null ? m.getMaterial().getNome() : null);
			csv.add(m.getUnidademedida() != null ? m.getUnidademedida().getSimbolo() : null);

			if (Tipocalculo.EM_VALOR.equals(form.getTabelaprecotipo())) {
				csv.add(m.getValor() != null ? formatadorReal.format(m.getValor()) : null);
			} else if (Tipocalculo.PERCENTUAL.equals(form.getTabelaprecotipo())) {
				csv.add(m.getPercentualdesconto() != null ? formatadorReal.format(m.getPercentualdesconto()) : null);
			}

			csv.add(m.getValorvendaminimo() != null ? formatadorReal.format(m.getValorvendaminimo()) : null);
			csv.add(m.getValorvendamaximo() != null ? formatadorReal.format(m.getValorvendamaximo()) : null);
			csv.add(m.getIdentificadorespecifico());
			csv.add(m.getComissionamento() != null ? m.getComissionamento().getNome() : null);
		}

		Resource resource = new Resource("text/csv", "tabela_preco_" + Util.strings.tiraAcento(form.getNome()).replace(" ", "_").toLowerCase() + ".csv", csv.toString().getBytes());

		return new ResourceModelAndView(resource);
	}
}