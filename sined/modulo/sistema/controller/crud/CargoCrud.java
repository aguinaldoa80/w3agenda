package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Cargoexameobrigatorio;
import br.com.linkcom.sined.geral.bean.Cbo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.CargoexameobrigatorioService;
import br.com.linkcom.sined.geral.service.CboService;
import br.com.linkcom.sined.geral.service.EpiService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CargoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.PesquisaCBO;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.PesquisaCBO.CBO;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Cargo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "cbo", "codigoFolha", "ativo"})
public class CargoCrud extends CrudControllerSined<CargoFiltro, Cargo, Cargo> {
	
	private EpiService epiService;
	private CboService cboService;
	private ParametrogeralService parametrogeralService;
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setCboService(CboService cboService) {
		this.cboService = cboService;
	}
	public void setEpiService(EpiService epiService) {
		this.epiService = epiService;
	}
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Cargo form) throws Exception {
		request.setAttribute("listaEpi", epiService.findAllEpi());
		
		String labelHora = "Semana";
		String paramCalculoHistograma = parametrogeralService.getValorPorNome(Parametrogeral.CALCULO_HISTOGRAMA_SEMANAL);
		if(paramCalculoHistograma != null && paramCalculoHistograma.trim().toUpperCase().equals("FALSE")){
			labelHora = "M�s";
		}
		request.setAttribute("labelHora", labelHora);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Cargo bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CARGO_NOME")) {
				throw new SinedException("Cargo j� cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "IDX_CARGO_CODIGOFOLHA")) {
				throw new SinedException("C�digo Folha j� cadastrado no sistema.");
			}
		}
		
	}

	@Override
	protected Cargo criar(WebRequestContext request, Cargo bean) throws Exception {
		bean =  super.criar(request, bean);
		bean.setAtivo(true);
		return bean;
	}

	@Override
	protected void listagem(WebRequestContext request, CargoFiltro filtro) throws Exception {
		super.listagem(request, filtro);
	}
	
	@Override
	protected void validateBean(Cargo bean, BindException errors){
		if (bean.getDescricao() != null && bean.getDescricao().length() > 500)
			errors.reject("001", "O campo Descri��o suporta no m�ximo 500 caracteres.");
		
		if(bean.getCdcargo() == null && (bean.getListaCargodepartamento() == null || bean.getListaCargodepartamento().isEmpty()))
			errors.reject("001", "� preciso vincular pelo menos 1 departamento ao cargo.");
	}
	
	public ModelAndView consultarCBO(WebRequestContext request){
		return new ModelAndView("direct:/crud/popup/popupConsultarCBO");
	}
	
	public ModelAndView pesquisarCBO(WebRequestContext request, CBO form){
		return new JsonModelAndView().addObject("lista", PesquisaCBO.findByCodigo(form.getCodigo()));
	}
	
	public ModelAndView selecionarCBO(WebRequestContext request, CBO form){
		
		Integer cdcbo = Integer.parseInt(form.getCodigo().replace("-", ""));
		String descricao = form.getDescricao();
		
		Cbo cbo = new Cbo(cdcbo);
		cbo = cboService.load(cbo);
		
		if(cbo == null){
			cbo = new Cbo();
			cbo.setCdcbo(cdcbo);
			cbo.setNome(descricao);
			cboService.insertCBO(cbo);
		} else if (cbo.getNome() != null && !cbo.getNome().trim().toUpperCase().equals(descricao.trim().toUpperCase())){
			cbo.setNome(descricao);
			cboService.saveOrUpdate(cbo);
		}
		
		return new JsonModelAndView().addObject("cbo", cbo);
	}
	
	public ModelAndView ajaxCarregarExamesObrigatorios(WebRequestContext request, Cargo cargo){
		List<Cargoexameobrigatorio> listaCargoexameobrigatorio = new ArrayList<Cargoexameobrigatorio>();
		
		if(cargo.getCdcargo() != null){
			listaCargoexameobrigatorio = CargoexameobrigatorioService.getInstance().findExameobrigatorioByCargo(cargo);
		}

		return new JsonModelAndView().addObject("listaCargoexameobrigatorio", listaCargoexameobrigatorio);
	}
	
//	@Override
//	protected ListagemResult<Cargo> getLista(WebRequestContext request,	CargoFiltro filtro) {
//		ListagemResult<Cargo> listagemResult = super.getLista(request, filtro);
//		List<Cargo> list = listagemResult.list();
//		
//		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcargo", ",");
//		list.removeAll(list);
//		list.addAll(cargoService.loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
//		
//		return listagemResult;
//	}
	
}
