package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Garantiareforma;
import br.com.linkcom.sined.geral.bean.Garantiareformahistorico;
import br.com.linkcom.sined.geral.bean.Garantiareformaitem;
import br.com.linkcom.sined.geral.bean.Garantiareformaitemconstatacao;
import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.geral.bean.Garantiatipopercentual;
import br.com.linkcom.sined.geral.bean.Motivodevolucao;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pedidovenda;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.Valecompra;
import br.com.linkcom.sined.geral.bean.enumeration.Garantiasituacao;
import br.com.linkcom.sined.geral.bean.enumeration.Pedidovendasituacao;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.GarantiareformaService;
import br.com.linkcom.sined.geral.service.GarantiareformahistoricoService;
import br.com.linkcom.sined.geral.service.GarantiareformaitemService;
import br.com.linkcom.sined.geral.service.GarantiareformaitemconstatacaoService;
import br.com.linkcom.sined.geral.service.GarantiareformaresultadoService;
import br.com.linkcom.sined.geral.service.GarantiatipoService;
import br.com.linkcom.sined.geral.service.GarantiatipopercentualService;
import br.com.linkcom.sined.geral.service.MotivodevolucaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PedidovendaService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.ValecompraService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiareformaFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={
				"/faturamento/crud/Garantiareforma",
				"/producao/crud/Garantiareforma"},
			authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdgarantiareforma", "dtgarantia", "cliente", "pedidovenda.cdpedidovenda", "identificadorexternopedidovenda", "vendedor", "servicogarantido", "pneu", "garantiasituacao"})
public class GarantiareformaCrud extends CrudControllerSined<GarantiareformaFiltro, Garantiareforma, Garantiareforma>{

	private GarantiareformaService garantiareformaService;
	private GarantiareformahistoricoService garantiareformahistoricoService;
	private MotivodevolucaoService motivodevolucaoService;
	private PneuService pneuService;
	private PedidovendaService pedidovendaService;
	private GarantiatipopercentualService garantiatipopercentualService;
	private GarantiatipoService garantiatipoService;
	private ValecompraService valecompraService;
	private EmpresaService empresaService;
	private ParametrogeralService parametrogeralService;
	private GarantiareformaitemService garantiareformaitemService;
	private GarantiareformaresultadoService garantiareformaresultadoService;
	private GarantiareformaitemconstatacaoService garantiareformaitemconstatacaoService;
	private ReportTemplateService reportTemplateService;
	
	public void setGarantiareformaService(
			GarantiareformaService garantiareformaService) {
		this.garantiareformaService = garantiareformaService;
	}
	
	public void setGarantiareformahistoricoService(
			GarantiareformahistoricoService garantiareformahistoricoService) {
		this.garantiareformahistoricoService = garantiareformahistoricoService;
	}
	
	public void setMotivodevolucaoService(
			MotivodevolucaoService motivodevolucaoService) {
		this.motivodevolucaoService = motivodevolucaoService;
	}
	
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	
	public void setPedidovendaService(PedidovendaService pedidovendaService) {
		this.pedidovendaService = pedidovendaService;
	}
	
	public void setGarantiatipopercentualService(
			GarantiatipopercentualService garantiatipopercentualService) {
		this.garantiatipopercentualService = garantiatipopercentualService;
	}
	
	public void setGarantiatipoService(GarantiatipoService garantiatipoService) {
		this.garantiatipoService = garantiatipoService;
	}
	
	public void setValecompraService(ValecompraService valecompraService) {
		this.valecompraService = valecompraService;
	}
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	public void setGarantiareformaitemService(
			GarantiareformaitemService garantiareformaitemService) {
		this.garantiareformaitemService = garantiareformaitemService;
	}
	
	public void setGarantiareformaresultadoService(
			GarantiareformaresultadoService garantiareformaresultadoService) {
		this.garantiareformaresultadoService = garantiareformaresultadoService;
	}
	
	public void setGarantiareformaitemconstatacaoService(GarantiareformaitemconstatacaoService garantiareformaitemconstatacaoService) {
		this.garantiareformaitemconstatacaoService = garantiareformaitemconstatacaoService;
	}
	
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}

	@Override
	protected void listagem(WebRequestContext request,
			GarantiareformaFiltro filtro) throws Exception {
		request.setAttribute("permitidoEdicao", (boolean)SinedUtil.isUserHasAction("EDICAO_GARANTIA_REFORMA"));
		request.setAttribute("gerarValeCompraGarantia", parametrogeralService.getBoolean(Parametrogeral.GERAR_VALE_COMPRA_GARANTIA));
		super.listagem(request, filtro);
	}
	
	@Override
	protected ListagemResult<Garantiareforma> getLista(
			WebRequestContext request, GarantiareformaFiltro filtro) {
		ListagemResult<Garantiareforma> lista = super.getLista(request, filtro);
		
		for(Garantiareforma bean: lista.list()){
			
			for(Garantiareformaitem item: bean.getListaGarantiareformaitem()){
				bean.setServicogarantido(item.getMaterial());
				if(item.getPneu() != null && item.getPneu().getCdpneu() != null){
					bean.setPneu(item.getPneu().getCdpneu().toString());
				}
			}
		}
		return lista;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Garantiareforma form)
			throws Exception {
		super.entrada(request, form);
		criaMapaListapercentuais(request, form);

		if(form.getListaGarantiareformaitem() != null){
			for(Garantiareformaitem item: form.getListaGarantiareformaitem()){
				if(item.getPneu() != null && item.getPneu().getMaterialbanda() != null){
					item.getPneu().setProfundidadesulco(item.getPneu().getMaterialbanda().getProfundidadesulco());
				}
			}
		}
		
		setConstatacao(form);
		
		request.setAttribute("gerarValeCompraGarantia", parametrogeralService.getBoolean(Parametrogeral.GERAR_VALE_COMPRA_GARANTIA));
		request.setAttribute("permitidoEdicao", (boolean)SinedUtil.isUserHasAction("EDICAO_GARANTIA_REFORMA"));
		request.setAttribute("isDisponivel", Garantiasituacao.DISPONIVEL.equals(form.getGarantiasituacao()));
		request.setAttribute("isCancelada", Garantiasituacao.CANCELADA.equals(form.getGarantiasituacao()));
		request.setAttribute("listaConstatacao", motivodevolucaoService.findForConstacacao());
		if(!"consultar".equals(request.getParameter("ACAO"))){
			request.setAttribute("listaResultados", garantiareformaresultadoService.findAtivos());
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Garantiareforma bean)
			throws Exception {
		boolean isCriar = bean.getCdgarantiareforma() == null;
		if(bean.getListaGarantiareformaitem() != null){
			for(Garantiareformaitem item: bean.getListaGarantiareformaitem()){
				if(item.getGarantiatipo() != null && item.getGarantiatipopercentual() == null &&
					Boolean.TRUE.equals(garantiatipoService.load(item.getGarantiatipo()).getGeragarantia())){
					
					throw new SinedException("Para tipo de garantia que gera garantia o campo % de garantia n�o pode ficar em branco.");
				}
				if(item.getPneu() == null || !item.getPneu().existeDados()){
					throw new SinedException("O campo Pneu n�o pode ficar em branco.");
				}
			}
		}
		try {
			if(bean.getListaGarantiareformaitem() != null){
				for(Garantiareformaitem item: bean.getListaGarantiareformaitem()){
					if(item.getPneu() != null && item.getPneu().existeDados()){
						pneuService.saveOrUpdate(item.getPneu());
					}
				}
			}
			super.salvar(request, bean);
			if(isCriar){
				Garantiareformahistorico historico = new Garantiareformahistorico();
				historico.setAcao(Garantiasituacao.DISPONIVEL);
				historico.setGarantiareforma(bean);
				historico.setObservacao("Garantia gerada manualmente.");
				garantiareformahistoricoService.saveOrUpdate(historico);
			}
			
			saveConstatacao(bean);
		} catch (Exception e) {
			throw new SinedException(e.getMessage());
		}
		
	}
	
	public ModelAndView abrePopupSelecaoServicogarantido(WebRequestContext request, Pedidovenda pedidovenda){
		return pedidovendaService.abrePopupSelecaoServicogarantido(request, pedidovenda);
	}
	
	public ModelAndView estornar(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		String controller = request.getParameter("controller");
		if(StringUtils.isEmpty(whereIn)){
			return this.errorFechaPopup(request, "Nenhum item selecionado.");
		}
		List<Garantiareforma> lista = garantiareformaService.findByNotInGarantiasituacao(whereIn, Garantiasituacao.UTILIZADA);
		if(!lista.isEmpty()){
			return this.errorFechaPopup(request, "Existe(m) garantia(s) selecionada(s) com situa��o diferente de Utilizada.");
		}
		for(Garantiareforma garantiareforma: garantiareformaService.findByWherein(whereIn)){
			if(garantiareforma.getPedidovenda() != null){
				if(Pedidovendasituacao.PREVISTA.equals(pedidovendaService.load(garantiareforma.getPedidovenda()).getPedidovendasituacao())){
					return this.errorFechaPopup(request, "N�o � poss�vel realizar o estorno de garantia referente a pedido na situa��o diferente de Prevista.");
				}
			}
			List<Valecompra> listavalecompra = valecompraService.findByGarantiareforma(garantiareforma, null);
			if(listavalecompra == null || listavalecompra.isEmpty() || !Tipooperacao.TIPO_CREDITO.equals(listavalecompra.get(0).getTipooperacao())){
				return this.errorFechaPopup(request, "N�o existe vale compra vinculado � garantia "+garantiareforma.getCdgarantiareforma()+".");
			}
			Garantiareformaitem garantiareformaitem = garantiareformaitemService.loadByVerificacaoUsoVenda(garantiareforma);
			if(garantiareformaitem.getPedidovendamaterialdestino() != null){
				return this.errorFechaPopup(request, "A garantia "+garantiareforma.getCdgarantiareforma()+" foi utilizada no pedido "+garantiareformaitem.getPedidovendamaterialdestino().getPedidovenda().getCdpedidovenda()+". Para que a garantia fique dispon�vel novamente ela deve ser exclu�da do pedido.");
			}
			if(garantiareformaitem.getVendamaterialdestino() != null){
				return this.errorFechaPopup(request, "A garantia "+garantiareforma.getCdgarantiareforma()+" foi utilizada na venda "+garantiareformaitem.getVendamaterialdestino().getVenda().getCdvenda()+". Para que a garantia fique dispon�vel novamente ela deve ser exclu�da da venda.");
			}
		}

		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("controller", controller);
		request.setAttribute("isEstornoCancelamento", false);
		request.setAttribute("isEstornoValecompra", true);
		Garantiareformahistorico historico = new Garantiareformahistorico();
		historico.setAcao(Garantiasituacao.DISPONIVEL);
		historico.setSelectedItens(whereIn);
		return new ModelAndView("direct:/crud/popup/estornogarantiaJustificativa").addObject("garantiareformahistorico", historico);
	}
	
	private ModelAndView errorFechaPopup(WebRequestContext request, String msgErro){
		SinedUtil.fechaPopUp(request);
		request.addError(msgErro);
		return null;
	}
	
	public ModelAndView estornarCancelamento(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		String controller = request.getParameter("controller");
		if(StringUtils.isEmpty(whereIn)){
			return this.errorFechaPopup(request, "Nenhum item selecionado.");
		}
		List<Garantiareforma> lista = garantiareformaService.findByNotInGarantiasituacao(whereIn, Garantiasituacao.CANCELADA);
		if(!lista.isEmpty()){
			return this.errorFechaPopup(request, "Existe(m) garantia(s) selecionada(s) com situa��o diferente de Cancelada.");
		}
		for(Garantiareforma garantiareforma: garantiareformaService.findByWherein(whereIn)){
			if(garantiareforma.getPedidovenda() != null){
				if(Pedidovendasituacao.PREVISTA.equals(pedidovendaService.load(garantiareforma.getPedidovenda()).getPedidovendasituacao())){
					return this.errorFechaPopup(request, "N�o � poss�vel realizar o estorno de garantia referente a pedido na situa��o diferente de Prevista.");
				}
			}
		}
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("controller", controller);
		request.setAttribute("isEstornoCancelamento", true);
		request.setAttribute("isEstornoValecompra", false);
		Garantiareformahistorico historico = new Garantiareformahistorico();
		historico.setAcao(Garantiasituacao.DISPONIVEL);
		historico.setSelectedItens(whereIn);
		return new ModelAndView("direct:/crud/popup/estornogarantiaJustificativa").addObject("garantiareformahistorico", historico);
	}
	
	public ModelAndView saveEstornar(WebRequestContext request, Garantiareformahistorico bean){
		String whereIn = request.getParameter("selectedItens");
		String isEstornoValecompra = request.getParameter("isEstornoValecompra");
		if(StringUtils.isNotEmpty(whereIn)){
			if("true".equalsIgnoreCase(isEstornoValecompra)){
				for(Garantiareforma garantiareforma: garantiareformaService.findByWherein(whereIn)){
					Garantiareformaitem garantiareformaitem = garantiareformaitemService.loadByGarantiareformaForCancelarValecompra(garantiareforma);
					if(garantiareformaitem != null)
						garantiareformaitemService.cancelaValecompraByGarantia(garantiareformaitem,
																		"D�bito de vale compra referente a estorno de vale compra de garantia.");
				}
			}
			garantiareformaService.updateSituacaoGarantia(whereIn, Garantiasituacao.DISPONIVEL);
			for(Garantiareforma garantiareforma: garantiareformaService.findByWherein(whereIn)){
				Garantiareformahistorico historico = new Garantiareformahistorico();
				historico.setAcao(bean.getAcao());
				historico.setGarantiareforma(garantiareforma);
				historico.setObservacao(bean.getObservacao());
				garantiareformahistoricoService.saveOrUpdate(historico);
			}
			request.addMessage("Estorno(s) realizado(s) com sucesso.");
		}
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	public ModelAndView cancelar(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		if(StringUtils.isEmpty(whereIn)){
			return this.errorFechaPopup(request, "Nenhum item selecionado.");
		}
		
		List<Garantiareforma> lista = garantiareformaService.findByNotInGarantiasituacao(whereIn, Garantiasituacao.DISPONIVEL);
		if(!lista.isEmpty()){
			return this.errorFechaPopup(request, "N�o � poss�vel realizar o cancelamento de garantias com situa��o diferente de Dispon�vel.");
		}
		
		request.setAttribute("selectedItens", whereIn);
		Garantiareformahistorico historico = new Garantiareformahistorico();
		historico.setAcao(Garantiasituacao.CANCELADA);
		historico.setSelectedItens(whereIn);
		return new ModelAndView("direct:/crud/popup/cancelamentogarantiaJustificativa").addObject("garantiareformahistorico", historico);
	}
	
	public ModelAndView saveCancelar(WebRequestContext request, Garantiareformahistorico bean){
		String whereIn = request.getParameter("selectedItens");
		if(StringUtils.isNotEmpty(whereIn)){
			garantiareformaService.updateSituacaoGarantia(whereIn, Garantiasituacao.CANCELADA);
			
			for(Garantiareforma garantiareforma: garantiareformaService.findByWherein(whereIn)){
				Garantiareformahistorico historico = new Garantiareformahistorico();
				historico.setAcao(bean.getAcao());
				historico.setGarantiareforma(garantiareforma);
				historico.setObservacao(bean.getObservacao());
				garantiareformahistoricoService.saveOrUpdate(historico);
			}
			request.addMessage("Cancelamento(s) realizado(s) com sucesso.");
		}
		SinedUtil.fechaPopUp(request);
		return null;
	}
	
	private ModelAndView getControllerModelAndView(WebRequestContext request){
		String action = request.getParameter("controller");
		if (request.getParameter("cdgarantiareforma") != null) {
			action += "&cdgarantiareforma=" + request.getParameter("cdgarantiareforma");
		}
		return new ModelAndView("redirect:"+action);
	}
	
	public void ajaxBuscaGarantiatipopercentual(WebRequestContext request, Garantiatipo garantiatipo){
		String listaStr = null;
		if(garantiatipo != null && garantiatipo.getCdgarantiatipo() != null){
			List<Garantiatipopercentual> listaPercentuais = garantiatipopercentualService.findBy(garantiatipo, "cdgarantiatipopercentual");
			if(listaPercentuais != null){
				listaStr = SinedUtil.convertToJavaScript(listaPercentuais, "listaGarantiatipopercentual", null);
			}
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(listaStr != null ? listaStr : "var listaGarantiatipopercentual = '';");
	}
	
	private void criaMapaListapercentuais(WebRequestContext request, Garantiareforma bean){
		Map<Garantiatipo, List<Garantiatipopercentual>> mapaPercentuais = new HashMap<Garantiatipo, List<Garantiatipopercentual>>();
		
		if(bean != null && bean.getListaGarantiareformaitem() != null) {
			for (Garantiareformaitem garantiareformaitem : bean.getListaGarantiareformaitem()) {
				if(garantiareformaitem.getGarantiatipo() != null){
					if(!mapaPercentuais.containsKey(garantiareformaitem.getGarantiatipo())){
						mapaPercentuais.put(garantiareformaitem.getGarantiatipo(),
											garantiatipopercentualService.findBy(garantiareformaitem.getGarantiatipo(), "cdgarantiatipopercentual"));
					}
				}
			}
		}
		request.setAttribute("mapaPercentuais", mapaPercentuais);
	}
	
	public ModelAndView ajaxBuscaPedidoorigem(WebRequestContext request, Pedidovenda pedidovenda){
		Pedidovenda bean = pedidovendaService.findForGarantiareforma(pedidovenda);
		ModelAndView retorno = new JsonModelAndView();
		
		if(bean != null && bean.getPedidovendaorigem() != null){
			retorno.addObject("cdpedidovendaorigem", bean.getPedidovendaorigem().getCdpedidovenda())
					.addObject("identificadorrigem", bean.getPedidovendaorigem().getIdentificador())
					.addObject("cdempresapedidoorigem", bean.getPedidovendaorigem().getEmpresa().getCdpessoa())
					.addObject("nomeCliente", bean.getNomeCliente())
					.addObject("cdcliente", bean.getCliente().getCdpessoa());
			if(bean.getColaborador() != null){
				retorno.addObject("cdvendedor", bean.getColaborador().getCdpessoa())
						.addObject("nomeVendedor", bean.getColaborador().getNome());
			}
		}
		return retorno;
	}
	
	
	@Override
	protected Garantiareforma criar(WebRequestContext request,
			Garantiareforma form) throws Exception {
		Garantiareforma bean = super.criar(request, form);
		bean.setGarantiasituacao(Garantiasituacao.DISPONIVEL);
		bean.setPedidoexterno(true);
		bean.setEmpresa(empresaService.loadPrincipal());
		bean.setDtgarantia(SinedDateUtils.currentDate());
		return bean;
	}
	
	public ModelAndView gerarValecompra(WebRequestContext request){
		String whereIn = request.getParameter("selectedItens");
		if(StringUtils.isEmpty(whereIn)){
			throw new SinedException("Nenhum item selecionado.");
		}
		List<Garantiareforma> listaGarantiaDiferenteCancelada = garantiareformaService.findByNotInGarantiasituacao(whereIn, Garantiasituacao.DISPONIVEL);
		if(!listaGarantiaDiferenteCancelada.isEmpty()){
			request.addError("N�o � poss�vel gerar vale compra de garantia com situa��o diferente de Dispon�vel.");
			return getControllerModelAndView(request);
		}
		List<Garantiareforma> listaGarantia = garantiareformaService.findForCalculargarantia(whereIn);
		for(Garantiareforma garantiareforma: listaGarantia){
			for(Garantiareformaitem garantiareformaitem: garantiareforma.getListaGarantiareformaitem()){
				if(garantiareformaitem.getGarantiatipopercentual() == null){
					request.addError("O tipo de garantia referente � garantia de reforma "+garantiareforma.getCdgarantiareforma()+" n�o gera garantia.");
					return getControllerModelAndView(request);
				}
			}
		}
		List<Valecompra> listaValecompra = new ArrayList<Valecompra>();
		List<Garantiareformahistorico> listaHistorico = new ArrayList<Garantiareformahistorico>();
		for(Garantiareforma garantiareforma: listaGarantia){
			for(Garantiareformaitem garantiareformaitem: garantiareforma.getListaGarantiareformaitem()){
				garantiareformaitem.setGarantiareforma(garantiareforma);
				try {
					Valecompra valecompra = valecompraService.criaValecompraByGarantia(garantiareformaitem);
					listaValecompra.add(valecompra);
					
					Garantiareformahistorico historico = new Garantiareformahistorico();
					historico.setAcao(Garantiasituacao.UTILIZADA);
					historico.setGarantiareforma(garantiareforma);
					historico.setObservacao("Garantia utilizada na gera��o de vale compra.");
					listaHistorico.add(historico);
				} catch (Exception e) {
					request.addError(e.getMessage());
					return getControllerModelAndView(request);
				}
			}
		}
		valecompraService.saveValeCompra(listaValecompra);
		garantiareformahistoricoService.saveHistorico(listaHistorico);
		garantiareformaService.updateSituacaoGarantia(whereIn, Garantiasituacao.UTILIZADA);
		request.addMessage("Vale compra gerado com sucesso.");
		return getControllerModelAndView(request);
	}
	
	private void setConstatacao(Garantiareforma form){
		if (form.getListaGarantiareformaitem()!=null){
			for (Garantiareformaitem garantiareformaitem: form.getListaGarantiareformaitem()){
				List<Garantiareformaitemconstatacao> listaGarantiareformaitemconstatacao = garantiareformaitem.getListaGarantiareformaitemconstatacao();
				if (listaGarantiareformaitemconstatacao!=null){
					
					Collections.sort(listaGarantiareformaitemconstatacao, 
										new Comparator<Garantiareformaitemconstatacao>() {
											@Override
											public int compare(Garantiareformaitemconstatacao o1, Garantiareformaitemconstatacao o2) {
												return o1.getMotivodevolucao().getDescricao().trim().compareToIgnoreCase(o2.getMotivodevolucao().getDescricao().trim());
											}
										});
					
					for (int i=0; i<listaGarantiareformaitemconstatacao.size(); i++){
						switch (i) {
							case 0:
								garantiareformaitem.setMotivodevolucaoConstatacao1(listaGarantiareformaitemconstatacao.get(i).getMotivodevolucao());
								break;	
							case 1:
								garantiareformaitem.setMotivodevolucaoConstatacao2(listaGarantiareformaitemconstatacao.get(i).getMotivodevolucao());
								break;	
							case 2:
								garantiareformaitem.setMotivodevolucaoConstatacao3(listaGarantiareformaitemconstatacao.get(i).getMotivodevolucao());
								break;	
							case 3:
								garantiareformaitem.setMotivodevolucaoConstatacao4(listaGarantiareformaitemconstatacao.get(i).getMotivodevolucao());
								break;	
							case 4:
								garantiareformaitem.setMotivodevolucaoConstatacao5(listaGarantiareformaitemconstatacao.get(i).getMotivodevolucao());
								break;	
							default:
								break;
						}
					}
				}
			}
		}
	}
	
	private void saveConstatacao(Garantiareforma bean){
		if (Boolean.TRUE.equals(bean.getPedidoexterno()) && Garantiasituacao.DISPONIVEL.equals(bean.getGarantiasituacao())){
			if (bean.getListaGarantiareformaitem()!=null){
				for (Garantiareformaitem garantiareformaitem: bean.getListaGarantiareformaitem()){
					garantiareformaitemconstatacaoService.delete(garantiareformaitem);
					if (garantiareformaitem.getListaMotivodevolucaoConstatacao()!=null){
						for (Motivodevolucao motivodevolucao: garantiareformaitem.getListaMotivodevolucaoConstatacao()){
							Garantiareformaitemconstatacao garantiareformaitemconstatacao = new Garantiareformaitemconstatacao();
							garantiareformaitemconstatacao.setGarantiareformaitem(garantiareformaitem);
							garantiareformaitemconstatacao.setMotivodevolucao(motivodevolucao);
							garantiareformaitemconstatacaoService.saveOrUpdate(garantiareformaitemconstatacao);
						}
					}
				}
			}
		}		
	}
	
	@Override
	protected void validateBean(Garantiareforma bean, BindException errors) {
		if (Boolean.TRUE.equals(bean.getPedidoexterno()) && Garantiasituacao.DISPONIVEL.equals(bean.getGarantiasituacao())){
			if (bean.getListaGarantiareformaitem()!=null){
				Set<String> listaErro = new HashSet<String>();
				
				for (Garantiareformaitem garantiareformaitem: bean.getListaGarantiareformaitem()){
					if (garantiareformaitem.getListaMotivodevolucaoConstatacao()!=null){
						for (int i=0; i<garantiareformaitem.getListaMotivodevolucaoConstatacao().size(); i++){
							Motivodevolucao motivodevolucao1 = garantiareformaitem.getListaMotivodevolucaoConstatacao().get(i);
							for (int j=0; j<garantiareformaitem.getListaMotivodevolucaoConstatacao().size(); j++){
								Motivodevolucao motivodevolucao2 = garantiareformaitem.getListaMotivodevolucaoConstatacao().get(j);
								if (i!=j){
									if (motivodevolucao1!=null
											&& motivodevolucao2!=null
											&& motivodevolucao1.getCdmotivodevolucao().equals(motivodevolucao2.getCdmotivodevolucao())){
										listaErro.add("A constata��o " + motivodevolucaoService.load(motivodevolucao1).getDescricao() + " est� duplicado. Verifique as constata��es e remova o item em duplicidade.");
									}
								}
							}
						}
					}
				}
				
				int cont = 1;
				for (String erro: listaErro){
					errors.reject(String.valueOf(cont++), erro);
				}
			}
		}
		
		garantiareformaService.validaObrigatoriedadePneu(bean, null, errors);
		
		super.validateBean(bean, errors);
	}
	
	public ModelAndView abrirImprimirGarantiaReforma(WebRequestContext request){
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isEmpty(whereIn)){
			return this.errorFechaPopup(request, "Nenhum item selecionado.");
		}
		
		List<ReportTemplateBean> templates = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.GARANTIA_DE_REFORMA, "nome");
		request.setAttribute("selectedItens", whereIn);
		request.setAttribute("garantiaReformaTemplates", templates);
		
		return new ModelAndView("direct:/crud/popup/garantiaReformaTemplate");
	}
	
	public ModelAndView ajaxListaTemplatesGarantiaReforma(WebRequestContext request) {
		JsonModelAndView json = new JsonModelAndView();
		
		List<ReportTemplateBean> reportTemplateBean = reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.GARANTIA_DE_REFORMA);
		json.addObject("garantiaReformaTemplatesSize", reportTemplateBean.size());
		
		if(reportTemplateBean.size() == 1){
			json.addObject("cdtemplate", reportTemplateBean.get(0).getCdreporttemplate());
		}
		
		return json;
	}
}