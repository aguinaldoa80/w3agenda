package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.AtributosMaterial;
import br.com.linkcom.sined.geral.bean.Campanhahistorico;
import br.com.linkcom.sined.geral.bean.AtributoMaterialHistorico;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.enumeration.AtributosMaterialAcao;
import br.com.linkcom.sined.geral.bean.enumeration.Expedicaoacao;
import br.com.linkcom.sined.geral.service.AtributosMaterialService;
import br.com.linkcom.sined.geral.service.AtributoMaterialHistoricoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AtributosMaterialFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Atributosmaterial", authorizationModule=CrudAuthorizationModule.class)
public class AtributosMaterialCrud extends CrudControllerSined<AtributosMaterialFiltro, AtributosMaterial, AtributosMaterial>{
	
	protected AtributoMaterialHistoricoService atributoMaterialHistoricoService;
	protected AtributosMaterialService atributosMaterialService;
	
	
	public void setatributoMaterialHistoricoService(AtributoMaterialHistoricoService atributoMaterialHistoricoService) {this.atributoMaterialHistoricoService = atributoMaterialHistoricoService;}
	public void setAtributosMaterialService(AtributosMaterialService atributosMaterialService) {this.atributosMaterialService = atributosMaterialService;}



	@Override
	protected void salvar(WebRequestContext request, AtributosMaterial bean)
			throws Exception {
		boolean isCriar = bean.getCdatributosmaterial() == null;
		AtributosMaterial beanAntigo = null;
		String obs = null;
		
		if(bean.getCdatributosmaterial() != null){
			beanAntigo = atributosMaterialService.loadForEntrada(bean);
		}
		
		if(beanAntigo != null) {
			obs = "De: "+beanAntigo.getNome()+" para: "+bean.getNome()+"<br>";
		//	obs = atributoMaterialHistoricoService.getCamposAlterados(beanAntigo, bean);
			//obs = compararPedidoVendaTipoSelectsPorObjeto(obs, beanAntigo, bean);
		}
		super.salvar(request, bean);
		
		if(bean != null ){
			AtributoMaterialHistorico historicoAtributo = new AtributoMaterialHistorico();
			
			historicoAtributo.setAtributosMaterial(bean);
			historicoAtributo.setAcao(isCriar ? AtributosMaterialAcao.CRIADA : AtributosMaterialAcao.ALTERADA);
			historicoAtributo.setObservacao(obs);
			historicoAtributo.setDtaltera(bean.getDtaltera());
			historicoAtributo.setCdusuarioaltera(bean.getCdusuarioaltera());
			
			atributoMaterialHistoricoService.saveOrUpdate(historicoAtributo);
		}
		
	}
	
	@Override
	protected void validateBean(AtributosMaterial bean, BindException errors) {
		boolean erro = false;
		if(bean.getCdatributosmaterial() != null){
			AtributosMaterial atributosMaterial = atributosMaterialService.load(bean);
			
			if(atributosMaterial != null && !atributosMaterial.getNome().equals(bean.getNome()) && atributosMaterialService.existeAtributo(bean)){
				erro = true;
			}
		}else if(atributosMaterialService.existeAtributo(bean)){
			erro = true;
		}
		
		if(erro){
			errors.reject("001", "J� existe um atributo cadastrado com este nome.");
		}
		
		super.validateBean(bean, errors);
	}
}
