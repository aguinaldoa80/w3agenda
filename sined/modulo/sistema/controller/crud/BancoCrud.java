package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Banco;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Banco", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "numero", "ativo"})
public class BancoCrud extends CrudControllerSined<BancoFiltro, Banco, Banco> {
	
	@Override
	protected void salvar(WebRequestContext request, Banco bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_banco_nome")) {throw new SinedException("Banco j� cadastrado no sistema.");}	
			if (DatabaseError.isKeyPresent(e, "idx_banco_numero")) {throw new SinedException("N�mero de banco j� cadastrado no sistema.");}
				
		}
		
	}

	@Override
	protected void validateBean(Banco bean, BindException errors) {
		if (bean.getNumeroString() != null && bean.getNumeroString().length() < 3) {
			errors.reject("001", "O campo n�mero deve possuir 3 d�gitos.");
		}
		if (bean.getNumeroString()!=null && !SinedUtil.validaNumeros(bean.getNumeroString())) {
			errors.reject("001", "O caracter digitado n�o � um n�mero v�lido.");
		}
	}

	@Override
	protected void entrada(WebRequestContext request, Banco bean) throws Exception {
		if (bean.getAtivo() == null) {
			bean.setAtivo(true);
		}
		
		if (bean.getCorPainelCobranca()==null){
			bean.setCorPainelCobranca(Banco.COR_PADRAO_PAINEL_COBRANCA);
		}
		
		request.setAttribute("ACAO", request.getParameter("ACAO"));
		
		super.entrada(request, bean);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, BancoFiltro filtro)
			throws CrudException {
		
		if (!filtro.isNotFirstTime()) {
			filtro.setMostrar(Boolean.TRUE);
		}
		
		return super.doListagem(request, filtro);
	}
	
}
