package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.ArquivoIntegracaoBridgestone;
import br.com.linkcom.sined.geral.service.ArquivoIntegracaoBridgestoneService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ArquivoIntegracaoBridgestoneFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/ArquivoBridgestone", authorizationModule=CrudAuthorizationModule.class)
public class ArquivoIntegracaoBridgestoneCrud extends CrudControllerSined<ArquivoIntegracaoBridgestoneFiltro, ArquivoIntegracaoBridgestone, ArquivoIntegracaoBridgestone> {

	protected ArquivoIntegracaoBridgestoneService arquivoIntegracaoBridgestoneService;
	public void setArquivoIntegracaoBridgestoneService(ArquivoIntegracaoBridgestoneService arquivoIntegracaoBridgestoneService) {this.arquivoIntegracaoBridgestoneService = arquivoIntegracaoBridgestoneService;}
	
	public ModelAndView downloadZip(WebRequestContext request){
		String whereIn = request.getParameter("whereIn");
		
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado.");
			return new ModelAndView("redirect:/sistema/crud/ArquivoBridgestone");
		}
		
		List<Arquivo> listaArquivos = arquivoIntegracaoBridgestoneService.findForDownloadZip(whereIn);
		
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(byteArrayOutputStream);
		ZipEntry zipEntry;
		try{
			for(Arquivo arquivo : listaArquivos){
				if(arquivo != null){
					zipEntry = new ZipEntry(arquivo.getName());
					zipEntry.setSize(arquivo.getSize());
					zip.putNextEntry(zipEntry);
					zip.write(arquivo.getContent());
				}
			}
			
			zip.closeEntry();
			zip.close();
			
			Resource resource = new Resource("application/zip", "bridgestone_" + SinedUtil.datePatternForReport() + ".zip", byteArrayOutputStream.toByteArray());
			return new ResourceModelAndView(resource);
		}catch (Exception e) {
			request.addError("Erro no download do arquivo: "+e.getMessage());
			return new ModelAndView("redirect:/sistema/crud/ArquivoBridgestone");
		}
	}
}
