package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.io.IOException;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Propostacaixa;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.PropostaService;
import br.com.linkcom.sined.geral.service.PropostacaixaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PropostacaixaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/PropostaCaixa",authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "numeroPropostaString", "clientesPropostaString", "descricaoPropostaString"})
public class PropostacaixaCrud extends CrudControllerSined<PropostacaixaFiltro, Propostacaixa, Propostacaixa>{
	
	private PropostaService propostaService;
	private PropostacaixaService propostacaixaService;
	private ArquivoService arquivoService;
	private ReportTemplateService reportTemplateService;
	
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}
	public void setPropostaService(PropostaService propostaService) {
		this.propostaService = propostaService;
	}
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	public void setPropostacaixaService(PropostacaixaService propostacaixaService) {
		this.propostacaixaService = propostacaixaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Propostacaixa form) throws Exception {
		request.setAttribute("listaTemplate", reportTemplateService.loadTemplatesByCategoria(EnumCategoriaReportTemplate.OPORTUNIDADE));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Propostacaixa bean) throws Exception {

		try{
			bean.setNome(bean.getNome().toUpperCase());
			if (bean.getArquivo() != null) {
				arquivoService.saveOrUpdate(bean.getArquivo());
			}
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_PROPOSTACAIXA_NOME")){
				throw new SinedException("Caixa de proposta j� cadastrada no sistema.");
			} else if(DatabaseError.isKeyPresent(e, "fk_oportunidadeitem_1")){
				throw new SinedException("Os campos adicionais n�o podem ser exclu�dos, j� possui refer�ncias em outros registros do sistema.");
			} else {
				throw e;
			}
		}
	}
	
	@Override
	protected ListagemResult<Propostacaixa> getLista(WebRequestContext request, PropostacaixaFiltro filtro) {
	
		ListagemResult<Propostacaixa> lista = super.getLista(request, filtro);
		List<Propostacaixa> list = lista.list();
		
		for (Propostacaixa propostacaixa : list) {
			propostacaixa.setListaProposta(propostaService.findByPropostacaixa(propostacaixa));
		}
		
		return lista;
	}
	
	@Override
	public ModelAndView doExportar(WebRequestContext request,
			PropostacaixaFiltro filtro) throws CrudException, IOException {
		Resource resource = propostacaixaService.gerarCSV(filtro);
		return new ResourceModelAndView(resource);
	}
}
