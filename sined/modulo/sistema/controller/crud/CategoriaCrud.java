package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CategoriaFiltro;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path={"/sistema/crud/Categoria"},
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = {"vcategoria.identificador", "nome"})
public class CategoriaCrud extends CrudControllerSined<CategoriaFiltro, Categoria, Categoria>{

	private CategoriaService categoriaService;
	private EmpresaService empresaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Categoria form) throws Exception {
		if (form.getCdcategoria() == null) {
			form.setFormato("00");
		}
		
		Empresa empresa = empresaService.loadPrincipal();
		request.setAttribute("isLinkcom", empresa.getCnpj().equals(new Cnpj(ControleAcessoUtil.CNPJLINKCOM)));
	}

	@Override
	protected void salvar(WebRequestContext request, Categoria bean)throws Exception {
		try{
			categoriaService.preparaParaSalvar(bean);
			if(!verificaPai(bean))
				throw new SinedException("Uma categoria n�o pode ser pai dela mesma.");
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_CATEGORIA_NOME")){
				throw new SinedException("Categoria j� cadastrada no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Categoria bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Categoria n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	public boolean verificaPai(Categoria bean){
		boolean retorno = true;
		if(bean.getCategoriapai() != null && bean.getCdcategoria() == bean.getCategoriapai().getCdcategoria())
			retorno = false;
		return retorno;
	}
}
