package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Exameclinica;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExameclinicaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Exameclinica", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "telefone"})
public class ExameclinicaCrud extends CrudControllerSined<ExameclinicaFiltro, Exameclinica, Exameclinica> {
	
	@Override
	protected void listagem(WebRequestContext request, ExameclinicaFiltro filtro)
			throws Exception {
	
		super.listagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Exameclinica bean)
			throws Exception {
	
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_exameclinica_nome")) 
				throw new SinedException("Cl�nica j� cadastrada no sistema.");
			if (DatabaseError.isKeyPresent(e, "idx_exameconvclin_unique")) 
				throw new SinedException("N�o � permitido cadastrar Conv�nios repetidos.");
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Exameclinica form)
			throws Exception {
		form.getLogotipo();
		super.entrada(request, form);
	}
}

