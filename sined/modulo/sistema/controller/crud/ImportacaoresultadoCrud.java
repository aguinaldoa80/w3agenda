	package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ResourceModelAndView;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Arquivo;
import br.com.linkcom.sined.geral.bean.Importacaoresultado;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.ImportacaoresultadoService;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path={"/sistema/crud/Importacaoresultado"}, authorizationModule=CrudAuthorizationModule.class)
public class ImportacaoresultadoCrud extends CrudControllerSined<FiltroListagemSined, Importacaoresultado, Importacaoresultado>{

	private ImportacaoresultadoService importacaoresultadoService;
	private ArquivoService arquivoService;
	
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public void setImportacaoresultadoService(
			ImportacaoresultadoService importacaoresultadoService) {
		this.importacaoresultadoService = importacaoresultadoService;
	}
	
	public ModelAndView imprimeRelatorioResultado(WebRequestContext request, Importacaoresultado importacaoresultado){
		importacaoresultado = importacaoresultadoService.loadForEntrada(importacaoresultado);
		Arquivo arquivo = arquivoService.loadWithContents(importacaoresultado.getArquivorelatorioresultado());
		return new ResourceModelAndView(new Resource(arquivo.getContenttype(), arquivo.getNome(), arquivo.getContent()));
	}
	
}
