package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Sindicato;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.SindicatoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Sindicato", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "codigofolha", "ativo"})
public class SindicatoCrud extends CrudControllerSined<SindicatoFiltro, Sindicato, Sindicato> {

	@Override
	protected Sindicato criar(WebRequestContext request, Sindicato bean) throws Exception {
		bean = super.criar(request, bean);
		bean.setAtivo(true);
		return bean;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Sindicato bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_SINDICATO_NOME")) {
				throw new SinedException("Sindicato j� cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "IDX_SINDICATO_CODIGOFOLHA")) {
				throw new SinedException("C�digo Folha j� cadastrado no sistema.");
			}
		}
	}
	

	
}
