package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Tabelaimposto;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TabelaimpostoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Tabelaimposto"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "arquivo", "versao", "chave", "dtimportacao", "dtvigenciainicio", "dtvigenciafim"})
public class TabelaimpostoCrud extends CrudControllerSined<TabelaimpostoFiltro, Tabelaimposto, Tabelaimposto> {	
	
	@Override
	protected void entrada(WebRequestContext request, Tabelaimposto form) throws Exception {
		throw new SinedException("A��o n�o permitida");
	}
	
}
