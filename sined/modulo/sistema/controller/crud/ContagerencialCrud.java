package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.sined.geral.bean.Agendamento;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contrato;
import br.com.linkcom.sined.geral.bean.Tipooperacao;
import br.com.linkcom.sined.geral.bean.enumeration.NaturezaContagerencial;
import br.com.linkcom.sined.geral.bean.enumeration.SituacaoContrato;
import br.com.linkcom.sined.geral.service.AgendamentoService;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.ContratoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContagerencialFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Contagerencial", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"vcontagerencial.identificador", "formato", "nome", "tipooperacao", "ativo", "vcontagerencial.nivel"})
public class ContagerencialCrud extends CrudControllerSined<ContagerencialFiltro, Contagerencial, Contagerencial> {

	private ContagerencialService contagerencialService;
	private ContratoService contratoService;
	private AgendamentoService agendamentoService;
	
	public void setContagerencialService(ContagerencialService contagerencialService) {	this.contagerencialService = contagerencialService;	}
	public void setContratoService(ContratoService contratoService) {this.contratoService = contratoService;	}
	public void setAgendamentoService(AgendamentoService agendamentoService) {this.agendamentoService = agendamentoService;	}
	
	@Override
	protected void salvar(WebRequestContext request, Contagerencial bean) throws Exception {
		
		contagerencialService.preparaBeanParaSalvar(bean);
		
		try {
			bean.setNome(bean.getNome().replaceAll("\"", "''"));
			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CONTAGERENCIAL_NOME")) {
				throw new SinedException("Conta gerencial j� cadastrada no sistema.");
			}else {
				throw new SinedException(e.getMessage());
			}
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Contagerencial form)throws Exception {
		
		String stCopiar = request.getParameter("copiar");
		if (StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")) {
			
			Contagerencial contagerencialpai = new Contagerencial();
			contagerencialpai.setCdcontagerencial(Integer.parseInt(request.getParameter("cdcontagerencial")));
			form.setContagerencialpai(contagerencialpai);

			
			//Atributos a serem modificados.
			form.setCdcontagerencial(null);
			form.setNome(null);
			form.setTipooperacao(form.getContagerencialpai().getTipooperacao());
			
		} else {
			if (form.getPai() != null) {
				form.setContagerencialpai(form.getPai());
			}
			
			if (StringUtils.isNotEmpty(request.getParameter("cdtipooperacao"))) {
				Integer cdtipooperacao = null;
				try{
					cdtipooperacao = Integer.parseInt(request.getParameter("cdtipooperacao"));
				} catch (NumberFormatException e) {
					throw new SinedException("Tipo opera��o inv�lido.");
				}
				Tipooperacao tipooperacaoobj = null;
				if (cdtipooperacao.equals(Tipooperacao.TIPO_CREDITO.getCdtipooperacao())) {
					tipooperacaoobj = new Tipooperacao(cdtipooperacao,"Cr�dito");
				} else {
					tipooperacaoobj = new Tipooperacao(cdtipooperacao,"D�bito");
				}
				form.setTipooperacao2(tipooperacaoobj);
				List<Contagerencial> lista = contagerencialService.findByTipooperacao(tipooperacaoobj);
				
				request.setAttribute("listaConta", lista);
				
			}
			
			if (form.getTipooperacao2() != null) {
				form.setTipooperacao(form.getTipooperacao2());
			}
		}
		
		if (form.getCdcontagerencial() != null) {
			request.setAttribute("insert", Boolean.FALSE);
		} else {
			request.setAttribute("insert", Boolean.TRUE);
		}
		
		List<NaturezaContagerencial> listaNaturezaContagerencial = Arrays.asList(NaturezaContagerencial.values());
		
		Collections.sort(listaNaturezaContagerencial, new Comparator<NaturezaContagerencial>() {
			@Override
			public int compare(NaturezaContagerencial o1, NaturezaContagerencial o2) {
				return o1.getCdsped().compareToIgnoreCase(o2.getCdsped());
			}
		});
		
		request.setAttribute("listaNaturezaContagerencial", listaNaturezaContagerencial);
		request.setAttribute("listaTipooperacao", Tipooperacao.getListaTipooperacao(false));
	}
	
	@Override
	protected void listagem(WebRequestContext request, ContagerencialFiltro filtro) throws Exception {
		request.setAttribute("listaTipooperacao", Tipooperacao.getListaTipooperacao(false));
		super.listagem(request, filtro);
	}

	
	/**
	 * Carregamento do n�vel via ajax para a tela de cadastro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#findPais
	 * @see br.com.linkcom.sined.geral.bean.Contagerencial#getNivel
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxNivel(WebRequestContext request, Contagerencial bean) {
		contagerencialService.ajaxNivel(request, bean);
	}

	/**
	 * Carregamento do tipo de opera��o via ajax  para a tela de cadastro.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#carregaConta
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxOperacao(WebRequestContext request, Contagerencial bean) {
		contagerencialService.ajaxOperacao(request, bean);
	}
	
	/**
	 * Carrega o campo transient boolean para fazer a confirma��o de update no banco.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ContagerencialService#isAnalitica
	 * @param request
	 * @param bean
	 * @author Rodrigo Freitas
	 */
	public void ajaxAnalitica(WebRequestContext request, Contagerencial bean) {
		contagerencialService.ajaxAnalitica(request, bean);
	}
	
	public void ajaxItem(WebRequestContext request, Contagerencial bean) {
		contagerencialService.ajaxItem(request, bean);
	}
	
	@Override
	protected void validateBean(Contagerencial bean, BindException errors) {
		
		if (bean.getItem()!=null && !contagerencialService.verificarItem(bean)){
			errors.reject("001", "J� possui conta gerencial com este item cadastrado");
		}
		
		if(bean.getAtivo() == null || !bean.getAtivo()){
			
			List<GenericBean> listaResultados = contagerencialService.ocorrenciaContaGerencial(bean.getCdcontagerencial());
			List<Contrato> listacontrato = contratoService.findByContaGerencialValidacao(bean);
			List<Agendamento> listaAgendamento = agendamentoService.findByContaGerencialValidacao(bean);
			StringBuilder mensagem = new StringBuilder();
			
			if(SinedUtil.isListNotEmpty(listaResultados)){				
				mensagem.append("Esta conta gerencial est� sendo utilizado nas configura��es de : ");
				for (GenericBean genericBean : listaResultados) {
					mensagem.append("<br>" + genericBean.getId() +" no campo " + genericBean.getValue());  
				}
				mensagem.append(". <br>Altere o(s) cadastro(s) da configura��o(�es) antes de inativar esta conta gerencial.<br><br>");
			
			}
			if(SinedUtil.isListNotEmpty(listacontrato)){
				Integer cont = 0;
				mensagem.append("Esta conta gerencial est� sendo utilizada no(s) contrato(s) : ");
				for (Contrato contrato : listacontrato) {
					if(contrato.getAux_contrato() != null
						&& contrato.getAux_contrato().getSituacao() != null 
						&& !SituacaoContrato.FINALIZADO.equals(contrato.getAux_contrato().getSituacao())){
					
					mensagem.append(contrato.getIdentificadorOrCdcontrato());
					if(cont >= 0)
						mensagem.append(",");
					if(cont % 28 == 0 && cont != 0)
						mensagem.append("<BR>");
					cont++;
				}
			}
				mensagem.delete(mensagem.length() - 1, mensagem.length());
				mensagem.append(". <BR>Altere o rateio do(s) contrato(s) antes de inativar esta conta gerencial.<BR><BR>");
			}
			if(SinedUtil.isListNotEmpty(listaAgendamento)){
				Integer cont = 0;
				mensagem.append("Esta conta gerencial est� sendo utilizada no(s) agendamento(s) : ");
				for (Agendamento agendamento : listaAgendamento) {
				if(agendamento.getAux_agendamento() != null
						&& agendamento.getAux_agendamento() != null
						&& 5 != agendamento.getAux_agendamento().getSituacao()){
					
					mensagem.append(agendamento.getCdagendamento());
					if(cont >= 0)
						mensagem.append(",");
					if(cont % 28 == 0 && cont != 0)
						mensagem.append("<BR>");
					cont++;
				}
			}
				mensagem.delete(mensagem.length() - 1, mensagem.length());
				mensagem.append(".<BR> Altere o rateio do(s) agendamento(s) antes de inativar esta conta gerencial.");
			}
			if(mensagem.length() > 0)
				errors.reject("002", mensagem.toString());
			}
			super.validateBean(bean, errors);
	}
	
	public void ajaxBuscaNaturezaContagerencial(WebRequestContext request, Contagerencial contagerencial){
		contagerencialService.ajaxBuscaNaturezaContagerencial(request, contagerencial);
	}
}