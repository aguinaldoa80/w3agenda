package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path={"/sistema/crud/Pneumarca"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "codigointegracao"})
public class PneumarcaCrud extends CrudControllerSined<FiltroListagemSined, Pneumarca, Pneumarca> {	
	
	@Override
	protected void salvar(WebRequestContext request, Pneumarca bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PNEUMARCA_NOME")) {
				throw new SinedException("Marca de pneu j� cadastrada no sistema.");
			} else throw e;
		}
	}
}
