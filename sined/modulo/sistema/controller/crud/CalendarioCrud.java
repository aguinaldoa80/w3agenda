package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Calendario;
import br.com.linkcom.sined.geral.bean.Calendarioitem;
import br.com.linkcom.sined.geral.bean.Projeto;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.CalendarioService;
import br.com.linkcom.sined.geral.service.CalendarioorcamentoService;
import br.com.linkcom.sined.geral.service.CalendarioprojetoService;
import br.com.linkcom.sined.geral.service.OrcamentoService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CalendarioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Calendario"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "projetosString", "orcamentosString"})
public class CalendarioCrud extends CrudControllerSined<CalendarioFiltro, Calendario, Calendario> {
	
	private CalendarioService calendarioService;
	private OrcamentoService orcamentoService;
	private ProjetoService projetoService;
	private CalendarioorcamentoService calendarioorcamentoService;
	private CalendarioprojetoService calendarioprojetoService;
	
	public void setCalendarioprojetoService(CalendarioprojetoService calendarioprojetoService) {
		this.calendarioprojetoService = calendarioprojetoService;
	}
	public void setCalendarioorcamentoService(CalendarioorcamentoService calendarioorcamentoService) {
		this.calendarioorcamentoService = calendarioorcamentoService;
	}
	public void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public void setOrcamentoService(OrcamentoService orcamentoService) {
		this.orcamentoService = orcamentoService;
	}
	public void setCalendarioService(CalendarioService calendarioService) {
		this.calendarioService = calendarioService;
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Calendario bean) {
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Calendario form) throws Exception {
		
		List<Projeto> listaProjeto = new ListSet<Projeto>(Projeto.class);
		if(form.getCdcalendario() != null && form.getListaCalendarioprojeto() != null &&  form.getListaCalendarioprojeto().size() > 0){
			List<Projeto> projetos = calendarioprojetoService.getProjetos(form.getListaCalendarioprojeto());
			listaProjeto = projetoService.findProjetosAbertosSemPermissao(projetos);
		} else {
			listaProjeto = projetoService.findProjetosAbertos(null);
		}
		request.setAttribute("listaProjeto", listaProjeto);
		

		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Calendario bean) throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CALENDARIO_DESCRICAO")) {throw new SinedException("Calend�rio j� cadastrado no sistema.");}	
		}
	}
	
	@Override
	protected ListagemResult<Calendario> getLista(WebRequestContext request,
			CalendarioFiltro filtro) {
		ListagemResult<Calendario> listagemResult = super.getLista(request, filtro);
		List<Calendario> list = listagemResult.list();
		
		String whereIn = CollectionsUtil.listAndConcatenate(list, "cdcalendario", ",");
		if(whereIn != null && !whereIn.equals("")){
			list.removeAll(list);
			list.addAll(calendarioService.loadWithLista(whereIn,filtro.getOrderBy(),filtro.isAsc()));
		}
		
		return listagemResult;
	}
	
	/**
	 * M�todo para copiar a lista de feriados do calendario selecionado.
	 * 
	 * @see br.com.linkcom.sined.modulo.sistema.controller.crud.CalendarioCrud#verificaListaItem
	 * @param request
	 * @param calendario
	 * @return
	 * @author Rodrigo Freitas
	 */
	public ModelAndView carregaCalendario(WebRequestContext request, Calendario calendario){
		
		if(calendario.getProjetotrans() == null || calendario.getProjetotrans().getCdprojeto() == null){
			throw new SinedException("Erro na passagem de par�metros.");
		}
		
		Calendario antigo = calendarioService.findWithLista(calendario.getProjetotrans());
		
		if(antigo != null){
			List<Calendarioitem> lista = calendario.getListaCalendarioitem();
			
			if(antigo.getListaCalendarioitem() != null){
				for (Calendarioitem item : antigo.getListaCalendarioitem()) {
					item.setCdcalendarioitem(null);
					if(!this.verificaListaItem(item, lista)){
						lista.add(item);
					}
				}
			}
		}
		
		calendario.setProjetotrans(null);
		
		return continueOnAction(ENTRADA, calendario);
	}

	/**
	 * Verifica se a data j� existe na lista de feriados.
	 *
	 * @param item
	 * @param lista
	 * @return
	 * @author Rodrigo Freitas
	 */
	private boolean verificaListaItem(Calendarioitem item, List<Calendarioitem> lista) {
		boolean achou = false;
		
		for (Calendarioitem ci : lista) {
			if(ci.getDtferiado().equals(item.getDtferiado())){
				achou = true;
				break;
			}
		}
		
		return achou;
	}
	
	@Override
	protected void validateBean(Calendario bean, BindException errors) {
		
		if(bean.getListaCalendarioorcamento() != null){
			boolean bool = false;
			for (int i = 0; i < bean.getListaCalendarioorcamento().size(); i++) {
				if(calendarioorcamentoService.verificaOutroCalendario(bean.getListaCalendarioorcamento().get(i).getOrcamento(), bean)){
					errors.reject("001", "Or�amento "+ orcamentoService.load(bean.getListaCalendarioorcamento().get(i).getOrcamento(), "orcamento.nome") +" j� est� cadastrado em outro calend�rio.");
				}
				for (int j = 0; j < bean.getListaCalendarioorcamento().size(); j++) {
					if(i != j && bean.getListaCalendarioorcamento().get(i).getOrcamento().getCdorcamento().equals(bean.getListaCalendarioorcamento().get(j).getOrcamento().getCdorcamento())){
						errors.reject("001", "Or�amento "+ orcamentoService.load(bean.getListaCalendarioorcamento().get(i).getOrcamento(), "orcamento.nome") +" j� est� cadastrado neste calend�rio.");
						bool = true;
						break;
					}
				}
				if(bool) break;
			}
		}
		
		if(bean.getListaCalendarioprojeto() != null){
			boolean bool2 = false;
			for (int i = 0; i < bean.getListaCalendarioprojeto().size(); i++) {
				if(calendarioprojetoService.verificaOutroCalendario(bean.getListaCalendarioprojeto().get(i).getProjeto(), bean)){
					errors.reject("001", "Projeto "+ projetoService.load(bean.getListaCalendarioprojeto().get(i).getProjeto(), "projeto.nome") +" j� est� cadastrado em outro calend�rio.");
				}
				for (int j = 0; j < bean.getListaCalendarioprojeto().size(); j++) {
					if(i != j && bean.getListaCalendarioprojeto().get(i).getProjeto().getCdprojeto().equals(bean.getListaCalendarioprojeto().get(j).getProjeto().getCdprojeto())){
						errors.reject("001", "Projeto "+ projetoService.load(bean.getListaCalendarioprojeto().get(i).getProjeto(), "projeto.nome") +" j� est� cadastrado neste calend�rio.");
						bool2 = true;
						break;
					}
				}
				if(bool2) break;
			}
		}
	}
	
}
