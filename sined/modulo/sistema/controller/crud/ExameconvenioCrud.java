package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Exameconvenio;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ExameconvenioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Exameconvenio", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class ExameconvenioCrud extends CrudControllerSined<ExameconvenioFiltro, Exameconvenio, Exameconvenio>{
	
	@Override
	protected void listagem(WebRequestContext request, ExameconvenioFiltro filtro) throws Exception {
	
		super.listagem(request, filtro);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Exameconvenio bean)
			throws Exception {
	
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_exameconvenio_nome")) 
				throw new SinedException("Conv�nio j� cadastrado no sistema.");	
		}
	}

	@Override
	protected void entrada(WebRequestContext request, Exameconvenio form) throws Exception {
		form.getLogotipo();
		super.entrada(request, form);
	}

}
