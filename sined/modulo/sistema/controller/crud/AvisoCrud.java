package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;
import java.text.ParseException;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Aviso;
import br.com.linkcom.sined.geral.bean.Avisousuario;
import br.com.linkcom.sined.geral.bean.Motivoaviso;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.AvisoOrigem;
import br.com.linkcom.sined.geral.bean.enumeration.MotivoavisoEnum;
import br.com.linkcom.sined.geral.service.AvisoService;
import br.com.linkcom.sined.geral.service.AvisousuarioService;
import br.com.linkcom.sined.geral.service.MotivoavisoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AvisoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Aviso", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"assunto", "complemento", "dtaviso", "tipoaviso", "papel", "usuario", "motivoaviso"})
public class AvisoCrud extends CrudControllerSined<AvisoFiltro, Aviso, Aviso> {

	private UsuarioService usuarioService;
	private AvisoService avisoService;
	private AvisousuarioService avisousuarioService;
	private MotivoavisoService motivoavisoService;
	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public void setAvisoService(AvisoService avisoService) {
		this.avisoService = avisoService;
	}
	
	public void setAvisousuarioService(AvisousuarioService avisousuarioService) {
		this.avisousuarioService = avisousuarioService;
	}
	
	public void setMotivoavisoService(MotivoavisoService motivoavisoService) {
		this.motivoavisoService = motivoavisoService;
	}

	@Override
	protected ListagemResult<Aviso> getLista(WebRequestContext request,	AvisoFiltro filtro) {
		Boolean mostrarTodosAvisos = (Boolean) request.getSession().getAttribute("mostrarTodosAvisos");
		if(mostrarTodosAvisos != null && mostrarTodosAvisos){
			request.getSession().removeAttribute("mostrarTodosAvisos");
			filtro.setUsuario(SinedUtil.getUsuarioLogado().getNome());
			filtro.setTipoaviso(null);
			filtro.setLido(false);
			filtro.setAssunto(null);
			filtro.setComplemento(null);
			filtro.setDtavisoate(null);
			filtro.setDtavisode(null);
			filtro.setAvisoOrigem(null);
			filtro.setMotivoaviso(null);
		}
		return super.getLista(request, filtro);
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, AvisoFiltro filtro) throws CrudException {
		if (filtro.getNotificacao() != null && filtro.getNotificacao()) {
			filtro.setAssunto(null);
			filtro.setComplemento(null);
			filtro.setDtavisoate(null);
			filtro.setDtavisode(null);
			filtro.setTipoaviso(null);
			filtro.setLido(false);
			filtro.setAvisoOrigem(null);
			
			if (filtro.getCdmotivoaviso() != null) {
				Motivoaviso m = motivoavisoService.load(new Motivoaviso(filtro.getCdmotivoaviso()));
				if (m != null) {
					filtro.setMotivoaviso(m);
				}
			}
			
			if (filtro.getCdusuario() != null) {
				filtro.setUsuario(usuarioService.load(new Usuario(filtro.getCdusuario())).getNome());
			}
		}
		
		return super.doListagem(request, filtro);
	}
	
	@Override
	protected void listagem(WebRequestContext request, AvisoFiltro filtro) throws Exception {		
		Boolean mostrarTodosAvisos = (Boolean) request.getSession().getAttribute("mostrarTodosAvisos");
		if(mostrarTodosAvisos != null && mostrarTodosAvisos){
			request.getSession().removeAttribute("mostrarTodosAvisos");
			filtro.setUsuario(SinedUtil.getUsuarioLogado().getNome());
			filtro.setTipoaviso(null);
			filtro.setLido(null);
		}
		request.setAttribute("listaAvisoOrigem", AvisoOrigem.getListaOrdenada());
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Aviso form) throws Exception {
		request.setAttribute("usuariosAtivos", usuarioService.findUsuariosDesbloqueados(form != null ? form.getUsuario() : null));
		request.setAttribute("listaOrigem", avisoService.montaDestino(form));
		
		if (form.getCdaviso() == null) {
			form.setAvisoOrigem(AvisoOrigem.CADASTRO);
			form.setMotivoaviso(motivoavisoService.findByMotivo(MotivoavisoEnum.CADASTRO_AVISO));
		}
		
		if(form.getCdaviso() != null && SinedUtil.isListNotEmpty(form.getListaAvisousuario())){
			for(Avisousuario avisousuario : form.getListaAvisousuario()){
				if(avisousuario.getCdavisousuario() != null && avisousuario.getDtleitura() == null && avisousuario.getUsuario() != null && avisousuario.getUsuario().equals(SinedUtil.getUsuarioLogado())){
					avisousuarioService.updateLido(avisousuario);
					avisousuario.setDtleitura(new Date(System.currentTimeMillis()));
					break;
				}
			}
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Aviso bean) throws Exception {
		boolean criar = bean.getCdaviso() == null;
		boolean atualizarUsuarios = true;
		if(!criar && avisousuarioService.existeUsuarioNotificado(bean)){
			atualizarUsuarios = false;
			Aviso aviso = avisoService.loadForEntrada(bean);
			if(aviso != null && (
					(  (aviso.getTipoaviso() == null && bean.getTipoaviso() != null) || (aviso.getTipoaviso() != null && bean.getTipoaviso() == null) || 
					   (aviso.getTipoaviso() != null && bean.getTipoaviso() != null && 
						aviso.getTipoaviso().getCdtipoaviso() != null && bean.getTipoaviso().getCdtipoaviso() != null &&
						!aviso.getTipoaviso().getCdtipoaviso().equals(bean.getTipoaviso().getCdtipoaviso()))
					)
					||
					(	(aviso.getPapel() == null && bean.getPapel() != null) || (aviso.getPapel() != null && bean.getPapel() == null) || 
					    (aviso.getPapel() != null && bean.getPapel() != null &&
						aviso.getPapel().getCdpapel() != null && bean.getPapel().getCdpapel() != null && 
						!aviso.getPapel().getCdpapel().equals(bean.getPapel().getCdpapel()))
					)
					||
					(	(aviso.getUsuario() == null && bean.getUsuario() != null) || (aviso.getUsuario() != null && bean.getUsuario() == null) || 
					    (aviso.getUsuario() != null && bean.getUsuario() != null &&
						!aviso.getUsuario().equals(bean.getUsuario()))
					))){
				throw new SinedException("N�o � poss�vel alterar os usu�rios. Existe usu�rio j� notificado.");
			}
		}
		
		
		super.salvar(request, bean);
		
		if(criar || atualizarUsuarios){
			if(!criar){
				avisousuarioService.deleteAllFromAviso(bean);
			}
			avisoService.incluirESalvarAvisoUsuario(bean);
		}
	}
	
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		Boolean notificacao = Boolean.parseBoolean(NeoWeb.getRequestContext().getParameter("notificacao"));		
		Boolean mostrarTodosAvisos = (Boolean) NeoWeb.getRequestContext().getSession().getAttribute("mostrarTodosAvisos");
		if(notificacao || (mostrarTodosAvisos != null && mostrarTodosAvisos)){
			return false;
		}
		return true;
	}
	
	public ModelAndView marcarTodosLido(WebRequestContext request){
		Boolean entrada = Boolean.valueOf(request.getParameter("entrada") != null ? request.getParameter("entrada") : "false");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(StringUtils.isNotBlank(whereIn)){
			avisousuarioService.updateLidoTodos(SinedUtil.getUsuarioLogado(), whereIn);
			request.addMessage("Aviso(s) marcado(s) como lido(s).");
		}
		SinedUtil.redirecionamento(request, "/sistema/crud/Aviso" + (entrada ? "?ACAO=consultar&cdaviso="+whereIn : ""));
		return null;
	}
	
	public void mostrarTodosAvisos(WebRequestContext request){
		request.getSession().setAttribute("mostrarTodosAvisos", Boolean.TRUE);
		SinedUtil.redirecionamento(request, "/sistema/crud/Aviso");
	}
	
	public void executarAvisoDiarioTeste(WebRequestContext request) throws ParseException{
		if(SinedUtil.isAmbienteDesenvolvimento()){
			avisoService.criarAvisosControladosPorDia();
			SinedUtil.redirecionamento(request, "/sistema/crud/Aviso");
		}
	}
}
