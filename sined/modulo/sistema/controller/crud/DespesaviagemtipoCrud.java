package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Despesaviagemtipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DespesaviagemtipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Despesaviagemtipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class DespesaviagemtipoCrud extends CrudControllerSined<DespesaviagemtipoFiltro, Despesaviagemtipo, Despesaviagemtipo>{

	@Override
	protected void salvar(WebRequestContext request, Despesaviagemtipo bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_DESPESAVIAGEMTIPO_NOME")){
				throw new SinedException("Tipo de despesa j� cadastrado no sistema.");
			}
		}
	}
}
