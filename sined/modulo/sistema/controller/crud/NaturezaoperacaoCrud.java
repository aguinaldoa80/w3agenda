package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.MessageType;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Naturezaoperacao;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaocfopService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaocstService;
import br.com.linkcom.sined.geral.service.OperacaocontabilService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.NaturezaoperacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Naturezaoperacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "descricao", "codigonfse", "operacaocontabilavista", "operacaocontabilaprazo", "operacaocontabilpagamento", "notaTipo", "padrao", "ativo"})
public class NaturezaoperacaoCrud extends CrudControllerSined<NaturezaoperacaoFiltro, Naturezaoperacao, Naturezaoperacao> {
	
	private NaturezaoperacaoService naturezaoperacaoService;
	private OperacaocontabilService operacaocontabilService;
	private NaturezaoperacaocfopService naturezaoperacaocfopService;
	private NaturezaoperacaocstService naturezaoperacaocstService;
	private ReportTemplateService reportTemplateService;

	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {
		this.naturezaoperacaoService = naturezaoperacaoService;
	}

	public void setOperacaocontabilService(OperacaocontabilService operacaocontabilService) {
		this.operacaocontabilService = operacaocontabilService;
	}

	public void setNaturezaoperacaocfopService(NaturezaoperacaocfopService naturezaoperacaocfopService) {
		this.naturezaoperacaocfopService = naturezaoperacaocfopService;
	}

	public void setNaturezaoperacaocstService(NaturezaoperacaocstService naturezaoperacaocstService) {
		this.naturezaoperacaocstService = naturezaoperacaocstService;
	}

	public void setReportTemplateService(ReportTemplateService reportTemplateService) {
		this.reportTemplateService = reportTemplateService;
	}

	@Override
	protected void listagem(WebRequestContext request, NaturezaoperacaoFiltro filtro) throws Exception {

		List<NotaTipo> listaNotaTipo = new ArrayList<NotaTipo>();
		listaNotaTipo.add(NotaTipo.NOTA_FISCAL_SERVICO);
		listaNotaTipo.add(NotaTipo.NOTA_FISCAL_PRODUTO);
		listaNotaTipo.add(NotaTipo.ENTRADA_FISCAL);

		request.setAttribute("listaNotaTipo", listaNotaTipo);

		super.listagem(request, filtro);
	}

	@Override
	protected void entrada(WebRequestContext request, Naturezaoperacao form) throws Exception {
		if (form.getCdnaturezaoperacao() != null) {
			if (form.getOperacaocontabilavista() != null && form.getOperacaocontabilavista().getCdoperacaocontabil() != null) {
				form.setOperacaocontabilavista(operacaocontabilService.load(form.getOperacaocontabilavista(),
						"operacaocontabil.cdoperacaocontabil, operacaocontabil.nome"));
			}
			if (form.getOperacaocontabilaprazo() != null && form.getOperacaocontabilaprazo().getCdoperacaocontabil() != null) {
				form.setOperacaocontabilaprazo(operacaocontabilService.load(form.getOperacaocontabilaprazo(),
						"operacaocontabil.cdoperacaocontabil, operacaocontabil.nome"));
			}
			if (form.getOperacaocontabilpagamento() != null
					&& form.getOperacaocontabilpagamento().getCdoperacaocontabil() != null) {
				form.setOperacaocontabilpagamento(operacaocontabilService.load(form.getOperacaocontabilpagamento(),
						"operacaocontabil.cdoperacaocontabil, operacaocontabil.nome"));
			}
			form.setListaNaturezaoperacaocfop(naturezaoperacaocfopService.find(form, null));
			form.setListaNaturezaoperacaocst(naturezaoperacaocstService.findByNaturezaoperacao(form));
		}
		
		if(StringUtils.isNotBlank(request.getParameter("codigonfse")))
			form.setCodigonfse(request.getParameter("codigonfse"));

		if(StringUtils.isNotBlank(request.getParameter("nome")))
			form.setNome(request.getParameter("nome"));

		
		if(StringUtils.isNotBlank(request.getParameter("descricao")))
			form.setDescricao(request.getParameter("descricao"));

		
		List<NotaTipo> listaNotaTipo = new ArrayList<NotaTipo>();
		listaNotaTipo.add(NotaTipo.NOTA_FISCAL_SERVICO);
		listaNotaTipo.add(NotaTipo.NOTA_FISCAL_PRODUTO);
		listaNotaTipo.add(NotaTipo.ENTRADA_FISCAL);

		request.setAttribute("listaNotaTipo", listaNotaTipo);
		request.setAttribute("listaTemplateinfproduto",
				reportTemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.INFO_ADICIONAIS_PRODUTO));

		super.entrada(request, form);
	}

	@Override
	public ModelAndView doSalvar(WebRequestContext request, Naturezaoperacao form) throws CrudException {
		if (validarSalvar(request, form)) {
			return super.doSalvar(request, form);
		} else {
			return super.doEntrada(request, form);
		}
	}

	@Override
	protected void excluir(WebRequestContext request, Naturezaoperacao bean) throws Exception {
		throw new SinedException("A��o n�o permitida.");
	}

	private boolean validarSalvar(WebRequestContext request, Naturezaoperacao form) {

		Long total = naturezaoperacaoService.getTotalNaturezaoperacaoPadrao(form.getCdnaturezaoperacao(), form.getNotaTipo());

		if (Boolean.TRUE.equals(form.getPadrao()) && total > 0) {
			String tipo = NotaTipo.NOTA_FISCAL_PRODUTO.equals(form.getNotaTipo()) ? "Produto" : NotaTipo.NOTA_FISCAL_SERVICO
					.equals(form.getNotaTipo()) ? "Servi�o" : "Entrada";
			request.addMessage("S� pode ter uma Natureza de Opera��o de " + tipo + " padr�o.", MessageType.ERROR);
			return false;
		}

		return true;
	}
}