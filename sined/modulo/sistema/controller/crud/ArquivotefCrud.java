package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Arquivotef;
import br.com.linkcom.sined.geral.bean.enumeration.Arquivotefsituacao;
import br.com.linkcom.sined.geral.service.ArquivotefhistoricoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ArquivotefFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.neo.DownloadFileServlet;

@Controller(path="/sistema/crud/Arquivotef", authorizationModule=CrudAuthorizationModule.class)
public class ArquivotefCrud extends CrudControllerSined<ArquivotefFiltro, Arquivotef, Arquivotef> {
	
	private ArquivotefhistoricoService arquivotefhistoricoService;
	
	public void setArquivotefhistoricoService(
			ArquivotefhistoricoService arquivotefhistoricoService) {
		this.arquivotefhistoricoService = arquivotefhistoricoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Arquivotef bean) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void excluir(WebRequestContext request, Arquivotef bean) throws Exception {
		if(!SinedUtil.isUsuarioLogadoAdministrador()){
			throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
		} else super.excluir(request, bean);
	}
	
	@Override
	protected Arquivotef criar(WebRequestContext request, Arquivotef form) throws Exception {
		throw new SinedException("N�o � poss�vel acessar esta funcionalidade.");
	}
	
	@Override
	protected void entrada(WebRequestContext request, Arquivotef form) throws Exception {
		form.setListaArquivotefhistorico(arquivotefhistoricoService.findByArquivotef(form));
	}
	
	@Override
	protected void listagem(WebRequestContext request, ArquivotefFiltro filtro) throws Exception {
		List<Arquivotefsituacao> listaSituacao = new ArrayList<Arquivotefsituacao>();
		for (int i = 0; i < Arquivotefsituacao.values().length; i++) {
			listaSituacao.add(Arquivotefsituacao.values()[i]);
		}
		request.setAttribute("listaSituacao", listaSituacao);
	}
	
	@Override
	protected ListagemResult<Arquivotef> getLista(WebRequestContext request, ArquivotefFiltro filtro) {
		ListagemResult<Arquivotef> listagemResult = super.getLista(request, filtro);
		List<Arquivotef> lista = listagemResult.list();
		
		for (Arquivotef arquivotef : lista) {
			if(arquivotef != null && arquivotef.getArquivoenvio() != null && arquivotef.getArquivoenvio().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivotef.getArquivoenvio().getCdarquivo());
			}
			if(arquivotef != null && arquivotef.getArquivoretorno() != null && arquivotef.getArquivoretorno().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivotef.getArquivoretorno().getCdarquivo());
			}
			if(arquivotef != null && arquivotef.getArquivoretornopagamento() != null && arquivotef.getArquivoretornopagamento().getCdarquivo() != null){
				DownloadFileServlet.addCdfile(request.getSession(), arquivotef.getArquivoretornopagamento().getCdarquivo());
			}
		}
		
		return listagemResult;
	}
	
}
