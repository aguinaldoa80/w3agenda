package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Contadigital;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path = "/sistema/crud/Contadigital", authorizationModule = CrudAuthorizationModule.class)
@ExportCSV(fields = { "conta", "apikey", "apisecret", "username", "saldo", "dtsaldo" })
public class ContadigitalCrud extends CrudControllerSined<FiltroListagemSined, Contadigital, Contadigital> {
	
	private ContaService contaService;
	
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Contadigital form) throws Exception {
		request.setAttribute("listaConta", contaService.findByTipo(Contatipo.TIPO_CONTA_BANCARIA));
	}

}
