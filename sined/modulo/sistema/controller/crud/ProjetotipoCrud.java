package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Projetotipo;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@Controller(path={"/sistema/crud/Projetotipo"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class ProjetotipoCrud extends CrudControllerSined<FiltroListagemSined, Projetotipo, Projetotipo> {	
	
}
