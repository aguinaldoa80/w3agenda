package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Garantia;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(
		path="/sistema/crud/Garantia", 
		authorizationModule=CrudAuthorizationModule.class
)
@ExportCSV(fields = "descricao")
public class GarantiaCrud extends CrudControllerSined<GarantiaFiltro, Garantia, Garantia>{

	@Override
	protected void salvar(WebRequestContext request, Garantia bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_GARANTIA_DESCRICAO")){
				throw new SinedException("Garantia j� cadastrada no sistema.");
			}
		}
	}
}
