package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Configuracaognre;
import br.com.linkcom.sined.geral.bean.Configuracaognreuf;
import br.com.linkcom.sined.geral.service.ConfiguracaognreService;
import br.com.linkcom.sined.geral.service.ConfiguracaognrerateioService;
import br.com.linkcom.sined.geral.service.ConfiguracaognreufService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/Configuracaognre", authorizationModule=CrudAuthorizationModule.class)
public class ConfiguracaognreCrud extends CrudControllerSined<FiltroListagemSined, Configuracaognre, Configuracaognre> {
	
	private EmpresaService empresaService;
	private ConfiguracaognreService configuracaognreService;
	private ConfiguracaognrerateioService configuracaognrerateioService;
	private ConfiguracaognreufService configuracaognreufService;
	
	public void setConfiguracaognreService(
			ConfiguracaognreService configuracaognreService) {
		this.configuracaognreService = configuracaognreService;
	}
	public void setConfiguracaognreufService(
			ConfiguracaognreufService configuracaognreufService) {
		this.configuracaognreufService = configuracaognreufService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setConfiguracaognrerateioService(
			ConfiguracaognrerateioService configuracaognrerateioService) {
		this.configuracaognrerateioService = configuracaognrerateioService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Configuracaognre form) throws Exception {
		if(form != null && form.getCdconfiguracaognre() != null){
			form.setListaConfiguracaognrerateio(configuracaognrerateioService.findByConfiguracaognre(form));
			form.setListaConfiguracaognreuf(configuracaognreufService.findByConfiguracaognre(form));
		}
						
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}
	
	@Override
	protected void validateBean(Configuracaognre bean, BindException errors) {
		super.validateBean(bean, errors);
		
		if(configuracaognreService.hasDuplicateByEmpresaAndCodigo(bean)){
			errors.reject("001", "J� existe uma configura��o de GNRE ativa com o mesmo c�digo da receita.");
		}
		
		boolean ufVersaoDuplicada = false;
		List<Configuracaognreuf> listaConfiguracaognreuf = bean.getListaConfiguracaognreuf();
		for (int i = 0; i < listaConfiguracaognreuf.size(); i++) {
			for (int j = 0; j < listaConfiguracaognreuf.size(); j++) {
				if(i == j) continue;
				if(listaConfiguracaognreuf.get(i).getUf().getCduf().equals(listaConfiguracaognreuf.get(j).getUf().getCduf())){
					ufVersaoDuplicada = true;
					break;
				}
			}
			if(ufVersaoDuplicada) break;
		}
		if(ufVersaoDuplicada){
			errors.reject("002", "A mesma UF n�o pode ser atribu�da a dois ou mais itens.");
		}
	}
	
}
