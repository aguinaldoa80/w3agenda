package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Configuracaodre;
import br.com.linkcom.sined.geral.bean.Configuracaodreitem;
import br.com.linkcom.sined.geral.bean.Configuracaodretipo;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.service.ConfiguracaodretipoService;
import br.com.linkcom.sined.geral.service.ContacontabilService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConfiguracaodreFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Configuracaodre"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "ativo"})
public class ConfiguracaodreCrud extends CrudControllerSined<ConfiguracaodreFiltro, Configuracaodre, Configuracaodre>{
	
	private ConfiguracaodretipoService configuracaodretipoService;
	private ContacontabilService contacontabilService;
	
	public void setContacontabilService(
			ContacontabilService contacontabilService) {
		this.contacontabilService = contacontabilService;
	}
	public void setConfiguracaodretipoService(
			ConfiguracaodretipoService configuracaodretipoService) {
		this.configuracaodretipoService = configuracaodretipoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Configuracaodre form) throws Exception {
		List<Configuracaodretipo> listaConfiguracaodretipo = configuracaodretipoService.findAll("configuracaodretipo.ordem");
		Map<Integer, List<Configuracaodreitem>> mapConfiguracaodreitem = new HashMap<Integer, List<Configuracaodreitem>>();
		for (Configuracaodretipo configuracaodretipo : listaConfiguracaodretipo) {
			mapConfiguracaodreitem.put(configuracaodretipo.getCdconfiguracaodretipo(), new ArrayList<Configuracaodreitem>());
		}
		if(request.getBindException().hasErrors()){
			this.mapToListConfiguracaodreitem(form, null);
		}
		
		List<Configuracaodreitem> listaConfiguracaodreitem = form.getListaConfiguracaodreitem();
		for (Configuracaodreitem configuracaodreitem : listaConfiguracaodreitem) {
			if(configuracaodreitem.getConfiguracaodretipo() != null && 
				configuracaodreitem.getConfiguracaodretipo().getCdconfiguracaodretipo() != null){
				mapConfiguracaodreitem.get(configuracaodreitem.getConfiguracaodretipo().getCdconfiguracaodretipo()).add(configuracaodreitem);
			}
		}
		
		request.setAttribute("listaConfiguracaodretipo", listaConfiguracaodretipo);
		request.setAttribute("mapConfiguracaodreitem", mapConfiguracaodreitem);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Configuracaodre bean) throws Exception {
		this.mapToListConfiguracaodreitem(bean, null);
		super.salvar(request, bean);
	}
	
	@Override
	protected void validateBean(Configuracaodre bean, BindException errors) {
		super.validateBean(bean, errors);
		
		List<ContaContabil> listaContaContabil = new ArrayList<ContaContabil>();
		this.mapToListConfiguracaodreitem(bean, listaContaContabil);
		
		List<Configuracaodreitem> listaConfiguracaodreitem = bean.getListaConfiguracaodreitem();
		listaContaContabil = contacontabilService.findContasContabeis(CollectionsUtil.listAndConcatenate(listaContaContabil, "cdcontacontabil", ","));
		for (int i = 0; i < listaConfiguracaodreitem.size(); i++) {
			Configuracaodreitem configuracaodreitem = listaConfiguracaodreitem.get(i);
			ContaContabil contacontabil = listaContaContabil.get(listaContaContabil.indexOf(configuracaodreitem.getContacontabil()));
			String identificador = contacontabil != null && contacontabil.getVcontacontabil() != null ? contacontabil.getVcontacontabil().getIdentificador() : null;
			
			if(identificador != null){
				for (int j = 0; j < listaConfiguracaodreitem.size(); j++) {
					if(j == i) continue;
					
					Configuracaodreitem configuracaodreitem_aux = listaConfiguracaodreitem.get(j);
					ContaContabil contacontabil_aux = listaContaContabil.get(listaContaContabil.indexOf(configuracaodreitem_aux.getContacontabil()));
					String identifcador_aux = contacontabil_aux != null && contacontabil_aux.getVcontacontabil() != null ? contacontabil_aux.getVcontacontabil().getIdentificador() : null;
					if(identifcador_aux != null && identificador.startsWith(identifcador_aux)){
						errors.reject("001", "Conta cont�bil <b style='color: white;'>" + contacontabil.getDescricaoInputPersonalizado() + "</b> em duplicidade com <b style='color: white;'>" + contacontabil_aux.getDescricaoInputPersonalizado() + "</b>.");
					}
				}
			}
		}
	}
	
	private void mapToListConfiguracaodreitem(Configuracaodre bean, List<ContaContabil> listaContaContabil) {
		List<Configuracaodreitem> listaConfiguracaodreitem = new ArrayList<Configuracaodreitem>();
		Map<Integer, List<Configuracaodreitem>> mapConfiguracaodreitem = bean.getMapConfiguracaodreitem();
		Set<Entry<Integer, List<Configuracaodreitem>>> entrySet = mapConfiguracaodreitem.entrySet();
		for (Entry<Integer, List<Configuracaodreitem>> entry : entrySet) {
			for (Configuracaodreitem configuracaodreitem : entry.getValue()) {
				configuracaodreitem.setConfiguracaodretipo(new Configuracaodretipo(entry.getKey()));
				if(listaContaContabil != null) listaContaContabil.add(configuracaodreitem.getContacontabil());
				listaConfiguracaodreitem.add(configuracaodreitem);
			}
		}
		bean.setListaConfiguracaodreitem(listaConfiguracaodreitem);
	}
	
}
