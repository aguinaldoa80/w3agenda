package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Modelodocumentofiscal;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path={"/sistema/crud/Modelodocumentofiscal"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"codigo", "nome"})
public class ModelodocumentofiscalCrud extends CrudControllerSined<FiltroListagemSined, Modelodocumentofiscal, Modelodocumentofiscal> {	
	
	
}