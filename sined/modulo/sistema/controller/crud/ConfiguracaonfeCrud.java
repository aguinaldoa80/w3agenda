package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Timestamp;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Configuracaonfe;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Endereco;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.enumeration.Prefixowebservice;
import br.com.linkcom.sined.geral.bean.enumeration.Tipoconfiguracaonfe;
import br.com.linkcom.sined.geral.service.ConfiguracaonfeService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.LoteConsultaDfeHistoricoService;
import br.com.linkcom.sined.geral.service.LoteConsultaDfeService;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfe;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.LoteConsultaDfeHistorico;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.bean.enumeration.SituacaoEmissorEnum;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConfiguracaonfeFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Configuracaonfe", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "descricao", "tipoconfiguracaonfe", "prefixowebservice", "ativo", "padrao"})
public class ConfiguracaonfeCrud extends CrudControllerSined<ConfiguracaonfeFiltro, Configuracaonfe, Configuracaonfe> {
	
	private EmpresaService empresaService;
	private ConfiguracaonfeService configuracaonfeService;
	private ReportTemplateService reporttemplateService;
	private LoteConsultaDfeService loteConsultaDfeService;
	private LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService;
	
	public void setConfiguracaonfeService(
			ConfiguracaonfeService configuracaonfeService) {
		this.configuracaonfeService = configuracaonfeService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setReporttemplateService(
			ReportTemplateService reporttemplateService) {
		this.reporttemplateService = reporttemplateService;
	}
	public void setLoteConsultaDfeService(LoteConsultaDfeService loteConsultaDfeService) {
		this.loteConsultaDfeService = loteConsultaDfeService;
	}
	public void setLoteConsultaDfeHistoricoService(LoteConsultaDfeHistoricoService loteConsultaDfeHistoricoService) {
		this.loteConsultaDfeHistoricoService = loteConsultaDfeHistoricoService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Configuracaonfe form) throws Exception {
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
		request.setAttribute("listaTemplateEmissao", reporttemplateService.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.EMITIR_NFSE));
		request.setAttribute("listaConfiguracaoNfeManifestoAutomatico", configuracaonfeService.procurarMd(form.getEmpresa(), form.getConfiguracaoNfeManifestoAutomatico(), Prefixowebservice.DDE_PROD.equals(form.getPrefixowebservice())));
	}
	
	/**
	 * Action em ajax para verificar se existe alguma outra configura��o padr�o.
	 *
	 * @param request
	 * @param configuracaonfe
	 * @since 27/08/2012
	 * @author Rodrigo Freitas
	 */
	public void ajaxVerificarPadrao(WebRequestContext request, Configuracaonfe configuracaonfe){
		try {
			boolean havePadrao = configuracaonfeService.havePadrao(configuracaonfe.getCdconfiguracaonfe(), 
																	configuracaonfe.getTipoconfiguracaonfe(),
																	configuracaonfe.getEmpresa());
			View.getCurrent().println("var havePadrao = " + havePadrao + ";");
			View.getCurrent().println("var sucesso = true;");
		} catch (Exception e) {
			e.printStackTrace();
			View.getCurrent().println("var sucesso = false;");
		}
	}
	
	/**
	 *	Action em ajax para buscar informa��es da empresa.
	 *
	 * @param request
	 * @param configuracaonfe
	 * @return
	 * @since 27/08/2012
	 * @author Rodrigo Freitas
	 */
	public ModelAndView ajaxBuscarInformacoesEmpresa(WebRequestContext request, Configuracaonfe configuracaonfe){
		JsonModelAndView json = new JsonModelAndView();
		try{
			if(configuracaonfe.getEmpresa() == null)
				throw new SinedException("Empresa n�o pode ser nula.");
			
			Empresa empresa = empresaService.loadForEntrada(configuracaonfe.getEmpresa());
			
			Telefone telefone = empresa.getTelefone();
			if(telefone != null){
				json.addObject("telefone", telefone.getTelefone());
			}
			
			Endereco endereco = empresa.getEndereco();
			if(endereco != null){
				json
					.addObject("logradouro", endereco.getLogradouro() != null ? endereco.getLogradouro() : "")
					.addObject("numero", endereco.getNumero() != null ? endereco.getNumero() : "")
					.addObject("complemento", endereco.getComplemento() != null ? endereco.getComplemento() : "")
					.addObject("bairro", endereco.getBairro() != null ? endereco.getBairro() : "")
					.addObject("cep", endereco.getCep() != null ? endereco.getCep().toString() : "")
					.addObject("uf", endereco.getMunicipio() != null && endereco.getMunicipio().getUf() != null ? "br.com.linkcom.sined.geral.bean.Uf[cduf=" + endereco.getMunicipio().getUf().getCduf() + "]" : "<null>")
					.addObject("municipio", endereco.getMunicipio() != null ? "br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio=" + endereco.getMunicipio().getCdmunicipio() + "]" : "<null>");
			}
			
			if(empresa.getCrt() != null){
				json.addObject("crt", empresa.getCrt().name());
			}
			
			if(empresa.getInscricaomunicipal() != null){
				json.addObject("inscricaomunicipal", empresa.getInscricaomunicipal());
			}
			
			if(empresa.getInscricaoestadual() != null){
				json.addObject("inscricaoestadual", empresa.getInscricaoestadual());
			}
			
			if(empresa.getInscricaoestadualst() != null){
				json.addObject("inscricaoestadualst", empresa.getInscricaoestadualst());
			}
			
			return json.addObject("sucesso", true);
		} catch (Exception e) {
			e.printStackTrace();
			return json
					.addObject("sucesso", false)
					.addObject("erro", e.getMessage());
		}
	}
	@Override
	protected void salvar(WebRequestContext request, Configuracaonfe bean) throws Exception {
		Boolean criarLoteConsultaDfe = Boolean.FALSE;
		
		if(bean.getLogoNfse() != null && !SinedUtil.isImage(bean.getLogoNfse())){
			throw new SinedException("S� � poss�vel inserir Logomarca de Prefeitura que possua formato de imagem.");
		}
		
		if (bean.getCdconfiguracaonfe() == null && bean.getTipoconfiguracaonfe() != null && bean.getTipoconfiguracaonfe().equals(Tipoconfiguracaonfe.DDFE)) {
			criarLoteConsultaDfe = Boolean.TRUE;
		}
		
		super.salvar(request, bean);
		
		if (criarLoteConsultaDfe) {
			LoteConsultaDfe loteConsultaDfe = loteConsultaDfeService.criarNovoLoteConsultaDfeNsuSeq(bean, Boolean.FALSE);
			
			loteConsultaDfeService.saveOrUpdate(loteConsultaDfe);
			
			LoteConsultaDfeHistorico loteConsultaDfeHistorico = new LoteConsultaDfeHistorico();
			Usuario usuario = SinedUtil.getUsuarioLogado();
			loteConsultaDfeHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
			loteConsultaDfeHistorico.setCdusuarioaltera(usuario != null ? usuario.getCdpessoa() : null);
			loteConsultaDfeHistorico.setLoteConsultaDfe(loteConsultaDfe);
			loteConsultaDfeHistorico.setObservacao("Lote Consulta DF-e criada a partir da cria��o da configura��o de nf-e.");
			loteConsultaDfeHistorico.setSituacaoEnum(SituacaoEmissorEnum.GERADO);
			
			loteConsultaDfeHistoricoService.saveOrUpdate(loteConsultaDfeHistorico);
		}
	}
	
	public ModelAndView comboBoxConfiguracaoNfeManifestoAutomatico(WebRequestContext request, Empresa empresa) {
		Prefixowebservice prefixowebservice = null;
		try {
			prefixowebservice = Prefixowebservice.valueOf(request.getParameter("prefixowebservice"));
		} catch (Exception e) {}
		
		return new JsonModelAndView().addObject("lista", configuracaonfeService.procurarMd(empresa,  null, Prefixowebservice.DDE_PROD.equals(prefixowebservice)));
	}
}
