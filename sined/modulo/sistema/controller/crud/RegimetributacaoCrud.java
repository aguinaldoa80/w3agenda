package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Regimetributacao;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/Regimetributacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "codigonfse", "codigonfe", "ativo"})
public class RegimetributacaoCrud extends CrudControllerSined<FiltroListagemSined, Regimetributacao, Regimetributacao> {
	
	@Override
	protected void excluir(WebRequestContext request, Regimetributacao bean) throws Exception {
		throw new SinedException("A��o n�o permitida.");
	}
	
}