package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Empresaconfiguracaonfproduto;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmpresaconfiguracaonfprodutoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Empresaconfiguracaonfproduto", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class EmpresaconfiguracaonfprodutoCrud extends CrudControllerSined<EmpresaconfiguracaonfprodutoFiltro, Empresaconfiguracaonfproduto,Empresaconfiguracaonfproduto> {
	

}
