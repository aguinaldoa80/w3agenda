package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.OperacaoContabilTipoLancamento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.OperacaoContabilTipoLancamentoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/OperacaoContabilTipoLancamento", authorizationModule=CrudAuthorizationModule.class)
public class OperacaoContabilTipoLancamentoCrud extends CrudControllerSined<OperacaoContabilTipoLancamentoFiltro, OperacaoContabilTipoLancamento, OperacaoContabilTipoLancamento> {
	@Override
	public ModelAndView doCriar(WebRequestContext request, OperacaoContabilTipoLancamento form) 
			throws CrudException {
		request.addError("A��o n�o permitida");
		return sendRedirectToAction("listagem");
	}
}
