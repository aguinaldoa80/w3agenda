package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.FaixaMarkupNome;
import br.com.linkcom.sined.geral.service.FaixaMarkupNomeService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FaixaMarkupNomeFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/FaixaMarkupNome", authorizationModule=CrudAuthorizationModule.class)
public class FaixaMarkupNomeCrud extends CrudControllerSined<FaixaMarkupNomeFiltro, FaixaMarkupNome, FaixaMarkupNome>{
	
	private FaixaMarkupNomeService faixaMarkupNomeService;
	
	public void setFaixaMarkupNomeService(
			FaixaMarkupNomeService faixaMarkupNomeService) {
		this.faixaMarkupNomeService = faixaMarkupNomeService;
	}

	@Override
	protected void validateBean(FaixaMarkupNome bean, BindException errors) {
		FaixaMarkupNome faixa = faixaMarkupNomeService.loadByNome(bean.getNome(), bean.getCdFaixaMarkupNome());
		if(faixa != null){
			errors.reject("001", "J� existe uma faixa cadastrada com esse nome.");
		}
		super.validateBean(bean, errors);
	}
}
