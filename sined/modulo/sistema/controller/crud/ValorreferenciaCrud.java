package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Valorreferencia;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.ValorreferenciaService;
import br.com.linkcom.sined.modulo.projeto.controller.crud.bean.AtualizarValorreferenciaBean;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ValorreferenciaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Valorreferencia", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"planejamento", "material", "cargo", "valor"})
public class ValorreferenciaCrud extends CrudControllerSined<ValorreferenciaFiltro, Valorreferencia, Valorreferencia> {	
	
	private ValorreferenciaService valorreferenciaService;
	private MaterialService materialService;
	
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}	
	public void setValorreferenciaService(
			ValorreferenciaService valorreferenciaService) {
		this.valorreferenciaService = valorreferenciaService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Valorreferencia bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_VALORREFERENCIA_CUSTO")) {throw new SinedException("Custo de planejamento j� cadastrado.");}	
		}
	}
	
	/**
	 * Action para atualizar o valores de refer�ncia de um planejamento.
	 *
	 * @see br.com.linkcom.sined.geral.service.ValorreferenciaService#atualizaValores
	 * @param request
	 * @param filtro
	 * @return
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public ModelAndView atualizarValores(WebRequestContext request, ValorreferenciaFiltro filtro) throws Exception {
		if (filtro == null || filtro.getPlanejamento() == null || filtro.getPlanejamento().getCdplanejamento() == null){
			throw new SinedException("Planejamento n�o pode ser nulo.");
		}		
		boolean fromOrcamento = "true".equals(request.getParameter("fromOrcamento"));
		List<Valorreferencia> listaReferencia = new ArrayList<Valorreferencia>();
		List<Valorreferencia> listaSemValorMaterial = new ArrayList<Valorreferencia>();
		List<Valorreferencia> listaSemValorCargo = new ArrayList<Valorreferencia>();
		valorreferenciaService.atualizaValores(filtro.getPlanejamento(), listaReferencia, listaSemValorMaterial, listaSemValorCargo);
		
		if ((listaReferencia == null || listaReferencia.isEmpty()) &&
			(listaSemValorMaterial == null || listaSemValorMaterial.isEmpty()) &&
			(listaSemValorCargo == null || listaSemValorCargo.isEmpty())) {
			if (fromOrcamento) {
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().println("<script>window.opener.location.href = '"+request.getServletRequest().getContextPath()+"/projeto/relatorio/OrcamentoSintetico?ACAO=gerar&planejamento=br.com.linkcom.sined.geral.bean.Planejamento[cdplanejamento="+filtro.getPlanejamento().getCdplanejamento()+"]';window.close();</script>");
				return null;
			}
			request.addError("Nenhum valor a ser atualizado.");
			SinedUtil.recarregarPaginaWithClose(request);
			return null;
		}
		
		AtualizarValorreferenciaBean bean = new AtualizarValorreferenciaBean(listaReferencia, listaSemValorMaterial, listaSemValorCargo, filtro.getFromOrcamento(), filtro.getPlanejamento());
		bean.setFromOrcamento(fromOrcamento);
		
		if ((listaSemValorCargo == null || listaSemValorCargo.isEmpty()) && 
				(listaSemValorMaterial == null || listaSemValorMaterial.isEmpty())) {
			return continueOnAction("saveAtualizarValores", bean);
		} else {
			return new ModelAndView("direct:/crud/popup/preencheValores","bean",bean);
		}
	}
	
	/**
	 * M�todo para salvar os valores a serem atualizados.
	 * 
	 * @see br.com.linkcom.sined.geral.service.ValorreferenciaService#saveValores
	 * @see br.com.linkcom.sined.util.SinedUtil#recarregarPagina
	 * @param request
	 * @param bean
	 * @throws Exception
	 * @author Rodrigo Freitas
	 */
	public void saveAtualizarValores(WebRequestContext request, AtualizarValorreferenciaBean bean) throws Exception {
		if (bean == null) {
			throw new SinedException("Bean de atualizar valores n�o pode ser nulo.");
		}
		valorreferenciaService.saveValores(bean);
		if (bean.getFromOrcamento()) {
			request.getServletResponse().setContentType("text/html");
			View.getCurrent().println("<script>window.opener.location.href = '"+request.getServletRequest().getContextPath()+"/projeto/relatorio/OrcamentoSintetico?ACAO=gerar&planejamento=br.com.linkcom.sined.geral.bean.Planejamento[cdplanejamento="+bean.getPlanejamento().getCdplanejamento()+"]';window.close();</script>");
		} else {
			request.addMessage("Valores atualizados com sucesso.");
			SinedUtil.recarregarPaginaWithClose(request);
		}
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request, ValorreferenciaFiltro filtro) throws CrudException {
		if (filtro != null && filtro.getOrderBy() != null && !filtro.getOrderBy().isEmpty()){
			String isasc =  filtro.isAsc() ? " asc " : " desc ";
			request.getSession().setAttribute("order", filtro.getOrderBy() + isasc);
		}	
		return super.doListagem(request, filtro);
	}
	
	public void ajaxCarregaUnidade(WebRequestContext request, Valorreferencia bean) {
		if(bean.getCargo() != null){
			View.getCurrent().eval("H/H");
		} else if(bean.getMaterial() != null){
			View.getCurrent().eval(materialService.getNomeUnidadeMedida(bean.getMaterial()));
		} else {
			View.getCurrent().eval("");
		}
	}
	
}
