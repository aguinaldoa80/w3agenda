package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Codigotributacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CodigotributacaoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Codigotributacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"codigo", "descricao", "municipioUf", "ativo"})
public class CodigotributacaoCrud extends CrudControllerSined<CodigotributacaoFiltro, Codigotributacao, Codigotributacao> {
	
}
