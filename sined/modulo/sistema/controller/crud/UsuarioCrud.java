package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudController;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Fornecedor;
import br.com.linkcom.sined.geral.bean.Papel;
import br.com.linkcom.sined.geral.bean.Restricaoacessopessoa;
import br.com.linkcom.sined.geral.bean.Tipopessoa;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.bean.Usuariodepartamento;
import br.com.linkcom.sined.geral.bean.Usuarioempresa;
import br.com.linkcom.sined.geral.bean.Usuariopapel;
import br.com.linkcom.sined.geral.bean.Usuarioprojeto;
import br.com.linkcom.sined.geral.bean.enumeration.EnumTipoRestricaoAcessoPessoa;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.PapelService;
import br.com.linkcom.sined.geral.service.PessoaService;
import br.com.linkcom.sined.geral.service.RestricaoacessopessoaService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.geral.service.UsuariodepartamentoService;
import br.com.linkcom.sined.geral.service.UsuarioempresaService;
import br.com.linkcom.sined.geral.service.UsuariopapelService;
import br.com.linkcom.sined.geral.service.UsuarioprojetoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UsuarioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Usuario","/servicointerno/crud/Usuario"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "login", "niveis", "bloqueado"})
public class UsuarioCrud extends CrudControllerSined<UsuarioFiltro, Usuario, Usuario> {
	
	private PapelService papelService;
	private UsuarioService usuarioService;
	private UsuariopapelService usuariopapelService;
	private EmpresaService empresaService;
	private UsuarioempresaService usuarioempresaService;
	private FornecedorService fornecedorService;
	private ColaboradorService colaboradorService;
	private UsuarioprojetoService usuarioprojetoService;
	private UsuariodepartamentoService usuariodepartamentoService;
	private PessoaService pessoaService;
	private RestricaoacessopessoaService restricaoacessopessoaService; 
	
	public void setPessoaService(PessoaService pessoaService) {this.pessoaService = pessoaService;}
	public void setUsuariodepartamentoService(UsuariodepartamentoService usuariodepartamentoService) {this.usuariodepartamentoService = usuariodepartamentoService;}
	public void setUsuarioprojetoService(UsuarioprojetoService usuarioprojetoService) {this.usuarioprojetoService = usuarioprojetoService;}
	public void setColaboradorService(ColaboradorService colaboradorService) {this.colaboradorService = colaboradorService;}
	public void setFornecedorService(FornecedorService fornecedorService) {this.fornecedorService = fornecedorService;}
	public void setUsuarioempresaService(UsuarioempresaService usuarioempresaService) {this.usuarioempresaService = usuarioempresaService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	public void setPapelService(PapelService papelService) {this.papelService = papelService;}
	public void setUsuarioService(UsuarioService usuarioService) {this.usuarioService = usuarioService;}	
	public void setUsuariopapelService(UsuariopapelService usuariopapelService) {this.usuariopapelService = usuariopapelService;}
	public void setRestricaoacessopessoaService(RestricaoacessopessoaService restricaoacessopessoaService) {this.restricaoacessopessoaService = restricaoacessopessoaService;}
	
	@Override
	protected void salvar(WebRequestContext request, Usuario bean) throws Exception {
		
		//Lan�a uma exce��o caso n�o seja permitido editar.
		validaPermissaoEdicao(bean);

		request.setAttribute("validacpf", false);
		
		bean.setTipopessoa(Tipopessoa.PESSOA_FISICA);
		
		if(!bean.isLoginMathcerValidate()){
			throw new SinedException("O login n�o pode conter caracteres especiais, exceto: '.','-','_'");
		}
		
		usuarioService.criarListaUsuarioPapel(bean);
		
		if (bean.getLogin().equals(Neo.getUser().getLogin())) {
			String listAndConcatenate = CollectionsUtil.listAndConcatenate(bean.getListaEmpresa(), "cdpessoa", ",");
			NeoWeb.getRequestContext().getSession().setAttribute("listaEmpresaLogado", listAndConcatenate);
		}
		
		usuarioService.criarListaUsuarioEmpresa(bean);

		if (bean.getCdpessoa() == null) { //se criando registro...
			String encryptPassword = usuarioService.encryptPassword(bean.getSenha());
			bean.setSenha(encryptPassword);
		} else {
			//preservar atributo, se editando
			String senha = usuarioService.carregarSenha(bean);
			bean.setSenha(senha);
		}

		if (bean.getCdpessoaaux() != null) { //se o usu�rio � um colaborador...
			bean.setCdpessoa(bean.getCdpessoaaux());
			
			pessoaService.updateEmail(bean);
			try {
				usuarioService.salvaUsuarioColaborador(bean);
			} catch (DataIntegrityViolationException e) {
				if (DatabaseError.isKeyPresent(e, "IDX_USUARIO_LOGIN")) {
					throw new SinedException("Login j� cadastrado no sistema.");
				}else {
					throw new SinedException(e.getMessage());
				}
			}
			
			if(bean.getListaUsuarioempresa() != null){
				for (Usuarioempresa usuarioempresa : bean.getListaUsuarioempresa()) {
					usuarioempresa.setUsuario(bean);
					usuarioempresaService.saveOrUpdate(usuarioempresa);
				}
			}
			
			if(bean.getListaUsuariopapel() != null){
				for (Usuariopapel usuariopapel : bean.getListaUsuariopapel()) {
					usuariopapel.setPessoa(bean);
					usuariopapelService.saveOrUpdate(usuariopapel);
				}
			}
			
			if(bean.getListaUsuarioprojeto() != null){
				for (Usuarioprojeto usuarioprojeto : bean.getListaUsuarioprojeto()) {
					usuarioprojeto.setUsuario(bean);
					usuarioprojetoService.saveOrUpdate(usuarioprojeto);
				}
			}
			
			if(bean.getListaUsuariodepartamento() != null){
				for (Usuariodepartamento usuariodepartamento : bean.getListaUsuariodepartamento()) {
					usuariodepartamento.setUsuario(bean);
					usuariodepartamentoService.saveOrUpdate(usuariodepartamento);
				}
			}
		} else {
			try {
				super.salvar(request, bean);
			} catch (DataIntegrityViolationException e) {
				if (DatabaseError.isKeyPresent(e, "IDX_PESSOA_CPF")) {
					throw new SinedException("CPF j� cadastrado no sistema.");
				}
				if (DatabaseError.isKeyPresent(e, "IDX_USUARIO_LOGIN")) {
					throw new SinedException("Login j� cadastrado no sistema.");
				}
				if (DatabaseError.isKeyPresent(e, "IDX_USUARIO_SENHATERMINAL")) {
					throw new SinedException("N�o pode haver senha de terminal repetidas.");
				}
			}
		}
	}
	private void validaPermissaoEdicao(Usuario bean) {
		
		if (!SinedUtil.isUsuarioLogadoAdministrador() 
				&& bean.getCdpessoa() != null ){
			
			if (bean.getCdpessoa().equals(SinedUtil.getUsuarioLogado().getCdpessoa()))
				throw new SinedException("Voc� n�o pode realizar altera��es no pr�prio usu�rio.");
			else if (usuarioService.possuiPerfilAdministrador(bean.getCdpessoa()))
				throw new SinedException("Voc� n�o pode realizar altera��es em um usu�rio Administrador.");
		}
	}

	@Override
	public ModelAndView doCriar(WebRequestContext request, Usuario form) throws CrudException {
		request.setAttribute("CRIAR", true);
		return super.doCriar(request, form);
	}

	@Override
	protected void excluir(WebRequestContext request, Usuario bean)throws Exception {
		throw new SinedException("N�o � permitido excluir um usu�rio.");
	}

	@Override
	public ModelAndView doEditar(WebRequestContext request, Usuario form)throws CrudException {

		if (CrudController.EDITAR.equalsIgnoreCase(request.getParameter(CrudController.ACTION_PARAMETER).toString())){
			
			//Lan�a uma exce��o caso n�o seja permitido editar.
			validaPermissaoEdicao(form);
		}

		request.setAttribute("CRIAR", null);
		return super.doEditar(request, form);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Usuario bean){
		Boolean manterLista = Boolean.FALSE;

		if (request.getParameter(ACTION_PARAMETER).equals(SALVAR)) { //Cai aqui quando � lan�ada uma exce��o no m�todo salvar
			
			// For�a mostrar os campos principais da tela (fieldSet2):
			bean.setValidaaux("verdadeiro");
			
			if (bean.getCdpessoa() == null) {
				// Se estiver CRIANDO for�a mostrar os campos senha e confirmar senha:
				request.setAttribute("CRIAR", true);
			} else {
				// Se estiver EDITANDO for�a exibir o bot�o "Cancelar":
				request.setAttribute("ACAO", EDITAR);
			}
			
//			N�o recarregar a lista de usu�rio-papel:
//			manterLista = Boolean.FALSE;
		} else if (request.getAttribute(CONSULTAR) == null) {
			// Cai aqui quando � clicado o bot�o "Novo"
			bean.setBloqueado(Boolean.FALSE);
		} else {
			// Cai aqui quando � clicado o bot�o "Editar"
			bean.setValidaaux("verdadeiro");
		}

		if (!manterLista) {
			List<Papel> listaPapel = usuariopapelService.carregaPapel(bean);
			bean.setListaPapel(listaPapel);
			
			List<Empresa> listaEmpresa = usuarioempresaService.carregaEmpresa(bean);
			bean.setListaEmpresa(listaEmpresa);
		}
		
		List<Papel> listaPapel = papelService.findAll();
		if (!SinedUtil.isUsuarioLogadoAdministrador()){
			Iterator<Papel> iterator = listaPapel.iterator();
			while (iterator.hasNext()){
				Papel papel = iterator.next();
				if (papel.getAdministrador())
					iterator.remove();
			}
		}
		
		request.setAttribute("listaPapelCompleta", listaPapel);
		request.setAttribute("listaEmpresaCompleta", empresaService.findAtivosSemFiltro());
		
		if (bean.getCdpessoa()!=null && !request.getBindException().hasErrors()){
			bean.setListaRestricaoacessopessoa(restricaoacessopessoaService.findByPessoa(bean));
		}
		
		request.setAttribute("existeDatasourceOffline", usuarioService.existeDatasourceOffline(SinedUtil.getUrlWithContext()));
	}

	@Override
	protected void validateBean(Usuario bean, BindException errors) {
		
		((WebRequestContext) Neo.getRequestContext()).setAttribute("validacpf", false);

		if (bean.getEmail() == null || bean.getEmail().equals("") || !SinedUtil.verificaEmail(bean.getEmail())) {
			errors.reject("001","Digite um e-mail v�lido.");
		}else if(usuarioService.existeEmailCadastrado(bean.getCdpessoa(), bean.getEmail())){
			errors.reject("001","Existe um email j� cadatrado para um usu�rio.");
		}

		if (bean.getListaPapel() == null || bean.getListaPapel().size()<1) {
			errors.reject("001","Voc� deve selecionar pelo menos um n�vel para este usu�rio.");
		}

		if (bean.getCdpessoa() == null && bean.getSenha().length() < 4) {
			errors.reject("001","A senha deve conter, no m�nimo, 4 caracteres.");
		}

		
		if (bean.getLogin() != null && fornecedorService.exiteFornecedorLogin(bean)) {
			errors.reject("001", "J� existe um fornecedor com este login.");
		}
		
		if (bean.getConfirmasenha() != null && !bean.getConfirmasenha().equals("") && !bean.getSenha().equals(bean.getConfirmasenha())) {
			errors.reject("001","A senha n�o confere.");
		}
		
		if (StringUtils.isNotBlank(bean.getSenhaterminal()) && usuarioService.existeSenhaTerminal(bean, bean.getSenhaterminal())) {
			errors.reject("001","Senha do terminal j� cadastrada no sistema.");
		}
		
		if (bean.getListaEmpresa() == null || bean.getListaEmpresa().isEmpty()) {
			errors.reject("001","Voc� deve selecionar pelo menos uma empresa para este usu�rio.");
		}
		
		if(bean.getImagemassinaturaemail() != null && bean.getImagemassinaturaemail().getTamanho() != null && bean.getImagemassinaturaemail().getTamanho()/1000 > 1024){
			errors.reject("001","O tamnaho da imagem de assinatura de email n�o pode ser superiror a 1 megabyte.");	
		}
		
		if (bean.getListaRestricaoacessopessoa()!=null){
			for (Restricaoacessopessoa restricaoacessopessoa: bean.getListaRestricaoacessopessoa()){
				if (EnumTipoRestricaoAcessoPessoa.DIA.equals(restricaoacessopessoa.getTipo()) && restricaoacessopessoa.getDiasemana()==null){
					errors.reject("001","Dia da Restri��o de Acesso n�o preenchido.");
					break;
				}else if (EnumTipoRestricaoAcessoPessoa.HORARIO.equals(restricaoacessopessoa.getTipo()) && restricaoacessopessoa.getHrinicio()==null && restricaoacessopessoa.getHrfim()==null){
					errors.reject("001","Hor�rio da Restri��o de Acesso n�o preenchido.");
					break;
				}
			}
			for (Restricaoacessopessoa restricaoacessopessoa: bean.getListaRestricaoacessopessoa()){
				if (EnumTipoRestricaoAcessoPessoa.DIA.equals(restricaoacessopessoa.getTipo())){
					restricaoacessopessoa.setHrinicio(null);
					restricaoacessopessoa.setHrfim(null);
				}else if (EnumTipoRestricaoAcessoPessoa.HORARIO.equals(restricaoacessopessoa.getTipo())){
					restricaoacessopessoa.setDiasemana(null);
				}
			}
		}
	}
	
	public ModelAndView criarUsuario(WebRequestContext request, Usuario form) throws CrudException{
		
		Usuario load = usuarioService.load(new Usuario(form.getCdpessoaaux()));
		if(load != null){
			request.addError("Usu�rio j� existente. Login: " + load.getLogin());
			return new ModelAndView("redirect:/rh/crud/Colaborador");
		}
		
		Colaborador colaborador = new Colaborador(form.getCdpessoaaux());
		colaborador = colaboradorService.load(colaborador, "colaborador.cdpessoa, colaborador.nome, colaborador.email");
		
		form.setNome(colaborador.getNome());
		form.setEmail(colaborador.getEmail());
		
		request.setAttribute("CRIAR", Boolean.TRUE);
		
		return getCriarModelAndView(request, form);
	}
	
	public ModelAndView criarUsuarioFornecedor(WebRequestContext request, Usuario form) throws CrudException{
		
		Usuario load = usuarioService.load(new Usuario(form.getCdpessoaaux()));
		String modulo = request.getParameter("modulo");
		if(load != null){
			request.addError("Usu�rio j� existente. Login: " + load.getLogin());
			return new ModelAndView("redirect:/"+modulo+"/crud/Fornecedor");
		}
		
		Fornecedor fornecedor = new Fornecedor(form.getCdpessoaaux());
		fornecedor = fornecedorService.load(fornecedor, "fornecedor.cdpessoa, fornecedor.nome, fornecedor.email");
		
		form.setNome(fornecedor.getNome());
		form.setEmail(fornecedor.getEmail());
		
		request.setAttribute("CRIAR", Boolean.TRUE);
		
		return getCriarModelAndView(request, form);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Usuario bean) {
		if(bean.getCdpessoaaux() != null){
			request.clearMessages();
			request.addMessage("Usu�rio gerado com sucesso.");
			return new ModelAndView("redirect:/rh/crud/Colaborador");
		} else return super.getSalvarModelAndView(request, bean);
	}
	
}
