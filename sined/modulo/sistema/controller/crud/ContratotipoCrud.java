package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Contratotipo;
import br.com.linkcom.sined.geral.bean.Contratotipodesconto;
import br.com.linkcom.sined.geral.service.AtividadetipoService;
import br.com.linkcom.sined.geral.service.ContratotipoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ContratotipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path = "/sistema/crud/Contratotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "frequencia", "locacao"})
public class ContratotipoCrud extends CrudControllerSined<ContratotipoFiltro, Contratotipo, Contratotipo>{
	
	private ContratotipoService contratotipoService;
	private AtividadetipoService atividadetipoService;
	
	public void setContratotipoService(ContratotipoService contratotipoService) {
		this.contratotipoService = contratotipoService;
	}
	public void setAtividadetipoService(
			AtividadetipoService atividadetipoService) {
		this.atividadetipoService = atividadetipoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Contratotipo bean)	throws Exception {
		int validarDataNula = 0;
		try {
			if(bean.getListacontratotipodesconto()!=null && bean.getListacontratotipodesconto().size()>0 && !bean.getListacontratotipodesconto().isEmpty()){
				for (Contratotipodesconto ctd : bean.getListacontratotipodesconto()) {
					if (ctd.getLimite() == null){
						validarDataNula++;
					}
				}
				if(validarDataNula > 1){
					throw new SinedException("N�o � poss�vel ter descontos no mesmo per�odo.");
				}
			}
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if(DatabaseError.isKeyPresent(e, "IDX_CONTRATOTIPO_NOME")){
				if (validarDataNula > 1)
					throw new SinedException("N�o � poss�vel ter descontos no mesmo per�odo.");
				else
					throw new SinedException("Tipo de contrato j� cadastrado no sistema.");
			}else {
				throw new SinedException(e.getMessage());
			}
		}		
	}	
	
	/**
	* M�todo ajax que verifica se existe periodicidade para o tipo de contrato
	*
	* @param request
	* @param contratotipo
	* @since Sep 29, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxVerificaperiodicidade(WebRequestContext request, Contratotipo contratotipo){
		
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		Boolean sucesso = false;
		StringBuilder s = new StringBuilder("");
		
		if(contratotipo != null && contratotipo.getCdcontratotipo() != null){
			contratotipo = contratotipoService.load(contratotipo);
			
			if(contratotipo != null && contratotipo.getFrequencia() != null && contratotipo.getFrequencia().getCdfrequencia() != null){
				s.append("var frequencia = 'br.com.linkcom.sined.geral.bean.Frequencia[cdfrequencia=" + contratotipo.getFrequencia().getCdfrequencia() + "]';");
				
			}else{
				s.append("var frequencia = '<null>';");
			}
			
			if(contratotipo.getLocacao() != null && contratotipo.getLocacao()){
				s.append("var locacaoContratotipo = 'true';");
			} else {
				s.append("var locacaoContratotipo = 'false';");
			}
			
			if(contratotipo.getFaturarhoratrabalhada() != null && contratotipo.getFaturarhoratrabalhada()){
				s.append("var faturarhoratrabalhadaContratotipo = 'true';");
			} else {
				s.append("var faturarhoratrabalhadaContratotipo = 'false';");
			}
			
			sucesso = Boolean.TRUE;	
		}
		
		s.append("var sucesso = " + (sucesso ? "true;" : "false;"));
		view.println(s.toString());
		
	}
	
	@Override
	protected void entrada(WebRequestContext request, Contratotipo form)
			throws Exception {
		request.setAttribute("listaAtividadetipo", atividadetipoService.findForCrm());
		super.entrada(request, form);
	}
	
}
