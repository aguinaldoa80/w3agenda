package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Colaborador;
import br.com.linkcom.sined.geral.bean.Grupoemail;
import br.com.linkcom.sined.geral.bean.GrupoemailColaborador;
import br.com.linkcom.sined.geral.service.ColaboradorService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GrupoemailFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/sistema/crud/Grupoemail", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class GrupoemailCrud extends CrudControllerSined<GrupoemailFiltro, Grupoemail, Grupoemail>{
	
	private ColaboradorService colaboradorService;
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Grupoemail bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if(DatabaseError.isKeyPresent(e, "IDX_GRUPOEMAIL_NOME")){
				throw new SinedException("Grupo j� cadastrado no sistema.");
			}
			throw e;
		}
	}
	
	@Override
	protected void validateBean(Grupoemail bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaGrupoemailColaborador())){
			for (GrupoemailColaborador gec : bean.getListaGrupoemailColaborador()) {
				if(StringUtils.isBlank(gec.getColaborador().getEmail())){
					errors.reject("001","H� colaboradores selecionados sem e-mail.");
					break;
				}
			}
		}
	}
	
	/**
	 * M�todo para carregar o email do colaborador selecionado na lista de destinat�rios do grupo de email.
	 * @param request
	 * @param colaborador
	 */
	public void carregaEmailColaborador(WebRequestContext request, Colaborador colaborador){
		colaborador = colaboradorService.load(colaborador);
		String email = colaborador.getEmail();
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("var email = '" + StringUtils.trimToEmpty(email) + "';");
	}
	
	@Override
	protected void listagem(WebRequestContext request, GrupoemailFiltro filtro)
			throws Exception {
		 
		//Mudando o filtro toda vez que se clica em  bot�o
		request.getSession().setAttribute("GrupoemailCrudCONTROLLERbr.com.linkcom.sined.modulo.servicointerno.controller.crud.filter.GrupoemailFiltro",
				 filtro);
		super.listagem(request, filtro);
	}
}