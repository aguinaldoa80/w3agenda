package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Obslancamentofiscal;
import br.com.linkcom.sined.geral.service.ObslancamentofiscalService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ObslancamentofiscalFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/sistema/crud/Obslancamentofiscal", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdobslancamentofiscal", "descricao"})
public class ObslancamentofiscalCrud extends CrudControllerSined<ObslancamentofiscalFiltro, Obslancamentofiscal, Obslancamentofiscal>{
	
	private ObslancamentofiscalService obslancamentofiscalService;
	
	public void setObslancamentofiscalService(ObslancamentofiscalService obslancamentofiscalService) {
		this.obslancamentofiscalService = obslancamentofiscalService;
	}

	@Override
	protected void entrada(WebRequestContext request, Obslancamentofiscal form)	throws Exception {
		if(form.getCdobslancamentofiscal() == null){
			form.setAtivo(Boolean.TRUE);
		}
	}
	
	@Override
	protected void validateBean(Obslancamentofiscal bean, BindException errors) {
		super.validateBean(bean, errors);
		
		if(obslancamentofiscalService.existeDescricao(bean)){
			errors.reject("001", "J� existe uma observa��o com esta descri��o.");
		}
	}
	
}
