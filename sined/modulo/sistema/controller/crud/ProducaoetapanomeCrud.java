package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Producaoetapanome;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ProducaoetapanomeFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Producaoetapanome", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class ProducaoetapanomeCrud extends CrudControllerSined<ProducaoetapanomeFiltro, Producaoetapanome, Producaoetapanome>{

	@Override
	protected void salvar(WebRequestContext request, Producaoetapanome bean) throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_PRODUCAOETAPANOME_NOME")){
				throw new SinedException("Nome de Etapa de Produ��o j� cadastrada no sistema.");
			} else if(DatabaseError.isKeyPresent(e, "fk_producaoetapanomecampo_0")){
				throw new SinedException("Os campos adicionais n�o podem ser exclu�dos, j� possui refer�ncias em outros registros do sistema.");
			} else if(DatabaseError.isKeyPresent(e, "idx_producaoetapanomecampo_nome")){
				throw new SinedException("Existe campo adicional com o mesmo nome.");
			} else {
				throw e;
			}
		}
	}
}