package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetorno;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoCampo;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoIdentificador;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoOcorrencia;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoRejeicao;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmento;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoRetornoSegmentoDetalhe;
import br.com.linkcom.sined.geral.bean.BancoConfiguracaoSegmentoImportBean;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoCampoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRemessaTipoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoAcaoEnum;
import br.com.linkcom.sined.geral.bean.enumeration.BancoConfiguracaoRetornoCriarMovimentacaoEnum;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRetornoCampoService;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRetornoSegmentoService;
import br.com.linkcom.sined.geral.service.BancoConfiguracaoRetornoService;
import br.com.linkcom.sined.geral.service.BancoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.BancoConfiguracaoRetornoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/BancoConfiguracaoRetorno", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "banco", "tamanho"})
public class BancoConfiguracaoRetornoCrud extends CrudControllerSined<BancoConfiguracaoRetornoFiltro, BancoConfiguracaoRetorno, BancoConfiguracaoRetorno> {
	
	private BancoService bancoService;
	private BancoConfiguracaoRetornoSegmentoService bancoConfiguracaoRetornoSegmentoService;
	private BancoConfiguracaoRetornoService bancoConfiguracaoRetornoService;
	private BancoConfiguracaoRetornoCampoService bancoConfiguracaoRetornoCampoService;
	private ParametrogeralService parametroGeralService;
	
	public void setBancoService(BancoService bancoService) {this.bancoService = bancoService;}
	public void setBancoConfiguracaoRetornoSegmentoService(BancoConfiguracaoRetornoSegmentoService bancoConfiguracaoRetornoSegmentoService) {this.bancoConfiguracaoRetornoSegmentoService = bancoConfiguracaoRetornoSegmentoService;}
	public void setBancoConfiguracaoRetornoService(BancoConfiguracaoRetornoService bancoConfiguracaoRetornoService) { this.bancoConfiguracaoRetornoService = bancoConfiguracaoRetornoService;}
	public void setBancoConfiguracaoRetornoCampoService(BancoConfiguracaoRetornoCampoService bancoConfiguracaoRetornoCampoService) {this.bancoConfiguracaoRetornoCampoService = bancoConfiguracaoRetornoCampoService;}
	public void setParametroGeralService(ParametrogeralService parametroGeralService) {this.parametroGeralService = parametroGeralService;}
	@Override
	protected void listagem(WebRequestContext request, BancoConfiguracaoRetornoFiltro filtro) throws Exception {
		request.setAttribute("listaBanco", bancoService.findAtivos());
		super.listagem(request, filtro);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, BancoConfiguracaoRetorno form) throws Exception {
		boolean isInclusao = Util.objects.isNotPersistent(form); 
		if (form.getCdbancoconfiguracaoretorno()==null){
			form.setCriarMovimentacao(BancoConfiguracaoRetornoCriarMovimentacaoEnum.POR_REGISTRO);
		}
		
		Set<BancoConfiguracaoRetornoCampo> listaBancoConfiguracaoRetornoCampo = new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class);
		Set<BancoConfiguracaoRetornoIdentificador> listaBancoConfiguracaoRetornoIdentificador = new ListSet<BancoConfiguracaoRetornoIdentificador>(BancoConfiguracaoRetornoIdentificador.class);
	
		if(parametroGeralService.getBoolean(Parametrogeral.CONFIGURACAO_RETORNO_VARIOS_SEGMENTOS) && form.getTipo()!=null && form.getTipo().equals(BancoConfiguracaoRemessaTipoEnum.PAGAMENTO)){
			/*if(!isInclusao && form.getListaBancoConfiguracaoRetornoSegmentoDetalhe().isEmpty()){
				
			}*/

				if (!form.getListaBancoConfiguracaoRetornoIdentificador().isEmpty()){
						for (BancoConfiguracaoCampoEnum campo : BancoConfiguracaoCampoEnum.values()){
							BancoConfiguracaoRetornoIdentificador bancoConfiguracaoIdentificadorDetalhe = new BancoConfiguracaoRetornoIdentificador();
							BancoConfiguracaoRetornoIdentificador bancoConfiguracaoIdentificadorHeader = new BancoConfiguracaoRetornoIdentificador();
							BancoConfiguracaoRetornoIdentificador bancoConfiguracaoIdentificadorTrailer = new BancoConfiguracaoRetornoIdentificador();
							if(campo == BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE){
								bancoConfiguracaoIdentificadorDetalhe.setTipo(campo);
								for(BancoConfiguracaoRetornoIdentificador bcri :form.getListaBancoConfiguracaoRetornoIdentificador()){
									if(bcri.getTipo() == BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE){
										bancoConfiguracaoIdentificadorDetalhe.setPosIni(bcri.getPosIni());
										bancoConfiguracaoIdentificadorDetalhe.setTamanho(bcri.getTamanho());
										break;
										
									}
								}
								listaBancoConfiguracaoRetornoIdentificador.add(bancoConfiguracaoIdentificadorDetalhe);
							}
							if(campo == BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER){
								bancoConfiguracaoIdentificadorHeader.setTipo(campo);
								for(BancoConfiguracaoRetornoIdentificador bcri :form.getListaBancoConfiguracaoRetornoIdentificador()){
									if(bcri.getTipo() == BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER){
										bancoConfiguracaoIdentificadorHeader.setPosIni(bcri.getPosIni());
										bancoConfiguracaoIdentificadorHeader.setTamanho(bcri.getTamanho());
										break;
										
									}
								}
								listaBancoConfiguracaoRetornoIdentificador.add(bancoConfiguracaoIdentificadorHeader);
							}
							if(campo == BancoConfiguracaoCampoEnum.IDENTIFICADOR_TRAILER){
								bancoConfiguracaoIdentificadorTrailer.setTipo(campo);
								for(BancoConfiguracaoRetornoIdentificador bcri :form.getListaBancoConfiguracaoRetornoIdentificador()){
									if(bcri.getTipo() == BancoConfiguracaoCampoEnum.IDENTIFICADOR_TRAILER){
										bancoConfiguracaoIdentificadorTrailer.setPosIni(bcri.getPosIni());
										bancoConfiguracaoIdentificadorTrailer.setTamanho(bcri.getTamanho());
										break;
										
									}
								}
								listaBancoConfiguracaoRetornoIdentificador.add(bancoConfiguracaoIdentificadorTrailer);
							}
							
						}
					form.setListaBancoConfiguracaoRetornoIdentificador(new ListSet<BancoConfiguracaoRetornoIdentificador>(BancoConfiguracaoRetornoIdentificador.class, listaBancoConfiguracaoRetornoIdentificador));
					
				}else{
					for (BancoConfiguracaoCampoEnum campo : BancoConfiguracaoCampoEnum.values()){
						BancoConfiguracaoRetornoIdentificador bancoConfiguracaoIdentificadorDetalhe = new BancoConfiguracaoRetornoIdentificador();
						BancoConfiguracaoRetornoIdentificador bancoConfiguracaoIdentificadorHeader = new BancoConfiguracaoRetornoIdentificador();
						BancoConfiguracaoRetornoIdentificador bancoConfiguracaoIdentificadorTrailer = new BancoConfiguracaoRetornoIdentificador();
						if(campo == BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE){
							bancoConfiguracaoIdentificadorDetalhe.setTipo(campo);
							listaBancoConfiguracaoRetornoIdentificador.add(bancoConfiguracaoIdentificadorDetalhe);
						}
						if(campo == BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER){
							bancoConfiguracaoIdentificadorHeader.setTipo(campo);
							listaBancoConfiguracaoRetornoIdentificador.add(bancoConfiguracaoIdentificadorHeader);
						}
						if(campo == BancoConfiguracaoCampoEnum.IDENTIFICADOR_TRAILER){
							bancoConfiguracaoIdentificadorTrailer.setTipo(campo);
							listaBancoConfiguracaoRetornoIdentificador.add(bancoConfiguracaoIdentificadorTrailer);
						}
						
					}
					form.setListaBancoConfiguracaoRetornoIdentificador(new ListSet<BancoConfiguracaoRetornoIdentificador>(BancoConfiguracaoRetornoIdentificador.class, listaBancoConfiguracaoRetornoIdentificador));
				}

			if(!isInclusao){
				if(SinedUtil.isListNotEmpty(form.getListaBancoConfiguracaoRetornoSegmentoDetalhe())){
					for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : form.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
						if(bcrsd != null && bcrsd.getCdBancoConfiguracaoRetornoSegmentoDetalhe() !=null){
							bcrsd.setListaBancoConfiguracaoRetornoCampo(new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class,bancoConfiguracaoRetornoCampoService.findBySegmento(bcrsd)));
							for(BancoConfiguracaoRetornoCampo bcrc : bcrsd.getListaBancoConfiguracaoRetornoCampo()){
								
									bcrc.setObrigatorio(bcrc.getCampo().isObrigatorio());
									bcrc.setCampoData(bcrc.getCampo().isData());
									bcrc.setCampoValor(bcrc.getCampo().isValor());
									bcrc.setCampoCalculado(bcrc.getCampo().isCalculado());
								
							}
						}
					}
				}
			}
				
			if(SinedUtil.isListNotEmpty(form.getListaBancoConfiguracaoRetornoSegmentoDetalhe())){
				for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd : form.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
					if(bcrsd.getBancoConfiguracaoRetornoSegmento()!= null && SinedUtil.isListEmpty(bcrsd.getListaBancoConfiguracaoRetornoCampo())){
						bcrsd.setListaBancoConfiguracaoRetornoCampo(bancoConfiguracaoRetornoCampoService.getListaBancoConfiguracaoRetornoCampo());
					}
				}
			}
		}else{
			if (form.getListaBancoConfiguracaoRetornoCampo() == null || 
					(form.getListaBancoConfiguracaoRetornoCampo() != null &&
					 form.getListaBancoConfiguracaoRetornoCampo().isEmpty())){
					
					for (BancoConfiguracaoCampoEnum campo : BancoConfiguracaoCampoEnum.values()){
						BancoConfiguracaoRetornoCampo bancoConfiguracaoRetornoCampo = new BancoConfiguracaoRetornoCampo();
						bancoConfiguracaoRetornoCampo.setCampo(campo);
						bancoConfiguracaoRetornoCampo.setObrigatorio(campo.isObrigatorio());
						bancoConfiguracaoRetornoCampo.setCampoData(campo.isData());
						bancoConfiguracaoRetornoCampo.setCampoValor(campo.isValor());
						bancoConfiguracaoRetornoCampo.setCampoCalculado(campo.isCalculado());
						listaBancoConfiguracaoRetornoCampo.add(bancoConfiguracaoRetornoCampo);
				
					}	
					
					form.setListaBancoConfiguracaoRetornoCampo(new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class, listaBancoConfiguracaoRetornoCampo));
					listaBancoConfiguracaoRetornoCampo = form.getListaBancoConfiguracaoRetornoCampo();
				} else {
					//listaBancoConfiguracaoRetornoCampo = bancoConfiguracaoRetornoCampoService.getListaBancoConfiguracaoRetornoCampo();
					for(BancoConfiguracaoRetornoCampo bcrc : form.getListaBancoConfiguracaoRetornoCampo()){
						bcrc.setObrigatorio(bcrc.getCampo().isObrigatorio());
						bcrc.setCampoData(bcrc.getCampo().isData());
						bcrc.setCampoValor(bcrc.getCampo().isValor());
						bcrc.setCampoCalculado(bcrc.getCampo().isCalculado());
					}
				//	form.setListaBancoConfiguracaoRetornoCampo(new ListSet<BancoConfiguracaoRetornoCampo>(BancoConfiguracaoRetornoCampo.class, listaBancoConfiguracaoRetornoCampo));
		
				}
		}
		request.setAttribute("CONFIGURACAO_RETORNO_VARIOS_SEGMENTOS",parametroGeralService.getBoolean(Parametrogeral.CONFIGURACAO_RETORNO_VARIOS_SEGMENTOS));
		List<BancoConfiguracaoRetornoSegmento> listaBancoConfiguracaoRetornoSegmento = new ArrayList<BancoConfiguracaoRetornoSegmento>();
		if (form.getBanco() != null)
			listaBancoConfiguracaoRetornoSegmento.addAll(bancoConfiguracaoRetornoSegmentoService.findByBanco(form.getBanco()));
		
		request.setAttribute("listaBanco", bancoService.findAtivos());
		request.setAttribute("listaBancoConfiguracaoRetornoSegmento", listaBancoConfiguracaoRetornoSegmento);
		
		
		super.entrada(request, form);
	}
	
	@Override
	protected void validateBean(BancoConfiguracaoRetorno bean, BindException errors) {
				
		//Verifica pelo menos uma ocorr�ncia com a��o baixar
		
		if (bean.getListaBancoConfiguracaoRetornoOcorrencia() == null ||
			(bean.getListaBancoConfiguracaoRetornoOcorrencia() != null &&
			 bean.getListaBancoConfiguracaoRetornoOcorrencia().isEmpty())){
			errors.reject("001", "Necess�rio ter pelo menos um c�digo de ocorr�ncia com a a��o de Baixar.");
		} else {
			boolean encontrouAcaoBaixar = false;
			for (BancoConfiguracaoRetornoOcorrencia ocorrencia : bean.getListaBancoConfiguracaoRetornoOcorrencia()){
				if (BancoConfiguracaoRetornoAcaoEnum.BAIXAR.equals(ocorrencia.getAcao())){
					encontrouAcaoBaixar = true;
					break;
				}
			}
			if (!encontrouAcaoBaixar)
				errors.reject("001", "Necess�rio ter pelo menos um c�digo de ocorr�ncia com a a��o de Baixar.");
		}
				
		//Verifica duplicidade de ocorr�ncia
		
		boolean encontrouOcorrenciaDuplicada = false;
		for (BancoConfiguracaoRetornoOcorrencia ocorrencia1 : bean.getListaBancoConfiguracaoRetornoOcorrencia()){
			for (BancoConfiguracaoRetornoOcorrencia ocorrencia2 : bean.getListaBancoConfiguracaoRetornoOcorrencia()){
				if (!ocorrencia1.equals(ocorrencia2)){
					if (ocorrencia1.getCodigo().equals(ocorrencia2.getCodigo())){
						encontrouOcorrenciaDuplicada = true;
						break;
					}
				}
			}
			if (encontrouOcorrenciaDuplicada)
				break;
		}
		
		if (encontrouOcorrenciaDuplicada)
			errors.reject("001", "N�o se  pode repetir o c�digo de ocorr�ncia.");
		
		//Verifica duplicidade de motivo de rejei��o
		
		boolean encontrouOcorrenciaRejeicao = false;
		for (BancoConfiguracaoRetornoRejeicao rejeicao1 : bean.getListaBancoConfiguracaoRetornoRejeicao()){
			for (BancoConfiguracaoRetornoRejeicao rejeicao2 : bean.getListaBancoConfiguracaoRetornoRejeicao()){
				if (!rejeicao1.equals(rejeicao2)){
					if (rejeicao1.getCodigo().equals(rejeicao2.getCodigo())){
						if (StringUtils.isBlank(rejeicao1.getOcorrencia()) && StringUtils.isBlank(rejeicao2.getOcorrencia()) ||
								(StringUtils.isNotBlank(rejeicao1.getOcorrencia()) && StringUtils.isNotBlank(rejeicao2.getOcorrencia()) &&
										rejeicao1.getOcorrencia().equals(rejeicao2.getOcorrencia()))){
							encontrouOcorrenciaRejeicao = true;
							break;
						}
					}
				}
			}
			if (encontrouOcorrenciaRejeicao)
				break;
		}
		
		if (encontrouOcorrenciaRejeicao)
			errors.reject("001", "O c�digo de mensagem/rejei��o n�o pode se repetir dentro de uma configura��o de arquivo de retorno.");
		
		//Verifica se tem segmentos iguais
		if(SinedUtil.isListNotEmpty(bean.getListaBancoConfiguracaoRetornoSegmentoDetalhe())){
			Integer repeticao;
			Boolean erro = false;
			for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd1 : bean.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
				repeticao = 0;
				for(BancoConfiguracaoRetornoSegmentoDetalhe bcrsd2 : bean.getListaBancoConfiguracaoRetornoSegmentoDetalhe()){
					if(bcrsd1.getBancoConfiguracaoRetornoSegmento().getCdbancoconfiguracaoretornosegmento() == bcrsd2.getBancoConfiguracaoRetornoSegmento().getCdbancoconfiguracaoretornosegmento()){
						repeticao++;
					}
				}
				if(repeticao >1){
					erro = true;
					break;
				}
			}
			if(erro){
				errors.reject("001", "Existem segmentos duplicados.");
			}
		}
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, BancoConfiguracaoRetorno bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
//			if (DatabaseError.isKeyPresent(e, "IDX_BANCOCONFIGURACAORETORNOESTRUTURA_0")){
//				throw new SinedException("A identifica��o do registro deve ser �nica dentro da estrutura do arquivo.");
//			}
//			if (DatabaseError.isKeyPresent(e, "IDX_BANCOCONFIGURACAORETORNOOCORRENCIA_0")){
//				throw new SinedException("N�o se  pode repetir o c�digo de ocorr�ncia.");
//			}
//			if (DatabaseError.isKeyPresent(e, "IDX_BANCOCONFIGURACAORETORNOREJEICAO_0")){
//				throw new SinedException("N�o se pode repetir o c�digo de motivo de rejei��o.");
//			}
			if (DatabaseError.isKeyPresent(e, "IDX_BANCOCONFIGURACAORETORNO_0")){
				throw new SinedException("Estrutura j� cadastrado no sistema.");
			}
			throw e;
		}
	}
	
	public ModelAndView exportarArquivo(WebRequestContext request, BancoConfiguracaoRetornoFiltro filtro) throws CrudException, IOException {
		ByteArrayOutputStream conteudoZip = new ByteArrayOutputStream();
		bancoConfiguracaoRetornoService.processaExportar(request, filtro, conteudoZip);
		
		HttpServletResponse response = request.getServletResponse();
		response.setContentType("application/zip");
		response.addHeader("Content-Disposition", "attachment; filename =\"bancoconfigretorno.zip\";");
		response.getOutputStream().write(conteudoZip.toByteArray());
				
		return null;
	}
	/**
	 * Abrindo o popup para importa��o de Arquivos
	 * @param request
	 * @since 08/06/2016
	 * @author C�sar
	 */
	public ModelAndView abrePopUpImportar(WebRequestContext request){		
		BancoConfiguracaoSegmentoImportBean bancoConfiguracaoSegmentoArquivosImportBean = new BancoConfiguracaoSegmentoImportBean();
		return new ModelAndView("direct:/crud/popup/bancoConfigSegImportArquivo","BancoConfiguracaoSegmentoImportBean", bancoConfiguracaoSegmentoArquivosImportBean);
	}
	
	/**
	 * Upload/Processando do arquivos
	 * @param request , arquivosImportBean
	 * @author C�sar
	 * @since 08/06/2016
	 */
	public void upload(WebRequestContext request, BancoConfiguracaoSegmentoImportBean arquivosImportBean) throws CrudException, IOException {
		String mensagemErro = "";
		Boolean semArquivos = Boolean.FALSE;
		
		if(arquivosImportBean == null 
				|| arquivosImportBean.getListaAquivosImport() == null 
				|| arquivosImportBean.getListaAquivosImport().get(0).getArquivo() == null){
			semArquivos = Boolean.TRUE;
			request.addError("N�o � poss�vel realizar a importa��o sem selecionar os arquivos.");
		}
		
		if(!semArquivos && arquivosImportBean != null && !arquivosImportBean.getListaAquivosImport().isEmpty()){	
			mensagemErro = bancoConfiguracaoRetornoService.importacaoDados(arquivosImportBean.getListaAquivosImport(), request);
		
			if(!mensagemErro.isEmpty()){	
				request.addError("Erro ao importar arquivos.");
				for(String erro : mensagemErro.split("<br>")){
					request.addError(erro);
				}
			} else{
				request.addMessage("Arquivos importados com sucesso.");
			}			
		}
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println("<script>parent.doFilter();</script>");
	}
	
	
	public ModelAndView ajaxCampoIdentificador(WebRequestContext request){
		JsonModelAndView json = new JsonModelAndView(); 
		json.addObject("campo1", BancoConfiguracaoCampoEnum.IDENTIFICADOR_DETALHE);
		json.addObject("campo2", BancoConfiguracaoCampoEnum.IDENTIFICADOR_HEADER);
		json.addObject("campo3", BancoConfiguracaoCampoEnum.IDENTIFICADOR_TRAILER);
		
		return json;
	}
}
