package br.com.linkcom.sined.modulo.sistema.controller.crud;

import net.sf.json.JSON;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.ComprovanteConfiguravel;
import br.com.linkcom.sined.geral.bean.enumeration.CampoComprovanteConfiguravel;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ComprovanteConfiguravelFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/ComprovanteConfiguravel", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "venda", "pedidoVenda", "orcamento", "coleta", "ativo", "txt"})
public class ComprovanteConfiguravelCrud extends CrudControllerSined<ComprovanteConfiguravelFiltro, ComprovanteConfiguravel, ComprovanteConfiguravel> {

	@Override
	protected void salvar(WebRequestContext request, ComprovanteConfiguravel bean) throws Exception {
		try{
			if(bean.getVenda() || bean.getPedidoVenda() || bean.getOrcamento())
				super.salvar(request, bean);
			else throw new SinedException("Selecione pelo menos um tipo de comprovante(Venda, Pedido de Venda ou Or�amento).");
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_COMPROVANTECONFIGURAVEL_DESCRICAO")){
				throw new SinedException("J� existe um comprovante com essa descri��o.");
			}
		}
	}

	@Override
	protected void entrada(WebRequestContext request, ComprovanteConfiguravel form) throws Exception {
		
		JSON listaCampos = View.convertToJson(CampoComprovanteConfiguravel.getListaCampos());
		request.setAttribute("listaTipos", CampoComprovanteConfiguravel.TipoCampo.values());
		request.setAttribute("listaCampos", listaCampos);
		super.entrada(request, form);
	}
}
