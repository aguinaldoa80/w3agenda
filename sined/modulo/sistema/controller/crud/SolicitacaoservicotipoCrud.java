package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Solicitacaoservicotipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.SolicitacaoservicotipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Solicitacaoservicotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields= {"nome", "ativo"})
public class SolicitacaoservicotipoCrud extends CrudControllerSined<SolicitacaoservicotipoFiltro, Solicitacaoservicotipo, Solicitacaoservicotipo>{

	@Override
	protected void salvar(WebRequestContext request, Solicitacaoservicotipo bean)throws Exception {
		try {
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_SOLICITACAOSERVTIPO_NOME")){
				throw new SinedException("Tipo de solicita��o j� cadastrado no sistema.");
			}
			
		}catch (Exception e) {
			if(e.getMessage().contains("Existe(m) registro(s) vinculado(s)")){
				 throw new  SinedException("N�o foi poss�vel remover, pois h� registro(s) vinculado(s) ao(s)campo(s).");
			 }
		}
	}
	
	
}
