package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Inspecaoitem;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.InspecaoitemFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path= {"/sistema/crud/Inspecaoitem"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "descricao", "inspecaoitemtipo", "visual", "ativo"})
public class InspecaoitemCrud extends CrudControllerSined<InspecaoitemFiltro, Inspecaoitem, Inspecaoitem> {

	@Override
	protected void salvar(WebRequestContext request, Inspecaoitem bean)	throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_INSPECAOITEM_NOME")) {
				throw new SinedException("Item de inspe��o j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Inspecaoitem bean) throws Exception {
		try {
			super.excluir(request, bean);
		} catch (SinedException e) {
			throw new SinedException("Item de inspe��o n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}

	
}
