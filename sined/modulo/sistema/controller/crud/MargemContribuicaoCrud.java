package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.MargemContribuicao;
import br.com.linkcom.sined.geral.service.MargemContribuicaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MargemContribuicaoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/MargemContribuicao", authorizationModule=CrudAuthorizationModule.class)
public class MargemContribuicaoCrud extends CrudControllerSined<MargemContribuicaoFiltro, MargemContribuicao, MargemContribuicao> {

	private MargemContribuicaoService margemContribuicaoService;
	
	public void setMargemContribuicaoService(MargemContribuicaoService margemContribuicaoService) {
		this.margemContribuicaoService = margemContribuicaoService;
	}
	
	@Override
	protected void validateBean(MargemContribuicao bean, BindException errors) {
		if (StringUtils.isEmpty(bean.getNome())) {
			errors.reject("001", "O campo 'NOME' � obrigat�rio.");
		}
		
		if (BooleanUtils.isTrue(bean.getGastosEmpresa()) && (bean.getValorGastosEmpresa() == null || bean.getValorGastosEmpresa().getValue().doubleValue() <= 0d)) {
			errors.reject("001", "O campo 'GASTOS DA EMPRESA' � obrigat�rio.");
		}
		
		if (BooleanUtils.isFalse(bean.getCalcularPontoEquilibrio()) && (bean.getValorPontoEquilibrio() == null || bean.getValorPontoEquilibrio() <= 0d)) {
			errors.reject("001", "O campo '(%) PONTO DE EQUIL�BRIO' � obrigat�rio.");
		}
		
		if (bean.getValorPontoEquilibrioAmarelo() == null || bean.getValorPontoEquilibrioAmarelo() <= 0d) {
			errors.reject("001", "O campo '(%) ACIMA DO PONTO DE EQUIL�BRIO (FAIXA AMARELA)' � obrigat�rio.");
		}
		
		if (BooleanUtils.isTrue(bean.getPadrao())) {
			if (margemContribuicaoService.validaPadrao(bean)) {
				errors.reject("001", "J� existe um registro definido como padr�o.");
			}
		}
	}
	
	public void buscaMargemContribuicaoPrincipal(WebRequestContext request){
		String hasPrincipal = "var hasPrincipal = ";
		
		request.getServletResponse().setContentType("text/html");
		View view = View.getCurrent();
		
		MargemContribuicao contribuicao = margemContribuicaoService.findPrincipal();
		
		if(contribuicao == null){
			hasPrincipal += "false;";
		} else {
			hasPrincipal += "true;";
			hasPrincipal += "var cdmargemcontribuicao = '" + contribuicao.getCdMargemContribuicao() + "';";
		}
				
		view.println(hasPrincipal);
	}
}
