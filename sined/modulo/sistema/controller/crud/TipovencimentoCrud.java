package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Tipovencimento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.TipovencimentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Tipovencimento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class TipovencimentoCrud extends CrudControllerSined<TipovencimentoFiltro, Tipovencimento, Tipovencimento>{

	@Override
	protected void salvar(WebRequestContext request, Tipovencimento bean) throws Exception{
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_tipovencimento_nome")) 
				throw new SinedException("Tipo de vencimento j� cadastrado no sistema");			
		}
	}
}
