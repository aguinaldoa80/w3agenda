package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Veiculotipo;
import br.com.linkcom.sined.modulo.veiculo.controller.crud.filter.VeiculotipomarcacorcombustivelFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Veiculotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "implemento"})
public class VeiculotipoCrud extends CrudControllerSined<VeiculotipomarcacorcombustivelFiltro, Veiculotipo, Veiculotipo>{
	
	@Override
	protected void salvar(WebRequestContext request, Veiculotipo bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_VEICULOTIPO_DESCRICAO")) {
				throw new SinedException("Descri��o de tipo de ve�culo j� cadastrado no sistema.");
			}
		}
	}
}
