package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Configuracaodretipo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ConfiguracaodretipoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Configuracaodretipo"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "configuracaodreseparacao", "configuracaodreoperacao"})
public class ConfiguracaodretipoCrud extends CrudControllerSined<ConfiguracaodretipoFiltro, Configuracaodretipo, Configuracaodretipo>{
	
}
