package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Ausenciamotivo;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AusenciamotivoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/sistema/crud/Ausenciamotivo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "motivo")
public class AusenciamotivoCrud extends CrudControllerSined<AusenciamotivoFiltro, Ausenciamotivo, Ausenciamotivo>{
	
	@Override
	protected void salvar(WebRequestContext request, Ausenciamotivo bean) throws Exception {
		try {
			bean.setMotivo(bean.getMotivo().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_ausenciamotivo_motivo")) {
				throw new SinedException("Motivo de aus�ncia j� cadastrado no sistema.");
			}
		}
	}
	
}
