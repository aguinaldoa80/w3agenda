package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Menu;
import br.com.linkcom.sined.geral.service.MenuService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MenuFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Menu"}, authorizationModule=CrudAuthorizationModule.class)
public class MenuCrud extends CrudControllerSined<MenuFiltro, Menu, Menu> {
	
	private MenuService menuService;
	
	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Menu form) throws Exception {
		request.setAttribute("listaMenu", menuService.findForTelaEntrada(form.getCdmenu()));
	}
	
}
