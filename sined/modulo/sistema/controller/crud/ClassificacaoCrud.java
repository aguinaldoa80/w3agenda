package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Classificacao;
import br.com.linkcom.sined.geral.service.ClassificacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.ClassificacaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Classificacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"vclassificacao.identificador", "formato", "nome", "vclassificacao.nivel"})
public class ClassificacaoCrud extends CrudControllerSined<ClassificacaoFiltro, Classificacao, Classificacao>{

	private ClassificacaoService classificacaoService;
	
	public void setClassificacaoService(ClassificacaoService classificacaoService) {this.classificacaoService = classificacaoService;}
	
	@Override
	protected void entrada(WebRequestContext request, Classificacao form) throws Exception {
		
		String stCopiar = request.getParameter("copiar");
		if (StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")) {
			
			Classificacao classificacaosuperior = new Classificacao();
			classificacaosuperior.setCdclassificacao(Integer.parseInt(request.getParameter("cdclassificacao")));
			form.setClassificacaosuperior(classificacaosuperior);

			//Atributos a serem modificados.
			form.setCdclassificacao(null);
			form.setNome(null);
			
		} else {
			if (form.getPai() != null) {
				form.setClassificacaosuperior(form.getPai());
			}
		}
		
	}
	
	@Override
	protected void salvar(WebRequestContext request, Classificacao bean) throws Exception {
		classificacaoService.preparaBeanParaSalvar(bean);
		try {
			bean.setNome(bean.getNome().replaceAll("\"", "''"));
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_CLASSIFICACAO_NOME_IRMA")) {
				throw new SinedException("Classifica��o j� cadastrada no sistema.");
			}
		}
	}
	
	public void ajaxNivel(WebRequestContext request, Classificacao bean) {
		request.getServletResponse().setContentType("text/html");
		if (bean == null || bean.getClassificacaosuperior() == null) {
			View.getCurrent().eval("");
		} else {
			bean = classificacaoService.carregaClassificacao(bean.getClassificacaosuperior());
			int proximoNivel = (bean.getVclassificacao().getNivel()+1);
			View.getCurrent().eval(String.valueOf(proximoNivel));
		}
	}
	
	public void ajaxAnalitica(WebRequestContext request, Classificacao bean) {
		if (bean == null || bean.getClassificacaosuperior() == null) {
			throw new SinedException("Classificacaosuperior nao pode ser nula.");
		} else {
			// verifica se a conta gerencial pai � anal�tica e tem registros.
			if (classificacaoService.isAnalitica(bean.getClassificacaosuperior())) {
				request.getServletResponse().setContentType("text/html");
				View.getCurrent().eval("true");
			} else View.getCurrent().eval("false");			
		}
	}
	
	public void ajaxItem(WebRequestContext request, Classificacao bean) {
		
		Integer item = null;
		
		if (bean.getClassificacaosuperior() != null && bean.getClassificacaosuperior().getCdclassificacao() != null) {
			List<Classificacao> listaFilhos = classificacaoService.findFilhos(bean.getClassificacaosuperior());
			if (listaFilhos.size() > 0) {
				item = listaFilhos.get(0).getItem() + 1;
			} else {
				item = 1;
			}
		} else {
			List<Classificacao> listaPais = classificacaoService.findRaiz();
			if (listaPais.size() > 0) {
				item = listaPais.get(0).getItem() + 1;
			} else {
				item = 1;
			}
		}
		View.getCurrent().println("var item = " + item + ";");
	}
	
}
