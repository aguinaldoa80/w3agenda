package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Materialcategoria;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialcategoriaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MaterialcategoriaFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Materialcategoria"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"vmaterialcategoria.identificador", "descricao", "ativo"})
public class MaterialcategoriaCrud extends CrudControllerSined<MaterialcategoriaFiltro, Materialcategoria, Materialcategoria>{
	
	private MaterialcategoriaService materialcategoriaService;
	private MaterialService materialService;
	
	public void setMaterialcategoriaService(
			MaterialcategoriaService materialcategoriaService) {
		this.materialcategoriaService = materialcategoriaService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}

	@Override
	protected void salvar(WebRequestContext request, Materialcategoria bean) throws Exception{
		try{
			materialcategoriaService.preparaParaSalvar(bean);
			if(!verificaPai(bean))
				throw new SinedException("Uma categoria n�o pode ser pai dela mesma.");
			super.salvar(request, bean);
			
			if (bean.isCriarCategoriaSuperior() && bean.getMaterialcategoriapai()!=null){
				materialService.updateCategoriaMaterial(bean.getMaterialcategoriapai(), bean);
			}
			bean.setCriarCategoriaSuperior(false);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_MATERIALCATEGORIA_DESCRICAO")){
				throw new SinedException("Categoria de material j� cadastrada no sistema.");
			}else if(DatabaseError.isKeyPresent(e, "PK_MATERIALCATEGORIA")){
				throw new SinedException("C�digo (cdmaterialcategoria) da categoria j� cadastrado no sistema.");
			}else {
				throw new SinedException(e.getMessage());
			}
				
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Materialcategoria bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Categoria n�o pode ser exclu�da, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	public boolean verificaPai(Materialcategoria bean){
		boolean retorno = true;
		if(bean.getMaterialcategoriapai() != null && bean.getCdmaterialcategoria() == bean.getMaterialcategoriapai().getCdmaterialcategoria())
			retorno = false;
		return retorno;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Materialcategoria form) throws Exception {
		if (form.getCdmaterialcategoria()==null && "true".equalsIgnoreCase(request.getParameter("criarCategoriaSuperior"))){
			form.setCriarCategoriaSuperior(true);
			form.setMaterialcategoriapai(materialcategoriaService.load(new Materialcategoria(Integer.valueOf(request.getParameter("cdmaterialcategoriapai")))));
			form.setAtivo(Boolean.TRUE);
		}
		
		super.entrada(request, form);
	}
}