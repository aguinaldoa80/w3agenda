package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.service.CartaocreditoService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CartaocreditoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Cartaocredito", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "numero", "vconta.saldoatual", "limite", "ativo"})
public class CartaocreditoCrud extends CrudControllerSined<CartaocreditoFiltro,Conta,Conta> {

	private EmpresaService empresaService;
	private CartaocreditoService cartaocreditoService;
	private ContaService contaService;
	
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setCartaocreditoService(CartaocreditoService cartaocreditoService) {
		this.cartaocreditoService = cartaocreditoService;
	}
	
	@Override
	protected Conta criar(WebRequestContext request, Conta form) throws Exception {
		Conta bean = super.criar(request, form);
		bean.setAtivo(Boolean.TRUE);
		return bean;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Conta form)throws Exception {
		if(form.getCdconta()!=null){
			form.setSaldoatual(contaService.calculaSaldoAtual(form));
		}
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Conta bean)throws Exception {
		bean.setContatipo(new Contatipo(Contatipo.CARTAO_DE_CREDITO));
		bean.setNome(bean.getNome().trim());
		super.salvar(request, bean);
	}
	
	@Override
	protected void excluir(WebRequestContext request, Conta bean)throws Exception {
		try {			
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Cart�o de Cr�dito n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
		
	@Override
	protected void validateBean(Conta bean, BindException errors) {
		if(cartaocreditoService.existeNomeCartaocredito(bean)){
			errors.reject("001","Descri��o de cart�o de cr�dito j� cadastrada no sistema.");
		}
		if(cartaocreditoService.existeNumeroCartaocredito(bean)){
			errors.reject("001","N�mero de cart�o de cr�dito j� cadastrado no sistema.");
		}
		if(bean.getLimite()!=null && bean.getLimite().getValue().doubleValue()<0){
			errors.reject("001","Limite n�o pode ser negativo.");
		}
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Conta form) {
		return new ModelAndView("crud/cartaocreditoEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, CartaocreditoFiltro filtro) {
		return new ModelAndView("crud/cartaocreditoListagem");
	}

}
