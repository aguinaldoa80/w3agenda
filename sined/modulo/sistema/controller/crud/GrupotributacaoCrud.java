package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Timestamp;
import java.util.Date;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Grupotributacao;
import br.com.linkcom.sined.geral.bean.Grupotributacaohistorico;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.CfopService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.GrupotributacaoService;
import br.com.linkcom.sined.geral.service.GrupotributacaohistoricoService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.MaterialtipoService;
import br.com.linkcom.sined.geral.service.NaturezaoperacaoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PopUpIcmsFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PopUpIcmsSTFiltro;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.filter.PopUpImpostoFiltro;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GrupotributacaoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Grupotributacao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "operacao", "cfopgrupo", "tributadoicms"})
public class GrupotributacaoCrud extends CrudControllerSined<GrupotributacaoFiltro, Grupotributacao, Grupotributacao> {
	
	private MaterialgrupoService materialgrupoService;
	private EmpresaService empresaService;
	private CfopService cfopService;
	private NaturezaoperacaoService naturezaoperacaoService;
	private GrupotributacaoService grupotributacaoService;
	private GrupotributacaohistoricoService grupotributacaohistoricoService;
	private MaterialtipoService materialtipoService;
	private ParametrogeralService parametrogeralService;
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setCfopService(CfopService cfopService) {
		this.cfopService = cfopService;
	}
	public void setNaturezaoperacaoService(NaturezaoperacaoService naturezaoperacaoService) {
		this.naturezaoperacaoService = naturezaoperacaoService;
	}
	public void setGrupotributacaoService(GrupotributacaoService grupotributacaoService) {
		this.grupotributacaoService = grupotributacaoService;
	}
	public void setGrupotributacaohistoricoService(GrupotributacaohistoricoService grupotributacaohistoricoService) {
		this.grupotributacaohistoricoService = grupotributacaohistoricoService;
	}
	public void setMaterialtipoService(MaterialtipoService materialtipoService) {
		this.materialtipoService = materialtipoService;
	}
	
	@Override
	public ModelAndView doEntrada(WebRequestContext request, Grupotributacao form)	throws CrudException {
		String cdgrupotributacao = request.getParameter("cdgrupotributacao");
		String stCopiar = request.getParameter("copiar");
		if (StringUtils.isNotEmpty(cdgrupotributacao) && StringUtils.isNotEmpty(stCopiar) && stCopiar.equals("true")){
			form.setCdgrupotributacao(Integer.parseInt(cdgrupotributacao));
			form = grupotributacaoService.loadForEntrada(form);
			form.setCdgrupotributacao(null);
			grupotributacaoService.limparReferenciasForCopiar(form);
		}
		
		
		return super.doEntrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request, GrupotributacaoFiltro filtro) throws Exception {
		if(filtro.getCfopgrupo() != null && filtro.getCfopgrupo().getCdcfop() != null){
			filtro.setCfopgrupo(cfopService.load(filtro.getCfopgrupo(), "cfop.cdcfop, cfop.codigo, cfop.descricaoresumida"));
		}
		super.listagem(request, filtro);
	}

	@Override
	protected void entrada(WebRequestContext request, Grupotributacao form)	throws Exception {
		request.setAttribute("listaMaterialgrupo", materialgrupoService.findAtivos());
		request.setAttribute("listaEmpresa", empresaService.findAtivosByUsuario());
		request.setAttribute("listaNaturezaoperacao", naturezaoperacaoService.find(null, null, Boolean.TRUE));
		request.setAttribute("listaMaterialtipo", materialtipoService.findAtivos());

		if (form.getCdgrupotributacao() != null){
			form.setListaGrupotributacaohistorico(grupotributacaohistoricoService.findByGrupotributacao(form));
		}
	}
	
	@Override
	protected void validateBean(Grupotributacao bean, BindException errors) {
		if(bean.getCondicao() != null && !"".equals(bean.getCondicao())){
			try {
				Object resultado = null;
				ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
				
				String startsWithJs = 	"if (typeof String.prototype.startsWith != 'function') { " +
						" 	String.prototype.startsWith = function (str){ " +
						"    	return this.indexOf(str) == 0; " +
						"  	}; " +
						"} \n" +
						"	function getData(data) {" +
						"		var arrayData = data.split('/');" +
						"		var novaData = new Date(arrayData[1] + '-' + arrayData[0] + '' + arrayData[2]);" +
						"		return novaData.getTime();" +
						" } ";
				
				engine.put("material_ncm", "");
				engine.put("material_nome", "");
				engine.put("material_tipo", "");
				engine.put("material_origem", "");
				engine.put("empresa_uf", "");
				engine.put("cliente_cnpj", "");
				engine.put("cliente_cpf", "");
				engine.put("cliente_nomefantasia", "");
				engine.put("cliente_municipio", "");
				engine.put("cliente_uf", "");
				engine.put("cliente_regimetributacao", "");
				engine.put("cliente_incidiriss", "");
				engine.put("cliente_codigotributacao", "");
				engine.put("cliente_categoria", "");
				engine.put("cliente_contribuinteICMS", "");
				engine.put("cliente_contribuinteicmstipo", "");
				engine.put("cliente_consumidorfinal", "");
				engine.put("fornecedor_uf", "");
				engine.put("fornecedor_categoria", "");
				engine.put("modelo", "");
				engine.put("consumidor_final", "");
				engine.put("nota_dtemissao", new Date(System.currentTimeMillis()));				
				engine.put("nota_tipooperacao", "");
				
				resultado = engine.eval(startsWithJs + bean.getCondicao());
				if (resultado != null && SinedException.class.equals(resultado.getClass()))
					throw (SinedException) resultado;
			} catch (ScriptException e) {
				errors.reject("001","Express�o inv�lida: '" + bean.getCondicao() + "'.");
			} catch (SinedException e) {
				errors.reject("001", e.getMessage());
			}
		}
		if(StringUtils.isNotEmpty(bean.getFormulabcicms())){
			String mensagemErro = grupotributacaoService.validaFormulaBCICMS(bean);
			if(StringUtils.isNotEmpty(mensagemErro)){
				errors.reject("001", "Erro na f�rmula de base de c�lculo de icms: " + mensagemErro);
			}
		}
		if(StringUtils.isNotEmpty(bean.getFormulabcicmsdestinatario())){
			String mensagemErro = grupotributacaoService.validaFormulaBCICMSDestinatario(bean);
			if(StringUtils.isNotEmpty(mensagemErro)){
				errors.reject("001", "Erro na f�rmula de base de c�lculo de icms destinat�rio: " + mensagemErro);
			}
		}
		if(StringUtils.isNotEmpty(bean.getFormulabcst())){
			String mensagemErro = grupotributacaoService.validaFormulaBCST(bean);
			if(StringUtils.isNotEmpty(mensagemErro)){
				errors.reject("001", "Erro na f�rmula de base de c�lculo de icmsst: " + mensagemErro);
			}
				
		}
		if(StringUtils.isNotEmpty(bean.getFormulavalorst())){
			String mensagemErro = grupotributacaoService.validaFormulaVALORST(bean);
			if(StringUtils.isNotEmpty(mensagemErro)){
				errors.reject("001", "Erro na f�rmula de valor de icmsst: " + mensagemErro);
			}
				
		}
		if(StringUtils.isNotEmpty(bean.getFormulabcipi())){
			String mensagemErro = grupotributacaoService.validaFormulaBCIPI(bean);
			if(StringUtils.isNotEmpty(mensagemErro)){
				errors.reject("001", "Erro na f�rmula de base de c�lculo de ipi: " + mensagemErro);
			}
				
		}
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Grupotributacao bean) throws Exception {

		Grupotributacaohistorico historico = new Grupotributacaohistorico();
		historico.setCdusuarioaltera(SinedUtil.getUsuarioLogado().getCdpessoa());
		historico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		historico.setGrupotributacao(bean);

		if (bean.getCdgrupotributacao() != null)
			historico.setObservacao("Altera��o realizada.");
		else
			historico.setObservacao("Registro criado.");
		
		super.salvar(request, bean);
		
		grupotributacaohistoricoService.saveOrUpdate(historico);
	}
	
	/**
	* M�todo que abre a popup de imposto
	*
	* @param request
	* @param filtro
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public ModelAndView popUpImposto(WebRequestContext request, PopUpImpostoFiltro filtro){
		return new ModelAndView("direct:/crud/popup/popUpImposto" + filtro.getTelaImposto(), "filtro", filtro);
	}
	
	/**
	* M�todo que abre a popup de imposto icms
	*
	* @param request
	* @param filtro
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public ModelAndView popUpIcms(WebRequestContext request, PopUpIcmsFiltro filtro){
		request.setAttribute("CALCULAR_DIFAL_NFE", parametrogeralService.getBoolean(Parametrogeral.CALCULAR_DIFAL_NFE));
		return new ModelAndView("direct:/crud/popup/popUpImpostoICMS", "filtro", filtro);
	}
	
	/**
	* M�todo que abre a popup de imposto icmsst
	*
	* @param request
	* @param filtro
	* @return
	* @since 29/06/2015
	* @author Luiz Fernando
	*/
	public ModelAndView popUpIcmsST(WebRequestContext request, PopUpIcmsSTFiltro filtro){
		return new ModelAndView("direct:/crud/popup/popUpImpostoICMSST", "filtro", filtro);
	}
}
