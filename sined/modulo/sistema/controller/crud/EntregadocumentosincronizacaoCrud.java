package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Entregadocumentosincronizacao;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EntregadocumentosincronizacaoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Entregadocumentosincronizacao", authorizationModule=CrudAuthorizationModule.class)
public class EntregadocumentosincronizacaoCrud extends CrudControllerSined<EntregadocumentosincronizacaoFiltro, Entregadocumentosincronizacao, Entregadocumentosincronizacao> {
	
	@Override
	public boolean isPaginacaoSimples() {
		return true;
	}
	
	@Override
	public ModelAndView doCriar(WebRequestContext request, Entregadocumentosincronizacao form) throws CrudException {
		throw new RuntimeException("N�o � permitido executar esta a��o.");
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request,	Entregadocumentosincronizacao form) throws CrudException {
		String acao = request.getParameter("ACAO");
		if(acao == null || !"consultar".equals(acao)){
			throw new RuntimeException("N�o � permitido executar esta a��o.");
		}
		return super.doEditar(request, form);
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Entregadocumentosincronizacao form) throws CrudException {
		throw new RuntimeException("N�o � permitido executar esta a��o.");
	}
	
}
