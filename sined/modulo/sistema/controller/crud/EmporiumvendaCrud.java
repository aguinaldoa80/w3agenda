package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Emporiumvenda;
import br.com.linkcom.sined.geral.bean.Emporiumvendahistorico;
import br.com.linkcom.sined.geral.bean.enumeration.Emporiumvendaacao;
import br.com.linkcom.sined.geral.service.EmporiumvendaService;
import br.com.linkcom.sined.geral.service.EmporiumvendahistoricoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmporiumvendaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Emporiumvenda", authorizationModule=CrudAuthorizationModule.class)
public class EmporiumvendaCrud extends CrudControllerSined<EmporiumvendaFiltro, Emporiumvenda, Emporiumvenda> {

	private EmporiumvendahistoricoService emporiumvendahistoricoService;
	private EmporiumvendaService emporiumvendaService;
	
	public void setEmporiumvendahistoricoService(
			EmporiumvendahistoricoService emporiumvendahistoricoService) {
		this.emporiumvendahistoricoService = emporiumvendahistoricoService;
	}
	public void setEmporiumvendaService(EmporiumvendaService emporiumvendaService) {
		this.emporiumvendaService = emporiumvendaService;
	}

	@Override
	protected void entrada(WebRequestContext request, Emporiumvenda form) throws Exception {
		if(form.getCdemporiumvenda() != null){
			form.setListaEmporiumvendahistorico(emporiumvendahistoricoService.findByEmporiumvenda(form));
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Emporiumvenda bean) throws Exception {
		boolean isCriar = bean.getCdemporiumvenda() == null;
		
		super.salvar(request, bean);
		
		Emporiumvendahistorico emporiumvendahistorico = new Emporiumvendahistorico();
		emporiumvendahistorico.setEmporiumvenda(bean);
		if(isCriar){
			emporiumvendahistorico.setAcao(Emporiumvendaacao.CRIADA);
		} else {
			emporiumvendahistorico.setAcao(Emporiumvendaacao.ALTERADA);
		}
		emporiumvendahistoricoService.saveOrUpdate(emporiumvendahistorico);
	}
	
	public ModelAndView processarCupom(WebRequestContext request) throws Exception {
		String whereIn = SinedUtil.getItensSelecionados(request);
		
		if(StringUtils.isBlank(whereIn)){
			request.addError("Nenhum item selecionado.");
		}else {
			List<Emporiumvenda> lista = emporiumvendaService.findForValidacao(whereIn);
			if(SinedUtil.isListNotEmpty(lista)){
				StringBuilder msg = new StringBuilder();
				boolean erro = false;
				for(Emporiumvenda emporiumvenda : lista){
					if(emporiumvenda.getVenda() != null){
						msg.append(emporiumvenda.getNumero_ticket()).append(",");
						erro = true;
					}
				}
				
				if(erro){
					request.addError("Existe cupom com venda j� criada. N�mero do ticket: " + msg.substring(0, msg.length()-1));
				}else {
					emporiumvendaService.processaVendasPendentes(null, false, null, whereIn);
					request.addMessage("Processamento realizado com sucesso.");
				}
			}
		}
		
		return sendRedirectToAction("listagem");
	}
}
