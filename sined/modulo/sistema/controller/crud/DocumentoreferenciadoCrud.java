package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documentoreferenciado;
import br.com.linkcom.sined.geral.service.ContagerencialService;
import br.com.linkcom.sined.geral.service.DocumentoreferenciadoService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentoreferenciadoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Documentoreferenciado", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "modelodocumento", "documentotipo", "fornecedor"})
public class DocumentoreferenciadoCrud extends CrudControllerSined<DocumentoreferenciadoFiltro, Documentoreferenciado, Documentoreferenciado> {
	
	private DocumentoreferenciadoService documentoreferenciadoService;
	private ContagerencialService contagerencialService;
	private ProjetoService projetoService;
	private EmpresaService empresaService;
	
	public void setDocumentoreferenciadoService(DocumentoreferenciadoService documentoreferenciadoService) {this.documentoreferenciadoService = documentoreferenciadoService;}
	public void setContagerencialService(ContagerencialService contagerencialService) {this.contagerencialService = contagerencialService;}
	public void setProjetoService(ProjetoService projetoService) {this.projetoService = projetoService;}
	public void setEmpresaService(EmpresaService empresaService) {this.empresaService = empresaService;}
	
	@Override
	protected void entrada(WebRequestContext request, Documentoreferenciado form) throws Exception {
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
		request.setAttribute("listaProjetoUsuarioNotCancelado", projetoService.findForComboByUsuariologadoNotCancelado());
	}
	
	@Override
	protected Documentoreferenciado criar(WebRequestContext request, Documentoreferenciado form) throws Exception {
		Documentoreferenciado bean = super.criar(request, form);
		bean.setEmpresa(empresaService.loadPrincipal());
		return bean;
	}
	
	@Override
	protected void listagem(WebRequestContext request, DocumentoreferenciadoFiltro filtro) throws Exception {
		request.setAttribute("whereInNaturezaContagerencial", contagerencialService.getWhereInNaturezaCompensacaoContaGerencialOutras());
	}
	
	@Override
	protected void validateBean(Documentoreferenciado bean, BindException errors) {
		if(bean.getEmpresa() != null && bean.getImposto() != null && documentoreferenciadoService.existeEmpresaImposto(bean)){
			errors.reject("001", "J� existe um documento referenciado com o imposto e empresa cadastrado no sistema.");
		}
	}
}
