package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Formulacusto;
import br.com.linkcom.sined.geral.bean.Formulacustoitem;
import br.com.linkcom.sined.geral.service.FormulacustoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.FormulacustoFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Formulacusto", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class FormulacustoCrud extends CrudControllerSined<FormulacustoFiltro, Formulacusto, Formulacusto> {
	
	private FormulacustoService formulacustoService;
	
	public void setFormulacustoService(FormulacustoService formulacustoService) {
		this.formulacustoService = formulacustoService;
	}

	@Override
	protected Formulacusto criar(WebRequestContext request, Formulacusto form) throws Exception {
		if(!formulacustoService.existsFormulaAtiva(form))
			form.setAtivo(Boolean.TRUE);
		return super.criar(request, form);
	}
	
	@Override
	protected void validateBean(Formulacusto bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaFormulacustoitem())){
			List<Integer> listaOrdem = new ArrayList<Integer>();
			List<String> listaIdentificador= new ArrayList<String>();
			for(Formulacustoitem formulacustoitem : bean.getListaFormulacustoitem()){	
				 if(StringUtils.isNotEmpty(formulacustoitem.getIdentificador())){
					 if(listaIdentificador.contains(formulacustoitem.getIdentificador())){
						 errors.reject("001", "N�o deve exitir dois identificadores iguais.");
					 }
					 listaIdentificador.add(formulacustoitem.getIdentificador());
				 }
				 if(formulacustoitem.getOrdem() != null){
					 if(listaOrdem.contains(formulacustoitem.getOrdem())){
						 errors.reject("001", "N�o deve existir duas ordens iguais.");
					 }
					 listaOrdem.add(formulacustoitem.getOrdem());
				 }
			}
		}else {
			errors.reject("001", "Pelo menos uma f�rmula deve ser inserida");
		}
		
		if(bean.getAtivo() != null && bean.getAtivo() && formulacustoService.existsFormulaAtiva(bean)){
			errors.reject("001", "Somente uma f�rmula pode estar ativa");
		}
	}
}
