package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.sined.geral.bean.Conta;
import br.com.linkcom.sined.geral.bean.ContaContabil;
import br.com.linkcom.sined.geral.bean.Contagerencial;
import br.com.linkcom.sined.geral.bean.Contapapel;
import br.com.linkcom.sined.geral.bean.Contatipo;
import br.com.linkcom.sined.geral.service.CaixaService;
import br.com.linkcom.sined.geral.service.ContaPapelService;
import br.com.linkcom.sined.geral.service.ContaService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.CaixaFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

import com.ibm.icu.text.SimpleDateFormat;

@Controller(path="/sistema/crud/Caixa", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "vconta.saldoatual", "desconsiderarsaldoinicialfluxocaixa", "ativo"})
public class CaixaCrud extends CrudControllerSined<CaixaFiltro,Conta,Conta> {

	private EmpresaService empresaService;
	private CaixaService caixaService;
	private ContaService contaService;
	private ContaPapelService contaPapelService;

	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	public void setCaixaService(CaixaService caixaService) {
		this.caixaService = caixaService;
	}
	public void setContaPapelService(ContaPapelService contaPapelService) {
		this.contaPapelService = contaPapelService;
	}
	
	@Override
	public ModelAndView doEditar(WebRequestContext request, Conta form)	throws CrudException {
		if(!contaService.isUsuarioPermissaoConta(form)){
			request.addError("Usu�rio n�o tem permiss�o para consultar/editar esta caixa!");
			SinedUtil.redirecionamento(request, "/sistema/crud/Caixa");
			return null;
		}
		return super.doEditar(request, form);
	}

	@Override
	protected void salvar(WebRequestContext request, Conta bean)throws Exception {
		bean.setContatipo(new Contatipo(Contatipo.CAIXA));
		bean.setNome(bean.getNome().trim());
		if(bean.getSaldo() == null){
			bean.setSaldo(new Money(0));
		}
		
		super.salvar(request, bean);
		
//		ATUALIZA (SALVA DELETA E ALTERA) A LISTA DE PAPEL
		String whereIn = SinedUtil.listAndConcatenate(bean.getListaContapapel(), "cdcontapapel", ",");
		contaPapelService.deleteListContaPapel(whereIn, bean);
		
		for (Contapapel cp : bean.getListaContapapel()) {
			cp.setConta(bean);
			contaPapelService.saveOrUpdate(cp);
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Conta bean)throws Exception {
		try {			
			super.excluir(request, bean);
		} catch (DataIntegrityViolationException e) {
			throw new SinedException("Caixa n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");		
		}
	}
	
		
	@Override
	protected void validateBean(Conta bean, BindException errors) {
		if(bean.getCdconta() != null){
			bean.setSaldoatual(contaService.calculaSaldoAtual(bean));
		}
		if(caixaService.existeNomeCaixa(bean)){
			errors.reject("001","Descri��o de caixa j� cadastrada no sistema.");
		}
		if(bean.getSaldo()!=null && bean.getSaldo().getValue().doubleValue()<0){
			errors.reject("001","Saldo inicial n�o pode ser negativo.");
		}
		if(bean.getDtsaldo() != null){
			if(bean.getDtsaldo().after(new java.sql.Date(System.currentTimeMillis()))){
				errors.reject("001","A Data do saldo inicial n�o poder� ser futura.");
			}
			
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Conta form)throws Exception {
		request.setAttribute("hoje", new SimpleDateFormat("dd/MM/yyyy").format(new Date(System.currentTimeMillis())));
		if(form.getCdconta()!=null){
			form.setSaldoatual(contaService.calculaSaldoAtual(form));
			if(!request.getBindException().hasErrors()){
				form.setListaContapapel(contaPapelService.findByConta(form));
			}
		}
		
		if (form.getContaContabil() != null){
			form.setContaContabil(new ContaContabil(form.getContaContabil().getCdcontacontabil()));
		}
		request.setAttribute("listaEmpresa", empresaService.findAtivos(form.getEmpresa()));
	}
	
	@Override
	protected Conta criar(WebRequestContext request, Conta form)throws Exception {
		Conta conta = super.criar(request, form);
		conta.setAtivo(true);
		conta.setDtsaldo(new Date(System.currentTimeMillis()));
		return conta;
	}
		
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request,Conta form) {
		return new ModelAndView("crud/caixaEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request,CaixaFiltro filtro) {
		return new ModelAndView("crud/caixaListagem");
	}
}
