package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MunicipioSistemaFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Municipio"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "uf"})
public class MunicipioCrud extends CrudControllerSined<MunicipioSistemaFiltro, Municipio, Municipio> {

}
