package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Prazopagamentoitem;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PrazopagamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@CrudBean
@Controller(path="/sistema/crud/Prazopagamento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class PrazopagamentoCrud extends CrudControllerSined<PrazopagamentoFiltro, Prazopagamento, Prazopagamento>{
	
	@Override
	protected void entrada(WebRequestContext request, Prazopagamento form) throws Exception {
		if(form.getCdprazopagamento() == null){
			form.setAvista(Boolean.TRUE);
			form.setPrazomedio(Boolean.TRUE);
		}
		if(form.getCdprazopagamento() != null){
			if(form.getPrazomedio() != null && form.getPrazomedio() && form.getListaPagamentoItem() != null && !form.getListaPagamentoItem().isEmpty()){
				form.setPrazomediodias(form.getListaPagamentoItem().get(0).getDias());				
			}
		}
		String nome = request.getParameter("marcarcompra");
		if("TRUE".equalsIgnoreCase(nome)){
			form.setProcessoDeVenda(true);
		}
		super.entrada(request, form);
	}
	@Override
	protected void salvar(WebRequestContext request, Prazopagamento bean) throws Exception {
		if(bean.getPrazomedio() != null && bean.getPrazomedio()){
			if(bean.getPrazomediodias()	!= null){
				List<Prazopagamentoitem> lista = new ArrayList<Prazopagamentoitem>();
				lista.add(new Prazopagamentoitem(1,bean.getPrazomediodias(),null));
				bean.setListaPagamentoItem(lista);
			}
		}
		
		try {
			bean.setNome(bean.getNome().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_PRAZOPAGAMENTO_NOME")) {
				throw new SinedException("Prazo de Pagamento j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void validateBean(Prazopagamento bean, BindException errors) {
		Integer somaDiasMeses = 0;
		
		if(bean.getPrazomedio() != null && bean.getPrazomedio()){
			if(bean.getPrazomediodias() == null){
				errors.reject("001", "O campo dias � obrigat�rio.");
			}
		}else if(bean.getListaPagamentoItem() == null || bean.getListaPagamentoItem().size() == 0){
			errors.reject("001", "O Prazo de Pagamento deve possuir pelo menos uma parcela.");
		}else{
			for (Prazopagamentoitem item : bean.getListaPagamentoItem()) {
				if(item.getDias() == null  && item.getMeses() == null)
					errors.reject("002", "A parcela "+item.getParcela()+" n�o pode ter dias ou meses sem valor.");
				
				Integer dias = item.getDias() == null ? 0 : item.getDias();
				Integer meses = item.getMeses() == null ? 0 : item.getMeses();
				
				if(item.getParcela() > 1)
					if(somaDiasMeses >= dias + meses * 30)
						errors.reject("003", "A parcela "+item.getParcela()+" n�o pode ter a soma dias e meses menor ou igual as parcelas anteriores.");
				
				somaDiasMeses = dias + meses * 30;
			}
		}
	}
	
}
