package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Declaracaoimportacao;
import br.com.linkcom.sined.geral.service.DeclaracaoimportacaoService;
import br.com.linkcom.sined.modulo.faturamento.controller.process.ImportarXmlDeclaracaoImportacaoProcess;
import br.com.linkcom.sined.modulo.faturamento.controller.process.bean.DeclaracaoImportacaoBean;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path={"/sistema/crud/Declaracaoimportacao"}, authorizationModule=CrudAuthorizationModule.class)
public class DeclaracaoimportacaoCrud extends CrudControllerSined<FiltroListagemSined, Declaracaoimportacao, Declaracaoimportacao> {	
	
	private DeclaracaoimportacaoService declaracaoimportacaoService;
	
	public void setDeclaracaoimportacaoService(
			DeclaracaoimportacaoService declaracaoimportacaoService) {
		this.declaracaoimportacaoService = declaracaoimportacaoService;
	}
	
	@Override
	protected Declaracaoimportacao criar(WebRequestContext request, Declaracaoimportacao form) throws Exception {
		if("true".equalsIgnoreCase(request.getParameter("importacaoXML"))){
			DeclaracaoImportacaoBean declaracaoImportacaoBean = (DeclaracaoImportacaoBean) request.getSession().getAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_BEAN_DI_SESSAO);
			if(declaracaoImportacaoBean != null && declaracaoImportacaoBean.getDeclaracaoimportacao() != null){
				return declaracaoImportacaoBean.getDeclaracaoimportacao();
			}
		}
		return super.criar(request, form);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Declaracaoimportacao bean) {
		if(Boolean.TRUE.equals(bean.getImportacaoXML())){
			DeclaracaoImportacaoBean declaracaoImportacaoBean = (DeclaracaoImportacaoBean) request.getSession().getAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_BEAN_DI_SESSAO);
			if(declaracaoImportacaoBean != null){
				declaracaoImportacaoBean.setDeclaracaoimportacao(bean);
				request.getSession().setAttribute(ImportarXmlDeclaracaoImportacaoProcess.ATTR_BEAN_DI_SESSAO, declaracaoImportacaoBean);
				SinedUtil.redirecionamento(request, "/faturamento/process/ImportarXMLDeclaracaoImportacao?ACAO=conferenciaDIcliente");
				return null;
			}
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Declaracaoimportacao bean)	throws Exception {
		declaracaoimportacaoService.ajustaDIToSave(bean);
		super.salvar(request, bean);
	}
	
	public ModelAndView consultarDetalhesDeclaracaoAjax(WebRequestContext request, Declaracaoimportacao declaracaoimportacao){
		JsonModelAndView jsonModelAndView = new JsonModelAndView();
		
		if(declaracaoimportacao != null && declaracaoimportacao.getCddeclaracaoimportacao() != null){
			declaracaoimportacao = declaracaoimportacaoService.loadForConsulta(declaracaoimportacao);
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td align='right'><b>Número:</b></td>").append("<td>").append(declaracaoimportacao.getNumerodidsidaFormatado()).append("</td>");
			sb.append("<td align='right'><b>Via de Transp.:</b></td>").append("<td>").append(declaracaoimportacao.getViatransporte() != null ? declaracaoimportacao.getViatransporte().getDescricao() : "").append("</td>");
			sb.append("<td align='right'><b>Data desembaraço:</b></td>").append("<td>").append(SinedDateUtils.toString(declaracaoimportacao.getData())).append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td align='right'><b>Data registro:</b></td>").append("<td>").append(SinedDateUtils.toString(declaracaoimportacao.getDtregistro())).append("</td>");
			sb.append("<td align='right'><b>Valor AFRMM:</b></td>").append("<td>").append("R$ " + declaracaoimportacao.getValorafrmm().toString()).append("</td>");
			sb.append("<td align='right'><b>UF desembaraço:</b></td>").append("<td>").append(declaracaoimportacao.getUf() != null ? declaracaoimportacao.getUf().getNome() : "").append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td align='right'><b>Cod. exportador:</b></td>").append("<td>").append(declaracaoimportacao.getCodigoexportador()).append("</td>");
			sb.append("<td align='right'><b>Forma de interm.:</b></td>").append("<td>").append(declaracaoimportacao.getFormaintermediacao() != null ? declaracaoimportacao.getFormaintermediacao().getDescricaoReduzida() : "").append("</td>");
			sb.append("<td align='right'><b>Local desembaraço:</b></td>").append("<td>").append(declaracaoimportacao.getLocaldesembaraco()).append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
			jsonModelAndView.addObject("conteudo", sb.toString());
		}
		
		 return jsonModelAndView;
	}
	
}
