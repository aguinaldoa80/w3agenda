package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Segmento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.SegmentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean		  
@Controller(path="/sistema/crud/Segmento", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class SegmentoCrud extends CrudControllerSined<SegmentoFiltro, Segmento, Segmento> {

	@Override
	protected void salvar(WebRequestContext request, Segmento bean)	throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_segmento_nome")) 
				throw new SinedException("Segmento j� cadastrado no sistema.");	
		}	}
	
}
