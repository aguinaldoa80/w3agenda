package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Garantiatipo;
import br.com.linkcom.sined.geral.bean.Garantiatipopercentual;
import br.com.linkcom.sined.geral.service.GarantiatipoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiatipoFiltro;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Garantiatipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "geragarantia", "ativo"})
public class GarantiatipoCrud extends CrudControllerSined<GarantiatipoFiltro, Garantiatipo, Garantiatipo> {

	private GarantiatipoService garantiatipoService;
	
	public void setGarantiatipoService(GarantiatipoService garantiatipoService) {
		this.garantiatipoService = garantiatipoService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Garantiatipo bean)
			throws Exception {
		List<Garantiatipo> garantiatipoList = garantiatipoService.findByDescricao(bean.getDescricao(), bean.getCdgarantiatipo());
		if(!garantiatipoList.isEmpty()){
			throw new SinedException("Tipo de Garantia j� cadastrado no sistema.");
		}
		if(bean.getListaGarantiatipopercentual() != null && !bean.getListaGarantiatipopercentual().isEmpty()){
			List<Double> percentuais = new ArrayList<Double>();
			for(Garantiatipopercentual garantiatipopercentual: bean.getListaGarantiatipopercentual()){
				if(garantiatipopercentual.getPercentual() < 0){
					throw new SinedException("O campo % de Garantia deve ter valor maior ou igual a 0.");
				}else if(percentuais.contains(garantiatipopercentual.getPercentual())){
					throw new SinedException("Percentual "+ garantiatipopercentual.getPercentual() +" duplicado no cadastro.");
				}else{
					percentuais.add(garantiatipopercentual.getPercentual());
				}
			}
		}else{
			if(Boolean.TRUE.equals(bean.getGeragarantia())){
				throw new SinedException("Pelo menos um % de garantia deve ser informado.");
			}
		}
		super.salvar(request, bean);
	}
	
	@Override
	protected Garantiatipo criar(WebRequestContext request, Garantiatipo form)
			throws Exception {
		Garantiatipo bean = super.criar(request, form);
		bean.setAtivo(true);
		return bean;
	}
}
