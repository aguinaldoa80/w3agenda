package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Localarmazenagem;
import br.com.linkcom.sined.geral.bean.Municipio;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.GerenciamentomaterialService;
import br.com.linkcom.sined.geral.service.LocalarmazenagemService;
import br.com.linkcom.sined.geral.service.MunicipioService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.LocalarmazenagemFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Localarmazenagem", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "localidade", "enderecoAux", "ativo"})
public class LocalarmazenagemCrud extends CrudControllerSined<LocalarmazenagemFiltro, Localarmazenagem, Localarmazenagem>{
	
	private MunicipioService municipioService;
	private EmpresaService empresaService;
	private LocalarmazenagemService localarmazenagemService;
	private GerenciamentomaterialService gerenciamentomaterialService;
	
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public void setLocalarmazenagemService(LocalarmazenagemService localarmazenagemService) {
		this.localarmazenagemService = localarmazenagemService;
	}
	public void setGerenciamentomaterialService(GerenciamentomaterialService gerenciamentomaterialService) {
		this.gerenciamentomaterialService = gerenciamentomaterialService;
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Localarmazenagem form) throws CrudException {
		if (validarSalvar(request, form)){
			return super.doSalvar(request, form);
		}else {
			return doEntrada(request, form);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Localarmazenagem bean) throws Exception {
		if(bean.getListaEmpresa() != null && bean.getListaEmpresa().size() > 0){
			localarmazenagemService.preencheLocalarmazenagemEmpresa(request, bean);
		}
		
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_LOCALARMAZENAGEM_NOME")) {
				throw new SinedException("Local de Armazenagem j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void validateBean(Localarmazenagem bean, BindException errors) {
		if(bean.getCdlocalarmazenagem() != null){
			Localarmazenagem localarmazenagem = localarmazenagemService.load(bean, "localarmazenagem.cdlocalarmazenagem, localarmazenagem.ativo");
			if((bean.getAtivo() == null || !bean.getAtivo()) && localarmazenagem != null && localarmazenagem.getAtivo() != null && localarmazenagem.getAtivo()){
				if(localarmazenagemService.existePedidoOuVendaPrevista(bean)){
					errors.reject("001", "N�o foi poss�vel inativar este local de armazenagem pois possui pedido de venda e/ou venda vinculado a ele com situa��o 'prevista' e ou 'aguardando aprova��o'.");
				}
				if(gerenciamentomaterialService.existeQtdeEmEstoque(bean)){
					errors.reject("001", "N�o foi poss�vel inativar este local de armazenagem pois possui quantidade dispon�vel de materiais em estoque vinculado a ele.");
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void entrada(WebRequestContext request, Localarmazenagem form) throws Exception {
		List<Empresa> listaEmpresaAtiva = empresaService.findAtivos();
		List<Empresa> listaEmpresa = null;
		List<Empresa> lista = new ArrayList<Empresa>();
		
		if (form.getCdlocalarmazenagem() != null) {
			List<Empresa> list = (List<Empresa>) CollectionsUtil.getListProperty(form.getListaempresa(), "empresa");
		
			for (Empresa empresa : list) {
				if(!listaEmpresaAtiva.contains(empresa)){
					listaEmpresaAtiva.add(empresa);
				}
			}
			
			listaEmpresa = localarmazenagemService.preencheListaEmpresa(form, listaEmpresaAtiva, lista);
		}
		
		if (!request.getBindException().hasErrors()) {
			form.setListaEmpresa(lista);
		
			request.getSession().setAttribute("listaEmpresa", listaEmpresa);
		}
		
		request.setAttribute("listaEmpresa", listaEmpresaAtiva);
	}
	
	/**
	 * M�todo para filtrar os municipios por uf.
	 * 
	 * @see br.com.linkcom.sined.geral.service.MunicipioService#findByUf(Uf)
	 * @param request
	 * @param uf
	 * @author Tom�s T. Rabelo
	 */
	public void makeAjaxMunicipio(WebRequestContext request, Uf uf){
		List<Municipio> listaMunicipio = municipioService.findByUf(uf);
		request.getServletResponse().setContentType("text/html");
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaMunicipio, "listaMunicipios", ""));
	}
	
	/**
	* M�todo ajax para carregar o municipio e uf do localarmazenagem
	*
	* @param request
	* @param localarmazenagem
	* @since Sep 9, 2011
	* @author Luiz Fernando F Silva
	*/
	public void ajaxCarregaUfMunicipio(WebRequestContext request, Localarmazenagem localarmazenagem){
		localarmazenagemService.ajaxCarregaUfMunicipio(request, localarmazenagem);
	}
	
	/**
	* M�todo ajax para recarregar a lista de locais de armazenagem
	*
	* @param request
	* @param empresa
	* @since 15/05/2012
	* @author Rafael Salvio Martins
	*/
	public void ajaxCarregarLocalArmazenagem(WebRequestContext request, Empresa empresa){
		List<Localarmazenagem> listaLocalarmazenagem = new ArrayList<Localarmazenagem>();
		if(empresa != null && empresa.getCdpessoa()!=null){
			listaLocalarmazenagem = localarmazenagemService.loadForVenda(empresa);
		}
	
		View.getCurrent().println(SinedUtil.convertToJavaScript(listaLocalarmazenagem, "listaLocalArmazenagem", ""));
	}
	
	private boolean validarSalvar(WebRequestContext request, Localarmazenagem form) throws CrudException {
		if (form.getListaEmpresa()!=null){
			for (Empresa empresa: form.getListaEmpresa()){
				if (Boolean.TRUE.equals(form.getPrincipal()) && !localarmazenagemService.verificarPrincipalIndenizacao(form.getCdlocalarmazenagem(), empresa, Boolean.TRUE, null)){
					request.addError("J� existe local principal definido para a empresa.");
					return false;
				}else if (Boolean.TRUE.equals(form.getIndenizacao()) && !localarmazenagemService.verificarPrincipalIndenizacao(form.getCdlocalarmazenagem(), empresa, null, Boolean.TRUE)){
					request.addError("J� existe local de indeniza��o definido para a empresa.");
					return false;
				}
			}
		}else{
			if(Boolean.TRUE.equals(form.getPrincipal())){
				Localarmazenagem localprincipalSemEmpresa = localarmazenagemService.loadPrincipalSemEmpresa();
				if(localprincipalSemEmpresa != null && !localprincipalSemEmpresa.equals(form)){
					request.addError("J� existe um local principal padr�o definido no sistema.");
					return false;
				}
			}
		}
		return true;		
	}
}