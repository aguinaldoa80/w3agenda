package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Material;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.Pneu;
import br.com.linkcom.sined.geral.bean.PneuSegmento;
import br.com.linkcom.sined.geral.bean.Pneumarca;
import br.com.linkcom.sined.geral.bean.Pneumedida;
import br.com.linkcom.sined.geral.bean.Pneumodelo;
import br.com.linkcom.sined.geral.service.MaterialService;
import br.com.linkcom.sined.geral.service.MaterialsimilarService;
import br.com.linkcom.sined.geral.service.OtrPneuTipoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PneuSegmentoService;
import br.com.linkcom.sined.geral.service.PneuService;
import br.com.linkcom.sined.geral.service.PneumarcaService;
import br.com.linkcom.sined.geral.service.PneumedidaService;
import br.com.linkcom.sined.geral.service.PneumodeloService;
import br.com.linkcom.sined.geral.service.PneuqualificacaoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.PneuFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Pneu"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"cdpneu", "pneumarca", "pneumodelo", "pneumedida", "serie", "dot", "materialbanda", "numeroreforma", "pneuqualificacao"})
public class PneuCrud extends CrudControllerSined<PneuFiltro, Pneu, Pneu> {	
	
	private PneuService pneuService;
	private MaterialsimilarService materialsimilarService;
	private MaterialService materialService;
	private ParametrogeralService parametrogeralService;
	private PneumarcaService pneumarcaService;
	private PneumodeloService pneumodeloService;
	private PneumedidaService pneumedidaService;
	private PneuqualificacaoService pneuqualificacaoService;
	private PneuSegmentoService pneuSegmentoService;
	private OtrPneuTipoService otrPneuTipoService;
	
	
	
	public void setPneuSegmentoService(PneuSegmentoService pneuSegmentoService) {
		this.pneuSegmentoService = pneuSegmentoService;
	}
	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	public void setMaterialsimilarService(
			MaterialsimilarService materialsimilarService) {
		this.materialsimilarService = materialsimilarService;
	}
	public void setPneuService(PneuService pneuService) {
		this.pneuService = pneuService;
	}
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	public void setPneumarcaService(PneumarcaService pneumarcaService) {
		this.pneumarcaService = pneumarcaService;
	}
	public void setPneumodeloService(PneumodeloService pneumodeloService) {
		this.pneumodeloService = pneumodeloService;
	}
	public void setPneumedidaService(PneumedidaService pneumedidaService) {
		this.pneumedidaService = pneumedidaService;
	}
	public void setPneuqualificacaoService(
			PneuqualificacaoService pneuqualificacaoService) {
		this.pneuqualificacaoService = pneuqualificacaoService;
	}
	public void setOtrPneuTipoService(OtrPneuTipoService otrPneuTipoService) {
		this.otrPneuTipoService = otrPneuTipoService;
	}
	
	@Override
	protected ListagemResult<Pneu> getLista(WebRequestContext request, PneuFiltro filtro) {
		boolean isPopup = Boolean.valueOf(request.getParameter("isPopup") != null ? request.getParameter("isPopup") : "false");
		boolean resgatarPneuPopUp = Boolean.valueOf(request.getParameter("resgatarPneuPopUp") != null ? request.getParameter("resgatarPneuPopUp") : "false");
		Integer cdclientePneuPopUp = request.getParameter("clientepedidovenda.cdpessoa") != null ? Integer.parseInt(request.getParameter("clientepedidovenda.cdpessoa")) : null;
		
		if(isPopup && resgatarPneuPopUp){
			request.setAttribute("isPopup", isPopup);
			request.setAttribute("resgatarPneu", resgatarPneuPopUp);
			request.setAttribute("resgatarPneuPopUp", resgatarPneuPopUp);
			if(cdclientePneuPopUp != null){
				filtro.setClientepedidovenda(new Cliente(cdclientePneuPopUp));
			}
			if(filtro.getResgatarPneu() == null){
				filtro.setResgatarPneu(true);
				filtro.setSomenteclientepedido(true);
			}
		}else {
			filtro.setSomenteclientepedido(null);
			filtro.setClientepedidovenda(null);
			filtro.setResgatarPneu(null);
		}
		return super.getLista(request, filtro);
	}
	
	public ModelAndView openPopup(WebRequestContext request, Pneu pneu){
		request.setAttribute("INTEGRACAO_VIPAL", parametrogeralService.getValorPorNome(Parametrogeral.INTEGRACAO_VIPAL).trim().toUpperCase());
		request.setAttribute("OBRIGAR_CAMPOS_PNEU", parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CAMPOS_PNEU).trim().toUpperCase());
		request.setAttribute("superDisplayName", request.getParameter("superDisplayName"));
		request.setAttribute("listaPneuSemento", pneuSegmentoService.findPneuSegmento());
		String cdmaterial = request.getParameter("cdmaterial");
		
		Boolean isObrigaBanda = pneuService.isObrigaBanda(Util.strings.isNotEmpty(cdmaterial) ? Integer.parseInt(cdmaterial) : null);
		
		if(pneu.getPneuSegmento() != null){
			pneu.getPneuSegmento().setNome(pneuSegmentoService.load(pneu.getPneuSegmento(), "pneuSegmento.cdPneuSegmento, pneuSegmento.nome").getNome());
		}else{
			pneu.setPneuSegmento(pneuSegmentoService.loadPrincipal());
		}
		
		if(pneu.getPneumarca() != null){
			pneu.getPneumarca().setNome(pneumarcaService.load(pneu.getPneumarca(), "pneumarca.cdpneumarca, pneumarca.nome").getNome());
		}
		if(pneu.getPneumodelo() != null){
			pneu.getPneumodelo().setNome(pneumodeloService.load(pneu.getPneumodelo(), "pneumodelo.cdpneumodelo, pneumodelo.nome").getNome());
		}
		if(pneu.getOtrPneuTipo() != null){
			pneu.getOtrPneuTipo().setNome(otrPneuTipoService.load(pneu.getOtrPneuTipo(), "otrPneuTipo.cdOtrPneuTipo, otrPneuTipo.nome").getNome());
		}
		if(pneu.getPneumedida() != null){
			pneu.getPneumedida().setNome(pneumedidaService.load(pneu.getPneumedida(), "pneumedida.cdpneumedida, pneumedida.nome").getNome());
		}else if(!pneu.existeDados() && Util.strings.isNotEmpty(cdmaterial)){
			Material material = materialService.load(new Material(Integer.parseInt(cdmaterial)), "material.cdmaterial, material.pneumedida");
			if(material != null && material.getPneumedida() != null){
				pneu.setPneumedida(material.getPneumedida());
			}
		}
		if(pneu.getMaterialbanda() != null){
			Material banda = materialService.load(pneu.getMaterialbanda(), "material.cdmaterial, material.nome, material.profundidadesulco");
			pneu.getMaterialbanda().setNome(banda.getNome());
			pneu.setProfundidadesulco(banda.getProfundidadesulco());
		}
		
		if(pneu.getPneuqualificacao() != null){
			pneu.getPneuqualificacao().setNome(pneuqualificacaoService.load(pneu.getPneuqualificacao(), "pneuqualificacao.cdpneuqualificacao, pneuqualificacao.nome").getNome());
		}
		request.setAttribute("isObrigaBanda", isObrigaBanda.toString().toUpperCase());
		return new ModelAndView("direct:process/popup/popUpPneu", "pneu", pneu);
	}
	
	public ModelAndView ajaxVerificaMaterialsimilarBanda(WebRequestContext request, Pneu pneu){
		return pneuService.ajaxVerificaMaterialsimilarBanda(pneu.getMaterialbanda());
	}
	
	public ModelAndView abrirTrocaMaterialBanda(WebRequestContext request, Pneu pneu){
		Material material = materialService.findForTrocaMaterialsimilar(pneu.getMaterialbanda(), pneu.getPneumedida(), pneu.getPneumodelo());
		
		request.getServletResponse().setContentType("text/html");
		if(material != null){
			return new ModelAndView("direct:/crud/popup/popUpTrocamaterialBanda", "bean", material);
		} else {
			View.getCurrent().println("<script>alert('O material n�o possui materiais similares.');parent.$.akModalRemove(true);</script>");
			return null;
		}
	}
	
	@Override
	protected void entrada(WebRequestContext request, Pneu form) throws Exception {
		request.setAttribute("pneuSegmento", pneuSegmentoService.findPneuSegmento());
		if(form.getCdpneu() != null){
			request.setAttribute("listaVinculo", pneuService.montaVinculo(form));
		}
		if(!Util.objects.isPersistent(form)){
			form.setPneuSegmento(pneuSegmentoService.loadPrincipal());
		}
		request.setAttribute("OBRIGAR_CAMPOS_PNEU", parametrogeralService.getValorPorNome(Parametrogeral.OBRIGAR_CAMPOS_PNEU).trim().toUpperCase());
		request.setAttribute("HABILITAR_CONFIGURACAO_OTR", parametrogeralService.getValorPorNome(Parametrogeral.HABILITAR_CONFIGURACAO_OTR).trim().toUpperCase());
	}
	
	/**
	* M�todo ajax que busca os dados do pneu
	*
	* @param request
	* @param bean
	* @return
	* @since 30/12/2015
	* @author Luiz Fernando
	*/
	public ModelAndView ajaxInformacoesPneu(WebRequestContext request, Pneu bean) {
		Pneu pneu = pneuService.informacoesPneu(request, bean);
		return new JsonModelAndView().addObject("pneu", pneu);
	}
	
	public ModelAndView ajaxDadosMaterialbanda(WebRequestContext request, Pneu pneu){
		return pneuService.ajaxDadosMaterialbanda(pneu);
	}
	
	public ModelAndView ajaxCarregaMedidaModeloOtr(WebRequestContext request, PneuSegmento bean){
		List<Pneumedida> listaPneuMedida = null;
		List<Pneumodelo> listaPneuModelo = null;
		PneuSegmento segmento = null;
		Pneumarca pneuMarca = pneumarcaService.findForCd(request.getParameter("cdPneuMarca"));
		if(!request.getParameter("cdPneuSegmento").isEmpty()){
			segmento = pneuSegmentoService.findPneuSegmentoCd(request.getParameter("cdPneuSegmento"));
			listaPneuMedida = pneumedidaService.findByPneuSegmento(segmento);
			if(!request.getParameter("cdPneuMarca").isEmpty()){
				listaPneuModelo = pneumodeloService.findByPneuSegmento(segmento, pneuMarca);
			}
		}else{
			listaPneuModelo = pneumodeloService.findByPneuSegmento(segmento, pneuMarca);
		}
		JsonModelAndView json = new JsonModelAndView();
		json.addObject("listaPneuModelo", listaPneuModelo);
		json.addObject("listaPneuMedida", listaPneuMedida);
		json.addObject("isOtr", segmento != null && Boolean.TRUE.equals(segmento.getOtr()));
		return json;
		
	}
	
	public ModelAndView ajaxHabilitacaoCampos(WebRequestContext request, PneuSegmento bean){
		bean = Util.objects.isPersistent(bean)? pneuSegmentoService.loadForHabilitarCampos(bean): null;
		return new JsonModelAndView()
					.addObject("pneuSegmento", bean);
	}
	
	public ModelAndView ajaxFlagsSegmentoPneu(WebRequestContext request, Pneu pneu){
		if(Util.objects.isPersistent(pneu.getPneuSegmento())){
			return new JsonModelAndView()
						.addObject("pneuSegmento", pneuSegmentoService.loadForHabilitarCampos(pneu.getPneuSegmento()));
		}
		return new JsonModelAndView()
			.addObject("pneuSegmento", null);
	}
	
	public ModelAndView ajaxVerificaCamposDependentesDoSegmento(WebRequestContext request, Pneu pneu){
		boolean limparMedida = false;
		boolean limparModelo = false;
		if(Util.objects.isPersistent(pneu.getPneuSegmento())){
			boolean isOtr = pneuSegmentoService.isOtr(pneu.getPneuSegmento());
			if(Util.objects.isPersistent(pneu.getPneumedida())){
				limparMedida = isOtr != pneumedidaService.isOtr(pneu.getPneumedida());
			}
			if(Util.objects.isPersistent(pneu.getPneumodelo())){
				limparModelo = isOtr != pneumodeloService.isOtr(pneu.getPneumodelo());
			}
		}
		return new JsonModelAndView()
			.addObject("limparModelo", limparModelo)
			.addObject("limparMedida", limparMedida);
	}
	
	public ModelAndView replicarPneuNVezes(WebRequestContext request, Pneu bean){
		
		bean = pneuService.loadForEntrada(bean);
		List<Pneu> listaPneu = new ArrayList<Pneu>();
		if(Util.objects.isPersistent(bean) && StringUtils.isNotBlank(request.getParameter("qtdeVezes"))){
			Integer qtdeVezes = 0;
			try {
				qtdeVezes = Integer.parseInt(request.getParameter("qtdeVezes"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			for(int i=0; i<qtdeVezes; i++){
				
				bean.setCdpneu(null);
				pneuService.saveOrUpdate(bean);
				Pneu pneu = pneuService.loadPneu(bean);
				listaPneu.add(pneu);
			}
		}
		return new JsonModelAndView()
					.addObject("listaPneu", listaPneu);
	}
	
	@Override
	protected Pneu carregar(WebRequestContext request, Pneu bean) throws Exception {
		if("true".equals(request.getParameter("fromInsertOne"))){
			return pneuService.loadPneu(bean);
		}
		return super.carregar(request, bean);
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Pneu bean) {
		if("true".equals(request.getParameter("fromInsertOne"))){
			try {
				bean = carregar(request, bean);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String forCombo = StringUtils.isBlank(request.getParameter("insertFromSelect")) ? "true" : "false";
			boolean includeDescription = Boolean.valueOf(request.getParameter("includeDescriptionFromInsertOne") != null ? request.getParameter("includeDescriptionFromInsertOne") : "false"); 
			Object id = Util.strings.toStringIdStyled(bean, includeDescription);
			String description = Util.strings.toStringDescription(bean);
			NeoWeb.getRequestContext().clearMessages();
			request.getServletResponse().setContentType("text/html");
			View.getCurrent()
			.println("<html>" +
					"<script language=\"JavaScript\" src=\""+request.getServletRequest().getContextPath()+"/resource/js/util.js\"></script>" +
					"<script language=\"JavaScript\">selecionar('" + id + "', '" + addEscape(description) + "'," + forCombo + ");</script>" +
					"</html>");
			
			if("true".equalsIgnoreCase(request.getParameter("closeOnSave"))){
				View.getCurrent().println("<html><script type='text/javascript'>window.close();</script></html>");
			} 
			
			return null;
		} else {
			BeanDescriptor<Pneu> beanDescriptor = Neo.getApplicationContext().getBeanDescriptor(bean);
			String idProperty = beanDescriptor.getIdPropertyName();
			Object id = beanDescriptor.getId();
			
			if(idProperty != null && !idProperty.equals("") && id != null){
				String requestQuery = request.getRequestQuery();
				String query = "?" + ACTION_PARAMETER + "=consultar&" + idProperty + "=" + id;
				return new ModelAndView("redirect:" + requestQuery + query);
			}
			
			request.setAttribute(CONSULTAR, true);
			return continueOnAction("consultar", bean);
		}
	}
}
