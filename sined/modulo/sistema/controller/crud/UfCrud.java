package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.sined.geral.bean.Uf;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.UfFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path={"/sistema/crud/Uf"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "sigla"})
public class UfCrud extends CrudControllerSined<UfFiltro, Uf, Uf> {

}
