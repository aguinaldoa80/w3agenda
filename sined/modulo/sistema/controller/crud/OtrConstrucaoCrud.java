package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.OtrConstrucao;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/OtrConstrucao", authorizationModule=CrudAuthorizationModule.class)
public class OtrConstrucaoCrud extends CrudControllerSined<FiltroListagemSined,OtrConstrucao,OtrConstrucao>{

}
