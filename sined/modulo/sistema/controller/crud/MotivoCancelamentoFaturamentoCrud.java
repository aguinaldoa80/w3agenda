package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.MotivoCancelamentoFaturamento;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MotivoCancelamentoFaturamentoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/MotivoCancelamentoFaturamento", authorizationModule = CrudAuthorizationModule.class)
@ExportCSV(fields = "descricao")
public class MotivoCancelamentoFaturamentoCrud extends CrudControllerSined<MotivoCancelamentoFaturamentoFiltro, MotivoCancelamentoFaturamento, MotivoCancelamentoFaturamento>{

	@Override
	protected void salvar(WebRequestContext request, MotivoCancelamentoFaturamento bean)throws Exception {
		try {			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_MOTIVOCANCELAMENTOFATURAMENTO_DESCRICAO")) throw new SinedException("Motivo de cancelamento j� cadastrado no sistema.");			
		}		
	}
}
