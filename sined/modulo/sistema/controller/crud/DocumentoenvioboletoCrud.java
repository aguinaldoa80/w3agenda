package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.persistence.ListagemResult;
import br.com.linkcom.sined.geral.bean.Cliente;
import br.com.linkcom.sined.geral.bean.Documentoacao;
import br.com.linkcom.sined.geral.bean.Documentoenvioboleto;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletoacao;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletocliente;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletodiascobranca;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletohistorico;
import br.com.linkcom.sined.geral.bean.Documentoenvioboletosituacao;
import br.com.linkcom.sined.geral.service.DocumentoenvioboletoService;
import br.com.linkcom.sined.geral.service.DocumentoenvioboletoclienteService;
import br.com.linkcom.sined.geral.service.DocumentoenvioboletodiascobrancaService;
import br.com.linkcom.sined.geral.service.DocumentoenvioboletohistoricoService;
import br.com.linkcom.sined.geral.service.DocumentoenvioboletosituacaoService;
import br.com.linkcom.sined.modulo.financeiro.controller.crud.bean.enumeration.Documentoenvioboletosituacaoagendamento;
import br.com.linkcom.sined.modulo.fiscal.controller.crud.filter.DocumentoenvioboletoFiltro;
import br.com.linkcom.sined.util.ControleAcessoUtil;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Documentoenvioboleto", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"empresa", "cliente", "situacaoConta", "periodoAgendamento", "diasCobranca", "situacaoagendamento"})
public class DocumentoenvioboletoCrud extends CrudControllerSined<DocumentoenvioboletoFiltro, Documentoenvioboleto, Documentoenvioboleto>{

	private DocumentoenvioboletoService documentoenvioboletoService;
	private DocumentoenvioboletodiascobrancaService documentoenvioboletodiascobrancaService;
	private DocumentoenvioboletoclienteService documentoenvioboletoclienteService;
	private DocumentoenvioboletosituacaoService documentoenvioboletosituacaoService;
	private DocumentoenvioboletohistoricoService documentoenvioboletohistoricoService;
	
	public void setDocumentoenvioboletoclienteService(
			DocumentoenvioboletoclienteService documentoenvioboletoclienteService) {
		this.documentoenvioboletoclienteService = documentoenvioboletoclienteService;
	}
	public void setDocumentoenvioboletodiascobrancaService(
			DocumentoenvioboletodiascobrancaService documentoenvioboletodiascobrancaService) {
		this.documentoenvioboletodiascobrancaService = documentoenvioboletodiascobrancaService;
	}
	public void setDocumentoenvioboletoService(
			DocumentoenvioboletoService documentoenvioboletoService) {
		this.documentoenvioboletoService = documentoenvioboletoService;
	}
	public void setDocumentoenvioboletosituacaoService(
			DocumentoenvioboletosituacaoService documentoenvioboletosituacaoService) {
		this.documentoenvioboletosituacaoService = documentoenvioboletosituacaoService;
	}
	public void setDocumentoenvioboletohistoricoService(
			DocumentoenvioboletohistoricoService documentoenvioboletohistoricoService) {
		this.documentoenvioboletohistoricoService = documentoenvioboletohistoricoService;
	}
	
	@Override
	protected ListagemResult<Documentoenvioboleto> getLista(WebRequestContext request, DocumentoenvioboletoFiltro filtro) {
		ListagemResult<Documentoenvioboleto> lista = super.getLista(request, filtro);
		List<Documentoenvioboleto> list = lista.list();
		for (Documentoenvioboleto documentoenvioboleto : list) {
			documentoenvioboleto.setListaCliente(documentoenvioboletoclienteService.findByDocumentoenvioboleto(documentoenvioboleto));
			documentoenvioboleto.setListaSituacao(documentoenvioboletosituacaoService.findByDocumentoenvioboleto(documentoenvioboleto));
			documentoenvioboleto.setListaDiascobranca(documentoenvioboletodiascobrancaService.findByDocumentoenvioboleto(documentoenvioboleto));
		}
		return lista;
	}
	
	@Override
	protected void listagem(WebRequestContext request,
			DocumentoenvioboletoFiltro filtro) throws Exception {
		List<Documentoacao> listaAcao = new ArrayList<Documentoacao>();
		listaAcao.add(Documentoacao.PREVISTA);
		listaAcao.add(Documentoacao.DEFINITIVA);
		request.setAttribute("listaSituacao", listaAcao);
		super.listagem(request, filtro);
	}
	
	@Override
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, Documentoenvioboleto form) {
		return new ModelAndView("crud/documentoenvioboletoEntrada");
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request, DocumentoenvioboletoFiltro filtro) {
		return new ModelAndView("crud/documentoenvioboletoListagem");
	}
	
	@Override
	protected Documentoenvioboleto criar(WebRequestContext request,
			Documentoenvioboleto form) throws Exception {
		Documentoenvioboleto bean = super.criar(request, form);
		bean.setDtinicio(SinedDateUtils.currentDate());
		return bean;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Documentoenvioboleto form)
			throws Exception {
		List<Documentoacao> listaAcao = new ArrayList<Documentoacao>();
		listaAcao.add(Documentoacao.PREVISTA);
		listaAcao.add(Documentoacao.DEFINITIVA);
		request.setAttribute("listaSituacao", listaAcao);
		super.entrada(request, form);
		if(form.getCddocumentoenvioboleto() != null){
			if(SinedUtil.isListNotEmpty(form.getListaSituacao())){
				for(Documentoenvioboletosituacao situacao: form.getListaSituacao()){
					form.getListaSituacaoTrans().add(situacao.getDocumentoacao());
				}
				form.setListaHistorico(documentoenvioboletohistoricoService.findByDocumentoenvioboleto(form));
			}
		}else{
			form.getListaSituacaoTrans().addAll(listaAcao);
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Documentoenvioboleto bean)
			throws Exception {
		if(SinedUtil.isListNotEmpty(bean.getListaSituacaoTrans())){
			if(bean.getListaHistorico() == null){
				bean.setListaHistorico(new ArrayList<Documentoenvioboletohistorico>());
			}
			Documentoenvioboletohistorico historico = new Documentoenvioboletohistorico();
			historico.setDocumentoenvioboleto(bean);
			historico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
			historico.setDtaltera(SinedDateUtils.currentTimestamp());
			if(bean.getCddocumentoenvioboleto() != null){
				historico.setDocumentoenvioboletoacao(Documentoenvioboletoacao.ALTERADO);
				bean.setListaSituacao(documentoenvioboletosituacaoService.findBy(bean, "cddocumentoenvioboletosituacao", "documentoacao"));
			}else{
				historico.setDocumentoenvioboletoacao(Documentoenvioboletoacao.CRIADO);
			}
			bean.getListaHistorico().add(historico);
			if(SinedUtil.isListNotEmpty(bean.getListaSituacao())){
				List<Documentoenvioboletosituacao> listaExcluir = new ArrayList<Documentoenvioboletosituacao>();
				List<Documentoacao> listaAcoesMantidas = new ArrayList<Documentoacao>();
				for(Documentoenvioboletosituacao situacao: bean.getListaSituacao()){
					if(!bean.getListaSituacaoTrans().contains(situacao.getDocumentoacao())){
						listaExcluir.add(situacao);
					}else{
						listaAcoesMantidas.add(situacao.getDocumentoacao());
					}
				}
				bean.getListaSituacao().removeAll(listaExcluir);
				bean.getListaSituacaoTrans().removeAll(listaAcoesMantidas);
			}else{
				bean.setListaSituacao(new ArrayList<Documentoenvioboletosituacao>());
			}
			for(Documentoacao documentoacao: bean.getListaSituacaoTrans()){
				Documentoenvioboletosituacao documentoenvioboletosituacao = new Documentoenvioboletosituacao();
				documentoenvioboletosituacao.setDocumentoacao(documentoacao);
				documentoenvioboletosituacao.setDocumentoenvioboleto(bean);
				bean.getListaSituacao().add(documentoenvioboletosituacao);
			}
		}
		bean.setSituacaoagendamento(Documentoenvioboletosituacaoagendamento.ATIVO);
		super.salvar(request, bean);
	}
	
	@Override
	public ModelAndView doExcluir(WebRequestContext request,
			Documentoenvioboleto form) throws CrudException {
		throw new SinedException("Exclus�o n�o permitida.");
	}
	
	public void cancelarAgendamento(WebRequestContext request){
		try {
			String whereIn = request.getParameter("selectedItens");
			
			if (!documentoenvioboletoService.existeAgedamentoCancelado(whereIn)) {
				documentoenvioboletoService.cancelarAgendamento(whereIn);
				request.addMessage("Agendamento(s) " + whereIn + " cancelado(s) com sucesso.");
			} else {
				request.addError("Selecione apenas agedamentos n�o cancelados.");
			}
		} catch (Exception e) {
			request.addMessage("Erro ao tentar cancelar agedamentos.");
			e.printStackTrace();
		}
		
		SinedUtil.redirecionamento(request, "/sistema/crud/Documentoenvioboleto?ACAO=listagem");
	}
	
	@Override
	protected void validateBean(Documentoenvioboleto bean, BindException errors) {
		if(SinedUtil.isListNotEmpty(bean.getListaCliente())){
			HashMap<Cliente, Documentoenvioboletocliente> listaClientes = new HashMap<Cliente, Documentoenvioboletocliente>();
			for(Documentoenvioboletocliente docCliente: bean.getListaCliente()){
				if(!listaClientes.containsKey(docCliente.getCliente())){
					listaClientes.put(docCliente.getCliente(), docCliente);
				}else{
					errors.reject("001", "Existe(m) cliente(s) repetido(s). Favor excluir os duplicados.");
					break;
				}
			}
			
		}
		
		if(SinedUtil.isListNotEmpty(bean.getListaDiascobranca())){
			HashMap<Integer, Documentoenvioboletodiascobranca> listaDias = new HashMap<Integer, Documentoenvioboletodiascobranca>();
			for(Documentoenvioboletodiascobranca docDias: bean.getListaDiascobranca()){
				if(!listaDias.containsKey(docDias.getQtdedias())){
					listaDias.put(docDias.getQtdedias(), docDias);
				}else{
					errors.reject("002", "Dias para cobran�a repetidos. Favor corrigir.");
					break;
				}
			}
			
		}
		
		if((bean.getDtinicio() != null && SinedDateUtils.beforeIgnoreHour(bean.getDtinicio(), SinedDateUtils.currentDate())) || 
				(bean.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(bean.getDtfim(), SinedDateUtils.currentDate()))){
			errors.reject("003", "O per�odo de agendamento � inv�lido. Favor corrigir.");
		}
		
		super.validateBean(bean, errors);
	}
	
	public ModelAndView executeJob(WebRequestContext request){
		ControleAcessoUtil.util.envioEmailBoletoAutomaticoAgendado(SinedUtil.getUrlWithContext(), Boolean.FALSE);
		return sendRedirectToAction("listagem");
	}
	
}