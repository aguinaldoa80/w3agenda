package br.com.linkcom.sined.modulo.sistema.controller.crud;



import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.OtrClassificacaoCodigo;
import br.com.linkcom.sined.geral.service.OtrClassificacaoCodigoService;
import br.com.linkcom.sined.geral.service.OtrDesenhoService;
import br.com.linkcom.sined.geral.service.OtrServicoTipoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.OtrClassificacaoCodigoFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@ExportCSV(fields={"cdOtrClassificacaoCodigo", "codigo", "otrDesenho", "otrServicoTipo", "codigoIntegracao"})
@Controller(path="/sistema/crud/OtrClassificacaoCodigo", authorizationModule=CrudAuthorizationModule.class)
public class OtrClassificacaoCodigoCrud extends CrudControllerSined<OtrClassificacaoCodigoFiltro, OtrClassificacaoCodigo, OtrClassificacaoCodigo>{
	protected OtrClassificacaoCodigoService  otrClassificacaoCodigoService;
	protected OtrDesenhoService otrDesenhoService;
	protected OtrServicoTipoService otrServicoTipoService;
	
	
	public void setOtrClassificacaoCodigoService(
			OtrClassificacaoCodigoService otrClassificacaoCodigoService) {
		this.otrClassificacaoCodigoService = otrClassificacaoCodigoService;
	}
	public void setOtrDesenhoService(OtrDesenhoService otrDesenhoService) {
		this.otrDesenhoService = otrDesenhoService;
	}
	
	public void setOtrServicoTipoService(OtrServicoTipoService otrServicoTipoService) {
		this.otrServicoTipoService = otrServicoTipoService;
	}
	
	
	@Override
	protected void validateBean(OtrClassificacaoCodigo bean, BindException errors) {
		if(otrClassificacaoCodigoService.validaCodigo(bean.getCodigo(), bean)){
			errors.reject("002","C�digo de Classifica��o OTR  j� cadastrado no sistema.");
		}
		
	}
	
	@Override
	protected void entrada(WebRequestContext request, OtrClassificacaoCodigo bean)throws Exception {
		request.setAttribute("desenhos", otrDesenhoService.findDesenho());
		request.setAttribute("tiposServicos", otrServicoTipoService.findTipoServico());
	}
	
	@Override
	protected void listagem(WebRequestContext request,
			OtrClassificacaoCodigoFiltro filtro) throws Exception {
		request.setAttribute("desenhos", otrDesenhoService.findDesenho());
		request.setAttribute("tiposServicos", otrServicoTipoService.findTipoServico());
		super.listagem(request, filtro);
	}
}
