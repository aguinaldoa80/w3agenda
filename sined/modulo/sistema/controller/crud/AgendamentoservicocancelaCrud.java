package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Agendamentoservicocancela;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;

@CrudBean
@Controller(path="/sistema/crud/Agendamentoservicocancela", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class AgendamentoservicocancelaCrud extends CrudControllerSined<FiltroListagemSined, Agendamentoservicocancela, Agendamentoservicocancela> {

	@Override
	protected void salvar(WebRequestContext request, Agendamentoservicocancela bean) throws Exception {
		try {
			bean.setNome(bean.getNome().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_agendamentoservicocancela_nome")) {
				throw new SinedException("Motivo de cancelamento j� cadastrado no sistema.");
			}
		}
	}
	
}
