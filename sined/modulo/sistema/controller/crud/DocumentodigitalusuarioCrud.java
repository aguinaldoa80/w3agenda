package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documentodigitalusuario;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentodigitalusuarioFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Documentodigitalusuario", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"email", "nome"})
public class DocumentodigitalusuarioCrud extends CrudControllerSined<DocumentodigitalusuarioFiltro, Documentodigitalusuario, Documentodigitalusuario>{

	@Override
	protected void salvar(WebRequestContext request, Documentodigitalusuario bean)	throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_documentodigitalusuario_email")) {
				throw new SinedException("E-mail de usu�rio j� cadastrado no sistema.");
			}
		}
	}
	
}
