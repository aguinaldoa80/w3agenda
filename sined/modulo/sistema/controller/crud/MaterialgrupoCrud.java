package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.lang.reflect.Method;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.ReflectionCache;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Categoria;
import br.com.linkcom.sined.geral.bean.Materialgrupo;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.bean.enumeration.MaterialGrupoEnum;
import br.com.linkcom.sined.geral.service.CategoriaService;
import br.com.linkcom.sined.geral.service.MaterialGrupoHistoricoService;
import br.com.linkcom.sined.geral.service.MaterialformulacustoService;
import br.com.linkcom.sined.geral.service.MaterialgrupoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MaterialgrupoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Materialgrupo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class MaterialgrupoCrud extends CrudControllerSined<MaterialgrupoFiltro, Materialgrupo, Materialgrupo>{
	
	private MaterialgrupoService materialgrupoService;
	private CategoriaService categoriaService;
	private MaterialformulacustoService materialformulacustoService;
	private ParametrogeralService parametrogeralService;
	private MaterialGrupoHistoricoService materialGrupoHistoricoService;
	
	
	
	
	public void setMaterialGrupoHistoricoService(
			MaterialGrupoHistoricoService materialGrupoHistoricoService) {
		this.materialGrupoHistoricoService = materialGrupoHistoricoService;
	}
	public void setMaterialformulacustoService(
			MaterialformulacustoService materialformulacustoService) {
		this.materialformulacustoService = materialformulacustoService;
	}
	public void setMaterialgrupoService(MaterialgrupoService materialgrupoService) {
		this.materialgrupoService = materialgrupoService;
	}

	public void setCategoriaService(CategoriaService categoriaService) {
		this.categoriaService = categoriaService;
	}
	
	public void setParametrogeralService(
			ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	protected void salvar(WebRequestContext request, Materialgrupo bean) throws Exception {
		Boolean criar = bean.getCdmaterialgrupo() == null;
		Materialgrupo beanAntigo = null;
		String obs = null;
		
		if(bean.getCdmaterialgrupo() != null) {
			beanAntigo = materialgrupoService.loadForEntrada(bean);
			}
			if(beanAntigo != null) {
			obs = materialGrupoHistoricoService.getCamposAlterados(beanAntigo, bean);
			obs = compararPedidoVendaTipoSelectsPorObjeto(obs, beanAntigo, bean);
			}

		try {
			Double rateiocustoproducaoAntigo = null;
			if(bean.getCdmaterialgrupo() != null){
				Materialgrupo materialgrupoAntigo = materialgrupoService.load(bean, "materialgrupo.cdmaterialgrupo, materialgrupo.rateiocustoproducao");
				rateiocustoproducaoAntigo = materialgrupoAntigo.getRateiocustoproducao();
			}
			
			bean.setNome(bean.getNome().trim());
			super.salvar(request, bean);
			
	//		super.salvar(request, bean);
			if(criar){
				materialGrupoHistoricoService.preencherHistorico(bean, MaterialGrupoEnum.CRIADO, "Cria��o do Grupo de Material", true,null,null);
			}else {
				if(beanAntigo != null && beanAntigo.getGerarBlocoH() != null){
					if(!beanAntigo.getGerarBlocoH().equals(bean.getGerarBlocoH())){
					materialGrupoHistoricoService.preencherHistorico(bean, MaterialGrupoEnum.ALTERADO, obs, true,bean.getGerarBlocoH(),null );
					}
				}
				if(beanAntigo != null && beanAntigo.getGerarBlocoK() != null){
					if(!beanAntigo.getGerarBlocoK().equals(bean.getGerarBlocoK())){
						materialGrupoHistoricoService.preencherHistorico(bean, MaterialGrupoEnum.ALTERADO, obs, true,null,bean.getGerarBlocoK());
						}
				}	
			}

			boolean rateiocustoproducaoIgual = (rateiocustoproducaoAntigo == null && bean.getRateiocustoproducao() == null) || (rateiocustoproducaoAntigo != null && bean.getRateiocustoproducao() != null && rateiocustoproducaoAntigo.equals(bean.getRateiocustoproducao()));
			if(!rateiocustoproducaoIgual){
				materialformulacustoService.calculaCustoMaterialWithRateiooperacional(request);
			}
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			if (DatabaseError.isKeyPresent(e, "IDX_MATERIALGRUPO_NOME")) {
				throw new SinedException("Grupo de material j� cadastrado no sistema.");
			}
			if (DatabaseError.isKeyPresent(e, "IDX_MATERIALGRUPO_SIMBOLO")) {
				throw new SinedException("S�mbolo j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void validateBean(Materialgrupo bean, BindException errors) {
		Boolean existDuplicidadeCargoComissionamentoDocumentotipo =  materialgrupoService.existDuplicidadeCargoComissionamentoDocumentotipoFornecedor(bean);
		if(existDuplicidadeCargoComissionamentoDocumentotipo){
			errors.reject("001", "Existe duplicidade na Comiss�o de Venda. (Cargo, Comissionamento, Forma de Pagamento, Tipo de Venda e Fornecedor iguais)");
		}
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Materialgrupo form) throws Exception {
		List<Categoria> listaCategorias = categoriaService.findForMaterialgrupocomissao();
		request.setAttribute("listaCategorias", listaCategorias);
		
		super.entrada(request, form);
		request.setAttribute("ignoreHackAndroidDownload", Boolean.TRUE);
		
		request.setAttribute("utilizaControleGado", parametrogeralService.getBoolean("CONTROLE_GADO"));
		request.setAttribute("PRODUTO_CONTROLADO", parametrogeralService.getBoolean(Parametrogeral.PRODUTO_CONTROLADO));
		if(form != null && form.getCdmaterialgrupo()!=null){
			form.setListaMaterialGrupoHistorico(materialGrupoHistoricoService.findByMaterialGrupoHistorico(form));
		}
	}
	
	public ModelAndView ajaxVerificaBloco(WebRequestContext request ) {
		String ids = request.getParameter("ids").replace(" ", "");
		Boolean blocoH = false;
		Boolean blocoK = false;
		for (String id: ids.split(",")){
			Materialgrupo materialGrupo = materialgrupoService.findGerarBloco(id);
			if(materialGrupo!=null){
				if(materialGrupo.getGerarBlocoH()!=null){
					if(materialGrupo.getGerarBlocoH().equals(Boolean.TRUE)){
						blocoH = true;
					}
				}
				if(materialGrupo.getGerarBlocoK()!=null){
					if(materialGrupo.getGerarBlocoK().equals(Boolean.TRUE)){
						blocoK = true;
					}
				}
			}
		}
		return new JsonModelAndView().addObject("blocoH", blocoH).addObject("blocoK", blocoK);
	}
	
	public ModelAndView gerarNaoGerarBlocos (WebRequestContext request) throws Exception{
		String bloco = request.getParameter("Blocoacao");
		String whereIn = SinedUtil.getItensSelecionados(request);
		if(whereIn == null || whereIn.trim().equals("")){
			throw new SinedException("Nenhum item selecionado.");
		}
		
		for (String id: whereIn.split(",")){
			Materialgrupo materialGrupo = materialgrupoService.loadForEntrada(materialgrupoService.findGerarBloco(id));
			if(bloco.equals("GERARBLOCOK")){
				materialGrupo.setGerarBlocoK(Boolean.TRUE);
			}
			if(bloco.equals("NAOGERARBLOCOK")){
				materialGrupo.setGerarBlocoK(Boolean.FALSE);
			}
			if(bloco.equals("GERARBLOCOH")){
				materialGrupo.setGerarBlocoH(Boolean.TRUE);
			}
			if(bloco.equals("NAOGERARBLOCOH")){
				materialGrupo.setGerarBlocoH(Boolean.FALSE);
			}
			salvar(request, materialGrupo);
		}
		return sendRedirectToAction("listagem");
	}
	
	private String compararPedidoVendaTipoSelectsPorObjeto(String obs, Materialgrupo beanAntigo, Materialgrupo bean) throws SecurityException, NoSuchMethodException {
		ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
		String descricaoAntigoH = "";
		String descricaoAntigoK = "";
		String descricaoCorrenteH = "";
		String descricaoCorrenteK = "";
		String campo = "";
		String de = " de: ";
		String para = " para: ";
		StringBuilder descricaoCompleta = new StringBuilder();
		
        if(bean.getGerarBlocoH() == null) bean.setGerarBlocoH(Boolean.FALSE);
        if(bean.getGerarBlocoK() == null) bean.setGerarBlocoK(Boolean.FALSE);
        
		//compara blocos
		if((bean != null && bean.getGerarBlocoH()!= null)||(bean != null && bean.getGerarBlocoK()!= null)){
			if(bean.getGerarBlocoH().equals(Boolean.TRUE)){
				descricaoCorrenteH = "Gerar";
			}else{
				descricaoCorrenteH = "N�o gerar";
			}
			if(bean.getGerarBlocoK().equals(Boolean.TRUE)){
				descricaoCorrenteK = "Gerar";
			}else{
				descricaoCorrenteK = "N�o gerar";
			}
		} 
		if((beanAntigo != null && beanAntigo.getGerarBlocoH() != null)||(beanAntigo != null && beanAntigo.getGerarBlocoK() != null)){
			if(beanAntigo.getGerarBlocoH().equals(Boolean.TRUE)){
				descricaoAntigoH = "Gerar";
			}else{
				descricaoAntigoH = "N�o gerar";
			}
			if(beanAntigo.getGerarBlocoK().equals(Boolean.TRUE)){
				descricaoAntigoK = "Gerar";
			}else{
				descricaoAntigoK = "N�o gerar";
			}
		
		if(beanAntigo.getGerarBlocoH() == null){
				beanAntigo.setGerarBlocoH(false);
		}
		if(!beanAntigo.getGerarBlocoH().equals(bean.getGerarBlocoH())){
			Method method = reflectionCache.getMethod(bean.getClass(), "getGerarBlocoH");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigoH.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrenteH).append("<br>");
			} else if(descricaoCorrenteH.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigoH).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigoH).append(para).append(descricaoCorrenteH).append("<br>");
			}
		}
		}

		if(beanAntigo.getGerarBlocoK() == null){
			beanAntigo.setGerarBlocoK(false);
		}
		if(!beanAntigo.getGerarBlocoK().equals(bean.getGerarBlocoK())){
			Method method = reflectionCache.getMethod(bean.getClass(), "getGerarBlocoK");
			campo = method.getAnnotation(DisplayName.class).value();
			if(descricaoAntigoK.isEmpty()){
				descricaoCompleta.append(campo).append(para).append(descricaoCorrenteK).append("<br>");
			} else if(descricaoCorrenteK.isEmpty()) {
				descricaoCompleta.append(campo).append(para).append(descricaoAntigoK).append("<br>");
			} else {
				descricaoCompleta.append(campo).append(de).append(descricaoAntigoK).append(para).append(descricaoCorrenteK).append("<br>");
			}
		}
	
		descricaoAntigoH = "";
		descricaoCorrenteH = "";
		descricaoAntigoK = "";
		descricaoCorrenteK = "";
		
		return descricaoCompleta.toString();
	}
}
