package br.com.linkcom.sined.modulo.sistema.controller.crud;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Indicecorrecao;
import br.com.linkcom.sined.geral.bean.Indicecorrecaovalor;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.IndicecorrecaoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path="/sistema/crud/Indicecorrecao", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"sigla", "descricao"})
public class IndicecorrecaoCrud extends CrudControllerSined<IndicecorrecaoFiltro, Indicecorrecao, Indicecorrecao> {

	@Override
	protected void salvar(WebRequestContext request, Indicecorrecao bean)throws Exception {
		try {
			bean.setSigla(bean.getSigla().trim());
			if(bean.getDescricao() != null)
				bean.setDescricao(bean.getDescricao().trim());
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "IDX_INDICECORRECAO_SIGLA")) {
				throw new SinedException("�ndice de corre��o j� cadastrado no sistema.");
			}
		}
	}

	@Override
	protected void validateBean(Indicecorrecao bean, BindException errors) {
		if(bean.getListaIndicecorrecaovalor() != null && !bean.getListaIndicecorrecaovalor().isEmpty()){
			for (Indicecorrecaovalor indicecorrecaovalor : bean.getListaIndicecorrecaovalor()) {
				int count = 0;
				for (Indicecorrecaovalor indicecorrecaovalorAux : bean.getListaIndicecorrecaovalor()) {
					if(indicecorrecaovalor.getMesanoAux().equals(indicecorrecaovalorAux.getMesanoAux()))
						count++;
					if(count > 1){
						errors.reject("001", "Existe mais de um percentual cadastrado para o M�s/ano "+indicecorrecaovalor.getMesanoAux());
						break;
					}
				}
				if(count > 1)
					break;
			}
		}else{
			errors.reject("002", "�ndice de corre��o deve ter pelo menos 1 valor associado a ele.");
		}
		
		super.validateBean(bean, errors);
	}
	
}
