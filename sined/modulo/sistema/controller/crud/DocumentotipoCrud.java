package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Documentotipo;
import br.com.linkcom.sined.geral.bean.PrazoPagamentoECF;
import br.com.linkcom.sined.geral.bean.Prazopagamento;
import br.com.linkcom.sined.geral.bean.Tipotaxa;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.DocumentotipoService;
import br.com.linkcom.sined.geral.service.FormapagamentoService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.PrazopagamentoService;
import br.com.linkcom.sined.geral.service.TipotaxaService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.DocumentotipoFiltro;
import br.com.linkcom.sined.util.DatabaseError;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Documentotipo", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = "nome")
public class DocumentotipoCrud extends CrudControllerSined<DocumentotipoFiltro, Documentotipo, Documentotipo>{

	private DocumentotipoService documentotipoService;
	private CentrocustoService centrocustoService;
	private FormapagamentoService formapagamentoService;
	private TipotaxaService tipotaxaService;
	private PrazopagamentoService prazoPagamentoService;
	private ParametrogeralService parametrogeralService;
	
	public void setPrazoPagamentoService(PrazopagamentoService prazoPagamentoService) {this.prazoPagamentoService = prazoPagamentoService;}
	public void setCentrocustoService(CentrocustoService centrocustoService) {this.centrocustoService = centrocustoService;}
	public void setDocumentotipoService(DocumentotipoService documentotipoService) {this.documentotipoService = documentotipoService;}
	public void setFormapagamentoService(FormapagamentoService formapagamentoService) {this.formapagamentoService = formapagamentoService;}
	public void setTipotaxaService(TipotaxaService tipotaxaService) {this.tipotaxaService = tipotaxaService;}
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {this.parametrogeralService = parametrogeralService;}
	
	@Override
	protected void entrada(WebRequestContext request, Documentotipo form) throws Exception {
		request.setAttribute("listaCentrocusto", centrocustoService.findAtivos(form.getCentrocusto()));
		request.setAttribute("listaPrazoPagamentoAtivo", prazoPagamentoService.findAtivosForFlex());
		request.setAttribute("listaFormapagamentoCredito", formapagamentoService.findCredito());
		request.setAttribute("listaFormapagamentoDebito", formapagamentoService.findDebito());
		request.setAttribute("listaTipotaxaContareceber", tipotaxaService.findTipotaxaContareceberForDocumentotipo());
		request.setAttribute("LCDPR", parametrogeralService.getValorPorNome("LCDPR"));
	}
	
	@Override
	protected void salvar(WebRequestContext request, Documentotipo bean)throws Exception {
		try{
			super.salvar(request, bean);
		}catch(DataIntegrityViolationException e){
			if(DatabaseError.isKeyPresent(e, "IDX_DOCUMENTOTIPO_NOME")){
				throw new SinedException("Tipo de documento j� cadastrado no sistema.");
			}
		}
	}
	
	@Override
	protected void excluir(WebRequestContext request, Documentotipo bean)throws Exception {
		try {
			super.excluir(request, bean);
		} catch (Exception e) {
			throw new SinedException("Tipo de documento n�o pode ser exclu�do, j� possui refer�ncias em outros registros do sistema.");
		}
	}
	
	@Override
	protected void validateBean(Documentotipo bean, BindException errors) {
		
		if(bean.getNotafiscal() && documentotipoService.haveAnotherNota(bean)){
			errors.reject("001", "J� existe outro tipo de documento sendo nota fiscal.");
		}
		
		if(!Boolean.TRUE.equals(bean.getExibirnavenda()) && !Boolean.TRUE.equals(bean.getExibirnacompra())){
			errors.reject("002", "Pelo menos um dos dois campos 'Exibir na venda/NF' ou 'Exibir na compra' deve estar marcado.");
		}
		if(Boolean.TRUE.equals(bean.getGerarmovimentacaoseparadataxa()) && Boolean.TRUE.equals(bean.getTaxanoprocessamentoextratocartaocredito())){
			errors.reject("003", "Tipo de documento que gera movimenta��o de taxa separada n�o pode possuir a flag \"CONSIDERAR TAXA APENAS NO PROCESSAMENTO DO EXTRATO DO CART�O DE CR�DITO\" marcada.");
		}
		
		if(bean.getFinalizadoraemporium() != null && !"".equals(bean.getFinalizadoraemporium()) && SinedUtil.isListNotEmpty(bean.getListaPrazoPagamentoECF())){
			errors.reject("006","N�o � permitido cadastrar o c�digo da finalizadora (ECF) da aba Principal quando existir registro na aba Prazos de Pagamentos por Finalizadora (ECF).");
		} 
		if(SinedUtil.isListNotEmpty(bean.getListaPrazoPagamentoECF())){
			List<PrazoPagamentoECF> listaPrazos = bean.getListaPrazoPagamentoECF();
			List<Integer> listCod = new ArrayList<Integer>();
			for (PrazoPagamentoECF prazo : listaPrazos) {
				if(SinedUtil.isListNotEmpty(listCod)){
					for (Integer cod : listCod) {
						if(cod.equals(prazo.getCodECF())){
							errors.reject("004","O C�digo Finalizador (ECF) "+ cod+ " est� vinculado a mais de um prazo de Pagamento.");
						}
					}
					List<Documentotipo> listaDocComCodRepetido = documentotipoService.findByCodECF(prazo.getCodECF(),bean);
					if(SinedUtil.isListNotEmpty(listaDocComCodRepetido)){
						String msg = "N�o � permitido cadastrar o mesmo C�digo da Finalizadora (ECF) em mais de um Tipo de Documento.\n";
						msg = msg.concat("O v�nculo Finalizadora (ECF): "+prazo.getCodECF()+" j� existe no Tipo de Documento: "+listaDocComCodRepetido.get(0).getNome()+".");
						errors.reject("005",msg);
					}
				}else{
					List<Documentotipo> listaDocComCodRepetido = documentotipoService.findByCodECF(prazo.getCodECF(),bean);
					if(SinedUtil.isListNotEmpty(listaDocComCodRepetido)){
						String msg = "N�o � permitido cadastrar o mesmo C�digo da Finalizadora (ECF) em mais de um Tipo de Documento.\n";
						msg = msg.concat("O v�nculo Finalizadora (ECF): "+prazo.getCodECF()+" j� existe no Tipo de Documento: "+listaDocComCodRepetido.get(0).getNome()+".");
						errors.reject("005",msg);
					}	
					listCod.add(prazo.getCodECF());
					continue;
				}
				listCod.add(prazo.getCodECF());
			}
		}else{
			List<Documentotipo> listaDocComCodRepetido = documentotipoService.findByCodECF(bean.getFinalizadoraemporium(),bean);
			if(SinedUtil.isListNotEmpty(listaDocComCodRepetido)){
				String msg = "N�o � permitido cadastrar o mesmo C�digo da Finalizadora (ECF) em mais de um Tipo de Documento.\n";
				msg = msg.concat("O v�nculo Finalizadora (ECF): "+bean.getFinalizadoraemporium()+" j� existe no Tipo de Documento: "+listaDocComCodRepetido.get(0).getNome()+".");
				errors.reject("005",msg);
			}	
		}
	}
	
}
