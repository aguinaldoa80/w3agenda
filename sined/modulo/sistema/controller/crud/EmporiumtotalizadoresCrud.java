package br.com.linkcom.sined.modulo.sistema.controller.crud;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.sined.geral.bean.Emporiumtotalizadores;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.EmporiumtotalizadoresFiltro;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Emporiumtotalizadores", authorizationModule=CrudAuthorizationModule.class)
public class EmporiumtotalizadoresCrud extends CrudControllerSined<EmporiumtotalizadoresFiltro, Emporiumtotalizadores, Emporiumtotalizadores> {

}
