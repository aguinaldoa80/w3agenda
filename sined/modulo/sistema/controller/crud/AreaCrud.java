package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.bean.annotation.CrudBean;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Area;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.service.AreaService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.AreaFiltro;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@CrudBean
@Controller(path={"/sistema/crud/Area"}, authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"descricao", "projeto", "ativo"})
public class AreaCrud extends CrudControllerSined<AreaFiltro, Area, Area> {	
	private AreaService areaService;
	private ProjetoService projetoService;
	
	public void setAreaService(AreaService areaService) {this.areaService = areaService;}
	public void setProjetoService(ProjetoService projetoService) { this.projetoService = projetoService; }
	
	/**
	 * @author Taidson
	 * @since 04/03/2010
	 * 
	 * M�todo respons�vel por verificar se a mesma �rea com o mesmo projeto que est�o sendo gravados,
	 * j� existem no banco.
	 */
	@Override
	protected void validateBean(Area bean, BindException errors) {

		List<Area> areas = areaService.verificaAreaProjeto(bean);
		
		for (Area area : areas) {
			if((area.getProjeto() != null) && (bean.getProjeto() != null)){
				if((bean.getDescricao().equalsIgnoreCase(area.getDescricao()))&& (bean.getProjeto().getCdprojeto().equals(area.getProjeto().getCdprojeto()))){
					errors.reject("001", "Informe uma �rea ou um projeto diferente.");
					break;
				}
			}else if((bean.getDescricao().equalsIgnoreCase(area.getDescricao())) && (bean.getProjeto() == null) && (area.getProjeto() == null)){
				errors.reject("001", "Informe uma �rea ou um projeto diferente.");
				break;
			}
		}
	}
	
	@Override
	protected ModelAndView getSalvarModelAndView(WebRequestContext request, Area bean) {
		if(bean.getIrListagemDireto() != null && bean.getIrListagemDireto()){
			return sendRedirectToAction("listagem");
		}
		return super.getSalvarModelAndView(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, Area form) throws Exception {
		String copiar = request.getParameter("copiar");
		String cdarea = request.getParameter("cdarea");
		if (copiar != null && cdarea != null) {
			form.setCdusuarioaltera(null);
			form.setDtaltera(null);
			form.setCdarea(null);
		}
		
		Usuario usuario = SinedUtil.getUsuarioLogado();
		if(!usuario.getTodosprojetos()){				
			String whereIn = SinedUtil.getListaProjeto();
			if(whereIn == null || whereIn.equals("")){
				request.setAttribute("permissaoProjeto", Boolean.FALSE);
			} else {
				request.setAttribute("permissaoProjeto", Boolean.TRUE);
			}
		} else {
			request.setAttribute("permissaoProjeto", Boolean.FALSE);
		}
		request.setAttribute("listaProjeto", projetoService.findNaoCancelados());
	}
	
	@Override
	protected void listagem(WebRequestContext request, AreaFiltro filtro) throws Exception {
		request.setAttribute("listaProjeto", projetoService.findNaoCancelados());
	}
	
}
