package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Garantiareformaresultado;
import br.com.linkcom.sined.geral.bean.Historicooperacao;
import br.com.linkcom.sined.geral.service.GarantiareformaresultadoService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.GarantiareformaresultadoFiltro;
import br.com.linkcom.sined.util.bean.GenericBean;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Garantiareformaresultado", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "ativo"})
public class GarantiareformaresultadoCrud extends CrudControllerSined<GarantiareformaresultadoFiltro, Garantiareformaresultado, Garantiareformaresultado> {
	
	private GarantiareformaresultadoService garantiareformaresultadoService;
	
	public void setGarantiareformaresultadoService(
			GarantiareformaresultadoService garantiareformaresultadoService) {
		this.garantiareformaresultadoService = garantiareformaresultadoService;
	}

	@Override
	protected Garantiareformaresultado criar(WebRequestContext request,
			Garantiareformaresultado form) throws Exception {
		Garantiareformaresultado bean = super.criar(request, form);
		bean.setAtivo(true);
		return bean;
	}
	
	public ModelAndView popUpVariaveisResultado(WebRequestContext request, Historicooperacao bean) {
		String nomeCampo = request.getParameter("nomeCampo");
		List<GenericBean> listaVariavel = garantiareformaresultadoService.findVariaveis();
		
		Collections.sort(listaVariavel, new Comparator<GenericBean>() {
			@Override
			public int compare(GenericBean o1, GenericBean o2) {
				return o1.getValue().toString().compareToIgnoreCase(o2.getValue().toString());
			}
		});
		
		return new ModelAndView("direct:/crud/popup/popUpVariavelResultadoGarantia", "variaveisResultado", listaVariavel).addObject("nomeCampo", nomeCampo);
	}
}
