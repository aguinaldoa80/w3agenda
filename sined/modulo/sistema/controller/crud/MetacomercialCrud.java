package br.com.linkcom.sined.modulo.sistema.controller.crud;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.ExportCSV;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.CollectionsUtil;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Cargo;
import br.com.linkcom.sined.geral.bean.Metacomercial;
import br.com.linkcom.sined.geral.bean.Metacomercialitem;
import br.com.linkcom.sined.geral.service.CargoService;
import br.com.linkcom.sined.geral.service.MetacomercialService;
import br.com.linkcom.sined.geral.service.MetacomercialitemService;
import br.com.linkcom.sined.modulo.sistema.controller.crud.filter.MetacomercialFiltro;
import br.com.linkcom.sined.util.SinedDateUtils;
import br.com.linkcom.sined.util.controller.CrudControllerSined;

@Controller(path="/sistema/crud/Metacomercial", authorizationModule=CrudAuthorizationModule.class)
@ExportCSV(fields = {"nome", "dtinicioAux", "dtfimAux", "meta"})
public class MetacomercialCrud extends CrudControllerSined<MetacomercialFiltro, Metacomercial, Metacomercial> {

	private MetacomercialService metacomercialService;
	private MetacomercialitemService metacomercialitemService;
	
	public void setMetacomercialService(MetacomercialService metacomercialService) {
		this.metacomercialService = metacomercialService;
	}
	public void setMetacomercialitemService(MetacomercialitemService metacomercialitemService) {
		this.metacomercialitemService = metacomercialitemService;
	}
	
	@Override
	protected Metacomercial criar(WebRequestContext request, Metacomercial form) throws Exception {
		Metacomercial bean = super.criar(request, form);
		
		if("true".equals(request.getParameter("copiar")) && form.getCdmetacomercial() != null){
			bean = metacomercialService.loadForEntrada(form);
			
			bean.setCdmetacomercial(null);
			List<Metacomercialitem> listaMetacomercialitem = bean.getListaMetacomercialitem();
			for (Metacomercialitem metacomercialitem : listaMetacomercialitem) {
				metacomercialitem.setCdmetacomercialitem(null);
			}
			
			if(bean.getDtinicio() != null){
				bean.setDtinicio(SinedDateUtils.addMesData(bean.getDtinicio(), 1));
			}
			if(bean.getDtfim() != null){
				bean.setDtfim(SinedDateUtils.addMesData(bean.getDtfim(), 1));
			}
		}
		
		
		return bean;
	}
	
	@Override
	protected void entrada(WebRequestContext request, Metacomercial form)throws Exception {
		if(form.getCdmetacomercial() == null && form.getDtinicio() == null) 
			form.setDtinicio(new Date(System.currentTimeMillis()));
		super.entrada(request, form);
	}

	@Override
	protected void validateBean(Metacomercial bean, BindException errors) {
//		if(bean.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(bean.getDtfim(), new Date(System.currentTimeMillis()))){
//				errors.reject("001", "Data limite n�o pode ser anterior a data atual.");
//		}
		if(bean.getDtfim() != null && SinedDateUtils.beforeIgnoreHour(bean.getDtfim(), bean.getDtinicio())){
			errors.reject("001", "Per�odo de validade inv�lido.");
		}
		
		if(bean.getNome().length() > 30){
			errors.reject("001", "O nome do registro de Meta Comercial n�o pode conter mais que 30 caracteres.");
		}
		
		if(bean.getListaMetacomercialitem() != null && !bean.getListaMetacomercialitem().isEmpty()){
			for(Metacomercialitem metacomercialitem : bean.getListaMetacomercialitem()){
				if(metacomercialitem.getColaborador() != null){
					if(metacomercialitemService.existsColaboradorPeriodo(bean, metacomercialitem.getColaborador(), bean.getDtinicio(), bean.getDtfim())){
						errors.reject("001", "Colaborador " + metacomercialitem.getColaborador().getNome() + " j� cadastrado em uma meta no per�odo.");
					}
				}
				if(metacomercialitem.getCargo() != null){
					if(metacomercialitemService.existsCargoPeriodo(bean, metacomercialitem.getCargo(), bean.getDtinicio(), bean.getDtfim())){
						Cargo cargo = CargoService.getInstance().load(metacomercialitem.getCargo());
						errors.reject("001", "Cargo " + cargo.getNome() + " j� cadastrado em uma meta no per�odo.");
					}
				}
				if(metacomercialitem.getColaborador() == null && metacomercialitem.getCargo() == null && metacomercialService.existsMetaPeriodo(bean)){
					errors.reject("001", "Meta j� cadastrada no per�odo.");
				}
			}
		}else if(metacomercialService.existsMetaPeriodo(bean)){
			errors.reject("001", "Meta j� cadastrada no per�odo.");
		}
		
		super.validateBean(bean, errors);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Metacomercial bean)throws Exception {
		String whereInListametacomercial = request.getParameter("listametacomercial");
		if(!"<NULL>".equalsIgnoreCase(whereInListametacomercial)){
			List<Metacomercial> metacomercialatualizacao = new ArrayList<Metacomercial>();
			metacomercialatualizacao = metacomercialService.findForMetaAtualizacao(whereInListametacomercial);
			
			for (Metacomercial mc : metacomercialatualizacao) {
				mc.setMeta(bean.getMeta());
				metacomercialService.saveOrUpdate(mc);
			}
		}
		super.salvar(request, bean);
	}
	
	public ModelAndView ajaxFindMetaColaborador(WebRequestContext request, Metacomercial metacomercial){
		metacomercial = metacomercialService.load(metacomercial);
		List<Metacomercialitem> listametacomercialitem = metacomercialitemService.findForColaboradores(metacomercial);
		String whereIn = "";
		
		for (Metacomercialitem it : listametacomercialitem) {
			if (it.getColaborador() != null){
				whereIn += it.getColaborador().getCdpessoa() + ", ";
			}
		}
		whereIn = !whereIn.isEmpty() ? whereIn.substring(0, whereIn.length()-2) : "";
		
		List<Metacomercial> listametacomercial = metacomercialService.findForMetaColaboradores(metacomercial, whereIn);
		
		String whereInListametacomercial = CollectionsUtil.listAndConcatenate(listametacomercial, "cdmetacomercial", ",");
		return new JsonModelAndView().addObject("whereInListametacomercial", whereInListametacomercial);
	}
}