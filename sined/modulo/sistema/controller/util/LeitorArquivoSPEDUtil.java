package br.com.linkcom.sined.modulo.sistema.controller.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import br.com.linkcom.sined.geral.bean.Arquivo;

public class LeitorArquivoSPEDUtil {

	private class Line {
		int number;
		String[] line;

		public Line(int i, String[] fields) {
			this.number = i;
			this.line = fields;
		}
	}

	private class Bloco {
		List<Line> lines;
		int pointer;

		Bloco() {
			this.lines = new ArrayList<Line>();
		}
	}

	private static final String MSG_INVALID_FORMAT = "Formato de arquivo inv�lido! O arquivo a ser importado deve ser um SPED FISCAL em formato TXT!";
	private Map<String, Bloco> map;
	private Arquivo arquivo;
	private BufferedReader reader;

	public LeitorArquivoSPEDUtil(Arquivo arquivo, String... codigosBlocos) throws IOException {
		if (isArquivoSPEDValido(arquivo))
			this.arquivo = arquivo;
		init(codigosBlocos);
	}

	private void init(String[] codigosBlocos) throws IOException {
		open();
		map = new LinkedHashMap<String, Bloco>();
		for (String code : codigosBlocos)
			map.put(code, new Bloco());
		String l;
		int count = 1;
		while ((l = reader.readLine()) != null) {
			String[] fields = split(l, '|');
			String code = extractField(fields, 1);
			if (!map.containsKey(code))
				continue;
			Bloco bloco = map.get(code);
			bloco.lines.add(new Line(count++, fields));
			bloco.pointer = -1;
		}
	}

	public boolean next(String code) {
		Bloco bloco = map.get(code);
		if (bloco.pointer > size(code))
			return false;
		bloco.pointer++;
		return true;

	}
	
	public String field(String code, int numberField) {
		return extractField(line(code), numberField);
	}

	public boolean hasNext(String code) {
		Bloco bloco = map.get(code);
		return bloco.pointer + 1 < bloco.lines.size();
	}

	public int size(String code) {
		return map.get(code).lines.size();
	}

	private boolean isArquivoSPEDValido(Arquivo arquivo) {
		if (!"text/plain".equals(arquivo.getTipoconteudo())) {
			throw new IllegalArgumentException(MSG_INVALID_FORMAT);
		}
		
		return true;
	}

	public void reset() throws IOException {
		close();
		init((String[]) map.keySet().toArray());
	}

	public void open() throws IOException {
		this.reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(this.arquivo.getContent())));
		this.reader.mark(this.arquivo.getContent().length);
	}

	public void close() throws IOException {
		this.reader.close();
		this.reader = null;
	}

	@Override
	protected void finalize() throws Throwable {
		close();
		super.finalize();
	}

	public int pointer(String code) {
		return map.get(code).pointer;
	}

	public int numberLine(String code) {
		Bloco bloco = map.get(code);
		return bloco.lines.get(bloco.pointer).number;
	}

	private String[] split(String l, char simbol) {
		Queue<Integer> simbols = new LinkedList<Integer>();
		for (int i = 0; i < l.length(); i++)
			if (l.charAt(i) == simbol)
				simbols.add(i);
		String[] arr = new String[simbols.size()];
		int init = 0;
		int i = 0;
		while (!simbols.isEmpty()) {
			int fim = simbols.poll();
			arr[i++] = l.substring(init, fim);
			init = fim + 1;
		}
		return arr;
	}

	private String extractField(String[] line, int numberField) {
		if (line.length <= numberField)
			return "";
		return line[numberField].trim();
	}

	private String[] line(String code) {
		Bloco bloco = map.get(code);
		return bloco.lines.get(bloco.pointer).line;
	}

	public void resetPointers() {
		for(Bloco bloco: map.values())
			bloco.pointer = -1;
	}
}
