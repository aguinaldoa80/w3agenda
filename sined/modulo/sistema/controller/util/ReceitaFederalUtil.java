package br.com.linkcom.sined.modulo.sistema.controller.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import br.com.linkcom.neo.view.ajax.View;


public class ReceitaFederalUtil {
	
	/**
	 * 
	 * @param input
	 * @author Thiago Clemente
	 * 
	 */
	public static String getHtml(InputStream input, String charset) throws Exception {
		
		StringBuilder builder = new StringBuilder();
		String linha = null;
		BufferedReader reader;
		
		if (charset!=null){
			reader = new BufferedReader(new InputStreamReader(input, charset));
		}else {
			reader = new BufferedReader(new InputStreamReader(input));
		}
		

		try {
			while ((linha = reader.readLine()) != null) {
				builder.append(linha).append("\n");

			}
		}catch (IOException e) {
			e.printStackTrace();
		}

		return builder.toString();		
	}
	
	/**
	 * 
	 * @param view
	 * @author Thiago Clemente
	 * 
	 */
	public static void inicializaVariveisJs(View view){
		
		view.println("var urlRF;");
		view.println("var respostaRF;");
		view.println("var cnpjRF = '';");
		view.println("var situacaoRF = '';");
		view.println("var dataAberturaRF = '';");
		view.println("var razaoSocialRF = '';");
		view.println("var nomeFantasiaRF = '';");
		view.println("var cnaePrincipalRF = '';");
		view.println("var cnaesSecundariosRF = new Array();");
		view.println("var logradouroRF = '';");
		view.println("var numeroRF = '';");
		view.println("var complementoRF = '';");
		view.println("var cepRF = '';");
		view.println("var bairroRF = '';");
		view.println("var municipioRF = '';");
		view.println("var ufRF = '';");	
	}
	
	/**
	 * 
	 * @param view
	 * @param cnpj
	 * @param situacao
	 * @param dataAbertura
	 * @param razaoSocial
	 * @param nomeFantasia
	 * @param cnaePrincipal
	 * @param cnaesSecundarios
	 * @param logradouro
	 * @param numero
	 * @param complemento
	 * @param cep
	 * @param bairro
	 * @param municipio
	 * @param uf
	 * @param natureza
	 * @author Thiago Clemente
	 * 
	 */
	public static void preencheVariaveisJs(View view, String cnpj, String situacao, String dataAbertura, String razaoSocial, 
											String nomeFantasia, String cnaePrincipal, List<String> cnaesSecundarios, String logradouro, 
											String numero, String complemento, String cep, String bairro, Integer municipio, Integer uf,
											String natureza){
		
		view.println("cnpjRF = '" + cnpj.replace("'", "\\'") + "';");
		view.println("situacaoRF = '" + situacao.replace("'", "\\'") + "';");
		view.println("dataAberturaRF = '" + dataAbertura.replace("'", "\\'") + "';");
		view.println("razaoSocialRF = '" + razaoSocial.replace("'", "\\'") + "';");
		view.println("nomeFantasiaRF = '" + nomeFantasia.replace("'", "\\'") + "';");
		view.println("cnaePrincipalRF = '" + cnaePrincipal.replace("'", "\\'") + "';");
		view.println("logradouroRF = '" + logradouro.replace("'", "\\'") + "';");
		view.println("numeroRF = '" + numero.replace("'", "\\'") + "';");
		view.println("complementoRF = '" + complemento.replace("'", "\\'") + "';");
		view.println("cepRF = '" + cep.replace("'", "\\'") + "';");
		view.println("bairroRF = '" + bairro.replace("'", "\\'") + "';");
		view.println("municipioRF = 'br.com.linkcom.sined.geral.bean.Municipio[cdmunicipio=" + municipio + "]';");
		view.println("ufRF = 'br.com.linkcom.sined.geral.bean.Uf[cduf=" + uf + "]';");
		view.println("naturezaRF = '" + natureza.replace("'", "\\'") + "';");
		view.println("enderecoTipoRF = 'br.com.linkcom.sined.geral.bean.Enderecotipo[cdenderecotipo=1]';");
		for (int i=0; i<cnaesSecundarios.size(); i++){
			view.println("cnaesSecundariosRF[" + i + "] = '" + cnaesSecundarios.get(i) + "';");
		}
		
	}
	
	/**
	 * 
	 * @param cnae
	 * @author Thiago Clemente
	 * 
	 */
	public static boolean isCnaePattern(String cnae){
		//86.30-5-03
		return cnae.matches("\\d{2}\\.\\d{2}-\\d{1}-\\d{2}");
	}
	
	
	public static String getCampoCnpj(String campocnpj) throws Exception{
		
		campocnpj = campocnpj.substring(campocnpj.indexOf("CNPJ:&nbsp;"));
		campocnpj = campocnpj.substring(campocnpj.indexOf("ctl00$"));
		campocnpj = campocnpj.substring(0, campocnpj.indexOf("\""));
		
		return campocnpj;
	}
}