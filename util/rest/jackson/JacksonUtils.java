package br.com.linkcom.util.rest.jackson;

import java.sql.Timestamp;
import java.util.Date;

//import br.com.linkcom.neo.types.Money;

import br.com.linkcom.neo.types.Money;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class JacksonUtils {
	
	private static ObjectMapper mapper = null;
	public static ObjectMapper getObjectMapper(){
		if(mapper==null){
			mapper = new ObjectMapper(); 
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false); //impede que ele lance exce��o com propriedades que estejam no JSON, mas nao no objeto
			mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true); //impede que String vazia em um enum lance exce��o, e a interpreta como nulo
			mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
			
			SimpleModule jacksonRESTModule = new SimpleModule("jacksonRESTModule", new Version(1, 0, 0, null, null, null));
			jacksonRESTModule.addDeserializer(Timestamp.class, new TimestampDeserializer()); 
			jacksonRESTModule.addDeserializer(Date.class, new DateDeserializer());
			jacksonRESTModule.addDeserializer(java.sql.Date.class, new SqlDateDeserializer());
			jacksonRESTModule.addDeserializer(Money.class, new MoneyDeserializer()); 
			mapper.registerModule(jacksonRESTModule);
			JacksonCustomizer.customize(mapper);//TODO melhorar
		}
		return mapper;
	}
}
