package br.com.linkcom.util.rest.jackson;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class TimestampDeserializer extends JsonDeserializer<Timestamp>{

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	@Override
	public Timestamp deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		String value = jp.getValueAsString();
		try{
			if(value.length()>10)
				return new Timestamp(dateTimeFormat.parse(value).getTime());
			else return new Timestamp(dateFormat.parse(value).getTime());
		}catch(ParseException e){e.printStackTrace();
			throw new RuntimeException("Erro ao fazer parse da data '"+value+"'");
		}
		
	}

}
