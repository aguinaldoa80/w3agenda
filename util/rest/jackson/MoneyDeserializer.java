package br.com.linkcom.util.rest.jackson;

import java.io.IOException;

import br.com.linkcom.neo.types.Money;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class MoneyDeserializer extends JsonDeserializer<br.com.linkcom.neo.types.Money>{


	@Override
	public Money deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		String value = jp.getValueAsString();
		return new Money(value.replaceAll("\\,", "."));
		
	}

}
