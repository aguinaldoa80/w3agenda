package br.com.linkcom.util.rest.jackson;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class DateDeserializer extends JsonDeserializer<Date>{

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private static final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	
	private static final SimpleDateFormat dateFormatBarra = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat dateTimeFormatBarra = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	
	@Override
	public Date deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		String value = jp.getValueAsString();
		try{
			if (!value.matches("(([0-9 -])+)")) {
				if(value.length()>10){
					if(value.contains("/"))
						return dateTimeFormatBarra.parse(value);
					else
						return dateTimeFormat.parse(value);
				}
				else {
					if(value.contains("/"))
						return dateFormatBarra.parse(value);
					else
						return dateFormat.parse(value);
				}
			}else {
				return new Date(new Long(value));
			}
		}catch(ParseException e){
			throw new RuntimeException("Erro ao fazer parse da data '"+value+"'");
		}
		
	}

}
