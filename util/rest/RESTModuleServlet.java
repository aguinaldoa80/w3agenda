package br.com.linkcom.util.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.linkcom.neo.classmanager.ClassManager;
import br.com.linkcom.neo.classmanager.WebClassRegister;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.util.rest.annotations.URI;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;
import br.com.linkcom.util.rest.jackson.JacksonUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * Servlet respons�vel pelo M�dulo REST da Aplica��o
 * @see SimpleRESTWS
 * @author igorcosta
 * 
 * Create = PUT with a new URI
 *         POST to a base URI returning a newly created URI
 *	Read   = GET
 * 	Update = PUT with an existing URI
 * 	Delete = DELETE
 *
 */
public class RESTModuleServlet extends HttpServlet {

	private enum MetodoHTTP{GET, POST, PUT, DELETE}
	private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
	private static final long serialVersionUID = -7692026013327452897L;
	protected static final Log log = LogFactory.getLog(RESTModuleServlet.class);
	
	@SuppressWarnings("unchecked")
	private List<Class> controllers = new ArrayList<Class>();
	@SuppressWarnings("unchecked")
	private Map<String, Class> mapURI = new HashMap<String, Class>();
	
	
	
	/**
	 * Ao iniciar a App, ele pega todos as Classes anotadas com @URI
	 * @throws IOException 
	 */
	public RESTModuleServlet() throws IOException {
		super();
	}

	@SuppressWarnings("unchecked")
	private void initRESTModule() throws IOException {
		long l = new Date().getTime();
		
//		WebApplicationContext parent = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
//		WebApplicationContext wac = createWebApplicationContext(parent);
		
		
		ClassManager classManager = WebClassRegister.getClassManager(getServletContext(), "br.com.linkcom.neo");
		controllers.addAll(Arrays.asList(classManager.getClassesWithAnnotation(URI.class)));
		StringBuilder urls = new StringBuilder();
		for(Class clazz : controllers){
			Annotation annotation = clazz.getAnnotation(URI.class);
			//TODO validar se a classe implementa a interface SimpleRESTWS
//			if(!clazz.getSuperclass().equals(SimpleRESTWS.class)){
//				throw new RESTException("As classes que tem a annotation @URI devem implementar a interface SimpleRESTWS.");
//			}
			URI URL = (URI) annotation;
			mapURI.put(URL.value(), clazz);
			urls.append(URL.value()).append(",");
		}
		long tempo = new Date().getTime()-l;
		log.info("RESTModuleServlet Servlet Iniciado em "+tempo+"ms. URLs: ["+urls.toString()+"]");
		classManager = null;
	}

	/**
	 * Met�do que encapsula a l�gica do Controller.
	 * Ele faz o binding da classe command e despacha para o met�do especifico correspondente ao Metodo HTTP da requisi��o.  
	 * @param request
	 * @param response
	 * @param metodo
	 */
	@SuppressWarnings("unchecked")
	private void call(HttpServletRequest request, HttpServletResponse response, MetodoHTTP metodo){
		
		String uri = request.getPathInfo();
		SimpleRESTWS ws = getWS(uri);
		
		if(ws instanceof AuthenticatedRESTWS){
			String token = request.getHeader("sec-token"); 
//			User user = ((AuthenticatedRESTWS)ws).verifyCredentials(token); //TODO melhorar e expor o objeto usuario
			((AuthenticatedRESTWS)ws).verifyCredentials(token);
		}
		
		Object command = newCommandInstance(ws);
		try {
			command = bindCommandRequest(request, command);
		} catch (Exception e) {
			throw new RESTException("Erro ao instanciar classe command.", e);
		}
		bindCommandURI(getWSURLByClass(ws.getClass()), uri, command);
		
		ModelAndStatus ms;
		HTTPStatus defaultStatus;
		if(metodo.equals(MetodoHTTP.GET)){
			ms = ws.get(command);
			defaultStatus = HTTPStatus.OK;
		}else if(metodo.equals(MetodoHTTP.POST)){
			ms = ws.post(command);
			defaultStatus = HTTPStatus.CREATED;
		}else if(metodo.equals(MetodoHTTP.PUT)){
			ms = ws.put(command);
			defaultStatus = HTTPStatus.OK;
		}else if(metodo.equals(MetodoHTTP.DELETE)){
			ms = ws.delete(command);
			defaultStatus = HTTPStatus.OK;
		}else{
			throw new RESTException("Met�do n�o suportado.");
		}
		
		if(ms==null)
			ms = new ModelAndStatus();
		
		if(ms.getStatus()==null){
			ms.setStatus(defaultStatus);
		}
		
		response.setStatus(ms.getStatus().getCode());
		try {
			String json = JacksonUtils.getObjectMapper().writeValueAsString(ms.getModel());
			response.setContentType(CONTENT_TYPE);
			response.getWriter().print(json);
		} catch (JsonProcessingException e) {
			new RESTException("Erro ao converter model para JSON", e);
		} catch (IOException e) {
			new RESTException("Erro ao retornar JSON", e);
		}
		
	}
	
	/**
	 * Retorna a URI correspondente a um WS
	 * @param classParam Classe do WS
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String getWSURLByClass(Class<? extends SimpleRESTWS> classParam) {
		for(Entry<String, Class> entry: mapURI.entrySet()){
			if(entry.getValue().equals(classParam)){
				return entry.getKey();
			}
		}
		return null;
	}

	/**
	 * Realiza o binding dos parametros enviados na URI - TODO
	 * @param URLWS
	 * @param uri
	 * @param command
	 */
	private void bindCommandURI(String URLWS, String uri, Object command) {
//		Pattern patternURI = Pattern.compile("(:\\w+)", Pattern.CASE_INSENSITIVE);
//		Pattern patternParametros = Pattern.compile("/(\\d+)", Pattern.CASE_INSENSITIVE);
//		Matcher matcherURI = patternURI.matcher(uri);
//		Matcher matcherParametros = patternParametros.matcher(URLWS);
//		while(matcherURI.matches()){
//			String valorStr = matcherURI.group(1);
//			String nomeParametro = null;
//			if(matcherParametros.matches()){
//				nomeParametro = matcherParametros.group(1);
//			}
//			System.out.println(valorStr + " === " + nomeParametro);
//		}
	}

	/**
	 * Realiza o bind do JSON recebido para a classe de binding correspondente.
	 */
	@SuppressWarnings("unchecked")
	private Object bindCommandRequest(HttpServletRequest request, Object command) throws IOException, InstantiationException, IllegalAccessException {
		Class clazz = command.getClass();
		StringBuilder sb = new StringBuilder();
		BufferedReader br = request.getReader();
		
		String line;
		while ((line = br.readLine()) != null){
		    sb.append(line);
		}
		String contentJSON = sb.toString();
		/*Usado somente para passar o parametro dtultimasincronizacao,
		 * para o bind.
		 */
		if(request.getParameter("dtUltimaSincronizacao") != null){
			//contentJSON = "{\"dtUltimaSincronizacao\" : "+ Timestamp.valueOf(request.getParameter("dtUltimaSincronizacao")).getTime()+"}";
			contentJSON = "{\"dtUltimaSincronizacao\" : "+ request.getParameter("dtUltimaSincronizacao")+"}";
		}
		System.out.println("JSON:: " + contentJSON);
		if(StringUtils.isNotEmpty(contentJSON)){
			try {
				return JacksonUtils.getObjectMapper().readValue(contentJSON, clazz);
			} catch (JsonParseException e) {
				throw new RESTException("Erro ao fazer parse de JSON do command", e);
			} catch (JsonMappingException e) {
				throw new RESTException("Erro ao fazer mapeamento de JSON do command", e);
			}
		}else 
			return clazz.newInstance();
	}

	/**
	 *	Cria uma nova inst�ncia da classe Comand de um SimpleRESTWS<COMMAND> 
	 * @param ws
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Object newCommandInstance(SimpleRESTWS<?> ws)  {
		ParameterizedType t = (ParameterizedType)ws.getClass().getGenericInterfaces()[0];
		try {
			Class c = (Class)t.getActualTypeArguments()[0];
			return c.newInstance();
		} catch (Exception e) {
			throw new RESTException("Erro ao instanciar classe command.", e);
		} 
	}

	/**
	 * Obt�m o singleton do SimpleRESTWS<?> correspondente a URI da requisi��o
	 * @param uri
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private  SimpleRESTWS<?> getWS(String uri) {
		for(String url : mapURI.keySet()){
			if(uri.startsWith(url)){//TODO processar url
				Class clazz = mapURI.get(url);
				try {
					return (SimpleRESTWS<?>) Neo.getObject(clazz);
				} catch (Exception e) {
					throw new RESTException("Erro ao instanciar classe controller.", e);
				}
			}
		}
		throw new RESTException("N�o foi poss�vel localizar um WebService para a URI solicitada.");
	}

	public void doCall(HttpServletRequest request, HttpServletResponse response, MetodoHTTP metodo){
		try{
			
			call(request, response, metodo);
		}catch(RESTException e){
			onError(e);
			response.setStatus(HTTPStatus.INTERNAL_SERVER_ERROR.getCode());
			response.setContentType(CONTENT_TYPE);
			try {
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				
				RESTErrorBean model = new RESTErrorBean(e.getMessage(), (e.getCause()!=null ? e.getCause().getMessage() : "Sem root cause") + " - " + stringWriter.toString());
				String json = JacksonUtils.getObjectMapper().writeValueAsString(model);
				response.setContentType(CONTENT_TYPE);
				response.getWriter().print(json);
			} catch (IOException e1) {
				onError(e);
			}
		}catch(NotAuthenticatedException e){
			response.setStatus(HTTPStatus.FORBIDDEN.getCode());
			try {
				RESTErrorBean model = new RESTErrorBean(e.getMessage(), "");
				String json = JacksonUtils.getObjectMapper().writeValueAsString(model);
				response.setContentType(CONTENT_TYPE);
				response.getWriter().print(json);
			} catch (IOException e2) {
				onError(e);
			}
		}
		catch(Exception e){
			onError(e);
			response.setStatus(HTTPStatus.INTERNAL_SERVER_ERROR.getCode());
			try {
				StringWriter stringWriter = new StringWriter();
				e.printStackTrace(new PrintWriter(stringWriter));
				
				RESTErrorBean model = new RESTErrorBean("Ocorreu um erro ao processar a requisi��o.",  stringWriter.toString());
				String json = JacksonUtils.getObjectMapper().writeValueAsString(model);
				response.setContentType(CONTENT_TYPE);
				response.getWriter().print(json);
			} catch (IOException e2) {
				onError(e);
			}
		}
	}
	
	/**
	 * Met�do para onde s�o direcionados os erros fatais.
	 * @param e
	 */
	protected void onError(Exception e) {
		e.printStackTrace();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doCall(request, response, MetodoHTTP.GET);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doCall(request, response, MetodoHTTP.POST);
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doCall(request, response, MetodoHTTP.PUT);
	}
	
	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doCall(request, response, MetodoHTTP.DELETE); 
	}

	public void init() throws ServletException {
		try {
			initRESTModule();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

