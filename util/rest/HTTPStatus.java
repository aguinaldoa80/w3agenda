package br.com.linkcom.util.rest;

/**
 * C�digos HTTP utilizados pelo {@link SimpleRESTWS}
 * @author igorcosta
 *
 */
public enum HTTPStatus {
	OK(200), 
	CREATED(201), 
	UNAUTHORIZED(401), 
	FORBIDDEN(403), 
	NOT_FOUND(404), 
	INTERNAL_SERVER_ERROR(500),
	NOT_IMPLEMENTED(501);
	
	private int code;
	
	HTTPStatus(int code){
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
}
