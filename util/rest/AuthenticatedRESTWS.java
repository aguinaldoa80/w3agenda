package br.com.linkcom.util.rest;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.util.rest.authentication.NotAuthenticatedException;

public interface AuthenticatedRESTWS {
	public User verifyCredentials(String token) throws NotAuthenticatedException;
}
