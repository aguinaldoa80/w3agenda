package br.com.linkcom.util.rest;

/**
 * Interface dos WS REST da app.
 * @author igorcosta
 *
 * @param <COMMAND> Classe command que ser� feito o binding json.
 */
public interface SimpleRESTWS<COMMAND> {
	public ModelAndStatus get(COMMAND command);
	public ModelAndStatus post(COMMAND command);
	public ModelAndStatus put(COMMAND command);
	public ModelAndStatus delete(COMMAND command);
	
}
