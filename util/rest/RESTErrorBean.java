package br.com.linkcom.util.rest;


public class RESTErrorBean {
	
	private String userMessage;
	private String developerMessage;
	
    public RESTErrorBean(){}
	
	public RESTErrorBean(String message, String erro){
		this.userMessage = message;
		this.developerMessage = erro;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public String getDeveloperMessage() {
		return developerMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
}
