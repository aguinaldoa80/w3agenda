package br.com.linkcom.util.rest;

/**
 * Command padr�o de erro padrao {@link SimpleRESTWS}
 * @author igorcosta
 *
 */
public class ErrorCommand {

	private String userMessage;
	
	public String getUserMessage() {
		return userMessage;
	}
	
	public void setUserMessage(String mensagem) {
		this.userMessage = mensagem;
	}
}
