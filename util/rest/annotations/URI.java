package br.com.linkcom.util.rest.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import br.com.linkcom.util.rest.SimpleRESTWS;

/**
 * Utilizado para o mapeamento do {@link SimpleRESTWS} 
 * @author igorcosta
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface URI {
	/**
	 * URI do WS
	 * @return
	 */
	String value();
}
