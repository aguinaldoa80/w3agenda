package br.com.linkcom.util.rest;

import java.sql.Timestamp;

/**
 * Classe que representa o retorno efetuados pelo @see {@link SimpleRESTWS}
 * @author igorcosta
 *
 */
public class ModelAndStatus {
	private Object model;
	private HTTPStatus status;
	private Timestamp dtSincronizacao;
	
	public ModelAndStatus() {
		
	}
	
	public ModelAndStatus(Object model) {
		this.model = model;
	}
	
	public ModelAndStatus(Object model, HTTPStatus status) {
		this.model = model;
		this.status = status;
	}
	
	
	public Object getModel() {
		return model;
	}
	public HTTPStatus getStatus() {
		return status;
	}
	public void setModel(Object model) {
		this.model = model;
	}
	
	public void setStatus(HTTPStatus status) {
		this.status = status;
	}

	public Timestamp getDtSincronizacao() {
		return dtSincronizacao;
	}

	public void setDtSincronizacao(Timestamp dtSincronizacao) {
		this.dtSincronizacao = dtSincronizacao;
	}
	
	
	
}
