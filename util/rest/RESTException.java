package br.com.linkcom.util.rest;

public class RESTException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String developerMessage;
	
	
	public RESTException() {
		super();
	}

	public RESTException(String userMessage, Throwable cause) {
		super(userMessage, cause);
	}
	
	public RESTException(String userMessage, String developerMessage, Throwable cause) {
		super(userMessage, cause);
		this.developerMessage = developerMessage;
	}

	public RESTException(String userMessage) {
		super(userMessage);
	}

	public RESTException(String userMessage, String developerMessage) {
		super(userMessage);
		this.developerMessage = developerMessage;
	}

	
	public RESTException(Throwable cause) {
		super(cause);
	}
	
	public String getDeveloperMessage() {
		return developerMessage;
	}
	
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
		
}
