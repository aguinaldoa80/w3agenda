package br.com.linkcom.geradorrelatorio.crud;

import br.com.linkcom.geradorrelatorio.bean.GrupoRelatorio;
import br.com.linkcom.geradorrelatorio.crud.filtro.GrupoRelatorioFiltro;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.util.controller.CrudControllerSined;


@Controller(path="/sistema/crud/GrupoRelatorio", authorizationModule=CrudAuthorizationModule.class)
public class GrupoRelatorioCrud extends CrudControllerSined <GrupoRelatorioFiltro, GrupoRelatorio, GrupoRelatorio> {

	@Override
	protected void salvar(WebRequestContext request, GrupoRelatorio bean)
			throws Exception {
		// TODO Auto-generated method stub
		super.salvar(request, bean);
	}
	
	@Override
	protected void entrada(WebRequestContext request, GrupoRelatorio form)
			throws Exception {
		// TODO Auto-generated method stub
		super.entrada(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request,
			GrupoRelatorioFiltro filtro) throws Exception {
		// TODO Auto-generated method stub
		super.listagem(request, filtro);
	}

}
