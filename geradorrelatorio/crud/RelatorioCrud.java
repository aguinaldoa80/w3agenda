package br.com.linkcom.geradorrelatorio.crud;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBeanHistorico;
import br.com.linkcom.geradorrelatorio.bean.enumeration.GeradorRelatorioBeanHistoricoAcao;
import br.com.linkcom.geradorrelatorio.crud.filtro.GeradorRelatorioFiltro;
import br.com.linkcom.geradorrelatorio.service.GeradorRelatorioBeanHistoricoService;
import br.com.linkcom.geradorrelatorio.service.GrupoRelatorioService;
import br.com.linkcom.geradorrelatorio.service.RelatorioService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path="/sistema/crud/Relatorio", authorizationModule=CrudAuthorizationModule.class)
public class RelatorioCrud extends AbstractGeradorRelatorioCrud<GeradorRelatorioFiltro, GeradorRelatorioBean, GeradorRelatorioBean> {

	protected RelatorioService relatorioservice;
	protected GrupoRelatorioService grupoRelatorioService;
	protected GeradorRelatorioBeanHistoricoService geradorRelatorioBeanHistoricoService;

	public void setRelatorioservice(RelatorioService relatorioservice) {
		this.relatorioservice = relatorioservice;
	}
	
	public void setGeradorRelatorioBeanHistoricoService(
			GeradorRelatorioBeanHistoricoService geradorRelatorioBeanHistoricoService) {
		this.geradorRelatorioBeanHistoricoService = geradorRelatorioBeanHistoricoService;
	}

	public void setGrupoRelatorioService(
			GrupoRelatorioService grupoRelatorioService) {
		this.grupoRelatorioService = grupoRelatorioService;
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, GeradorRelatorioBean form) throws CrudException {
		if(!isQueryValidaParaDisponibilizarAreaCliente(form))
			throw new SinedException("� permitido marcar a flag \"Disponibilizar na �rea do cliente\" apenas se na query possuir a vari�vel \"@CLIENTE\" na cl�usula WHERE.");
		return super.doSalvar(request, form);
	}
	
	@Override
	protected void listagem(WebRequestContext request,
			GeradorRelatorioFiltro filtro) throws Exception {
		request.setAttribute("listaGruporelatorio", grupoRelatorioService.findAll("grupoRelatorio.nome"));
		super.listagem(request, filtro);
	}
	
	@Override
	protected void entrada(WebRequestContext request, GeradorRelatorioBean form)
			throws Exception {
		request.setAttribute("listaGruporelatorio", grupoRelatorioService.findAll("grupoRelatorio.nome"));
	
		if(form.getCdgeradorrelatorio() != null ) {
		List<GeradorRelatorioBeanHistorico> listaRelatoriohistorico = geradorRelatorioBeanHistoricoService.findRelatorioHistorico(form);
		
			if(SinedUtil.isListNotEmpty(listaRelatoriohistorico)){
				form.setListaGeradorRelatorioBeanHistorico(listaRelatoriohistorico);			
			}	
		}
		
		super.entrada(request, form);
	}
	/*
	 * Verifica se o atributo disponibilizarareacliente do geradorrelatorio est� marcado e se
	 * estiver verifica se existe a variavel @cliente ap�s o where da query.
	 */
	private boolean isQueryValidaParaDisponibilizarAreaCliente(GeradorRelatorioBean geradorrelatorio){
		if(geradorrelatorio.getDisponibilizarAreaCliente() == null || !geradorrelatorio.getDisponibilizarAreaCliente())
			return true;
		String query = geradorrelatorio.getQuery().toLowerCase();
		int iwhere = query.indexOf("where");
		int icliente = -1;
		if(iwhere != -1)
			icliente = query.indexOf("@cliente", iwhere);
		return icliente != -1;
	}
	
	
	
	@Override
	protected void salvar(WebRequestContext request, GeradorRelatorioBean bean)
			throws Exception {
		GeradorRelatorioBeanHistorico geradorRelatorioBeanHistorico = new GeradorRelatorioBeanHistorico();
		boolean isCriar = bean.getCdgeradorrelatorio() == null;
		
		if(bean.getCdgeradorrelatorio() != null) {
		geradorRelatorioBeanHistorico =	relatorioservice.setValoresAnterior(bean);
		}
		
		super.salvar(request, bean);
	
		geradorRelatorioBeanHistorico.setCdusuarioaltera(SinedUtil.getCdUsuarioLogado());
		geradorRelatorioBeanHistorico.setDtaltera(new Timestamp(System.currentTimeMillis()));
		geradorRelatorioBeanHistorico.setGeradorRelatorioBean(bean);
		
		if(isCriar){
			geradorRelatorioBeanHistorico.setAcao(GeradorRelatorioBeanHistoricoAcao.CRIADO);
		} else {
			geradorRelatorioBeanHistorico.setAcao(GeradorRelatorioBeanHistoricoAcao.ALTERADO);
		}
		geradorRelatorioBeanHistoricoService.saveOrUpdate(geradorRelatorioBeanHistorico);
	}
	
	public ModelAndView visualizarGeradorRelatorioBeanHistorico (WebRequestContext request, GeradorRelatorioBeanHistorico geradorRelatorioBeanHistorico){
		geradorRelatorioBeanHistorico = geradorRelatorioBeanHistoricoService.loadForEntrada(geradorRelatorioBeanHistorico);
	
		request.setAttribute("TEMPLATE_beanName", geradorRelatorioBeanHistorico);
		request.setAttribute("TEMPLATE_beanClass", GeradorRelatorioBeanHistorico.class);
		request.setAttribute("observacao", "Relat�rio Hist�rico");
		request.setAttribute("consultar", true);

		return new ModelAndView("direct:crud/popup/visualizacaoGeradorRelatorioBeanHistorico").addObject("geradorRelatorioBeanHistorico", geradorRelatorioBeanHistorico);
	}
}
