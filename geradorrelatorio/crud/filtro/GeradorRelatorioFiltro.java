package br.com.linkcom.geradorrelatorio.crud.filtro;

import br.com.linkcom.geradorrelatorio.bean.GrupoRelatorio;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;


public class GeradorRelatorioFiltro extends FiltroListagemSined{
	GrupoRelatorio grupoRelatorio;
	protected String nome;
	protected String query;

	public GeradorRelatorioFiltro() {}

	@DisplayName("Grupo de Relatório")
	public GrupoRelatorio getGrupoRelatorio() {
		return grupoRelatorio;
	}

	public void setGrupoRelatorio(GrupoRelatorio grupoRelatorio) {
		this.grupoRelatorio = grupoRelatorio;
	}
	
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@DisplayName("Query")
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
}
