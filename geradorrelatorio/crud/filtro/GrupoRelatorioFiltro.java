package br.com.linkcom.geradorrelatorio.crud.filtro;

import br.com.linkcom.sined.util.controller.FiltroListagemSined;

public class GrupoRelatorioFiltro extends FiltroListagemSined{
	private String nome;

	public GrupoRelatorioFiltro() {}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
