package br.com.linkcom.geradorrelatorio.crud;

import br.com.linkcom.geradorrelatorio.bean.GeradorWSBean;
import br.com.linkcom.geradorrelatorio.crud.filtro.GeradorWSFiltro;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;

@Controller(path="/sistema/crud/Webservice", authorizationModule=CrudAuthorizationModule.class)
public class WSCrud extends AbstractGeradorWSCrud<GeradorWSFiltro, GeradorWSBean, GeradorWSBean>  {

}
