package br.com.linkcom.geradorrelatorio.crud;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.AbstractReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.crud.filtro.ReportTemplateCrudFiltro;
import br.com.linkcom.geradorrelatorio.reporttemplate.ReportTemplateController;
import br.com.linkcom.geradorrelatorio.reporttemplate.W3ReportTemplateFunctions;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.neo.authorization.crud.CrudAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.crud.CrudException;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.util.SinedException;

@Controller(path="/sistema/crud/RelatorioTemplate", authorizationModule=CrudAuthorizationModule.class)
public class ReportTemplateCrud extends	AbstractGeradorRelatorioTemplateCrud<ReportTemplateCrudFiltro, ReportTemplateBean, ReportTemplateBean> {

	protected ReportTemplateService reportTemplateService;
	
	public void setReportTemplateService(ReportTemplateService reportTemplateService) {this.reportTemplateService = reportTemplateService;}

	@Override
	protected List<String> getParametrosSistema() {
		List<String> lista = new ListSet<String>(String.class);
		for(ReportTemplateController.SISTEMA variavel : ReportTemplateController.SISTEMA.values()){
			lista.add(variavel.name());
		}
		Collections.sort(lista);
		return lista;
	}

	@Override
	protected List<String> getReportFunctions() {
		List<String> lista = new ListSet<String>(String.class);
		for(Method metodo : W3ReportTemplateFunctions.class.getDeclaredMethods()){
			String metodoSTR = "(" + metodo.getReturnType().getSimpleName() + ") " + metodo.getName() + "(";
			for(Class<?> arg : metodo.getParameterTypes()){
					metodoSTR += arg.getSimpleName() + ", ";
			}
			if(metodoSTR.contains(",")){
				metodoSTR = metodoSTR.substring(0, metodoSTR.lastIndexOf(","));
			}
			lista.add(metodoSTR + ")");
		}
		for(Method metodo : W3ReportTemplateFunctions.class.getSuperclass().getDeclaredMethods()){
			String metodoSTR = "(" + metodo.getReturnType().getSimpleName() + ") " + metodo.getName() + "(";
			for(Class<?> arg : metodo.getParameterTypes()){
					metodoSTR += arg.getSimpleName() + ", ";
			}
			if(metodoSTR.contains(",")){
				metodoSTR = metodoSTR.substring(0, metodoSTR.lastIndexOf(","));
			}
			lista.add(metodoSTR + ")");
		}
		Collections.sort(lista);
		return lista;
	}
	
	@Override
	protected void salvar(WebRequestContext request, AbstractReportTemplateBean bean) throws Exception {
		
		AbstractReportTemplateBean reportTemplateDanfe = reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.DANFE);
		AbstractReportTemplateBean reportTemplatePneu = reportTemplateService.loadTemplateByCategoria(EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU);
		
		
		if(EnumCategoriaReportTemplate.DANFE.equals(bean.getCategoria()) && reportTemplateDanfe !=null){
			if(bean.getCdreporttemplate()==null || !bean.equals(reportTemplateDanfe)){
				throw new SinedException("� permitida somente uma configura��o de relat�rio com a categoria Emiss�o de Danfe.");
			}
		}
		if(EnumCategoriaReportTemplate.ETIQUETA_DO_PNEU.equals(bean.getCategoria()) && reportTemplatePneu !=null){
			if(bean.getCdreporttemplate()==null || !bean.equals(reportTemplatePneu)){
				throw new SinedException("� permitida somente uma configura��o de relat�rio com a categoria Emitir de Pneu Produzido/Recusado.");
			}
		}
		
		try {
			super.salvar(request, bean);
		} catch (Exception e) {
			if(e.getCause().getMessage().indexOf("reporttemplate_nome_key") > -1){
				throw new SinedException("J� existe um modelo de relat�rio com esse nome.");
			}
			throw new CrudException(SALVAR, e);
		}
	}

}
