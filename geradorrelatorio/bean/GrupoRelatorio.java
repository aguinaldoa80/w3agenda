package br.com.linkcom.geradorrelatorio.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@Table(name="gruporelatorio")
@DisplayName(value="Cadastro de grupo de relatório")
@SequenceGenerator(name = "sq_gruporelatorio", sequenceName = "sq_gruporelatorio")

public class GrupoRelatorio implements Log{
	
	protected Integer cdgruporelatorio;
	protected String nome;
	protected Timestamp dtaltera;
	protected Integer cdusuarioaltera;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sq_gruporelatorio")
	public Integer getCdgruporelatorio(){
		return cdgruporelatorio;
	}
	public void setCdgruporelatorio(Integer cdgruporelatorio) {
		this.cdgruporelatorio = cdgruporelatorio;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
}
