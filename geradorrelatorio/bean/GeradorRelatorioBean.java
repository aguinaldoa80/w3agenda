package br.com.linkcom.geradorrelatorio.bean;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.linkcom.geradorrelatorio.util.pdf.WKHTMLConfigEnum;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.sined.geral.bean.Arquivo;

@Entity
@Table(name="geradorrelatorio")
@DisplayName(value="Cadastro de relat�rio")
public class GeradorRelatorioBean extends AbstractGeradorEntity {
	
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	protected Integer cdgeradorrelatorio;
	protected Boolean gerarrelatorio;
	protected Boolean gerargrafico;
	protected Boolean gerarcsv;
	protected Boolean gerartxt;
	protected Boolean gerarpdf;
	protected Boolean gerarhtml;
	protected Boolean gerarW3erp;
	protected Boolean visualizardados;
	protected Boolean exibirtotalizadores;
	protected Boolean disponibilizarAreaCliente;
	protected List<GeradorGraficoBean> listaGeradorGrafico;
	protected String x;
	protected String y;
	protected String serie;
	protected String leiauterelatorio;
	protected String leiautecabecalho;
	protected String leiauterodape;
	protected WKHTMLConfigEnum orientacao;
	protected Integer margemSuperior;
	protected Integer margemInferior;
	protected Integer margemEsquerda;
	protected Integer margemDireita;
	protected Integer largura;
	protected Integer altura;
	protected GrupoRelatorio grupoRelatorio;
	protected Set<GeradorRelatorioTotalizadorBean> listaGeradorRelatorioTotalizador  = new ListSet<GeradorRelatorioTotalizadorBean>(GeradorRelatorioTotalizadorBean.class);
	protected Set<GeradorPermissaoBean> listaGeradorPermissao = new ListSet<GeradorPermissaoBean>(GeradorPermissaoBean.class);
	protected List<GeradorRelatorioBeanHistorico> listaGeradorRelatorioBeanHistorico = new ListSet<GeradorRelatorioBeanHistorico>(GeradorRelatorioBeanHistorico.class);
	protected String descricao;
	protected String observacao;
	protected Integer qtdeCamposLinha;
	private Arquivo arquivo;
	
	
	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	@Id
	public Integer getCdgeradorrelatorio(){
		return cdgeradorrelatorio;
	}
	public void setCdgeradorrelatorio(Integer cdgeradorrelatorio) {
		this.cdgeradorrelatorio = cdgeradorrelatorio;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdarquivo")
	@DisplayName("Arquivo")
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	
	@DisplayName("Grupo de relat�rio")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgruporelatorio")
	public GrupoRelatorio getGrupoRelatorio() {
		return grupoRelatorio;
	}
	
	public Boolean getGerarrelatorio() {
		return gerarrelatorio;
	}
	public void setGerarrelatorio(Boolean gerarrelatorio) {
		this.gerarrelatorio = gerarrelatorio;
	}

	public Boolean getGerargrafico() {
		return gerargrafico;
	}
	public void setGerargrafico(Boolean gerargrafico) {
		this.gerargrafico = gerargrafico;
	}

	public Boolean getGerartxt() {
		return gerartxt;
	}
	public Boolean getGerarhtml() {
		return gerarhtml;
	}
	public void setGerarhtml(Boolean gerarhtml) {
		this.gerarhtml = gerarhtml;
	}
	public void setGerartxt(Boolean gerartxt) {
		this.gerartxt = gerartxt;
	}
	
	public Boolean getGerarcsv() {
		return gerarcsv;
	}
	public void setGerarcsv(Boolean gerarcsv) {
		this.gerarcsv = gerarcsv;
	}
	
	public Boolean getVisualizardados() {
		return visualizardados;
	}
	public void setVisualizardados(Boolean visualizardados) {
		this.visualizardados = visualizardados;
	}
	
	@OneToMany(mappedBy="geradorrelatorio")
	public List<GeradorGraficoBean> getListaGeradorGrafico() {
		return listaGeradorGrafico;
	}
	public void setListaGeradorGrafico(List<GeradorGraficoBean> listaGeradorGrafico) {
		this.listaGeradorGrafico = listaGeradorGrafico;
	}
	
	@DisplayName("Eixo x")
	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	
	@DisplayName("Eixo y")
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	
	@DisplayName("S�rie")
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	@DisplayName("Leiaute relat�rio")
	public String getLeiauterelatorio() {
		return leiauterelatorio;
	}
	public void setLeiauterelatorio(String leiauterelatorio) {
		this.leiauterelatorio = leiauterelatorio;
	}
	
	@DisplayName("Leiaute cabe�alho")
	public String getLeiautecabecalho() {
		return leiautecabecalho;
	}
	public void setLeiautecabecalho(String leiautecabecalho) {
		this.leiautecabecalho = leiautecabecalho;
	}
	
	@DisplayName("Leiaute rodap�")
	public String getLeiauterodape() {
		return leiauterodape;
	}
	public void setLeiauterodape(String leiauterodape) {
		this.leiauterodape = leiauterodape;
	}
	
	@DisplayName("Orienta��o")
	public WKHTMLConfigEnum getOrientacao() {
		if(orientacao == null){
			orientacao = WKHTMLConfigEnum.ORIENTATION_PORTRAIT;
		}
		return orientacao;
	}
	public void setOrientacao(WKHTMLConfigEnum orientacao) {
		this.orientacao = orientacao;
	}
	
	@DisplayName("Margem Superior (mm)")
	public Integer getMargemSuperior() {
		return margemSuperior;
	}
	public void setMargemSuperior(Integer margemSuperior) {
		this.margemSuperior = margemSuperior;
	}
	
	@DisplayName("Margem Inferior (mm)")
	public Integer getMargemInferior() {
		return margemInferior;
	}
	public void setMargemInferior(Integer margemInferior) {
		this.margemInferior = margemInferior;
	}

	@DisplayName("Margem Esquerda (mm)")
	public Integer getMargemEsquerda() {
		return margemEsquerda;
	}
	public void setMargemEsquerda(Integer margemEsquerda) {
		this.margemEsquerda = margemEsquerda;
	}

	@DisplayName("Margem Direita (mm)")
	public Integer getMargemDireita() {
		return margemDireita;
	}
	public void setMargemDireita(Integer margemDireita) {
		this.margemDireita = margemDireita;
	}
	
	@DisplayName("Largura da p�gina (mm)")
	public Integer getLargura() {
		return largura;
	}
	public void setLargura(Integer largura) {
		this.largura = largura;
	}
	
	@DisplayName("Altura da p�gina (mm)")
	public Integer getAltura() {
		return altura;
	}
	public void setAltura(Integer altura) {
		this.altura = altura;
	}

	
	
	

	@DisplayName("Exibir Totalizadores")
	public Boolean getExibirtotalizadores() {
		return exibirtotalizadores;
	}
	
	
	public void setGrupoRelatorio(GrupoRelatorio grupoRelatorio) {
		this.grupoRelatorio = grupoRelatorio;
	}
	public void setExibirtotalizadores(Boolean exibirtotalizadores) {
		this.exibirtotalizadores = exibirtotalizadores;
	}
	
	@OneToMany(mappedBy="geradorrelatorio")
	public Set<GeradorRelatorioTotalizadorBean> getListaGeradorRelatorioTotalizador() {
		return listaGeradorRelatorioTotalizador;
	}
	public void setListaGeradorRelatorioTotalizador(Set<GeradorRelatorioTotalizadorBean> listaGeradorRelatorioTotalizador) {
		this.listaGeradorRelatorioTotalizador = listaGeradorRelatorioTotalizador;
	}
	
	public Boolean getGerarpdf() {
		return gerarpdf;
	}
	
	public void setGerarpdf(Boolean gerarpdf) {
		this.gerarpdf = gerarpdf;
	}
	
	public Boolean getGerarW3erp() {
		return gerarW3erp;
	}
	public void setGerarW3erp(Boolean gerarW3erp) {
		this.gerarW3erp = gerarW3erp;
	}
	
	@OneToMany(mappedBy="geradorrelatorio")
	public Set<GeradorPermissaoBean> getListaGeradorPermissao() {
		return listaGeradorPermissao;
	}
	public void setListaGeradorPermissao(Set<GeradorPermissaoBean> listaGeradorPermissao) {
		this.listaGeradorPermissao = listaGeradorPermissao;
	}
	
	@DisplayName("Descri��o do Relat�rio")
	@Required
	@MaxLength(500)
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@DisplayName("Observa��o")
	@MaxLength(200)
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@DisplayName("Disponibilizar na �rea do cliente")
	public Boolean getDisponibilizarAreaCliente() {
		return disponibilizarAreaCliente;
	}
	public void setDisponibilizarAreaCliente(Boolean disponibilizarAreaCliente) {
		this.disponibilizarAreaCliente = disponibilizarAreaCliente;
	}
	
	@Required
	@DisplayName("Qtde. de campos por linha no filtro")
	public Integer getQtdeCamposLinha() {
		return qtdeCamposLinha;
	}
	
	public void setQtdeCamposLinha(Integer qtdeCamposLinha) {
		this.qtdeCamposLinha = qtdeCamposLinha;
	}
	
	@DisplayName("Hist�rico")
	@OneToMany(mappedBy="geradorRelatorioBean")
	public List<GeradorRelatorioBeanHistorico> getListaGeradorRelatorioBeanHistorico() {
		return listaGeradorRelatorioBeanHistorico;
	}
	public void setListaGeradorRelatorioBeanHistorico(
			List<GeradorRelatorioBeanHistorico> listaGeradorRelatorioBeanHistorico) {
		this.listaGeradorRelatorioBeanHistorico = listaGeradorRelatorioBeanHistorico;
	}
}
