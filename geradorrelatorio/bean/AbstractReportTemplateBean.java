package br.com.linkcom.geradorrelatorio.bean;

import java.util.LinkedHashMap;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumTipoEmissao;
import br.com.linkcom.geradorrelatorio.util.pdf.WKHTMLConfigEnum;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.neo.validation.annotation.MaxLength;
import br.com.linkcom.neo.validation.annotation.Required;

//@Entity
//@Table(name="reporttemplate")
//@DisplayName(value="Cadastro de modelo de relat�rio")
@MappedSuperclass
public abstract class AbstractReportTemplateBean {
	
	protected Integer cdreporttemplate;
	protected String nome;
	protected String descricao;
	protected String leiaute;
	protected WKHTMLConfigEnum orientacao;
	protected Integer margemSuperior;
	protected Integer margemInferior;
	protected Integer margemEsquerda;
	protected Integer margemDireita;
	protected Integer largura;
	protected Integer altura;
	protected String cabecalho;
	protected String rodape;
	protected Boolean habilitarcabecalho;
	protected Boolean habilitarrodape;
	protected EnumCategoriaReportTemplate categoria;
	protected EnumTipoEmissao tipoEmissao;
	protected String arquivoleiaute;
	protected String arquivocabecalho;
	protected String arquivorodape;
	
	protected LinkedHashMap<String, Object> datasource = new LinkedHashMap<String, Object>();
	
	public AbstractReportTemplateBean() {
	}
	
	public AbstractReportTemplateBean(Integer cdreporttemplate) {
		this.cdreporttemplate = cdreporttemplate;
	}

	@Id
	public Integer getCdreporttemplate() {
		return cdreporttemplate;
	}
	public void setCdreporttemplate(Integer cdreporttemplate) {
		this.cdreporttemplate = cdreporttemplate;
	}


	@DescriptionProperty
	@MaxLength(30)
	@Required
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@MaxLength(200)
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Required
	public String getLeiaute() {
		return leiaute;
	}
	public void setLeiaute(String leiaute) {
		this.leiaute = leiaute;
	}

	@DisplayName("Orienta��o")
	public WKHTMLConfigEnum getOrientacao() {
		if(orientacao == null){
			orientacao = WKHTMLConfigEnum.ORIENTATION_PORTRAIT;
		}
		return orientacao;
	}
	public void setOrientacao(WKHTMLConfigEnum orientacao) {
		this.orientacao = orientacao;
	}
	
	@DisplayName("Margem Superior (mm)")
	public Integer getMargemSuperior() {
		return margemSuperior;
	}
	public void setMargemSuperior(Integer margemSuperior) {
		this.margemSuperior = margemSuperior;
	}
	
	@DisplayName("Margem Inferior (mm)")
	public Integer getMargemInferior() {
		return margemInferior;
	}
	public void setMargemInferior(Integer margemInferior) {
		this.margemInferior = margemInferior;
	}

	@DisplayName("Margem Esquerda (mm)")
	public Integer getMargemEsquerda() {
		return margemEsquerda;
	}
	public void setMargemEsquerda(Integer margemEsquerda) {
		this.margemEsquerda = margemEsquerda;
	}

	@DisplayName("Margem Direita (mm)")
	public Integer getMargemDireita() {
		return margemDireita;
	}
	public void setMargemDireita(Integer margemDireita) {
		this.margemDireita = margemDireita;
	}
	
	@DisplayName("Largura da p�gina (mm)")
	public Integer getLargura() {
		return largura;
	}
	public void setLargura(Integer largura) {
		this.largura = largura;
	}
	
	@DisplayName("Altura da p�gina (mm)")
	public Integer getAltura() {
		return altura;
	}
	public void setAltura(Integer altura) {
		this.altura = altura;
	}
	
	public String getCabecalho() {
		return cabecalho;
	}
	public void setCabecalho(String cabecalho) {
		this.cabecalho = cabecalho;
	}

	public String getRodape() {
		return rodape;
	}
	public void setRodape(String rodape) {
		this.rodape = rodape;
	}

	@Transient
	public boolean isHabilitarcabecalho() {
		return habilitarcabecalho;
	}
	public void setHabilitarcabecalho(boolean habilitarcabecalho) {
		this.habilitarcabecalho = habilitarcabecalho;
	}

	@Transient
	public boolean isHabilitarrodape() {
		return habilitarrodape;
	}
	public void setHabilitarrodape(boolean habilitarrodape) {
		this.habilitarrodape = habilitarrodape;
	}

	@Required
	@Enumerated(EnumType.STRING)
	@DisplayName("Categoria")
	public EnumCategoriaReportTemplate getCategoria() {
		return this.categoria;
	}
	
	@Enumerated(EnumType.STRING)
	@DisplayName("Tipo de Emiss�o")
	public EnumTipoEmissao getTipoEmissao() {
		return tipoEmissao;
	}
	
	public void setCategoria(EnumCategoriaReportTemplate categoria) {
		this.categoria = categoria;
	}

	public void setTipoEmissao(EnumTipoEmissao tipoEmissao) {
		this.tipoEmissao = tipoEmissao;
	}

	public String getArquivoleiaute() {
		return arquivoleiaute;
	}
	public void setArquivoleiaute(String arquivoleiaute) {
		this.arquivoleiaute = arquivoleiaute;
	}

	public String getArquivocabecalho() {
		return arquivocabecalho;
	}
	public void setArquivocabecalho(String arquivocabecalho) {
		this.arquivocabecalho = arquivocabecalho;
	}

	public String getArquivorodape() {
		return arquivorodape;
	}
	public void setArquivorodape(String arquivorodape) {
		this.arquivorodape = arquivorodape;
	}

	@Transient
	public LinkedHashMap<String, Object> getDatasource() {
		return datasource;
	}

	public void setDatasource(LinkedHashMap<String, Object> datasource) {
		this.datasource = datasource;
	}	
}
