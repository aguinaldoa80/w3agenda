package br.com.linkcom.geradorrelatorio.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;

@Entity
@Table(name="reporttemplate")
@DisplayName(value="Cadastro de modelo de relatório")
public class ReportTemplateBean extends AbstractReportTemplateBean implements Log {
	
	private String assunto;
	
	public ReportTemplateBean() {
	}
	
	public ReportTemplateBean(Integer cdreporttemplate) {
		super(cdreporttemplate);
	}
	
	@Override
	@Transient
	public Integer getCdusuarioaltera() {
		return null;
	}

	@Override
	@Transient
	public Timestamp getDtaltera() {
		return null;
	}

	@Override
	public void setCdusuarioaltera(Integer cdusuarioaltera) {
	}

	@Override
	public void setDtaltera(Timestamp dtaltera) {
	}
	
	@Override
	public boolean equals(Object obj) {
		try {
			return getCdreporttemplate().equals(((ReportTemplateBean) obj).getCdreporttemplate());
		} catch (Exception e) {
			return false;
		}
	}
	
	public String getAssunto() {
		return assunto;
	}
	
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
}
