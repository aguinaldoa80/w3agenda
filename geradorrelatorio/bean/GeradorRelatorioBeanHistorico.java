package br.com.linkcom.geradorrelatorio.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import br.com.linkcom.geradorrelatorio.bean.enumeration.GeradorRelatorioBeanHistoricoAcao;
import br.com.linkcom.neo.bean.annotation.DisplayName;
import br.com.linkcom.sined.util.Log;
import br.com.linkcom.sined.util.tag.TagFunctions;

@Entity
@SequenceGenerator(name = "sq_geradorrelatoriohistorico", sequenceName = "sq_geradorrelatoriohistorico")
public class GeradorRelatorioBeanHistorico implements Log{

	protected Integer cdGeradorRelatorioBeanHistorico;
	protected Integer cdusuarioaltera;
	protected Timestamp dtaltera;
	
	//Configura��es de visualiza��o
	protected Boolean gerarrelatorio;
	protected Boolean gerargrafico;
	protected Boolean gerarcsv;
	protected Boolean gerartxt;
	protected Boolean gerarpdf;
	protected Boolean gerarW3erp;
	protected Boolean visualizardados;
	protected Boolean exibirtotalizadores;
	protected Boolean disponibilizarAreaCliente;
	
	//Consulta
	protected String nome;
	protected String descricao;
	protected String observacaoconsulta;
	protected String grupoRelatorio;
	protected String query;
	
	//Relatorio
	protected String leiauterelatorio;
	
	//Cabe�alho
	protected String leiautecabecalho;
	
	//Rodap�
	protected String leiauterodape;
	
	//Grafico
	protected String eixox;
	protected String eixoy;
	protected String serie;
	
	//Totalizadores
	protected String totalizadores;

	//Permiss�es
	protected String permissoes;

	protected String observacao;
	protected GeradorRelatorioBeanHistoricoAcao acao;
	protected GeradorRelatorioBean geradorRelatorioBean;
	
	
	
	@Id
	@GeneratedValue(generator="sq_geradorrelatoriohistorico",strategy=GenerationType.AUTO)
	public Integer getCdGeradorRelatorioBeanHistorico() {
		return cdGeradorRelatorioBeanHistorico;
	}
	
	public void setCdGeradorRelatorioBeanHistorico(
			Integer cdGeradorRelatorioBeanHistorico) {
		this.cdGeradorRelatorioBeanHistorico = cdGeradorRelatorioBeanHistorico;
	}

	public Integer getCdusuarioaltera() {
		return cdusuarioaltera;
	}
	

	public void setCdusuarioaltera(Integer cdusuarioaltera) {
		this.cdusuarioaltera = cdusuarioaltera;
	}
	
	@DisplayName("Data de altera��o")
	public Timestamp getDtaltera() {
		return dtaltera;
	}
	
	public void setDtaltera(Timestamp dtaltera) {
		this.dtaltera = dtaltera;
	}
	
	// Configura��es de Visualiza��o
	
	public Boolean getGerarrelatorio() {
		return gerarrelatorio;
	}

	public void setGerarrelatorio(Boolean gerarrelatorio) {
		this.gerarrelatorio = gerarrelatorio;
	}

	public Boolean getGerargrafico() {
		return gerargrafico;
	}

	public void setGerargrafico(Boolean gerargrafico) {
		this.gerargrafico = gerargrafico;
	}

	public Boolean getGerarcsv() {
		return gerarcsv;
	}

	public void setGerarcsv(Boolean gerarcsv) {
		this.gerarcsv = gerarcsv;
	}

	public Boolean getGerartxt() {
		return gerartxt;
	}

	public void setGerartxt(Boolean gerartxt) {
		this.gerartxt = gerartxt;
	}

	public Boolean getGerarpdf() {
		return gerarpdf;
	}

	public void setGerarpdf(Boolean gerarpdf) {
		this.gerarpdf = gerarpdf;
	}

	public Boolean getGerarW3erp() {
		return gerarW3erp;
	}

	public void setGerarW3erp(Boolean gerarW3erp) {
		this.gerarW3erp = gerarW3erp;
	}

	public Boolean getVisualizardados() {
		return visualizardados;
	}

	public void setVisualizardados(Boolean visualizardados) {
		this.visualizardados = visualizardados;
	}

	public Boolean getExibirtotalizadores() {
		return exibirtotalizadores;
	}

	public void setExibirtotalizadores(Boolean exibirtotalizadores) {
		this.exibirtotalizadores = exibirtotalizadores;
	}
	
	@DisplayName("Disponibilizar na �rea do cliente")
	public Boolean getDisponibilizarAreaCliente() {
		return disponibilizarAreaCliente;
	}

	public void setDisponibilizarAreaCliente(Boolean disponibilizarAreaCliente) {
		this.disponibilizarAreaCliente = disponibilizarAreaCliente;
	}
	
	
	// Consulta
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacaoconsulta() {
		return observacaoconsulta;
	}

	public void setObservacaoconsulta(String observacaoconsulta) {
		this.observacaoconsulta = observacaoconsulta;
	}

	public String getGrupoRelatorio() {
		return grupoRelatorio;
	}

	public void setGrupoRelatorio(String grupoRelatorio) {
		this.grupoRelatorio = grupoRelatorio;
	}
	
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	
	// Relatorio
	
	public String getLeiauterelatorio() {
		return leiauterelatorio;
	}

	public void setLeiauterelatorio(String leiauterelatorio) {
		this.leiauterelatorio = leiauterelatorio;
	}
	
	//Cabe�alho
	
	public String getLeiautecabecalho() {
		return leiautecabecalho;
	}

	public void setLeiautecabecalho(String leiautecabecalho) {
		this.leiautecabecalho = leiautecabecalho;
	}
	
	//Rodap�
	
	public String getLeiauterodape() {
		return leiauterodape;
	}

	public void setLeiauterodape(String leiauterodape) {
		this.leiauterodape = leiauterodape;
	}
	
	//Grafico
	
	public String getEixox() {
		return eixox;
	}

	public void setEixox(String eixox) {
		this.eixox = eixox;
	}

	public String getEixoy() {
		return eixoy;
	}

	public void setEixoy(String eixoy) {
		this.eixoy = eixoy;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	//Totalizadores
	
	public String getTotalizadores() {
		return totalizadores;
	}

	public void setTotalizadores(String totalizadores) {
		this.totalizadores = totalizadores;
	}
	
	//Permiss�es
	
	public String getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(String permissoes) {
		this.permissoes = permissoes;
	}
	
	@DisplayName("A��o")
	public GeradorRelatorioBeanHistoricoAcao getAcao() {
		return acao;
	}

	public void setAcao(GeradorRelatorioBeanHistoricoAcao acao) {
		this.acao = acao;
	}
	
	@DisplayName("Observa��o")
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cdgeradorrelatorio")
	public GeradorRelatorioBean getGeradorRelatorioBean() {
		return geradorRelatorioBean;
	}

	public void setGeradorRelatorioBean(
			GeradorRelatorioBean geradorRelatorioBean) {
		this.geradorRelatorioBean = geradorRelatorioBean;
	}
	
	
	
	@Transient
	@DisplayName("Respons�vel")
	public String getResponsavel() {
		if(cdusuarioaltera != null)
			return TagFunctions.findUserByCd(cdusuarioaltera);
		return null;
	}
	
	
}
