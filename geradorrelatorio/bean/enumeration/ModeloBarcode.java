package br.com.linkcom.geradorrelatorio.bean.enumeration;


public enum ModeloBarcode {
	EAN8,
	EAN13,
	CODE39,
	CODE128,
	CODABAR,
	UPCE,
	UPCA,
	INT2OF5;
	
	public static ModeloBarcode existsModelo(String siglaModelo){
		ModeloBarcode[] modelos = ModeloBarcode.values();
		
		for(ModeloBarcode modelo: modelos){
			if(modelo.toString().equals(siglaModelo.toUpperCase())){
				return modelo;
			}
		}
		return null;
	}
}
