package br.com.linkcom.geradorrelatorio.bean.enumeration;

import br.com.linkcom.neo.bean.annotation.DescriptionProperty;

public enum GeradorRelatorioBeanHistoricoAcao {
	
	CRIADO					(0, "Criado"),
	ALTERADO				(1, "Alterado"),
	;
	
	private Integer value;
	private String nome;
	
	private GeradorRelatorioBeanHistoricoAcao(Integer value, String nome){
		this.value = value;
		this.nome = nome;
	}
	
	public Integer getValue() {
		return value;
	}
	
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		return nome;
	}
	
}

