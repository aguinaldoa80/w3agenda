package br.com.linkcom.geradorrelatorio.bean.enumeration;

import java.util.List;

import br.com.linkcom.neo.types.ListSet;
import br.com.linkcom.sined.geral.bean.Contratofaturalocacao;
import br.com.linkcom.sined.geral.bean.Oportunidadesituacao;
import br.com.linkcom.sined.geral.bean.Telefone;
import br.com.linkcom.sined.geral.service.rtf.bean.EmailTemplateBean;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeBean;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeCampoadicionalBean;
import br.com.linkcom.sined.geral.service.rtf.bean.OportunidadeMaterialBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioClienteBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioEmpresaBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioEnderecoBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioItemBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioMaterialBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioMaterialNumeroSerieBean;
import br.com.linkcom.sined.geral.service.rtf.bean.RomaneioPatrimonioBean;
import br.com.linkcom.sined.modulo.crm.controller.process.bean.EmailAceiteDigital;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.AgendaInteracaoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.AgendaInteracaoClienteBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.AgendaInteracaoResponsavelBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteCategoriaBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteContatoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteEnderecoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteReferenciaBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteRestricaoBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteTelefoneBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.EmitirClienteVendedorBean;
import br.com.linkcom.sined.modulo.crm.controller.report.bean.TelefoneBean;
import br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.InformacaoAdicionalProdutoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.CampoAdicionalBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DanfeBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DanfeItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.DescricaoDeServicoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirColetaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirColetaMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteTEFPagamentoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirComprovanteValeCompraBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirEtiquetaDestinatarioBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirEtiquetaDestinatarioRemetenteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirEtiquetaRemetenteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoClienteItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirExpedicaoEtiquetaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirFaturaLocacaoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmitirFaturaLocacaoMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ExpedicaoEtiquetaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.NotaFiscalEletronicaItemReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.NotaFiscalEletronicaReportBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemCompraBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemCompraItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemServicoCamposAdicionaisBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemServicoItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraClienteBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraContatoBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraEmpresaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraFornecedorBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.OrdemcompraMaterialBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.ValeCompraBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaItemBean;
import br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaPagamentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.DevedoresBean;
import br.com.linkcom.sined.modulo.financeiro.controller.process.bean.MailingDevedorBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ChequeMovimentacaoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeChequeDoc;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeDocumento;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeDocumentoBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeMovimentacao;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirCopiaChequeMovimentacaoDoc;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirDespesaViagemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirDespesaViagemItemBean;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.EmitirDespesaViagemProjetoBean;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.InformacaoContribuinteNfeBean;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.InformacaoContribuinteNfseBean;
import br.com.linkcom.sined.modulo.fiscal.controller.report.bean.NotaItemBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ContratoReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.EmitirProducaoordemReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.MaterialReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PedidovendaReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PedidovendamaterialReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.PneuReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ProducaoagendaReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ProducaoagendamaterialReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ProducaoagendamaterialitemReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ProducaoagendamaterialmateriaprimaReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.ProducaoordemmaterialReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.VendaReportBean;
import br.com.linkcom.sined.modulo.producao.controller.report.bean.VendamaterialReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraAtividadesReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraRecursosReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraReportBean;
import br.com.linkcom.sined.modulo.projeto.controller.report.bean.DiarioObraTipoAtividadesReportBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSAtividadeBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSHistoricoBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSItemBean;
import br.com.linkcom.sined.modulo.servicointerno.controller.report.bean.EmitirOSMaterialBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.CampoAdicionalReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ClienteReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ColetaInfoContribuinteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ColetaMaterialInfoContribuinteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.EmpresaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.EmpresaReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaClienteBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaEmpresaBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaPneuBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GarantiaReformaResultadoBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.GrupoTributacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.NaturezaOperacaoBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.PedidoVendaMaterialReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.PedidoVendaReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ProducaoAgendaMaterialReportBean;
import br.com.linkcom.sined.modulo.sistema.controller.report.bean.ProducaoPneuReportBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirAtestadoObitoBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirComprovanteOSAtividadeBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirComprovanteOSBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirComprovanteOSHistoricoBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirComprovanteOSItemBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirComprovanteOSMaterialBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirFichaAnimalOSAnamneseBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirFichaAnimalOSBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirFichaAnimalOSHistoricoBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirFichaAnimalOSPatologiaBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirFichaAnimalOSPesoBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirFichaAnimalOSReceituarioBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirFichaAnimalOSVacinaBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirReceituarioOSBean;
import br.com.linkcom.sined.modulo.veterinaria.controller.report.bean.EmitirTermoRetiradaBean;
import br.com.linkcom.sined.util.veterinaria.EnviaEmailProximaVacinacaoBean;



public enum EnumCategoriaReportTemplate {
	OPORTUNIDADE							("Oportunidade", 									Boolean.TRUE, 	OportunidadeBean.class, 
																												OportunidadeMaterialBean.class,
																												OportunidadeCampoadicionalBean.class,
																												Oportunidadesituacao.class,
																												TelefoneBean.class),
	COMPROVANTE_ORDEM_SERVICO_VETERINARIA	("Comprovante de Ordem de Servi�o (Veterin�ria)",  	Boolean.TRUE, 	EmitirComprovanteOSBean.class, 
																												EmitirComprovanteOSMaterialBean.class, 
																												EmitirComprovanteOSAtividadeBean.class, 
																												EmitirComprovanteOSHistoricoBean.class, 
																												EmitirComprovanteOSItemBean.class),
	EMITIR_ORDEM_SERVICO					("Emitir OS", 										Boolean.TRUE, 	EmitirOSBean.class, 
																												EmitirOSMaterialBean.class, 
																												EmitirOSAtividadeBean.class, 
																												EmitirOSHistoricoBean.class, 
																												EmitirOSItemBean.class),
	AGENDA_INTERACAO						("Agenda de Intera��o", 							Boolean.TRUE, 	AgendaInteracaoBean.class, 
																												AgendaInteracaoClienteBean.class, 
																												AgendaInteracaoResponsavelBean.class),
	CONTRATO_FATURA_LOCACAO_LISTAGEM		("Listagem de Fatura de Loca��o", 					Boolean.TRUE, 	Contratofaturalocacao.class),
	CONTRATO_FATURA_LOCACAO_EMISSAO			("Emiss�o de Fatura de Loca��o", 					Boolean.TRUE, 	EmitirFaturaLocacaoBean.class, 
																												EmitirFaturaLocacaoMaterialBean.class),
	EMITIR_EXPEDICAO 						("Emiss�o de Expedi��o",                            Boolean.TRUE, 	EmitirExpedicaoBean.class, 
																												EmitirExpedicaoClienteBean.class, 
																												EmitirExpedicaoClienteItemBean.class),
	EMITIR_DIARIO_OBRA						("Emiss�o de Di�rio de Obra",						Boolean.TRUE, 	DiarioObraReportBean.class, 
																												DiarioObraRecursosReportBean.class, 
																												DiarioObraTipoAtividadesReportBean.class, 
																												DiarioObraAtividadesReportBean.class),
	EMITIR_PRODUCAO_ORDEM					("Emiss�o de Ordem de Produ��o",					Boolean.TRUE, 	EmitirProducaoordemReportBean.class, 
																												ProducaoordemmaterialReportBean.class, 
																												ProducaoagendaReportBean.class, 
																												ProducaoagendamaterialReportBean.class,
																												ProducaoagendamaterialmateriaprimaReportBean.class,
																												ProducaoagendamaterialitemReportBean.class,
																												ContratoReportBean.class, 
																												VendaReportBean.class, 
																												PedidovendaReportBean.class, 
																												VendamaterialReportBean.class, 
																												PedidovendamaterialReportBean.class,
																												ProducaoagendamaterialmateriaprimaReportBean.MaterialReportBean.class,
																												ProducaoagendamaterialmateriaprimaReportBean.LoteestoqueReportBean.class,
																												PneuReportBean.class),	
	EMITIR_ORDEMCOMPRA						("Emiss�o de Ordem de Compra",						Boolean.TRUE, 	OrdemCompraBean.class, 
																												OrdemCompraItemBean.class, 
																												OrdemcompraMaterialBean.class, 
																												OrdemcompraContatoBean.class,
																												OrdemcompraFornecedorBean.class, 
																												OrdemcompraEmpresaBean.class, 
																												OrdemcompraClienteBean.class),
	EMITIR_FICHA_COLETA						("Emiss�o de Ficha de Coleta",						Boolean.TRUE, 	EmitirColetaBean.class, 
																												EmitirColetaMaterialBean.class,
																												PneuReportBean.class),
	EMITIR_FICHA_CLIENTE					("Emiss�o de Ficha de Cliente",						Boolean.TRUE,	EmitirClienteBean.class,
																												EmitirClienteTelefoneBean.class,
																												EmitirClienteContatoBean.class,
																												EmitirClienteEnderecoBean.class,
																												EmitirClienteRestricaoBean.class,
																												EmitirClienteCategoriaBean.class,
																												EmitirClienteVendedorBean.class,
																												EmitirClienteReferenciaBean.class),
    RECEITUARIO_VETERINARIA		("Receitu�rio de Ordem de Servi�o (Veterin�ria)",				Boolean.TRUE, 	EmitirReceituarioOSBean.class),
    FICHA_ANIMAL_VETERINARIA	("Emitir Ficha do Animal (Veterin�ria)",						Boolean.TRUE, 	EmitirFichaAnimalOSBean.class, 
    																											EmitirFichaAnimalOSHistoricoBean.class, 
    																											EmitirFichaAnimalOSPesoBean.class, 
    																											EmitirFichaAnimalOSPatologiaBean.class, 
    																											EmitirFichaAnimalOSVacinaBean.class,
    																											EmitirFichaAnimalOSReceituarioBean.class,
    																											EmitirFichaAnimalOSAnamneseBean.class),
    DANFE                       ("Emiss�o de Danfe",											Boolean.TRUE,	DanfeBean.class,
    																											DanfeItemBean.class),
	ETIQUETA_MATERIAL 			("Emiss�o de Etiqueta de Material",                             Boolean.TRUE,   MaterialReportBean.class),
	CHEQUE_MOVIMENTACAO 		("Emiss�o de Cheque - Movimenta��o Financeira (Contas a pagar)",Boolean.TRUE,   ChequeMovimentacaoBean.class),
	EMITIR_FICHA_DESPESA_VIAGEM ("Emiss�o de Ficha de Despesa de Viagem",                       Boolean.TRUE,   EmitirDespesaViagemBean.class,
	                                                                                                     		EmitirDespesaViagemItemBean.class,
		                                                                                                    	EmitirDespesaViagemProjetoBean.class),
    DISCRIMINACAO_DOS_SERVICOS	("NFSe - Discrimina��o dos servi�os",							Boolean.TRUE,   DescricaoDeServicoBean.class,
    																											OrdemServicoCamposAdicionaisBean.class,
    																											OrdemServicoItemBean.class,
    																											VendaBean.class,
    																											VendaItemBean.class,
    																											VendaPagamentoBean.class),
	EMAIL_VET_VACINACAO			("E-mail de Vacina��o (Veterin�ria)",									Boolean.TRUE, 	EnviaEmailProximaVacinacaoBean.class),
	TERMO_RETIRADA_VETERINARIA		("Termo de retirada (Veterin�ria)",							Boolean.TRUE, 	EmitirTermoRetiradaBean.class),
	ATESTADO_OBITO_VETERINARIA		("Atestado de �bito (Veterin�ria)",							Boolean.TRUE, 	EmitirAtestadoObitoBean.class),
	INFORMACOES_AO_CONTRIBUINTE_NFSE ("NFSe - Informa��es ao contribuinte", 					Boolean.TRUE, InformacaoContribuinteNfseBean.class,
																											  br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaInfoContribuinteBean.class,
																											  EmpresaBean.class,
																											  ClienteBean.class,
																											  GrupoTributacaoBean.class,
																											  NaturezaOperacaoBean.class,
																											  CampoAdicionalBean.class,
																											  VendamaterialReportBean.class,
																											  MaterialReportBean.class),
	INFORMACOES_AO_CONTRIBUINTE_NFE ("NFe - Informa��es ao contribuinte", 						Boolean.TRUE, InformacaoContribuinteNfeBean.class,
																											  br.com.linkcom.sined.modulo.faturamento.controller.report.bean.VendaInfoContribuinteBean.class,
																											  ColetaInfoContribuinteBean.class,
																											  ColetaMaterialInfoContribuinteBean.class,
																											  EmpresaBean.class,
																											  ClienteBean.class,
																											  PneuReportBean.class,
																											  GrupoTributacaoBean.class,
																											  NaturezaOperacaoBean.class,
																											  CampoAdicionalBean.class,
																											  VendamaterialReportBean.class,
																											  MaterialReportBean.class,
																											  NotaItemBean.class),
	EMITIR_COMPROVANTE_VALE_COMPRA("Vale compra", Boolean.TRUE, EmitirComprovanteValeCompraBean.class,
												  				ValeCompraBean.class,
												  				br.com.linkcom.sined.modulo.faturamento.controller.report.bean.EmpresaBean.class),
												  				
	ETIQUETA_DESTINATARIO_REMETENTE ("Emiss�o de Etiqueta de destinat�rio/remetente da venda", Boolean.TRUE, EmitirEtiquetaDestinatarioRemetenteBean.class,
																									EmitirEtiquetaDestinatarioBean.class,
												  													EmitirEtiquetaRemetenteBean.class),
												  													
	MAILING_DEVEDOR_LISTAGEM("Listagem de Cobran�as a Devedores", Boolean.TRUE, DevedoresBean.class, MailingDevedorBean.class),
	EMITIR_COPIA_CHEQUE_DOCUMENTO("Emiss�o de c�pia de cheque documento", Boolean.TRUE, EmitirCopiaChequeChequeDoc.class,
																						EmitirCopiaChequeDocumentoBean.class,
																						EmitirCopiaChequeMovimentacaoDoc.class),
	EMITIR_COPIA_CHEQUE_MOVIMENTACAO_CHEQUE("Emiss�o de c�pia de cheque movimenta��o/cheque", Boolean.TRUE, EmitirCopiaChequeBean.class,
																											EmitirCopiaChequeDocumento.class,
																											EmitirCopiaChequeMovimentacao.class),
	EMITIR_NFSE("Emiss�o de NFS-e", Boolean.TRUE, NotaFiscalEletronicaReportBean.class,
													NotaFiscalEletronicaItemReportBean.class),
													
	INFO_ADICIONAIS_PRODUTO("Informa��es adicionais do produto ", Boolean.TRUE, InformacaoAdicionalProdutoBean.class,
																				br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.MaterialReportBean.class,
																				PneuReportBean.class,
																				br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.VendamaterialReportBean.class,
																				br.com.linkcom.sined.modulo.faturamento.controller.crud.bean.PedidovendamaterialReportBean.class),
	ROMANEIO                                ("Romaneio",                                        Boolean.TRUE,   RomaneioBean.class,
																								RomaneioEmpresaBean.class,
																								RomaneioClienteBean.class,
																								RomaneioEnderecoBean.class,
																								RomaneioItemBean.class,
																								RomaneioMaterialBean.class,
																								RomaneioPatrimonioBean.class,
																								RomaneioMaterialNumeroSerieBean.class),
																								
	GARANTIA_DE_REFORMA("Garantia de Reforma", Boolean.TRUE, GarantiaReformaBean.class,
			GarantiaReformaEmpresaBean.class,
			GarantiaReformaClienteBean.class,
			GarantiaReformaPneuBean.class,
			GarantiaReformaResultadoBean.class),
			
	EMAIL_ACEITE_DIGITAL("E-mail de aceite digital de documento", Boolean.TRUE, EmailAceiteDigital.class),
	
	EMAIL_BOLETO ("E-mail de envio de boleto", Boolean.TRUE, EmailTemplateBean.class),
	
	ETIQUETA_DO_PNEU ("Etiqueta do Pneu Produzido/Recusado",Boolean.TRUE, PneuReportBean.class,
			ProducaoPneuReportBean.class,
			EmpresaReportBean.class,
			ClienteReportBean.class,
			PedidoVendaReportBean.class,
			ProducaoAgendaMaterialReportBean.class,
			CampoAdicionalReportBean.class,
			PedidoVendaMaterialReportBean.class),
			
	ETIQUETA_DO_PNEU_MATRICIAL ("Etiqueta do Pneu Produzido/Recusado (Matricial)",Boolean.TRUE, PneuReportBean.class,
					ProducaoPneuReportBean.class,
					EmpresaReportBean.class,
					ClienteReportBean.class,
					PedidoVendaReportBean.class,
					ProducaoAgendaMaterialReportBean.class,
					CampoAdicionalReportBean.class,
					PedidoVendaMaterialReportBean.class),
	
	COMPROVANTE_VENDA_TEF ("Comprovante de venda da TEF", Boolean.TRUE, EmitirComprovanteTEFBean.class, EmitirComprovanteTEFItemBean.class, EmitirComprovanteTEFPagamentoBean.class),
	COMPROVANTE_NFE_TEF ("Comprovante de NF-e da TEF", Boolean.TRUE, EmitirComprovanteTEFBean.class, EmitirComprovanteTEFItemBean.class, EmitirComprovanteTEFPagamentoBean.class),
	COMPROVANTE_NFCE_TEF ("Comprovante de NFC-e da TEF", Boolean.TRUE, EmitirComprovanteTEFBean.class, EmitirComprovanteTEFItemBean.class, EmitirComprovanteTEFPagamentoBean.class),
	
	ETIQUETA_EPEDICAO ("Emiss�o de Etiqueta para Expedi��o", Boolean.TRUE,  EmitirExpedicaoEtiquetaBean.class),

	;
	
	
	private String nome;
	private Class<?>[] classesauxiliares;
	private Boolean showcategoria;
		
	private EnumCategoriaReportTemplate(String nome, Boolean showcategoria, Class<?>... classesauxiliares) {
		this.nome = nome;
		this.showcategoria = showcategoria;
		this.classesauxiliares = classesauxiliares;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Class<?>[] getClassesauxiliares() {
		return classesauxiliares;
	}
	public void setClassesauxiliares(Class<?>[] classesauxiliares) {
		this.classesauxiliares = classesauxiliares;
	}

	public Boolean getShowcategoria() {
		return showcategoria;
	}
	public void setShowcategoria(Boolean showcategoria) {
		this.showcategoria = showcategoria;
	}

	public static List<EnumCategoriaReportTemplate> getCategoriasVisiveis(){
		 List<EnumCategoriaReportTemplate> lista = new ListSet<EnumCategoriaReportTemplate>(EnumCategoriaReportTemplate.class);
		 for(EnumCategoriaReportTemplate categoria : EnumCategoriaReportTemplate.values()){
			if(categoria.getShowcategoria()){
				lista.add(categoria);
			}
		 }
		 return lista;
	}

	@Override
	public String toString() {
		return getNome();
	}	
	
	
}
