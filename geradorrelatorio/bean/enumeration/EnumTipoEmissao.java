package br.com.linkcom.geradorrelatorio.bean.enumeration;


public enum EnumTipoEmissao {
	
	PDF 	("PDF"),
	W3ERP	("W3ERP"),
	TXT		("TXT");
	
	private String nome;
		
	private EnumTipoEmissao(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return getNome();
	}	
}
