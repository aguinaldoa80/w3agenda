package br.com.linkcom.geradorrelatorio.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.GeradorWSBean;
import br.com.linkcom.geradorrelatorio.crud.filtro.GeradorWSFiltro;
import br.com.linkcom.geradorrelatorio.service.WSService;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.WebRequestContext;

@Controller(path="/pub/WS")
public class WSProcess extends GeradorWSAbstractProcess<GeradorWSFiltro, GeradorWSBean, WSService> {

	@Override
	public ModelAndView doFiltro(WebRequestContext request,
			GeradorWSFiltro filtro) throws Exception {
		return null;
	}

	@Override
	public Resource generateResource(WebRequestContext request,
			GeradorWSFiltro filtro) throws Exception {
		return null;
	}
}
