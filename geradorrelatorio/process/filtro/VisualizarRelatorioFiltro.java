package br.com.linkcom.geradorrelatorio.process.filtro;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.bean.GrupoRelatorio;
import br.com.linkcom.geradorrelatorio.bean.transiente.CampoGeneric;
import br.com.linkcom.neo.bean.annotation.DisplayName;

public class VisualizarRelatorioFiltro extends GenericGeradorFiltroListagem<GeradorRelatorioBean> {

	private Boolean csv;
	private List<CampoGeneric> listaCampos;
	private GrupoRelatorio grupoRelatorio;
	private GeradorRelatorioBean geradorRelatorioBean;
	private Boolean exibirtotais;
	private String descricao;
	public List<CampoGeneric> getListaCampos() {
		return listaCampos;
	}
	
	@DisplayName("Formato")
	public Boolean getCsv() {
		return csv;
	}
	
	
	public void setCsv(Boolean csv) {
		this.csv = csv;
	}

	public void setListaCampos(List<CampoGeneric> listaCampos) {
		this.listaCampos = listaCampos;
	}

	public GrupoRelatorio getGrupoRelatorio() {
		return grupoRelatorio;
	}

	public void setGrupoRelatorio(GrupoRelatorio grupoRelatorio) {
		this.grupoRelatorio = grupoRelatorio;
	}

	public GeradorRelatorioBean getGeradorRelatorioBean() {
		return geradorRelatorioBean;
	}

	public void setGeradorRelatorioBean(GeradorRelatorioBean geradorRelatorioBean) {
		this.geradorRelatorioBean = geradorRelatorioBean;
		setGeradorEntity(geradorRelatorioBean);
	}
	
	@DisplayName("Exibir Totais")
	public Boolean getExibirtotais() {
		return exibirtotais;
	}

	public void setExibirtotais(Boolean exibirtotais) {
		this.exibirtotais = exibirtotais;
	}	
	
	@DisplayName("Descri��o do Relat�rio")
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
