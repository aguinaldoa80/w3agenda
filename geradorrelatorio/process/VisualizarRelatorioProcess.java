package br.com.linkcom.geradorrelatorio.process;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.bean.transiente.CampoGeneric;
import br.com.linkcom.geradorrelatorio.process.filtro.VisualizarRelatorioFiltro;
import br.com.linkcom.geradorrelatorio.service.GeradorRelatorioEntityService;
import br.com.linkcom.geradorrelatorio.service.GraficoService;
import br.com.linkcom.geradorrelatorio.service.RelatorioService;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.types.GenericBean;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.pub.controller.process.IntegracaoWebserviceProcess;
import br.com.linkcom.sined.util.SinedUtil;

@Controller(path={
		"/sistema/process/VisualizarRelatorio",
		"/financeiro/process/VisualizarRelatorio",
		"/rh/process/VisualizarRelatorio",
		"/projeto/process/VisualizarRelatorio",
		"/juridico/process/VisualizarRelatorio",
		"/crm/process/VisualizarRelatorio",
		"/faturamento/process/VisualizarRelatorio",
		"/servicointerno/process/VisualizarRelatorio",
		"/suprimento/process/VisualizarRelatorio",
		"/veiculo/process/VisualizarRelatorio",
		"/fiscal/process/VisualizarRelatorio",
		"/producao/process/VisualizarRelatorio",
		"/contabil/process/VisualizarRelatorio"
}, authorizationModule=ProcessAuthorizationModule.class)
public class VisualizarRelatorioProcess extends VisualizarRelatorioAbstractProcess<VisualizarRelatorioFiltro, GeradorRelatorioBean, RelatorioService, GraficoService> {

	@Override
	public ModelAndView doGerar(WebRequestContext request, VisualizarRelatorioFiltro filtro) throws Exception {
		String modulo = (String) NeoWeb.getRequestContext().getAttribute("NEO_MODULO");
		if(modulo.equals("pub")){
			String tokenValidacao = ParametrogeralService.getInstance().getValorPorNome(IntegracaoWebserviceProcess.PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
			String hash = request.getParameter("hash");
			if(!hash.equals(tokenValidacao)){
				throw new Exception("Autenticação inválida.");
			}
		}
		
		return super.doGerar(request, filtro);
	}
	
	@Override
	protected String getExtensaoTxt(VisualizarRelatorioFiltro filtro) {
		if(filtro.getGeradorEntity()!=null && filtro.getGeradorEntity().getGerarW3erp()){
			return ".w3erp";
		}
		return ".txt";
	}
	
	@SuppressWarnings("unchecked")
	private void persisteDadosSessao(WebRequestContext request, Integer cdrelatorio, List<CampoGeneric> listaCampos){
		if(listaCampos != null && !listaCampos.isEmpty()){
			Map<Integer, Map<String, String>> mapaSessao = (request.getSession().getAttribute(GeradorRelatorioEntityService.MAPA_CAMPOS_GERADOR_RELATORIO) != null
					? (Map<Integer, Map<String, String>>) request.getSession().getAttribute(GeradorRelatorioEntityService.MAPA_CAMPOS_GERADOR_RELATORIO)
					: new HashMap<Integer, Map<String,String>>());
			Map<String, String> mapaCampos = new HashMap<String, String>();
			
			for(CampoGeneric campo : listaCampos){
				if(campo.persisteDadosSessao() && campo.getListaParametros()!= null && !campo.getListaParametros().isEmpty()){
					 Iterator<GenericBean> it = campo.getListaParametros().iterator();
					 while (it.hasNext()) {
						GenericBean param = (GenericBean) it.next();
						if(param.getId() != null && param.getValue() != null){
							if(param.getId().toString().equalsIgnoreCase("query")){
								mapaCampos.put(campo.getNome(), param.getValue().toString());
								it.remove();
							} else if(param.getId().toString().equalsIgnoreCase("queryArgs")){
								mapaCampos.put(campo.getNome()+"QueryArgs", param.getValue().toString());
							}
						}
					}
				}
			}
			mapaSessao.put(cdrelatorio, mapaCampos);
			request.getSession().setAttribute(GeradorRelatorioEntityService.MAPA_CAMPOS_GERADOR_RELATORIO, mapaSessao);
		}
	}
	
	
	@Action("loadRelatorio")
	public ModelAndView loadRelatorio(WebRequestContext request, VisualizarRelatorioFiltro filtro){
		ModelAndView view = new JsonModelAndView();
		Integer cdUsarioLogado = SinedUtil.getCdUsuarioLogado();
		request.setAttribute("cdUsuarioLogado", cdUsarioLogado);
		try {
			Map<String, Object> modelMap = new HashMap<String, Object>();
			
			SinedUtil.markAsReader();
			br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean bean = service.loadForEntrada(filtro.getGeradorEntity());
			
			List<CampoGeneric> listaCampos = service.getCampos(bean.getQuery());
			persisteDadosSessao(request, bean.getCdgeradorrelatorio(), listaCampos);
			filtro.setListaCampos(listaCampos);
			
			modelMap.put("listaCampos", filtro.getListaCampos());
			modelMap.put("gerarRelatorio", bean.getGerarrelatorio());
			modelMap.put("gerarGrafico", bean.getGerargrafico());
			modelMap.put("visualizarDados", bean.getVisualizardados());
			modelMap.put("gerarPdf", bean.getGerarpdf());
			modelMap.put("gerarHtml", bean.getGerarhtml());
			modelMap.put("gerarCsv", bean.getGerarcsv());
			modelMap.put("gerarTxt", bean.getGerartxt());
			modelMap.put("descricao", bean.getDescricao());
			modelMap.put("qtdeCamposLinha", bean.getQtdeCamposLinha());
			modelMap.put("cdUsuarioLogado", request.getAttribute("cdUsuarioLogado"));
			
			view.addAllObjects(modelMap);
		
		} catch (Exception e) {
			view.addObject("error", e.getMessage());
			e.printStackTrace();
		}
		return view;
	}
}
