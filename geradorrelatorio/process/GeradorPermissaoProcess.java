package br.com.linkcom.geradorrelatorio.process;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.GeradorPermissaoBean;
import br.com.linkcom.geradorrelatorio.process.filtro.VisualizarPermissaoFiltro;
import br.com.linkcom.geradorrelatorio.service.GeradorPermissaoService;
import br.com.linkcom.neo.authorization.process.ProcessAuthorizationModule;
import br.com.linkcom.neo.controller.Controller;
import br.com.linkcom.neo.core.web.WebRequestContext;

@Controller(path="/sistema/process/GeradorPermissao", authorizationModule=ProcessAuthorizationModule.class)
public class GeradorPermissaoProcess extends GeradorPermissaoAbstractProcess<VisualizarPermissaoFiltro, GeradorPermissaoBean, GeradorPermissaoService>{
	
	@Override
	public ModelAndView doFilter(WebRequestContext requestContext, VisualizarPermissaoFiltro filtro) throws Exception {
		ModelAndView modelAndView = super.doFilter(requestContext, filtro);
		requestContext.setAttribute("listaPapel", geradorPapelViewService.findAll());
		return modelAndView;
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext requestContext, VisualizarPermissaoFiltro filtro) {
		ModelAndView modelAndView = super.doSalvar(requestContext, filtro);
		requestContext.addMessage("Registros atualizados com sucesso!");
		return modelAndView;
	}
}