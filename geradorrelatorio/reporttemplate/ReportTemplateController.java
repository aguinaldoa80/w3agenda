package br.com.linkcom.geradorrelatorio.reporttemplate;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.reporttemplate.filtro.ReportTemplateFiltro;
import br.com.linkcom.geradorrelatorio.service.ReportTemplateService;
import br.com.linkcom.geradorrelatorio.util.ReportTemplateUtil;
import br.com.linkcom.neo.controller.resource.Resource;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Empresa;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ArquivoService;
import br.com.linkcom.sined.geral.service.CentrocustoService;
import br.com.linkcom.sined.geral.service.ClienteService;
import br.com.linkcom.sined.geral.service.EmpresaService;
import br.com.linkcom.sined.geral.service.FornecedorService;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.geral.service.ProjetoService;
import br.com.linkcom.sined.modulo.financeiro.controller.report.bean.ItemDetalhe;
import br.com.linkcom.sined.util.SinedException;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.neo.ReportFileServlet;

public abstract class ReportTemplateController<FILTRO extends ReportTemplateFiltro> extends AbstractReportTemplateController<ReportTemplateBean,FILTRO> {
	protected ReportTemplateService reportTemplateService;
	protected ArquivoService arquivoService;
	protected EmpresaService empresaService;
	protected CentrocustoService centrocustoService;
	protected ProjetoService projetoService;
	protected ClienteService clienteService;
	protected FornecedorService fornecedorService;
	protected ParametrogeralService parametrogeralService;

		
	public ReportTemplateService getReportTemplateService() {
		if(reportTemplateService == null)
			reportTemplateService = Neo.getObject(ReportTemplateService.class);
		
		return reportTemplateService;
	}
	public final ArquivoService getArquivoService() {
		return arquivoService;
	}
	public final EmpresaService getEmpresaService() {
		return empresaService;
	}
	public final void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	public final void setEmpresaService(EmpresaService empresaService) {
		this.empresaService = empresaService;
	}
	public final CentrocustoService getCentrocustoService() {
		return centrocustoService;
	}
	public final void setCentrocustoService(CentrocustoService centrocustoService) {
		this.centrocustoService = centrocustoService;
	}
	public final ProjetoService getProjetoService() {
		return projetoService;
	}
	public final void setProjetoService(ProjetoService projetoService) {
		this.projetoService = projetoService;
	}
	public final ClienteService getClienteService() {
		return clienteService;
	}
	public final void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
	}
	public final FornecedorService getFornecedorService() {
		return fornecedorService;
	}
	public final void setFornecedorService(FornecedorService fornecedorService) {
		this.fornecedorService = fornecedorService;
	}
	public final void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}

	public static enum SISTEMA{
		logo, empresa, usuario, data, centroCusto, projeto, cliente, fornecedor, empresaFiltro
	}
	
	@Override
	public ModelAndView doGerar(WebRequestContext request, FILTRO filtro) throws Exception {
		if(request.getSession().getAttribute(Parametrogeral.GERAR_RELATORIO_PDF) == null){
			String gerarRelatorioPdf = parametrogeralService.buscaValorPorNome(Parametrogeral.GERAR_RELATORIO_PDF);
			if(gerarRelatorioPdf == null || "true".equalsIgnoreCase(gerarRelatorioPdf.trim()))
				request.getSession().setAttribute(Parametrogeral.GERAR_RELATORIO_PDF, true);
			else
				request.getSession().setAttribute(Parametrogeral.GERAR_RELATORIO_PDF, false);
		}
		
		if("true".equalsIgnoreCase(request.getParameter("rawTemplate"))) {
			ReportTemplateBean template = getTemplate(request, filtro, Boolean.FALSE);
		    String rawString = criarUrlsPublicas(getRelatorioCompilado(request, filtro, template));
			View.getCurrent().print(rawString);
			return null;
		} else if((Boolean)request.getSession().getAttribute(Parametrogeral.GERAR_RELATORIO_PDF)){
			ModelAndView modelAndView = super.doGerar(request, filtro);
			if(modelAndView == null){
				request.getSession().removeAttribute("FILTRO_"+filtro.getClass().getName().toUpperCase());
			}
			return modelAndView;
		} else {
			Resource recurso = getPdfResource(request, filtro);
			
			String key = new BigInteger(130, new SecureRandom()).toString(32);
			ReportFileServlet.add(key, recurso);
			
			request.setAttribute("reportFileURL", SinedUtil.getUrlWithContext()+"/REPORTFILE/"+key+"."+recurso.getContentType().split("/")[1]);
			request.setAttribute("reportFileURLIframe", SinedUtil.getUrlWithContext()+"/REPORTFILE/"+key+"."+recurso.getContentType().split("/")[1]);
			request.setAttribute("isHttps", SinedUtil.getUrlWithContext().indexOf("https") != -1);
			
			return new ModelAndView("/relatorio/relatorioPreview");	
		}
	}
	
	@Override
	protected List<ReportTemplateBean> gerarListaTemplatesDisponiveis(WebRequestContext request, FILTRO filtro) throws Exception {
			EnumCategoriaReportTemplate categoria = setCategoriaReportTemplate();
			List<ReportTemplateBean> templates = getReportTemplateService().loadTemplatesByCategoria(categoria);
			request.setAttribute("listaTemplates", templates);
			return templates;
	}
	
	@Override
	protected final ReportTemplateBean getTemplate(WebRequestContext request, FILTRO filtro, Boolean isTemplateLoaded) throws Exception {
		if(isTemplateLoaded == null || !isTemplateLoaded){
			ReportTemplateBean template = setTemplate(request, filtro);
			if(template == null){
				template = getReportTemplateService().load(filtro.getTemplate());			
				filtro.setTemplate(template);
			} else {
				filtro.setTemplate(template);
			}
			
			if(filtro.getTemplate() == null) throw new SinedException("N�o existe nenhum template para esta categoria de relat�rio.");
			
			if(filtro.getTemplate().getArquivoleiaute() != null && !filtro.getTemplate().getArquivoleiaute().trim().isEmpty()){
				filtro.getTemplate().setLeiaute(ReportTemplateUtil.readTemplateFromFile(filtro.getTemplate().getArquivoleiaute()));
				if(filtro.getTemplate().getArquivocabecalho() != null && !filtro.getTemplate().getArquivocabecalho().trim().isEmpty()){
					filtro.getTemplate().setCabecalho(ReportTemplateUtil.readTemplateFromFile(filtro.getTemplate().getArquivocabecalho()));
				}
				if(filtro.getTemplate().getArquivorodape() != null && !filtro.getTemplate().getArquivorodape().trim().isEmpty()){
					filtro.getTemplate().setRodape(ReportTemplateUtil.readTemplateFromFile(filtro.getTemplate().getArquivorodape()));
				}
			}
		}
		return filtro.getTemplate();
	}
	
	@Override
	@SuppressWarnings({ "unchecked" })
	protected final Map<String, Object> configuraMapaParametrosSistema(FILTRO filtro) {
		LinkedHashMap<String, Object> mapaParametros = new LinkedHashMap<String, Object>();
		Empresa empresa = this.getEmpresa(filtro);
		
		mapaParametros.put(SISTEMA.logo.name(), SinedUtil.getLogoURLForReport(empresa));
		mapaParametros.put(SISTEMA.empresa.name(), empresa == null ? null : empresa.getRazaosocialOuNome());
		mapaParametros.put(SISTEMA.usuario.name(), Util.strings.toStringDescription(Neo.getUser()));
		mapaParametros.put(SISTEMA.data.name(), new Date(System.currentTimeMillis()));
		
		try {
			mapaParametros.put(SISTEMA.centroCusto.name(), centrocustoService.getNomeCentroCustos((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaCentrocusto").invoke(filtro)));
		} catch (Exception e) {}

		try {
			mapaParametros.put(SISTEMA.projeto.name(), projetoService.getNomeProjetos((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaProjeto").invoke(filtro)));
		} catch (Exception e) {}
		try {
			mapaParametros.put(SISTEMA.cliente.name(), clienteService.getNomeClientes((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaCliente").invoke(filtro)));
		} catch (Exception e) {}

		try {
			mapaParametros.put(SISTEMA.fornecedor.name(), fornecedorService.getNomeFornecedores((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaFornecedor").invoke(filtro)));
		} catch (Exception e) {}

		try {
			mapaParametros.put(SISTEMA.empresaFiltro.name(), empresaService.getNomeEmpresas((List<ItemDetalhe>)filtro.getClass().getDeclaredMethod("getListaEmpresa").invoke(filtro)));
		} catch (Exception e) {}
		
		
		Map<String, Object> parametrosAdicionais = setParametrosSistemaAdicionais(filtro);
		if(parametrosAdicionais != null && parametrosAdicionais.size() > 0){
			mapaParametros.putAll(parametrosAdicionais);
		}
		
		return mapaParametros;
	}
	
	/**
	 * Tenta encontrar um atributo "empresa" no filtro. Se for encontrado, pega o valor
	 * deste atributo. Caso n�o encontre, � feito um load na empresa marcada como
	 * principal no cadastro de empresas.
	 * *M�todo extra�do do SinedReport*
	 * 
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadComArquivo(Empresa)
	 * @see br.com.linkcom.sined.geral.service.EmpresaService#loadPrincipal()
	 * @param filtro
	 * @return
	 */
	protected final Empresa getEmpresa(FILTRO filtro) {
		Empresa empresa = null;

		try {
			empresa = (Empresa) filtro.getClass().getDeclaredMethod("getEmpresa").invoke(filtro);
			empresa = empresaService.loadComArquivo(empresa);
		} catch (Exception e) {
			empresa = empresaService.loadPrincipal();
		}

		return empresa;
	}
		
	@SuppressWarnings("unchecked")
	@Override
	protected final W3ReportTemplateFunctions getReportFunctions() {
		return new W3ReportTemplateFunctions();
	}
	
	@Override
	protected LinkedHashMap<String , Object> carregaDados(WebRequestContext request, FILTRO filtro) throws Exception{ return new LinkedHashMap<String, Object>(); }
	
	@Override
	protected LinkedList<ReportTemplateBean> carregaListaDados(WebRequestContext request, FILTRO filtro) throws Exception{ return null; }
	
	protected Map<String, Object> setParametrosSistemaAdicionais(FILTRO filtro){return null;};
	protected ReportTemplateBean setTemplate(WebRequestContext request, FILTRO filtro){return null;};
	protected abstract EnumCategoriaReportTemplate setCategoriaReportTemplate();
	
	@Override
	protected String criarUrlsPublicas(String relatorioHTML) {

		return SinedUtil.criarUrlsPublicas(relatorioHTML);
	}
}
