package br.com.linkcom.geradorrelatorio.reporttemplate.filtro;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.neo.validation.annotation.Required;

public class ReportTemplateFiltro extends AbstractReportTemplateFiltro {
	protected ReportTemplateBean template;

	@Required
	public ReportTemplateBean getTemplate() {
		return template;
	}

	public void setTemplate(ReportTemplateBean template) {
		this.template = template;
	}

}
