package br.com.linkcom.geradorrelatorio.reporttemplate;

import java.awt.Color;
import java.awt.Image;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import br.com.linkcom.geradorrelatorio.bean.enumeration.ModeloBarcode;
import br.com.linkcom.geradorrelatorio.functions.StringFunctions;
import br.com.linkcom.geradorrelatorio.util.ReportTemplateFunctions;
import br.com.linkcom.lkbanco.boleto.BarCode2of5;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.money.Extenso;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.utils.StringUtils;

import com.lowagie.text.pdf.Barcode;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.Barcode39;
import com.lowagie.text.pdf.BarcodeCodabar;
import com.lowagie.text.pdf.BarcodeEAN;

public class W3ReportTemplateFunctions extends ReportTemplateFunctions {
	
	public static String setCssPadrao(){
		//TODO Verificar a padroniza��o do css.
		StringBuilder css = new StringBuilder();
		css.append("<style>")
		
		   	.append("#cabecalho, #canhoto, #corpo,#tabela #detalhe, #rodape{")
		   	.append("	font-family: Arial;")
			.append("	font-size:12px;")
			.append("	padding-left:10px;")
			.append("	margin: 0;")
			.append("	padding-top:0;")
			.append("	padding-bottom:0;")
			.append("}")
			
			.append("#borda{")
			.append("	border:1px solid #000;")
			.append("}")
			
			.append("#cabecalho, #assinatura{")
			.append("      text-align:left;")
			.append("}")
			
			.append("#cabecalho{")
			.append("	border:1px solid #000;")
			.append("	font-size: 14px;")
			.append("	height:100px;")
			.append("}")
						      
			.append("#corpo{")
			.append("	line-height: 130%;")
			.append("	border:1px solid #000;")
			.append("	font-size:12px;")
			.append("	padding:10px;")
			.append("}")
			
			.append("#tabela{")
			.append("	padding:0px;")
			.append("	border-collapse:collapse;")
			.append("	word-wrap: break-word;")
			.append("	width: 100%;")
			.append("}")
			
			.append("#tabela table{")
			.append("	width:100%;")
			.append("	padding:0px;")
			.append("	table-layout:fixed;")
			.append("}")
			
			.append("#tablea th{")
			.append("	padding: 3px;")
			.append("	font-weight: bold;")
			.append("	text-align: center;")
			.append("	font-size: 14px;")
			.append("}")
			
			.append("#tabela tr{")
			.append("	padding: 3px;")
			.append("	text-align: center;")
			.append("}")
			
			.append("#tabela td{")
			.append("	font-size:12px;")
			.append("	padding: 3px;")
			.append("	border-collapse:collapse;")
			.append("	border-top: 1px solid black;")
			.append("	vertical-align: top;")			
			.append("}")
			
			.append("#assinatura{")
			.append("	font-size:10px;")
			.append("	padding:0px;")
			.append("}")
			
			.append("#rodape{")
			.append("	font-size:14px;")
			.append("	padding:10px;")
			.append("	border:1px solid #000;")
			.append("}")
			
			.append("#subtitulo{")
			.append("	font-family: Arial;")
			.append("	font-size:14px;")
			.append("	padding:10px;")
			.append("	font-weight:bold;")
			.append("}")
			
			.append("#quebraPagina{")
			.append("	page-break-after: always;")
			.append("}")
			
			.append("</style>"); 						
		return css.toString();
	}
	
	public static String replaceAll(String str, String replaceTo, String replaceFor){
		if(str == null) return "";
		return str.replaceAll(replaceTo, replaceFor);
	}
	
	public static String formataTimestamp(Timestamp timestamp, String pattern){
		if(timestamp == null) return "";
		return new SimpleDateFormat(pattern).format(timestamp);
	}
	
	public static String[] split(String str, String delimiter){
		return StringFunctions.split(str, delimiter);
	}
	
	public static String substring(String str, int posicaoInicial){
		return StringFunctions.substring(str, posicaoInicial);
	}
	
	public static String substring(String str, int posicaoInicial, int posicaoFinal){
		return StringFunctions.substring(str, posicaoInicial, posicaoFinal);
	}
	
	public static int indexOf(String str, String str2){
		return StringFunctions.indexOf(str, str2);
	}
	
	public static int lastIndexOf(String str, String str2){
		return StringFunctions.lastIndexOf(str, str2);
	}
	
	public static String stringCheia(String s, String c, int tamanho, boolean direita) {
		if (s == null){
			s = "";
		}
		String result = s;
		if (c==null){
			c = "";
		}
		if (c.equals("")) 
			c += "0";		
		while (result.length() < tamanho) {
			if (direita) 
				result += c;
			else
				result  = c + result;
		}
		if (result.length() > tamanho) {
			if (direita) 
				result = result.substring(0,tamanho);
			else
				result = result.substring(result.length() - tamanho, result.length());
		}
		return result.toUpperCase();
	}
	
	public static String stringCheia(String s, String c, int inicio, int tamanho, boolean direita) {
		if (s == null){
			s = "";
		}
		String result = s.length() >= inicio ? s.substring(inicio) : "";
		if (c==null){
			c = "";
		}
		if (c.equals("")) 
			c += "0";		
		while (result.length() < tamanho) {
			if (direita) 
				result += c;
			else
				result  = c + result;
		}
		if (result.length() > tamanho) {
			if (direita) 
				result = result.substring(0,tamanho);
			else
				result = result.substring(result.length() - tamanho, result.length());
		}
		return result.toUpperCase();
	}
	/*M�todo criado para n�o quebrar palavra no meio. Conforme solicitado, o m�todo considera que valor por extenso do cheque ter� no m�ximo 2 linhas.
	 * Solicitado na AT 160988*/
	public static String formataValorPorExtensoCheque(String s, String c, int inicio, int tamanho, boolean direita) {
		if (s == null || inicio > s.length()){
			return "";
		}
		int index = -1;
		if(inicio == 0){
			int tam = tamanho > s.length()? s.length(): tamanho; 
			index = (s + " ").substring(inicio, tam+1).lastIndexOf(" ");
			if(index > -1)
				s = (s + " ").substring(inicio, index);
		}else{
			if((s.length() > 1) && s.length() >= inicio+1 && " ".equals(s.substring(inicio, inicio+1))){
				inicio++;
			}else{
				if(!s.substring(inicio-1, inicio).equals(" ")){
					index = s.substring(0, inicio).lastIndexOf(" ");
					if(index > -1)
						inicio = index+1;
			
				}				
			}
		}

		return stringCheia(s, c, inicio, tamanho, direita);
	}

	public static String valorPorExtenso(Double valor){
		if(valor==null){
			return "";
		}
		return new Extenso(valor).toString();
	}
	
	public static String valorPorExtenso(Money valor){
		if(valor==null){
			return "";
		}
		return new Extenso(valor).toString();
	}
	
	public static String formataValor(Double valor){
		return new DecimalFormat("0.00").format(valor);
	}
	
	public static String formataValor(Double valor, int casasdecimais){
		String qtdecasas = stringCheia("", "0", casasdecimais, true);
		return new DecimalFormat("0."+qtdecasas).format(valor);
	}
	
	public static Double roundValor(Double valor, int casasdecimais){
		BigDecimal decimal = new BigDecimal(valor);
		decimal = decimal.setScale(10, BigDecimal.ROUND_HALF_UP);
		decimal = decimal.setScale(casasdecimais, BigDecimal.ROUND_HALF_UP);
		
		return decimal.doubleValue();
	}
	
	public static String roundValorFormatado(Double valor, int casasdecimais){
		BigDecimal decimal = new BigDecimal(valor);
		decimal = decimal.setScale(10, BigDecimal.ROUND_HALF_UP);
		decimal = decimal.setScale(casasdecimais, BigDecimal.ROUND_HALF_UP);
		
		String qtdecasas = stringCheia("", "0", casasdecimais, true);
		return new DecimalFormat("0."+qtdecasas).format(decimal.doubleValue());
	}
	
	public static String codigobarra(String modelo, String codigo){
		Image im = null;
		ModeloBarcode modeloBarcode = ModeloBarcode.existsModelo(modelo);
		if(modeloBarcode==null){
			return "";
		}
		if(modeloBarcode.equals(ModeloBarcode.INT2OF5)){
			BarCode2of5 barcode2of5 = new BarCode2of5();
			im = barcode2of5.createImage(codigo);			
		}else{
			Barcode barcode = null;
			switch (modeloBarcode) {
			case  EAN13:
				barcode = new BarcodeEAN();
				barcode.setCodeType(1);
				barcode.setCode(codigo);
				break;
			
			case  EAN8:
					barcode = new BarcodeEAN();
					barcode.setCodeType(2);
					barcode.setCode(codigo);
					break;
					
			case UPCA:
				barcode = new BarcodeEAN();
				barcode.setCodeType(3);
				barcode.setCode(codigo);
				break;
				
			case UPCE:
				barcode = new BarcodeEAN();
				barcode.setCodeType(4);
				barcode.setCode(codigo);
				break;

			case CODE39:
				barcode = new Barcode39();
				barcode.setCode(codigo);
				break;

			case CODE128:
				barcode = new Barcode128();
				barcode.setCodeType(9);
				barcode.setCode(codigo);
				break;
				
			case CODABAR:
				barcode = new BarcodeCodabar();
				barcode.setCode(codigo);
				break;
				
			default: return "";
			}
			
			try {
				im = barcode.createAwtImage(Color.BLACK, Color.WHITE);
			} catch (Exception e) {
				return "";
			}			
		}

	    return SinedUtil.convertImageToBase64(im);
	}
	
	public static String concat(String s1, String s2){
		return StringUtils.limpaNull(s1).concat(
									StringUtils.limpaNull(s2));
	}
}