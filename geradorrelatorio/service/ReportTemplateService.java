package br.com.linkcom.geradorrelatorio.service;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.dao.ReportTemplateDAO;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.sined.geral.bean.NotaTipo;
import br.com.linkcom.sined.util.SinedException;

public class ReportTemplateService extends ReportTemplateEntityService<ReportTemplateBean> {
	
	private static ReportTemplateService instance;
	private ReportTemplateDAO reportTemplateDAO;
	
	public static ReportTemplateService getInstance(){
		if (instance==null){
			instance = Neo.getObject(ReportTemplateService.class);
		}
		return instance;
	}
	
	public void setReportTemplateDAO(ReportTemplateDAO reportTemplateDAO) {
		this.reportTemplateDAO = reportTemplateDAO;
	}
	public ReportTemplateBean loadTemplateByCategoriaAndNome(EnumCategoriaReportTemplate categoria,String Nome) {
		try{			
			List<ReportTemplateBean> lista = this.loadTemplatesByCategoriaOrderByNewest(categoria);	
			for (ReportTemplateBean bean : lista) {
				String nomeTemplate = bean.getNome().replaceAll(" ", "");
				if(nomeTemplate.trim().equalsIgnoreCase(Nome)){
					return bean;
				}
			}
			return loadTemplateByCategoria(categoria);
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar a lista de templates disponíveis.");
		}
	}
	public ReportTemplateBean loadTemplateByCategoria(EnumCategoriaReportTemplate categoria) {
		try{			
			List<ReportTemplateBean> lista = this.loadTemplatesByCategoriaOrderByNewest(categoria);
			
			if(lista != null && lista.size() > 0) 
				return lista.get(0);
			else
				return null;
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar a lista de templates disponíveis.");
		}
	}

	public List<ReportTemplateBean> loadListaTemplateByCategoria(EnumCategoriaReportTemplate categoria) {
		try{			
			List<ReportTemplateBean> lista = this.loadTemplatesByCategoriaOrderByNewest(categoria);
			return lista;	
		}catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar a lista de templates disponíveis.");
		}
	}
	
	public List<ReportTemplateBean> loadListaTemplateByCategoria(EnumCategoriaReportTemplate categoria, String order) {
		try{			
			List<ReportTemplateBean> lista = reportTemplateDAO.loadTemplatesByCategoriaOrderBy(categoria, order);
			return lista;	
		}catch (Exception e) {
			e.printStackTrace();
			throw new SinedException("Erro ao gerar a lista de templates disponíveis.");
		}
	}
	
	public List<ReportTemplateBean> listaTemplatesInfoContribuinte(NotaTipo notatipo){
		if(notatipo != null){
			if(notatipo.equals(NotaTipo.NOTA_FISCAL_SERVICO)){
				return this.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.INFORMACOES_AO_CONTRIBUINTE_NFSE);
			}else{
				return this.loadListaTemplateByCategoria(EnumCategoriaReportTemplate.INFORMACOES_AO_CONTRIBUINTE_NFE);
			}
		}
		return null;	
	}	
}
