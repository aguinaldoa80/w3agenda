package br.com.linkcom.geradorrelatorio.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.com.linkcom.geradorrelatorio.bean.GeradorPermissaoBean;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBeanHistorico;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioTotalizadorBean;
import br.com.linkcom.geradorrelatorio.bean.GrupoRelatorio;
import br.com.linkcom.geradorrelatorio.dao.RelatorioDAO;
import br.com.linkcom.geradorrelatorio.process.filtro.GenericGeradorFiltroListagem;
import br.com.linkcom.geradorrelatorio.util.GeradorRelatorioException;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.util.SinedUtil;

public class RelatorioService extends GeradorRelatorioEntityService {
	
	private RelatorioDAO relatorioDAO;
	
	public void setRelatorioDAO (RelatorioDAO relatorioDAO){
		this.relatorioDAO = relatorioDAO;
	}
	
	@Override
	public boolean isUsuarioAdmin() {
		String modulo = (String) NeoWeb.getRequestContext().getAttribute("NEO_MODULO");
		
		if(!modulo.equalsIgnoreCase("pub")){
			String url = SinedUtil.getUrlWithoutContext();
			
			List<String> urlsAdmin = new ArrayList<String>();
			urlsAdmin.add("linkcom.w3erp.com.br");
			urlsAdmin.add("izap.w3erp.com.br");
			urlsAdmin.add("linkcom.teste1:8080");
			urlsAdmin.add("izap.teste1:8080");
			
			urlsAdmin.add("linkcom.localhost:8080");
			urlsAdmin.add("izap.localhost:8080");
			urlsAdmin.add("linkcom.w3erp.local:8080");
			urlsAdmin.add("linkcom.piedade.local:8080");
			urlsAdmin.add("izap.w3erp.local:8080");
			urlsAdmin.add("izap.piedade.local:8080");
			
			Usuario usuario = SinedUtil.getUsuarioLogado();
			
			if(usuario.getLogin().equals("aline.lopes")) return true;
			if(usuario.getLogin().equals("leandro.oliveira")) return true;
			if(usuario.getLogin().equals("pablo.lemos")) return true;
			if(usuario.getLogin().equals("edirene.silva")) return true;
			if(urlsAdmin.contains(url)){
				return SinedUtil.isUsuarioLogadoAdministrador();
			} else {
				return usuario.getCdpessoa()==1;
			}
		} else {
			return true;
		}
	}

	@Override
	protected List<Map<String, Object>> getDataSource(GeradorRelatorioBean relatorio, HashMap<String, Object> parametros, Boolean usarFormat, Boolean markAsReader) {
		try {
			return super.getDataSource(relatorio, parametros, usarFormat, markAsReader);
		} catch (GeradorRelatorioException e) {
			String msg = "Erro na parametrização da query!";
			if(isUsuarioAdmin())
				msg += "<br/>QUERY: " + relatorio.getQuery();
			throw new GeradorRelatorioException(msg);
		} catch (Exception e) {
			String msg = "Erro ao realizar a consulta da query!";
			if(isUsuarioAdmin())
				msg += "<br/>QUERY: " + relatorio.getQuery();
			throw new GeradorRelatorioException(msg);
		}
	}

	@Override
	protected List<Map<String, Object>> getDataSource(GeradorRelatorioBean relatorio, String query, HashMap<String, Object> parametros, Integer currentPage, Integer pageSize) {
		try {			
			return super.getDataSource(relatorio, query, parametros, currentPage, pageSize, Boolean.TRUE, Boolean.FALSE);
		} catch (GeradorRelatorioException e) {
			String msg = "Erro na parametrização da query!";
			if(isUsuarioAdmin())
				msg += "<br/>QUERY: " + relatorio.getQuery();
			throw new GeradorRelatorioException(msg);
		} catch (Exception e) {
			String msg = "Erro ao realizar a consulta da query!";
			if(isUsuarioAdmin())
				msg += "<br/>QUERY: " + relatorio.getQuery();
			throw new GeradorRelatorioException(msg);
		}
	}
	
	@Override
	public Map<String, String> getVariaveisQuery() {
		Map<String, String> variaveis = new HashMap<String, String>();
		Usuario usuarioLogado = SinedUtil.getUsuarioLogado();
		variaveis.put("USUARIO_LOGADO", usuarioLogado != null ? usuarioLogado.getCdpessoa()+"" : null);
		
		return variaveis;
	}
	
	@Override
	public Map<String, Object> getVariaveisRelatorio() {
		Map<String, Object> variaveis = new LinkedHashMap<String, Object>();
		variaveis.put("usuario_logado", SinedUtil.getUsuarioLogado());
		variaveis.put("colaborador_logado", SinedUtil.getUsuarioComoColaborador());
		
		variaveis.put("pdf_page", "<span class='page'></span>");
		variaveis.put("pdf_frompage", "<span class='frompage'></span>");
		variaveis.put("pdf_topage", "<span class='topage'></span>");
		variaveis.put("pdf_webpage", "<span class='webpage'></span>");
		variaveis.put("pdf_section", "<span class='section'></span>");
		variaveis.put("pdf_subsection", "<span class='subsection'></span>");
		variaveis.put("pdf_date", "<span class='date'></span>");
		variaveis.put("pdf_isodate", "<span class='isodate'></span>");
		variaveis.put("pdf_time", "<span class='time'></span>");
		variaveis.put("pdf_title", "<span class='title'></span>");
		variaveis.put("pdf_doctitle", "<span class='doctitle'></span>");
		variaveis.put("pdf_sitepage", "<span class='sitepage'></span>");
		variaveis.put("pdf_sitepages", "<span class='sitepages'></span>");

		return variaveis;
	}
	
	public List<GeradorRelatorioBean> findByGrupoRelatorio(GrupoRelatorio gruporelatorio){
		SinedUtil.markAsReader();
		return relatorioDAO.findByGrupoRelatorio(gruporelatorio, isUsuarioAdmin());
	}
	
	
	public GeradorRelatorioBeanHistorico setValoresAnterior (GeradorRelatorioBean bean) {
		GeradorRelatorioBean geradorRelatorioBean =  loadForEntrada(bean);

		GeradorRelatorioBeanHistorico geradorRelatorioBeanHistorico = new GeradorRelatorioBeanHistorico();
		
		//Histórico Configurações de visualização
		geradorRelatorioBeanHistorico.setGerarrelatorio(geradorRelatorioBean.getGerarrelatorio());
		geradorRelatorioBeanHistorico.setGerargrafico(geradorRelatorioBean.getGerargrafico());
		geradorRelatorioBeanHistorico.setVisualizardados(geradorRelatorioBean.getVisualizardados());
		geradorRelatorioBeanHistorico.setGerarpdf(geradorRelatorioBean.getGerarpdf());
		geradorRelatorioBeanHistorico.setGerarcsv(geradorRelatorioBean.getGerarcsv());
		geradorRelatorioBeanHistorico.setGerartxt(geradorRelatorioBean.getGerartxt());
		geradorRelatorioBeanHistorico.setGerarW3erp(geradorRelatorioBean.getGerarW3erp());
		geradorRelatorioBeanHistorico.setExibirtotalizadores(geradorRelatorioBean.getExibirtotalizadores());
		geradorRelatorioBeanHistorico.setDisponibilizarAreaCliente(geradorRelatorioBean.getDisponibilizarAreaCliente());
		
		//Histórico Consulta
		geradorRelatorioBeanHistorico.setNome(geradorRelatorioBean.getNome());
		geradorRelatorioBeanHistorico.setDescricao(geradorRelatorioBean.getDescricao());
		geradorRelatorioBeanHistorico.setObservacaoconsulta(geradorRelatorioBean.getObservacao());
		
		if(geradorRelatorioBean.getGrupoRelatorio() != null){
		geradorRelatorioBeanHistorico.setGrupoRelatorio(geradorRelatorioBean.getGrupoRelatorio().getNome());
		}
		
		geradorRelatorioBeanHistorico.setQuery(geradorRelatorioBean.getQuery());
		
		//Histórico Relatório
		geradorRelatorioBeanHistorico.setLeiauterelatorio(geradorRelatorioBean.getLeiauterelatorio());
		
		//Histórico Cabeçalho
		geradorRelatorioBeanHistorico.setLeiautecabecalho(geradorRelatorioBean.getLeiautecabecalho());
		
		//Histórico Rodapé
		geradorRelatorioBeanHistorico.setLeiauterodape(geradorRelatorioBean.getLeiauterodape());
		
		//Histórico Gráfico
		geradorRelatorioBeanHistorico.setEixox(geradorRelatorioBean.getX());
		geradorRelatorioBeanHistorico.setEixoy(geradorRelatorioBean.getY());
		geradorRelatorioBeanHistorico.setSerie(geradorRelatorioBean.getSerie());
		
		//Histórico Totalizadores
		if(geradorRelatorioBean.getListaGeradorRelatorioTotalizador().size() > 0 ) {
		StringBuilder dadostotalizadores = new StringBuilder();
		for( GeradorRelatorioTotalizadorBean totalizadores: geradorRelatorioBean.getListaGeradorRelatorioTotalizador()) {
		dadostotalizadores.append(totalizadores.getCampo()+"\n");
		}
		geradorRelatorioBeanHistorico.setTotalizadores(dadostotalizadores.toString());
		}
		
		//Histórico Permissões
		if(geradorRelatorioBean.getListaGeradorPermissao().size() > 0 ) {
		StringBuilder dadospermissoes = new StringBuilder();
		for(GeradorPermissaoBean permissoes: geradorRelatorioBean.getListaGeradorPermissao()) {
		dadospermissoes.append(permissoes.getGeradorpapelview().getNome()+"\n");
		}
		geradorRelatorioBeanHistorico.setPermissoes(dadospermissoes.toString());
		}	
		return geradorRelatorioBeanHistorico;
	}
	
	
	@Override
	public void processRelatorioForRequest(WebRequestContext request, GenericGeradorFiltroListagem<GeradorRelatorioBean> filtro) throws Exception {
		SinedUtil.markAsReader();
		super.processRelatorioForRequest(request, filtro);
	}
	
}
