package br.com.linkcom.geradorrelatorio.service;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBeanHistorico;
import br.com.linkcom.geradorrelatorio.dao.GeradorRelatorioBeanHistoricoDAO;
import br.com.linkcom.neo.service.GenericService;

public class GeradorRelatorioBeanHistoricoService extends GenericService<GeradorRelatorioBeanHistorico> {

	private GeradorRelatorioBeanHistoricoDAO geradorRelatoriobeanHistoricoDAO;
	
	
	public void setGeradorRelatoriobeanHistoricoDAO(
			GeradorRelatorioBeanHistoricoDAO geradorRelatoriobeanHistoricoDAO) {
		this.geradorRelatoriobeanHistoricoDAO = geradorRelatoriobeanHistoricoDAO;
	}


	public List<GeradorRelatorioBeanHistorico> findRelatorioHistorico (GeradorRelatorioBean bean) {
		return geradorRelatoriobeanHistoricoDAO.findRelatorioHistorico(bean);
	}
	
	
}
