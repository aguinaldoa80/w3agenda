package br.com.linkcom.geradorrelatorio.service;

import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.modulo.pub.controller.process.IntegracaoWebserviceProcess;
public class WSService extends GeradorWSEntityService {
	
	private RelatorioService relatorioService;
	private ParametrogeralService parametrogeralService;
	
	public void setRelatorioService(RelatorioService relatorioService) {
		this.relatorioService = relatorioService;
	}
	
	public void setParametrogeralService(ParametrogeralService parametrogeralService) {
		this.parametrogeralService = parametrogeralService;
	}
	
	@Override
	public boolean isUsuarioAdmin() {
		return relatorioService.isUsuarioAdmin();
	}
	
	@Override
	protected boolean validHash(String hash) {
//		if(true) return true;
		String tokenValidacao = parametrogeralService.getValorPorNome(IntegracaoWebserviceProcess.PARAMETRO_TOKEN_VALIDACAO_WEBSERVICE);
		return hash != null && tokenValidacao != null && hash.equals(tokenValidacao);
	}
}
