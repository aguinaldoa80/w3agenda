<tr class="entrada1">
	<td colspan="1" class=" propertyColumn ">
		{{= label}}
	</td>
</tr>
<tr class="entrada1" >
	<td colspan="1" class=" propertyColumn ">
		<select {{if obrigatorio==true}} class="required" {{/if}} id="{{= nome}}" name="{{= nome}}" onchange="$GR.updateDescriptionProperty(this);" multiple="true" size="6" style="max-width:100%; height: auto;width: 300px;">
			{{if obrigatorio==false}} <option value="<null>"></option>{{/if}}
			{{each(i, value) listaParametros}}
				<option value="{{= id}}">{{= value}}</option>
			{{/each}}
		</select>
		<input id="{{= nome}}_descriptionproperty" name="{{= nome}}_descriptionproperty" type="hidden"></input>
	</td>
</tr>
