<tr class="entrada1">
	<td colspan="1" class=" propertyColumn ">
		{{= label}}
	</td>
</tr>
<tr class="entrada1" >
	<td colspan="1" class=" propertyColumn ">
		<select {{if obrigatorio==true}} class="required" {{/if}} id="{{= nome}}" name="{{= nome}}" onchange="$GR.updateDescriptionProperty(this);" style="width: 300px; height: auto;">
			{{if obrigatorio==false}} <option value="<null>"></option>{{/if}}
			{{each(i, value) listaParametros}}
				<option value="{{= id}}">{{= value}}</option>
			{{/each}}
		</select>
		<input id="{{= nome}}_descriptionproperty" name="{{= nome}}_descriptionproperty" type="hidden"></input>
	</td>
</tr>
