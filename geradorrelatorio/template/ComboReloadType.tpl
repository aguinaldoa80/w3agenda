<tr class="entrada1">
	<td colspan="1" class=" propertyColumn ">
		{{= label}}:
	</td>
</tr>
<tr class="entrada1" >
	<td colspan="1" class=" propertyColumn ">
		<select {{if obrigatorio==true}} class="required" {{/if}} id="{{= nome}}" name="{{= nome}}" onchange="$GR.updateDescriptionProperty(this);" style="max-width:100%; height: auto;">
			{{if obrigatorio==false}} <option value="<null>"></option>{{/if}}
		</select>
		<input id="{{= nome}}_descriptionproperty" name="{{= nome}}_descriptionproperty" type="hidden"></input>
	</td>
</tr>