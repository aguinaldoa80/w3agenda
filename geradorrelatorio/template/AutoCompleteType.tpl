<tr class="entrada1">
	<td colspan="1" class=" propertyColumn ">
		{{= label}}
	</td>
</tr>
<tr class="entrada1" >
	<td colspan="1" class=" propertyColumn ">
		<span id="spanAutocomplete_{{= nome}}" style="white-space: nowrap;">
			<input type="text" 
			style="width: 294px;"
			name="{{= nome}}_label" 
			{{if obrigatorio==true}} class="required" {{/if}}
			funcao="autocomplete" 
			getterlabel="" 
			propertylabel="" 
			propertymatch="" 
			loadfunctionautocomplete="relatorioService.findForAutocompleteGR" 
			beanname="{{= nome}}" 
			value="" style="width:300px;" 
			haveautocomplete="true" 
			autocomplete="off" 
			class="ac_input"
			onFocus="$GR.selecionarAutocomplete('${relatorio.value.substring(relatorio.value.indexOf('=')+1,relatorio.value.indexOf(']'))}', '{{= nome}}')"
			/>
			<input type="hidden" autocompleteId="{{= nome}}_value" name="{{= nome}}"/>
			
		
			<span autocompleteId="{{= nome}}_NaoSelecionado" style="">
				<i class="fas fa-exclamation-circle" onmouseover="Tip('N�o selecionado')"></i>
				<i class="fas fa-trash-alt" onmouseover="Tip('Excluir')"></i>
			</span>
			<span autocompleteId="{{= nome}}_Selecionado" style="display: none;">
				<i class="fas fa-check-circle" onmouseover="Tip('Selecionado')"></i>
				<a href="javascript:excluirAutocomplete('{{= nome}}')"><i class="fas fa-trash-alt" onmouseover="Tip('Excluir')"></i></a>
			</span>	
		</span>	
	</td>
</tr>

