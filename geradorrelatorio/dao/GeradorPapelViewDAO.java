package br.com.linkcom.geradorrelatorio.dao;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.view.GeradorPapelView;
import br.com.linkcom.neo.persistence.DefaultOrderBy;
import br.com.linkcom.neo.persistence.GenericDAO;

@DefaultOrderBy("geradorPapelView.nome")
public class GeradorPapelViewDAO<BEAN extends GeradorPapelView> extends GenericDAO<GeradorPapelView> {
	
	public List<GeradorPapelView> findAllForCrud(){
		return query()
		.select("geradorPapelView.cdgeradorpapel, geradorPapelView.nome, geradorPapelView.administrador, geradorPapelView.descricao")
				.where("geradorPapelView.administrador is not true")
				.orderBy("geradorPapelView.nome")
				.list();
	}
}
