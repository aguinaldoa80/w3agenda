package br.com.linkcom.geradorrelatorio.dao;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.ReportTemplateBean;
import br.com.linkcom.geradorrelatorio.bean.enumeration.EnumCategoriaReportTemplate;
import br.com.linkcom.geradorrelatorio.crud.filtro.ReportTemplateCrudFiltro;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.persistence.QueryBuilder;


public class ReportTemplateDAO extends ReportTemplateEntityDAO<ReportTemplateBean> {

	private String whereInCategorias = null;
	
	private String getWhereInCategorias(){
		if(this.whereInCategorias == null){
			StringBuilder whereIn = new StringBuilder();
			for(EnumCategoriaReportTemplate categoria : EnumCategoriaReportTemplate.getCategoriasVisiveis()){
				whereIn.append("'").append(categoria.name()).append("', ");
			}
			if(whereIn.length() > 0){
				this.whereInCategorias = whereIn.deleteCharAt(whereIn.length()-2).toString();
			}
		}
		return this.whereInCategorias;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<ReportTemplateBean> query,	FiltroListagem filtro) {
		if(filtro != null){
			ReportTemplateCrudFiltro _filtro = (ReportTemplateCrudFiltro)filtro;
			query
				.where("categoria = ?", _filtro.getCategoria())
				.whereIn("categoria", getWhereInCategorias())
				.whereLikeIgnoreAll("nome", _filtro.getNome())
				.where("arquivoleiaute is null");
		}
			
	}
	
	@Override
	public List<ReportTemplateBean> loadTemplatesByCategoriaOrderByNewest(EnumCategoriaReportTemplate categoria) {
		return query()
				.whereLikeIgnoreAll("categoria", categoria.name())
				.orderBy("cdreporttemplate DESC")
				.list();
	}
	
	public List<ReportTemplateBean> loadTemplatesByCategoriaOrderBy(EnumCategoriaReportTemplate categoria, String order) {
		return query()
				.whereLikeIgnoreAll("categoria", categoria.name())
				.orderBy(order)
				.list();
	}
}
