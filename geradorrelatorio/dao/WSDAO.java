package br.com.linkcom.geradorrelatorio.dao;

import java.util.List;
import java.util.Map;

import br.com.linkcom.geradorrelatorio.bean.AbstractGeradorEntity;
import br.com.linkcom.geradorrelatorio.bean.GeradorWSBean;
import br.com.linkcom.geradorrelatorio.bean.transiente.ColunaBean;
import br.com.linkcom.sined.util.SinedUtil;


public class WSDAO extends GeradorWSEntityDAO<GeradorWSBean> {

	@Override
	public List<GeradorWSBean> findAllWithPermission(boolean isUsuarioAdmin) {
		SinedUtil.markAsReader();
		return super.findAllWithPermission(isUsuarioAdmin);
	}
	
	@Override
	public GeradorWSBean loadWithPermission(Integer pk, boolean isUsuarioAdmin) {
		SinedUtil.markAsReader();
		return super.loadWithPermission(pk, isUsuarioAdmin);
	}
	
	@Override
	public List<Map<String, Object>> getDataSource(String queryParametrizada, List<Object> parametros, boolean usarFormat, Boolean markAsReader) {
		return super.getDataSource(queryParametrizada, parametros, usarFormat, true);
	}
	
	@Override
	public List<Map<String, Object>> getListaGenericBean(String sql) {
		SinedUtil.markAsReader();
		return super.getListaGenericBean(sql);
	}
	
	@Override
	public Integer getNextId() {
		SinedUtil.markAsReader();
		return super.getNextId();
	}
	
	@Override
	public AbstractGeradorEntity findByChave(String chave) {
		SinedUtil.markAsReader();
		return super.findByChave(chave);
	}
	
	@Override
	public List<ColunaBean> getResultColunaBean(String query) {
		SinedUtil.markAsReader();
		return super.getResultColunaBean(query);
	}
	
}
