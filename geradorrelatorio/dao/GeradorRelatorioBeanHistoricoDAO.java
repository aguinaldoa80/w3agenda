package br.com.linkcom.geradorrelatorio.dao;

import java.util.List;

import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBeanHistorico;
import br.com.linkcom.neo.persistence.GenericDAO;


public class GeradorRelatorioBeanHistoricoDAO extends GenericDAO<GeradorRelatorioBeanHistorico> {
	
	
	public List<GeradorRelatorioBeanHistorico> findRelatorioHistorico(GeradorRelatorioBean bean) {
		return query()
				.select("geradorRelatorioBeanHistorico.cdGeradorRelatorioBeanHistorico, geradorRelatorioBeanHistorico.cdusuarioaltera,"+
						"geradorRelatorioBeanHistorico.dtaltera, geradorRelatorioBeanHistorico.gerarrelatorio, geradorRelatorioBean.cdgeradorrelatorio,"+
						"geradorRelatorioBeanHistorico.gerargrafico, geradorRelatorioBeanHistorico.gerarcsv, " +
						"geradorRelatorioBeanHistorico.gerartxt,geradorRelatorioBean.gerarhtml, geradorRelatorioBeanHistorico.gerarpdf, geradorRelatorioBeanHistorico.gerarW3erp," +
						"geradorRelatorioBeanHistorico.visualizardados, geradorRelatorioBeanHistorico.exibirtotalizadores, geradorRelatorioBeanHistorico.disponibilizarAreaCliente,"+
						"geradorRelatorioBeanHistorico.nome, geradorRelatorioBeanHistorico.descricao, geradorRelatorioBeanHistorico.observacaoconsulta,"+
						"geradorRelatorioBeanHistorico.grupoRelatorio, geradorRelatorioBeanHistorico.query, geradorRelatorioBeanHistorico.leiauterelatorio,"+
						"geradorRelatorioBeanHistorico.leiautecabecalho, geradorRelatorioBeanHistorico.leiauterodape, geradorRelatorioBeanHistorico.eixox,"+
						"geradorRelatorioBeanHistorico.eixoy, geradorRelatorioBeanHistorico.serie, geradorRelatorioBeanHistorico.totalizadores,"+
						"geradorRelatorioBeanHistorico.permissoes, geradorRelatorioBeanHistorico.acao, geradorRelatorioBean.cdgeradorrelatorio")
						.leftOuterJoin("geradorRelatorioBeanHistorico.geradorRelatorioBean geradorRelatorioBean")
						.where("geradorRelatorioBean = ?", bean)
						.orderBy("geradorRelatorioBeanHistorico.dtaltera desc")
						.list();
	}
}

