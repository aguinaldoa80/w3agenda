package br.com.linkcom.geradorrelatorio.dao;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.linkcom.geradorrelatorio.bean.AbstractGeradorEntity;
import br.com.linkcom.geradorrelatorio.bean.GeradorRelatorioBean;
import br.com.linkcom.geradorrelatorio.bean.GrupoRelatorio;
import br.com.linkcom.geradorrelatorio.bean.transiente.ColunaBean;
import br.com.linkcom.geradorrelatorio.bean.view.GeradorPapelView;
import br.com.linkcom.geradorrelatorio.crud.filtro.GeradorRelatorioFiltro;
import br.com.linkcom.neo.controller.crud.FiltroListagem;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.persistence.QueryBuilder;
import br.com.linkcom.neo.persistence.SaveOrUpdateStrategy;
import br.com.linkcom.neo.util.HibernateUtils;
import br.com.linkcom.sined.geral.dao.ArquivoDAO;
import br.com.linkcom.sined.util.SinedUtil;

public class RelatorioDAO extends GeradorRelatorioEntityDAO<GeradorRelatorioBean> {

	private ArquivoDAO arquivoDAO;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<GeradorRelatorioBean> query, FiltroListagem _filtro) {
		GeradorRelatorioFiltro filtro = (GeradorRelatorioFiltro) _filtro;
		query
			.where("geradorRelatorioBean.grupoRelatorio = ?", filtro.getGrupoRelatorio())
			.whereLikeIgnoreAll("geradorRelatorioBean.nome", filtro.getNome())
			.whereLikeIgnoreAll("geradorRelatorioBean.query", filtro.getQuery());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<GeradorRelatorioBean> query) {
		query
			.select("geradorRelatorioBean.cdgeradorrelatorio, geradorRelatorioBean.gerarrelatorio, geradorRelatorioBean.descricao, " +
					"geradorRelatorioBean.nome, geradorRelatorioBean.query, geradorRelatorioBean.observacao, " +
					"geradorRelatorioBean.gerargrafico, geradorRelatorioBean.gerarcsv,geradorRelatorioBean.gerarhtml, geradorRelatorioBean.gerartxt, geradorRelatorioBean.gerarpdf, " +
					"geradorRelatorioBean.gerarW3erp,geradorRelatorioBean.visualizardados, geradorRelatorioBean.exibirtotalizadores, " +
					"geradorRelatorioBean.x, geradorRelatorioBean.y, geradorRelatorioBean.qtdeCamposLinha, arquivo.cdarquivo, arquivo.nome, " +
					"geradorRelatorioBean.serie, geradorRelatorioBean.leiauterelatorio, geradorRelatorioBean.orientacao, " +
					"geradorRelatorioBean.leiautecabecalho, geradorRelatorioBean.leiauterodape, " +
					"geradorRelatorioBean.margemSuperior, geradorRelatorioBean.margemInferior, geradorRelatorioBean.margemEsquerda, " +
					"geradorRelatorioBean.margemDireita, geradorRelatorioBean.largura, geradorRelatorioBean.altura, geradorRelatorioBean.disponibilizarAreaCliente, " +
					"listaGeradorGrafico.cdgeradorgrafico, listaGeradorGrafico.cdtipografico, " +
					"listaGeradorGrafico.ativo, listaGeradorGrafico.padrao, " +
					"listaGeradorRelatorioTotalizador.cdgeradorrelatoriototalizador, listaGeradorRelatorioTotalizador.campo," +
					"grupoRelatorio.cdgruporelatorio, grupoRelatorio.nome," +
					"listaGeradorPermissao.cdgeradorpermissao, geradorpapelview.cdgeradorpapel, geradorpapelview.nome, " +
					"geradorpapelview.descricao")
			.leftOuterJoin("geradorRelatorioBean.listaGeradorGrafico listaGeradorGrafico")
			.leftOuterJoin("geradorRelatorioBean.listaGeradorRelatorioTotalizador listaGeradorRelatorioTotalizador")
			.leftOuterJoin("geradorRelatorioBean.grupoRelatorio grupoRelatorio")
			.leftOuterJoin("geradorRelatorioBean.listaGeradorPermissao listaGeradorPermissao")
			.leftOuterJoin("listaGeradorPermissao.geradorpapelview geradorpapelview")
			.leftOuterJoin("geradorRelatorioBean.arquivo arquivo");
	}
	
	@Override
	public void updateSaveOrUpdate(final SaveOrUpdateStrategy save) {
		getTransactionTemplate().execute(new TransactionCallback(){
			public Object doInTransaction(TransactionStatus status) {
				save.useTransaction(false);
				save.saveOrUpdateManaged("listaGeradorGrafico");
				save.saveOrUpdateManaged("listaGeradorRelatorioTotalizador");
				save.saveOrUpdateManaged("listaGeradorPermissao");
				
				GeradorRelatorioBean geradorRelatorioBean = (GeradorRelatorioBean) save.getEntity();
				
				if(geradorRelatorioBean.getArquivo() != null){
					arquivoDAO.saveFile(geradorRelatorioBean, "arquivo");
				}
				
				return null;
			}
		});
	}
	
	@Override
	public List<GeradorRelatorioBean> findAllForPermissaoByPapel(GeradorPapelView papel) throws Exception {
		SinedUtil.markAsReader();
		return super.findAllForPermissaoByPapel(papel);
	}
	
	@Override
	public List<ColunaBean> getResultColunaBean(String query) {
		SinedUtil.markAsReader();
		return super.getResultColunaBean(query);
	}
	
	@Override
	public AbstractGeradorEntity findByChave(String chave) {
		SinedUtil.markAsReader();
		return super.findByChave(chave);
	}
	
	@Override
	public List<Map<String, Object>> getListaGenericBean(String sql) {
		SinedUtil.markAsReader();
		return super.getListaGenericBean(sql);
	}
	
	@Override
	public Integer getNextId() {
		SinedUtil.markAsReader();
		return super.getNextId();
	}
	
	@Override
	public List<GeradorRelatorioBean> findAllWithPermission(boolean isUsuarioAdmin) {
		SinedUtil.markAsReader();
		return super.findAllWithPermission(isUsuarioAdmin);
	}
	
	
	@Override
	public List<Map<String, Object>> getDataSource(String queryParametrizada, List<Object> parametros, boolean usarFormat, Boolean markAsReader) {
		return super.getDataSource(queryParametrizada, parametros, usarFormat, true);
	}
	
	@Override
	public GeradorRelatorioBean loadWithPermission(Integer pk, boolean isUsuarioAdmin) {
		SinedUtil.markAsReader();
		return super.loadWithPermission(pk, isUsuarioAdmin);
	}
	
	@Override
	protected List<Object> executeQuery(String query) {
		SinedUtil.markAsReader();
		return super.executeQuery(query);
	}
	
	@Override
	public Integer getTotalPaginas(String queryParametrizada, List<Object> parametros) {
		SinedUtil.markAsReader();
		return super.getTotalPaginas(queryParametrizada, parametros);
	}
	
	public List<GeradorRelatorioBean> findByGrupoRelatorio(GrupoRelatorio gruporelatorio, boolean isUsuarioAdmin){

		SinedUtil.markAsReader();
		return query()
					.leftOuterJoin("geradorRelatorioBean.listaGeradorPermissao listaGeradorPermissao", Boolean.TRUE)
					.leftOuterJoin("listaGeradorPermissao.geradorpapelview geradorpapelview", Boolean.TRUE)
					.leftOuterJoin("geradorpapelview.listaGeradorUsuarioPapelView listaGeradorUsuarioPapelView", Boolean.TRUE)
					.openParentheses()
							.where("listaGeradorPermissao is null")
							.or()
						.where("listaGeradorUsuarioPapelView.cdgeradorusuario = ?", HibernateUtils.getId(getHibernateTemplate(), Neo.getRequestContext().getUser()))
						.or()
						.where("1=1", Boolean.TRUE.equals(SinedUtil.isUsuarioLogadoAdministrador()))
					.closeParentheses()
					.where("geradorRelatorioBean.grupoRelatorio = ?", gruporelatorio)
					.orderBy("geradorRelatorioBean.nome")
					.list();
	}

}
