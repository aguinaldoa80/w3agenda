package br.com.linkcom.neo.authorization;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jasypt.util.password.StrongPasswordEncryptor;

import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ajax.View;
import br.com.linkcom.sined.geral.bean.Usuario;
import br.com.linkcom.sined.geral.dao.UsuarioDAO;
import br.com.linkcom.sined.geral.service.SessaoService;
import br.com.linkcom.sined.geral.service.UsuarioService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.offline.UsuarioOfflineJSON;

/**
 * Classe que cont�m os met�dos utilit�rios de autentica��o no Neo. 
 * Criada para que a l�gica da autentica��o fique desvinvulada dos filtros e pode ser reutilizada em outras partes do c�digo
 */


public class AuthorizationUtilSined {

	public static boolean verifyPassword(User user, String password) {
    	boolean passwordMatch = false;
		boolean updatePassword = false;
		
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		
		// se o usu�rio existe e a senha foi passada
		if (user != null && user.getPassword() != null) {
			boolean loginAutomatico = "true".equals(NeoWeb.getRequestContext().getParameter("loginAutomatico"));
			
			if(loginAutomatico){
				String urlDestino = NeoWeb.getRequestContext().getParameter("urlDestino");
				if(urlDestino != null && !urlDestino.equals("")){
					NeoWeb.getRequestContext().getSession().setAttribute("originator", urlDestino);
				}
			}
			
			if(!loginAutomatico && user.getPassword().matches("[a-zA-Z0-9\\+/]{64}")) {
				// Jasypt Strong Encryption
				updatePassword = true;
				if (passwordEncryptor.checkPassword(password, user.getPassword())) {
					passwordMatch = true;
				}
			}
			else if(!loginAutomatico && user.getPassword().matches("[0-9a-f]{32}")) {
				// MD5 simples
				if (Util.crypto.makeHashMd5(password).equals(user.getPassword())) {
					passwordMatch = true;
				}
			}
			else {
				// Senha pura
				if(!loginAutomatico) updatePassword = true;
				if (user.getPassword().equals(password)) {
					passwordMatch = true;
				}
			}
		}

		if (passwordMatch) {
			String hashSenha = Util.crypto.makeHashMd5(user.getPassword());
			if(updatePassword) {
				updatePasswordOnDataBase(user,hashSenha);
			}
		}
			
		return passwordMatch;
	}

    
    /**
     * M�todo que dever� ser implementado para atualizar a senha criptografada no banco de dados.
     * 
     * @param user - Usu�rio a ser alterado.
     * @param hashSenha - Hash da senha que dever� ser atualizada no banco de dados.
     */
    public static void updatePasswordOnDataBase(User user, String hashSenha) {
	}
    
    

    /**
     * M�todo que dever� ser implementado caso seja necess�rio informa��es extras sobre o usu�rio.
     * 
     * @param user - Usu�rio que ser� autenticado.
     * @param request
     * @param response
     * @return - Usu�rio com as informa��es atualizadas.
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public static User updateUserInfo(User user, HttpServletRequest request, HttpServletResponse response) throws java.io.IOException, javax.servlet.ServletException{
    	if (user instanceof Usuario){				
			Usuario usuario = UsuarioDAO.getInstance().load((Usuario)user);

			// N�o permite acessar o sistema se o usu�rio estiver bloqueado
			if (usuario.getBloqueado().equals(true)) {					
				request.setAttribute("login_bloqueado", true);
				return null;					
			}else{
				usuario = UsuarioService.getInstance().carregaUsuario(usuario);
				user = usuario;
				// Cria o log de registro do login.
				SessaoService.getInstance().makeLogin(usuario);
				if(SinedUtil.isUserHasAction(usuario, "PEDIDO_VENDA_OFFLINE")){
					UsuarioOfflineJSON userOff = new UsuarioOfflineJSON();
					userOff.setLogin(usuario.getLogin());
					userOff.setHash( Util.crypto.makeHashMd5(usuario.getLogin()+request.getParameter("password")) );
					userOff.setCdpessoa(usuario.getCdpessoa());
					userOff.setCodigo(usuario.getCdpessoa().toString());
					userOff.setNome(usuario.getNome().toString());
					userOff.setRestricaoclientevendedor(usuario.getRestricaoclientevendedor());
					userOff.setTodosprojetos(usuario.getTodosprojetos());
					request.getSession().setAttribute("usuarioOffline", View.convertToJson(userOff));
				}
			}				
		}
    	return user;
	}
    
}
