<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	 b
<c:choose>
	<c:when test="${tag.dynamicAttributesMap['usehtml5'] == 'TRUE' || tag.dynamicAttributesMap['usehtml5'] == 'true'}">
		<input 	type="number" 
				id="${tag.id}" 
				name="${tag.name}" 
				value="${tag.valueToString}" 
				onKeyDown="return mascara_integer(this,event,${tag.onlyPositiveNumbers})" 
				onKeyPress="if(valida_tecla(this, event, true,true)) {${tag.dynamicAttributesMap['onkeypress']};return true;} else {return false;}" 
				onchange="${tag.reloadOnChangeString}" ${tag.dynamicAttributesToString}/>
	</c:when>
	<c:otherwise>
		<input 	type="text" 
				id="${tag.id}" 
				name="${tag.name}" 
				value="${tag.valueToString}" 
				onKeyDown="return mascara_integer(this,event,${tag.onlyPositiveNumbers})" 
				onKeyPress="if(valida_tecla(this, event, true,true)) {${tag.dynamicAttributesMap['onkeypress']};return true;} else {return false;}" 
				onchange="${tag.reloadOnChangeString}" ${tag.dynamicAttributesToString}/>
	</c:otherwise>
</c:choose>