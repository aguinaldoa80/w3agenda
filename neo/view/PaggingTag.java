/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;

import br.com.linkcom.neo.core.web.NeoWeb;

/**
 * @author rogelgarcia
 * @since 08/02/2006
 * @version 1.1
 */
public class PaggingTag extends BaseTag {

	protected Integer currentPage;
	protected Integer totalNumberOfPages;
	protected String parameters;
	
	protected String selectedClass;
	protected String unselectedClass;
	
	public static void main(String[] args) throws Exception {
		PaggingTag paggingTag = new PaggingTag(){

			@Override
			protected JspWriter getOut() {
				return new JspWriter(100, true){

					@Override
					public void clear() throws IOException {
						
						
					}

					@Override
					public void clearBuffer() throws IOException {
						
						
					}

					@Override
					public void close() throws IOException {
						
						
					}

					@Override
					public void flush() throws IOException {
						
						
					}

					@Override
					public int getRemaining() {
						
						return 0;
					}

					@Override
					public void newLine() throws IOException {
						
						
					}

					@Override
					public void print(boolean arg0) throws IOException {
						
						
					}

					@Override
					public void print(char arg0) throws IOException {
						
						
					}

					@Override
					public void print(int arg0) throws IOException {
						
						
					}

					@Override
					public void print(long arg0) throws IOException {
						
						
					}

					@Override
					public void print(float arg0) throws IOException {
						
						
					}

					@Override
					public void print(double arg0) throws IOException {
												
					}

					@Override
					public void print(char[] arg0) throws IOException {
												
					}

					@Override
					public void print(String arg0) throws IOException {
//						System.out.print(arg0);
						
					}

					@Override
					public void print(Object arg0) throws IOException {
												
					}

					@Override
					public void println() throws IOException {
						
						
					}

					@Override
					public void println(boolean arg0) throws IOException {
						
						
					}

					@Override
					public void println(char arg0) throws IOException {
						
						
					}

					@Override
					public void println(int arg0) throws IOException {
						
						
					}

					@Override
					public void println(long arg0) throws IOException {
						
						
					}

					@Override
					public void println(float arg0) throws IOException {
						
						
					}

					@Override
					public void println(double arg0) throws IOException {
						
						
					}

					@Override
					public void println(char[] arg0) throws IOException {
						
						
					}

					@Override
					public void println(String arg0) throws IOException {
//						System.out.println(arg0);
						
					}

					@Override
					public void println(Object arg0) throws IOException {
						
						
					}

					@Override
					public void write(char[] cbuf, int off, int len) throws IOException {
						
						
					}
					
				};
			}
			
		};
		paggingTag.currentPage = 0;
		paggingTag.totalNumberOfPages = 1;
		paggingTag.doComponent();
	}
	
	@Override
	protected void doComponent() throws Exception {
		int start = Math.max(1, currentPage+1 - 2) - 1;
		boolean start3pontos = start != 0;
		int fim = Math.min( 5 - (currentPage - start) + currentPage , totalNumberOfPages);
		boolean fim3pontos = fim < totalNumberOfPages;//fim nao � incluido
		if(start3pontos)getOut().print("...&nbsp;");
		for (int i = start; i < fim; i++) {
			if(i == currentPage){
				String cs = selectedClass != null? " class=\""+selectedClass +"\"": "";
				getOut().print("<span"+cs+">"+(i+1)+"</span> ");
			} else {
				String cs = unselectedClass != null? " class=\""+unselectedClass +"\"": "";
				getOut().print("<a href=\""+getRequest().getContextPath()+NeoWeb.getRequestContext().getRequestQuery()+"?currentPage="+i+getParameters()+"\" "+cs+">"+(i+1)+"</a> ");
			}
			//codigo de teste
//			if(i==currentPage){
//				System.out.println(">"+(i+1));
//			} else {
//				System.out.println((i+1));
//			}
		}
		if(fim3pontos)getOut().print("...&nbsp;");
	}
	
	public Integer getCurrentPage() {
		return currentPage;
	}
	public String getSelectedClass() {
		return selectedClass;
	}
	public Integer getTotalNumberOfPages() {
		return totalNumberOfPages;
	}
	public String getUnselectedClass() {
		return unselectedClass;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public void setSelectedClass(String selectedClass) {
		this.selectedClass = selectedClass;
	}
	public void setTotalNumberOfPages(Integer totalNumberOfPages) {
		this.totalNumberOfPages = totalNumberOfPages;
	}
	public void setUnselectedClass(String unselectedClass) {
		this.unselectedClass = unselectedClass;
	}

	public String getParameters() {
		String parameter = "";
		
		try {
			String listarQantidade = NeoWeb.getRequestContext().getParameter("listarQantidade");
			
			if(listarQantidade != null){
				String paramEmpresa = NeoWeb.getRequestContext().getParameter("cdempresaFiltroVenda");
				String paramLocal = NeoWeb.getRequestContext().getParameter("cdLocalFiltro");
				String projetoPedido = NeoWeb.getRequestContext().getParameter("cdProjeto");
				
				if(paramEmpresa != null){
					parameter += "&cdempresaFiltroVenda="+ paramEmpresa;
				}
				if(paramLocal != null){
					parameter += "&cdLocalFiltro="+ paramLocal;
				}
				if(projetoPedido != null){
					parameter += "&cdProjeto="+ projetoPedido;
				}
				parameter += "&listarQantidade="+ listarQantidade;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (parameter != null && !"".equals(parameter)){
			parameters += parameter;
		}
		if(parameters == null){
			return "";
		}
		return "&"+parameters.replaceAll(";", "&");
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
}
