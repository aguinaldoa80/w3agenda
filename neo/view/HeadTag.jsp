<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<%
request.setAttribute("app", request.getContextPath());
%>

<script language="JavaScript" src="${app}/resource/js/ajax.js"></script>
<script language="JavaScript" src="${app}/resource/js/input.js?v=4"></script>
<script language="JavaScript" src="${app}/resource/js/validate.js"></script>

<c:if test="${tag.includeUtilJs}">
<script language="JavaScript" src="${app}/resource/js/util.js"></script>
</c:if>
<script language="JavaScript" src="${app}/resource/js/dynatable.js?versao=1"></script>
<script language="JavaScript" src="${app}/resource/js/JSCookMenu.js"></script>

<c:if test="${includeAutocomplete}">
	<script language="JavaScript">
		var contextoAutoComplete = '${app}';
	</script>

	<script language="JavaScript" src="${app}/resource/js/autocomplete/jquery.js"></script>	
	<script language="JavaScript" src="${app}/resource/js/autocomplete/jquery.ajaxQueue.js"></script>	
	<script language="JavaScript" src="${app}/resource/js/autocomplete/jquery.bgiframe.min.js"></script>	
	<script language="JavaScript" src="${app}/resource/js/autocomplete/thickbox-compressed.js"></script>	
	<script language="JavaScript" src="${app}/resource/js/autocomplete/jquery.autocomplete.js"></script>	
	<script language="JavaScript" src="${app}/resource/js/autocomplete/neo.autocomplete.js"></script>	
	
	<link rel="stylesheet" type="text/css" href="${app}/resource/js/autocomplete/thickbox.css" />
	<link rel="stylesheet" type="text/css" href="${app}/resource/js/autocomplete/jquery.autocomplete.css" />
	
	
</c:if>

<%-- DEFAULT CSS --%>
<c:if test="${tag.includeDefaultCss}">
<link rel="StyleSheet"        href="${app}/resource/css/default.css" type="text/css">	
</c:if>

<%-- CSS JS DA APLICA��O --%>
<c:if test="${searchCssDir == true}">
	<c:forEach items="${csss}" var="css">
	<link rel="StyleSheet"        href="${app}${css}" type="text/css">	
	</c:forEach>
</c:if>

<c:if test="${searchJsDir == true}">
	<c:forEach items="${jss}" var="js">
	<script language="JavaScript" src="${app}${js}"></script>	
	</c:forEach>
</c:if>

<%-- INICIALIZA��O DO MENU --%>
<script language="JavaScript">
	//menu
	var cmThemeOfficeBase = '${app}/resource/menu/';
	var tb_pathToImage = "${app}/resource/js/autocomplete/loadingAnimation.gif";
</script>

<%-- MENU --%>
<c:if test="${tag.includeThemeCss}">
<link rel="StyleSheet"        href="${app}/resource/menu/theme.css" type="text/css">
</c:if>
<script language="JavaScript" src="${app}/resource/menu/theme.js"></script>



<%-- INICIALIZA��O DO HTMLAREA --%>
<script language="JavaScript">
	//htmlarea
	try {
		preparaHtmlArea('${app}/resource/htmlarea/');	
	} catch(e){}// se n�o conseguiu achar o javascript n�o dar pau
</script>

<%-- Personaliza��o do tema do W3erp--%>
<script>
		if (typeof DataGridUtil != 'undefined'){
			DataGridUtil.prototype.bgColorOver = "#9FB6CD";
			DataGridUtil.prototype.bgColorActive = "#9FB6CDB3";
		}
</script>