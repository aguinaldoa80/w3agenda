<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%request.setAttribute("app", request.getContextPath());%>

<span id="spanAutocomplete_${tag.name}" style="white-space: nowrap;">
	<input type="text" 
	name="${tag.name}_label" 
	funcao="autocomplete" 
	getterLabel="${tag.autocompleteGetterLabel}"
	propertyLabel="${tag.autocompleteLabelProperty}"
	propertyMatch="${tag.autocompleteMatchProperty}"
	loadFunctionAutocomplete="${tag.loadFunctionAutocomplete}" 
	beanName="${tag.beanName}"
	value="${tag.autocompleteDescriptionLabel}"
	<%-- local="${tag.local}"
	formatResult="${tag.formatResult }"
	formatItem="${tag.formatItem}"
	valueField="${tag.valueField}"
	data="${tag.data}"--%>  
	${tag.dynamicAttributesToString}
	/>
	<input type="hidden" autocompleteId="${tag.name}_value" name="${tag.name}" value="${tag.autocompleteIdWithDescription}"/>
	
	<c:if test="${!consultar}">
		<c:choose>
			<c:when test="${tag.autocompleteIdWithDescription == '<null>'}">
				<span autocompleteId="${tag.name}_NaoSelecionado" style="">
					<i class="fas fa-exclamation-circle" onmouseover="Tip('N�o selecionado')"></i>
					<i class="fas fa-trash-alt" onmouseover="Tip('N�o selecionado')"></i>
				</span>
				
				<span autocompleteId="${tag.name}_Selecionado" style="display: none;">
					<i class="fas fa-check-circle" onmouseover="Tip('Selecionado')"></i>
					<a href="javascript:excluirAutocomplete('${tag.name}')" onmouseover="Tip('Remover valor')"><i class="fas fa-trash-alt"></i></a>
				</span>
			</c:when>
			<c:otherwise>
				<span autocompleteId="${tag.name}_NaoSelecionado" style="display: none;" onmouseover="Tip('Selecionado')">
					<i class="fas fa-exclamation-circle"></i>
					<i class="fas fa-trash-alt"></i>
				</span>
				
				<span autocompleteId="${tag.name}_Selecionado" style="" onmouseover="Tip('Remover valor')">
					<i class="fas fa-check-circle" onmouseover="Tip('Selecionado')"></i>
					<a href="javascript:excluirAutocomplete('${tag.name}')"><i class="fas fa-trash-alt"></i></a>
				</span>
			</c:otherwise>
		</c:choose>		
		
		<c:if test="${tag.insertPath != null && tag.insertPath != ''}">
			<span style="cursor: pointer;" onclick="${tag.insertOneButtonOnClickAutocomplete}">
				<i class="fas fa-plus-circle" onmouseover="Tip('Novo')"></i>
			</span>
		</c:if>
	</c:if>
</span>

