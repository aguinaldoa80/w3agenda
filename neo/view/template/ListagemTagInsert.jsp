<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>

<n:form validate="false">
	<n:validation>
		<input type="hidden" name="notFirstTime" value="true"/>
		<table class="outterTable" cellspacing="0" cellpadding="0" align="center">
			<tr class="outterTableHeader">
				<td>
					<span class="outterTableHeaderLeft">
						${listagemTag.titulo}
					</span>
					<span class="outterTableHeaderRight">
						${listagemTag.invokeLinkArea}
						
						<c:if test="${listagemTag.showNewLink || !empty listagemTag.linkArea}">
							<n:link onmouseover="Tip(\"Criar\")" action="criar" parameters="INSELECTONE=true&fromInsertOne=true&insertFromSelect=true&closeOnCancel=true" id="btn_novo" checkPermission="true"><i class="fas fa-plus-circle w3green"></i> Novo</n:link>&nbsp;
						</c:if>
						
						<%--
						<c:if test="${listagemTag.showDeleteLink}">
							<n:link url="#" onmouseover="Tip(\"Excluir\")" onclick="javascript:excluirItensSelecionados();" id="btn_excluir"><i class="fas fa-minus-circle w3red"></i>Excluir</n:link><span id="_excluir">&nbsp;</span>
						</c:if>
						 --%>

						<span><a href="javascript:closeWindow();" title="" id="btn_cancelar" onmouseover='Tip("Cancelar")'><i class="fas fa-times-circle w3red"></i> Cancelar</a>&nbsp;</span>

						<n:link id="btn_help" url="javascript:$sinedCtx.openHelp('${listagemTag.taghelp}')" onmouseover="Tip(\"Help\")"><i class="fas fa-question-circle w3darkgray"></i>  Ajuda</n:link>		
					</span>
				</td>
			</tr>
			<tr>
				<td>
					<n:doBody />
				</td>
			</tr>
		</table>
	</n:validation>
</n:form>

<script type="text/javascript">
	function closeWindow() {
		if(confirm('Deseja cancelar a a��o e fechar esta janela?')) window.top.close();
	}
</script>