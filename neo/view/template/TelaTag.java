/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view.template;

import java.io.CharArrayWriter;

import javax.servlet.jsp.tagext.JspFragment;

import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.tag.TagFunctions;

/**
 * @author rogelgarcia
 * @since 03/02/2006
 * @version 1.1
 */
public class TelaTag extends TemplateTag {
	
	protected String taghelp;
	protected boolean includeForm = true;
	protected String formAction;
	protected String formMethod = "POST";
	protected String titulo;
	protected boolean showHelpLink = false;
	protected boolean showTitulo = true;
	protected boolean showBarraSuperior = true;
	protected boolean includeAtalho = true;
	protected JspFragment linkArea;
	protected boolean verifySession = true;
	protected String formEnctype = "multipart/form-data";

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String name) {
		this.titulo = name;
	}
	

	public boolean isIncludeForm() {
		return includeForm;
	}

	public void setIncludeForm(boolean includeForm) {
		this.includeForm = includeForm;
	}
	
	public boolean isShowTitulo() {
		return showTitulo;
	}

	public void setShowTitulo(boolean showTitulo) {
		this.showTitulo = showTitulo;
	}
	
	public boolean isShowBarraSuperior() {
		return showBarraSuperior;
	}
	
	public void setShowBarraSuperior(boolean showBarraSuperior) {
		this.showBarraSuperior = showBarraSuperior;
	}

	@Override
	protected void doComponent() throws Exception {
		if(Util.strings.isEmpty(titulo)){
			titulo = TagFunctions.getTelaDescricao();
			if(Util.strings.isEmpty(titulo))
				titulo = (String) getPageContext().findAttribute("TEMPLATE_beanDisplayName");
		}
		pushAttribute("Ttela", this);
		includeJspTemplate();
		popAttribute("Ttela");
	}

	public String getFormAction() {
		return formAction;
	}
	
	

	public void setFormAction(String formAction) {
		this.formAction = formAction;
	}

	public String getFormMethod() {
		return formMethod;
	}
	
	public String getFormEnctype() {
		return formEnctype;
	}
	
	public void setFormEnctype(String formEnctype) {
		this.formEnctype = formEnctype;
	}

	public void setFormMethod(String formMethod) {
		this.formMethod = formMethod;
	}

	public boolean isShowHelpLink() {
		return showHelpLink;
	}
	
	public void setShowHelpLink(boolean showHelpLink) {
		this.showHelpLink = showHelpLink;
	}

	public String getTaghelp() {
		return taghelp;
	}

	public void setTaghelp(String taghelp) {
		this.taghelp = taghelp;
	}

	public boolean isIncludeAtalho() {
		return includeAtalho;
	}

	public JspFragment getLinkArea() {
		return linkArea;
	}

	public void setIncludeAtalho(boolean includeAtalho) {
		this.includeAtalho = includeAtalho;
	}

	public void setLinkArea(JspFragment linkArea) {
		this.linkArea = linkArea;
	}
	
	public String getInvokeLinkArea(){
		CharArrayWriter charArrayWriter = new CharArrayWriter();
		try {
			if (linkArea != null) {
				linkArea.invoke(charArrayWriter);
			}
		} catch (Exception e) {
			throw new NeoException(e);
		}
		return charArrayWriter.toString();
	}

	public boolean getVerifySession() {
		return verifySession;
	}
	public void setVerifySession(boolean verifySession) {
		this.verifySession = verifySession;
	}
}
