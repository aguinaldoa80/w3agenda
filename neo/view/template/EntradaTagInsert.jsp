<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="n" uri="neo"%>   
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>   
<%@ taglib prefix="s" uri="sined"%>   
 
<c:set var="alteracao" value="${TEMPLATE_beanName}.cdusuarioaltera"/>
<c:set var="dtaltera" value="${TEMPLATE_beanName}.dtaltera"/> 

<n:form validate="false" validateFunction="validarFormulario">
	<n:validation  functionName="validateForm">
		<script language="javascript">
			// caso seja alterada a fun��o validation ela ser� chamada ap�s a validacao do formulario
			var validation;
			function validarFormulario(){
				var valido = validateForm();
				if(validation){
					valido = validation(valido);
				}
				if(valido){
					$.blockUI.defaults.pageMessage = '<h1>Salvando...</h1>';
					$.blockUI();
				}
				return valido;
			}
		</script>
		<c:if test="${consultar}">
			<input type="hidden" name="forcarConsulta" value="true"/>
			<style>input, select, textarea, .required {background-color:#ffffff; color:#000000;}</style>
		</c:if>
		<c:if test="${param.fromInsertOne}">
			<input type="hidden" name="fromInsertOne" value="true"/>
		</c:if>
		<c:if test="${param.insertFromSelect}">
			<input type="hidden" name="insertFromSelect" value="true"/>
		</c:if>
		<c:if test="${!empty param.includeDescriptionFromInsertOne && param.includeDescriptionFromInsertOne}">
			<input type="hidden" name="includeDescriptionFromInsertOne" value="true"/>
		</c:if>
		<table class="outterTable" cellspacing="0" align="center">
			<tr class="outterTableHeader">
				<td colspan="2">
					<span class="outterTableHeaderLeft">
						${entradaTag.titulo}
					</span>
					<span class="outterTableHeaderRight">
						${entradaTag.invokeLinkArea}
					
						<c:if test="${consultar}">
							<c:if test="${entradaTag.showVoltarListagem}">
								<n:link action="listagem" parameters="INSELECTONE=true" id="btn_voltar"  onmouseover="Tip(\"Voltar\")" confirmationScript="function(){$.blockUI('<h1>Carregando...</h1>'); return true; }" checkPermission="true"><i class="fas fa-arrow-left w3darkgray"></i> Retornar � listagem</n:link>&nbsp;
							</c:if>
							<c:if test="${entradaTag.showNewLink}">
								<n:link action="criar" parameters="INSELECTONE=true" onmouseover="Tip(\"Novo\")" id="btn_novo" checkPermission="true"><i class="fas fa-plus-circle w3green"></i> Novo</n:link>&nbsp;
							</c:if>								
							<c:if test="${entradaTag.showEditLink}">
								<span class="btn_editar"><n:link action="editar"  onmouseover="Tip(\"Editar\")" id="btn_editar" editaraction="true" parameters="INSELECTONE=true&${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}=${n:id(n:reevaluate(TEMPLATE_beanName,pageContext))}" checkPermission="true"><i class="fas fa-pencil-alt w3darkgray"></i> Editar</n:link>&nbsp;</span>
							</c:if>
							
							<%-- 
							<n:link action="excluir" id="btn_excluir" onmouseover="Tip(\"Excluir\")" parameters="INSELECTONE=true&${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}=${n:id(n:reevaluate(TEMPLATE_beanName,pageContext))}" confirmationMessage="Voc� tem certeza que deseja excluir este registro?"><i class="fas fa-minus-circle w3red"></i> Excluir</n:link><span id="_excluir">&nbsp;</span>
							--%>
							
							<n:link id="btn_help" url="javascript:$sinedCtx.openHelp('${entradaTag.taghelp}')" onmouseover="Tip(\"Help\")"><i class="fas fa-question-circle w3darkgray"></i>  Ajuda</n:link>
						</c:if>
						<c:if test="${!consultar}">
							<c:if test="${entradaTag.showListagemLink && (empty param.showListagemLink || param.showListagemLink)  && (empty showListagemLink || showListagemLink)}">
								<n:link action="listagem" parameters="INSELECTONE=true" id="btn_voltar" onmouseover="Tip(\"Voltar\")" confirmationScript="function(){$.blockUI('<h1>Carregando...</h1>'); return true; }" confirmationMessage="Deseja retornar � listagem sem salvar as altera��es?" ><i class="fas fa-arrow-left w3darkgray"></i>Retornar � listagem</n:link>&nbsp;
							</c:if>
							<span id="btn_salvar1">
								<n:submit id="btn_gravar1" title="Gravar" action="salvar"
										  parameters="INSELECTONE=true&closeOnCancel=true&showListagemLink=${!empty param.insertFromSelect}&isModal=${(!empty param.isModal && param.isModal) || (!empty isModal && isModal)}&closeOnSave=${!empty param.closeOnSave}&includeDescriptionFromInsertOne=${!empty param.includeDescriptionFromInsertOne && param.includeDescriptionFromInsertOne}"
										  validate="true"
										  confirmationScript="${entradaTag.submitConfirmationScript}"
										  onmouseover="Tip(\"Salvar\")"><i class="fas fa-save w3green"></i> Salvar
							 	</n:submit>
						  </span>&nbsp;
							
							<c:choose>
								<c:when test="${(param.ACAO == 'editar' || ACAO == 'salvar') && !(closeOnCancel || param.closeOnCancel)}">
									<n:link action="consultar" id="btn_cancelar"  onmouseover="Tip(\"Cancelar\")" parameters="INSELECTONE=true&${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}=${n:id(n:reevaluate(TEMPLATE_beanName,pageContext))}" confirmationMessage="Deseja retornar � consulta sem salvar as altera��es?" checkPermission="true"><i class="fas fa-times-circle w3red"></i> Cancelar</n:link>&nbsp;
								</c:when>
								<c:otherwise>
									<c:if test="${closeOnCancel || param.closeOnCancel}">
										<span><a href="javascript:closeWindow();" title="" id="btn_cancelar" onmouseover='Tip("Cancelar")'><i class="fas fa-times-circle w3red"></i> Cancelar</a>&nbsp;</span>
									</c:if>
								</c:otherwise>
							</c:choose>
							
							
							<n:link id="btn_help" url="javascript:$s.openHelp('${entradaTag.taghelp}')" onmouseover="Tip(\"Help\")"><i class="fas fa-question-circle w3darkgray"></i>  Ajuda</n:link>
						</c:if>
					</span>				
				</td>
			</tr>
			<tr>
				<td class="tableBody" colspan="2">
					<n:bean name="${TEMPLATE_beanName}">
					<n:doBody />
					</n:bean>
				</td>
			</tr>
			<tr class="outterTableFooter">
				<td class="log">
					<c:if test="${n:reevaluate(alteracao,pageContext) != null }">
						<n:panel><br><br>�ltima altera��o: ${s:formatadata(n:reevaluate(dtaltera,pageContext))} Por: ${s:finduserbycd(n:reevaluate(alteracao,pageContext))} </n:panel>
					</c:if>
				</td>
				<TD>
					<span class="outterTableFooterRight">
						${entradaTag.invokeLinkArea}			
					
						<c:if test="${consultar}">
							<c:if test="${entradaTag.showVoltarListagem}">
								<n:link action="listagem" parameters="INSELECTONE=true" id="btn_voltar"  onmouseover="Tip(\"Voltar\")" confirmationScript="function(){$.blockUI('<h1>Carregando...</h1>'); return true; }" checkPermission="true"><i class="fas fa-arrow-left w3darkgray"></i>Retornar � listagem</n:link>&nbsp;
							</c:if>
							<c:if test="${entradaTag.showNewLink}">
								<n:link action="criar" parameters="INSELECTONE=true" onmouseover="Tip(\"Novo\")" id="btn_novo" checkPermission="true"><i class="fas fa-plus-circle w3green"></i> Novo</n:link>&nbsp;
							</c:if>								
							<c:if test="${entradaTag.showEditLink}">
								<span class="btn_editar"><n:link action="editar"  onmouseover="Tip(\"Editar\")" id="btn_editar" editaraction="true" parameters="INSELECTONE=true&${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}=${n:id(n:reevaluate(TEMPLATE_beanName,pageContext))}" checkPermission="true"><i class="fas fa-pencil-alt w3darkgray"></i> Editar</n:link>&nbsp;</span>
							</c:if>
							
							<%-- 
							<n:link action="excluir" id="btn_excluir" onmouseover="Tip(\"Excluir\")" parameters="INSELECTONE=true&${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}=${n:id(n:reevaluate(TEMPLATE_beanName,pageContext))}" confirmationMessage="Voc� tem certeza que deseja excluir este registro?"><i class="fas fa-minus-circle w3red"></i> Excluir</n:link><span id="_excluir">&nbsp;</span>
							--%>
							
							<n:link id="btn_help" url="javascript:$sinedCtx.openHelp('${entradaTag.taghelp}')" onmouseover="Tip(\"Help\")"><i class="fas fa-question-circle w3darkgray"></i> Ajuda</n:link>
						</c:if>
						<c:if test="${!consultar}">
							<c:if test="${entradaTag.showListagemLink && (empty param.showListagemLink || param.showListagemLink)  && (empty showListagemLink || showListagemLink)}">
								<n:link action="listagem" parameters="INSELECTONE=true" id="btn_voltar" onmouseover="Tip(\"Voltar\")" confirmationScript="function(){$.blockUI('<h1>Carregando...</h1>'); return true; }" confirmationMessage="Deseja retornar � listagem sem salvar as altera��es?" ><i class="fas fa-arrow-left w3darkgray"></i>Retornar � listagem</n:link>&nbsp;
							</c:if>					
							<span id="btn_salvar2">
								<n:submit id="btn_gravar2" title="Gravar" action="salvar" 
										  parameters="INSELECTONE=true&closeOnCancel=true&showListagemLink=${!empty param.insertFromSelect}&isModal=${(!empty param.isModal && param.isModal) || (!empty isModal && isModal)}&closeOnSave=${!empty param.closeOnSave}"
										  validate="true"
										  confirmationScript="${entradaTag.submitConfirmationScript}"
										  onmouseover="Tip(\"Salvar\")"><i class="fas fa-save w3green"></i> Salvar
								</n:submit>
							</span>&nbsp;
														
							<c:choose>
								<c:when test="${(param.ACAO == 'editar' || ACAO == 'salvar') && !(closeOnCancel || param.closeOnCancel)}">
									<n:link action="consultar" id="btn_cancelar"  onmouseover="Tip(\"Cancelar\")" parameters="${n:idProperty(n:reevaluate(TEMPLATE_beanName,pageContext))}=${n:id(n:reevaluate(TEMPLATE_beanName,pageContext))}" confirmationMessage="Deseja retornar � consulta sem salvar as altera��es?" checkPermission="true"><i class="fas fa-times-circle w3red"></i> Cancelar</n:link>&nbsp;
								</c:when>
								<c:otherwise>
									<c:if test="${closeOnCancel || param.closeOnCancel}">
										<a href="javascript:closeWindow();" title="" id="btn_cancelar" onmouseover='Tip("Cancelar")'><i class="fas fa-times-circle w3red"></i> Cancelar</a>&nbsp;
									</c:if>
								</c:otherwise>
							</c:choose>
							
							
							<n:link id="btn_help" url="javascript:$s.openHelp('${entradaTag.taghelp}')" onmouseover="Tip(\"Help\")"><i class="fas fa-question-circle w3darkgray"></i> Ajuda</n:link>
						</c:if>					
						<script>
							function alertExclude(){
								confirm("Voc� tem certeza que deseja excluir este registro?");
							}
							
							function alertCancel(){
								return confirm("Deseja retornar � consulta sem salvar as altera��es?");
							}
							
							function closeWindow() {
								if(confirm('Deseja cancelar a a��o e fechar esta janela?')) {
									if (isModal()) {
										parent.$.akModalRemove(true);
										parent.document.location.reload();
									} else {
										window.top.close();
									}
								}
							}
							
							function isModal() {
								if (${empty isModal && empty param.isModal}) {
									return false;
								}
								
								if (${!empty isModal})
									return ${isModal};
									
								if (${!empty param.isModal})
									return ${param.isModal};
								
								return false;
							}
						</script>				
					</span>	
				</TD>
			</tr>
		</table>
		</n:validation>
</n:form>