<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:choose>
	<c:when test="${INSELECTONE || param.INSELECTONE}">
		<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/ListagemTagInsert.jsp" />
	</c:when>
	<c:otherwise>
		<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/ListagemTagDefault.jsp" />
	</c:otherwise>
</c:choose>

<c:if test="${!tag.showDeleteLink}">
	<script>
		$("#btn_excluir").hide();
		$("#_excluir").hide();
	</script>
</c:if>