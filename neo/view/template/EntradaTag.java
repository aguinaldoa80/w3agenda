/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view.template;
 
import java.io.CharArrayWriter;

import javax.servlet.jsp.tagext.JspFragment;

import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.sined.util.tag.TagFunctions;

/**
 * @author rogelgarcia
 * @since 03/02/2006
 * @version 1.1
 */
public class EntradaTag extends TemplateTag {

	protected String taghelp;
	protected String titulo;
	protected boolean showListagemLink = true;
	protected boolean showDeleteLink = true;
	protected boolean showNewLink = true;
	protected boolean showEditLink = true;
	protected boolean showVoltarListagem = true;
	protected boolean showCancelLink = true;
	protected String submitConfirmationScript;
	protected JspFragment linkArea;
	protected boolean includeAtalho = false;


	protected boolean showSaveLink = true;
	
	public JspFragment getLinkArea() {
		return linkArea;
	}

	public void setLinkArea(JspFragment linkArea) {
		this.linkArea = linkArea;
	}

	public boolean isShowListagemLink() {
		return showListagemLink;
	}

	public void setShowListagemLink(boolean showListagemLink) {
		this.showListagemLink = showListagemLink;
	}

	public boolean isShowDeleteLink() {
		return showDeleteLink;
	}
	
	public void setShowDeleteLink(boolean showDeleteLink) {
		this.showDeleteLink = showDeleteLink;
	}
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getSubmitConfirmationScript() {
		return submitConfirmationScript;
	}
	
	public void setSubmitConfirmationScript(String submitConfirmationScript) {
		this.submitConfirmationScript = submitConfirmationScript;
	}
	
	@Override
	protected void doComponent() throws Exception {
		if(Util.strings.isEmpty(titulo)){
			titulo = TagFunctions.getTelaDescricao();
			if(Util.strings.isEmpty(titulo))
				titulo = (String) getPageContext().findAttribute("TEMPLATE_beanDisplayName");
		}
		pushAttribute("entradaTag", this);
		includeJspTemplate();
		popAttribute("entradaTag");
	}
	
	/**
	 * m�todo para ser chamado do template
	 * @return
	 */
	public String getInvokeLinkArea(){
		CharArrayWriter charArrayWriter = new CharArrayWriter();
		try {
			if (linkArea != null) {
				linkArea.invoke(charArrayWriter);
			}
		} catch (Exception e) {
			throw new NeoException(e);
		}
		return charArrayWriter.toString();
	}

	public boolean isShowEditLink() {
		return showEditLink;
	}
	public boolean isShowVoltarListagem() {
		return showVoltarListagem;
	}
	public boolean isShowCancelLink() {
		return showCancelLink;
	}

	public void setShowVoltarListagem(boolean showVoltarListagem) {
		this.showVoltarListagem = showVoltarListagem;
	}
	public void setShowEditLink(boolean showEditLink) {
		this.showEditLink = showEditLink;
	}
	public void setShowCancelLink(boolean showCancelLink) {
		this.showCancelLink = showCancelLink;
	}

	public boolean isShowNewLink() {
		return showNewLink;
	}

	public void setShowNewLink(boolean showNewLink) {
		this.showNewLink = showNewLink;
	}
	
	public boolean isShowSaveLink() {
		return showSaveLink;
	}
	
	public void setShowSaveLink(boolean showSaveLink) {
		this.showSaveLink = showSaveLink;
	}

	public String getTaghelp() {
		return taghelp;
	}

	public void setTaghelp(String taghelp) {
		this.taghelp = taghelp;
	}

	public boolean isIncludeAtalho() {
		return includeAtalho;
	}
	public void setIncludeAtalho(boolean includeAtalho) {
		this.includeAtalho = includeAtalho;
	}
}
