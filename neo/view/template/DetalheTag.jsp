<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<n:panel title="${Tdetalhe.detailDysplayName}">
	<div class="detailBlock">
	<n:dataGrid itens="${Tdetalhe.itens}" cellspacing="0" dynaLine="true" id="${Tdetalhe.tableId}"  var="${Tdetalhe.detailVar}">
		<n:bean name="${Tdetalhe.detailVar}" valueType="${Tdetalhe.detailClass}" propertyPrefix="${Tdetalhe.fullNestedName}" propertyIndex="${index}">
			<n:getContent tagName="acaoTag" vars="acoes">
				<t:propertyConfig mode="input" renderAs="column" disabled="${consultar}">
					<n:doBody/>
				</t:propertyConfig>
				<c:if test="${!consultar}">
					<c:if test="${Tdetalhe.showColunaAcao}">
					<n:column header=" " style="width: 1%; padding-right: 3px;">
						${acoes}
						<c:if test="${Tdetalhe.showBotaoRemover}">
							<c:if test="${!empty Tdetalhe.beforeDeleteLine}">
								<c:set var="beforeDeleteLine">
									${Tdetalhe.beforeDeleteLine}(this.id,'${Tdetalhe.tableId}');
								</c:set>
							</c:if>
							
							<c:if test="${!propertyConfigDisabled || dataGridDynaline}">					
								<button type="button" class="excluirdetalhe" onmouseover="Tip('Remover item', CLICKCLOSE, true)" onclick="${Tdetalhe.confirmDeleteStart}${beforeDeleteLine}excluirLinhaPorNome(this.id,true);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true);reindexContagerencial(this.id, '${Tdetalhe.fullNestedName}');${Tdetalhe.afterDeleteLine}${Tdetalhe.confirmDeleteEnd}" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]" >
									<i class="fas fa-ban"></i>
								</button>
							</c:if>
							<c:if test="${propertyConfigDisabled && !dataGridDynaline}">
								<button type="button" class="excluirdetalhe" onmouseover="Tip('Remover item', CLICKCLOSE, true)" onclick="${Tdetalhe.confirmDeleteStart}${beforeDeleteLine}excluirLinhaPorNome(this.id,true);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true);reindexContagerencial(this.id, '${Tdetalhe.fullNestedName}');${Tdetalhe.afterDeleteLine}${Tdetalhe.confirmDeleteEnd}" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]" >
									<i class="fas fa-ban"></i>
								</button>						
							</c:if>					
						</c:if>
					</n:column>
					</c:if>
				</c:if>	
			</n:getContent>
		</n:bean>
	</n:dataGrid>
	<c:if test="${!consultar}">
		<div class="lineNovalinha">
			<c:if test="${Tdetalhe.showBotaoNovaLinha}">
				<c:if test="${empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
					<c:set value="Nova linha" scope="page" var="labelnovalinha"/>
				</c:if>
				<c:if test="${!empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
					<c:set value="${Tdetalhe.dynamicAttributesMap['labelnovalinha']}" scope="page" var="labelnovalinha"/>
				</c:if>
				
				<c:if test="${!propertyConfigDisabled}">
					<button type="button" id="${Tdetalhe.name}_btnNewline" class="btnApp" onclick="newLine${Tdetalhe.tableId}();if(typeof $CampoObrigatorio != 'undefined'){$CampoObrigatorio.configureRequiredFields();}$s.loadMask();applyAutocomplete();applyContagerencial();applyCategoria();applyContacontabil();${Tdetalhe.onNewLine}">
						${labelnovalinha}
					</button>
				</c:if>
				<c:if test="${propertyConfigDisabled}">
					
					<button type="button" id="${Tdetalhe.name}_btnNewline" class="btnApp" disabled="disabled" onclick="newLine${Tdetalhe.tableId}();${Tdetalhe.onNewLine}">
						${labelnovalinha}
					</button>
				</c:if>
			</c:if>
		</div>
	</c:if>
	</div>
</n:panel>
