<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>


<n:dataGrid itens="${TtabelaResultados.itens}" var="${TtabelaResultados.name}" width="100%" cellspacing="0" 
	rowondblclick="javascript:$dg.editarRegistro(this)"
	rowonclick="javascript:$dg.coloreLinha('tabelaResultados',this)" 
	rowonmouseover="javascript:$dg.mouseonOverTabela('tabelaResultados',this)" 
	rowonmouseout="javascript:$dg.mouseonOutTabela('tabelaResultados',this)" 
	id="tabelaResultados" varIndex="index">
	
	<n:bean name="${TtabelaResultados.name}" valueType="${TtabelaResultados.valueType}">
		<n:getContent tagName="acaoTag" vars="acoes">
			<c:if test="${TtabelaResultados.showExcluirLink}">
				<n:column>
					<n:header style="width: 1%;"><input type="checkbox" class="checkBoxClass" name="selectAll" id="selectAll" onclick="javascript:$dg.changeCheckState();"></n:header>
					<n:body><input class="checkBoxClass" type="checkbox" name="selecteditens" value="${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}"></n:body>
				</n:column>
			</c:if>
			<t:propertyConfig mode="output" renderAs="column">
				<n:doBody />
			</t:propertyConfig>
			<c:if test="${(!empty acoes) || (TtabelaResultados.showEditarLink) || (TtabelaResultados.showExcluirLink) || (TtabelaResultados.showConsultarLink)}">
				<n:column header=" " style="width: 1%; white-space: nowrap; padding-right: 3px;">
					${acoes}
					<script language="javascript">
						imprimirSelecionar(new Array(${n:hierarchy(TtabelaResultados.valueType)}), "${n:escape(n:valueToString(n:reevaluate(TtabelaResultados.name, pageContext)))}", "${n:escape(n:descriptionToString(n:reevaluate(TtabelaResultados.name, pageContext)))}");
					</script>
					<c:if test="${TtabelaResultados.showConsultarLink}">
						<n:link action="consultar"  onmouseover="Tip(\"Consultar registro\")" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" class="activation">
							<i class="fas fa-search w3darkgray"></i>
						</n:link>
					</c:if>
					<c:if test="${TtabelaResultados.showEditarLink}">
						<n:link action="editar"  onmouseover="Tip(\"Editar registro\")" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" class="activation">
							<i class="fas fa-pencil-alt"></i>
						</n:link>
					</c:if>
				</n:column>
			</c:if>
		</n:getContent>
	</n:bean>
</n:dataGrid>
<table width="100%">
	<c:choose>
		<c:when test="${ignorarPagina��o ==true}">
			<tr>
				<td class="paginacao" align="left">
				</td>
			</tr>
		</c:when>
		<c:when test="${filtro.paginacaoSimples==true}">
			<tr>
				<td class="paginacao" align="left">
					<n:pagging currentPage="${currentPage}" totalNumberOfPages="${numberOfPages}" selectedClass="pageSelected" unselectedClass="pageUnselected" />
				</td>
				<c:if test="${!empty lista}">
					<td align="right"><b>${filtro.currentPage * filtro.pageSize + 1}</b> - <b>${s:gettotalpagepaginacaosimples(filtro, lista)}</b> de <b><span id="spanTotalPaginacao"><n:link action="javascript:calcularTotalRegistrosPaginacao()">Calcular Total</n:link></span></b></td>
				</c:if>
				<c:if test="${empty lista}">
					<td align="right"><b>N�o existem registros!</b></td>		
				</c:if>
			</tr>
		</c:when>
		<c:otherwise>
			<tr>
				<td class="paginacao" align="left">
					<n:pagging currentPage="${currentPage}" totalNumberOfPages="${numberOfPages}" selectedClass="pageSelected" unselectedClass="pageUnselected" />
				</td>
				<c:if test="${!empty lista}">
					<td align="right"><b>${filtro.currentPage * filtro.pageSize + 1}</b> - <b>${s:gettotalpage(filtro)}</b> de <b>${filtro.numberOfResults}</b></td>
				</c:if>
				<c:if test="${empty lista}">
					<td align="right"><b>N�o existem registros!</b></td>		
				</c:if>
			</tr>
		</c:otherwise>
	</c:choose>
</table>
<script>
	<c:if test="${TtabelaResultados.showExcluirLink}">
		function excluirItensSelecionados(){
			if($dg.validateSelectedValues()){
				document.location = '?ACAO=excluir&itenstodelete='+$dg.getSelectedValues();
			}
		}
	</c:if>
	<c:if test="${!TtabelaResultados.showExcluirLink}">
		$("#btn_excluir").hide();
	</c:if>
	
	function calcularTotalRegistrosPaginacao(){
	
		$("#spanTotalPaginacao").html("calculando...");
 		form.ACAO.value = 'calcularTotalRegistrosPaginacaoAJax';
 		
 		var dataFormSerialize = $(form).serialize();
 		
 		if(typeof getParametroCalcularTotalRegistrosPaginacao != 'undefined'){
 			dataFormSerialize += getParametroCalcularTotalRegistrosPaginacao();
 		}
 		
 		jQuery.ajax({
			type: "POST",
			url: "${ctx}${TEMPLATE_requestQuery}",
			contentType:"application/x-www-form-urlencoded; charset=ISO-8859-1",
			data: dataFormSerialize,
			success: function(data){						
						eval(data);
						$("#spanTotalPaginacao").html(totalRegistrosPaginacao);
					},
			dataType: "script"
		});
	}
</script>