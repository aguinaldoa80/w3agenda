<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>
      
<c:set var="alteracao" value="${TEMPLATE_beanName}.cdusuarioaltera"/>
<c:set var="dtaltera" value="${TEMPLATE_beanName}.dtaltera"/> 

<c:choose>
	<c:when test="${INSELECTONE || param.INSELECTONE}">
		<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/EntradaTagInsert.jsp" />
	</c:when> 
	<c:otherwise>  
		<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/EntradaTagDefault.jsp" />
	</c:otherwise>
</c:choose>