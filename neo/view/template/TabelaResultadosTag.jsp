<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>


<c:choose>
	<c:when test="${INSELECTONE}">
		<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/TabelaResultadosTagSelectOne.jsp" />
	</c:when> 
	<c:otherwise>
		<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/TabelaResultadosTagDefault.jsp" />
	</c:otherwise>
</c:choose>