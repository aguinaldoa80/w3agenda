<%@page import="br.com.linkcom.sined.util.SinedUtil"%>
<%@page import="br.com.linkcom.neo.core.web.NeoWeb"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>

<c:if test="${Ttela.includeForm}">
<n:form validate="true" method="${Ttela.formMethod}" action="${Ttela.formAction}" validateFunction="validarFormulario" verifySession="${Ttela.verifySession}" enctype="${Ttela.formEnctype}">
	<n:validation functionName="validateForm">
		<script language="javascript">
			// caso seja alterada a fun��o validation ela ser� chamada ap�s a validacao do formulario
			var validation;
			function validarFormulario(){
				var valido = validateForm();
				if(validation){
					valido = validation(valido);
				}
				return valido;
			}
		</script>
		
		<table class="outterTable" cellspacing="0" align="center">
			<c:if test="${Ttela.showBarraSuperior}">
				<tr class="outterTableHeader">
					<td>
						<c:if test="${!empty Ttela.titulo && Ttela.showTitulo}">
							<span class="outterTableHeaderLeft">${Ttela.titulo}</span>
						</c:if>
						<span class="outterTableHeaderRight" id="helpspan">
							${Ttela.invokeLinkArea} 
							<c:if test="${Ttela.includeAtalho}">
								<c:choose>
									<c:when test="${!s:isAtalhoCadastrado(uriAtalho)}">
										<n:link onmouseover="Tip(\"Adicionar atalho\")" url="#" onclick="javascript:ajaxUpdateAtalho(\"${uriAtalho}\")" id="btnAtalho" class="addAtalho"><i class="fas fa-sort-amount-up w3darkgray"></i>Adicionar atalho</n:link>
									</c:when>
									<c:otherwise>
										<n:link onmouseover="Tip(\"Remover atalho\")" url="#" onclick="javascript:ajaxUpdateAtalho(\"${uriAtalho}\")"  id="btnAtalho" class="remAtalho"><i class="fas fa-minus-circle"></i>Remover atalho</n:link>
									</c:otherwise>
								</c:choose>
							</c:if>
							<span id="helpButton">&nbsp;<n:link id="btn_help" url="javascript:$s.openHelp('${Ttela.taghelp}')" onmouseover="Tip(\"Help\")"><i class="fas fa-question-circle w3darkgray"></i> Ajuda</n:link></span>
						</span>
					</td>
				</tr>
			</c:if>
			<tr>
				<td>
					<n:doBody />
				</td>
			</tr>
		</table>


	</n:validation>
</n:form>
</c:if>

<c:if test="${!Ttela.includeForm}">
	<table class="outterTable" cellspacing="0" align="center">
		<c:if test="${Ttela.showBarraSuperior}">
			<tr class="outterTableHeader">
				<td>
					<c:if test="${!empty Ttela.titulo && Ttela.showTitulo}">
							<span class="outterTableHeaderLeft">${Ttela.titulo}</span>
						</c:if>
					<span class="outterTableHeaderRight" id="helpspan">
						<c:choose>
							<c:when test="${!s:isAtalhoCadastrado(uriAtalho)}">
								<n:link onmouseover="Tip(\"Adicionar atalho\")" url="#" onclick="javascript:ajaxUpdateAtalho(\"${uriAtalho}\")" id="btnAtalho" class="addAtalho"><i class="fas fa-sort-amount-up w3darkgray"></i>Adicionar atalho</n:link>
							</c:when>
							<c:otherwise>
								<n:link onmouseover="Tip(\"Remover atalho\")" url="#" onclick="javascript:ajaxUpdateAtalho(\"${uriAtalho}\")"  id="btnAtalho" class="remAtalho"><i class="fas fa-minus-circle"></i>Remover atalho</n:link>
							</c:otherwise>
						</c:choose>				
						<n:link id="btn_help" url="javascript:$s.openHelp('${Ttela.taghelp}')" onmouseover="Tip(\"Help\")"><span style="font-size: 11px;">Ajuda</span></n:link>
					</span>
				</td>
			</tr>
		</c:if>
		<tr>
			<td>
				<n:doBody />
			</td>
		</tr>
	</table>
</c:if>
