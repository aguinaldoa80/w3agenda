<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>

<c:set var="alteracao" value="${TEMPLATE_beanName}.cdusuarioaltera"/>
<n:panel title="${TtabelaEntrada.title}" colspan="${TtabelaEntrada.colspan}">
	<n:panelGrid columns="${TtabelaEntrada.columns}" style="${TtabelaEntrada.style}" styleClass="${TtabelaEntrada.styleClass}" rowStyleClasses="${TtabelaEntrada.rowStyleClasses}" rowStyles="${TtabelaEntrada.rowStyles}"
		columnStyleClasses="${TtabelaEntrada.columnStyleClasses}" columnStyles="${TtabelaEntrada.columnStyles}" colspan="${TtabelaEntrada.colspan}" propertyRenderAsDouble="${TtabelaEntrada.propertyRenderAsDouble}"
		dynamicAttributesMap="${TtabelaEntrada.dynamicAttributesMap}" cellpadding="1" cellspacing="0">

		<t:propertyConfig mode="input" showLabel="${tag.propertyShowLabel}" disabled="${consultar}">
			<n:doBody />
		</t:propertyConfig>
		
	</n:panelGrid>
</n:panel>