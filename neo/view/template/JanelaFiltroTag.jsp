<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<n:bean name="${tag.name}">
	<table width="100%" align="center" class="window inputWindow janelaFiltroW3"  cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<n:doBody />
			</td>
		</tr>
	</table>
</n:bean>