<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>

<div id="filtroTitle" class="janelaFiltro">
	<span class="selectContainer" style="float: left;">
		<select name="__filtroSalvo__" style="width: 280px; " id="__filtroSalvo__" onchange="$Neo.filtro.onChangeCombo(this)">
			<option value="<null>">Selecione um filtro...</option>
			<c:if test="${not empty listaFiltrosIndividuais}">
				<optgroup label="Meus Filtros">
					<c:forEach items="${listaFiltrosIndividuais}" var="itemFiltro">
						<option data-individual="true" value="br.com.linkcom.sined.geral.bean.Filtro[cdfiltro=${itemFiltro.cdfiltro}]" ${itemFiltro.cdfiltro==__filtroSelecionado__.cdfiltro ? "selected" : ""}> ${itemFiltro.nome}</option>
					</c:forEach>
				</optgroup>
			</c:if>
			<c:if test="${not empty listaFiltrosCompartilhados}">
				<optgroup label="Compartilhados comigo">
					<c:forEach items="${listaFiltrosCompartilhados}" var="itemFiltro">
						<option data-compartilhado="true" value="br.com.linkcom.sined.geral.bean.Filtro[cdfiltro=${itemFiltro.cdfiltro}]" ${itemFiltro.cdfiltro==__filtroSelecionado__.cdfiltro ? "selected" : ""}> ${itemFiltro.nome}</option>
					</c:forEach>
				</optgroup>
			</c:if>
		</select>
		<input type="hidden" name="usarFiltroSalvo" value="false" />
		<span id="novoFiltro" style="display:none;">
			<i class="fas fa-plus-circle w3iconlink" onmouseover="Tip('Novo filtro')" onclick="$Neo.filtro.new_();"></i>
		</span>
		<span id="editarFiltro" style="display:none;">
			<i class="fas fa-pencil-alt w3iconlink" onmouseover="Tip('Editar Filtro')" onclick="$Neo.filtro.edit();"></i>	
		</span>
		<span id="salvarFiltro" style="${!existeFiltroSelecionadoValido || podeSalvarOuExcluirFiltro ? '':'display:none;'}">
			<i class="fas fa-save w3iconlink" onmouseover="Tip('Salvar Filtro')" onclick="$Neo.filtro.save();"></i>
		</span>
		<span id="salvarFiltroNovo" style="${existeFiltroSelecionadoValido  ? '':'display:none;'}">
			<i class="far fa-save w3iconlink" onmouseover="Tip('Salvar como um novo Filtro')" onclick="$Neo.filtro.saveNew();"></i>
		</span>
		<span id="cancelarFiltro" style="display:none">
			<i class="fas fa-times-circle w3iconlink" onmouseover="Tip('Cancelar')" onclick="$Neo.filtro.cancel();"></i>
		</span>
		<span id="excluirFiltro" style="${existeFiltroSelecionadoValido && podeSalvarOuExcluirFiltro ? '':'display:none;'}">
			<i class="fas fa-minus-circle w3iconlink" onmouseover="Tip('Excluir Filtro')" onclick="$Neo.filtro.delete_();"></i>
		</span>
		<span id="expandirFiltro">
			<n:link url="javascript:$Neo.filtro.expand()" onmouseover="Tip(\"Expande o filtro para exibir os campos de pesquisa.\")" id="btn_expandirFiltro">
				<i class="fas fa-arrow-down"></i> Expandir Filtro
			</n:link>
		</span>
	</span>
</div>


