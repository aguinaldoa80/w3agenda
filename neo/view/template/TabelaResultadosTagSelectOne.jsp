<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>     
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>


<n:dataGrid itens="${TtabelaResultados.itens}" var="${TtabelaResultados.name}" width="100%" cellspacing="0" 
	rowondblclick="javascript:$dg.editarRegistro(this)"
	rowonclick="javascript:$dg.coloreLinha('tabelaResultados',this)" 
	rowonmouseover="javascript:$dg.mouseonOverTabela('tabelaResultados',this)" 
	rowonmouseout="javascript:$dg.mouseonOutTabela('tabelaResultados',this)" 
	id="tabelaResultados" varIndex="index">
	
	<n:bean name="${TtabelaResultados.name}" valueType="${TtabelaResultados.valueType}">
		<n:getContent tagName="acaoTag" vars="acoes">
			<c:if test="${TtabelaResultados.showExcluirLink}">
				<n:column>
					<n:header style="width: 1%;"><input type="checkbox" class="checkBoxClass" name="selectAll" id="selectAll" onclick="javascript:$dg.changeCheckState();"></n:header>
					<n:body><input class="checkBoxClass" type="checkbox" name="selecteditens" value="${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}"></n:body>
				</n:column>
			</c:if>
			<t:propertyConfig mode="output" renderAs="column">
				<n:doBody />
			</t:propertyConfig>
				<n:column header=" " style="width: 1%; white-space: nowrap; padding-right: 3px;">
					${acoes}
					<c:if test="${TtabelaResultados.showSelecionarLink}">
						<a href="javascript:selecionar(addEscape('${n:escape(n:valueToString(n:reevaluate(TtabelaResultados.name, pageContext)))}'),addEscape('${n:escape(n:descriptionToString(n:reevaluate(TtabelaResultados.name, pageContext)))}'))">selecionar</a>
					</c:if>
					<c:if test="${TtabelaResultados.showConsultarLink}">
						<n:link action="consultar"  onmouseover="Tip(\"Consultar registro\")" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}&INSELECTONE=true" class="activation">
							<i class="fas fa-search w3darkgray"></i>
						</n:link>
					</c:if>
					<c:if test="${TtabelaResultados.showEditarLink}">
						<n:link action="editar"  onmouseover="Tip(\"Editar registro\")" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}&INSELECTONE=true">
							<img src="${ctx}/imagens/editar.gif" border="0"/>
						</n:link>
					</c:if>
				</n:column>
		</n:getContent>
	</n:bean>
</n:dataGrid>
<table width="100%">
	<c:choose>
		<c:when test="${ignorarPagina��o ==true}">
			<tr>
				<td class="paginacao" align="left">
				</td>
			</tr>
		</c:when>
		<c:when test="${filtro.paginacaoSimples==true}">
			<tr>
				<td class="paginacao" align="left">
					<n:pagging currentPage="${currentPage}" totalNumberOfPages="${numberOfPages}" selectedClass="pageSelected" unselectedClass="pageUnselected" parameters="${param.chequePopup == 'true' ? 'clearBase=true&isPopup=true&INSELECTONE=true&closeOnCancel=false&chequePopup=true' : (param.isPopup == 'true' && param.resgatarPneuPopUp == 'true' ? 'clearBase=true&isPopup=true&resgatarPneuPopUp=true&INSELECTONE=true&closeOnCancel=false' : '')}"/>
				</td>
				<c:if test="${!empty lista}">
					<td align="right"><b>${filtro.currentPage * filtro.pageSize + 1}</b> - <b>${s:gettotalpagepaginacaosimples(filtro, lista)}</b> de <b><span id="spanTotalPaginacao"><n:link action="javascript:calcularTotalRegistrosPaginacao()">Calcular Total</n:link></span></b></td>
				</c:if>
				<c:if test="${empty lista}">
					<td align="right"><b>N�o existem registros!</b></td>		
				</c:if>
			</tr>
		</c:when>
		<c:otherwise>
			<tr>
				<td class="paginacao" align="left">
					<n:pagging currentPage="${currentPage}" totalNumberOfPages="${numberOfPages}" selectedClass="pageSelected" unselectedClass="pageUnselected" parameters="${param.chequePopup == 'true' ? 'clearBase=true&isPopup=true&INSELECTONE=true&closeOnCancel=false&chequePopup=true' : (param.isPopup == 'true' && param.resgatarPneu == 'true' ? 'clearBase=true&isPopup=true&resgatarPneu=true&INSELECTONE=true&closeOnCancel=false' : '')}"/>
				</td>
				<c:if test="${!empty lista}">
					<td align="right"><b>${filtro.currentPage * filtro.pageSize + 1}</b> - <b>${s:gettotalpage(filtro)}</b> de <b>${filtro.numberOfResults}</b></td>
				</c:if>
				<c:if test="${empty lista}">
					<td align="right"><b>N�o existem registros!</b></td>		
				</c:if>
			</tr>
		</c:otherwise>
	</c:choose>
</table>
<script>
	<c:if test="${TtabelaResultados.showExcluirLink}">
		function excluirItensSelecionados(){
			if($dg.validateSelectedValues()){
				document.location = '?ACAO=excluir&itenstodelete='+$dg.getSelectedValues();
			}
		}
	</c:if>
	<c:if test="${!TtabelaResultados.showExcluirLink}">
		$("#btn_excluir").hide();
	</c:if>
	
	function calcularTotalRegistrosPaginacao(){
	
		$("#spanTotalPaginacao").html("calculando...");
 		form.ACAO.value = 'calcularTotalRegistrosPaginacaoAJax';
 		
 		jQuery.ajax({
			type: "POST",
			url: "${ctx}${TEMPLATE_requestQuery}",
			data: $(form).serialize(),
			success: function(data){						
						eval(data);
						$("#spanTotalPaginacao").html(totalRegistrosPaginacao);
					},
			dataType: "script"
		});
	}
</script>