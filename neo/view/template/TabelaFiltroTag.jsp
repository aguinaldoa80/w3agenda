<%@page import="br.com.linkcom.sined.util.controller.IFiltroSalvo"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>


<c:set var="submitLabel" value="${n:default('Filtrar', TabelaFiltroTag.submitLabel)}" />
<c:set var="panelGridColumns" value="${n:default(1, TabelaFiltroTag.columns)}" />
<c:set var="panelGridStyleClass" value="${n:default('inputTable', TabelaFiltroTag.styleClass)}" />
<c:set var="panelGridColumnStylesClasses" value="${n:default('doublelineformat', TabelaFiltroTag.columnStyleClasses)}" />
<c:set var="instanceOfIFiltroSalvo" value="<%= new Boolean(request.getAttribute("filtro") != null && request.getAttribute("filtro") instanceof IFiltroSalvo) %>" />

<c:if test="${instanceOfIFiltroSalvo && exibirFiltroSalvo != null && exibirFiltroSalvo}">
	<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/FiltroSalvo.jsp" />
</c:if>
	<n:panelGrid columns="${panelGridColumns}"
		 style="${tag.style} "
		 colspan="${tag.colspan}"
		 columnStyleClasses="${panelGridColumnStylesClasses}"
		 columnStyles="${tag.columnStyles}"
		 dynamicAttributesMap="${tag.dynamicAttributesMap}"
		 rowStyles="${tag.rowStyles}"
		 styleClass="${panelGridStyleClass} tabelaFiltro"
		 propertyRenderAsDouble="${tag.propertyRenderAsDouble}" width="${tag.width}" rowStyleClasses="${tag.rowStyleClasses}" border="0" cellspacing="0" cellpadding="0" 
		 >
		<c:if test="${instanceOfIFiltroSalvo && exibirFiltroSalvo != null && exibirFiltroSalvo}">
			<t:property name="hashFiltro" type="hidden"/>
		</c:if>
		<c:catch>
			<c:if test="${!empty filtro.filterSimple}">
				<t:property name="filterSimple" id="filterType" type="hidden"/>
			</c:if>
		</c:catch>
		<t:propertyConfig mode="input" showLabel="${tag.propertyShowLabel}" renderAs="doubleline">
			<n:doBody />
			<c:if test="${tag.showInputHiddenPaginacaoSimples==true}">
				<input type="hidden" name="paginacaoSimples" value="${PAGINACAO_SIMPLES}">
			</c:if>
			
		</t:propertyConfig>
	
	</n:panelGrid>

	<c:if test="${tag.showSubmit}">
		<table width="100%">
		<TR align="right">						
			<td>
				<input type="hidden" name="resetCurrentPage" value="<null>">
				<input type="hidden" name="clearFilter" value="false">
				<span id="filtrar"><n:link url="#" onmouseover="Tip(\"Filtrar\")" onclick="javascript:resetPage();doFilter();" id="btn_filtro"><i class="fas fa-filter w3darkgray"></i> Filtrar</n:link></span>		
				<span id="limpar"><n:link url="#" onmouseover="Tip(\"Reiniciar\")" onclick="javascript:resetFilter();$s.clearForm(\"form\");${tag.dynamicAttributesMap['afterclear']};submitForm();" id="btn_limpar"><i class="fas fa-sync-alt w3darkgray"></i> Reiniciar</n:link></span>
				<c:if test="${TEMPLATE_showExportLink}">
					<span id="gerar_csv"><n:link url="#" onmouseover="Tip(\"Gerar CSV\")" onclick="javascript:resetPage();doExport();" id="btn_gerar"><i class="far fa-file-code"></i> Gerar CSV</n:link></span>
				</c:if>
			</td>
		</TR>
		</table>

	<script>
		function doFilter(){
			if($Neo.blockSubmit.canSubmit()){
				form.ACAO.value ='${TabelaFiltroTag.submitAction}';
				form.action = '';
				form.validate = '${TabelaFiltroTag.validateForm}';
				submitForm();
			}else{
				$Neo.blockSubmit.message();
			}
		}
		function resetPage(){
			form.resetCurrentPage.value = 'true';
		}
		
		function resetFilter(){
			form.clearFilter.value = 'true';
		}
		
		function doExport() {
			if($Neo.blockSubmit.canSubmit()){
				form.ACAO.value ='exportar';
				form.validate = '${TabelaFiltroTag.validateForm}';
				submitForm();
			}else{
				$Neo.blockSubmit.message();
			}
		}
	</script>
</c:if>
<script>
	//Fun�ao onload para controle de filtro avan�ado ou simples
	$s.mudaFiltro(true, ${exibirFiltroSalvo != null && exibirFiltroSalvo});
</script>

<div id="errocampolistagem" style="display: none; background-color: red; text-align:center; color: white; font-size: 14px; font-weight: bold;">
	O n�mero de campos da listagem n�o confere com o n�mero de campos cadastrado para a Tela.
</div>
 