<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<n:bean name="filtro">
	<table width="100%" align="center" class="window inputWindow"  cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<n:doBody />
				<c:if test="${TJanelaRelatorio.showSubmit}">
					<div class="actionBar">
						<n:submit action="${TJanelaRelatorio.submitAction}" validate="true" parameters="${TJanelaRelatorio.parameters}" confirmationScript="${TJanelaRelatorio.submitConfirmationScript}">${TJanelaRelatorio.submitLabel}</n:submit>
					</div>
				</c:if>
			</td>
		</tr>
	</table>
</n:bean>


