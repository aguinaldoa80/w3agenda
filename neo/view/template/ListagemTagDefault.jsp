<%@page import="br.com.linkcom.sined.util.SinedUtil"%>
<%@page import="br.com.linkcom.neo.core.web.NeoWeb"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="s" uri="sined"%>

<c:choose>
	<c:when test="${INSELECTONE}">
		<jsp:include page="/WEB-INF/classes/br/com/linkcom/neo/view/template/ListagemTagInsert.jsp" />
	</c:when>
	<c:otherwise>
		<n:form validate="false">
			<n:validation>
				<input type="hidden" name="notFirstTime" value="true"/>
				<table class="outterTable" cellspacing="0" cellpadding="0" align="center">
					<tr class="outterTableHeader">
						<td>
							<span class="outterTableHeaderLeft">
								${listagemTag.titulo}
							</span>
							<span class="outterTableHeaderRight">
								${listagemTag.invokeLinkArea} 
								<c:if test="${listagemTag.showNewLink}">
									<n:link onmouseover="Tip(\"Criar\")" action="criar" id="btn_novo" checkPermission="true"><i class="fas fa-plus-circle w3green"></i> Novo</n:link>&nbsp;
								</c:if>
								<c:if test="${listagemTag.showDeleteLink}">
									<n:hasAuthorization action="excluir">
										<n:link onclick="javascript:excluirItensSelecionados();" url="#" action="excluir" onmouseover="Tip(\"Excluir\")" id="btn_excluir" checkPermission="true"><i class="fas fa-minus-circle w3red"></i> Excluir</n:link><span id="_excluir">&nbsp;</span>
									</n:hasAuthorization>
								</c:if>
								<c:choose>
									<c:when test="${!s:isAtalhoCadastrado(uriAtalho)}">
										<n:link onmouseover="Tip(\"Adicionar atalho\")" url="#" onclick="javascript:ajaxUpdateAtalho(\"${uriAtalho}\")" id="btnAtalho" class="addAtalho"><i class="fas fa-sort-amount-up w3darkgray"></i> Adicionar atalho</n:link>&nbsp;
									</c:when>
									<c:otherwise>
										<n:link onmouseover="Tip(\"Remover atalho\")" url="#" onclick="javascript:ajaxUpdateAtalho(\"${uriAtalho}\")"  id="btnAtalho" class="remAtalho"><i class="fas fa-minus-circle"></i>Remover atalho</n:link>&nbsp;
									</c:otherwise>
								</c:choose>								
								<n:link id="btn_help" url="javascript:$sinedCtx.openHelp('${listagemTag.taghelp}')" onmouseover="Tip(\"Help\")"><i class="fas fa-question-circle w3darkgray"></i>  Ajuda</n:link>		
							</span>
						<br></td>
					</tr>
					<tr>
						<td>
							<n:doBody />
						</td>
					</tr>
				</table>
			</n:validation>
		</n:form>
	</c:otherwise>
</c:choose>
<script>
var empresaSelecionadaCabecalho = <%=SinedUtil.getIdEmpresaSelecionadaCabecalho()%>;						
</script>
<c:if test="${!tag.showDeleteLink}">
	<script>
		$("#btn_excluir").hide();
		$("#_excluir").hide();
	</script>
</c:if>
<c:if test="${listagemTag.isOrdenacaocolunas}">
	<script>
	//ORDENA��O E VISIBILIDADE DOS CAMPOS DA LISTAGEM
	var ordem = ${s:campolistagemOrdem()};
	var naoExibir = ${s:campolistagemNaoexibir()};
	var colunaFixa = ${s:campolistagemColunafixa()};
	
	if((ordem != '<null>' && ordem.length > 1) || (naoExibir != '<null>' && naoExibir.length > 0) ){
		var oTable = $('#tabelaResultados').dataTable( {
				"sDom": 'RC<"clear">lfrtip',			
				"bPaginate": false,			
				"bPaginate": false,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": false,
				"bInfo": false,
				"oColReorder": {								
					"iFixedCol": colunaFixa,			
					"aiOrder": ordem				
				},
				"aoColumnDefs": [			
					{ "bVisible": false, "aTargets": naoExibir }			
				],
				"iFixedCol": colunaFixa,
				"oColVis": {
					"aiExclude": colunaFixa,
					"buttonText": "Exibir / Ocultar Colunas",
					"iOverlayFade": 50			
				}					
		} );
		
		var salvaOrdem = false;	
		//POSICIONAMENTO DO BOT�O EXIBIR/OCULTAR COLUNAS AO LADO DO BOT�O REINICIAR	
		$('#limpar').append("").append($('#ColVisTableTools>button'));				
	}
	
	//salva a ordem e colunas a exibir
	function salvaOrdemusuario(aoColumns, evento){    
		
		var ordemUsuario = "";
		var ordemexibicaoUsuario = "";
				
		for(var i = 0; i < aoColumns.length; i++){
			if(ordemUsuario != '')
				ordemUsuario += "," + aoColumns[i]._ColReorder_iOrigCol;
			else	
				ordemUsuario += "" + aoColumns[i]._ColReorder_iOrigCol;
		}
		
		for(var i = 0; i < aoColumns.length; i++){
			if(ordemexibicaoUsuario != '')
				ordemexibicaoUsuario += "," + aoColumns[i].bVisible;
			else
				ordemexibicaoUsuario += "" + aoColumns[i].bVisible;
		}
		
		$.post("${ctx}/sistema/process/GerenciaCampolistagemusuario",
		   {"ACAO" : "ajaxSalvaOrdenacaoCampolistagemUsuario",
		    "pathTela": '<%=SinedUtil.pathTela()%>',
		    "ordemUsuario" : ordemUsuario,
		    "ordemexibicaoUsuario" : ordemexibicaoUsuario}
		);
	}
	</script>
</c:if>