/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view.template;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;

import br.com.linkcom.neo.bean.PropertyDescriptor;
import br.com.linkcom.neo.bean.annotation.DescriptionProperty;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.util.ReflectionCache;
import br.com.linkcom.neo.util.ReflectionCacheFactory;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.BaseTag;
import br.com.linkcom.neo.view.BeanTag;
import br.com.linkcom.neo.view.ColumnTag;
import br.com.linkcom.neo.view.ComboReloadGroupTag;
import br.com.linkcom.neo.view.DataGridTag;
import br.com.linkcom.neo.view.PanelGridTag;
import br.com.linkcom.neo.view.PanelTag;
import br.com.linkcom.neo.view.SelecionarCadastrarServlet;

/**
 * @author rogelgarcia
 * @since 03/02/2006
 * @version 1.1
 */
public class PropertyTag extends TemplateTag {

	private static final String INPUT = "input";

	private static final String OUTPUT = "output";

	private static final String COLUMN = "column";

	private static final String SINGLE = "single";

	static public final String DOUBLE = "double";
	
	public static final String DOUBLELINE = "doubleline";
	
	
	// tag property
	protected String name;
	private boolean ignoreRequired = false;

	// input / output
	protected String mode = null;
	
	protected String pattern = null;
	
	//column
	protected String order = null;

	protected String renderAs = null;
	
	protected Integer truncate = null;
	
	//panel
	protected Integer colspan = null;
	
	// tag input
	// atributos
	protected String label;

	protected Object type;

	protected Boolean showLabel;

	protected Boolean reloadOnChange = false;
	
	protected Boolean onlyPositiveNumbers = false;

	// checkbox
	protected String trueFalseNullLabels;

	// select-one-button
	protected String selectOnePath;
	protected String selectOnePathParameters;
	
	//select-one-insert
	protected String insertPath;
	protected String insertOnePathParameters;

	// select-one ou select-many
	protected Object itens;
	protected Boolean useAjax;
	protected Boolean autoSugestUniqueItem;
	protected String optionalParams = "";
	protected Boolean holdValue;

	//ajax - somente utilizado se userAjax = true; 
	//executado quando termina-se de atualizar os itens do combo
	protected String onLoadItens = "";
	
	protected String selectLabelProperty;
	
	protected String autocompleteLabelProperty;
	protected String autocompleteMatchProperty;
	protected String autocompleteGetterLabel;
	// select-one
	protected Boolean includeBlank = true;
	protected String blankLabel;

	// text-area
	protected Integer cols;

	protected Integer rows;
	
	//hidden
	protected Boolean write;

	//arquivo
	protected Boolean transientFile;
	protected boolean showRemoverButton = true;
	protected Boolean showFileLink = true;
	
	// taginput fim
	
	
	//estilos
	private String headerStyle = "";
	private String bodyStyle = "";
	private String panelStyle = "";
	private String labelStyle = "";
	
	private String headerStyleClass = "";
	private String bodyStyleClass = "";
	private String panelStyleClass = "";
	private String labelStyleClass = "";
	
	public String getBodyStyle() {
		return bodyStyle;
	}

	public void setBodyStyle(String bodyStyle) {
		this.bodyStyle = bodyStyle;
	}

	public String getHeaderStyle() {
		return headerStyle;
	}

	public void setHeaderStyle(String headerStyle) {
		this.headerStyle = headerStyle;
	}

	public String getLabelStyle() {
		return labelStyle;
	}

	public void setLabelStyle(String labelStyle) {
		this.labelStyle = labelStyle;
	}

	public String getPanelStyle() {
		return panelStyle;
	}

	public void setPanelStyle(String panelStyle) {
		this.panelStyle = panelStyle;
	}

	public Boolean getWrite() {
		return write;
	}


	public boolean isEntityId() {
		boolean res = false;
		Object attribute = getRequest().getAttribute("annotations");
		if(attribute != null){
			Annotation[] annotations = (Annotation[]) attribute;
			for (Annotation annotation : annotations) {
				if(annotation instanceof Id){
					res = true;
				}else if(annotation instanceof EmbeddedId){
					res = true;
				}
			}
		}
		return res;
	}
	

	public void setWrite(Boolean write) {
		this.write = write;
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void doComponent() throws Exception {	
		if (Util.strings.isNotEmpty(mode)) {
			mode = mode.toLowerCase();
			if (!INPUT.equals(mode) && !OUTPUT.equals(mode)) {
				throw new NeoException("A tag property s� aceita no atributo 'mode' os seguintes valores: input ou output. Valor encontrado: " + mode);
			}
		}
		if (Util.strings.isNotEmpty(renderAs)) {
			renderAs = renderAs.toLowerCase();
			if (!COLUMN.equals(renderAs) && !SINGLE.equals(renderAs) && !DOUBLE.equals(renderAs) && !DOUBLELINE.equals(renderAs)) {
				throw new NeoException("A tag property s� aceita no atributo 'renderAs' os seguintes valores: column, single, double ou doubleline. Valor encontrado: " + renderAs);
			}
		}
		PropertyConfigTag configTag = findParent(PropertyConfigTag.class);
		if(configTag != null && configTag.getShowLabel() != null && this.showLabel == null){
			showLabel = configTag.getShowLabel();
		}
		BaseTag findFirst = findFirst(PropertyConfigTag.class, PanelTag.class, ColumnTag.class, DataGridTag.class, PanelGridTag.class);
		if(Util.strings.isEmpty(renderAs)){
			do {
				if (findFirst instanceof PropertyConfigTag && Util.strings.isNotEmpty(configTag.getRenderAs())) {
					renderAs = configTag.getRenderAs();
				} else if (findFirst instanceof PanelTag) {
					PanelTag panel = (PanelTag) findFirst;
					Boolean propertyRenderAsDouble = panel.getPropertyRenderAsDouble();
					if(propertyRenderAsDouble != null){
						renderAs = Util.booleans.isTrue(propertyRenderAsDouble) ? DOUBLE : SINGLE;	
					} else {
						// procurar opcoes de renderAs nas tags mais acima do panel, j� que esse panel n�o est� for�ando a renderiza��o double
						if(configTag != null  && Util.strings.isNotEmpty(configTag.getRenderAs())){
							if(configTag.getRenderAs().toLowerCase().equals(DOUBLELINE)){
								renderAs = DOUBLELINE;
							} else {
								renderAs = SINGLE;
							}
						} else {
							renderAs = SINGLE;
						}
					}					
				} else if (findFirst instanceof PanelGridTag) {
					PanelGridTag panelGrid = (PanelGridTag) findFirst;
					Boolean propertyRenderAsDouble = panelGrid.getPropertyRenderAsDouble();
					if(propertyRenderAsDouble != null){
						renderAs = Util.booleans.isTrue(propertyRenderAsDouble) ? DOUBLE : SINGLE;	
					} else {
						//procurar opcoes de renderAs nas tags mais acima do panel, j� que esse panel n�o est� for�ando a renderiza��o double
						if(configTag != null  && Util.strings.isNotEmpty(configTag.getRenderAs())){
							if(configTag.getRenderAs().toLowerCase().equals(DOUBLELINE)){
								renderAs = DOUBLELINE;
							} else {
								renderAs = SINGLE;
							}
						} else {
							renderAs = SINGLE;
						}
					}					
				} else if (findFirst instanceof DataGridTag) {
					renderAs = COLUMN;
				} else {
					renderAs = SINGLE;
				}
				break; //TODO ARRUMAR ESSE ALGORITMO ESCROTO
			} while (true);
		}
		if(showLabel == null){
			if(SINGLE.equals(renderAs)){
				showLabel = false; // nao faz muito sentido escreve sozinho o label, � melhor mandar escrever quando quiser
			}			
		}
		if(DOUBLE.equals(renderAs)){
			showLabel = false;//se for modo double n�o imprimir o label porque j� vai estar sendo escrito um
		}

		if(Util.strings.isEmpty(mode)){
			if(configTag != null && Util.strings.isNotEmpty(configTag.getMode())){
				mode = configTag.getMode();
			} else {
				if(Util.objects.isNotEmpty(type)){
					mode = INPUT;
				} else {
					mode = OUTPUT;	
				}
					
			}
		}
			
		
		if(colspan != null && DOUBLE.equals(renderAs)){
			colspan = colspan - 1;
		}
		if(useAjax == null){
			ComboReloadGroupTag comboReloadGroupTag = findParent(ComboReloadGroupTag.class);
			if(comboReloadGroupTag != null){
				useAjax = comboReloadGroupTag.getUseAjax();
			}
		}
		String labelseparator = "";
		if(DOUBLELINE.equals(renderAs)){
			renderAs = SINGLE;
			labelseparator = "<BR>";
			showLabel = true;
		}
		if(colspan == null || colspan == 0){
			colspan = 1;
		}
		
		//Pedro: coloca <label> no componente 02/08/2007
		
		if (getId() == null || "".equals(getId())) {
			id = generateUniqueId()+"_"+getName();
		}
//		label = "<label for=\""+componentId+"\">"+label+"</label>";
		
		
		pushAttribute("labelseparator", labelseparator);
		pushAttribute("compId", id);		
		pushAttribute("Tproperty", this);

		includeJspTemplate();
		popAttribute("Tproperty");
		popAttribute("compId");
		popAttribute("labelseparator");
	}
	
	/**
	 * Monta o par�metro orderBy para a listagem de dados.
	 */
	private void makeOrderByField() {
		if(getRequest().getAttribute("TEMPLATE_listagem") != null && order == null){
			BeanTag beanTag = findParent(BeanTag.class);
			order = "";
			if(beanTag != null){
				PropertyDescriptor propertyDescriptor = beanTag.getBeanDescriptor().getPropertyDescriptor(name);
				String[] split = name.split("\\.");
				String propertyName = split.length > 0 ? split[split.length -1] : null;
				String prop = null;
				Class<?> clazz = propertyDescriptor.getPropertyInfo().getClazz();
				ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
				Method[] methods = reflectionCache.getMethods(clazz);
				
				String descriptionProperty = null;
				String pk = null;
				
				for (Method method : methods) {
					if(reflectionCache.isAnnotationPresent(method, DescriptionProperty.class)) {
						descriptionProperty = Util.beans.getPropertyFromGetter(method.getName());
					} else if(reflectionCache.isAnnotationPresent(method, DescriptionProperty.class)){
						pk = Util.beans.getPropertyFromGetter(method.getName());
					}
				}
				
				prop = (descriptionProperty != null) ? descriptionProperty : pk;
				
				order = propertyName + (prop == null ? "" : "."+prop );
				//c�digo anterior
				if(prop == null)
					order = Util.strings.uncaptalize(beanTag.getBeanDescriptor().getTargetClass().getSimpleName())+"."+name;
			}
			//c�digo anterior
			//order += name;
		}
	}

	//funcionalidade chamada do template.. nao deve ser invocada
	//configura a proprieade caso ela seja id
	public Object getIdConfig(){
		//id
		if(type == null && write == null && isEntityId()){
			write = true;
			type = "hidden";
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String getHeader(){
		makeOrderByField();
		if(Util.strings.isNotEmpty(order)) {
			
			Object attributeLabel = label == null? getRequest().getAttribute("label") : label;
			
			String parameter = "";
			Map<String, String[]> parameters = NeoWeb.getRequestContext().getServletRequest().getParameterMap();
			for(Entry<String, String[]> paramEntry : parameters.entrySet()){
				String key = paramEntry.getKey();
				if(!key.equalsIgnoreCase("isPopup") && !key.equalsIgnoreCase("resgatarPneuPopUp")) continue;
				parameter += "&" + key + "=";
				String[] value = paramEntry.getValue();
				for (String string : value) {
					parameter += string;
				}
			}
			String url = "";
			if("true".equals(getRequest().getParameter(SelecionarCadastrarServlet.INSELECTONE))){
				parameter += "&" + SelecionarCadastrarServlet.INSELECTONE + "=true";
				url = "/SELECIONARCADASTRAR";
			}
			
			
			String listarQantidade = NeoWeb.getRequestContext().getParameter("listarQantidade");
			if(listarQantidade != null){
				String paramEmpresa = NeoWeb.getRequestContext().getParameter("cdempresaFiltroVenda");
				String paramLocal = NeoWeb.getRequestContext().getParameter("cdLocalFiltro");
				String projetoPedido = NeoWeb.getRequestContext().getParameter("cdProjeto");
				
				if(paramEmpresa != null){
					parameter += "&cdempresaFiltroVenda="+ paramEmpresa;
				}
				if(paramLocal != null){
					parameter += "&cdLocalFiltro="+ paramLocal;
				}
				if(projetoPedido != null){
					parameter += "&cdProjeto="+ projetoPedido;
				}
				parameter += "&listarQantidade="+ listarQantidade;
			}
			return "<a class=\"order\" href=\""+getRequest().getContextPath()+url+NeoWeb.getRequestContext().getRequestQuery()+"?orderBy="+order+parameter+"\">"+attributeLabel+"</a>";
		} else if(Util.strings.isNotEmpty(label)) {
			return label;
		} else {
			return (String) getRequest().getAttribute("label");
		}
	}
	
	/**
	 * Auto alinhamento dos valores de uma determinada coluna
	 * @return
	 */
	public String getColumnAlign(){
		Object type = getRequest().getAttribute("type");
		if(type.equals(Money.class) || type.equals(Double.class)){
			return "right";
		} else {
			return "";
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCols() {
		return cols;
	}

	public Boolean getIncludeBlank() {
		return includeBlank;
	}
	
	public String getBlankLabel() {
		return blankLabel;
	}

	public Object getItens() {
		return itens;
	}

	public String getLabel() {
		if(label == null){
			return (String) getRequest().getAttribute("label");
		}
		return label;
	}

	public Boolean getReloadOnChange() {
		return reloadOnChange;
	}

	public Boolean getOnlyPositiveNumbers() {
		return onlyPositiveNumbers;
	}
	
	public Integer getRows() {
		return rows;
	}

	public String getSelectOnePath() {
		return selectOnePath;
	}

	public Boolean getShowLabel() {
		return showLabel;
	}

	public String getTrueFalseNullLabels() {
		return trueFalseNullLabels;
	}

	public void setCols(Integer cols) {
		this.cols = cols;
	}

	public void setIncludeBlank(Boolean includeBlank) {
		this.includeBlank = includeBlank;
	}
	
	public void setBlankLabel(String blankLabel) {
		this.blankLabel = blankLabel;
	}

	public void setItens(Object itens) {
		this.itens = itens;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setReloadOnChange(Boolean reloadOnChange) {
		this.reloadOnChange = reloadOnChange;
	}

	public void setOnlyPositiveNumbers(Boolean onlyPositiveNumbers) {
		this.onlyPositiveNumbers = onlyPositiveNumbers;
	}
	
	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public void setSelectOnePath(String selectOnePath) {
		this.selectOnePath = selectOnePath;
	}

	public void setShowLabel(Boolean showLabel) {
		this.showLabel = showLabel;
	}

	public void setTrueFalseNullLabels(String trueFalseNullValues) {
		this.trueFalseNullLabels = trueFalseNullValues;
	}

	public Object getType() {
		return type;
	}

	public void setType(Object type) {
		this.type = type;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getRenderAs() {
		return renderAs;
	}

	public void setRenderAs(String renderAs) {
		this.renderAs = renderAs;
	}

	public Integer getColspan() {
		return colspan;
	}

	public void setColspan(Integer colspan) {
		this.colspan = colspan;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getBodyStyleClass() {
		return bodyStyleClass;
	}

	public void setBodyStyleClass(String bodyStyleClass) {
		this.bodyStyleClass = bodyStyleClass;
	}

	public String getHeaderStyleClass() {
		return headerStyleClass;
	}

	public void setHeaderStyleClass(String headerStyleClass) {
		this.headerStyleClass = headerStyleClass;
	}

	public String getLabelStyleClass() {
		return labelStyleClass;
	}

	public void setLabelStyleClass(String labelStyleClass) {
		this.labelStyleClass = labelStyleClass;
	}

	public String getPanelStyleClass() {
		return panelStyleClass;
	}

	public void setPanelStyleClass(String panelStyleClass) {
		this.panelStyleClass = panelStyleClass;
	}

	public String getSelectLabelProperty() {
		return selectLabelProperty;
	}

	public void setSelectLabelProperty(String selectLabelProperty) {
		this.selectLabelProperty = selectLabelProperty;
	}

	public Boolean getUseAjax() {
		return useAjax;
	}
	
	public String getUseAjaxString() {
		return useAjax ==null? "": useAjax.toString();
	}

	public void setUseAjax(Boolean useAjax) {
		this.useAjax = useAjax;
	}

	public String getOnLoadItens() {
		return onLoadItens;
	}

	public void setOnLoadItens(String onLoadItens) {
		this.onLoadItens = onLoadItens;
	}

	public Boolean getAutoSugestUniqueItem() {
		return autoSugestUniqueItem;
	}

	public void setAutoSugestUniqueItem(Boolean autoSugestUniqueItem) {
		this.autoSugestUniqueItem = autoSugestUniqueItem;
	}

	public Boolean getTransientFile() {
		return transientFile;
	}

	public void setTransientFile(Boolean transientFile) {
		this.transientFile = transientFile;
	}

	public String getOptionalParams() {
		return optionalParams;
	}

	public void setOptionalParams(String optionalParams) {
		this.optionalParams = optionalParams;
	}

	public boolean isShowRemoverButton() {
		return showRemoverButton;
	}

	public void setShowRemoverButton(boolean showRemoverButon) {
		this.showRemoverButton = showRemoverButon;
	}

	public Boolean getHoldValue() {
		return holdValue;
	}

	public void setHoldValue(Boolean holdValue) {
		this.holdValue = holdValue;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getInsertPath() {
		return insertPath;
	}
	
	public Integer getTruncate() {
		return truncate;
	}
	
	public void setTruncate(Integer truncate) {
		this.truncate = truncate;
	}

	public String getSelectOnePathParameters() {
		return selectOnePathParameters;
	}

	public void setInsertPath(String insertPath) {
		this.insertPath = insertPath;
	}

	public void setSelectOnePathParameters(String selectOnePathParameters) {
		this.selectOnePathParameters = selectOnePathParameters;
	}
	
	public Boolean getShowFileLink() {
		return showFileLink;
	}
	
	public void setShowFileLink(Boolean showFileLink) {
		this.showFileLink = showFileLink;
	}
	
	public void setInsertOnePathParameters(String insertOnePathParameters) {
		this.insertOnePathParameters = insertOnePathParameters;
	}
	
	public String getInsertOnePathParameters() {
		return insertOnePathParameters;
	}

	public String getAutocompleteLabelProperty() {
		return autocompleteLabelProperty;
	}

	public String getAutocompleteMatchProperty() {
		return autocompleteMatchProperty;
	}

	public void setAutocompleteLabelProperty(String autocompleteLabelProperty) {
		this.autocompleteLabelProperty = autocompleteLabelProperty;
	}

	public void setAutocompleteMatchProperty(String autocompleteMatchProperty) {
		this.autocompleteMatchProperty = autocompleteMatchProperty;
	}

	public String getAutocompleteGetterLabel() {
		return autocompleteGetterLabel;
	}

	public void setAutocompleteGetterLabel(String autocompleteGetterLabel) {
		this.autocompleteGetterLabel = autocompleteGetterLabel;
	}

	public boolean isIgnoreRequired() {
		return ignoreRequired;
	}

	public void setIgnoreRequired(boolean ignoreRequired) {
		this.ignoreRequired = ignoreRequired;
	}
	
}
