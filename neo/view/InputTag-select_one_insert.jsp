<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>

<!-- Valor selecionado ${tag.valueToString} -->
<select name="${tag.name}" id="${tag.id}" onchange="${tag.reloadOnChangeString}" ${tag.dynamicAttributesToString}>${tag.selectoneblankoption}${tag.selectItensString}</select>
<c:if test="${!consultar}">
	<button id="btn_insert_one" 
			name="${tag.name}_btn" 
			type="button" 
			onclick="${tag.selectOneInsertOnClick}" 
			onmouseover="Tip('Novo')" 
			class="w3link"
			style='border: 0px; background-color: transparent; text-transform: none;'>
		<i class="fas fa-plus-circle w3darkgray"></i> 
	</button>
</c:if>
