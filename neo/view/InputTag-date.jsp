<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:choose>
	<c:when test="${android != null && android}">
		<input type="text"
			   id="${tag.id}"
			   name="${tag.name}"
			   value="${tag.valueToString}" 
			   maxlength="10"
			   size="11" 
			   onKeyPress="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){hide_calendar();return true;}"
			   onChange="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){${tag.reloadOnChangeString}${tag.dynamicAttributesMap['change']}}" 
			   onfocus="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){this.select();lcs(this)}" 
			   onclick="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){event.cancelBubble=true;this.select();lcs(this)}"
			   onblur="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){verifica_data(this);${tag.dynamicAttributesMap['blur']}}" ${tag.dynamicAttributesToString}/>
		<input type="hidden" name="${tag.name}_datePattern" value="${tag.pattern}"/>
	</c:when>
	<c:otherwise>
		<input type="text"
			   id="${tag.id}"
			   name="${tag.name}"
			   value="${tag.valueToString}" 
			   maxlength="10"
			   size="11" 
			   autocomplete="off"
			   onKeyUp="mascara_data(this,event,'${tag.pattern}');" 
			   onKeyDown="if(isTab(this, event)) {hide_calendar();}"
			   onKeyPress="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){hide_calendar();return valida_tecla_data(this, event,'${tag.pattern}');}"
			   onChange="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){${tag.reloadOnChangeString}${tag.dynamicAttributesMap['change']}}" 
			   onfocus="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){this.select();lcs(this)}" 
			   onclick="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){event.cancelBubble=true;this.select();lcs(this)}"
			   onblur="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){verifica_data(this);${tag.dynamicAttributesMap['blur']}}" ${tag.dynamicAttributesToString}/>
		<input type="hidden" name="${tag.name}_datePattern" value="${tag.pattern}"/>
	</c:otherwise>
</c:choose>