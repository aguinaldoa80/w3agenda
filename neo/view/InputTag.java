/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import br.com.linkcom.neo.authorization.User;
import br.com.linkcom.neo.bean.BeanDescriptor;
import br.com.linkcom.neo.bean.BeanDescriptorFactoryImpl;
import br.com.linkcom.neo.bean.PropertyDescriptor;
import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.controller.crud.AbstractCrudController;
import br.com.linkcom.neo.controller.crud.CrudController;
import br.com.linkcom.neo.core.config.Config;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.persistence.GenericDAO;
import br.com.linkcom.neo.types.Cep;
import br.com.linkcom.neo.types.Cnpj;
import br.com.linkcom.neo.types.Cpf;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.neo.types.Hora;
import br.com.linkcom.neo.types.InscricaoEstadual;
import br.com.linkcom.neo.types.Money;
import br.com.linkcom.neo.types.Password;
import br.com.linkcom.neo.types.Telefone;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.validation.JavascriptValidationItem;
import br.com.linkcom.neo.validation.ValidationItem;
import br.com.linkcom.neo.validation.ValidatorMapper;
import br.com.linkcom.neo.validation.annotation.Required;
import br.com.linkcom.neo.view.ajax.ComboCallback;
import br.com.linkcom.neo.view.code.DebugInputsTag;
import br.com.linkcom.neo.view.template.PropertyConfigTag;
import br.com.linkcom.neo.view.util.DefaultFunctionCallInfo;
import br.com.linkcom.neo.view.util.FunctionCall;
import br.com.linkcom.neo.view.util.FunctionParameter;
import br.com.linkcom.neo.view.util.ParameterType;
import br.com.linkcom.sined.util.SinedUtil;

/**
 * @author rogelgarcia
 * @since 26/01/2006
 * @version 1.1
 */
public class InputTag extends BaseTag {

	// atributos
	protected String name;

	protected String label;

	protected Object type;
	protected Object autowiredType;

	protected Object value;

	protected String pattern;

	protected Boolean autowire = true;

	protected Boolean required;

	protected Boolean showLabel = false;

	protected Annotation[] annotations;

	protected Boolean reloadOnChange = false;
	
	protected Boolean onlyPositiveNumbers = false;	

	protected Boolean write = false;

	private boolean ignoreRequired = false;

	// checkbox
	protected String trueFalseNullLabels;
	protected Boolean booleanValue = null;

	protected boolean forceValidation = false;

	// select-one-button
	protected String selectOnePath;
	protected String selectOnePathParameters;

	// select-one-insert
	protected String insertPath;
	protected String insertOnePathParameters;

	// select-one ou select-many
	protected Object itens;
	protected String selectLabelProperty;
	protected Boolean useAjax;
	protected Boolean autoSugestUniqueItem;
	protected String optionalParams = "";
	protected Boolean holdValue;

	protected PropertySetter propertySetter;

	// ajax - somente utilizado se userAjax = true;
	// executado quando termina-se de atualizar os itens do combo
	protected String onLoadItens = "";

	// select-one
	protected Boolean includeBlank = true;
	protected String blankLabel = " ";

	// text-area
	protected Integer cols;

	protected Integer rows;

	// button do file upload
	protected boolean showRemoverButton = true;
	protected Boolean showFileLink = true;

	// extra
	protected Type selectedType = Type.TEXT;

	static protected Map<String, Type> mapaTipos = new HashMap<String, Type>();

	@SuppressWarnings("unchecked")
	static protected Map<Class, Type> mapaTiposClasses = new HashMap<Class, Type>();

	private String selectItensString;

	private String selectoneblankoption;

	private Boolean checked;

	private String checkboxValue = "true";
	
	//autocomplete
	protected String beanName;
	protected String loadFunctionAutocomplete;
	protected String autocompleteLabelProperty;
	protected String autocompleteMatchProperty;
	protected String autocompleteGetterLabel;
//	protected String local;
//	protected String formatItem;
//	protected String formatResult;
//	protected String valueField;
//	protected String data;
	
	
	
	// estilos
	private String labelStyle = "";

	private String labelStyleClass = "";

	// arquivo
	protected Boolean transientFile;

	static {
		mapaTiposClasses.put(InscricaoEstadual.class, Type.INSCRICAO_ESTADUAL);
		mapaTiposClasses.put(java.sql.Date.class, Type.DATE);
		mapaTiposClasses.put(Collection.class, Type.SELECT_MANY);
		mapaTiposClasses.put(Date.class, Type.DATE);
		mapaTiposClasses.put(Calendar.class, Type.DATE);
		mapaTiposClasses.put(Time.class, Type.TIME);
		mapaTiposClasses.put(Timestamp.class, Type.TIMESTAMP);
		mapaTiposClasses.put(Boolean.class, Type.CHECKBOX);
		mapaTiposClasses.put(boolean.class, Type.CHECKBOX);
		mapaTiposClasses.put(Integer.class, Type.INTEGER);
		mapaTiposClasses.put(Short.class, Type.INTEGER);
		mapaTiposClasses.put(Long.class, Type.INTEGER);
		mapaTiposClasses.put(Byte.class, Type.INTEGER);
		mapaTiposClasses.put(BigInteger.class, Type.INTEGER);
		mapaTiposClasses.put(Float.class, Type.FLOAT);
		mapaTiposClasses.put(Double.class, Type.FLOAT);
		mapaTiposClasses.put(BigDecimal.class, Type.FLOAT);
		mapaTiposClasses.put(Cep.class, Type.CEP);
		mapaTiposClasses.put(Telefone.class, Type.TELEFONE);
		mapaTiposClasses.put(Cpf.class, Type.CPF);
		mapaTiposClasses.put(Cnpj.class, Type.CNPJ);
		mapaTiposClasses.put(Hora.class, Type.TIME);
		mapaTiposClasses.put(Money.class, Type.MONEY);
		mapaTiposClasses.put(File.class, Type.FILE);
	}

	protected enum Type {
		BUTTON, CHECKBOX, CHECKLIST, FILE, HIDDEN, IMAGE, PASSWORD, RADIO, RESET, SUBMIT, TEXT, DATE, TIMESTAMP, TIME, FLOAT, INTEGER, MONEY, CPF, CNPJ, CEP, INSCRICAO_ESTADUAL, CREDIT_CARD, SELECT_ONE, SELECT_MANY, SELECT_ONE_BUTTON, SELECT_ONE_RADIO, SELECT_ONE_INSERT, TEXT_AREA, TELEFONE, HTML, AUTOCOMPLETE, DATE_HTML5, NUMBER_HTML5
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void doComponent() throws Exception {
		// boolean valueExplicito = value != null;
		autowireAttributes();
		BeanTag beanTag = findParent(BeanTag.class);
		if (Util.strings.isNotEmpty(onLoadItens)
				&& (useAjax == null || !useAjax)) {
			throw new NeoException(
					"N�o � poss�vel utilizar onLoadItens se useAjax n�o for true");
		}
		selectedType = chooseType();
		if ((selectedType == Type.SELECT_ONE
				|| selectedType == Type.SELECT_ONE_INSERT
				|| selectedType == Type.SELECT_ONE_RADIO 
				|| selectedType == Type.SELECT_MANY)
				&& includeBlank) {
			String value = "<null>";
			String label = this.blankLabel;
			selectoneblankoption = createOption(value, label);
		}
		
		boolean disabled = "disabled".equals(getDAAtribute("disabled", false));
		
		String loadItensFunction = "";
		if (selectedType == Type.SELECT_MANY 
				|| selectedType == Type.SELECT_ONE
				|| selectedType == Type.SELECT_ONE_INSERT
				|| selectedType == Type.SELECT_ONE_RADIO
				|| selectedType == Type.SELECT_ONE_BUTTON) {
			InputTag lastInput = null;
			FunctionCall call = null;
			DefaultFunctionCallInfo callInfo = null;

			// esse c�digo ficava dentro do if abaixo .. foi retirado para
			// verificar se estamos lidando com enums mais abaixo
			Class usingType = getClassType();

			boolean selectAll = false;
			if ("all".equalsIgnoreCase(Util.strings.toString(itens))) {
				itens = null;
				selectAll = true;
			}

			if (selectedType != Type.SELECT_ONE_RADIO
					&& selectedType != Type.SELECT_ONE_BUTTON) {
				ComboReloadGroupTag comboReload = findParent(ComboReloadGroupTag.class);

				if (comboReload != null) {

					if (this.useAjax == null) {
						this.useAjax = comboReload.useAjax;
					}

					String itensFunction = Util.strings.toString(itens);

					if (itensFunction != null) {
						itensFunction = itensFunction.trim();
						if (beanTag == null && useAjax) {
							throw new RuntimeException(
									"A tag input deve estar aninhada a uma tag bean para poder utilizar useAjax=true");
						} else {
							boolean isList = itensFunction.length() > 0 ? itensFunction.charAt(0) == '[' && itensFunction.charAt(itensFunction.length()-1) == ']' : false;
							if (beanTag != null && useAjax
									&& !"all".equalsIgnoreCase(itensFunction) 
									&& !isList) { // &&
																					// useAjax
																					// ->
																					// Alterado
																					// em
																					// 26/10/2006
																					// (R�gel
																					// -
																					// N�o
																					// sei
																					// se
																					// causar�
																					// afeito
																					// colateral)
								call = new FunctionCall(itensFunction);
								callInfo = montarCallInfo(call, beanTag);
							}
						}
					}
					if (!Enum.class.isAssignableFrom(usingType) || usingType.isEnum()) {

						if (!selectAll) {
							comboReload.registerProperty(getName(), call,
									includeBlank);
							// comboReload.registerProperty(usingType,
							// getName(), this.useAjax, itensFunction,
							// selectLabelProperty, includeBlank, call,
							// callInfo, onLoadItens, autoSugestUniqueItem);
							lastInput = comboReload.getLastInput(this);
						} else {
							comboReload.getLastInput(this);
						}
						if (call == null && lastInput != null) {
							// registrar a chamada (o registro quando call !=
							// null � feita depois
							ComboCallback.register(usingType.getSimpleName(),
									"findBy", new Class[0]);
						}
					} else {
						// para enums o lastInput sempre � ignorado
						comboReload.getLastInput(this);
					}

					// c�digo removido em 30/11/2006
					// if(lastInput == null){
					// useAjax = false;
					// }

					loadItensFunction = montarLoadItensFunction(call, callInfo,
							lastInput, usingType);

				}
			} else if (selectedType == Type.SELECT_ONE_BUTTON) {
				itens = new ArrayList(); // se for select_one_button n�o
											// existe lista
				ComboReloadGroupTag comboReload = findParent(ComboReloadGroupTag.class);
				if (comboReload != null) {
					comboReload.getLastInput(this);
				}
			}  
			List<String> organizeItens;

			if (itens != null && itens instanceof String) {
				if (useAjax == null || !useAjax) {
					String expression = (String) itens;
					Object value = getOgnlValue(expression);
					organizeItens = organizeItens(value);
				} else {
					// String expression = (String) itens;
					if (call == null || callInfo == null) {
						throw new NullPointerException(
								"Resultado inesperado. call ou callinfo nulos. O algoritmo do framework n�o est� correto");
					}
					String functionName = call.getFunctionName();
					Object bean = Neo.getApplicationContext().getConfig()
							.getDefaultListableBeanFactory().getBean(
									call.getObject());
					Class[] classes = null;
					Object[] values = null;
					boolean ignorecall = false;
					if (call.getParameterArray().length == 0
							&& !call.getCall().endsWith("()")) {
						if (lastInput.getValue() == null) {
							organizeItens = new ArrayList<String>();
							values = new Object[0];
							ignorecall = true;
						} else {
							classes = new Class[] { lastInput.getValue()
									.getClass() };
							values = new Object[] { lastInput.getValue() };
						}
					} else {
						classes = new Class[call.getParameterArray().length];
						values = new Object[call.getParameterArray().length];
					}
					for (int i = 0; i < call.getParameterArray().length; i++) {
						classes[i] = callInfo
								.getType(call.getParameterArray()[i]);
						values[i] = callInfo
								.getValue(call.getParameterArray()[i]);
					}
					boolean anynull = false;
					HashSet<String> optionalParametersSet = getOptionalParametersSet();
					int i = 0;
					for (Object object : values) {
						if (!optionalParametersSet.contains(call
								.getParameterArray()[i].getParameterValue())) {
							// se for obrigatorio e for null n�o fazer a chamada
							if (object == null) {
								anynull = true;
								break;
							}
						}
						i++;
					}

					// registrar a chamada
					ComboCallback.register(call.getObject(), call
							.getFunctionName(), classes);

					ignorecall = (ignorecall || anynull)
							&& (call.getParameterArray().length != 0);
					Object lista;
					if (!ignorecall) {
						lista = Util.objects.findAndInvokeMethod(bean,
								functionName, values, classes);
						if (!(lista instanceof List)) {
							throw new RuntimeException("O retorno do m�todo "
									+ functionName + " n�o foi uma lista");
						}
						organizeItens = organizeItens((List<String>) lista);
					} else {
						organizeItens = new ArrayList<String>();
					}

				}
			} else if (itens != null) {
				organizeItens = organizeItens(itens);
			} else {
				if (Enum.class.isAssignableFrom(usingType)) {
					Method method = usingType.getMethod("values");
					Enum[] enumValues = (Enum[]) method.invoke(null);
					Map<Object, Object> values = new LinkedHashMap<Object, Object>();
					for (Enum enumValue : enumValues) {
						values.put(Enum.valueOf(usingType, enumValue.name()),
								enumValue.toString());
					}
					organizeItens = organizeItens(values);
				} else {
					organizeItens = organizeItens(doSelectAllFromService(lastInput));
				}

			}
			this.selectItensString = toString(organizeItens);
		}

		if (((selectedType == Type.CHECKBOX || selectedType == Type.SELECT_ONE_RADIO) && Util.strings
				.isNotEmpty(trueFalseNullLabels))
				|| (Util.strings.isNotEmpty(trueFalseNullLabels))) {
			if (selectedType == Type.CHECKBOX) {
				selectedType = Type.SELECT_ONE;
			}
			String[] split = trueFalseNullLabels.split(",");
			Map<Boolean, String> mapa = new LinkedHashMap<Boolean, String>();
			mapa.put(Boolean.TRUE, split[0]);
			mapa.put(Boolean.FALSE, split[1]);
			
			if (split.length == 3) {
				selectoneblankoption = createOption("<null>", split[2],
						this.value == null);
			} else if (includeBlank != null && includeBlank) {
				selectoneblankoption = createOption("<null>", this.blankLabel,
						this.value == null);
			}

			this.selectItensString = toString(organizeItens(mapa));
		}
		if (selectedType == Type.CHECKLIST) {
			if (value instanceof Collection || value != null
					&& value.getClass().isArray()) {
				// if(!valueExplicito)
				throw new NeoException(
						"O atributo value da tag input n�o pode ser um Collection ou Array quando o tipo for CHECKLIST. Voc� deve ter utilizado a tag property para montar esse input. Nesse caso deve ser explicitado qual � o valor desse checkbox ao inv�s de utilizar o value adquirido automaticamente pelo property. Coloque no atributo value desse input qual o valor que esse checkbox deve representar");
				// else
				// throw new NeoException("O atributo value da tag input n�o
				// pode ser um Collection ou Array quando o tipo for
				// CHECKLIST");
			}
			Object itensValue = null;
			if (itens != null && itens instanceof String) {
				String expression = (String) itens;
				itensValue = getOgnlValue(expression);
			} else if (itens != null) {
				itensValue = itens;
			}
			List<String> lista = new ArrayList<String>();
			if (itensValue instanceof Collection) {
				for (Object object : (Collection) itensValue) {
					lista.add(getObjectValueToString(object));
				}
			} else if (itensValue != null && itensValue.getClass().isArray()) {
				Object[] array = (Object[]) itensValue;
				for (Object object : array) {
					lista.add(getObjectValueToString(object));
				}
			}
			String valueToString = getObjectValueToString(value);
			boolean toCheck = false;
			for (String string : lista) {
				if (string.equals(valueToString)) {
					toCheck = true;
					break;
				}
			}
			this.checked = toCheck;
			this.checkboxValue = valueToString;
		}
		// getOut().println("<nobr>");
		if (showLabel) {
			if (label != null && label.trim().length() != 0) {
				boolean usespan = labelStyle != null && labelStyle.length() > 0
						|| labelStyleClass != null
						&& labelStyleClass.length() > 0;

				getOut().print("<label for=\"" + getId() + "\">");

				if (usespan) {
					getOut().print("<span ");
					getOut().print("style=\"" + labelStyle + "\" ");
					getOut().print("class=\"" + labelStyleClass + "\"");
					getOut().print(">");
				}
				// removido o (dois pontos) + ": "
				getOut().print(label);
				if (usespan) {
					getOut().print("</span>");
				}
				getOut().print("</label>");
			}
		}
		

		PropertyConfigTag propertyConfig = findParent(PropertyConfigTag.class);
		DataGridTag dataGridTag = findParent(DataGridTag.class);
		if (propertyConfig != null
				&& Boolean.TRUE.equals(propertyConfig.getDisabled())
				&& (dataGridTag == null || dataGridTag.getCurrentStatus() != DataGridTag.Status.DYNALINE)) {
			if (disabled) {
				getDynamicAttributesMap().put("originaldisabled", "disabled");
			}
			getDynamicAttributesMap().put("disabled", "disabled");
		}

		if (!disabled && (selectedType != Type.HIDDEN || forceValidation)) {
			ValidationItem validationItem = null;
			String labelSimples = label != null ? (label.replaceAll("&nbsp;",
					"").replaceAll("<BR>", "")) : "";
			
			
			if (type instanceof Class) {
				validationItem = Neo.getApplicationContext().getConfig()
						.getValidatorRegistry().getExtractor()
						.getValidationItem(labelSimples, (Class) type,
								annotations);
				addExtraValidation(beanTag,validationItem);
				
			} else if (type instanceof String) {
				validationItem = Neo.getApplicationContext().getConfig()
						.getValidatorRegistry().getExtractor()
						.getValidationItem(labelSimples, (String) type,
								annotations);
				
				
				
			} else if (type instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) type;
				validationItem = Neo.getApplicationContext().getConfig()
						.getValidatorRegistry().getExtractor()
						.getValidationItem(labelSimples,
								(Class) parameterizedType.getRawType(),
								annotations);
			}
			
			if (ignoreRequired){
				Iterator<ValidatorMapper> iterator = validationItem.getValidations().iterator();
				while (iterator.hasNext())
					if (Required.class.isAssignableFrom(iterator.next().getAnnotation().getClass()))
						iterator.remove();
			}
			
			if (validationItem != null && (validationItem.getTypeValidator() != null || validationItem.getValidations().size() > 0)) {
				ValidationTag validationTag = findParent(ValidationTag.class);
				if (validationTag != null) {
					JavascriptValidationItem javascriptValidationItem = new JavascriptValidationItem(validationItem);
					javascriptValidationItem.setFieldDisplayName(labelSimples);
					javascriptValidationItem.setFieldName(name);
					validationTag.register(javascriptValidationItem);
				}
			}
		}

		// fazer os listeners
		for (Annotation annotation : annotations) {
			getInputListener(annotation).onRender(this, annotation);
		}
		if (selectedType == Type.CHECKLIST) {
			selectedType = Type.CHECKBOX;// utilizar o template do checkbox
		}
		
		if(selectedType == Type.FILE && Boolean.TRUE.equals(propertyConfig.getDisabled())){
			this.showRemoverButton = false;
		}
		
		DebugInputsTag debugInputsTag = findParent(DebugInputsTag.class, false);
		if (debugInputsTag != null) {
			boolean add = debugInputsTag.addProperty(name);
			if (!add) {
				getDynamicAttributesMap().put("style", "border: 1px solid red");
				getDynamicAttributesMap().put("title",
						"Propriedade duplicada: " + name);
			}
			try {
				Class class1 = debugInputsTag.getCommandClass();
				Neo.getApplicationContext().getBeanDescriptor(null, class1)
						.getPropertyDescriptor(name);
			} catch (Exception e) {
				getDynamicAttributesMap().put("style", "border: 1px solid red");
				getDynamicAttributesMap().put("title",
						"Propriedade inv�lida: " + name);
			}

		}

		// alguns inputs sao alinhados a direta
		if (selectedType == Type.INTEGER || selectedType == Type.FLOAT
				|| selectedType == Type.MONEY || selectedType == Type.NUMBER_HTML5) {
			Object style = getDAAtribute("style", false);
			if (style != null) {
				style = "text-align: right; " + style;
			} else {
				style = "text-align: right; ";
			}
			setDynamicAttribute(null, "style", style);
		}

		if (selectedType == Type.DATE || selectedType == Type.TIME || selectedType == Type.TIMESTAMP || selectedType == Type.DATE_HTML5) {
			if (Util.strings.isEmpty(pattern)) {
				if (selectedType == Type.DATE) {
					pattern = "dd/MM/yyyy";
				} else if(selectedType == Type.TIMESTAMP) {
					pattern = "dd/MM/yyyy HH:mm";
				} else {
					pattern = "HH:mm";
				}
			}

			getDynamicAttributesMap().put("maxlength", pattern.length());
			getDynamicAttributesMap().put("size", pattern.length() + 1);
		} 
		
		//controla a maneira que � renderizado quando um campo � required
		String renderRequiredType = Neo.getApplicationContext().getConfig().getRequiredRenderType();
		boolean skipRenderAsterisk = false;

		if(required != null && required && "addclass".equals(renderRequiredType)){
            skipRenderAsterisk = true;
            if(selectedType != Type.CHECKBOX)
                if(getDynamicAttributesMap().containsKey("class")){
                    getDynamicAttributesMap().put("class", "required "+getDynamicAttributesMap().get("class"));
                } else {
                    getDynamicAttributesMap().put("class", "required");
                }
        }
		
		if(selectedType == Type.AUTOCOMPLETE){
			PropertyDescriptor propertyDescriptor;
			
			if(name != null && name.contains(".")  && name.contains("[") && name.contains("]")){
				propertyDescriptor = beanTag.getBeanDescriptor().getPropertyDescriptor(name.substring(name.lastIndexOf(".")+1, name.length()));
			} else{
				propertyDescriptor = beanTag.getBeanDescriptor().getPropertyDescriptor(name);
			}
				
			
			beanName = Util.strings.uncaptalize(propertyDescriptor.getPropertyInfo().getClazz().getSimpleName());
			
			if(itens != null){
				String itensFunction = Util.strings.toString(itens);
				FunctionCall functionCall = new FunctionCall(itensFunction);
				String call = functionCall.getCall();
				loadFunctionAutocomplete = call;
			}
			
		}

		includeTemplate();
		
		if(!skipRenderAsterisk) printRequired();

		if (selectedType == Type.SELECT_ONE || selectedType == Type.SELECT_MANY
				|| selectedType == Type.SELECT_ONE_INSERT) {
			if (Util.strings.isNotEmpty(loadItensFunction.trim())) {
				getOut().println("<script language='javascript'>");
				getOut().println(loadItensFunction);
				getOut().println("</script>");
			}

		}
		
		// getOut().println("</nobr>");
	}
	
	private void addExtraValidation(BeanTag beanTag, ValidationItem validationItem) {
		if(name != null && name.contains(".") && !name.contains("{index}") && !(name.contains("[") && name.contains("]"))){
			PropertyDescriptor propertyDescriptor = beanTag.getBeanDescriptor().getPropertyDescriptor(name.substring(0,name.indexOf(".")));
			Annotation[] annotationsValidators = propertyDescriptor.getAnnotations();
			
			boolean overrideValidation = Neo.getApplicationContext().getConfig()
				.getValidatorRegistry().getExtractor()
				.overrideValidation(validationItem, name, annotationsValidators);
			
			if(overrideValidation && !ignoreRequired) required = validationItem.existsRequired();
		}
	}

	@SuppressWarnings("unchecked")
	private Class getClassType() {
		Class class1 = type instanceof Class ? (Class) type
				: (type instanceof ParameterizedType ? Util.generics
						.getActualClassesArgument((ParameterizedType) type)[0]
						: (Class) autowiredType);
		if (class1 == null) {
			// verificar pelo itens
			if (itens instanceof Collection) {
				if (((Collection) itens).size() > 0) {
					Object o = ((Collection) itens).iterator().next();
					if (o != null) {
						return o.getClass();
					}
				}
			}
		}
		return class1;
	}

	protected void includeTemplate() throws ServletException, IOException {
		includeJspTemplate(selectedType.toString().toLowerCase());
	}

	protected void printRequired() throws IOException {
		if (required != null && required && selectedType != Type.HIDDEN && !getDynamicAttributesMap().containsKey("readonly")) {
			getOut().println("<span class='requiredMark'>*</span>");
		}
	}

	@SuppressWarnings("unchecked")
	private String montarLoadItensFunction(FunctionCall call,
			DefaultFunctionCallInfo callInfo, InputTag lastInput,
			Class usingType) {
		if (useAjax == null || !useAjax) {
			return "";
		}
		FormTag formTag = findParent(FormTag.class, true);
		String form = formTag.getName();
		String ifcode = "";
		HashSet<String> optionalParametersSet = getOptionalParametersSet();
		if (call != null) {
			FunctionParameter[] parameterArray = call.getParameterArray();
			for (FunctionParameter parameter : parameterArray) {
				if (parameter.getParameterType() == ParameterType.REFERENCE
						&& !optionalParametersSet.contains(parameter
								.getParameterValue())) {
					ifcode += form + "['" + parameter.getParameterValue()
							+ "'].value != '<null>' && ";
				}
			}
		} else if (lastInput != null) {
			ifcode = form + "['" + lastInput.getName()
					+ "'].value != '<null>' && ";
		}

		String listaParametrosFuncao = "var listaParametros = '";
		String listaClassesFuncao = "var listaClasses = '";
		if (call != null && call.getParameterArray().length > 0) {

			FunctionParameter[] parameterArray = call.getParameterArray();
			for (int j = 0; j < parameterArray.length; j++) {
				// boolean hasNext = j+1 <
				// functionCall.getParameterArray().length;
				String param = call.getParameterArray()[j].getParameterValue();
				switch (call.getParameterArray()[j].getParameterType()) {
				case REFERENCE:
					listaParametrosFuncao += "'+getInputValue("
							+ formTag.getName() + "['" + param + "'])+'";
					listaClassesFuncao += callInfo.getType(
							call.getParameterArray()[j]).getName();
					listaClassesFuncao += ComboReloadGroupTag.CLASS_SEPARATOR;
					listaParametrosFuncao += ComboReloadGroupTag.PARAMETRO_SEPARATOR;
					break;
				case STRING:
					listaParametrosFuncao += param;
					listaClassesFuncao += String.class.getName();
					listaClassesFuncao += ComboReloadGroupTag.CLASS_SEPARATOR;
					listaParametrosFuncao += ComboReloadGroupTag.PARAMETRO_SEPARATOR;
					break;
				case BOOLEAN:
					listaParametrosFuncao += param;
					listaClassesFuncao += Boolean.class.getName();
					listaClassesFuncao += ComboReloadGroupTag.CLASS_SEPARATOR;
					listaParametrosFuncao += ComboReloadGroupTag.PARAMETRO_SEPARATOR;
					break;
				case USER:
					listaParametrosFuncao += param;
					listaClassesFuncao += User.class.getName();
					listaClassesFuncao += ComboReloadGroupTag.CLASS_SEPARATOR;
					listaParametrosFuncao += ComboReloadGroupTag.PARAMETRO_SEPARATOR;
					break;
				default:
					throw new RuntimeException("Tipo n�o suportado: "
							+ call.getParameterArray()[j].getParameterType());
				}
			}
		}
		listaParametrosFuncao += "';";
		listaClassesFuncao += "';";
		String parentValue = "";
		if (call == null && lastInput != null) {
			parentValue = form + "['" + lastInput.getName() + "'].value";
		} else {
			parentValue = "''";
		}

		String holdValue = Util.booleans.isTrue(this.holdValue) ? ", " + form
				+ "['" + getName() + "'].value" : "";
		String addItensHoldValue = Util.booleans.isTrue(this.holdValue) ? "true"
				: "false";
		ifcode += "1 == 1";
		String functionCode = form + "['" + getName()
				+ "'].loadItens = function(){\n";

		functionCode += "    var executeOnchange = " + form + "['" + getName()
				+ "'].value != '<null>' && " + form + "['" + getName()
				+ "'].value != '';\n";
		functionCode += "    " + form + "['" + getName()
				+ "'].wasEmpty = !executeOnchange;\n";

		functionCode += "    if(" + ifcode + "){\n";
		functionCode += "        limparCombo(" + form + "['" + getName()
				+ "'], " + includeBlank + " " + holdValue + ", '"
				+ getBlankLabel() + "');\n";
		functionCode += "        " + listaParametrosFuncao + "\n";
		functionCode += "        " + listaClassesFuncao + "\n";
		functionCode += "        ajaxLoadCombo('"
				+ getRequest().getContextPath() + "', " + form + "['"
				+ getName() + "'], '" + usingType.getName() + "', '"
				+ Util.strings.escape(call != null ? call.getCall() : "")
				+ "', listaClasses, listaParametros, '" + selectLabelProperty
				+ "', " + parentValue + ");\n";
		functionCode += "    }\n";
		functionCode += "    else {\n";
		functionCode += "        limparCombo(" + form + "['" + getName()
				+ "'], " + includeBlank + ", '" + getBlankLabel() + "');\n";
		functionCode += "        if(executeOnchange) " + form + "['"
				+ getName() + "'].onchange();\n";
		functionCode += "    }\n";

		functionCode += "};\n\n";

		functionCode += "form['" + getName()
				+ "'].setItens = function(lista){\n";
		functionCode += "      var valorMantido = addItensToCombo(" + form
				+ "['" + getName() + "'], lista, " + addItensHoldValue + ");\n";

		if (autoSugestUniqueItem != null && autoSugestUniqueItem) {
			String consultar = "";
			try {
				if(getRequest().getAttribute("consultar") != null){
					consultar = ((Boolean) getRequest().getAttribute("consultar")).toString();
				}
			} catch (Exception e) {}
			String indice = includeBlank ? "1" : "0";
			functionCode += "        if(lista.length >= 1){";
			functionCode += "            if(lista.length == 1 && '" + consultar + "' != 'true'){" + form + "['"
					+ getName() + "'].selectedIndex = " + indice
					+ "; "+form+"['"+getName()+"'].onchange();" +
					"}/*AUTO SUGEST UNIQUE ITEM*/\n";
			functionCode += "        }";
		}

		functionCode += "       if(!" + form + "['" + getName()
				+ "'].wasEmpty && !valorMantido){" + form + "['" + getName()
				+ "'].onchange();}\n";

		functionCode += "        " + onLoadItens + "\n";

		functionCode += "};\n";
		return functionCode;
	}

	private HashSet<String> getOptionalParametersSet() {
		String[] split = optionalParams.split("( )*?,( )*?");
		for (int i = 0; i < split.length; i++) {
			split[i] = split[i].trim();
		}
		HashSet<String> optionalParametersSet = new HashSet<String>(Arrays
				.asList(split));
		return optionalParametersSet;
	}

	@SuppressWarnings("unchecked")
	private DefaultFunctionCallInfo montarCallInfo(FunctionCall call,
			BeanTag beanTag) {
		if (call.getCall().equals("all")) {
			return null;
		}
		DefaultFunctionCallInfo callInfo;
		callInfo = new DefaultFunctionCallInfo();
		FunctionParameter[] parameterArray = call.getParameterArray();
		for (FunctionParameter param : parameterArray) {
			Class clazz;
			Object value;
			switch (param.getParameterType()) {
			case REFERENCE:
				PropertyDescriptor propertyDescriptor = beanTag.getBeanDescriptor().getPropertyDescriptor(param.getParameterValue());
				clazz = propertyDescriptor.getPropertyInfo().getClazz();
				value = propertyDescriptor.getPropertyInfo().getValue();
				break;
			case STRING:
				clazz = String.class;
				value = param.getParameterValue();
				break;
			case BOOLEAN:
				clazz = Boolean.class;
				value = new Boolean(param.getParameterValue());
				break;
			case USER:
				clazz = User.class;
				value = Neo.getRequestContext().getUser();
				break;
			default:
				throw new RuntimeException("Tipo de parametro n�o suportado: "
						+ param.getParameterType() + "   "
						+ param.getParameterValue());
			}
			callInfo.addParam(param.getParameterValue(), value, clazz);
		}
		return callInfo;
	}

	// utilizado em file
	public String getFileName() {
		if (value instanceof File) {
			String name2 = ((File) value).getName();
			if (Util.strings.isEmpty(name2)) {
				return "[vazio]";
			} else {
				return escape(name2);
			}

		}
		return "[vazio]";
	}

	private String createOption(String value, String label) {
		return createOption(value, label, false);
	}

	private String createOption(String value, String label, boolean selected) {
		return createOption(value, label, (selected ? "selected=\"selected\""
				: ""));
	}
	
	private String createOption(String value, String label, String selected) {

		if (selectedType != Type.SELECT_ONE_RADIO) {
			return "<option value='" + (value != null ? value : "<null>")
					+ "' " + selected + ">" + label + "</option>";
		} else {
			
			Map<String, Object> map = getDynamicAttributesMap();
			Set<Entry<String, Object>> entrySet = map.entrySet();
			String onclick = null;
			for (Entry<String, Object> entry : entrySet) {
				if(entry.getKey().toUpperCase().equals("ONCLICK")){
					onclick = entry.getValue().toString();
					break;
				}
			}
			
			
			String inputId = generateUniqueId() + "_opt_" + name;
			selected = selected != null && selected.length() > 0 ? "checked=\"checked\""
					: "";
			//c�digo novo alterado em 27/12/2007 por Jo�o Paulo Zica
			//altera��o para desabilitar campos do tipo radio em consulta
			String disabled = "";
			Boolean consultar = (Boolean)getRequest().getAttribute(CrudController.CONSULTAR);

			if (consultar != null && consultar == true ) {
				disabled = "disabled";
			}
			
			return "<input type='radio' id='" + inputId
					+ "' class='radioClass' value='"
					+ (value != null ? value : "<null>") + "' name='" + name
					+ "' " + selected +" " + disabled + " " 
					+ (onclick != null && !onclick.equals("") ? " onclick='" + onclick + "' " : "") 
					+ " /><label for='" + inputId + "'>"
					+ label + "</label>";
		}
	}

	public Object getOnKeyPress() {
		return getDAAtribute("onKeyPress", true);
	}

	public Object getOnKeyUp() {
		return getDAAtribute("onKeyUp", true);
	}

	private Object getDAAtribute(String string, boolean remove) {
		Set<String> keySet = getDynamicAttributesMap().keySet();
		for (String string2 : keySet) {
			if (string2.equalsIgnoreCase(string)) {
				if (remove) {
					return getDynamicAttributesMap().remove(string2);
				} else {
					return getDynamicAttributesMap().get(string2);
				}
			}
		}
		return null;
	}

	public String getFileOnChange() {
		// modificado por pedro em 31/07/07, pois quando seta como false o
		// removerbutton da erro de javascript
		String complemento = "";
		if (showRemoverButton) {
			complemento = "document.getElementById('" + name
					+ "_removerbtn').style.dysplay = '';";
		}
		
		int maxUploadSize = NeoWeb.getApplicationContext().getConfig().getMaxUploadSize();
		String onchangestring = "if(form['"+name+"'].files && form['"+name+"'].files[0].size>"+maxUploadSize+"){ alert('O arquivo selecionado n�o pode ter mais de "+maxUploadSize/1024/1024+" Mb. Por favor, selecione outro arquivo.'); form['"+name+"'].value = ''; return false;}" +
				"document.getElementById('" + name
				+ "_excludeField').value='false'; document.getElementById('"
				+ name + "_div').style.textDecoration = 'line-through'; "
				+ complemento + " ";
		String daOnChange = (String) getDAAtribute("onChange", true);
		if (daOnChange != null) {
			onchangestring = onchangestring + ";" + daOnChange;
		}
		return onchangestring;
	}

	public String getReloadOnChangeString() {
		String onchangestring = "";
		if (reloadOnChange != null && reloadOnChange) {
			FormTag form = findParent(FormTag.class, true);
			onchangestring = form.getName()
					+ ".validate = 'false'; "
					+ form.getName()
					+ ".suppressErrors.value = 'true';"
					+ form.getName()
					+ "."
					+ MultiActionController.ACTION_PARAMETER
					+ ".value = '"
					+ Util.strings.toString(((WebRequestContext) Neo
							.getRequestContext()).getLastAction(), "")
					+ "';"// MODIFICADO EM 10/08/2006
					+ form.getName() + ".suppressValidation.value = 'true';"
					+ form.getSubmitFunction() + "()";
		} else {
			ComboReloadGroupTag comboReloadGroupTag = findParent(ComboReloadGroupTag.class);
			if (comboReloadGroupTag != null) {
				if (selectedType == Type.SELECT_ONE_BUTTON) {
					// onchangestring = "alert();";
					onchangestring = comboReloadGroupTag.getFunctionName()
							+ "('" + getName() + "', form['" + name
							+ "'].value);";
				} else {
					onchangestring = comboReloadGroupTag.getFunctionName()
							+ "('" + getName() + "', this.value);";
				}

			}
		}
		String daOnChange = (String) getDAAtribute("onChange", true);
		if (daOnChange != null) {
			onchangestring = daOnChange + ";" + onchangestring;
		}
		return onchangestring;
	}

	public String getReloadOnClickString() {
		String onchangestring = "";
		if (reloadOnChange != null && reloadOnChange) {
			FormTag form = findParent(FormTag.class, true);
			onchangestring = form.getName() + ".validate = 'false'; "
					+ form.getName() + ".suppressErrors.value = 'true';"
					+ form.getName()
					+ "."
					+ MultiActionController.ACTION_PARAMETER
					+ ".value = '"
					+ Util.strings.toString(((WebRequestContext) Neo
							.getRequestContext()).getLastAction(), "")
					+ "';"// MODIFICADO EM 17/10/2016
					+ form.getName() + ".suppressValidation.value = 'true';"
					+ form.getSubmitFunction() + "()";
		} else {
			ComboReloadGroupTag comboReloadGroupTag = findParent(ComboReloadGroupTag.class);
			if (comboReloadGroupTag != null) {
				onchangestring = comboReloadGroupTag.getFunctionName() + "('"
						+ getName() + "');";
			}
		}
		String daOnClick = (String) getDAAtribute("onClick", true);
		if (daOnClick != null) {
			onchangestring = daOnClick + ";" + onchangestring;
		}
		return onchangestring;
	}

	@SuppressWarnings("unchecked")
	private Object doSelectAllFromService(InputTag lastInput) {
		Object dao;
		String beanName;
		String parentProperty = "";
		if (lastInput != null) {
			parentProperty = "by_" + lastInput.getName();
		}
		try {
			if (type instanceof Class
					|| autowiredType instanceof Class
					|| (type instanceof ParameterizedType && selectedType == Type.SELECT_MANY)) {
				Class usingType = getClassType();
				beanName = Util.strings.uncaptalize(usingType.getSimpleName());
				Object attribute = getRequest().getAttribute(
						beanName + "FINDALL_" + parentProperty);
				if (attribute == null) {
					dao = Neo.getApplicationContext().getBean(beanName + "DAO");
				} else {
					return attribute;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
		if (dao != null) {
			String[] extraFields = null;
			if (selectLabelProperty != null
					&& selectLabelProperty.trim().length() > 0) {
				extraFields = new String[] { selectLabelProperty };
			}
			if (lastInput == null) {
				GenericDAO<?> genericDao = (GenericDAO<?>) dao;
				List<?> findAll;
				try {
					findAll = genericDao.findForCombo(extraFields);
				} catch (Exception e) {
					// se acontecer algum erro no findForCombo.. tentar o
					// findAll
					log.warn("findForCombo (propriedade: " + name
							+ ") jogou exe��o: " + e.getMessage()
							+ "   Utilizando findAll()");
					findAll = genericDao.findAll();
				}
				getRequest().setAttribute(
						beanName + "FINDALL_" + parentProperty, findAll);
				return findAll;
			} else {
				GenericDAO<?> genericDAO = (GenericDAO<?>) dao;
				List<?> findAll;
				if (lastInput.getValue() != null
						&& !Util.beans.isTransient(lastInput.getValue())) {
					try {
						findAll = genericDAO.findBy(lastInput.getValue(), true,
								extraFields);
					} catch (Exception e) {
						// se acontecer algum erro no findForCombo.. tentar o
						// findAll
						log
								.warn("findBy("
										+ lastInput.getValue().getClass()
												.getSimpleName()
										+ ") (propriedade: "
										+ name
										+ ") jogou exe��o: "
										+ e.getMessage()
										+ "   Utilizando findBy sem especificar os campos");
						findAll = genericDAO.findBy(lastInput.getValue(),
								false, extraFields);
					}
				} else {
					findAll = new ArrayList();
				}
				getRequest().setAttribute(
						beanName + "FINDALL_" + parentProperty, findAll);
				return findAll;
			}
		}
		return null;
	}

	private String toString(List<String> organizeItens) {
		StringBuilder builder = new StringBuilder();
		for (String string : organizeItens) {
			builder.append(string);
			builder.append("\n");
		}
		return builder.toString();
	}

	/**
	 * Monta a lista de itens para ser colocado no combo (cada posicao do array
	 * � um option ou um optgroup)
	 * 
	 * @param itens
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<String> organizeItens(Object itens) {
		boolean sugest = false;
		boolean first = true;
		boolean consultar = false;
		if (autoSugestUniqueItem != null && autoSugestUniqueItem) {
			if (value == null) {
				sugest = true;
				try {
					if(getRequest().getAttribute("consultar") != null){
						consultar = (Boolean) getRequest().getAttribute("consultar");
					}
				} catch (Exception e) {}
			}
		}
		List<String> valores = new ArrayList<String>();
		if (itens == null) {
			return valores;
		}
		if (itens instanceof Map) {
			Map map = (Map) itens;
			Set keySet = map.keySet();
			sugest = sugest && keySet.size() == 1 && !consultar;
			for (Object key : keySet) {
				Object mapValue = map.get(key);
				if (mapValue instanceof Collection || mapValue instanceof Map
						|| mapValue.getClass().isArray()) {
					if (selectedType != Type.SELECT_ONE_RADIO)
						valores.add("<optgroup label=\"" + key + "\">");
					valores.addAll(organizeItens(mapValue));
					if (selectedType != Type.SELECT_ONE_RADIO)
						valores.add("</optgroup>");
				} else {
					if (sugest
							|| (first && value == null && includeBlank != null
									&& includeBlank == false && selectedType != Type.SELECT_MANY)) {
						value = key; // trocado de mapValue por key em
										// 22/01/2007
						propertySetter.set(value);
					}
					String opDesc = getSelectLabel(mapValue);
					String opValue = getObjectValueToString(key, false);
					opValue = escapeSingleQuotes(opValue);
					String selected = getSelectedString(key);
					valores.add(createOption(opValue, opDesc, selected)); // "<option
					// value='"+opValue+"'"+selected+">"+opDesc+"</option>")
				}
				first = false;
			}
		} else if (itens instanceof Collection) {
			Collection collection = (Collection) itens;
			sugest = collection.size() == 1 && sugest  && !consultar;
			for (Object object : collection) {
				if (sugest
						|| (first && value == null && includeBlank != null
								&& includeBlank == false && selectedType != Type.SELECT_MANY)) {
					value = object;
					propertySetter.set(value);
				}
				String opDesc = getSelectLabel(object);
				String opValue = getObjectValueToString(object, false);
				opValue = escapeSingleQuotes(opValue);
				String selected = getSelectedString(object);
				valores.add(createOption(opValue, opDesc, selected));
				first = false;
			}
		} else if (itens.getClass().isArray()) {
			Object[] collection = (Object[]) itens;
			sugest = collection.length == 1 && sugest  && !consultar;
			for (Object object : collection) {
				if (sugest
						|| (first && value == null && includeBlank != null && includeBlank == false)) {
					value = object;
					propertySetter.set(value);
				}
				String opDesc = getSelectLabel(object);
				String opValue = getObjectValueToString(object, false);
				opValue = escapeSingleQuotes(opValue);
				String selected = getSelectedString(object);
				valores.add(createOption(opValue, opDesc, selected));
				first = false;
			}
		}
		return valores;
	}

	private String getSelectLabel(Object value) {
		if (selectLabelProperty != null
				&& selectLabelProperty.trim().length() != 0 && value != null) {
			return getObjectDescriptionToString(Neo.getApplicationContext()
					.getBeanDescriptor(value).getPropertyDescriptor(
							getSelectLabelProperty()).getValue());
		} else {
			return getObjectDescriptionToString(value);
		}
	}

	@SuppressWarnings("unchecked")
	private String getSelectedString(Object optionValue) {
		String selected = "";
		if (value instanceof Collection) {
			Collection collection = (Collection) value;
			for (Object object : collection) {
				boolean eq = areEqual(optionValue, object);
				selected = eq ? " selected='selected'" : "";
				if (eq) {
					break;
				}
			}
		} else if (value != null && value.getClass().isArray()) {
			Object[] array = (Object[]) value;
			for (Object object : array) {
				boolean eq = areEqual(optionValue, object);
				selected = eq ? " selected='selected'" : "";
				if (eq) {
					break;
				}
			}
		} else {
			selected = areEqual(optionValue, value) ? " selected='selected'"
					: "";
		}
		return selected;
	}

	protected void autowireAttributes() {
		if (autowire) {
			autowiredType = getPageContext().findAttribute("type");
			if (type == null) {
				type = getPageContext().findAttribute("type");
			}
			if (name == null) {
				try {
					name = (String) getPageContext().findAttribute("name");
				} catch (ClassCastException e) {
					// ignorar se nao for string
				}
			}
			if (value == null) {
				value = getPageContext().findAttribute("value");
			}
			if (propertySetter == null) {
				propertySetter = (PropertySetter) getPageContext()
						.findAttribute("propertySetter");
				if (propertySetter == null) {
					propertySetter = new PropertySetter() {
						public void set(Object value) {
							log
									.warn(name
											+ " tentou setar o valor da propriedade, mas n�o existe propertySetter para essa propriedade");
						}
					};
				}
			}
			if (required == null) {
				try {
					if (ignoreRequired)
						required = false;
					else
						required = (Boolean) getPageContext().findAttribute("required");
				} catch (ClassCastException e) {
					// ignorar se nao for boolean
				}
			}
			if (Util.strings.isEmpty(label)) {
				try {
					label = getPageContext().findAttribute("label").toString();
				} catch (NullPointerException e) {
					// ignorar se nao existir o atributo
				}
			}
			if (annotations == null) {
				try {
					annotations = (Annotation[]) getPageContext()
							.findAttribute("annotations");
				} catch (Exception e) {
					annotations = new Annotation[0];
				}
				if (annotations == null) {
					annotations = new Annotation[0];
				}
			}
			// verificar se � required
			if (required == null) {
				if (ignoreRequired)
					required = false;
				else{
					for (Annotation ann : annotations) {
						if (ann instanceof Required) {
							required = true;
							break;
						}
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Type chooseType() {
		Type selectedType;
		if (Util.objects.isEmpty(type)) {
			if (value == null) {
				throw new NullPointerException(
						"Em uma tag input o atributo value e o type n�o podem ser ambos nulos");
			}
			Class c = value.getClass();
			selectedType = chooseTypeByClass(c);
		} else {
			if (type instanceof String) {
				String typeString = ((String) type).toUpperCase().replaceAll(
						"-", "_");
				try {
					Type valueOf = Type.valueOf(typeString);
					selectedType = valueOf;
				} catch (IllegalArgumentException e) {
					throw new NeoException("Type n�o suportado por input: "
							+ type);
				}
			} else if (type instanceof Class) {
				selectedType = chooseTypeByClass((Class) type);
			} else if (type instanceof ParameterizedType) {
				selectedType = chooseTypeByClass((Class) ((ParameterizedType) type)
						.getRawType());
			} else if (type instanceof Collection) {
				selectedType = Type.SELECT_MANY;
			} else {
				throw new IllegalArgumentException("Input n�o suporta valor '"
						+ type + "' no atributo type");
			}
		}
		if (selectedType == Type.TEXT
				&& ((rows != null && rows != 0) || (cols != null && cols != 0))
				&& !"text".equals(type)) {
			selectedType = Type.TEXT_AREA;
		}
		if (selectedType == Type.TEXT) {
			for (Annotation ann : annotations) {
				if (ann instanceof Password) {
					selectedType = Type.PASSWORD;
				}
			}
		}
		if (selectedType == Type.CHECKBOX && itens != null && !"".equals(itens)
				&& value != null) {
			selectedType = Type.CHECKLIST;
		}
		if (type == null && itens != null && !"".equals(itens)
				&& selectedType != Type.SELECT_MANY) {
			selectedType = Type.SELECT_ONE;
		}
		if (selectedType == Type.SELECT_ONE
				&& Util.strings.isNotEmpty(selectOnePath)) {
			selectedType = Type.SELECT_ONE_BUTTON;
		}
		if (selectedType == Type.SELECT_ONE
				&& Util.strings.isNotEmpty(insertPath)) {
			selectedType = Type.SELECT_ONE_INSERT;
		}
		return selectedType;
	}

	@SuppressWarnings("unchecked")
	private Type chooseTypeByClass(Class c) {
		if (Map.class.isAssignableFrom(c)) {
			throw new IllegalArgumentException(
					"O input n�o suporta valores do tipo Map");
		}
		if (Enum.class.isAssignableFrom(c)) {
			return Type.SELECT_ONE;
		}
		if (File.class.isAssignableFrom(c)) {
			return Type.FILE;
		}
		if (isEntity(c)) {
			return Type.SELECT_ONE;
		}
		if (Collection.class.isAssignableFrom(c)) {
			return Type.SELECT_MANY;
		}

		if (itens != null) {
			return Type.SELECT_ONE;
		}
		Type type2 = mapaTiposClasses.get(c);
		if (type2 == null)
			type2 = Type.TEXT;
		return type2;
	}

	public String getValueToString() {
		if (value == null && selectedType == Type.HIDDEN) {
			return "<null>";
		} else if (value instanceof String) {
			return (String) value;
		}
		if (pattern != null && value != null) {
			try {
				if (selectedType == Type.DATE || selectedType == Type.TIME || selectedType == Type.TIMESTAMP || selectedType== Type.DATE_HTML5) {
					if (value instanceof Calendar) {
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								pattern);
						return dateFormat.format(((Calendar) value).getTime());
					} else {
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								pattern);
						return dateFormat.format(value);
					}
				} else if (selectedType == Type.FLOAT) {
					DecimalFormat decimalFormat = new DecimalFormat(pattern);
					String format = decimalFormat.format(value);
					if (format.startsWith(",")) {
						format = "0" + format;
					}
					return format;
				}
			} catch (Exception e) {
				throw new NeoException("Erro ao utilizar pattern. Valor: "
						+ value.getClass().getName() + " " + value + "   ."
						+ e.getMessage(), e);
			}
		}
		return getObjectValueToString(value, false);
	}

	public String getEscapeValueToString() {
		if (value == null) {
			if (selectedType == Type.HIDDEN) {
				return "<null>";
			} else if (selectedType == Type.TEXT) {
				return "";
			}
		}
		String opValue;
		if (selectedType == Type.TEXT && hasId(value.getClass())) {
			opValue = getObjectDescriptionToString(value);
		} else {
			Object includeFieldAtribute = getDAAtribute("includeField", false);
			if(includeFieldAtribute != null && !includeFieldAtribute.toString().isEmpty()){
				autocompleteLabelProperty = includeFieldAtribute.toString();
				try {
					opValue = getIdAutocomplete();
				} catch (Exception e) {
					opValue = getObjectValueToString(value, false);
				}
			} else {
				opValue = getObjectValueToString(value, false);
			}
		}

		if (opValue == null)
			return null;

		return opValue.replaceAll("\\\\", "\\\\").replaceAll("\"", "&#34;");
	}

	public String getValueWithDescriptionToString() {
		if (value == null) {
			return "<null>";
		}
		return getObjectValueToString(value, true);
	}

	public Boolean getWrite() {
		return write;
	}

	public void setWrite(Boolean write) {
		this.write = write;
	}

	public String getBooleanDescriptionToString() {

		if (write != null && write) {
			String booleanDescription = null;
			try {
				// se nao for boolean utilizar o m�todo normal
				if ((value instanceof Boolean || value == null)
						&& Util.strings.isNotEmpty(trueFalseNullLabels)) {
					String[] split = trueFalseNullLabels.split(",");
					String trueString = split[0];
					String falseString = split[1];
					String nullString = "";
					if (split.length == 3) {
						nullString = split[2];
					}
					if (value == null) {
						booleanDescription = nullString;
					} else if (value instanceof Boolean) {
						if (((Boolean) value)) {
							booleanDescription = trueString;
						} else {
							booleanDescription = falseString;
						}
					}
					return booleanDescription;
				} else if (value instanceof Boolean) {
					if (((Boolean) value)) {
						booleanDescription = "Sim";
					} else {
						booleanDescription = "N�o";
					}
					return booleanDescription;
				} else if ((value instanceof Number)
						&& Util.strings.isNotEmpty(pattern)) {
					DecimalFormat decimalFormat = new DecimalFormat(pattern);
					return decimalFormat.format(value);
				} else if (value instanceof File){
					return getArquivoLink();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				throw new NeoException(
						"trueFalseNullLabels inv�lido "
								+ trueFalseNullLabels
								+ ". Esse atributo deve ser uma string separada por v�rgula indicando o valor de TRUE FALSE e NULL. ex.: sim,n�o,vazio");
			}

			return getObjectDescriptionToString(value);
		} else {
			return "";
		}
	}

	public String getDescriptionToString() {
		return getObjectDescriptionToString(value);
	}

	public String getSelectOneButtonOnClick() {
		if (selectOnePath == null) {
			throw new NeoException(
					"Quando o tipo do input for select-one-button o atributo selectOnePath � obrigat�rio");
		}
		Config config = Neo.getApplicationContext().getConfig();

		String contextPath = getRequest().getContextPath();
		String fullPath = contextPath + SelecionarCadastrarServlet.SELECIONAR_CADASTRAR_PATH + selectOnePath;

		if (Util.strings.isNotEmpty(selectOnePathParameters)) {
			if (!fullPath.contains("?")) {
				fullPath += "?";
			}
			if (selectOnePathParameters.startsWith("javascript:")) {
				fullPath += "' +" + selectOnePathParameters.substring("javascript:".length()) + " + '";
			} else {
				fullPath += selectOnePathParameters;
			}
		}
		if (Util.strings.isNotEmpty(config.getSelectOnePathParameters())) {
			String parameters = config.getSelectOnePathParameters();
			if (!fullPath.contains("?")) {
				fullPath += "?";
			}
			if (parameters.startsWith("javascript:")) {
				fullPath += "' +" + parameters.substring("javascript:".length()) + " + '";
			} else {
				fullPath += parameters;
			}
		}

		FormTag form = findParent(FormTag.class, true);
		String typeString = "java.lang.Object";
		if (type instanceof Class<?>) {
			typeString = ((Class<?>) type).getName();
		}
		Object atribute = getDAAtribute("popupstr", true);
		String popupStr = null;
		
		if(atribute == null) {
			popupStr = "width=" + config.getSelectOnePopupWidth() + ", " +
					   "height=" + config.getSelectOnePopupHeight() + ", " +
					   "top=50," +
					   "left=0," +
					   "resizable," +
					   "scrollbars";
		} else
			popupStr = atribute.toString();
		
		String onclick = "var c = new selecionarCallbackObject("
				+ form.getName()
				+ "['"
				+ getName()
				+ "'], "
				+ form.getName()
				+ "['"
				+ getName()
				+ "_label'], '"
				+ typeString
				+ "', "
				+ form.getName()
				+ "['"
				+ getName()
				+ "_btn'], "
				+ form.getName()
				+ "['"
				+ getName()
				+ "_btnUnselect']"
				+ "); var win = open('"
				+ fullPath
				+ "','filha"
				+ ((int) (Math.random() * 100000))
				+ "','" + popupStr + "'); setTimeout(function(){win.selecionarCallback = c;}, 500);";
		return onclick;
	}
	
	@SuppressWarnings("unchecked")
	public String getInsertOneButtonOnClickAutocomplete() {
		if (selectOnePath == null) {
			throw new NeoException(
					"Quando o tipo do input for select-one-insert o atributo insertPath � obrigat�rio");
		}
		Config config = Neo.getApplicationContext().getConfig();
		
		String contextPath = getRequest().getContextPath();
		String fullPath = contextPath + SelecionarCadastrarServlet.SELECIONAR_CADASTRAR_PATH + insertPath;
		if (fullPath.contains("?")) {
			fullPath += "&";
		} else {
			fullPath += "?";
		}
		fullPath += MultiActionController.ACTION_PARAMETER + "=" + AbstractCrudController.CRIAR + "&fromInsertOne=true" + "&includeDescriptionFromInsertOne=true";
		
		if (Util.strings.isNotEmpty(insertOnePathParameters)) {
			if (insertOnePathParameters.startsWith("javascript:")) {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + insertOnePathParameters.substring("javascript:".length());
			} else {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + insertOnePathParameters;
			}
		}
		if (Util.strings.isNotEmpty(config.getInsertOnePathParameters())) {
			String parameters = config.getInsertOnePathParameters();
			if (parameters.startsWith("javascript:")) {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + parameters.substring("javascript:".length());
			} else {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + parameters;
			}
		}
		
		FormTag form = findParent(FormTag.class, true);
		String typeString = "java.lang.Object";
		if (getClassType() instanceof Class) {
			typeString = getClassType().getName();
		}
		
		Object atribute = getDAAtribute("popupstr", true);
		String popupStr = null;
		
		if(atribute == null) {
			popupStr = "width=" + config.getSelectOnePopupWidth() + ", " +
					   "height=" + config.getSelectOnePopupHeight() + ", " +
					   "top=50," +
					   "left=0," +
					   "resizable," +
					   "scrollbars";
		} else
			popupStr = atribute.toString();
		
		String onclick = "var c = new selecionarCallbackObject("
			+ form.getName() + "['" + getName() + "'], "
			+ form.getName() + "['" + getName() + "_label'], " 
			+ "'" + typeString + "', "
			+ "null, null, " 
			+ "function(label,valor){"
			
			+ "$('input[name=" + getName() + "_label]').val(label);"
			+ "$('input[autocompleteId=" + getName() + "_value]').val(valor);"
			+ "$('input[autocompleteId=" + getName() + "_value]').change();"
			+ "$('input[name=" + getName() + "_label]').unautocomplete();"
			+ "verificaBotoes('" + getName() + "'); "
//			+ "$('input[name=" + getName() + "_label]').change();"
			
			+ "}); " 
			
			+ "var win = open('" + fullPath + "','filha" + ((int) (Math.random() * 100000)) + "','" + popupStr + "'); " 
			
			+ "setTimeout(function(){win.selecionarCallback = c;}, 500);";
		return onclick;
	}

	@SuppressWarnings("unchecked")
	public String getSelectOneInsertOnClick() {
		if (selectOnePath == null) {
			throw new NeoException(
					"Quando o tipo do input for select-one-insert o atributo insertPath � obrigat�rio");
		}
		Config config = Neo.getApplicationContext().getConfig();
		
		String contextPath = getRequest().getContextPath();
		String fullPath = contextPath + SelecionarCadastrarServlet.SELECIONAR_CADASTRAR_PATH + insertPath;
		if (fullPath.contains("?")) {
			fullPath += "&";
		} else {
			fullPath += "?";
		}
		fullPath += MultiActionController.ACTION_PARAMETER + "=" + AbstractCrudController.CRIAR + "&fromInsertOne=true";
		
		if (Util.strings.isNotEmpty(insertOnePathParameters)) {
			if (insertOnePathParameters.startsWith("javascript:")) {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + insertOnePathParameters.substring("javascript:".length());
			} else {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + insertOnePathParameters;
			}
		}
		if (Util.strings.isNotEmpty(config.getInsertOnePathParameters())) {
			String parameters = config.getInsertOnePathParameters();
			if (parameters.startsWith("javascript:")) {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + parameters.substring("javascript:".length());
			} else {
				fullPath += (!fullPath.endsWith("&") ? "&" : "") + parameters;
			}
		}
		
		FormTag form = findParent(FormTag.class, true);
		String typeString = "java.lang.Object";
		if (type instanceof Class) {
			typeString = ((Class) type).getName();
		}
		
		Object atribute = getDAAtribute("popupstr", true);
		String popupStr = null;
		
		if(atribute == null) {
			popupStr = "width=" + config.getSelectOnePopupWidth() + ", " +
					   "height=" + config.getSelectOnePopupHeight() + ", " +
					   "top=50," +
					   "left=0," +
					   "resizable," +
					   "scrollbars";
		} else
			popupStr = atribute.toString();
		
		String onclick = "var c = new selecionarCallbackObject("
				+ form.getName()
				+ "['"
				+ getName()
				+ "'], "
				+ "null, '"
				+ typeString
				+ "', "
				+ "null, null, function(label,valor){"
				+ form.getName()
				+ "['"
				+ getName()
				+ "'].options.add(new Option(label, valor, false, true)); "
				+ (insertOnePathParameters!=null && insertOnePathParameters.contains("selectOnChange=true") ? form.getName() + "['" + getName() + "'].onchange(); " : "")
				+ "}); var win = open('"
				+ fullPath
				+ "','filha"
				+ ((int) (Math.random() * 100000))
				+ "','" + popupStr + "');" + " setTimeout(function(){win.selecionarCallback = c;}, 500); ";
		
		return onclick;
	}

	public String getChecked() {
		if (checked != null) {
			return checked ? "checked" : null;
		}
		if (value != null && new Boolean(value.toString())) {
			return "checked";
		}
		return null;
	}

	public Boolean getAutowire() {
		return autowire;
	}

	public Integer getCols() {
		return cols;
	}

	public Boolean getIncludeBlank() {
		return includeBlank;
	}
	
	public Object getItens() {
		return itens;
	}

	public String getName() {
		return name;
	}

	public Boolean getRequired() {
		return required;
	}

	public Integer getRows() {
		return rows;
	}

	public Object getType() {
		return type;
	}

	public Object getValue() {
		return value;
	}
	
	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public void setAutowire(Boolean autowire) {
		this.autowire = autowire;
	}

	public void setCols(Integer cols) {
		this.cols = cols;
	}

	public void setIncludeBlank(Boolean includeBlank) {
		this.includeBlank = includeBlank;
	}
	
	public void setItens(Object itens) {
		this.itens = itens;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public void setType(Object type) {
		this.type = type;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getSelectItensString() {
		return selectItensString;
	}

	public String getSelectoneblankoption() {
		return selectoneblankoption;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Boolean getShowLabel() {
		return showLabel;
	}

	public void setShowLabel(Boolean showLabel) {
		this.showLabel = showLabel;
	}

	public String getSelectOnePath() {
		return selectOnePath;
	}

	public void setSelectOnePath(String selectOnePath) {
		this.selectOnePath = selectOnePath;
	}

	public Boolean getReloadOnChange() {
		return reloadOnChange;
	}

	public void setReloadOnChange(Boolean reloadOnChange) {
		this.reloadOnChange = reloadOnChange;
	}

	public Boolean getOnlyPositiveNumbers() {
		return onlyPositiveNumbers;
	}
	
	public void setOnlyPositiveNumbers(Boolean onlyPositiveNumbers) {
		this.onlyPositiveNumbers = onlyPositiveNumbers;
	}
	
	public String getTrueFalseNullLabels() {
		return trueFalseNullLabels;
	}

	public void setTrueFalseNullLabels(String trueFalseNullValues) {
		this.trueFalseNullLabels = trueFalseNullValues;
	}

	public String getShowRemoverBtn() {
		if (value instanceof File) {
			String name2;
			Long cdfile = ((File) value).getCdfile();
			try {
				name2 = ((File) value).getName();
			} catch (Exception e) {
				return "style=\"color: red\"";
			}

			if (Util.strings.isEmpty(name2) || cdfile == null) {
				return "style=\"display: none\"";
			} else {
				return "";
			}

		}
		return "style=\"display: none\"";
	}

public String getArquivoLink() {
		if(showFileLink) {
			String fileName;
			try {
				fileName = getFileName();
			} catch (Exception e) {
				return "<span style=\"color: red\"><B>Erro ao adquirir nome do arquivo.</B> "
						+ e.getMessage() + "</span>";
			}
			if (transientFile != null && transientFile) {
				return "<span id=\"" + getName() + "_div\">" + SinedUtil.getNomeArquivoReduzido(fileName, 50) + "</span>";
			} 
			if (fileName.equals("[vazio]")) {
				return "<span id=\"" + getName() + "_div\">" + fileName + "</span>";
			} else {
				Long cdfile;
				try {
					cdfile = ((File) value).getCdfile();
				} catch (Exception e) {
					return "<span id=\""
							+ getName()
							+ "_div\" style=\"color: red\"><B>Ocorreu um erro ao adquirir o c�digo do arquivo.</B> "
							+ e.getMessage() + "</span>";
				}
				// dar autorizacao para fazer o download do arquivo
				DownloadFileServlet.addCdfile(getRequest().getSession(), cdfile);
				return cdfile == null ? "<span id=\"" + getName() + "_div\">[Escolha o arquivo novamente]</span>"
						: "<a href=\"" + getRequest().getContextPath()
								+ "/DOWNLOADFILE/" + cdfile + "\">" + "<span id=\""
								+ getName() + "_div\">" + SinedUtil.getNomeArquivoReduzido(fileName, 50) + "</span>"
								+ "</a>";
			}
		}else {
			return "";
		}
		
	}

	public String getFileValue() {
		if (value instanceof File) {
			String name2 = ((File) value).getName();
			Long cdfile = ((File) value).getCdfile();
			if (Util.strings.isEmpty(name2) || cdfile != null) {
				return "";
			} else {
				return escape(name2);
			}
		}
		return "";
	}

	public String getSelectOneUnselectButtonStyle() {
		if (Util.objects.isEmpty(value) || value.equals("<null>")) {
			return "display:none";
		} else {
			return "";
		}
	}

	public String getSelectOneButtonStyle() {
		if (Util.objects.isEmpty(value) || value.equals("<null>")) {
			return "";
		} else {
			return "display:none";
		}
	}

	public String getSelectLabelProperty() {
		return selectLabelProperty;
	}

	public void setSelectLabelProperty(String selectLabelProperty) {
		this.selectLabelProperty = selectLabelProperty;
	}

	public String getCheckboxValue() {
		return checkboxValue;
	}

	public String getLabelStyle() {
		return labelStyle;
	}

	public String getLabelStyleClass() {
		return labelStyleClass;
	}

	public void setLabelStyle(String labelStyle) {
		this.labelStyle = labelStyle;
	}

	public void setLabelStyleClass(String labelStyleClass) {
		this.labelStyleClass = labelStyleClass;
	}

	public Boolean getUseAjax() {
		return useAjax;
	}

	public void setUseAjax(Boolean useAjax) {
		this.useAjax = useAjax;
	}

	public String getOnLoadItens() {
		return onLoadItens;
	}

	public void setOnLoadItens(String onLoadItens) {
		this.onLoadItens = onLoadItens;
	}

	public Boolean getAutoSugestUniqueItem() {
		return autoSugestUniqueItem;
	}

	public String getBlankLabel() {
		return blankLabel;
	}

	public void setAutoSugestUniqueItem(Boolean autoSugestUniqueItem) {
		this.autoSugestUniqueItem = autoSugestUniqueItem;
	}

	public Boolean getBooleanValue() {
		return booleanValue;
	}

	public Boolean getTransientFile() {
		return transientFile;
	}

	public void setTransientFile(Boolean transientFile) {
		this.transientFile = transientFile;
	}

	public void setBlankLabel(String blankLabel) {
		this.blankLabel = blankLabel;
	}

	public String getOptionalParams() {
		return optionalParams;
	}

	public void setOptionalParams(String optionalParams) {
		this.optionalParams = optionalParams;
	}

	public boolean isForceValidation() {
		return forceValidation;
	}

	public void setForceValidation(boolean forceValidation) {
		this.forceValidation = forceValidation;
	}

	public boolean isShowRemoverButton() {
		return showRemoverButton;
	}

	public void setShowRemoverButton(boolean showRemoverButon) {
		this.showRemoverButton = showRemoverButon;
	}

	public Boolean getHoldValue() {
		return holdValue;
	}
	
	public Boolean getShowFileLink() {
		return showFileLink;
	}
	
	public void setShowFileLink(Boolean showFileLink) {
		this.showFileLink = showFileLink;
	}
	public void setHoldValue(Boolean holdValue) {
		this.holdValue = holdValue;
	}

	public String getInsertPath() {
		return insertPath;
	}

	public String getPattern() {
		return pattern;
	}

	public String getSelectOnePathParameters() {
		return selectOnePathParameters;
	}

	public void setInsertPath(String insertPath) {
		this.insertPath = insertPath;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public void setSelectOnePathParameters(String selectOnePathParameters) {
		this.selectOnePathParameters = selectOnePathParameters;
	}
	
	public void setInsertOnePathParameters(String insertOnePathParameters) {
		this.insertOnePathParameters = insertOnePathParameters;
	}
	
	public String getInsertOnePathParameters() {
		return insertOnePathParameters;
	}

	public String getLoadFunctionAutocomplete() {
		return loadFunctionAutocomplete;
	}

	public void setLoadFunctionAutocomplete(String loadFunctionAutocomplete) {
		this.loadFunctionAutocomplete = loadFunctionAutocomplete;
	}

	public String getAutocompleteLabelProperty() {
		return autocompleteLabelProperty;
	}

	public String getAutocompleteMatchProperty() {
		return autocompleteMatchProperty;
	}

	public void setAutocompleteLabelProperty(String autocompleteLabelProperty) {
		this.autocompleteLabelProperty = autocompleteLabelProperty;
	}

	public void setAutocompleteMatchProperty(String autocompleteMatchProperty) {
		this.autocompleteMatchProperty = autocompleteMatchProperty;
	}

	public String getAutocompleteGetterLabel() {
		return autocompleteGetterLabel;
	}

	public void setAutocompleteGetterLabel(String autocompleteGetterLabel) {
		this.autocompleteGetterLabel = autocompleteGetterLabel;
	}
	
	public String getAutocompleteDescriptionLabel() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		if(value != null && autocompleteGetterLabel != null && !autocompleteGetterLabel.equals("")){
			
			Class<?> clazz = value.getClass();
			Method m1 = clazz.getMethod(autocompleteGetterLabel);
			Object invoke = m1.invoke(value);
			return invoke != null ? invoke.toString() : "";
			
		} else return getDescriptionToString();
	}
	
	public String getAutocompleteIdWithDescription() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		if(value != null && autocompleteLabelProperty != null && !autocompleteLabelProperty.equals("")){
			
			String id = getIdAutocomplete();
			return id;
		} else return getValueWithDescriptionToString();	
	}

	private String getIdAutocomplete() throws IllegalAccessException,
			InvocationTargetException {
		Class<?> clazz = value.getClass();
		
		String id = clazz.getName();
		id += "[";
		
		BeanDescriptor<?> beanDescriptor = new BeanDescriptorFactoryImpl().createBeanDescriptor(null, clazz);
		id += beanDescriptor.getIdPropertyName() + "=" + Util.beans.getGetterMethod(clazz, beanDescriptor.getIdPropertyName()).invoke(value);
		
		id += ",";
		String[] array = autocompleteLabelProperty.split(",");
		String campo;
		Object invoke;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		for (int i = 0; i < array.length; i++) {
			campo = array[i].substring(array[i].lastIndexOf(".")+1, array[i].length());
			invoke = Util.beans.getGetterMethod(clazz, campo).invoke(value);
			if(invoke != null){
				id += campo + "=";
				if(invoke instanceof Date || invoke instanceof java.util.Date){
					 id += Util.strings.addScapesToDescription(format.format(invoke));
				 } else {
					 id += Util.strings.addScapesToDescription(invoke.toString());
				 }
				id += ",";
			}
		}
		
		id = id.substring(0, id.length()-1);
		id += "]";
		return id;
	}
	
	public boolean isIgnoreRequired() {
		return ignoreRequired;
	}

	public void setIgnoreRequired(boolean ignoreRequired) {
		this.ignoreRequired = ignoreRequired;
	}
	
}
