<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="neo"%>

<form method="${tag.method}" enctype="${tag.enctype}" name="${tag.name}" action="${tag.url}" ${tag.dynamicAttributesToString} onsubmit="return false;">
	<input type="hidden" name="${tag.actionParameter}" value="${tag.action}"/>
	<input type="hidden" name="suppressValidation" value="false"/>
	<input type="hidden" name="suppressErrors" value="false"/>
	<script language="javascript">
		var ${tag.name} = document.forms["${tag.name}"];
		${tag.name}.validate ='${tag.validate}';
		function ${tag.submitFunction}() {
			var validar = ${tag.name}.validate;
			var validarCampoObrigatorio = validar;
			try {
				${tag.validateFunction};
			} catch (e) {
				validar = false;
			}
			
			if(typeof validarCampoObrigatorio != 'undefined' && validarCampoObrigatorio =='true' && typeof $CampoObrigatorio != 'undefined'){
				if(!$CampoObrigatorio.validaRequiredFields()){
					$.unblockUI();
					return false;
				}
			}
			
			try {
				clearMessages();//limpa as mensagens que vieram do servidor
			} catch(e){
			}
			if(validar == 'true') {
				var valid = ${tag.validateFunction}();
				if(valid) {
					if($Neo.blockSubmit.canSubmit()){
						${tag.name}.submit();
					}else{
						$Neo.blockSubmit.message();
					}
				}
			} else {
				var verifiedSession = ${tag.verifySession} ? $Neo.util.verifySession() : true;
				if(verifiedSession){
					if(typeof ${tag.name}.ACAO != 'undefined' && 
						typeof ${tag.name}.ACAO.value != 'undefined' && 
						(${tag.name}.ACAO.value == "listagem" || 
								${tag.name}.ACAO.value == null || 
								${tag.name}.ACAO.value == "")){
						$.blockUI.defaults.pageMessage = '<h1>Carregando...</h1>';
						$.blockUI();
					}
					if($Neo.blockSubmit.canSubmit()){
						console.log("submitando...");
						${tag.name}.submit();
					}else{
						$Neo.blockSubmit.message();
					}
				}
			}
		}
	</script>
	<n:doBody/>
</form>