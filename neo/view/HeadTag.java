/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view;

import java.util.Iterator;
import java.util.Set;

public class HeadTag extends BaseTag {
	
	protected boolean includeDefaultCss = true;
	protected boolean includeThemeCss = true;
	protected boolean includeUtilJs = true;
	protected boolean searchCssDir = true;
	protected boolean searchJsDir = true;
	protected boolean includeAutocomplete = false;


	@SuppressWarnings("unchecked")
	@Override
	protected void doComponent() throws Exception {
		//procurar css
		Set<String> resourcePathsCss = getServletContext().getResourcePaths("/css");
		if (resourcePathsCss != null) {
			for (String string : resourcePathsCss) {
				if (string.endsWith("default.css")) {
					includeDefaultCss = false;
				}
				if (string.endsWith("theme.css")) {
					includeThemeCss = false;
				}
			}
			
			for (Iterator<?> iterator = resourcePathsCss.iterator(); iterator.hasNext();) {
				String str = (String) iterator.next();
				if(!str.endsWith(".css")){
					iterator.remove();
				}
			}
		}
		
		
		
		Set<String> resourcePathsJs = getServletContext().getResourcePaths("/js");
		if (resourcePathsJs != null) {
			for (String string : resourcePathsJs) {
				if (string.endsWith("util.js")) {
					includeUtilJs = false;
				}
			}
			
			for (Iterator<?> iterator = resourcePathsJs.iterator(); iterator.hasNext();) {
				String str = (String) iterator.next();
				if(!str.endsWith(".js")){
					iterator.remove();
				}
			}
		}
		pushAttribute("jss", resourcePathsJs);
		pushAttribute("csss", resourcePathsCss);
		pushAttribute("searchJsDir", this.searchJsDir);
		pushAttribute("searchCssDir", this.searchCssDir);
		pushAttribute("includeAutocomplete", this.includeAutocomplete);
		includeJspTemplate();
		popAttribute("includeAutocomplete");
		popAttribute("searchJsDir");
		popAttribute("searchCssDir");
		popAttribute("csss");
		popAttribute("jss");
	}

	public boolean isIncludeDefaultCss() {
		return includeDefaultCss;
	}

	public void setIncludeDefaultCss(boolean includeDefault) {
		this.includeDefaultCss = includeDefault;
	}

	public boolean isIncludeUtilJs() {
		return includeUtilJs;
	}

	public void setIncludeUtilJs(boolean includeUtilJs) {
		this.includeUtilJs = includeUtilJs;
	}

	public boolean isIncludeThemeCss() {
		return includeThemeCss;
	}

	public void setIncludeThemeCss(boolean includeThemeCss) {
		this.includeThemeCss = includeThemeCss;
	}

	public boolean isSearchCssDir() {
		return searchCssDir;
	}

	public boolean isSearchJsDir() {
		return searchJsDir;
	}

	public void setSearchCssDir(boolean searchCssDir) {
		this.searchCssDir = searchCssDir;
	}

	public void setSearchJsDir(boolean searchJsDir) {
		this.searchJsDir = searchJsDir;
	}

	public boolean isIncludeAutocomplete() {
		return includeAutocomplete;
	}

	public void setIncludeAutocomplete(boolean includeAutocomplete) {
		this.includeAutocomplete = includeAutocomplete;
	}
	
	
}
