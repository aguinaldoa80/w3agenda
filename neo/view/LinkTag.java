/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view;

import br.com.linkcom.neo.controller.MultiActionController;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.template.PropertyConfigTag;

/**
 * @author rogelgarcia
 * @since 26/01/2006
 * @version 1.1
 */
public class LinkTag extends BaseTag {

	// atributos
	protected String confirmationMessage;//mensagem de confirmacao.. (janela javascript)
	
	protected String confirmationScript;
	
	protected String url;

	protected String action;

	protected String img;

	protected String description;

	protected String type;

	protected String parameters;
	
	protected Boolean checkPermission = false;
	

	enum Tipo {
		BUTTON, IMAGE, LINK
	}

	// extra


	private String onclick;

	@Override
	protected void doComponent() throws Exception {
		
		if(checkPermission) {
			boolean hasAuthorization = hasAuthorization();
			
			if(!hasAuthorization){
				getOut().println("<!-- Sem autorização para acessar: "+url+"-->");
				return;
			}
		}
		url = montarUrlCompleta();
		//corpo = getBody();
		Tipo tipo = definirTipo();
		
		if(tipo == Tipo.BUTTON){
			boolean disabled = "disabled".equals(getDynamicAttributesMap().get("disabled"));
			boolean enabled = "false".equals(getDynamicAttributesMap().get("disabled"));
			
			if(!enabled){
				PropertyConfigTag propertyConfig = findParent(PropertyConfigTag.class);
				DataGridTag dataGridTag = findParent(DataGridTag.class);
				if(propertyConfig != null && Boolean.TRUE.equals(propertyConfig.getDisabled())
						&& (dataGridTag == null || dataGridTag.getCurrentStatus() != DataGridTag.Status.DYNALINE)){
					if(disabled){
						getDynamicAttributesMap().put("originaldisabled", "disabled");
					}
					getDynamicAttributesMap().put("disabled", "disabled");
				}
			} else {
				getDynamicAttributesMap().remove("disabled");
			}
		}
				
		switch (tipo) {
		case IMAGE:
			includeTextTemplate("image");
			break;
		case BUTTON:
			if(url.startsWith("javascript:")){
				url = url.substring("javascript:".length());
			} else {
				url = "window.location='"+url+"'";
			}
			includeTextTemplate("button");
			break;
		case LINK:
			includeTextTemplate("link");
			break;
		}
	}

	private boolean hasAuthorization() {
		try {
			String partialURL = getPartialURL();
			if(partialURL.contains("?")){
				partialURL = partialURL.substring(0, partialURL.indexOf('?'));
			}
			return Neo.getApplicationContext().getAuthorizationManager().isAuthorized(partialURL, action, Neo.getRequestContext().getUser());
		} catch (Exception e) {
			throw new NeoException("Problema ao verificar autorização", e);
		}
	}
	
	
	private String getPartialURL(){
		if (url != null && url.startsWith(getRequest().getContextPath())) {
			return url.substring(getRequest().getContextPath().length());
		}
		String fullUrl = url == null ? Util.web.getFirstUrl() : (url.startsWith("/") ?  url : url);
		return fullUrl;
	}

	private Tipo definirTipo() {
		Tipo tipo = Tipo.LINK;
		if ("button".equalsIgnoreCase(type)) {
			tipo = Tipo.BUTTON;
		}
		if (img != null) {
			tipo = Tipo.IMAGE;
		}
		return tipo;
	}


	private String montarUrlCompleta() {
		if(url != null && url.startsWith("javascript:")){
			return url;
		}

		if(action != null && action.startsWith("javascript:")){
			url = action;
			action = null;
			return url;
		}

		String fullUrl = url == null ? Util.web.getFirstFullUrl() : (url.startsWith("/") ? Util.web.getFullUrl(getRequest(), url) : url);
		String separator = fullUrl.contains("?") ? "&" : "?";
		if (action != null) {
			fullUrl += separator + MultiActionController.ACTION_PARAMETER + "=" + action;
			separator = "&";
		}
		// adicionar parameters na url
		if (parameters != null) {
			fullUrl += separator + parameters.replace(";", "&");
		}
		return fullUrl;
	}

	public String getOnclick() {
		return onclick;
	}


	public String getAction() {
		return action;
	}

	public String getDescription() {
		return description;
	}

	public String getImg() {
		return img;
	}

	public String getParameters() {
		return parameters;
	}

	public String getType() {
		return type;
	}

	public String getUrl() {
		Tipo tipo = definirTipo();
		if(confirmationMessage != null && (tipo == Tipo.LINK || tipo == Tipo.IMAGE)){//TODO FAZER PARA OUTROS TIPOS
			return "javascript: if(confirm('"+confirmationMessage+"')){window.location = '"+url+"';}";
		}
		if(confirmationScript != null && (tipo == Tipo.LINK || tipo == Tipo.IMAGE)){
			if(!confirmationScript.endsWith(")")){
				confirmationScript = confirmationScript + "()";
			}
			String instrucao = "window.location = '"+url+"'";
			Object target = getDynamicAttributesMap().get("target");
			if(target != null && target.toString().equals("_blank")){
				instrucao = "window.open('" + url + "')";
			}
			
			return "javascript: if("+confirmationScript+"){" + instrucao + ";}";
		}
		return url;
	}
	
	public Boolean getCheckPermission() {
		return checkPermission;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getConfirmationMessage() {
		return confirmationMessage;
	}

	public void setConfirmationMessage(String confirmationMessage) {
		this.confirmationMessage = confirmationMessage;
	}
	
	public void setCheckPermission(Boolean checkPermission) {
		this.checkPermission = checkPermission;
	}
	
	public String getConfirmationScript() {
		return confirmationScript;
	}
	
	public void setConfirmationScript(String confirmationScript) {
		this.confirmationScript = confirmationScript;
	}
}
