<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	 
<c:choose>
	<c:when test="${(tag.dynamicAttributesMap['usehtml5'] == 'TRUE' || tag.dynamicAttributesMap['usehtml5'] == 'true') && android == false}">
		<input type="date"
			   id="${tag.id}"
			   name="${tag.name}"
			   value="${tag.valueToString}" 
			   maxlength="10"
			   size="11" 
			   ${tag.dynamicAttributesToString}/>
		<input type="hidden" name="${tag.name}_datePattern" value="yyyy-MM-dd"/>
	</c:when>
	<c:otherwise>
		<c:choose>
			<c:when test="${android == true}">
				<input type="text"
					   id="${tag.id}"
					   name="${tag.name}"
					   value="${tag.valueToString}" 
					   maxlength="10"
					   size="11" 
					   onChange="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){${tag.reloadOnChangeString}${tag.dynamicAttributesMap['change']}}" 
					   onfocus="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){this.select();lcs(this)}" 
					   onclick="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){event.cancelBubble=true;this.select();lcs(this)}"
					   onblur="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){verifica_data(this);${tag.dynamicAttributesMap['blur']}}" ${tag.dynamicAttributesToString}
					   />
					<input type="hidden" name="${tag.name}_datePattern" value="dd/MM/yyyy"/> 
			</c:when>
			<c:otherwise>
				<input type="text"
					   id="${tag.id}"
					   name="${tag.name}"
					   value="${tag.valueToString}" 
					   maxlength="10"
					   size="11" 
					   onKeyUp="mascara_data(this,event,'dd/MM/yyyy');" 
					   onKeyPress="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){hide_calendar();return valida_tecla_data(this, event,'dd/MM/yyyy');}"
					   onChange="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){${tag.reloadOnChangeString}${tag.dynamicAttributesMap['change']}}" 
					   onfocus="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){this.select();lcs(this)}" 
					   onclick="if(($(this).attr('readonly') == null || $(this).attr('readonly') == false) && (($(this).attr('escolhacalendario') == null || $(this).attr('escolhacalendario') == true))){event.cancelBubble=true;this.select();lcs(this)}"
					   onblur="if($(this).attr('readonly') == null || $(this).attr('readonly') == false){verifica_data(this);${tag.dynamicAttributesMap['blur']}}" ${tag.dynamicAttributesToString}/>
					<input type="hidden" name="${tag.name}_datePattern" value="dd/MM/yyyy"/> 
			</c:otherwise>
		</c:choose>
	</c:otherwise>
</c:choose>