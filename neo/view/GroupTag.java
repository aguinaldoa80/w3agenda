/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.view;

import java.util.Set;

import br.com.linkcom.neo.controller.crud.CrudController;
import br.com.linkcom.neo.view.combo.ComboTag;

/**
 * @author rogelgarcia
 * @since 07/02/2006
 * @version 1.1
 */
public class GroupTag extends ComboTag {
	
	protected Integer colspan;
	
	protected String legend;
	
	protected Boolean showBorder = true;
	
	// panelgridproperties
	
	protected int columns = 1;
	
	protected String style;

	protected String styleClass;

	protected String rowStyleClasses;

	protected String rowStyles;
	
	protected String columnStyleClasses;

	protected String columnStyles;
	
	protected Boolean showBotaoEditar = false;
	protected String linkBotaoEditar;
	
	protected Boolean showBotaoLimpar = false;
	protected String linkBotaoLimpar;
	
	@Override
	protected void doComponent() throws Exception {
		PanelTag panelTag = new PanelTag();
		PanelGridTag panelGridTag = new PanelGridTag();
		
		Set<String> keySet = getDynamicAttributesMap().keySet();
		for (String string : keySet) {
			if(string.startsWith("panel")){
				panelTag.setDynamicAttribute(null, string.substring("panel".length()), getDynamicAttributesMap().get(string));
				getDynamicAttributesMap().remove(string);
			}
		}
		
		panelGridTag.setDynamicAttributesMap(getDynamicAttributesMap());
		panelGridTag.setColumns(getColumns());
		panelGridTag.setStyle(getStyle());
		panelGridTag.setStyleClass(getStyleClass());
		panelGridTag.setRowStyleClasses(getRowStyleClasses());
		panelGridTag.setRowStyles(getRowStyles());
		panelGridTag.setColumnStyleClasses(getColumnStyleClasses());
		panelGridTag.setColumnStyles(getColumnStyles());
		panelGridTag.setUseParentPanelGridProperties(false);
		
		
		TextTag text = null; 
			
		if(colspan != null){
			panelTag.setDynamicAttribute(null, "colspan", colspan);
		}
		
		
		TagHolder panelHolder = new TagHolder(panelTag);
		TagHolder panelGridHolder = new TagHolder(panelGridTag);
		TagHolder textHolder = null;
		if(showBorder){
			if(legend != null){
				Boolean consultar = (Boolean)getRequest().getAttribute(CrudController.CONSULTAR);
				
				String styleFieldSet = "";
				if(getDynamicAttributesMap().containsKey("stylefieldset")){
					styleFieldSet = " style=\"" + getDynamicAttributesMap().get("stylefieldset").toString() + "\" ";
					getDynamicAttributesMap().remove("stylefieldset");
				}
				
				if(consultar == null || !consultar){
					String s = "<fieldset " + styleFieldSet + getDynamicAttributesToString() + "><legend>" + legend;
					
					
					if(showBotaoEditar != null && showBotaoEditar && linkBotaoEditar != null && !linkBotaoEditar.equals("")){
						s += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
								"<a onmouseover=\"Tip('Editar')\" style=\"text-transform: none;font-weight: normal; color: white;\" id=\"botaoeditar\" title=\"\" href=\"" + 
								linkBotaoEditar + 
								"\"><i class=\"fas fa-pencil-alt\" style=\"color: white\"></i> Editar</a>";
					}
					
					if(showBotaoLimpar != null && showBotaoLimpar && linkBotaoLimpar != null && !linkBotaoLimpar.equals("")){
						s += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
								"<a onmouseover=\"Tip('Limpar')\" style=\"text-transform: none;font-weight: normal; color: white;\" id=\"botaolimpar\" title=\"\" href=\"" + 
								linkBotaoLimpar + 
								"\"><i class=\"fas fa-sync-alt\" style=\"color: white;\"></i> Limpar</a>";
					}
									
					s += "</legend>";
					text = new TextTag(s,"</fieldset>");
				} else {
					text = new TextTag("<fieldset " + styleFieldSet + getDynamicAttributesToString() + "><legend>"+legend+"</legend>","</fieldset>");
				}
				
			} else {
				text = new TextTag("<fieldset>","</fieldset>");
			}
			textHolder = new TagHolder(text);
		}
		if(textHolder != null){
			panelHolder.addChild(textHolder);
			textHolder.addChild(panelGridHolder);
		} else {
			panelHolder.addChild(panelGridHolder);	
		}
		
		panelGridTag.setJspBody(getJspBody());
		
		invoke(panelHolder);
	}


	public Integer getColspan() {
		return colspan;
	}


	public int getColumns() {
		return columns;
	}


	public String getColumnStyleClasses() {
		return columnStyleClasses;
	}


	public String getColumnStyles() {
		return columnStyles;
	}


	public String getLegend() {
		return legend;
	}


	public String getRowStyleClasses() {
		return rowStyleClasses;
	}


	public String getRowStyles() {
		return rowStyles;
	}


	public Boolean getShowBorder() {
		return showBorder;
	}


	public String getStyle() {
		return style;
	}


	public String getStyleClass() {
		return styleClass;
	}


	public void setColspan(Integer colspan) {
		this.colspan = colspan;
	}


	public void setColumns(int columns) {
		this.columns = columns;
	}


	public void setColumnStyleClasses(String columnStyleClasses) {
		this.columnStyleClasses = columnStyleClasses;
	}


	public void setColumnStyles(String columnStyles) {
		this.columnStyles = columnStyles;
	}


	public void setLegend(String legend) {
		this.legend = legend;
	}


	public void setRowStyleClasses(String rowStyleClasses) {
		this.rowStyleClasses = rowStyleClasses;
	}


	public void setRowStyles(String rowStyles) {
		this.rowStyles = rowStyles;
	}


	public void setShowBorder(Boolean showBorder) {
		this.showBorder = showBorder;
	}


	public void setStyle(String style) {
		this.style = style;
	}


	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}
	
	public Boolean getShowBotaoEditar() {
		return showBotaoEditar;
	}
	
	public void setShowBotaoEditar(Boolean showBotaoEditar) {
		this.showBotaoEditar = showBotaoEditar;
	}
	
	public String getLinkBotaoEditar() {
		return linkBotaoEditar;
	}
	
	public void setLinkBotaoEditar(String linkBotaoEditar) {
		this.linkBotaoEditar = linkBotaoEditar;
	}


	public Boolean getShowBotaoLimpar() {
		return showBotaoLimpar;
	}


	public String getLinkBotaoLimpar() {
		return linkBotaoLimpar;
	}


	public void setShowBotaoLimpar(Boolean showBotaoLimpar) {
		this.showBotaoLimpar = showBotaoLimpar;
	}


	public void setLinkBotaoLimpar(String linkBotaoLimpar) {
		this.linkBotaoLimpar = linkBotaoLimpar;
	}
	
}

class TextTag extends BaseTag {
	
	protected String parte1;
	protected String parte2;
	
	public TextTag(String parte1, String parte2){
		this.parte1 = parte1;
		this.parte2 = parte2;
	}
	
	@Override
	protected void doComponent() throws Exception {
		getOut().println(parte1);
		doBody();
		getOut().println(parte2);
	}
}
