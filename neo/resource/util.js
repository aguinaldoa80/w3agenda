
		/**
		 * Fun??o utilizada como callback de janelas que foram criadas com o objectivo
		 * de selecionar algum bean.
		 * Possui as informa??es necess?rias para a janela saber como deve preencher o
		 * formu?rio que a chamou
		 */
		function selecionarCallbackObject(valueInput, labelInput, valueType, button, buttonUnselect, callback){
		    this.valueInput = valueInput;
			this.labelInput = labelInput;
			this.valueType  = valueType;
			this.button = button;
			this.buttonUnselect = buttonUnselect;
			this.callback = callback; 
		}
		
		function imprimirVoltar(){
			if(top.cadastrar){
				document.write("<input type='button' value='Voltar' onclick='refreshPai()'>");
			}
		}
		
		function refreshPai() {
			//top.opener.document.forms[0].ACAO.value = top.cadastrar;
			//alert(top.opener.document.forms[0].ACAO.value);
			top.opener.document.forms[0].submit();
			top.close();
		}
		
		/**
		 * Imprime o bot?o selecionar onde for necess?rio
		 * O bot?o selecionar s? ? impresso onde a classe for da hierarquia da classe que pediu para selecionar
		 */
		function imprimirSelecionar(listaClasses, valor, label){
			//alert('valor '+valor+'  label '+label+'    listaclasses '+listaClasses);
			//alert(window.top.opener.selecionarCallback);
			if(window.top.selecionarCallback){
				//document.write("dd");
				var ok = false;
				for(i in listaClasses){
					var clazz = listaClasses[i];
					if(clazz == window.top.selecionarCallback.valueType){
						ok = true;
					}
				}
				
				if(ok){
					//document.write("<a href=\"javascript:alert('Info: valor="+valor+" label="+label+"')\">info</a>&nbsp;");
					document.write("<a href=\"javascript:selecionar('"+addEscape(valor)+"','"+addEscape(label)+"')\">selecionar</a>&nbsp;");				
				}
				//DEBUG ----- c?digo abaixo ? debug descomente se nao aparecer o botao selecionar
				//else {
				//	alert('A classe \n'+listaClasses[0]+' \nn?o ? a mesma ou uma subclasse de \n'+top.selecionarCallback.valueType);
				//}
			}
		}		
		
		function addEscape(value){
			var newValue = '';
			for(var i=0; i<value.length; i++){
				switch(value.charCodeAt(i)){
					case 34 :
						newValue += "&quot;";
						break;
					case 39 :
						newValue += "\\'";
						break;
					case 92 :
						newValue += "\\\\";
						break;
					default:
						newValue += value.charAt(i);
						break;
				}
			}
			return newValue;
		}
		
		/**
		 *
		 */
		function selecionar(valor, label, forcombo){
			var isNN = navigator.appName.indexOf("Netscape")!= -1;
			
			if(top.selecionarCallback){
				if(forcombo){
				
					var callback = top.selecionarCallback;
					var callbackcallback = callback.callback;
					
					callbackcallback(label, valor);
					
					if(callback.valueInput){
						var onchangeFunction = callback.valueInput.onchange;
						if(onchangeFunction){
							onchangeFunction();
						}
					}

					if(isNN){
						setTimeout('top.close()', 500);
					} else {
						top.close();					
					}					
				} else {

					var callback = top.selecionarCallback;
					var callbackcallback = callback.callback;
					
					if(callback.labelInput){
						var onchangeFunction = callback.labelInput.onchange;
						callback.labelInput.value = label;
					}
					
					if(callback.valueInput){
						callback.valueInput.value = valor;
					}

					if(callbackcallback){
						callbackcallback(label,valor);
					}
							
					if(onchangeFunction){
						onchangeFunction();
						//alert(onchangeFunction);
					}
					
					if(callback.button)
						callback.button.style.display = 'none';
					if(callback.buttonUnselect)
						callback.buttonUnselect.style.display = '';
					
					if(isNN){
						setTimeout('top.close()', 500);
					} else {
						top.close();					
					}

				}
				
			}

		}
		
		
		function selecionarNaJanela(valor, label, selecionarCallback){
			var isNN = navigator.appName.indexOf("Netscape")!= -1;
			
			if(selecionarCallback){
				var callback = selecionarCallback;
				var callbackcallback = callback.callback;
				var onchangeFunction = callback.labelInput.onchange;
				callback.valueInput.value = valor;
				callback.labelInput.value = label;

				if(callbackcallback){
					callbackcallback(label,valor);
				}
						
				if(onchangeFunction){
					onchangeFunction();
					//alert(onchangeFunction);
				}
				
				if(callback.button)
					callback.button.style.display = 'none';
				if(callback.buttonUnselect)
					callback.buttonUnselect.style.display = '';
				
				if(isNN){
					setTimeout('top.close()', 500);
				} else {
					top.close();					
				}

			}

		}
		
		
		
		
		function preparaHtmlArea(editorurl){
			_editor_url = editorurl;
			var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
			if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; }
			if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; }
			if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; }
			if (win_ie_ver >= 5.5) {
			  document.write('<scr' + 'ipt src="' +editorurl+ 'editor.js"');
			  document.write(' language="Javascript1.2"></scr' + 'ipt>');  
			}
			else { 
				document.write('<scr'+'ipt>function editor_generate() { return false; }</scr'+'ipt>'); 
			}
			//alert(_editor_url);
		}
		
		function limparCombo(combo, includeblank, currentValue,blankLabel){
			pararEm = 0;
			var remove = 1;
			
			if(combo.type == 'select-multiple'){
				currentValue = false;
			}
	
			while(combo.options.length > pararEm + remove - 1){
				//alert((combo.options.length -1));
				if(currentValue){
					if(remove == 1 && currentValue != '<null>' && combo.options[combo.options.length - 1].value == currentValue){
						remove = 2;
					}
				}
				if((combo.options.length - remove) >= 0){
					combo.remove((combo.options.length - remove));
				}
				
			}		
			
			if(blankLabel == null) blankLabel = " ";
			
			var op = new Option();
			op.text = blankLabel;
			op.value = '<null>';
			combo.options.add(op);//for�ar o redimensionamento do form
			combo.remove(pararEm + remove - 1);
			
			
			if(includeblank){
				op = new Option();
				op.text = blankLabel;
				op.value = '<null>';
				combo.options.add(op);
			}
		}
		
		
		function enableProperties(form){
			var elements = form.elements;
			for(var i = 0; i < elements.length; i++){
				var element = elements[i];
				var disabled = element.getAttribute("originaldisabled");
				if(disabled == null){
					element.disabled = false;
				}
			}
		}
		
	function autoFieldFocus(){
		var k = 0;
		var totalForms = document.forms.length;
		var find = false;
		for(k = 0; k < totalForms; k++){
			var aForm = document.forms[k];
			if( aForm != null && aForm.elements[0]!=null) {
				var i;
				var max = aForm.length;
				for( i = 0; i < max; i++ ) {
					var focus = aForm.elements[i].getAttribute("setfocus");
					var disabled = aForm.elements[i].getAttribute("disabled");
					var id = aForm.elements[i].getAttribute("id");
					if(aForm.elements[i].type != "hidden" && aForm.elements[ i ].type != "button" &&
						!aForm.elements[i].disabled && (focus != null && focus == "true") &&
						!aForm.elements[i].readOnly) {
						//alert("encontrou "+aForm.elements[ i ].value + " document.forms["+k+"].elements["+i+"].focus() " + id);
						//setTimeout("document.forms["+k+"].elements["+i+"].focus()",10);						
						aForm.elements[ i ].focus();
						find = true;
						break;
					}
				}
				if(find) break;
			}
		}
	}
		