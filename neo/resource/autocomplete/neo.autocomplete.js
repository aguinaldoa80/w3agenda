$(document).ready(function(){
	applyAutocomplete();
});

//Fun��es utilizadas no autocomplete
function formatItem(row) {
	return retirarAcento(row[0]);
}
function formatResult(row) {
	return row[0];
}

/*function retirarAcento(varString) {
	var stringAcentos = new String('����������������������������');
	var stringSemAcento = new String('aaeouaoaeioucuAAEOUAOAEIOUCU');
	
	var i = new Number();
	var j = new Number();
	var cString = new String();
	var varRes = '';
	
	for (i = 0; i < varString.length; i++) {
		cString = varString.substring(i, i + 1);
		for (j = 0; j < stringAcentos.length; j++) {
			if (stringAcentos.substring(j, j + 1) == cString){
				cString = stringSemAcento.substring(j, j + 1);
			}
		}
		varRes += cString;
	}
	return varRes;
}*/


var mapRetirarAcento = {
	'�': 'a',
	'�': 'a', 
	'�': 'e',
	'�': 'o',
	'�': 'u',
	'�': 'a',
	'�': 'o',
	'�': 'a',
	'�': 'e',
	'�': 'i',
	'�': 'o',
	'�': 'u',
	'�': 'c',
	'�': 'u',
	'�': 'A',
	'�': 'A',
	'�': 'E',
	'�': 'O',
	'�': 'U',
	'�': 'A',
	'�': 'O',
	'�': 'A',
	'�': 'E',
	'�': 'I',
	'�': 'O',
	'�': 'U',
	'�': 'C',
	'�': 'U'
};

function retirarAcento(varString){
	return varString.replace(/[^A-Za-z0-9\[\] ]/g,function(a){
		var char = mapRetirarAcento[a]||a;
		return char;
	})
};


function applyAutocomplete(){
	$('input[funcao=autocomplete]').each(function(){
		
		var $el = $(this);
		
		var funcaoAtivar = function(){
			var haveAutocomplete = $el.attr('haveAutocomplete');
			if(typeof haveAutocomplete == 'undefined' || haveAutocomplete == null || haveAutocomplete != "true"){
				$(this).attr('haveAutocomplete', 'true');
				
				$el.unautocomplete();
				var element = this;
				var name = element.name.substring(0,element.name.length-6);
				
				var change = $el.attr('changeAutocomplete');
				//log("apply", name, change)
				var $elHidden = $("input[autocompleteId='" + name + "_value']");
				$elHidden.change(function(){eval(change)});
				var $elLabel = $("input[name='" + name + "_label']");
				
				if($elHidden.val() == '<null>'){
					setarAutocomplete(name,  $elLabel, $elHidden);
				}
				
				var formatted = $elLabel.val();
				var newFormatted = formatted.split('<span>');			
				$elLabel.val(newFormatted[0]);
				
				verificaBotoes(name);
			}
		};
		
		$el.click(funcaoAtivar);
		$el.blur(funcaoAtivar);
		$el.focus(funcaoAtivar);
		
		$el.click();
		
			
//		c�digo anterior
//		
//		var $el = $(this);
//		$el.unautocomplete();
//		var element = this;
//		var name = element.name.substring(0,element.name.length-6);
//		
//		var change = $el.attr('changeAutocomplete');
//		//log("apply", name, change)
//		var $elHidden = $('input[autocompleteId=' + name + '_value]');
//		$elHidden.change(function(){eval(change)});
//		var $elLabel = $('input[name=' + name + '_label]');
//		
//		if($elHidden.val() == '<null>'){
//			setarAutocomplete(name,  $elLabel, $elHidden);
//		}
//		
//		var formatted = $elLabel.val();
//		var newFormatted = formatted.split('<span>');			
//		$elLabel.val(newFormatted[0]);
//		
//		verificaBotoes(name);
	});
}

function applyAutocompleteInput($el){
	var funcaoAtivar = getFuncaoAtivar($el);
	
	$el.click(funcaoAtivar);
	$el.blur(funcaoAtivar);
	$el.focus(funcaoAtivar);
	
	$el.click();
}

function getFuncaoAtivar($el){
	return function(){
		var haveAutocomplete = $el.attr('haveAutocomplete');
		if(typeof haveAutocomplete == 'undefined' || haveAutocomplete == null || haveAutocomplete != "true"){
			$(this).attr('haveAutocomplete', 'true');
			
			$el.unautocomplete();
			var element = this;
			var name = element.name.substring(0,element.name.length-6);
			
			var change = $el.attr('changeAutocomplete');
			//log("apply", name, change)
			var $elHidden = $("input[autocompleteId='" + name + "_value']");
			$elHidden.change(function(){eval(change)});
			var $elLabel = $("input[name='" + name + "_label']");
			
			if($elHidden.val() == '<null>'){
				setarAutocomplete(name,  $elLabel, $elHidden);
			}
			
			var formatted = $elLabel.val();
			var newFormatted = formatted.split('<span>');			
			$elLabel.val(newFormatted[0]);
			
			verificaBotoes(name);
		}
	};
}

function excluirAutocomplete(name){
	var inputLabel = $("input[name='" + name + "_label']");
	var inputValue = $("input[autocompleteId='" + name + "_value']");
	var includeConfirmExcludeAutocomplete = inputLabel.attr('includeConfirmExcludeAutocomplete');
	
	var continua = true;
	if(includeConfirmExcludeAutocomplete == "true"){
		continua = confirm('Deseja realmente deselecionar este valor?');
	}

	if(continua){
		inputLabel.val('');
		inputValue.val('<null>');	
		inputValue.change();
		setarAutocomplete(name);
		verificaBotoes(name);
	}
}

function verificaBotoes(name, inputLabel, inputValue){
	if(typeof inputLabel == 'undefined' || typeof inputValue == 'undefined'){
		inputLabel = $("input[name='" + name + "_label']");
		inputValue = $("input[autocompleteId='" + name + "_value']");
	}
	if(inputValue.val() != '<null>'){
		$("span[autocompleteId='" + name + "_Selecionado']").show();
		$("span[autocompleteId='" + name + "_NaoSelecionado']").hide();
		
		eventoExcluirSelecionado(name, inputLabel, inputValue);
		inputLabel.attr('readOnly','true');
	} else {
		$("span[autocompleteId='" + name + "_Selecionado']").hide();
		$("span[autocompleteId='" + name + "_NaoSelecionado']").show();
		
		inputLabel.removeAttr('readOnly');
	}
}

function setarAutocomplete(name, _$elLabel, _$elHidden){
	var elLabel = _$elLabel || $("input[name='" + name + "_label']");
	var $elHidden = _$elHidden || $("input[autocompleteId='" + name + "_value']")
	
	var beanName = elLabel.attr('beanName');
	var loadFunctionAutocomplete = elLabel.attr('loadFunctionAutocomplete');
	var propertyLabel = elLabel.attr('propertyLabel');
	var propertyMatch = elLabel.attr('propertyMatch');
	var getterLabel = elLabel.attr('getterLabel');
	var maxResult = elLabel.attr('maxResult');
	var widthAutocomplete = elLabel.attr('widthAutocomplete');
	var matchAutocomplete = elLabel.attr('matchAutocomplete');
	var startAutocomplete = elLabel.attr('startAutocomplete');
	//local
	var isLocal = elLabel.attr('local')=="true";
	var fnFormatItem = eval(elLabel.attr('formatitem')) || formatItem;
	var fnFormatResult = eval(elLabel.attr('formatresult')) || formatResult;
	var fnFormatMatch = eval(elLabel.attr('formatmatch')) ;
	var fnMatchSubset = eval(elLabel.attr('fnMatchSubset')) ;
	
	//var descriptionProperty = elLabel.attr('valueField');
	var valueField = elLabel.attr('valueField');
	var dataLocal = eval(elLabel.attr('data'));
	var package = elLabel.attr("package");
	if(loadFunctionAutocomplete === undefined) {loadFunctionAutocomplete = '';}
	if(propertyLabel === undefined) {propertyLabel = '';}
	if(propertyMatch === undefined) {propertyMatch = '';}
	if(getterLabel === undefined) {getterLabel = '';}
	if(maxResult === undefined) {maxResult = 30;}
	if(widthAutocomplete === undefined) {widthAutocomplete = '300';}
	if(matchAutocomplete === undefined) {matchAutocomplete = 'true';}
	
	if(startAutocomplete === undefined) {
		startAutocomplete = 3;
	} else {
		startAutocomplete = parseFloat(startAutocomplete);
	}

	var url = contextoAutoComplete + '/ajax/autocomplete?beanName=' + beanName + '&functionLoad=' + loadFunctionAutocomplete + '&propertyLabel=' + propertyLabel + '&propertyMatch=' + propertyMatch + '&getterLabel=' + getterLabel + '&matchOption=' + matchAutocomplete;
	var dataSource = dataLocal || url;
	
	var optsAC = {
		width: parseFloat(widthAutocomplete),
		formatItem: fnFormatItem,
		formatResult: fnFormatResult,
		minChars: startAutocomplete,
		max: maxResult
	};
//	formatMatch: fnFormatMatch,
	if(fnFormatMatch) optsAC.formatMatch = fnFormatMatch
	if(fnMatchSubset) {optsAC.fnMatchSubset = fnMatchSubset; optsAC.matchContains = true;}
	if(isLocal) {optsAC.isLocal = isLocal;} 
//	optsAC.formatMatch = function(rawValue, i, length){return rawValue.nome;}
	
	elLabel.autocomplete(dataSource, optsAC).result(function(event, data, formatted) {
		//log(event, data, formatted)
		if(isLocal){
			log("result", formatted, data)
			elLabel.val( (formatted.indexOf('<span')==-1 ? formatted : formatted.substr(0, formatted.indexOf('<span'))) );
			$elHidden.val(package+"["+valueField+"="+data[valueField]+"]");//valueField
			elLabel.blur();
			$elHidden.focus();
		}else{
			var newFormatted = formatted.split('<span>');
			elLabel.val(newFormatted[0]);
			$elHidden.val(data[1]);
		}
		$elHidden.change();	
		elLabel.unautocomplete();
		
		
		verificaBotoes(name);
	});
}


function eventoExcluirSelecionado(name, inputLabel, inputValue) {
	if(typeof inputLabel == 'undefined' || typeof inputValue == 'undefined'){
		inputLabel = $("input[name='" + name + "_label']");
		inputValue = $("input[autocompleteId='" + name + "_value']");
	}
	inputLabel.keyup(function(event) {
		if (inputValue.val() != '<null>') {
			var code = (event.keyCode ? event.keyCode : event.which);
			if (code == 8) { 
				excluirAutocomplete(name);
			} else {
				event.preventDefault();
			}
		}
	});
}