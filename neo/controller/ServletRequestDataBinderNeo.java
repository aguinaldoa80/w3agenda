/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.controller;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.PropertyEditor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.PropertyValues;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;

import br.com.linkcom.neo.bean.editors.EmbeddedIdPropertyEditor;
import br.com.linkcom.neo.bean.editors.FileEditor;
import br.com.linkcom.neo.core.standard.Neo;
import br.com.linkcom.neo.exception.NeoException;
import br.com.linkcom.neo.types.File;
import br.com.linkcom.neo.util.Util;
import br.com.linkcom.neo.view.ComboReloadGroupTag;

/**
 * @author rogelgarcia
 * @since 31/01/2006
 * @version 1.1
 */
public class ServletRequestDataBinderNeo extends ServletRequestDataBinder {
	
	//propriedade especial.. seta o campo para null
	public static final String EXCLUDE = "_excludeField";
	public static final String DATE_PATTERN = "_datePattern";
	public static final String AUTOCOMPLETE_PATTERN = "_label";

	public ServletRequestDataBinderNeo(Object target, String objectName) {
		super(target, objectName);
		
		//Se � uma classe persistente vou verificar o tipo de ID, se for chave prim�ria composta
		//vou registrar um custom editor.
		if (target.getClass().isAnnotationPresent(Entity.class) || target.getClass().isAnnotationPresent(MappedSuperclass.class)){
			try {
				String idPropertyName = Neo.getApplicationContext().getBeanDescriptor(target).getIdPropertyName();
				PropertyDescriptor descriptor = new PropertyDescriptor(idPropertyName, target.getClass());
				if (descriptor.getReadMethod().getAnnotation(EmbeddedId.class) != null){
					registerCustomEditor(descriptor.getPropertyType(), new EmbeddedIdPropertyEditor(descriptor.getPropertyType()));
				}
			} catch (IntrospectionException e) {
				logger.fatal("Erro ao fazer reflectiona na propriedade ID da classe " + target.getClass());
			}
		}
	}

	
	private static final String OBJECT_VALUE_REGEX = "(([a-zA-Z_0-9]*\\.)++\\w*)\\[(.*)\\]";
	//private static final String OBJECT_VALUE_REGEX = "((.*\\.)++\\w*)\\[(.*)\\]"; // removida por pedro.


	protected BindException createErrors(Object target, String objectName) {
		return new BindExceptionNeo(target, objectName);
	}
	
	@Override
	protected void doBind(MutablePropertyValues mpvs) {
		//cria se necess�rio o file property editor
		PropertyEditor customEditor = this.getBeanWrapper().findCustomEditor(File.class, null);
		if(customEditor == null){
			final Class<?>[] classes = Neo.getApplicationContext().getClassManager().getAllClassesOfType(File.class);
			if(classes.length == 1){
				FileEditor fileEditor = new FileEditor(){
					@Override
					protected File createFile(Object value) {
						try {
							return (File) classes[0].newInstance();
						} catch (InstantiationException e) {
							throw new RuntimeException(e);
						} catch (IllegalAccessException e) {
							throw new RuntimeException(e);
						}
					}
				};
				registerCustomEditor(classes[0], fileEditor);
			}
		}
		PropertyValue[] propertyValues = mpvs.getPropertyValues();
		boolean trimProperties = Util.config.getDefaultConfigBoolean(false, "trimProperties");
		List<String> listProperties = new ArrayList<String>();
		for (int i = 0; i < propertyValues.length; i++) {
			PropertyValue propertyValue = propertyValues[i];
			if(propertyValue.getName().endsWith(DATE_PATTERN)){
				extractDatePattern(mpvs, propertyValue);
			}else if(propertyValue.getName().endsWith(AUTOCOMPLETE_PATTERN)){
				listProperties.add(propertyValue.getName());	
			}
		}
		
		//traduz parametros do tipo meupacote.MinhaClasse[id=1], para os objetos correspondentes		
		for (int i = 0; i < propertyValues.length; i++) {
			PropertyValue value = propertyValues[i];
			boolean isAutocomplete = listProperties.contains(value.getName().concat(AUTOCOMPLETE_PATTERN));
			if(isObjectValue(value.getValue(), isAutocomplete)){
				Object translatedObjectValue = translateObjectValue(value.getName(), value.getValue(), mpvs);
				mpvs.addPropertyValue(value.getName(), translatedObjectValue);
				if(translatedObjectValue == null){
					propertyValues = mpvs.getPropertyValues();
					i = -1;
					continue;
				}
			} else if(isObjectArrayValue(value.getValue())){
				mpvs.addPropertyValue(value.getName(), translateObjectArrayValue(value.getName(), value.getValue()));
			} else if(value.getValue() instanceof String && value.getValue() != null){
				String valueStr = (String) value.getValue();
				if(trimProperties){
					valueStr = valueStr.trim();
				}
				
				if(valueStr.equals("")){
					mpvs.addPropertyValue(value.getName(), null);
				} else {
					mpvs.addPropertyValue(value.getName(), valueStr);
				}
			}
		}
		

		super.doBind(mpvs);
	}



	/**
	 * Faz a tradu��o de parametros do tipo meupacote.MinhaClasse[id=1] para o objeto esperado<BR>
	 * 
	 * @param name
	 * @param value
	 * @param mpvs 
	 * @return
	 */
	public static Object translateObjectValue(String name, Object value, MutablePropertyValues mpvs) {
		if("<null>".equals(value)){
			//temos que remover todos os nomes subsequentes ao objeto nulo..
			//ex.: se tivermoos municipio = <null>
			//municipio.uf tamb�m tem que ser nulo
			
			//modificado por Pedro em 16/10/2007
			//tamb�m � poss�vel dar um translate com o mpvs null, neste caso apenas retorna o objeto como null.
			if(mpvs == null) return null;
			
			PropertyValue[] propertyValues = mpvs.getPropertyValues();
			for (int i = 0; i < propertyValues.length; i++) {
				if(propertyValues[i].getName().startsWith(name + ".")){
					mpvs.removePropertyValue(propertyValues[i]);
				}
			}
			return null;
		}
		String valueString = value.toString();
		Pattern pattern = Pattern.compile(OBJECT_VALUE_REGEX,Pattern.DOTALL);
		Matcher matcher = pattern.matcher(valueString);
		
		Object resultado = null;
		if (matcher.find()) {
			String nomeClasse = matcher.group(1);
			
			String nameValuesString = matcher.group(3);			
			PropertyValues properties = getPropertyValues(nameValuesString);
			
			Class<?> clazz;
			try {
				clazz = Class.forName(nomeClasse);
			} catch (ClassNotFoundException e) {
				throw new NeoException("N�o foi poss�vel instanciar classe ["+nomeClasse+"] da propriedade "+name, e);
			}
			if(mpvs == null){
				resultado = createObject(properties, clazz);	
			} else {
				PropertyValue[] propertyValues = properties.getPropertyValues();
				for (PropertyValue pv : propertyValues) {
					PropertyValue propertyValue = new PropertyValue(name+"."+pv.getName(), pv.getValue());
					mpvs.addPropertyValue(propertyValue);
				}
				resultado = createObject(properties, clazz);
			}
		}
		return resultado;
	}

	public static Object createObject(PropertyValues properties, Class<?> clazz) {
		Object resultado = BeanUtils.instantiateClass(clazz);
		ServletRequestDataBinder binder = new ServletRequestDataBinderNeo(resultado, null);
		binder.bind(properties);
		return resultado;
	}

	private static PropertyValues getPropertyValues(String nameValuesString) {
		MutablePropertyValues properties = new MutablePropertyValues();
		
		/* Este m�todo foi modificado 18/02/2009
		 * Antigamente o value pair era pego atrav�s de um split no caracter v�rgula. 
		 * Agora os caracteres especiais chegam com escape / seguido do caracter especial. 
		 * Escape indica para desconsiderar o caracter especial.
		 * Tom�s Rabelo
		  
		  Forma antiga:
		StringTokenizer tokenizer = new StringTokenizer(nameValuesString, ",");
		while (tokenizer.hasMoreTokens()){
			String token = tokenizer.nextToken();
			String nameValuePair[] = token.trim().split("=");
			PropertyValue propertyValue = new PropertyValue(nameValuePair[0], nameValuePair[1]);
			properties.addPropertyValue(propertyValue);
		}
    	*/
		
		String[] nameValuePair = new String[2];
		StringBuilder stringBuilder = new StringBuilder();
		for(int i = 0; i < nameValuesString.length(); i++){  
			if(nameValuePair[0] != null && nameValuePair[1] != null){
				properties.addPropertyValue(nameValuePair[0], nameValuePair[1]);
				nameValuePair = new String[2];
			}
			switch (nameValuesString.charAt(i)) {
				case 61 :
					if(nameValuesString.charAt(i-1) != 47){
						nameValuePair[0] = stringBuilder.toString();
						stringBuilder = new StringBuilder();
						continue;
					}
				case 44 :
					if(nameValuesString.charAt(i-1) != 47){
						nameValuePair[1] = stringBuilder.toString();
						stringBuilder = new StringBuilder();
						continue;
					}
				case 47 :
					if(nameValuesString.charAt(i-1) != 47)
						continue;
			}
			stringBuilder.append(nameValuesString.charAt(i));
		}
		nameValuePair[1] = stringBuilder.toString();
		properties.addPropertyValue(nameValuePair[0], nameValuePair[1]);
		return properties; 
	}
	
	public static boolean isObjectValue(Object value) {
		return isObjectValue(value, false);
	}
	
	public static boolean isObjectValue(Object value, boolean isAutocomplete) {
		if(value instanceof String){
			String string = (String) value;
			if(string.equals("<null>")){
				return true;
			}
			//fazer uma verificacao r�pida para evitar o matches que deu pau em determinadas strings
			char[] toCharArray = string.toCharArray();
			if (toCharArray.length > 3) {
				boolean temColchetes = false;
				for (int i = 0; i < toCharArray.length; i++) {
					if(toCharArray[i] == '['){
						temColchetes = true;
						//se tem abre colchetes tem grandes chances de ser um objectValue entao vamos dar o brake para cair no matches
						break;
					}
					if(toCharArray[i] == ' '){
						//se achou espaco em branco antes de um colchetes nao � objectvalue. Pode retornar falso
						return false;
					}
				}			
			
				//verifcar se termina com ']'
				int i = toCharArray.length - 1;
				while (toCharArray[i] == ' ') {
					i--;
				}
				if (toCharArray[i] != ']') {
					return false; // se nao termina com ] pode retornar falso porque nao � objectValue
				}
				
				if(!temColchetes){
					return false;
				}
			} else {
				//se tiver menos de 3 caracteres pode retornar falso
				return false;
			}
			
			if((string.matches(OBJECT_VALUE_REGEX) || "<null>".equals(string)) && (!string.contains(ComboReloadGroupTag.PARAMETRO_SEPARATOR) || isAutocomplete)){
				return true;
			}	
		}
		return false;
	}
	
	private boolean isObjectArrayValue(Object value) {
		if(value != null && value.getClass().isArray()){
			if(((Object[])value).length > 0){
				Object svalue = ((Object[])value)[0];
				if(svalue instanceof String && (svalue.toString().matches(OBJECT_VALUE_REGEX) || "<null>".equals(svalue))){
					return true;
				}
			}
		}
		return false;
	}
	
	private Object translateObjectArrayValue(String name, Object value) {
		List<Object> list = new ArrayList<Object>();
		Object[] array = (Object[]) value;
		
		for (Object object : array) {
			list.add(translateObjectValue(name, object, null));
		}
		
		return list.toArray();
	}

	@Override
	protected void checkFieldMarkers(MutablePropertyValues mpvs) {
		super.checkFieldMarkers(mpvs);
		// checar se existe algum campo com remove
		// se existir o valor ser� setado para null
		PropertyValue[] propertyValues = mpvs.getPropertyValues();
		for (PropertyValue propertyValue : propertyValues) {
			Pattern compile = Pattern.compile("(.*)"+EXCLUDE);
			Matcher matcher = compile.matcher(propertyValue.getName());
			if(matcher.matches() && new Boolean((String)propertyValue.getValue())){
				String fieldName = matcher.group(1);
				mpvs.removePropertyValue(fieldName);
			}
		}

	}
	
	private void extractDatePattern(MutablePropertyValues mpvs, PropertyValue propertyValue) {
		String dateProperty = propertyValue.getName().substring(0, propertyValue.getName().length() - DATE_PATTERN.length());
		PropertyValue pv = mpvs.getPropertyValue(dateProperty);
		if(pv != null && pv.getValue() != null && pv.getValue() instanceof String){
			String dataString = (String) pv.getValue();
			SimpleDateFormat dateFormat = new SimpleDateFormat((String) propertyValue.getValue());
			try {
				mpvs.addPropertyValue(new PropertyValue(dateProperty, dateFormat.parse(dataString)));
			} catch (ParseException e) {
				// se n�o conseguir converter n�o fazer nada.. deixa a exce��o vazar
			}
		}
	}

}
