/*
 * Neo Framework http://www.neoframework.org
 * Copyright (C) 2007 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package br.com.linkcom.neo.controller.resource;

import java.math.BigInteger;
import java.security.SecureRandom;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import br.com.linkcom.neo.controller.Action;
import br.com.linkcom.neo.core.web.NeoWeb;
import br.com.linkcom.neo.core.web.WebRequestContext;
import br.com.linkcom.neo.view.ajax.JsonModelAndView;
import br.com.linkcom.sined.geral.bean.Parametrogeral;
import br.com.linkcom.sined.geral.service.ParametrogeralService;
import br.com.linkcom.sined.util.SinedUtil;
import br.com.linkcom.sined.util.controller.FiltroListagemSined;
import br.com.linkcom.sined.util.neo.ReportFileServlet;

/**
 * @author rogelgarcia
 * @since 02/02/2006
 * @version 1.1
 */
public abstract class ResourceSenderController<FILTRO> extends AbstractResourceSenderController<FILTRO> {

	@Override
	public ModelAndView doGerar(WebRequestContext request, FILTRO filtro) throws Exception {
		HttpServletResponse response = request.getServletResponse();
		Resource recurso = generateResource(request, filtro);
		if (recurso == null) {
			// TODO DEVERIA SER CRIADO OUTRO CONTROLER PARA MANDAR EMAILS POR EXEMPLO
			// ESSE AQUI � PARA ENVIAR COISAS PARA O CLIENTE, FAZER DOWNLOAD
			return goToAction(FILTRO);
		}
		response.setContentType(recurso.getContentType());

		if(filtro instanceof FiltroListagemSined && Boolean.TRUE.equals(((FiltroListagemSined) filtro).getPodeVisualizarReportEmTela())
			&& !ParametrogeralService.getInstance().getBoolean(Parametrogeral.GERAR_RELATORIO_PDF)){
			String key = new BigInteger(130, new SecureRandom()).toString(32);
			ReportFileServlet.add(key, recurso);
			System.out.println("URL da requisi��o: "+NeoWeb.getRequestContext().getServletRequest().getRequestURL().toString());
			request.setAttribute("reportFileURL", SinedUtil.getUrlWithContext()+"/REPORTFILE/"+key+"."+recurso.getContentType().split("/")[1]);
			request.setAttribute("reportFileURLIframe", SinedUtil.getUrlWithContext()+"/REPORTFILE/"+key+"."+recurso.getContentType().split("/")[1]);
			request.setAttribute("isHttps", SinedUtil.getUrlWithContext().indexOf("https") != -1);
			
			String modulo = (String) NeoWeb.getRequestContext().getAttribute("NEO_MODULO");
			if(modulo != null && modulo.equals("pub")){
				return new ModelAndView("direct:relatorio/relatorioPreview");
			} else {
				return new ModelAndView("relatorio/relatorioPreview");
			}
		}
		response.addHeader("Content-Disposition", "attachment; filename=\"" + recurso.getFileName() + "\";");
		response.getOutputStream().write(recurso.getContents());
		return null;
	}

	public abstract Resource generateResource(WebRequestContext request, FILTRO filtro) throws Exception;
	
	@Action("gerouResourceJson")
	public ModelAndView doGerouResourceJson(WebRequestContext request, FILTRO filtro) throws Exception {
		Resource resource = null;
		
		try {
			resource = generateResource(request, filtro);
		}catch (Exception e) {
		}
		
		return new JsonModelAndView().addObject("gerou", resource!=null);
	}
}
